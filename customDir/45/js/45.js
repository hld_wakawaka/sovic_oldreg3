$(function(){
    var money_fee_area    = $("#money_fee_area");
    var money_option_area = $("#money_option_area");
    var options = {};
    if(money_fee_area.size() > 0){

        var total_area = (money_option_area.size() > 0) ? money_option_area : money_fee_area;
        total_area.after('<p class="r">Total : <span id="total_price">0</span></p>');

        if((money_option_area.size() > 0)){
            options = $("select", money_option_area);
            options.bind("change", function(){   total_update(); });
        }
        money_fee_area.bind("click", function(){ total_update(); });

        total_update();
    }


    function total_update(){
        var total = 0;

        // fee
        // 単一
        if($("#money_fee_area").hasClass("fee1")){
            total = parseInt($("#money_fee_area #p1_0").text());
        // 選択
        }else{
            // 選択した金額
            var fee = $("input:radio", money_fee_area).filter(":checked").val();
            if(typeof fee !== "undefined") {
                total = parseInt($("#p1_"+fee).text());
            }
        }

        // option
        options.each(function(obj){
            var ticket = parseInt($("option:selected", this).val());
            if(ticket >= 1){
                var id    = $(this).attr("id");
                var price = parseInt($("#option_"+id, money_option_area).text());
                total += price*ticket;
            }
        });

        if(!$.isNumeric(total)) total = 0;
        $("#total_price").text(number_format(total));


        function number_format(num) {
          return num.toString().replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,')
        }

    }
});