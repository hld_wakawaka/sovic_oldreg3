$(function(){

    $(".edata27").bind("change", function(){
        var chk = $(this).filter(":checked").val(); // input:radio,check
        if(typeof chk == 'undefined'){ return; }

        chk = parseInt(chk);  // convert to integer

        $("table.case1 :input").prop("disabled", true);
        $("table.case2 :input").prop("disabled", true);
        $("table.case3 :input").prop("disabled", true);

        $("table.case"+chk+" :input").prop("disabled", false);
    }).change();

});
