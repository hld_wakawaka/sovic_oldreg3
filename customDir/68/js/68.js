
$(document).ready(function(){

    // ランチョンセミナーの単一選択制御
    $("#p1_4,#p1_5,#p1_6,#p1_7").bind("change", function(){
        var chk = $(this, "option:selected").val();
        if(typeof chk == 'undefined'){ return; }
        
        var id  = $(this, "option:selected").attr("id");
        var val = $(this, "option:selected").val();

        $("#p1_4,#p1_5,#p1_6,#p1_7").each(function(){ $(this).prop("disabled", false); });
        if(val.length > 0){
            $("#p1_4,#p1_5,#p1_6,#p1_7").each(function(){ $(this).prop("disabled", false); });
            $("#p1_4,#p1_5,#p1_6,#p1_7").each(function(){ $(this).prop("disabled", true); });
            $('#'+id).prop('disabled', false);
        }
    });


    // モーニングセミナーの単一選択制御
    $("#p1_8,#p1_9,#p1_10").bind("change", function(){
        var chk = $(this, "option:selected").val();
        if(typeof chk == 'undefined'){ return; }
        
        var id  = $(this, "option:selected").attr("id");
        var val = $(this, "option:selected").val();

        $("#p1_8,#p1_9,#p1_10").each(function(){ $(this).prop("disabled", false); });
        if(val.length > 0){
            $("#p1_8,#p1_9,#p1_10").each(function(){ $(this).prop("disabled", false); });
            $("#p1_8,#p1_9,#p1_10").each(function(){ $(this).prop("disabled", true); });
            $('#'+id).prop('disabled', false);
        }
    });

    $("#money_fee_area").feeCalculator({subtarget:["#money_option_area"], showElement:"#money_option_area", 'showHTML': '<p class="r middle"><b><u>お支払合計金額: <span id="total_price">0</span>円</u></b></p>'});
});

