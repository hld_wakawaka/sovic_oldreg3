<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 * 
 * @author sovic kato
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource80 implements externalResource
{
	
	/**
	 * 項目の並び替えを行う
	 *
	 * @param  $obj ページオブジェクト
	 * @return void
	 * */

	public function sortFormIni($obj){
            //---------------------
            // グループ1 並び替え
            //---------------------
            $arrGroup1 =& $obj->arrItemData[1];
            // 入れ替え
            $array = array();

            foreach($arrGroup1 as $key => $data){
                switch($key){
                    case 57:
                        $array[26] = $arrGroup1[26];
                        $array[2] = $arrGroup1[2];
                        $array[7] = $arrGroup1[7];
                        $array[1] = $arrGroup1[1];
                        $array[57] = $arrGroup1[57];
                        $array[28] = $arrGroup1[28];
                        $array[63] = $arrGroup1[63];
                        $array[64] = $arrGroup1[64];
                        $array[69] = $arrGroup1[69];
                        $array[70] = $arrGroup1[70];
                        $array[71] = $arrGroup1[71];
                        $array[10] = $arrGroup1[10];
                        $array[13] = $arrGroup1[13];
                        $array[14] = $arrGroup1[14];
                        $array[72] = $arrGroup1[72];
                        $array[73] = $arrGroup1[73];
                        $array[74] = $arrGroup1[74];
                        $array[58] = $arrGroup1[58];
                        $array[17] = $arrGroup1[17];
                        $array[75] = $arrGroup1[75];
                        $array[76] = $arrGroup1[76];
                        $array[21] = $arrGroup1[21];
                        $array[77] = $arrGroup1[77];
                        $array[25] = $arrGroup1[25];
                        $array[78] = $arrGroup1[78];
                        $array[79] = $arrGroup1[79];
                        $array[80] = $arrGroup1[80];
                        $array[122] = $arrGroup1[122];
                        $array[123] = $arrGroup1[123];
                        $array[124] = $arrGroup1[124];
                        $array[125] = $arrGroup1[125];
                        $array[126] = $arrGroup1[126];
                        $array[127] = $arrGroup1[127];
                        $array[128] = $arrGroup1[128];
                        $array[129] = $arrGroup1[129];
                        $array[130] = $arrGroup1[130];
                        $array[81] = $arrGroup1[81];
                        $array[82] = $arrGroup1[82];
                        $array[115] = $arrGroup1[115];
                        $array[116] = $arrGroup1[116];
                        $array[117] = $arrGroup1[117];
                        $array[118] = $arrGroup1[118];
                        $array[120] = $arrGroup1[120];
                        $array[121] = $arrGroup1[121];
                        break;
                    default:
                        $array[$key] = $data;
                        break;
                }
            }
            $arrGroup1 = $array;
	}

}