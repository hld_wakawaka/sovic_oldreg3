<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."application/model/package.model.php");

class Mng_Entry77 extends AbstracePackage
{

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_detail_premain($obj){
        // パッケージ機能のロード
        parent::mng_detail_premain($obj);
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------
    
//     /** CSVヘッダ-グループ1生成 */
//     function entry_csv_entryMakeHeader1($obj, $all_flg=false){
    
//         $group = 1;
//         $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
//         foreach($obj->arrItemData[$group] as $_key => $_data){
//             // 表示する設定の場合出力
//             if($_data["item_view"] == "1") continue;
    
//             // 画面に表示する項目の名称
//             $name = strip_tags($_data["item_name"]);
    
//             if($name != "カテゴリー") {
//                 $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
//             }
            
//         }
//         return $groupHeader[$group];
//     }
    
//     /** CSV出力データ グループ1生成 */
//     function entry_csv_entryMakeData1($obj, $pa_param, $all_flg=false){
       
//         $group = 1;
//         $groupBody[$group] = array();
//         $groupBody[$group][] = $pa_param["entry_no"];
//         foreach($obj->arrItemData[$group] as $_key => $_data){
//             // 表示する設定の場合出力
//             if($_data["item_view"] == "1") continue;
    
//             $item_id = $_data["item_id"];
    
//             $methodname = "csvfunc".$item_id;
//             // カスタマイズあり
//             if(method_exists($this->exClass, $methodname)) {
//                 $wk_body = $this->exClass->$methodname($obj, $group, $pa_param, $item_id);
    
//                 // カスタマイズなし
//             } else {
//                 // 任意項目
//                 if($_data["controltype"] == "1"){
//                     if($_data["item_name"] != "ｘｘｘ"){
//                         $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array("", ","), false);
//                         $wk_body = trim($wk_body, ",");
//                     }
                    
    
//                     // 標準項目
//                 }else{
//                     switch($item_id){
//                         //会員・非会員
//                     	case 8:
//                     	    $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_kaiin[$pa_param["edata".$item_id]] : "";
//                     	    break;
//                     	    //連絡先
//                     	case 16:
//                     	    $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_contact[$pa_param["edata".$item_id]] : "";
//                     	    break;
//                     	    //都道府県
//                     	case 18:
//                     	    //英語フォームの場合
//                     	    if($GLOBALS["userData"]["lang"] == "2"){
//                     	        $wk_body =  ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
//                     	    }
//                     	    else{
//                     	        $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["prefectureList"][$pa_param["edata".$item_id]] : "";
//                     	    }
//                     	    break;
//                     	    //共著者の有無
//                     	case 29:
//                     	    $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";
//                     	    break;
    
//                     	    //共著者の所属機関
//                     	case 31:
//                     	    $wk_kikan_names = array();
//                     	    $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);
    
//                     	    $i =  "";
//                     	    $set_select = array();
//                     	    foreach($wk_kikanlist as $num => $val){
//                     	        $i = $num+1;
//                     	        $set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
//                     	        $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
//                     	    }
//                     	    $wk_body = str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names));
//                     	    break;
    
//                     	    //Mr. Mis Dr
//                     	case 57:
//                     	    if($pa_param["edata".$item_id] != ""){
//                     	        $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
//                     	    }
//                     	    else{
//                     	        $wk_body = "";
//                     	    }
//                     	    break;
//                     	    //国名リストボックス
//                     	case 114:
//                     	    $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["country"][$pa_param["edata".$item_id]] : "";
//                     	    break;
//                     	default:
//                     	    $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
//                     	    break;
//                     }
//                 }
//             }
//             $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
//         }
//         return $groupBody[$group];
//     }
    
    function __constructMngPaymentCSV($obj) {
        unset($obj->wa_ather_price["-1"]);
    }
    
    /**
     * CSVヘッダ生成
     */
    function payment_payMakeHeader($obj, $all_flg=false){
    
        $header   = array();
        // payment_csvのみ出力
        if(!$all_flg){
            $header[] = "応募者氏名";
            $header[] = "応募者氏名（カナ）";
        }
        $header[] = "取引ID";
        $header[] = "お支払い方法";
        $header[] = "ステータス";
    
        $header[] = "支払期限";
        $header[] = "クレジット番号";
        $header[] = "カード会社";
        $header[] = "カード名義人";
        $header[] = "有効期限";
        $header[] = "クレジット支払い区分";
        $header[] = "クレジット支払い回数";
        //        $header[] = "セキュリティコード";
    
        $header[] = "お振込み人名義";
        $header[] = "お支払銀行名";
        $header[] = "お振込み日";
    
        $header[] = "コンビニ　ショップコード";
        $header[] = "姓";
        $header[] = "名";
        $header[] = "セイ";
        $header[] = "メイ";
        $header[] = "電話番号";
        $header[] = "メールアドレス";
        $header[] = "郵便番号";
        $header[] = "住所";
        $header[] = "成約日";
        $header[] = "決済機関コード";
        //$header[] = "支払詳細";
    
    
        //------------------------------
        //決済項目
        //------------------------------
        foreach($obj->wa_ather_price as $data){
            //            $head = GeneralFnc::smarty_modifier_del_tags($data);
            $head = strip_tags($data);
            $header[] = strip_tags(str_replace(array("_"), " ",$head));
            // 明細表示フラグ
            if($obj->payment_csv_price_disp){
                $header[] = $obj->payment_csv_price_head;
            }
        }
    
        $header[] = "合計金額";
        $header[] = "登録日";
        $header[] = "更新日";
    
        //生成
        $ret_buff  = implode($obj->delimiter, $header);
        $ret_buff .="\n";
    
        return $ret_buff;
    }
    
    
    
    /**
     * CSV出力データ生成
     */
    function payment_payMakeData($obj, $pa_param, $pa_detail, $all_flg=false){
    
        $ret_buff = "";
        $wk_buff = "";
    
        // payment_csvのみ出力
        if(!$all_flg){
            //応募者氏名
            $wk_buff[] =  "\"".$pa_param["edata1"]."　".$pa_param["edata2"]. "\"";
    
            //応募者氏名（カナ）
            $wk_buff[] =  "\"".$pa_param["edata3"]."　".$pa_param["edata4"]. "\"";
        }
    
        //取引ID
        $wk_buff[] = "\"".$pa_param["order_id"]."\"";
    
        //お支払い方法
        $wk_buff[] = ($pa_param["payment_method"] != "") ? "\"".$obj->wa_method[$pa_param["payment_method"]]."\"" : "";
    
        //ステータス
        $wk_buff[] = ($pa_param["payment_status"] != "") ? "\"".$obj->wa_pay_status[$pa_param["payment_status"]]."\"" : "";
    
    
    
        //支払期限
        $wk_buff[] = "\"".$pa_param["limit_date"]."\"";
    
        //クレジット番号
        $wk_buff[] = ($pa_param["c_number"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_number"])."\"" : "";
    
        //カード会社
        $wk_buff[] = ($pa_param["c_company"] != "") ? "\"".$obj->wa_card_type[$pa_param["c_company"]]."\"" : "";
    
        //カード名義人
        $wk_buff[] = "\"".$pa_param["c_holder"]."\"";
    
        //有効期限
        $wk_buff[] = ($pa_param["c_date"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_date"])."\"" : "";
    
    
        //クレジット支払い方法
        $wk_buff[] = ($pa_param["c_method"] != "") ? "\"".$obj->wa_jpo1[$pa_param["c_method"]]."\"" : "";
    
        //クレジット支払い回数
        $wk_buff[] = "\"".$pa_param["c_split"]."\"";
    
        //セキュリティコード
        //        $wk_buff[] = ($pa_param["c_scode"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_scode"])."\"" : "";
    
    
        //お振込み人名義
        $wk_buff[] = "\"".$pa_param["lessee"]."\"";
    
        //お支払銀行名
        $wk_buff[] = "\"".$pa_param["bank"]."\"";
    
    
        //お振込み日
        $wk_buff[] = "\"".$pa_param["closure"]."\"";
    
    
        //コンビニ　ショップコード
        $wk_buff[] = "";
    
        //姓
        $wk_buff[] = "\"".$pa_param["name1"]."\"";
    
        //名
        $wk_buff[] = "\"".$pa_param["name2"]."\"";
    
        //セイ
        $wk_buff[] =  "\"".$pa_param["kana1"]."\"";
    
        //メイ
        $wk_buff[] = "\"".$pa_param["kana2"]."\"";
    
        //電話番号
        $wk_buff[] = "\"".$pa_param["tel"]."\"";
    
        //メールアドレス
        $wk_buff[] = "\"".$pa_param["mail"]."\"";
    
    
        //郵便番号
        $wk_buff[] = "\"".$pa_param["zip"]."\"";
    
        //住所
        $wk_buff[] = "\"".$pa_param["addr"]."\"";
    
        //成約日
        $wk_buff[] = "\"".$pa_param["sold_date"]."\"";
    
        //決済機関コード
        $wk_buff[] = "\"".$pa_param["bank_code"]."\"";
    
    
        //支払詳細
        $wk_detail = "";
    
        if($pa_detail){
            $wk_buff = array_merge($wk_buff, $obj->pay_c($obj, $pa_detail));
        }else{
            foreach($obj->wa_ather_price as $ather_name){
                $wk_buff[] = "";
            }
        }
    
    
    
        //金額
        $wk_buff[] = "\"".$pa_param["price"]."\"";
    
    
    
        //登録日
        $wk_buff[] = ($pa_param["insday"] != "") ? "\"".$pa_param["insday"]."\"" : "";
    
        //更新日
        $wk_buff[] = ($pa_param["upday"] != "") ? "\"".$pa_param["upday"]."\"" : "";
    
    
        $ret_buff = implode($obj->delimiter, $wk_buff);
        $ret_buff .= "\n";
        return $ret_buff;
    }
    
    // 金額、その他決済項目のCSV整形
    function payment_pay_c($obj, $pa_detail){

        $isSet = array();
        foreach($obj->wa_ather_price as $key => $ather_name){
            $quantity = "";
            $match_flg = "";
            // 金額表示フラグ
            $price_flg = "";
            $price     = "";

            foreach($pa_detail as $_key => $data){
                // バッファ済みのキーは判定しない
                if(in_array($_key, $isSet)) continue;
             

                // 金額
                if($data['type'] == 0){
//                     $from = "payment";
//                     $column = "csv_price_head0, csv_ather_price_head0";
//                     $where = "eid = ".$obj->db->quote($data['eid'])."";
//                     $csv_header =  $obj->db->getData($column, $from, $where, __FILE__, __LINE__);

//                     $pos = strpos($data["name"], "事前登録料金");
//                     if($pos !== false){
//                         $quantity = $data["name"];

//                         // CSV用に整形--------------------------------------------------------------
//                         if($csv_header['csv_price_head0'] != ""){
//                             $quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
//                         }
//                         $match_flg = "1";

//                         $price_flg = "1";
//                         $price = $data['price'];
//                         break 1;
//                     }

                // その他決済
                }elseif($key== $data["key"]){
                    $quantity = $data["quantity"];
                    $match_flg = "1";

                    $price_flg = "1";
                    $price = $data['price'];
                    break 1;
                }
            }


            if($match_flg == "1"){
                $wk_buff[] = "\"".GeneralFnc::smarty_modifier_del_tags($quantity)."\"";
                // 明細表示フラグ
                if($obj->payment_csv_price_disp && $price_flg == "1"){
                    $wk_buff[] = "\"".$price."\"";
                }

                // バッファ済みのキーをセット
                $isSet[] = $_key;
                $isSet[] = $_key."t";

            }else{
                $wk_buff[] = "\"\"";
                // 明細表示フラグ
                if($obj->payment_csv_price_disp){
                    $wk_buff[] = "\"\"";
                }
            }
        }

        return $wk_buff;
    }
}
