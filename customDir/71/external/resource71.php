<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 * 
 * @author sovic kato
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource71 implements externalResource
{
	
	/**
	 * 項目の並び替えを行う
	 *
	 * @param  $obj ページオブジェクト
	 * @return void
	 * */
	public function sortFormIni($obj){
		//---------------------
		// グループ1 並び替え
		//---------------------
		$arrGroup1 =& $obj->arrItemData[1];
		// 入れ替え
		$array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 1:
                    $array[10] = $arrGroup1[10];
                    $array[11] = $arrGroup1[11];
                    $array[17] = $arrGroup1[17];
                    $array[19] = $arrGroup1[19];
                    $array[21] = $arrGroup1[21];
                    $array[24] = $arrGroup1[24];
                    $array[73] = $arrGroup1[73];
                    $array[74] = $arrGroup1[74];
                    $array[75] = $arrGroup1[75];
                    $array[1] = $arrGroup1[1];
                    $array[2] = $arrGroup1[2];
                    $array[3] = $arrGroup1[3];
                    $array[26] = $arrGroup1[26];
                    $array[27] = $arrGroup1[27];
                    $array[25] = $arrGroup1[25];
                    $array[28] = $arrGroup1[28];
                    $array[63] = $arrGroup1[63];
                    $array[64] = $arrGroup1[64];
                    $array[69] = $arrGroup1[69];
                    $array[70] = $arrGroup1[70];
                    $array[71] = $arrGroup1[71];
                    $array[72] = $arrGroup1[72];
                    break;
                
                case 10:
                case 11:
                case 17:
                case 19:
                case 21:
                case 24:
                case 73:
                case 1:
                case 2:
                case 3:
                case 26:
                case 27:
                case 25:
                case 28:
                case 63:
                case 64:
                case 69:
                case 70:
                case 71:
                case 72:
                    break;
                
                case 74:
                    break;
                case 75:
                    break;
                
                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
        
	}

}