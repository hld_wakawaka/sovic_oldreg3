<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."application/model/package.model.php");

class Mng_entry72 extends AbstracePackage
{

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_detail_premain($obj){
        $obj->assign("arrMemberShipNoMap", $obj->arrMemberShipNoMap);
        $this->filter_section($obj);
        parent::mng_detail_premain($obj);
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /* MngCSVコンストラクタ */
    function __constructMngCSV($obj){
    }

}
