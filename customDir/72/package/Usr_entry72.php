<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */
require_once (ROOT_DIR . "customDir/72/package/Mng_entry72.php");

class Usr_entry72 extends Mng_entry72
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // society と membership noのitem_idの対応配列
        $obj->arrMemberShipNoMap = array(1=>27, 2=>28, 3=>63);

        // パッケージ機能のロード
        $this->onloadPackage();
    }


    /**
     * 入力/選択しているかチェック
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return boolean true:入力状態, false:未入力
     *
     */
    function isNull($arrParam, $group_id, $item_id, $arrItemData){
        // チェックボックス
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            // 入力数をカウントするので反転
            return !(bool)$this->countChecked($arrParam, $group_id, $item_id, $arrItemData);
        }
        
        // その他
        // 標準関数の戻り値のbool値が逆なので反転
        return !Validate::isNull($arrParam["edata".$item_id]);
    }


    /**
     * チェックボックスの選択数を返す
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return integer $checked     チェックボックスの選択数
     *                              チェックボックスでない場合は0
     */
    function countChecked($arrParam, $group_id, $item_id, $arrItemData){
        $checked =0;
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            $options = $arrItemData['select'];
            foreach($options as $key => $val){
                $wk_val = isset($arrParam["edata".$item_id.$key]) ? $arrParam["edata".$item_id.$key]: "";
                if($wk_val != "") $checked++;
            }
        }
    
        return $checked;
    }



    /* デバッグ用 */
    function developfunc($obj)
    {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        $this->filter_section($obj);
        
        $obj->assign("arrMemberShipNoMap", $obj->arrMemberShipNoMap);
    }


    // エラーチェック # ブロック1
    function _check1($obj)
    {
//        $obj->admin_flg = "";

        $group_id = 1;
        
        // Fee
        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;
        // ★5:選択状態
        $key = 'ather_price0';
        $ather_price1 = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;
        // ★6:選択状態
        $key = 'ather_price1';
        $ather_price2 = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;
        
        //-----------------------------
        // No.1,11は相互チェックあり(→:左を入力したら右を必須)
        // ★1 → A → MembershipNo
        // ★2 → A → MembershipNo
        //-----------------------------
        // No.1 # ★1[IEE Member]or★2[Life/Student/Retired]の場合はA[Name of society of which...]を必須
        if(in_array($amount, array(0, 1))) Usr_init::addCheck($obj, array(26), 0);
        
        // No.2 # PaperReferenceNo.1の入力時は★2,★2-2[Student]を選択するとエラー
        $item_id = 64;
        $key = "edata" . $item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0)
        {
            if(in_array($amount, array(1, 2))) $obj->objErr->addErr('If you are author who registering a paper, please select "IEICE, IEEE or IPSJ Member" or "Non-member" for Fee.', $key);
        }
        
        //-----------------------------
        // No.4,5,6,7,8は相互チェックあり
        // ★4 ←→ ★5 → ★1
        // ★4 ←→ ★6 → ★3
        //-----------------------------
        // No.4 # ★4[PaperReferenceNo3]の入力時は★5[Additional Paper:IEEE Member],★6[Additional Paper:Non IEEE Member]のいずれか選択必須
        $item_id = 70;
        $key = "edata" . $item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0)
        {
            // ★5または★6の選択必須
            if(($ather_price1 + $ather_price2) == 0) $obj->objErr->addErr("Select [Additional Paper] for Fee.", $key);
        }
        
        // No.6 # ★5選択時に★4を必須
        $obj->itemData[70]['strip_tags'] = '"'.$obj->itemData[70]['strip_tags'].'"';
        if($ather_price1 > 0) Usr_init::addCheck($obj, array(70), 0);
        
        // No.8 # ★5選択時に★4を必須
        if($ather_price2 > 0) Usr_init::addCheck($obj, array(70), 0);
        
        // No.11 # ★1または★2のいずれかを選択かつAを入力した場合にAに対応したMemberShipNoを必須にする
        $item_id = 26;
        $arrItemData = $obj->arrItemData[$group_id][$item_id];
        if(in_array($amount, array(0, 1)) && !$this->isNull($obj->arrParam, $group_id, $item_id, $arrItemData))
        {
            $options = $arrItemData['select'];
            foreach($options as $key => $label){
                if(!isset($obj->arrParam["edata".$item_id.$key])) continue;
                if(strlen($obj->arrParam["edata".$item_id.$key]) == 0) continue;
                
                $val = $obj->arrParam["edata".$item_id.$key];
                Usr_init::addCheck($obj, array($obj->arrMemberShipNoMap[$val]), 0);
            }
            
        }
        
        
        // 標準エラーチェック
        Usr_Check::_check1($obj);
        
        // {{ 以下エラーメッセージの並び替えが効かなく末尾に追加するメッセージ
        
        // No.5 # ★5選択時に★1必須
        if($ather_price1 > 0 && $amount != 0) $obj->objErr->addErr("Category for registration and that for additional paper fee should be the same.", 'ather_price1');
        
        // No.7 # ★6選択時に★3必須
        if($ather_price2 > 0 && $amount != 3) $obj->objErr->addErr("Category for registration and that for additional paper fee should be the same.", 'ather_price2');
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    // 帯名「[お申込者様]」の表示
    function mailfunc26($obj, $item_id, $name, $i = null)
    {
        $group = 1;
        $key = "edata" . $item_id . $i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        
        // @see Usr_Assign::nini
        $select = $obj->arrItemData[$group][$item_id]["select"];
        $value  = '';
        foreach($select as $n=>$val) {
            if(!isset($obj->arrForm[$key.$n])) continue;
            if($obj->arrForm[$key.$n] == $n) {
                $value .= "\n ".trim($select[$n]);
                // MembershipNo
                $_item_id = $obj->arrMemberShipNoMap[$n];
                $_key     = "edata".$_item_id;
                $value .= " Membership No.: ".Usr_Assign::nini($obj, $group, $_item_id, $obj->arrForm[$_key]);
            }
        }

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    /**
     * Config.filter # Usr_mail_makeMailBody
     *
     * @param object $obj
     * @return void
     */
    function filter_section($obj)
    {
        foreach($obj->arrMemberShipNoMap as $_key => $item_id)
        {
            $obj->arrItemData[1][$item_id]['disp'] = 1;
        }
        
        if(method_exists($obj, "assign"))
        {
            $obj->assign("arrItemData", $obj->arrItemData);
        }
    }

}
