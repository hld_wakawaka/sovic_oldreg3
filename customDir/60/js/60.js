
$(document).ready(function(){
	var amount = $(".amount");
    if(amount.size() > 0){
        amount.bind("change", function(){
            var chk = $(".amount").filter(":checked").val(); // input:radio
            if(typeof chk == 'undefined'){
            	$(".method").prop("disabled",false);
            	return;
            }
            if(chk == 2 || chk == 3) {
            	$(".method").prop("disabled",true);
            	return;
            }
            $(".method").prop("disabled",false);
        }).change();
    } 
    
    var edata63 = $(".edata63");
    edata63.bind("change", function(){
    	var chk = $(".edata63").val();	
    	$("#amount_2").prop("disabled",chk.length > 0 ? false : true);
    	$("#amount_3").prop("disabled",chk.length > 0 ? false : true);	
    }).change();
});