
$(function(){
    $("div.bname:eq(0)").css("display", "none")

    var lang2 = [];
    var lang1 = [28,63,64,69,70,71,72,73,74,75,76,77,78,79,80,141];

    var edata27 = $(".edata27");
    if(edata27.size() > 0){
        edata27.bind("change", function(){
            var chk = $(this).filter(":checked").val(); // input:radio
            if(typeof chk == 'undefined'){ return; }

            init_form();
            if(chk == 1) {init_lang1();return;}
            if(chk == 2) {init_lang2();return;}
        }).change();
    }

    function init_form(){
        for(i in lang1){
            var key = "edata"+lang1[i];
            $("."+key).css("background-color", "");
//            $("."+key).prop("disabled", false).css("background-color", "");
        }
        for(i in lang2){
            var key = "edata"+lang2[i];
            $("."+key).css("background-color", "");
//            $("."+key).prop("disabled", false).css("background-color", "");
        }
    }


    /**
     * 日英の出展
     *     英を非活性
     * */
    function init_lang1(){
        for(i in lang2){
            var key = "edata"+lang2[i];
            $("."+key).css("background-color", "#eee");
//            $("."+key).prop("disabled", true).css("background-color", "#eee");
        }
    }

    /**
     * 英の出展
     *     日英を非活性
     * */
    function init_lang2(){
        for(i in lang1){
            var key = "edata"+lang1[i];
            $("."+key).css("background-color", "#eee");
//            $("."+key).prop("disabled", true).css("background-color", "#eee");
        }
    }

    var arrurl = ['edata69', 'edata83'];
    for(i in arrurl){
	    if($("."+arrurl[i]).val().length == 0){
	        $("."+arrurl[i]).val("http://");
	    }
    }
});
