var Common = Common || {};

Common.init = function(){
    this.pageTop();
    this.windowClose();
    this.accordionJp();
    this.accordionEn();
    this.toScroll();
    this.itemCheck();
};

Common.pageTop = function(){
    var $target = $('.back');
    $target.on('click',function(){
        $('body,html').stop(false,true).animate({'scrollTop':0},300);
        return false;
    });
};

Common.toScroll = function() {
    $('a[href^=#]').click(function() {

      var speed = 400;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
};
Common.accordionJp = function(){
    if( $(".ja_search").length !== 0 ) {
        var origin = $(".ja_search .show-btn").find('img').attr('src');
        var open = origin.replace('.', '_off.');
 
        $(".ja_search .show-btn").click(function(){
            if( $(this).find('img').attr('src') === origin ) {
                $(this).find('img').attr('src', open);
            }else {
                $(this).find('img').attr('src', origin);
            }
            $(".ja_search .hide-content").slideToggle();
            return false;
        });
        $(window).on('load resize',function(){
            var win = $(window).width();
            var p = 767;
            if(win > p){
                $(".ja_search .hide-content").show();
                $(".ja_search .show-btn").hide();
                $(".ja_search .show-btn").find('img').attr('src',origin);
            } else {
                $(".ja_search .hide-content").hide();
                $(".ja_search .show-btn").show();
                $(".ja_search .show-btn").find('img').attr('src',origin);
        }
        });
    }
};

Common.accordionEn = function(){
    if( $(".en_search").length !== 0 ) {
        var origin = $(".en_search .show-btn").find('img').attr('src');
        var open = origin.replace('.', '_off.');

        $(".en_search .show-btn").click(function(){
            if( $(this).find('img').attr('src') === origin ) {
                $(this).find('img').attr('src', open);
            }else {
                $(this).find('img').attr('src', origin);
            }
            $(".en_search .hide-content").slideToggle();
            return false;
        });
        $(window).on('load resize',function(){
            var win = $(window).width();
            var p = 767;
            if(win > p){
                $(".en_search .hide-content").show();
                $(".en_search .show-btn").hide();
                $(".en_search .show-btn").find('img').attr('src',origin);
            } else {
                $(".en_search .hide-content").hide();
                $(".en_search .show-btn").show();
                $(".en_search .show-btn").find('img').attr('src',origin);
            }
        });
    }
};

Common.itemCheck = function(){
    if( $('.item_list').size() ){
        if( $('.item_list').find('li').size() === 0 ){
            var url, p1, p2, p3, param=[];
            url = location.href;
            p1 = url.split("?");
            if(p1[1]) {
                p2   = p1[1].split("&");
                for ( i = 0; i < p2.length; i++ ) {
                    p3 = p2[i].split("=");
                    param.push(p3[0]);
                    param[p3[0]] = p3[1];
                }
                if( param['lang'] ) {
                    if( param['lang'] == 2 ) {
                        $('.item_list').replaceWith('<p class="item_list">There is no item.</p>');
                    } else {
                        $('.item_list').replaceWith('<p class="item_list">出展品目はありません。</p>');
                    }
                } else {
                    $('.item_list').replaceWith('<p class="item_list">出展品目はありません。</p>');
                }
            }
        }
    }
};

Common.windowClose = function () {
    $('.close').click(function () { window.close(); return false; });
};

$(function(){
    Common.init();
});

