$(function(){
    /* Activity列 上の行の入力をコピー */
    $("input:button.f16col_1").bind('click', function(){
        var index = parseInt($(this).attr('id').substring(10));
        var arr = [99];
        copyFormVal(index, arr);
    });

    /* Hotel列 上の行の入力をコピー */
    $("input:button.f16col_2").bind('click', function(){
        var index = parseInt($(this).attr('id').substring(10));
        var arr = [100, 101, 102, 103];

        // 3日目以降
        if(index > 0){
            return copyFormVal(index, arr);
        }

        $('#edata100_'+index).val($('#edata54').val()); // 100 -> 54
        $('#edata101_'+index).val($('#edata55').val()); // 101 -> 55
        $('#edata102_'+index).val($('#edata56').val()); // 102 -> 56
        $('#edata103_'+index).val($('#edata67').val()); // 103 -> 67
    });

    function copyFormVal(index, arr){
        for(k in arr){
            var item_id = arr[k];
            var fromId    = 'edata'+item_id+'_'+(index-1);
            var ToId      = 'edata'+item_id+'_'+index;
            $('#'+ToId).val($('#'+fromId).val());
        }
    }

});