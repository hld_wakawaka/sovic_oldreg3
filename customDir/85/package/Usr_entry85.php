<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */
require_once (ROOT_DIR . "customDir/85/package/Mng_entry85.php");

class Usr_entry85 extends Mng_entry85
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // パッケージ機能のロード
        $this->onloadPackage();
        
        // No2. # お支払い方法の[銀行振込]を削除 / [Other]を追加
        unset($GLOBALS["method_E"]["2"]);
        $GLOBALS["method_E"]["99"] = "Others";
        $GLOBALS["method_J"]["99"] = "Others";
        
    }

    
    function usr_login_construct($obj){
        require_once(ROOT_DIR."application/cnf/en.php");
        
        $GLOBALS["msg"]["login_id"] = "Registration ID";
        $GLOBALS["msg"]["msg_login"] = "Enter your Registration ID to proceed abstract submission. e.g.) icmrbs-reg-12345<br>
Please complete your registration <a href='http://www.icmrbs2016.org/registration.html'>here</a> before submitting your abstract.";
        $GLOBALS["msg"]["err_not_loginid"] = "Incorrect login ID";
        

        $obj->_processTemplate = "Usr/include/85/Usr_login.html";
    }
    
    
    function usr_login__check($obj) {
        $objErr = New Validate;
        
        while(true){
            // ログインID未入力チェック
            if(!$objErr->isNull($obj->arrForm["login_id"])) {
                $objErr->addErr($GLOBALS["msg"]["err_not_loginid"]);
                break;
            }
            
            // ログインID半角英数チェック
            if(!$objErr->isAlphaNumericMark($obj->arrForm["login_id"])) {
                 $objErr->addErr(sprintf($GLOBALS["msg"]["err_han_alphanumericmark"], $GLOBALS["msg"]["login_id"]));
                 break;
            }
            
            // ログインIDの妥当性チェック
            $isValidLoginId = $this->isValidLoginId($obj->arrForm["login_id"]);
            if(!$isValidLoginId) {
                $objErr->addErr($GLOBALS["msg"]["err_not_loginid"]);
                break;
            // ここに書くのは非常によろしくないが…
            }else{
                //echo "入力されたログインIDは登録済みなのでリダイレクトします。<br/>";
                //echo "リダイレクト先：未定";
                header("location: https://www.e-hots.jp/ics/ICEMS2016/");
                exit;
            }
            break;
        }
        
        return $objErr->_err;
    }
    
    
    /**
     * ログインIDの妥当性チェック
     * @access	public
     * @param	string	$login_id
     * @return	boolean	ログインIDの妥当性
     */
    function isValidLoginId($login_id)
    {
        // ログインユーザを取得
        $entryData = LoginBase::getLoginObjectUsr(85, $login_id);
        if (!$entryData) {
            // ログインIDの妥当性NG
            return false;
        }
        
        // ログインIDの妥当性OK
        return true;
    }


    /** 1ページ目 */
    function pageAction1($obj)
    {
        Usr_pageAction::pageAction1($obj);
        
        // No.3 # お支払い方法が[Others]の場合は[お支払情報ページ]をスキップ
        while(true){
            // 決済フォームを利用 かつ 決済金額が0円より多い場合
            if($obj->formdata["credit"] != "0" && $obj->total_price > 0){
                // お支払い方法が[Others]の場合は次のページへ
                if($obj->arrParam["method"] == "99"){
                    // 3ページ目を使用
                    if($obj->formdata["group3_use"] != "1"){
                        $obj->block = "3";
                        $obj->_processTemplate = "Usr/form/Usr_entry.html";
                        break;
                    }
                    // 確認画面へ
                    $obj->block = "4";
                    $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
                    break;
                }
            }
            break;
        }
    }




    /** 戻るボタン */
    function backAction($obj) {
        Usr_pageAction::backAction($obj);
        
        // No.3 # お支払い方法が[Others]の場合は[お支払情報ページ]をスキップ
        switch($obj->wk_block) {
            // 3ページ目からの戻り
            case "3";
                // お支払い方法が[Others]の場合は次のページへ
                if($obj->arrForm["method"] == "99"){
                    $obj->block = "1";
                    $obj->_processTemplate = "Usr/form/Usr_entry.html";
                }
                break;
        }
    }



    // エラーチェック # ブロック1
    function _check1($obj)
    {
        $group_id = 1;
        Usr_Check::_check1($obj);
        
        // Fee
        $method = strlen($obj->arrParam ['method']) > 0 ? intval($obj->arrParam['method']) : - 1;
        
        // No.1 # グループ1 > Paymento Code
        if($method == "99"){
            $item_id = 26;
            $key = "edata" . $item_id;
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $msg = 'To apply for “Others” for your payment method, please fill in the payment code.';
                $obj->objErr->addErr($msg, $key);
            }
        }
        
        // 同伴者のチェック
        if($obj->arrParam['edata28'] !== "" && $obj->arrParam['ather_price0'] !== "1"){
            $item_id = 28;
            $msg = 'Please select "Accompanying Person" for fee.';
            $obj->objErr->addErr($msg, $key);
        }elseif($obj->arrParam['edata28'] === "" && $obj->arrParam['ather_price0'] !== ""){
            $item_id = 28;
            $msg = 'Please fill in the information of Accompanying Person.';
            $obj->objErr->addErr($msg, $key);
        }
        
    }


    // メール
    function makePaymentBody($obj, $exec_type)
    {
        // その他は帯を非表示
        if($obj->arrForm["method"] == "99"){
            // 決済なし
            if($obj->formdata["kessai_flg"] != "1") return "";
    
            //支払合計
            if($exec_type == "1"){
                $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
            }else{
                $total = $GLOBALS["session"]->getVar("ss_total_payment");
            }
            $obj->point_mark = "*";
            
            $body_pay = "\n\n[Payment Information]\n\n";
            // お支払確認ページの表示/非表示 : デフォルトは表示
            if(!in_array($obj->form_id, $obj->payment_not)){
                $body_pay .= ($obj->arrForm["method"] != "3") 
                            ? $obj->point_mark."Payment:".$obj->wa_method[$obj->arrForm["method"]]."\n"
                            : $obj->point_mark."Payment:Cash on-site\n";
            }
            $body_pay .= $obj->point_mark."Amount of Payment:\n";
        }
        
        return Usr_mail::makePaymentBody($obj, $exec_type);
    }


    /* デバッグ用 */
    function developfunc($obj)
    {
/*
         // メールデバッグ
         $obj->ins_eid = 1615;
         print "--------------------<pre style='text-align:left;'>";
         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
         print "</pre><br/><br/>";
*/
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    /** 完了ページ */
    function completeAction($obj) {
        // 二重登録チェック
        $rl = new Reload();
        if($rl->isReload()) {
            $msg = $GLOBALS["msg"]["reload"];
            $obj->complete($msg);
            return;
        }

        // セッションパラメータ取得
        $obj->arrForm = array_merge((array)$GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
        if(empty($obj->arrForm)){
            Error::showErrorPage("*セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
        }

        $obj->arrForm = array_merge($obj->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));
        if(empty($obj->arrForm)){
            Error::showErrorPage("※セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
        }

        $obj->arrForm = array_merge($obj->arrForm, (array)$GLOBALS["session"]->getVar("form_parampay"));
        if(empty($obj->arrForm)){
            Error::showErrorPage("セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
        }

        $obj->u_date = date('YmdHis');


        //トランザクション
        $obj->db->begin();

        // 二重登録チェック
        if(!Usr_Check::checkRepeat($obj, $obj->arrForm)){
            $msg = ($obj->formdata["lang"] == LANG_JPN)
                 ? "既にご登録済みです。"
                 :"You are already registered.";
            $obj->arrErr["edata25"] = $msg;
            $obj->_processTemplate = "Usr/form/Usr_entry.html";
            return;
        }

        //------------------------------------
        //新規登録の場合
        //------------------------------------
        if($obj->eid == ""){

            $exec_type = "1";

            // ロック
            $obj->lockStart();

            // 合計金額#参加登録で決済利用
            if($obj->formdata["kessai_flg"] == "1"){
                $obj->total_price = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
            }

            //---------------------------------
            // 支払方法別処理
            //---------------------------------
            $method = $obj->arrForm["method"] == "99" ? 2 : $obj->arrForm["method"];
            $actionName = "payAction".$method;
            $obj->$actionName();

            //メール送信
            $obj->sendRegistMail($obj->user_id, $obj->passwd);

            //完了メッセージ
            $obj->msg = $obj->replaceMsg($obj->formWord["word1"]);

            $obj->lockEnd();


        //------------------------------------
        // 更新の場合
        //------------------------------------
        }else{
            $exec_type = "2";

            //ファイルアップロード
            if(!$obj->TempUploadAction()){
                $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
                $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
                $obj->db->rollback();
                $obj->lockEnd();
                $obj->complete($msg);
                return;
            }

            //応募者のログイン情報をセッションから復元
            $ss_user = $GLOBALS["session"]->getVar("SS_USER");
            $user_id = $ss_user["e_user_id"];
            $passwd  = $ss_user["e_user_passwd"];

            // 更新処理
            $obj->updateEntry();

            // ファイルの移動
            if(!$obj->MoveFileAction()) {
                $obj->db->rollback();
                $obj->complete("ファイルの移動に失敗しました。");
                return;
            }

            //メール送信
            $obj->sendEditMail($user_id, $passwd);

            //完了メッセージ
            $obj->msg = $obj->replaceMsg($obj->formWord["word2"]);

            // ログインセッションを破棄
            LoginEntry::doLogout();
        }

        // コミット
        $obj->db->commit();

        $obj->showComplete();
        exit;
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------


}
