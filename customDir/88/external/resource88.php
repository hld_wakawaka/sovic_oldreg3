<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 * 
 * @author hunglead doishun
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource88 implements externalResource
{


//---------------------------
// サンプルプログラム
//---------------------------
    /**
     * 項目の並び替えを行う
     *
     * @param  $obj ページオブジェクト
     * @return void
     * */
    public function sortFormIni($obj){
        //---------------------
        // グループ1 並び替え
        //---------------------
        $arrGroup1 =& $obj->arrItemData[1];
    
        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 4:
                    $array[$key] = $data;
                    $array[26]  = $arrGroup1[26];
                    break;
                case 9:
                    $array[27]  = $arrGroup1[27];
                    $array[$key] = $data;
                    break;
                case 21:
                    break;
                case 26:
                    break;
                case 27:
                    break;
                case 28:
                    $array[21]  = $arrGroup1[21];
                    $array[$key] = $data;
                    break;
    
                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



}