<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */
require_once (ROOT_DIR . "customDir/88/package/Mng_entry88.php");

class Usr_entry88 extends Mng_entry88
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // パッケージ機能のロード
        $this->onloadPackage();
    }


    /**
     * 入力/選択しているかチェック
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return boolean true:入力状態, false:未入力
     *
     */
    function isNull($arrParam, $group_id, $item_id, $arrItemData){
        // チェックボックス
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            // 入力数をカウントするので反転
            return !(bool)$this->countChecked($arrParam, $group_id, $item_id, $arrItemData);
        }
        
        // その他
        // 標準関数の戻り値のbool値が逆なので反転
        return !Validate::isNull($arrParam["edata".$item_id]);
    }


    /**
     * チェックボックスの選択数を返す
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return integer $checked     チェックボックスの選択数
     *                              チェックボックスでない場合は0
     */
    function countChecked($arrParam, $group_id, $item_id, $arrItemData){
        $checked =0;
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            $options = $arrItemData['select'];
            foreach($options as $key => $val){
                $wk_val = isset($arrParam["edata".$item_id.$key]) ? $arrParam["edata".$item_id.$key]: "";
                if($wk_val != "") $checked++;
            }
        }
    
        return $checked;
    }



    /* デバッグ用 */
    function developfunc($obj)
    {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }
    
    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        //管理者モードの場合でも行う処理
        if($obj->itemData[29]["disp"] != "1"){
            if(isset($obj->arrParam["edata29"]) && $obj->arrParam["edata29"] == "2"){
                $obj->arrParam["edata30"] = "";
                $obj->arrParam["edata31"] = "";
            }
        }
    
        // 管理者はここまで
        if($obj->admin_flg == "1") return;
    
    
        //氏名、メールアドレス一致チェック
        if(!$obj->checkRepeat($obj->arrParam)){
            $msg = ($obj->formdata["lang"] == LANG_JPN)
            ? "既にご登録済みです。"
                :"You are already registered.";
            $obj->objErr->addErr($msg, "edata25");
        }
    
        // 基本チェック
        $obj->objErr->check($obj->_init(1), $obj->arrParam);
    
        //同意書
        if($obj->formdata["agree"] == "1"){
            if($obj->arrParam["agree"] == "" ){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                ? "同意文をご覧になり、同意の上、お進みください"
                    : "Please read the agreement and proceed further only if you agree.";
                $obj->objErr->addErr($msg, "agree");
            }
        }
    
        //メールアドレス一致チェック
        if($obj->arrParam["edata25"] != "" && $obj->itemData[25]["mail"] == "1"){
            if($obj->arrParam["edata25"] != $_REQUEST["chkemail"]){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                ? $obj->itemData[25]['strip_tags']."と".$obj->itemData[25]['strip_tags']."（確認用）の入力内容が一致していません。"
                    : $obj->itemData[25]['strip_tags']. " and ".$obj->itemData[25]['strip_tags']."（confirm）  do not match.";
                $obj->objErr->addErr($msg, "edata25");
            }
        }
    
        // 共著者チェック
        //        $obj->_checkAuthor();
    
        //任意項目
        $obj->_checkNini(1);
        
        //
        
        if($obj->arrParam["edata631"] == "" && $obj->arrParam["edata641"] == ""
            && $obj->arrParam["edata691"] == "" && $obj->arrParam["edata701"] == ""
            && $obj->arrParam["edata711"] == "" && $obj->arrParam["edata721"] == ""
            && $obj->arrParam["edata731"] == "" && $obj->arrParam["edata741"] == ""
            && $obj->arrParam["edata751"] == "" && $obj->arrParam["edata761"] == ""
            && $obj->arrParam["edata771"] == "" && $obj->arrParam["edata781"] == ""){
            //未選択の場合
            $msg = sprintf($GLOBALS["msg"]["err_require_select"], "参加会場と時間");
            $obj->objErr->addErr($msg, "edata63");
            
        }
        
        
        
        //英語フォームの場合、国のチェック
        if($obj->formdata["lang"] == LANG_ENG){
            //国名プルダウンが必須の場合
            if($obj->itemData[114]["disp"] != "1"){
                if($obj->itemData[114]["need"] == "1" && $obj->arrParam["edata114"] == "239"){
                    if($obj->arrParam["edata60"] == ""){
                        $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[60]['strip_tags']);
                        $obj->objErr->addErr($msg, "edata60");
                    }
                }
    
            }else{
                if($obj->itemData[60]["disp"] != "1" && $obj->itemData[60]["need"] == "1"){
                    if($obj->arrParam["edata60"] == ""){
                        $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[60]['strip_tags']);
                        $obj->objErr->addErr($msg, "edata60");
                    }
                }
            }
        }
    
    
    
        //------------------------------
        //支払金額
        //------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            // 編集時はチェックしない
            if($obj->eid != "") return;
    
            // 合計金額が0円の場合はお支払い方法を入力しなくてもOK
            $total_price = Usr_function::_setTotal($obj->wa_price, $obj->arrParam, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
            if($total_price == 0){
                unset($obj->objErr->_err["method"]);
            }
    
            if($obj->arrParam["amount"] == ""){
                if($obj->formdata["lang"] == LANG_JPN){
                    $obj->objErr->addErr("お支払い金額を選択してください。", "amount");
                }
                else{
                    $obj->objErr->addErr("Select one(s) for Fee.", "amount");
                }
            }
    
            //-------------------------------
            //その他決済を利用する場合
            //-------------------------------
            if($obj->formdata["kessai_flg"] == "1" && $obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){
                foreach($obj->wa_ather_price as $pkey => $data){
                    if($data["limit"] != "0"){
                        if($obj->arrParam["ather_price".$pkey] > $data["limit"] ){
                            if($obj->formdata["lang"] == LANG_JPN){
                                $obj->objErr->addErr($data["name"]."は".$data["limit"].$data["tani"]."以内で入力してください。", "ather_price".$pkey);
                            }
                            else{
                                $obj->objErr->addErr("The maximum allowed number of ".$data["name"]." is ".$data["limit"].".", "ather_price".$pkey);
                            }
                        }
                    }
                }
                // その他決済項目の必須設定をON @ Config.add_error_exist_otherprice
                if($obj->exist_otherprice){
                    $values = array();
                    foreach($obj->wa_ather_price as $pkey => $data){
                        $values[$pkey] = $obj->arrParam["ather_price".$pkey];
                    }
                    // 未選択
                    if(array_sum($values) == 0){
                        $otherPriceName = strlen($obj->exist_otherprice_title) > 0 ? $obj->exist_otherprice_title : "";
                        if(strlen($otherPriceName) == 0){
                            $otherPriceName = $obj->formdata["lang"] == LANG_JPN ? "お支払い金額" : "Fee";
                        }
                        $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_require_select"], $otherPriceName), "exist_otherprice");
                    }
                }
            }
        }
        return;
    }


}
