$(document).ready(function(){

    // No.3
    var arrItem = [64,69,70,71,72,73];
    $(".edata63").bind("change", function(){
        var chk = $(".edata63").filter(":checked").val();
        for(i in arrItem){
            var element = ".edata"+arrItem[i];
            $(element).prop("disabled", false);
        }

        if(chk == 2){
            for(i in arrItem){
                var element = ".edata"+arrItem[i];
                $(element).prop("disabled", true);
            }
        }
    }).change();


    // No. 4
    var arrOther = ['p1_0', 'p1_1'];
    $(".edata27").bind("change", function(){
        var chk = $(".edata27").filter(":checked").val();
        for(i in arrOther){
            var element = "select#"+arrOther[i];
            $(element).prop("disabled", false);
        }

        if(chk == 3){
            for(i in arrOther){
                var element = "select#"+arrOther[i];
                $(element).prop("disabled", true);
            }
        }
    }).change();
    
    
    
    // No. 12
    var arrItem = [69,70,71];
    $("#edata64_1").bind("change", function(){
        var chk = $("#edata64_1").filter(":checked").val();
        for(i in arrItem){
            var element = ".edata"+arrItem[i];
            $(element).prop("disabled", true);
        }

        if(chk == 1){
            for(i in arrItem){
                var element = ".edata"+arrItem[i];
                $(element).prop("disabled", false);
            }
        }
    }).change();
    
    
    var arrItem2 = 72;
    $("#edata64_2").bind("change", function(){
        var chk = $("#edata64_2").filter(":checked").val();
        var element = ".edata"+arrItem2;
        $(element).prop("disabled", true);

        if(chk == 2){
            var element = ".edata"+arrItem2;
            $(element).prop("disabled", false);
        }
    }).change();
    
    
    var arrItem3 = 73;
    $("#edata64_3").bind("change", function(){
        var chk = $("#edata64_3").filter(":checked").val();
        var element = ".edata"+arrItem3;
        $(element).prop("disabled", true);

        if(chk == 3){
            var element = ".edata"+arrItem3;
            $(element).prop("disabled", false);
        }
    }).change();

});

