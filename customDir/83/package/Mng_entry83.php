<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."application/model/package.model.php");

class Mng_Entry83 extends AbstracePackage
{

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    public $arrOperation = array(
          "101" => "当選"
        , "102" => "落選"
        , "103" => "招待"
    );
    
    
    function __constructMng($obj)
    {
        // No.3 # 当選ステータス
         if (isset($obj->_smarty)) {
            $obj->assign("arrOperation", $this->arrOperation);
        }
        // No.3 # ステータスマスタに当選ステータスを追加
        $GLOBALS["entryStatusList"]   = $GLOBALS["entryStatusList"]   + $this->arrOperation;
        $GLOBALS["paymentstatusList"] = $GLOBALS["paymentstatusList"] + $this->arrOperation;

        // No.3 # 検索条件として保持パラメータに追加
        foreach($this->arrOperation as $_key => $_label){
            $obj->searchkey[] = "status".$_key;
        }
    }


    /**
     * No.3 # 検索条件に追加
     * 
     */
    function Mng_buildWhere_status_filter($obj, $condition, $subwhere)
    {
        foreach($this->arrOperation as $_key => $_label){
            if($condition["status".$_key] == $_key) $subwhere[] = "v_entry.status = ".$_key;
        }
        
        return $subwhere;
    }


    function mng_index_premain($obj)
    {
        // パッケージ機能のロード
        parent::mng_index_premain($obj);
    }


    function mng_detail_premain($obj)
    {
        // パッケージ機能のロード
        parent::mng_detail_premain($obj);
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

}
