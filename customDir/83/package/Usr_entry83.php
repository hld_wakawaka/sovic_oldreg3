<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */
require_once (ROOT_DIR . "customDir/83/package/Mng_entry83.php");

class Usr_entry83 extends Mng_entry83
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // パッケージ機能のロード
        $this->onloadPackage();
    }


    /**
     * 同意画面
     *   No.5 # 初期状態の場合にチェックボックスをON
     * 
     */
    function agreeAction($obj)
    {
        Usr_pageAction::agreeAction($obj);
        
        // 新規登録
        if($obj->eid == "") {
            $arrForm = $GLOBALS["session"]->getVar("form_param1");
            if(!is_array($arrForm)) $arrForm = array();
            
            if(!isset($arrForm['edata69']) || strlen($arrForm['edata69']) == 0){
                $arrForm['edata69'] = 1;
            }
            $GLOBALS["session"]->setVar("form_param1", $arrForm);
            $obj->getDispSession();
        }
    }


    /* デバッグ用 */
    function developfunc($obj)
    {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        $group_id = 3;
        
        // No.4 # 認知経路でその他を選択したらテキストを必須
        // 項目追加により、キーを修正( sovic : 2015.11.24 )
        $item_id = 28;
        $key     = "edata".$item_id;
        // if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 9){
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 11){
            Usr_init::addCheck($obj, array(63), 0);
        }
        
        Usr_Check::_check1($obj);
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /**
     * No.4 # 認知経路@その他にテキストボックスを移動
     */
    function mailfunc28($obj, $item_id, $name, $i = null) {
        $group = 1;
        
        // 認知経路@ラジオ
        $item_id = 28;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
        
        // 認知経路@テキスト
        $item_id = 63;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
        
        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


}
