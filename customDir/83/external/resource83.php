<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 * 
 * @author sovic kato
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource83 implements externalResource
{
	
	/**
	 * 項目の並び替えを行う
	 *
	 * @param  $obj ページオブジェクト
	 * @return void
	 * */

	public function sortFormIni($obj){
            //---------------------
            // グループ1 並び替え
            //---------------------
            $arrGroup1 =& $obj->arrItemData[1];
            // 入れ替え
            $array = array();
            foreach($arrGroup1 as $key => $data){
                switch($key){
                    case 1:
                        $array[10] = $arrGroup1[10];
                        $array[11] = $arrGroup1[11];
                        $array[12] = $arrGroup1[12];
                        $array[13] = $arrGroup1[13];
                        $array[1] = $arrGroup1[1];
                        $array[2] = $arrGroup1[2];
                        $array[3] = $arrGroup1[3];
                        $array[4] = $arrGroup1[4];
                        $array[25] = $arrGroup1[25];
                        $array[21] = $arrGroup1[21];
                        $array[28] = $arrGroup1[28];
                        $array[63] = $arrGroup1[63];
                        $array[71] = $arrGroup1[71];
                        $array[72] = $arrGroup1[72];
                        $array[73] = $arrGroup1[73];
                        $array[64] = $arrGroup1[64];
                        $array[69] = $arrGroup1[69];
                        $array[70] = $arrGroup1[70];
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 21:
                    case 25:
                    case 28:
                    case 64:
                    case 69:
                    case 70:
                    case 71:
                    case 72:
                    case 73:
                        break;
                    default:
                        $array[$key] = $data;
                        break;
                }
            }
            $arrGroup1 = $array;
	}

}