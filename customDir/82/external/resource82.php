<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 * 
 * @author sovic kato
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource82 implements externalResource
{
	
	/**
	 * 項目の並び替えを行う
	 *
	 * @param  $obj ページオブジェクト
	 * @return void
	 * */

	public function sortFormIni($obj){
            //---------------------
            // グループ1 並び替え
            //---------------------
            $arrGroup1 =& $obj->arrItemData[1];
            // 入れ替え
            $array = array();
            foreach($arrGroup1 as $key => $data){
                switch($key){
                    case 1:
                        $array[26] = $arrGroup1[26];
                        $array[1] = $arrGroup1[1];
                        break;
                    case 26:
                        break;
                    default:
                        $array[$key] = $data;
                        break;
                }
            }
            $arrGroup1 = $array;
	}

}