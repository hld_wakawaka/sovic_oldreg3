<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *
 */

require_once(ROOT_DIR."application/model/package.model.php");

class Mng_Entry113 extends AbstracePackage
{

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_detail_premain($obj){
        // パッケージ機能のロード

        parent::mng_detail_premain($obj);
    }

//     function __constructMng($obj) {
//     	$obj->arrItemData[3][55]['disp'] = 1;
//     }

//     function __constructMngCSV($obj) {
//     	$obj->arrItemData[3][55]['disp'] = 1;
//     }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------
    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){

    	$group = 1;
    	$groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
    	foreach($obj->arrItemData[$group] as $_key => $_data){
    		// 表示する設定の場合出力
    		if($_data["item_view"] == "1") continue;

    		// 画面に表示する項目の名称
    		$name = strip_tags($_data["item_name"]);

    		switch($_key){

    			case 71:
    			case 72:
     			case 73:
     			case 74:
    			case 75:
    			case 76:
     			case 77:
     			case 78:
    			case 79:
    				$groupHeader[$group][] = $obj->fix."Arrival Flight 1 ".$name.$obj->fix;
    				break;

    			case 80:
    			case 81:
     			case 82:
     			case 83:
    			case 115:
    			case 116:
     			case 117:
     			case 118:
    			case 119:
    				$groupHeader[$group][] = $obj->fix."Arrival Flight 2 ".$name.$obj->fix;
    				break;

    			case 120:
    			case 121:
    			case 122:
    			case 123:
    			case 124:
    			case 125:
    			case 126:
    			case 127:
    			case 128:
    				$groupHeader[$group][] = $obj->fix."Arrival Flight 3 ".$name.$obj->fix;
    				break;

    			case 131:
    			case 132:
     			case 133:
     			case 134:
    			case 135:
     			case 137:
     			case 138:
    				$groupHeader[$group][] = $obj->fix."Departure Flight ".$name.$obj->fix;
    				break;

    			default:
    				$groupHeader[$group][] = $obj->fix.$name.$obj->fix;
    				break;
    		}

    	}
    	return $groupHeader[$group];
    }


// /** CSV出力データ グループ1生成 */
//     function entry_csv_entryMakeData1($obj, $pa_param, $all_flg=false){

//         $group = 1;
//         $groupBody[$group] = array();
//         $groupBody[$group][] = $pa_param["entry_no"];
//         foreach($obj->arrItemData[$group] as $_key => $_data){
//             // 表示する設定の場合出力
//             if($_data["item_view"] == "1") continue;

//             $item_id = $_data["item_id"];

//             if(in_array($item_id, array(74, 78, 83, 118, 134, 138))) continue;

//             $methodname = "csvfunc".$item_id;
//             // カスタマイズあり
//             if(method_exists($this, $methodname)) {
//                 $wk_body = $this->$methodname($obj, $group, $pa_param, $item_id);

//             // カスタマイズなし
//             } else {
//                 // 任意項目
//                 if($_data["controltype"] == "1"){
//                     $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array("", ","), true);
//                     $wk_body = trim($wk_body, ",");

//                 // 標準項目
//                 }else{
//                     switch($item_id){
//                         //会員・非会員
//                         case 8:
//                             $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_kaiin[$pa_param["edata".$item_id]] : "";
//                             break;
//                         //連絡先
//                         case 16:
//                             $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_contact[$pa_param["edata".$item_id]] : "";
//                             break;
//                         //都道府県
//                         case 18:
//                             //英語フォームの場合
//                             if($GLOBALS["userData"]["lang"] == "2"){
//                                 $wk_body =  ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
//                             }
//                             else{
//                                 $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["prefectureList"][$pa_param["edata".$item_id]] : "";
//                             }
//                             break;
//                         //共著者の有無
//                         case 29:
//                             $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";
//                             break;

//                         //共著者の所属機関
//                         case 31:
//                             $wk_kikan_names = array();
//                             $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);

//                             $i =  "";
//                             $set_select = array();
//                             foreach($wk_kikanlist as $num => $val){
//                                 $i = $num+1;
//                                 $set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
//                                 $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
//                             }
//                             $wk_body = str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names));
//                             break;

//                         //Mr. Mis Dr
//                         case 57:
//                             if($pa_param["edata".$item_id] != ""){
//                                 $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
//                             }
//                             else{
//                                 $wk_body = "";
//                             }
//                             break;

//                         case 73:
//                         	$meridiem = Usr_Assign::nini($obj, 1, 74, $pa_param["edata74"], array("", ","), false);
//                         	$name = $meridiem.$name;
//                         	break;

//                         //国名リストボックス
//                         case 114:
//                             $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["country"][$pa_param["edata".$item_id]] : "";
//                             break;
//                         default:
//                             $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
//                             break;
//                     }
//                 }
//             }
//             $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
//         }
//         return $groupBody[$group];
//     }


//     function csvfunc73($obj, $group, $pa_param, $item_id) {
// //    	if(is_null($pa_param[$item_id])) return "";

//     	$name = $pa_param[$item_id];

//         $meridiem = Usr_Assign::nini($obj, 1, 74, $pa_param["edata74"], array("", ","), false);
// 		$name = $meridiem.$name;

//     	return $name;
//     }


//     function csvfunc77($obj, $group, $pa_param, $item_id) {
// //    	if(is_null($pa_param[$item_id])) return "";

//     	$name = $pa_param[$item_id];
//     	$meridiem = Usr_Assign::nini($obj, 1, 78, $pa_param["edata78"], array("", ","), false);
//     	$name = $meridiem.$name;

//     	return $name;
//     }


//     function csvfunc82($obj, $group, $pa_param, $item_id) {
// //    	if(is_null($pa_param[$item_id])) return "";

//     	$name = $pa_param[$item_id];
//     	$meridiem = Usr_Assign::nini($obj, 1, 83, $pa_param["edata83"], array("", ","), false);
//     	$name = $meridiem.$name;

//     	return $name;
//     }



//     function csvfunc117($obj, $group, $pa_param, $item_id) {
// //    	if(is_null($pa_param[$item_id])) return "";

//     	$name = $pa_param[$item_id];
//     	$meridiem = Usr_Assign::nini($obj, 1, 117, $pa_param["edata118"], array("", ","), false);
//     	$name = $meridiem.$name;

//     	return $name;
//     }


//     function csvfunc133($obj, $group, $pa_param, $item_id) {
// //    	if(is_null($pa_param[$item_id])) return "";

//     	$name = $pa_param[$item_id];
//     	$meridiem = Usr_Assign::nini($obj, 1, 134, $pa_param["edata134"], array("", ","), false);
//     	$name = $meridiem.$name;

//     	return $name;
//     }


//     function csvfunc137($obj, $group, $pa_param, $item_id) {
// //    	if(is_null($pa_param[$item_id])) return "";

//     	$name = $pa_param[$item_id];
//     	$meridiem = Usr_Assign::nini($obj, 1, 138, $pa_param["edata138"], array("", ","), false);
//     	$name = $meridiem.$name;

//     	return $name;
//     }


}
