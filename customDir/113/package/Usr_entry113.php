<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *
 */
require_once (ROOT_DIR . "customDir/113/package/Mng_entry113.php");

class Usr_entry113 extends Mng_entry113
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // パッケージ機能のロード
        $this->onloadPackage();


        // No.10 # 編集メールを差分モード
        $obj->save_editbefore = true;
        $obj->editmail_diff = true;

//        $obj->arrItemData[3][55]['disp'] = 1;
    }


    /**
     * 入力/選択しているかチェック
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return boolean true:入力状態, false:未入力
     *
     */
    function isNull($arrParam, $group_id, $item_id, $arrItemData){
        // チェックボックス
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            // 入力数をカウントするので反転
            return !(bool)$this->countChecked($arrParam, $group_id, $item_id, $arrItemData);
        }

        // その他
        // 標準関数の戻り値のbool値が逆なので反転
        return !Validate::isNull($arrParam["edata".$item_id]);
    }


    /**
     * チェックボックスの選択数を返す
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return integer $checked     チェックボックスの選択数
     *                              チェックボックスでない場合は0
     */
    function countChecked($arrParam, $group_id, $item_id, $arrItemData){
        $checked =0;
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            $options = $arrItemData['select'];
            foreach($options as $key => $val){
                $wk_val = isset($arrParam["edata".$item_id.$key]) ? $arrParam["edata".$item_id.$key]: "";
                if($wk_val != "") $checked++;
            }
        }

        return $checked;
    }



    /* デバッグ用 */
    function developfunc($obj)
    {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 2));
//         print "</pre><br/><br/>";
    }



    //----------------------------------------------------------
    // ▽編集メールの差分表示
    //----------------------------------------------------------

    // No.10 # 編集メールを差分モード
    function makeDiffBodyGroup1($obj, &$arrbody, $exec_type){
/*
        // 不要な差分を除去
        $arrData = $GLOBALS["session"]->getVar("form_editbefore");
        $keys = array(74, 78, 83, 118, 123, 127, 134, 138);
        foreach($keys as $item_id){
            $_key = "edata".$item_id;
            if($arrData[$_key] == "Please select"){
                $arrData[$_key] = "";
            }
        }
        $GLOBALS["session"]->setVar("form_editbefore", $arrData);
        // 編集の前後の差分
        $obj->arrDiff = Usr_entryDB::getParamEditDiff($obj);
*/
        // グループ1
        $group = 1;

        // 言語別設定
        $key = "group".$group;

        // 差分なし
        if(!isset($obj->arrDiff[$group]) || empty($obj->arrDiff[$group])){
            $arrbody[$group] .= $GLOBALS["msg"]["edit_mail_non"];
            return;
        }

        // 入力情報の退避
        $arrForm = $obj->arrForm;
        $head = ($obj->formdata["group1"] != "") ? "[".$obj->formdata["group1"]."]\n\n" : "";


        // T05-1の帯グループ
        $group1   = array(27);
        $group51  = array(57, 1, 2, 10, 13, 25, 14, 114, 58, 59, 18, 17, 21, 24, 26);
        $group52  = array(28, 63, 7, 64);
        $group53  = array(69, 70);
        $group58  = array(71, 72, 73, 74, 75, 76, 77, 78, 79);
        $group59  = array(80, 81, 82, 83, 115, 116, 117, 118, 119);
        $group510 = array(120, 121, 122, 123, 124, 125, 126, 127, 128);
        $group54  = array(129);
        $group55  = array(131, 132, 133, 134, 135, 136, 137, 138, 139);
        $group56  = array(140, 142, 143, 144);

        $arrGroup = array($group1, $group51, $group52, $group53, $group58, $group59, $group510, $group54, $group55, $group56);
        if($obj->arrForm["edata27"] == "2"){
            $arrGroup = array($group1, $group51);
        }

        foreach($arrGroup as $_key => $_arrGroup){
            $arrDiff = $this->makeDiffBodyGroup1byObi($obj, $group, $_arrGroup);
            if($arrDiff['cnt'] == 0) continue;

            $obi = "";

            $key = sprintf("%s@obi", $_arrGroup[0]);
            if($_key == 0) $obi .= $head;
            if(strlen(Config::$arrConfig['Layout'][$key]) > 0){
                $obi .= "[".Config::$arrConfig['Layout'][$key]."]\n\n";
            }

            $key2 = sprintf("%s@obi_after_text", $_arrGroup[0]);
            if(strlen(Config::$arrConfig['Layout'][$key2]) > 0){
                $obi .= strip_tags(Config::$arrConfig['Layout'][$key2])."\n\n";
            }

            $body  = $obi;
            $body .= $arrDiff["diff"];
            $arrbody[$group] .=  $body;
        }


        $obj->arrForm = $arrForm;
//        $arrbody[$group] .=  print_r($obj->arrForm, true);
    }


    function makeDiffBodyGroup1byObi($obj, $group, $arrGroup)
    {
        $str     = "";
        $diffCnt = 0;

        foreach($arrGroup as $item_id){
            if(!isset($obj->arrDiff[$group]["edata".$item_id])) continue;

            $diffCnt++;
            $diff = $obj->arrDiff[$group]["edata".$item_id];

            if(strlen($diff->before) == 0) $diff->before = $GLOBALS["msg"]["edit_non"];
            if(strlen($diff->after)  == 0) $diff->after  = $GLOBALS["msg"]["edit_non"];

            $data = $obj->arrItemData[$group][$item_id];

            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            $methodname = "mailfuncDiff".$item_id;
            if(method_exists($obj, $methodname)) {
                $str .= $obj->$methodname($name, $diff);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $str .= $obj->exClass->$methodname($obj, $item_id, $name, $diff);

                }else{
                    // 任意項目
                    if($data["controltype"] == "1") {
                        $str .= $obj->point_mark.$name.": ".$obj->mailfuncDiffNiniBody($group, $item_id, $diff);

                    // 標準項目 # テキストタイプ
                    } else {
                        $str .= $obj->point_mark.$name.": ".$diff->before.'→'.$diff->after."\n";
                    }
                }
            }
            $str .= "\n";
        }

        return array("diff" => $str, "cnt" => $diffCnt);
    }


    /**
     * 同意画面
     *   No.1 # 初期状態の場合に「I will〜」を選択
     *
     */
    function defaultAction($obj)
    {
        Usr_pageAction::defaultAction($obj);

        // 新規登録
        if($obj->eid == "") {
            $arrForm = $GLOBALS["session"]->getVar("form_param1");
            if(!is_array($arrForm)) $arrForm = array();

            if(!isset($arrForm['edata27']) || strlen($arrForm['edata27']) == 0){
                $arrForm['edata27'] = 1;
            }
            $GLOBALS["session"]->setVar("form_param1", $arrForm);
            $obj->getDispSession();
        }
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
//        // Assignを直前で変更する場合や特定のエラーチェックで利用
//    	$obj->arrItemData[3][55]['disp'] = 1;
        // 確認画面
        if($obj->block == "4"){
            // No.1 # I am 〜を選んだ時はAエリアだけ表示・不要な必須を解除
            $this->switch_1($obj, $obj->arrForm);
            // No.2 # Noを選んだ時はBエリアを非表示・必須を解除
            $this->switch_2($obj, $obj->arrForm);
            // No.3 # Noを選んだ時はBエリアを非表示・必須を解除
            $this->switch_3($obj, $obj->arrForm);
            // No.4 # Noを選んだ時はBエリアを非表示・必須を解除
            $this->switch_4($obj, $obj->arrForm);
        }
    }


    // エラーチェック # ブロック1
    function _check1($obj) {
        // No.1 # I am 〜を選んだ時はAエリアだけ表示・不要な必須を解除
        $this->switch_1($obj, $obj->arrParam, 2);
        // No.2 # Noを選んだ時はBエリアを非表示・必須を解除
        $this->switch_2($obj, $obj->arrParam, 2);
        // No.3 # Noを選んだ時はBエリアを非表示・必須を解除
        $this->switch_3($obj, $obj->arrParam, 2);
        // No.4 # Noを選んだ時はBエリアを非表示・必須を解除
        $this->switch_4($obj, $obj->arrParam, 2);

        Usr_Check::_check1($obj);
    }

    function _check3($obj){
        $group_id = 3;

        //ファイルアップロードチェック
        foreach($obj->arrfile as $item_id => $n){
            $obj->_checkfile($obj->arrParam, $group_id, $item_id, "1");
        }

        // 管理者はしない
        if($obj->admin_flg == "1") return;

        $checkId = array(56, 67);
        $param = $obj->arrParam;
        foreach($checkId as $item_id) {
            $i = 1;
            $inputData[$item_id] = array();
            $key = "edata".$item_id.$i;
            while(array_key_exists($key, $param) !== FALSE) {
                if($param[$key] != "") $inputData[$item_id][] = $param[$key];
                $i++;
                $key = "edata".$item_id.$i;
            }


            if(count($inputData[$item_id]) == 0) continue;
            if(count($inputData[$item_id]) == 2) continue;
            switch($item_id) {
            	case 56:
            	    $name = "No.3";
            	    $method = "%s : You are required to select two items.";
                    //$method = "You are required to select two items.";
            	    $obj->objErr->addErr(sprintf($method, $name), $key);
                    //$obj->objErr->addErr($method, $key);
            	    break;
            	case 67:
            	    $name = "No.4";
                    $method = "%s : You are required to select two items.";
                    //$method = "You are required to select two items.";
                    $obj->objErr->addErr(sprintf($method, $name), $key);
                    //$obj->objErr->addErr($method, $key);
            	    break;
            }
        }


        $obj->objErr->check($obj->_init($group_id), $obj->arrParam);
        $obj->_checkNini($group_id);
    }

    // No.1 # I am 〜を選んだ時はAエリアだけ表示・不要な必須を解除
    function switch_1($obj, $arrForm, $type=1){
        if($arrForm["edata27"] == "2"){
            $target = array(10, 13, 58, 59, 18, 17, 7, 21, 24, 26, 114, 28, 63, 64, 69, 129, 140, 141, 8);
            foreach($target as $_key => $_item_id){
                if($type == 1) $obj->arrItemData[1][$_item_id]['disp'] = "1";
                if($type == 2){
                    $obj->arrItemData[1][$_item_id]['item_check'] = str_replace("0", "", $obj->arrItemData[1][$_item_id]['item_check']);
                    $obj->itemData[$_item_id]['item_check'] = $obj->arrItemData[1][$_item_id]['item_check'];
                }
            }
        }
    }


    // No.2 # Noを選んだ時はBエリアを非表示・必須を解除
    function switch_2($obj, $arrForm, $type=1){
        if($arrForm["edata69"] != "1"){
            $target = array(70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 136);
            foreach($target as $_key => $_item_id){
                if($type == 1) $obj->arrItemData[1][$_item_id]['disp'] = "1";
                if($type == 2){
                    $obj->arrItemData[1][$_item_id]['item_check'] = str_replace("0", "", $obj->arrItemData[1][$_item_id]['item_check']);
                    $obj->itemData[$_item_id]['item_check'] = $obj->arrItemData[1][$_item_id]['item_check'];
                }
            }
        }
    }


    // No.3 # Noを選んだ時はBエリアを非表示・必須を解除
    function switch_3($obj, $arrForm, $type=1){
        if($arrForm["edata129"] != "1"){
            $target = array(130, 131, 132, 133, 134, 135, 136, 137, 138, 139);
            foreach($target as $_key => $_item_id){
                if($type == 1) $obj->arrItemData[1][$_item_id]['disp'] = "1";
                if($type == 2){
                    $obj->arrItemData[1][$_item_id]['item_check'] = str_replace("0", "", $obj->arrItemData[1][$_item_id]['item_check']);
                    $obj->itemData[$_item_id]['item_check'] = $obj->arrItemData[1][$_item_id]['item_check'];
                }
            }
        }
    }


    // No.4 # Noを選んだ時はBエリアを非表示・必須を解除
    function switch_4($obj, $arrForm, $type=1){
        if($arrForm["edata140"] != "1"){
            $target = array(142, 143, 144);
            foreach($target as $_key => $_item_id){
                if($type == 1) $obj->arrItemData[1][$_item_id]['disp'] = "1";
                if($type == 2){
                    $obj->arrItemData[1][$_item_id]['item_check'] = str_replace("0", "", $obj->arrItemData[1][$_item_id]['item_check']);
                    $obj->itemData[$_item_id]['item_check'] = $obj->arrItemData[1][$_item_id]['item_check'];
                }
            }
        }
    }


    /** メール本文生成 グループ1 */
    function makeBodyGroup1($obj, &$arrbody, $exec_type){
        // No.1 # I am 〜を選んだ時はAエリアだけ表示・不要な必須を解除
        $this->switch_1($obj, $obj->arrForm);
        // No.2 # Noを選んだ時はBエリアを非表示・必須を解除
        $this->switch_2($obj, $obj->arrForm);
        // No.3 # Noを選んだ時はBエリアを非表示・必須を解除
        $this->switch_3($obj, $obj->arrForm);
        // No.4 # Noを選んだ時はBエリアを非表示・必須を解除
        $this->switch_4($obj, $obj->arrForm);
        return Usr_mail::makeBodyGroup1($obj, $arrbody, $exec_type);
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /** No.7 */
    function mailfunc74 ($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc78 ($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc83 ($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc118($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc123($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc127($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc134($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc138($obj, $item_id, $name, $i = null) { return ""; }

    function mailfunc73($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 74;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc77($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 78;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc82($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 83;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc117($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 118;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc122($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 123;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc126($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 127;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc133($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 134;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    function mailfunc137($obj, $item_id, $name, $i = null) {
        $group = 1;

        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $item_id = 138;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value .= " ".Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);

        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


}
