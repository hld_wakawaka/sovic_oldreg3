$(document).ready(function(){

    var common = {
        edata: function(item_id){
            return "edata"+ item_id;
        },

        el: function(item_id){
            if($.isNumeric(item_id)){
                return $("."+ this.edata(item_id));
            }
            return $("."+ item_id);
        }
    }



    var operetaion1 = {
        target: 27,
        area  : ["row10", "row13", "row58", "row59", "row18", "row17", "row7", "row8", "row21", "row24", "row26", "row114", "row28", "row63", "row64"
               , "rtbl69", "row129", "row139", "rtbl140", "obi28", "obi69", "obi140", "obi69_text", "obi69_sub", "obi129_sub", "obi129_text", "obi140_text"],

        _show: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "");
            }
        },

        _hidden: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "none");
            }
        },

        run: function(){
            var context = this
            var $d = common.el(this.target);
            $d.bind("change", function(){
                var chk = $d.filter(":checked").val();

                if(chk != 2){
                    context._show();

                }else{
                    $("#edata69_2").click()
                    $("#edata129_2").click()
                    $("#edata140_2").click();
                    context._hidden();
                    $(".edata69").prop("checked", false)
                    $(".edata129").prop("checked", false)
                    $(".edata140").prop("checked", false)
                }

            }).change();
        }
    };



    var operetaion2 = {
        target: 69,
        area  : ["tr70", "obi71", "71_sub", "rtbl71", "obi80_sub", "rtbl80", "obi120_sub", "rtbl120", "row136"],

        _show: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "");
            }
        },

        _hidden: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "none");
            }
        },

        run: function(){
            var context = this
            var $d = common.el(this.target);
            $d.bind("change", function(){
                var chk = $d.filter(":checked").val();

                if(chk != 1){
                    context._hidden();

                }else{
                    context._show();
                }

            }).change();
        }
    };



    var operetaion3 = {
        target: 129,
        area  : ["tr130", "obi131", "obi131_sub", "rtbl131", "row139"],

        _show: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "");
            }
        },

        _hidden: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "none");
            }
        },

        run: function(){
            var context = this
            var $d = common.el(this.target);
            $d.bind("change", function(){
                var chk = $d.filter(":checked").val();

                if(chk != 1){
                    context._hidden();

                }else{
                    context._show();
                }

            }).change();
        }
    };



    var operetaion4 = {
        target: 140,
        area  : ["rtbl142", "description57"],

        _show: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "");
            }
        },

        _hidden: function(){
            for(var i in this.area){
                var $_area = common.el(this.area[i]);
                $_area.css("display", "none");
            }
        },

        run: function(){
            var context = this
            var $d = common.el(this.target);
            $d.bind("change", function(){
                var chk = $d.filter(":checked").val();

                if(chk != 1){
                    context._hidden();

                }else{
                    context._show();
                }

            }).change();
        }
    };



    operetaion1.run();
    operetaion2.run();
    operetaion3.run();
    operetaion4.run();

});
