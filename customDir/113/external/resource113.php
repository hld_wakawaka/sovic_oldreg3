<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 *
 * @author hunglead doishun
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource113 implements externalResource
{


    /**
     * 項目の並び替えを行う
     *
     * @param  $obj ページオブジェクト
     * @return void
     * */
    public function sortFormIni($obj){
        //---------------------
        // グループ1 並び替え
        //---------------------
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 57:
                    $array[27]   = $arrGroup1[27];
                    $array[$key] = $data;
                    break;

                case 14:
                    $array[$key] = $data;
                    $array[114]  = $arrGroup1[114];
                    break;

                case 26:
                    $array[$key] = $data;
                    $array[8]  = $arrGroup1[8];
                    break;

                case 13:
                    $array[$key] = $data;
                    $array[25]   = $arrGroup1[25];
                    break;

                case 63:
                    $array[$key] = $data;
                    $array[7]    = $arrGroup1[7];
                    break;

                case 70:
                    $array[$key] = $data;
                    $array[136]    = $arrGroup1[136];
                    break;

                case 130:
                    $array[$key] = $data;
                    $array[139]    = $arrGroup1[139];
                    break;

                case 7:
                case 8:
                case 25:
                case 27:
                case 114:
                case 136:
                case 139:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



}