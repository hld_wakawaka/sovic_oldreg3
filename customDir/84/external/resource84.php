<?php

/**
 * 共通プログラムの処理に加入するクラス
 *   + 並び替え処理を実行 # sortFormIni
 * 
 * @author sovic kato
 * */

require_once(ROOT_DIR."application/model/interface.external-resource.php");

class resource84 implements externalResource
{
	
	/**
	 * 項目の並び替えを行う
	 *
	 * @param  $obj ページオブジェクト
	 * @return void
	 * */

	public function sortFormIni($obj){
            //---------------------
            // グループ1 並び替え
            //---------------------
            $arrGroup1 =& $obj->arrItemData[1];
            // 入れ替え
            $array = array();
            foreach($arrGroup1 as $key => $data){
                switch($key){
                    case 57:
                        $array[57] = $arrGroup1[57];
                        $array[1]  = $arrGroup1[1];
                        $array[2]  = $arrGroup1[2];
                        $array[7]  = $arrGroup1[7];
                        $array[80] = $arrGroup1[80];
                        $array[10] = $arrGroup1[10];
                        $array[13] = $arrGroup1[13];
                        $array[14] = $arrGroup1[14];
                        $array[16] = $arrGroup1[16];
                        $array[58] = $arrGroup1[58];
                        $array[59] = $arrGroup1[59];
                        $array[18] = $arrGroup1[18];
                        $array[17] = $arrGroup1[17];
                        $array[26] = $arrGroup1[26];
                        $array[21] = $arrGroup1[21];
                        $array[24] = $arrGroup1[24];
                        $array[25] = $arrGroup1[25];
                        $array[26] = $arrGroup1[26];
                        $array[27] = $arrGroup1[27];
                        $array[79] = $arrGroup1[79];
                        $array[28] = $arrGroup1[28];
                        $array[63] = $arrGroup1[63];
                        $array[64] = $arrGroup1[64];
                        break;
                    case 1:
                    case 2:
                    case 7:
                    case 10:
                    case 13:
                    case 14:
                    case 16:
                    case 58:
                    case 59:
                    case 18:
                    case 17:
                    case 26:
                    case 21:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 79:
                    case 28:
                    case 63:
                    case 64:
                        break;
                    default:
                        $array[$key] = $data;
                        break;
                }
            }
            $arrGroup1 = $array;
	}

}