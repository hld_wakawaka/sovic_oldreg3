<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */
require_once (ROOT_DIR . "customDir/84/package/Mng_entry84.php");

class Usr_entry84 extends Mng_entry84
{

    const label0  = "Full Conference*1";
    const label4  = "One Day (Delegate)<br/>Per day, 2 days maximum";
    const label10 = "One Day (Delegate from developing countries*2 with participation in a workshop*3)<br/>Per day, 2 days maximum";
    const label16 = "One Day(NGO)<br/>Per day, 2 days maximum";
    const label22 = "One Day(Student)<br/>Per day, 2 days maximum";
    const label28 = "Accompanying Person(s)";

    public $arrTableGroup = array(
          "0"  => array("type" => Usr_init::INPUT_RADIO , "sub" => array( 0,  1,  2,  3)        , "label" => self::label0  , "show_unit" => false, "show_label_confirm" => true)
        , "4"  => array("type" => Usr_init::INPUT_CHECK , "sub" => array( 4,  5,  6,  7,  8,  9), "label" => self::label4  , "show_unit" => false, "show_label_confirm" => true)
        , "10" => array("type" => Usr_init::INPUT_CHECK , "sub" => array(10, 11, 12, 13, 14, 15), "label" => self::label10 , "show_unit" => false, "show_label_confirm" => true)
        , "16" => array("type" => Usr_init::INPUT_CHECK , "sub" => array(16, 17, 18, 19, 20, 21), "label" => self::label16 , "show_unit" => false, "show_label_confirm" => true)
        , "22" => array("type" => Usr_init::INPUT_CHECK , "sub" => array(22, 23, 24, 25, 26, 27), "label" => self::label22 , "show_unit" => false, "show_label_confirm" => true)
        , "28" => array("type" => Usr_init::INPUT_SELECT, "sub" => array(28)                    , "label" => self::label28 , "show_unit" => true , "show_label_confirm" => false)
    );


    // ★1
    public $arrFeeList1 = array(
          "ather_price1"
        , "ather_price10"
        , "ather_price11"
        , "ather_price12"
        , "ather_price13"
        , "ather_price14"
        , "ather_price15"
    );


    // ★2
    public $arrFeeList2 = array(
          "ather_price2"
        , "ather_price16"
        , "ather_price17"
        , "ather_price18"
        , "ather_price19"
        , "ather_price20"
        , "ather_price21"
    );
    
    


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // パッケージ機能のロード
        $this->onloadPackage();
        
        if(method_exists($obj, "assign")){
            $obj->assign("arrTableGroup", $this->arrTableGroup);
            $obj->_smarty->register_function('find_group_id', array ($this,'find_group_id'));
        }
        
        // [reg3-form84] 「PhD」の選択肢を非表示にして、「Dr.」の選択肢を以下に変更 → Dr./PhD
        $GLOBALS["titleList"] = array();
        $GLOBALS["titleList"]["1"] = "Prof.";
        $GLOBALS["titleList"]["2"] = "Dr./PhD";
        //$GLOBALS["titleList"]["3"] = "PhD";
        $GLOBALS["titleList"]["4"] = "Mr.";
        $GLOBALS["titleList"]["5"] = "Ms.";
       
    }


    /**
     * 指定のIDが金額テーブルのどのグループに所属するか判定する
     *
     * @param  integer 指定のその他決済項目のID
     * @return integer グループID
     *
     */
    function find_group_id($params, $smarty="")
    {
        $p_key = $params["p_key"];
        $group_id;
        foreach($this->arrTableGroup as $_group_id => $_arrTableGroup){
            if(array_search($p_key, $_arrTableGroup["sub"]) !== false){
                $group_id = $_group_id;
                break;
            }
        }
        
        $response = isset($params["response"]) ? $params["response"] : "assign";
        if($response == "assign"){
            $smarty->assign("fee_group_id", $group_id);
        }else{
            return $group_id;
        }
    }


    /**
     * グループ1において指定の入力があるかチェック
     * 
     * @param  array $search 探索配列
     * @return mixed false:入力なし / string:リクエストキー
     */
    function selectOtherto($serach, $master)
    {
        $result = false;
        
        // グループ1から1回目の遷移は$arrFormは情報が1つ古くなるので扱いに注意
        $arrForm = is_null($master) ? $GLOBALS["session"]->getVar("form_param1") : $master;
        foreach($serach as $_key => $_filecheck){
            if(isset($arrForm[$_filecheck]) && $arrForm[$_filecheck] > 0){
                $result = $_filecheck;
                break;
            }
        }
        return $result;
    }


    /**
     * self::$arrTableGroup.subのコールバック関数
     *   # array_map定義メソッド
     * 
     * @param  string
     * @return string
     */
    private function setOtherheader($value)
    {
        return "ather_price".$value;
    }



    function setMoneyPaydata($obj)
    {
        Usr_initial::setMoneyPaydata($obj);
        
        //-----------------------------------------
        // A10 : ファイル1を特定のその他決済項目を選択した場合に表示＆必須
        //-----------------------------------------
        $obj->filecheck[] = "ather_price3";
        $obj->filecheck[] = "ather_price22";
        $obj->filecheck[] = "ather_price23";
        $obj->filecheck[] = "ather_price24";
        $obj->filecheck[] = "ather_price25";
        $obj->filecheck[] = "ather_price26";
        $obj->filecheck[] = "ather_price27";
        $obj->assign("filecheck", $obj->filecheck);

        // amountベースのチェックなので細工をする
        if($key = $this->selectOtherto($obj->filecheck)){
            $arrForm = $GLOBALS["session"]->getVar("form_param1");
            $arrForm["amount"] = $key;
            $GLOBALS["session"]->setVar("form_param1", $arrForm);
            $obj->isUseGroup3[2]++;
            $obj->assign("isUseGroup3",$obj->isUseGroup3);
        }
    }



    // エラーチェック # ブロック1
    function _check1($obj)
    {
        $group_id = 1;
        Usr_Check::_check1($obj);
        
        // A2,A9 # 同伴者の入力数と購入数の不一致でエラー
        $this->checkA2A9($obj);
        
        // No.2,3
        $arrGroupChecked = array();
        foreach($this->arrTableGroup as $_group_id => $_arrTableGroup){
            if($_arrTableGroup["type"] == Usr_init::INPUT_SELECT) continue;
            
            $_sub = array_map(array($this, "setOtherheader"), $_arrTableGroup["sub"]);
            $_arrIntersect = array_intersect_key($obj->arrParam, array_flip($_sub));
            $arrGroupChecked[$_group_id] = array_sum($_arrIntersect);
        }
        while(true){
            // 異なるテーブルを跨いでのチェックはNG
            if(count(array_filter($arrGroupChecked)) > 1){
                $msg = "You can only select either Full Conference or One Day registration. Only one registration can be made at once.";
                $obj->objErr->addErr($msg, "ather_price4");
                break;
            }
            // 各テーブルの選択制限を2つまでとする
            if(array_sum($arrGroupChecked) > 2){
                $msg = "You can only select 2 days at most for One Day registration. If you would like to attend more than 2 days, please apply for Full Conference registration.";
                $obj->objErr->addErr($msg, "ather_price4");
                break;
            }
            // A11 # ⾦額☆1のいずれかの項⽬を選択した場合に必須設定
            if($key = $this->selectOtherto($this->arrFeeList1, $obj->arrParam)){
                $item_id = 27;
                $key = "edata" . $item_id;
                if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                    $msg = 'Please fill in “ID No. of Workshop Application” if you apply for “Delegate from developing countries with participation in a workshop” registration.';
                    $obj->objErr->addErr($msg, $key);
                    break;
                }
            }
            // A12 # ⾦額☆2のいずれかの項⽬を選択した場合に必須設定
            if($key = $this->selectOtherto($this->arrFeeList2, $obj->arrParam)){
                $items = array(28, 63, 64);
                foreach($items as $_key => $item_id){
                    $key = "edata" . $item_id;
                    if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                        $msg = 'Please fill in the details of NGO, if you apply for "NGO" registraion.';
                        $obj->objErr->addErr($msg, $key);
                        break;
                    }
                }
            }
            break;
        }
        
        // 追加No.1 # AccompanyingPerson以外のFeeエリアのいずれか必須
        while(true){
            // その他決済項目に関するエラーが発生しているかチェック
            if(count(preg_grep("/ather_price[0-9]*/u", array_keys($obj->objErr->_err))) > 0) break;
            
            // その他決済項目の選択数をチェック
            $keys = preg_grep("/ather_price[0-9]*/u", array_keys($obj->arrParam));
            $others = array();
            foreach($keys as $_key => $_otherkey){
                if($_otherkey == "ather_price28") continue;
                $others[$_otherkey] = $obj->arrParam[$_otherkey];
            }
            if(count(array_filter($others)) == 0){
                $obj->objErr->addErr("Select one(s) for Fee.", "ather_price28");
            }
            break;
        }
    }



    //-----------------------------------------
    // A2,A9 # 同伴者の入力数と購入数の不一致でエラー
    //-----------------------------------------
    function checkA2A9($obj)
    {
        $cnt     = 0; // 同伴者数
        $item_id = 70;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $item_id) && $obj->objErr->isNull($obj->arrParam[$key])) $cnt++;
        
        $item_id = 75;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $item_id) && $obj->objErr->isNull($obj->arrParam[$key])) $cnt++;
        
        
        // その他決済 # 同伴者
        $other = strlen($obj->arrParam['ather_price28']) > 0 ? intval($obj->arrParam['ather_price28']) : 0;
        
        while(true){
            // A2 : 同伴者入力がありその他決済項目の同伴者の入力が不一致
            if($cnt > 0 && ($cnt != $other)){
                $obj->objErr->addErr("Please select Accompanying Person(s) for Fee.", "ather_price28");
                break;
            }
            // A9 : その他決済項目の同伴者の入力があり、同伴者の入力がない場合
            if($other > 0 && ($cnt != $other)){
                $obj->objErr->addErr("Please fill out the personal information of your Accompanying Person(s).", "ather_price28");
                break;
            }
            break;
        }
    }


    /* デバッグ用 */
    function developfunc($obj)
    {
/*
         // メールデバッグ
         $obj->ins_eid = 1615;
         print "--------------------<pre style='text-align:left;'>";
         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
         print "</pre><br/><br/>";
*/
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }




    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makeBodyGroup3($obj, &$arrbody, $exec_type)
    {
        //-----------------------------------------
        // A10 : ファイル1を特定のその他決済項目を選択した場合に表示＆必須
        //-----------------------------------------
        $obj->filecheck[] = "ather_price3";
        $obj->filecheck[] = "ather_price22";
        $obj->filecheck[] = "ather_price23";
        $obj->filecheck[] = "ather_price24";
        $obj->filecheck[] = "ather_price25";
        $obj->filecheck[] = "ather_price26";
        $obj->filecheck[] = "ather_price27";

        // amountベースのチェックなので細工をする
        if($key = $this->selectOtherto($obj->filecheck, $obj->arrForm)){
            $obj->arrForm["amount"] = $key;
        }
        
        Usr_mail::makeBodyGroup3($obj, &$arrbody, $exec_type);
    }


    /** その他決済のメール本文 */
    function makePaymentBody2($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //その他決済がある場合
        if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

            //新規の場合
            if($exec_type == "1"){
                $pre_group_id = -1;
                foreach($obj->wa_ather_price  as $p_key => $data ){
                    $group_id = $this->find_group_id(array("p_key" => $p_key, "response" => "return"));
                    
                    if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                    if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
                        $price = $data["p1"];

                        // Free, - => 0
                        if(!is_numeric($data["p1"])){
                            $data["p1"] = 0;
                        }
                        $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                        // タグを除去
                        $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = number_format($price)."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = $obj->yen_mark.number_format($price);
                            }
                            // 
                            $sp = "";
                            if($this->arrTableGroup[$group_id]['show_label_confirm'] && $pre_group_id != $group_id){
                                $label = strip_tags($this->arrTableGroup[$group_id]['label']);
                                $body_pay .="      ".$label."\n";
                                $sp = "   ";
                            }
                            if($pre_group_id == $group_id){
                                $sp = "   ";
                            }
                            
                            $body_pay .="      ".$sp.$data["name"].":".$obj->yen_mark.number_format($sum)."  (".$price." ×".$obj->arrForm["ather_price".$p_key].")\n";
                            $pre_group_id = $group_id;
                        }
                    }
                }
                
            }

            //更新の場合
            if($exec_type == "2"){
                //更新の場合は、登録済みのデータから引っ張ってくる
                $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid, "", $obj->hide_fee_area);

                foreach($wa_payment_detail as $data){
                    $price = $data["price"];
                    // Free, - => 0
                    if(!is_numeric($data["price"])){
                        $data["price"] = 0;
                    }
                    $sum = $data["price"]*$data["quantity"];

                    // タグを除去
                    $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["lang"] == LANG_JPN){
                        if(is_numeric($price)){
                            $price = $price."円";
                        }
                        $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                    }
                    else{
                        if(is_numeric($price)){
                            $price = $obj->yen_mark.$price;
                        }
                        $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum);
                    }

                    // その他決済の場合は内訳を後ろにつける
                    if($data["type"] == "1"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .="　（".$price." ×".$data["quantity"]."）";
                        }
                        else{
                            $body_pay .="  (".$price." ×".$data["quantity"].")";
                        }
                    }
                    $body_pay .= "\n";
                }
            }
        }
        return $body_pay;
    }

}
