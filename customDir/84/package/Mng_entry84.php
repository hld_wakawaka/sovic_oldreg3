<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."application/model/package.model.php");

class Mng_Entry84 extends AbstracePackage
{

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_detail_premain($obj){
        // パッケージ機能のロード
        parent::mng_detail_premain($obj);
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------
    /**
     * CSVヘッダ生成
     */
    function payment_payMakeHeader($obj, $all_flg=false){
        $header   = array();
        // payment_csvのみ出力
        if(!$all_flg){
            $header[] = "応募者氏名";
            $header[] = "応募者氏名（カナ）";
        }
        $header[] = "取引ID";
        $header[] = "お支払い方法";
        $header[] = "ステータス";

        $header[] = "支払期限";
        $header[] = "クレジット番号";
        $header[] = "カード会社";
        $header[] = "カード名義人";
        $header[] = "有効期限";
        $header[] = "クレジット支払い区分";
        $header[] = "クレジット支払い回数";
//        $header[] = "セキュリティコード";

        $header[] = "お振込み人名義";
        $header[] = "お支払銀行名";
        $header[] = "お振込み日";

        $header[] = "コンビニ　ショップコード";
        $header[] = "姓";
        $header[] = "名";
        $header[] = "セイ";
        $header[] = "メイ";
        $header[] = "電話番号";
        $header[] = "メールアドレス";
        $header[] = "郵便番号";
        $header[] = "住所";
        $header[] = "成約日";
        $header[] = "決済機関コード";
        //$header[] = "支払詳細";


        //------------------------------
        //決済項目
        //------------------------------
        foreach($obj->wa_ather_price as $p_key => $data){
            $group_id = $this->find_group_id(array("p_key" => $p_key, "response" => "return"));
            $label = ($this->arrTableGroup[$group_id]["type"] == Usr_init::INPUT_CHECK)
                   ? $this->arrTableGroup[$group_id]["label"]
                   : "";
            
            $head = strip_tags($label." ".$data);
            $header[] = strip_tags(str_replace(array("_"), " ",$head));
            // 明細表示フラグ
            if($obj->payment_csv_price_disp){
                $header[] = $obj->payment_csv_price_head;
            }
        }

        $header[] = "合計金額";
        $header[] = "登録日";
        $header[] = "更新日";

        //生成
        $ret_buff  = implode($obj->delimiter, $header);
        $ret_buff .="\n";

        return $ret_buff;
    }


}
