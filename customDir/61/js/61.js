/*  */
$(document).ready(function(){
    var group1 = ["edata27","edata28" ,"edata63" ,"edata64" ,"edata69" ,"edata70" ,"edata71" ,"edata72" ,"edata73"];   // May15
    var group2 = ["edata74","edata75" ,"edata76" ,"edata77" ,"edata78" ,"edata79" ,"edata80" ,"edata81" ,"edata82"];   // May16
    var group3 = ["edata83","edata115","edata116","edata117","edata118","edata119","edata120","edata121","edata122"];  // May17

    $(".tbl_ls input:radio").bind("change", function(){
        var chks = $(".tbl_ls input:radio").filter(":checked");
        var cnt  = chks.size();
        if(cnt == 0) return;

        var arrGroup1 = [];
        var arrGroup2 = [];
        var arrGroup3 = [];
        var i = 0;
        chks.each(function(){
            var className = $(this).attr("class");
            if($.inArray(className, group1) != -1) arrGroup1.push(className);
            if($.inArray(className, group2) != -1) arrGroup2.push(className);
            if($.inArray(className, group3) != -1) arrGroup3.push(className);
        });

        var chk = $(this).filter(":checked").attr("id");
        if(arrGroup1.length > 1){
            for(i in group1) $("."+group1[i]).prop("checked", false);
            $("#"+chk).prop("checked", true);
        }
        if(arrGroup2.length > 1){
            for(i in group2) $("."+group2[i]).prop("checked", false);
            $("#"+chk).prop("checked", true);
        }
        if(arrGroup3.length > 1){
            for(i in group3) $("."+group3[i]).prop("checked", false);
            $("#"+chk).prop("checked", true);
        }
    });

});
