<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *        
 */
class Mng_Entry61 {

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_detail_premain($obj){
        // May15,16,17の表示設定
        $this->setMayUnDisplay($obj);
        
        //--------------------------
        // パッケージ機能
        //--------------------------
        $template_dir = $obj->_smarty->template_dir."../customDir/61/package/templates/";
        $obj->_processTemplate = $template_dir."Mng/entry/Mng_entry_detail.html";
        $obj->assign("package", $template_dir);
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /* MngCSVコンストラクタ */
    function __constructMngCSV($obj){
        // May15
        $i = 0;
        foreach($this->group1 as $_key => $item_id){
            if($i < 2){ $i++; continue; } // 最初から2つ目までのitem_idは表示
            $obj->arrItemData[1][$item_id]["item_view"] = 1;
        }
        // May16
        $i = 0;
        foreach($this->group2 as $_key => $item_id){
            if($i < 2){ $i++; continue; } // 最初から2つ目までのitem_idは表示
            $obj->arrItemData[1][$item_id]["item_view"] = 1;
        }
        // May17
        $i = 0;
        foreach($this->group3 as $_key => $item_id){
            if($i < 2){ $i++; continue; } // 最初から2つ目までのitem_idは表示
            $obj->arrItemData[1][$item_id]["item_view"] = 1;
        }
        
        return "";
    }


    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;
    
            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);
            if(in_array($_key, array(27,  28))) $name = "May 15 (Fri.)";
            if(in_array($_key, array(74,  75))) $name = "May 16 (Sat.)";
            if(in_array($_key, array(83, 115))) $name = "May 17 (Sun.)";
            
            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }

    /* May15:選択項目名 */
    function csvfunc27($obj, $group, $pa_param, $item_id) {
        $item_id = Usr_Entry61::getMaySelectItemId($this->group1, $pa_param);
        if(is_null($item_id)) return "";
        
        $name = $obj->arrItemData[1][$item_id]["item_name"];
        return $name;
    }

    /* May15:選択値 */
    function csvfunc28($obj, $group, $pa_param, $item_id) {
        $item_id = Usr_Entry61::getMaySelectItemId($this->group1, $pa_param);
        if(is_null($item_id)) return "";
        
        $body = Usr_Assign::nini($obj, 1, $item_id, $pa_param["edata".$item_id], array("", ","), false);
        $body = trim($body, ",");
        return $body;
    }


    /* May16:選択項目名 */
    function csvfunc74($obj, $group, $pa_param, $item_id) {
        $item_id = Usr_Entry61::getMaySelectItemId($this->group2, $pa_param);
        if(is_null($item_id)) return "";
        
        $name = $obj->arrItemData[1][$item_id]["item_name"];
        return $name;
    }

    /* May16:選択値 */
    function csvfunc75($obj, $group, $pa_param, $item_id) {
        $item_id = Usr_Entry61::getMaySelectItemId($this->group2, $pa_param);
        if(is_null($item_id)) return "";
        
        $body = Usr_Assign::nini($obj, 1, $item_id, $pa_param["edata".$item_id], array("", ","), false);
        $body = trim($body, ",");
        return $body;
    }


    /* May17:選択項目名 */
    function csvfunc83($obj, $group, $pa_param, $item_id) {
        $item_id = Usr_Entry61::getMaySelectItemId($this->group3, $pa_param);
        if(is_null($item_id)) return "";
        
        $name = $obj->arrItemData[1][$item_id]["item_name"];
        return $name;
    }

    /* May17:選択値 */
    function csvfunc115($obj, $group, $pa_param, $item_id) {
        $item_id = Usr_Entry61::getMaySelectItemId($this->group3, $pa_param);
        if(is_null($item_id)) return "";
       
        $body = Usr_Assign::nini($obj, 1, $item_id, $pa_param["edata".$item_id], array("", ","), false);
        $body = trim($body, ",");
        return $body;
    }

}
