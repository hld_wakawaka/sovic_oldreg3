<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Mng
 * @author hunglead doishun
 *        
 */
class Mng_Entry63 {

    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_detail_premain($obj){
        //--------------------------
        // パッケージ機能
        //--------------------------
        $form_id = 63;
        $template_dir = $obj->_smarty->template_dir."../customDir/{$form_id}/package/templates/";
        $obj->_processTemplate = $template_dir."Mng/entry/Mng_entry_detail.html";
        $obj->assign("package", $template_dir);
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /* MngCSVコンストラクタ */
    function __constructMngCSV($obj){
    }


    /** CSVヘッダ生成 */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){
        // 卒業年月はCSVは表示する
        $item_id = 63;
        $obj->arrItemData[1][$item_id]['disp']      = '';
        $obj->arrItemData[1][$item_id]['item_view'] = '';
        
        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());
        
        // グループ1ッダ
        $groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);
        
        // グループ2ヘッダ
        $groupHeader[2] = $obj->entryMakeHeader2($obj, $all_flg);
        
        // グループ3ヘッダ
        $groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);
        
        
        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[] = $obj->fix.$GLOBALS["csv"]["status"].$obj->fix;
        $header[] = $obj->fix.$GLOBALS["csv"]["rdate"] .$obj->fix;
        $header[] = $obj->fix.$GLOBALS["csv"]["udate"] .$obj->fix;
        
        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }


}
