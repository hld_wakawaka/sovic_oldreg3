$(document).ready(function(){
	var edata = $(".edata71");
	var arrAdClass = [72,73,74,75,78,79,80,81,116]; // クラスを付けたいitem_id配列を定義
	for (i = 0; i < arrAdClass.length; i++){
	    var key = "edata"+arrAdClass[i];
	    $("."+key).closest("tr").addClass(key);
	}
	$(".bname:eq(1)").addClass("obi");

    if(edata.size() > 0){
        edata.bind("change", function(){
            var chk = $(".edata71").filter(":checked").val(); // input:radio
            if(typeof chk == 'undefined'){
            	$(".obi").fadeOut('normal');
            	$(".obi_text").fadeOut('normal');
            	$(".edata72").fadeOut('normal');
                $(".edata73").fadeOut('normal');
                $(".edata74").fadeOut('normal');
                $(".edata75").fadeOut('normal');
                $(".edata76").fadeOut('normal');
//                $(".edata77").fadeOut('normal');
                $(".edata78").fadeOut('normal');
                $(".edata79").fadeOut('normal');
                $(".edata80").fadeOut('normal');
                $(".edata81").fadeOut('normal');
                $(".edata82").fadeOut('normal');
//                $(".edata83").fadeOut('normal');
//                $(".edata115").fadeOut('normal');
                $(".edata116").fadeOut('normal');
            	return;
            }
            if(chk == 2) {
            	$(".obi").fadeIn('normal');
            	$(".obi_text").fadeIn('normal');
            	$(".edata72").fadeIn('normal');
            	$(".edata73").fadeIn('normal');
                $(".edata74").fadeIn('normal');
                $(".edata75").fadeIn('normal');
                $(".edata76").fadeIn('normal');
//                $(".edata77").fadeIn('normal');
                $(".edata78").fadeIn('normal');
                $(".edata79").fadeIn('normal');
                $(".edata80").fadeIn('normal');
                $(".edata81").fadeIn('normal');
                $(".edata82").fadeIn('normal');
//                $(".edata83").fadeIn('normal');
//                $(".edata115").fadeIn('normal');
                $(".edata116").fadeIn('normal');
            	return;
            }
            $(".obi").fadeOut('normal');
            $(".obi_text").fadeOut('normal');
            $(".edata72").fadeOut('normal');
            $(".edata73").fadeOut('normal');
            $(".edata74").fadeOut('normal');
            $(".edata75").fadeOut('normal');
            $(".edata76").fadeOut('normal');
//            $(".edata77").fadeOut('normal');
            $(".edata78").fadeOut('normal');
            $(".edata79").fadeOut('normal');
            $(".edata80").fadeOut('normal');
            $(".edata81").fadeOut('normal');
            $(".edata82").fadeOut('normal');
//            $(".edata83").fadeOut('normal');
//            $(".edata115").fadeOut('normal');
            $(".edata116").fadeOut('normal');
        }).change();
    } 
    
    var edata63 = $(".edata63");
    edata63.bind("change", function(){
    	var chk = $(".edata63").val();	
    	$("#amount_2").prop("disabled",chk.length > 0 ? false : true);
    	$("#amount_3").prop("disabled",chk.length > 0 ? false : true);	
    }).change();
});