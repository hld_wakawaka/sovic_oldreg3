<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */
require_once (ROOT_DIR . "customDir/72/package/Mng_entry_form_id.php");

class Usr_entry_form_id extends Mng_entry_form_id
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        // パッケージ機能のロード
        $this->onloadPackage();
    }


    /**
     * 入力/選択しているかチェック
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return boolean true:入力状態, false:未入力
     *
     */
    function isNull($arrParam, $group_id, $item_id, $arrItemData){
        // チェックボックス
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            // 入力数をカウントするので反転
            return !(bool)$this->countChecked($arrParam, $group_id, $item_id, $arrItemData);
        }
        
        // その他
        // 標準関数の戻り値のbool値が逆なので反転
        return !Validate::isNull($arrParam["edata".$item_id]);
    }


    /**
     * チェックボックスの選択数を返す
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return integer $checked     チェックボックスの選択数
     *                              チェックボックスでない場合は0
     */
    function countChecked($arrParam, $group_id, $item_id, $arrItemData){
        $checked =0;
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            $options = $arrItemData['select'];
            foreach($options as $key => $val){
                $wk_val = isset($arrParam["edata".$item_id.$key]) ? $arrParam["edata".$item_id.$key]: "";
                if($wk_val != "") $checked++;
            }
        }
    
        return $checked;
    }



    /* デバッグ用 */
    function developfunc($obj)
    {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }




    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------


}
