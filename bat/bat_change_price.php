<?php
/**
 * 金額自動変更バッチ
 * 指定した日時になったら金額の自動変更を行う
 *
 */
if(strpos(__FILE__, "testhp.jp") !== false) {
    include('/var/www/vhosts/testhp.jp/httpdocs/reg3/application/cnf/batch_include.php');
} else {
    include('/var/www/vhosts/evt-reg.jp/httpdocs/reg3/application/cnf/batch_include.php');
}
include_once(MODULE_DIR.'mail_class.php');

class change_price{

	/**
	 * コンストラクタ
	 */
	function __construct(){


	}

	/**
	 * メイン処理
	 */
	function main(){
        //実行日
        $this->exec_date = date("YmdH");

        //-------------------------------
        //インスタンス生成
        //--------------------------------
        $this->db = new DbGeneral;
        $this->o_mail = new Mail();


		//-------------------------------
		//対象データ取得
		//-------------------------------
        $formData = $this->_getForm();

        if(file_exists(LOG_DIR."DBLog/DB_".date('Ymd').".log")){
            chmod(LOG_DIR."DBLog/DB_".date('Ymd').".log", 0777);
        }
                
        if(!$formData){
        	//終了
            exit;
        }
        



		//-------------------------------
		//金額書き換え
		//-------------------------------
        $msg = "金額自動変更を行いました。\n\n";
   
        
        //フォーム単位でコミット
        foreach($formData as $data){
            
            //--------------------------------
            //トランザクション
            //--------------------------------
            $this->db->begin();
            
            //--------------------------------
            //更新処理
            //--------------------------------
            $param["disp"] = $data["auto_change_disp"];   //表示形式
            $param["price"] = $data["auto_change_price"];   //金額
            //$param["price_header"] = $data["auto_change_price_head0"];   //金額見出し  （early）
            
            $param["price_head0"] = $data["auto_change_price_head0"];   //金額見出し  （early）
            $param["price_head1"] = $data["auto_change_price_head1"];   //金額見出し  （late）   
            $param["ather_price"] = $data["auto_change_ather_price"];   //その他決済項目　金額
            $param["ather_price_head0"] = $data["auto_change_ather_price_head0"];   //その他決済項目　金額見出し  （early）
            $param["ather_price_head1"] = $data["auto_change_ather_price_head1"];   //その他決済項目　金額見出し  （late）

			// csv見出し
            $param["csv_price_head0"] = $data["csv_auto_change_price_head0"];   //csv金額見出し  （early）
//            $param["csv_price_head1"] = $data["csv_auto_change_price_head1"];   //csv金額見出し  （late）   
            $param["csv_ather_price_head0"] = $data["csv_auto_change_ather_price_head0"];   //csvその他決済項目　金額
//            $param["csv_ather_price_head1"] = $data["csv_auto_change_ather_price_head1"];   //csvその他決済項目　金額見出し  （early）



            //変更したら予約データをクリア
            $param["auto_change_price"] = "";
            $param["auto_change_price_head0"] = "";
            $param["auto_change_price_head1"] = "";
            $param["auto_change_ather_price"] = "";           
            $param["auto_change_ather_price_head0"] = "";    
            $param["auto_change_ather_price_head1"] = "";
            $param["auto_change_flg"] = "0";
            $param["auto_change_day"] = "";
            $param["auto_change_disp"] = NULL;

            $param["csv_auto_change_price_head0"] ="";
//            $param["csv_auto_change_price_head1"] ="";
            $param["csv_auto_change_ather_price_head0"] ="";
//            $param["csv_auto_change_ather_price_head1"] ="";

               
            $where[] = "form_id = ".$data["form_id"];
            
            $rs = $this->db->update("form", $param, $where, __FILE__, __LINE__);
            if(!$rs) {
                $this->db->rollback();
                continue;
            }           
            
            unset($param);
            unset($where);
            
            //-------------------------------
            //ログ登録
            //-------------------------------
            $param["form_id"] = $data["form_id"];
            $param["change_date"] =$data["auto_change_day"];
            $param["from_disp_mode"] = $data["disp"];
            $param["from_price"] = $data["price"];
            $param["from_ather_price"] = $data["ather_price"];
            $param["from_price_head0"] = $data["price_head0"];
            $param["from_price_head1"] = $data["price_head1"];
            $param["from_disp_mode"] = $data["disp"];
            
            $param["to_price"] = $data["auto_change_price"];
            $param["to_ather_price"] = $data["auto_change_ather_price"];
            $param["to_price_head0"] = $data["auto_change_ather_price_head0"];
            $param["to_price_head1"] = $data["auto_change_ather_price_head1"];
            $param["to_disp_mode"] = $data["auto_change_disp"];

            $param["from_csv_price_head0"] 	 = $data["csv_price_head0"];
//            $param["from_csv_price_head1"] 	 = $data["csv_price_head1"];
            $param["from_csv_ather_price_head0"] = $data["csv_ather_price_head0"];
//            $param["from_csv_ather_price_head1"] = $data["csv_ather_price_head1"];
            $param["to_csv_price_head0"]		 = $data["csv_auto_change_price_head0"];
//            $param["to_csv_price_head1"]		 = $data["csv_auto_change_price_head1"];
            $param["to_csv_ather_price_head0"]	 = $data["csv_auto_change_ather_price_head0"];
//            $param["to_csv_ather_price_head1"]	 = $data["csv_auto_change_ather_price_head1"];

            $rs = $this->db->insert("change_money_log", $param, __FILE__, __LINE__);
    
            if(!$rs) {
                $this->db->rollback();
                continue;                

            }
 
            unset($param);
            unset($where);   
 
            //--------------------------------
            //コミット
            //--------------------------------
            $this->db->commit(); 


            $msg .="\nフォームID:".$data["form_id"];


            //--------------------------------------
            //金額変更通知メール
            //--------------------------------------
            $body = "";
            $body = "金額の自動変更を行いました。\n";
            $body .= "フォームをご確認ください。\n\n";
            $body .="フォーム番号:".$data["form_id"]."\n";
            $body .="フォーム名:".$data["form_name"]."\n";

            //$this->o_mail->SendMail($data["form_mail"], "金額自動変更通知", $body, SYSTEM_MAIL);
        }


		//--------------------------------
		//終了処理
		//--------------------------------
		$this->_complete($msg);
        
        

        

		return true;

	}

    /**
     * 対象データ取得
     */
    private function _getForm(){

        //SQL生成
        $column = "*";
        $from = "form";
        $where[] = "del_flg = 0";
        $where[] = "auto_change_day = '".$this->exec_date."'";
        $where[] = "auto_change_flg = 1";
        
        $rs = $this->db->getListData($column, $from, $where, $orderby, -1, -1, __FILE__, __LINE__);
        return $rs;
    	
    }
     
	/**
	 * 終了処理
	 */
	private function _complete($msg){

        chmod(LOG_DIR."DBLog/DB_".date('Ymd').".log", 0777);

		$body = $msg;
		$subject = "論文投稿システム　金額自動変更バッチ実行通知";


		//管理者宛て送信
		$this->o_mail->SendMail(SYSTEM_MAIL, $subject, $body, "");


	}

}

$c = new change_price();
$c->main();

