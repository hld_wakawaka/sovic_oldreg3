<?php
/**
 * ファイル圧縮バッチ
 *  指定したフォームにアップされたファイルを圧縮する
 *
 */

include(dirname(__FILE__).'/../../../application/cnf/batch_include.php');
include_once(MODULE_DIR.'mail_class.php');

class file_archive54{

	/**
	 * コンストラクタ
	 */
	function __construct($batData){


ini_set("error_reporting", E_ALL);
ini_set( "error_log", "/tmp/php.log" );


        //フォームID
        $this->form_id = $batData[1];

        //取得対象ファイル番号
        $this->target_file = $batData[2];

        // アーカイブ化の対象者
        $this->arrEid = explode(",", $batData[3]);

        // アップロードファイルの添付IDと添え字
        $this->arrfile = array(
                            "1" => "a",
                            "2" => "b",
                            "3" => "c"
                        );

	}

	/**
	 * メイン処理
	 */
	function main(){

        //実行日
        $this->exec_date = date("YmdHis");

        //-------------------------------
        //インスタンス生成
        //--------------------------------
        $this->db = new DbGeneral;
        $this->o_mail = new Mail();


		//-------------------------------
		//対象データ取得
		//-------------------------------
        $this->formData = $this->_getForm();

        $msg="フォーム名：".$this->formData["form_name"]."\n\n";

        //--------------------------------
        //ダウンロード用ディレクトリ作成
        //--------------------------------
        $this->download_dir = ROOT_DIR."download/form".$this->form_id."/".$this->exec_date."/";
        if(!is_dir($this->download_dir)){
            mkdir($this->download_dir, 0777, true);
        }


        //--------------------------------
        //ダウンロード用ディレクトリにファイルを集める
        //--------------------------------
        $this->moto_dir = ROOT_DIR."upload/Usr/form".$this->form_id;
        
        // 対象者のみファイルを集める
        foreach($this->arrEid as $_key => $eid){
            $cmd = "cp ".$this->moto_dir."/{$eid}/word/"."*.docx ".$this->download_dir.".";
            system($cmd);
        }
        
        
        
        //-------------------------------
        //集めたファイルの数をカウント
        //-------------------------------
        $cmd = "ls ".$this->download_dir."/* | wc -w";
        $file_count = system($cmd);
        $file_count = str_replace(array("\r\n","\r","\n"), "", $file_count);        
        if($file_count == 0){
            $this->_complete($msg."ファイルのアーカイブに失敗しました.");
            exit;
        }

        
        //-------------------------------
        //集めたファイルを圧縮
        //-------------------------------
        $zip_filename = "file_archives".$this->target_file."_".$this->exec_date.".zip";
        
        $cmd = "zip -j ".ROOT_DIR."download/form".$this->form_id."/".$zip_filename." ".$this->download_dir."/*";
        system($cmd);


        if(!is_file(ROOT_DIR."download/form".$this->form_id."/".$zip_filename)){
            $this->_complete($msg."ファイルのアーカイブに失敗しました。");
            exit;        	
        }

        
        


        //-------------------------------
        //ダウンロード用URL生成
        //-------------------------------
        //ダウンロードURL生成
        $hash = md5(HASH_GENETATOR. $this->formData["password"]. $this->exec_date);
        $down_url = URL_ROOT."bat/include/54/file_download54.php?id=".$this->form_id."&hash=".$hash."&key=".$this->exec_date."&n=".$this->target_file;

        $msg = $msg."ファイルのアーカイブが完了しました。\n下記のURLよりダウンロードしてください。\n\n";
        $msg .= $down_url."\n\n";
        $msg .="※ダウンロードの有効期間は3日間です。";

        $this->_complete($msg);
        exit;  

		return true;

	}

    /**
     * 対象データ取得
     */
    private function _getForm(){

        //SQL生成
        $column = "*";
        $from = "form";
        $where[] = "form_id = ".$this->db->quote($this->form_id);
        $where[] = "del_flg = 0";

        $rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);
        return $rs;
    	
    }
     
	/**
	 * 終了処理
	 */
	private function _complete($msg){

        chmod(LOG_DIR."DBLog/DB_".date('Ymd').".log", 0777);

		$body = $msg."\n";
		$subject = "【".$this->formData["form_name"]."】ファイルアーカイブ通知 論文データ一括DL ".$this->target_file;

        $this->_deleteDir($this->download_dir);


		//送信
		$this->o_mail->SendMail($this->formData["form_mail"], $subject, $body, "filedownload@evt-reg2.jp");


	}


    /**
     * ディレクトリ削除
     */
    private function _deleteDir($ps_dir){
        
        if (is_dir($ps_dir)) {
            $wo_dir_h = opendir($ps_dir);
            while (false != ($ws_file = readdir($wo_dir_h)))
            {
                if($ws_file!="." && $ws_file != "..") {
                    if (is_dir($ps_dir . '/' . $ws_file)) {
                        $this->_deleteDir($ps_dir . '/' . $ws_file);
                    }
                    else if (is_file($ps_dir . '/' . $ws_file)) {
                        unlink($ps_dir . '/' . $ws_file);
                    }
                }
            }
            closedir($wo_dir_h);
            return rmdir($ps_dir);
        }
        else {
            return true;
        }    	
    }     

}

$c = new file_archive54($argv);
$c->main();

