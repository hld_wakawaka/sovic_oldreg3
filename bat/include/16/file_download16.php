<?php

/**
 * ファイルダウンロード
 * 	アーカイブされた圧縮ファイルをダウンロードする
 */


include(dirname(__FILE__).'/../../../application/cnf/batch_include.php');
include(MODULE_DIR.'Download.class.php');

Class file_download16 {
	
	/**
	 * コンストラクタ
	 * @access	public
	 */
	function __construct() {

	}

	/**
	 * メイン処理
	 * @access	public
	 */
	function main() {
		
		//----------------------------
		//初期化
		//----------------------------
		$arrErr = "";
	
		//----------------------------
		// パラメータ取得
		//----------------------------		 		
		$this->arrForm = $_REQUEST;
		
		//----------------------------
		//画面タイトル設定
		//----------------------------
		$this->_title = "";
		

		//---------------------------
		//パラメータチェック
		//---------------------------
		$this->_prame_chk();
		
		
		//---------------------------
		//ハッシュ値のチェック
		//---------------------------
		$this->_hash_chk();
		
	
		//---------------------------
		//ダウロード
		//---------------------------
		
		//ファイル名
        $down_file = "file_archives".$this->arrForm["n"]."_".$this->arrForm["key"].".zip";
		$name = $down_file;
		
		//ディレクトリ名
        $file_dir = ROOT_DIR."download/form".$this->arrForm["id"];
		$down_file = $file_dir. "/" .$down_file;

		if(is_file($down_file)){
			
			//ダウンロード実行
			$wo_download = new Download();
			$wo_download->file($down_file, $name, "");			
			
		}
		else{
			$this->_err_msg("ファイルが存在しませんでした。");

		}

		exit;
	}
	
	

	/**
	 * パラメータチェック
	 * 	GETパラメータのチェック
	 * 
	 * @access private
	 */
	private function _prame_chk(){

		$msg = "パラメータが不正です。";
		
		//フォームID
		if(!isset($this->arrForm["id"])){
			$this->_err_msg($msg);
		}
		
		if($this->arrForm["id"] == ""){
			$this->_err_msg($msg);
		}
		
		if(!is_numeric($this->arrForm["id"])){
			$this->_err_msg($msg);
		}
		
		//ハッシュ
		if(!isset($this->arrForm["hash"])){
			$this->_err_msg($msg);
		}
		if($this->arrForm["hash"] == ""){
			$this->_err_msg($msg);
		}
		
		//key
		if(!isset($this->arrForm["key"])){
			$this->_err_msg($msg);
		}
		
		if($this->arrForm["key"] == ""){
			$this->_err_msg($msg);
		}
		
        //ファイル番号
        if(!isset($this->arrForm["n"])){
            $this->_err_msg($msg);
        }
        
        if($this->arrForm["n"] == ""){
            $this->_err_msg($msg);
        }        

        if(!($this->arrForm["n"] == 'word')){
        	$this->_err_msg($msg);
        }
	}

	/**
	 * ハッシュ値のチェック
	 *  パラメータのハッシュ値と合っているかチェック
	 * 
	 * @access private
	 */
	private function _hash_chk(){
		
		$db = New DbGeneral;
		
        $column = "password";
        $from = "form";
        $where[] = "form_id = ".$db->quote($this->arrForm["id"]);
        $where[] = "del_flg = 0";

        $rs = $db->getData($column, $from, $where, __FILE__, __LINE__);

		$ws_confirm_hash = md5(HASH_GENETATOR. $rs["password"]. $this->arrForm["key"]);		
	
		if($ws_confirm_hash != $this->arrForm["hash"]){
			$this->_err_msg("パラメータが不正です。");
		}
		
	}	 


	
	/**
	 * 該当データが存在しない場合の処理
	 * 	メッセージを表示する
	 */
	 private function _err_msg($msg){

		print $msg;
		exit;
	 }
}

/**
 * メイン処理開始
 **/

$c = new file_download16();
$c->main();

?>