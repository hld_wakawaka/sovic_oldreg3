<?php

include('../../application/cnf/include.php');


/**
 * フォーム管理者　各種文言設定
 *
 *
 * @subpackage Mng
 * @author salon
 *
 */
class word_replace extends ProcessBase {

	/**
	 * コンストラクタ
	 */
	function form_word(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){


		LoginMember::checkLoginRidirect();

		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_word_replace.html";


		// 親クラスに処理を任せる
		parent::main();

	}

}

/**
 * メイン処理開始
 **/

$c = new word_replace();
$c->main();







?>