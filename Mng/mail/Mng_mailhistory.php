<?php

include('../../application/cnf/include.php');
include_once('../function.php');

/**
 * 管理者メール送信履歴
 * 
 * @author salon
 *
 */
class mailhistory extends ProcessBase {


    /**
     * コンストラクタ
     */
    function mailhistory(){
        parent::ProcessBase();

        // ログインチェック
        LoginMember::checkLoginRidirect();

        // 初期化
        $this->form_id = $GLOBALS["userData"]["form_id"];
        $this->db      = new DbGeneral();
        $this->objErr  = new Validate();
        $this->arrErr  = array();
        $this->arrForm = $_REQUEST;
        $this->arrData = array();
        $this->count   = 0;
        $this->limit   = ROW_LIMIT;	//１ページ内の表示件数

        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_init.class.php');

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMngMailhistory")){
                $this->exClass->__constructMngMailhistory($this);
            }
        }
    }


    /**
     * メイン処理
     */	 
    function main(){
	    // テンプレート設定
	    $this->setTemplateMng($this);


		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));
		

		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;
	
		$eyear[$wk_year0] = $wk_year0;
		$eyear[$wk_year1] = $wk_year1;
		$eyear[$wk_year2] = $wk_year2;
	

		//月	
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;
	
		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		//時間
		for($i=1; $i<25; $i++){
			$wk_hour = sprintf('%02d', $i);
			$hour[$wk_hour] = $wk_hour;
		}

		for($i=1; $i<61; $i++){
			$wk_time = sprintf('%02d', $i);
			$time[$wk_time] = $wk_time;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）
		
		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）

		$this->assign("hour", $hour);				//時間
		$this->assign("timer", $time);				//時間


        //---------------------------------
        //アクション別処理
        //---------------------------------
        $this->mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        $actionName = $this->mode."Action";
        $exAction   = 'Mng_mailhistory_'.$actionName;

        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            $this->exClass->$exAction($this);
        }else{
            if(method_exists($this, $actionName)){
                $this->$actionName();
            }else{
                $this->defaultAction();
            }
        }


        if(count($this->arrErr) > 0){
            //エラーがあった場合はセッションクリア
            $GLOBALS["session"]->unsetVar('search_key');
        }

        // エントリー一覧取得
        $this->getMailHistory();

        $this->assign("count",  $this->count);
        $this->assign("arrErr", $this->arrErr);
        $this->assign("list",   $this->arrData);


		// 親クラスに処理を任せる
		parent::main();
	}


    function defaultAction(){
        // 検索パラメータ初期化
        $GLOBALS["session"]->unsetVar('search_key');
    }


    function searchAction(){
        $GLOBALS["session"]->setVar("search_key", $this->arrForm);

        //検索条件の妥当性チェック
        $this->arrErr = $this->check();

        //フォームパラメータにセッションの値をセット
        $this->arrForm = $GLOBALS["session"]->getVar("search_key");
    }


    function getMailHistory(){
        $page_no = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;

        if(count($this->arrErr) == 0){
            list($this->count, $this->arrData) = $this->_getList($page_no, $this->limit);

            //改ページリンク
            $this->pager = array("allcount"=>$this->count, "limit"=>$this->limit, "page"=>$page_no );
        }
    }


	/**
	 * 入力チェック
	 * 
	 * @access public
	 * @return object
	 */
	function check(){
		
		//妥当姓チェック（存在しない日付の場合はエラー）
		if(($this->arrForm["syear"] != "") && ($this->arrForm["smonth"] != "") && ($this->arrForm["sday"] != "") ){
			if(!checkdate((int)$this->arrForm["smonth"], (int)$this->arrForm["sday"], (int)$this->arrForm["syear"])){
				$this->objErr->addErr("配信日（開始）に存在しない日付が指定されています。", "search_sday");
			}
			
		}
		
		if($this->arrForm["eyear"] != "" && $this->arrForm["emonth"] != "" && $this->arrForm["eday"] != ""){
			
			if(!checkdate((int)$this->arrForm["emonth"], (int)$this->arrForm["eday"], (int)$this->arrForm["eyear"])){
				$this->objErr->addErr("配信日（終了）に存在しない日付が指定されています。", "search_eday");
			}
		}

		return $this->objErr->_err;
	}



    function setTemplateMng($obj) {
        $includeDir = TEMPLATES_DIR."Mng/mail/include/".$obj->form_id."/";

        //------------------------
        // 表示HTMLの設定
        //------------------------
        $this->_processTemplate = "Mng/mail/Mng_mailhistory.html";
        $this->_title = "管理者ページ";


        // 検索拡張
        $tmpfile = $obj->form_id."history_search.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("history_search_flg", 1);
            $obj->assign("tpl_history_search", $tmpfile);
        }


        // 送信履歴一覧拡張
        $tmpfile = $obj->form_id."mailhistory.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("mailhistoryt_inc_flg", 1);
            $obj->assign("tpl_mailhistory", $tmpfile);
        }


        $obj->assign("includeDir", $includeDir);
    }



    /**
     * メール送信履歴検索
     * 
     * @param int ページ番号
     * @param int limit
     */
    function _getList($page = 1, $limit){
        // SQL生成
        $column  = $this->buildColumn();
        $from    = $this->buildFrom();
        $where   = $this->buildWhere();
        $orderby = "rdate desc";
        $offset  = $limit * ($page - 1);


        // 全件数取得
        $count = $this->db->_getOne("COUNT(send_id)", $from, $where, __FILE__, __LINE__);
        if(!$count) return array("0", "");

        // 一覧取得
        $arrData = $this->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
        if(!$arrData) return array($count, "");

        if(is_object($this->exClass) && method_exists($this->exClass, "Mng_mailhistory_getList_after")){
            $this->exClass->Mng_mailhistory_getList_after($this, $count, $arrData);
        }

        return array($count, $arrData);
    }


    function buildColumn(){
        if(is_object($this->exClass) && method_exists($this->exClass, "Mng_mailhistory_buildColumn")){
            return $this->exClass->Mng_mailhistory_buildColumn($this);
        }

        return "*";
    }


    function buildFrom(){
        if(is_object($this->exClass) && method_exists($this->exClass, "Mng_mailhistory_buildFrom")){
            return $this->exClass->Mng_mailhistory_buildFrom($this);
        }

        return "mail_history";
    }


    function buildWhere(){
        $where = array();
        $where[] = "form_id = ".$this->db->quote($this->form_id);

        // セッションに保存した検索条件を取得
        $search_key = $GLOBALS["session"]->getVar("search_key"); 
        if($search_key != ""){
            //タイトルが指定されている場合
            if($search_key["mail_ttl"] != ""){
                $where[] = "subject like ".$this->db->quote("%".$search_key["mail_ttl"]."%");
            }


            //キーワードが指定されている場合
            if($search_key["keyword"] != ""){
                $where[] = "( subject like ".$this->db->quote("%".$search_key["keyword"]."%").") OR  (body like ".$this->db->quote("%".$search_key["keyword"]."%").")";
            }


            //開始日が指定されいる場合
            if($search_key["syear"] > 0){
                //月、日
                if($search_key["smonth"] == ""){
                    $search_key["smonth"] = "1";
                }

                if($search_key["sday"] == ""){
                    $search_key["sday"] = "1";
                }

                //日付フォーマット生成
                $search_sdate = sprintf("%04d", $search_key["syear"])."-".sprintf("%02d", $search_key["smonth"])."-".sprintf("%02d", $search_key["sday"]);
                $where[] = "rdate >= ".$this->db->quote($search_sdate);
            }


            //終了日が指定されている場合
            if($search_key["eyear"] > 0){
                //月、日
                if($search_key["emonth"] == ""){
                    $search_key["emonth"] = "12";
                }

                if($search_key["eday"] == ""){
                      $search_edate = date("Y-m-d", mktime(0, 0, 0, $search_key["emonth"]+1, 0, $search_key["eyear"]));
                }else{
                     $search_edate = sprintf("%04d", $search_key["eyear"])."-".sprintf("%02d", $search_key["emonth"])."-".sprintf("%02d", $search_key["eday"]);
                }

                $where[] = "rdate <= ".$this->db->quote($search_edate);
            }
        }

        if(is_object($this->exClass) && method_exists($this->exClass, "Mng_mailhistory_buildWhere_after")){
            $where = $this->exClass->Mng_mailhistory_buildWhere_after($this, $where, $search_key);
        }

        return $where;
    }





}

/**
 * メイン処理開始
 **/

$c = new mailhistory();
$c->main();







?>