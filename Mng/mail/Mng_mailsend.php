<?php

include('../../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include_once('../function.php');

/**
 * 管理者メール送信
 *
 * @author salon
 *
 */
class mailsend extends ProcessBase {


    /**
     * コンストラクタ
     */
    function mailsend(){
        parent::ProcessBase();

        // ログインチェック
        LoginMember::checkLoginRidirect();

        // 初期化
        $this->db = new DbGeneral();
        $this->onload  = "";
        $this->o_entry = new Entry();
        $this->arrForm = $_REQUEST;
        $this->arrErr  = array();
        $this->form_id = $GLOBALS["userData"]["form_id"];
        $_REQUEST["form_id"] = $this->form_id;
        $this->o_form   = new Form();
        $this->formWord = $this->o_form->getFormWord($this->form_id);
        $this->formdata = $this->o_form->formData;
        $this->o_itemini= new item_ini;

        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_init.class.php');
        Usr_initial::setFormIni($this);

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }
            // 一括メール送信用設定の読み込み
            if(method_exists($this->exClass, "__constructMngMailSend")){
                $this->exClass->__constructMngMailSend($this);
            }
        }
    }


    /**
     * メイン処理
     */
    function main(){
        // テンプレート設定
        $this->setTemplateMng($this);

        // アクセスチェック
        if(!Mng_function::checkParam($this->arrForm)){
            Error::showErrorPage("不正なアクセスです。");
            exit;
        }
        // 処理対象のエントリーIDを取得
        Mng_function::getEids($this->arrForm);
        // パラメータチェック
        if(!isset($this->arrForm["eid"]) || !count($this->arrForm["eid"]) > 0) {
            $this->complete("エントリーが指定されていません。");
        }

        // 一括メールの対象者を取得
        $this->getEntryList($this);


        //---------------------------------
        // アクション別処理
        //---------------------------------
        $this->mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        $actionName = $this->mode."Action";
        $exAction   = 'Mng_mailsend_'.$actionName;

        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            $this->exClass->$exAction($this);
        }else{
            if(method_exists($this, $actionName)){
                $this->$actionName();
            }else{
                $this->defaultAction();
            }
        }

        $this->assign("onload",  $this->onload);
        $this->assign("arrData", $this->arrData);
        $this->assign("arrErr",  $this->arrErr);

        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'Mng_mailsend_premain')){
            $this->exClass->Mng_mailsend_premain($this);
        }

        // 親クラスに処理を任せる
        parent::main();
	}


    function setTemplateMng($obj){
        $includeDir = TEMPLATES_DIR."Mng/mail/include/".$obj->form_id."/";

        // 表示HTMLの設定
        $obj->_processTemplate = "Mng/mail/Mng_mailsend.html";
        $obj->_title = "管理者ページ";

        // 配信内容拡張
        $tmpfile = $obj->form_id."mailbody.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("mailbody_inc_flg", 1);
            $obj->assign("tpl_mailbody", $tmpfile);
        }

        // 配信先一覧拡張
        $tmpfile = $obj->form_id."mailsend.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("mailsend_inc_flg", 1);
            $obj->assign("tpl_mailsend", $tmpfile);
        }

        // 配信先一覧拡張 # 確認画面
        $tmpfile = $obj->form_id."mailsend_confirm.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("mailsend_confirm_inc_flg", 1);
            $obj->assign("tpl_mailsend_confirm", $tmpfile);
        }

        $obj->assign("includeDir", $includeDir);
    }


    // 一括メールの対象者を取得
    function getEntryList($obj){
        $obj->arrData = $obj->o_entry->getListFromId($obj->arrForm["eid"]);
    }


    function backAction(){}

    function defaultAction(){}

    function completeAction(){
        $this->_title = "メール送信完了";

        // 一括メール送信
        $this->lfSendMail();
        // 送信履歴を残す
        $this->sevehistory();

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

        $this->complete("メールを送信しました。");
    }


    function confirmAction(){
        if($this->arrForm["mail_ttl"] == "")  $this->arrErr[] = "件名を入力してください。";
        if($this->arrForm["mail_body"] == "") $this->arrErr[] = "本文を入力してください。";
        if(count($this->arrErr) > 0) return;

        $this->_processTemplate = "Mng/mail/Mng_mailsend_confirm.html";
    }


    function complete($msg){
        $this->assign("msg", $msg);
        $this->_processTemplate = "Mng/Mng_complete.html";
        parent::main();
        exit;
    }


	function lfSendMail() {
	    $exAction = "Mng_mailsend_".__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            return $this->exClass->$exAction($this);
        }

		mb_internal_encoding("utf-8");
		mb_language("japanese");

		$this->count = 0;

		foreach($this->arrData as $key=>$val) {
			$this->arrData[$key]["send"] = "0";

			$to = $val["edata25"];
			if($to == "") continue;

			$subject = $this->arrForm["mail_ttl"];
			$body    = $this->arrForm["mail_body"];
			$body    = $this->replaceStr($body, $val);
            $body    =  str_replace(array("\r\n", "\r"), "\n", $body);


			//送信元
			if( $this->formWord["word13"] != "" ){
				$ps_fromname = mb_convert_encoding($this->formWord["word13"], "ISO-2022-JP", "utf-8");
				$ps_from = mb_encode_mimeheader($ps_fromname)."<".$GLOBALS["userData"]["form_mail"]."> ";
	        }
	        else{
	        	$ps_from = $GLOBALS["userData"]["form_mail"];
	        }

			$head  = "From: ".$ps_from;
			$head .= "\n";
			$head .= "Cc: ".$GLOBALS["userData"]["form_mail"];
			//$head .= "\n";
			//$head .= "Bcc: ".SYSTEM_MAIL;

			$rs = mb_send_mail($to, $subject, $body, $head);

			if($rs) {
				$this->arrData[$key]["send"] = "1";
				$this->count++;
			}
			$this->arrData[$key]["send_date"] = date("Y-m-d H:i:s");
		}
    }


    // 送信履歴を残す
    function sevehistory(){
	    $exAction = "Mng_mailsend_".__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            return $this->exClass->$exAction($this);
        }

        $objDb = new DbGeneral();
        $objDb->begin();

        // 履歴を残す
        $param = array();
        $param["form_id"]  = $this->form_id;
        $param["subject"]  = $this->arrForm["mail_ttl"];
        $param["body"]     = $this->arrForm["mail_body"];
        $param["send_num"] = count($this->arrData);
        $param["success"]  = $this->count;
        $rs = $objDb->insert("mail_history", $param, __FILE__, __LINE__);

        if(!$rs) {
            $objDb->rollback();
            $this->complete("履歴の登録に失敗しました。");
        }

        $where = "form_id = ".$objDb->quote($this->form_id);
        $send_id = $objDb->_getOne("max(send_id)", "mail_history", $where, __FILE__, __LINE__);

        if(!$send_id) {
            $objDb->rollback();
            $this->complete("送信番号の取得に失敗しました。");
        }

        unset($param);
        unset($where);

        foreach($this->arrData as $val) {
            $param = array();
            $param["send_id"]     = $send_id;
            $param["eid"]         = $val["eid"];
            $param["mailaddress"] = $val["edata25"];
            $param["result"]      = $val["send"];
            $param["send_date"]   = $val["send_date"];
            $rs = $objDb->insert("mail_sendentry", $param, __FILE__, __LINE__);
            if(!$rs) {
                $objDb->rollback();
                $this->complete("履歴の登録に失敗しました。");
            }
            unset($param);
        }

        $objDb->commit();
    }


    /*
     * Sysと同じ置換文字列
     * @see Usr_entry#makeMailBody2
     */
    function replaceStr($body, $arrForm){
        if(is_object($this->exClass) && method_exists($this->exClass, 'Mng_mailsend_replaceStr_before')){
            $this->exClass->Mng_mailsend_replaceStr_before($this, $body, $arrForm);
        }

        $objDb = new DbGeneral();

        //--------------------------------
        //置換文字列
        //--------------------------------
        //文言設定がある場合
        $wa_replaceStr= array();
        $wa_replaceStr["ID"] = $arrForm['e_user_id'];
        $wa_replaceStr["SEI"] = $arrForm["edata1"];
        $wa_replaceStr["MEI"] = $arrForm["edata2"];
        $wa_replaceStr["MIDDLE"]= $arrForm["edata7"];

        $wk_title = ($arrForm["edata57"] == "") ? "" : $GLOBALS["titleList"][$arrForm["edata57"]];
        $wa_replaceStr["TITLE"]= $wk_title;

        $wa_replaceStr["SECTION"]= $arrForm["edata10"];

//        if($this->formdata["type"] != "3"){
            if($this->formdata["edit_flg"] == "1"){
                $wa_replaceStr["PASSWORD"]= $arrForm['e_user_passwd'];
            }
//        }

        $wk_day = Mng_function::getEntryDate($objDb, $arrForm['eid']);

        $wa_replaceStr["INSERT_DAY"]= $wk_day["insday"];
        $wa_replaceStr["UPDATE_DAY"]= $wk_day["upday"];


        if(is_object($this->exClass) && method_exists($this->exClass, 'Mng_mailsend_replaceStr_after')){
            $this->exClass->Mng_mailsend_replaceStr_after($this, $body, $arrForm, $wa_replaceStr);
        }

        return Mng_function::wordReplace($body, $wa_replaceStr);
    }

}

/**
 * メイン処理開始
 **/

$c = new mailsend();
$c->main();







?>