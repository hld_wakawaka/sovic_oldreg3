<?php

include('../../application/cnf/include.php');
include_once(MODULE_DIR.'custom/Entry.class.php');
include_once('../function.php');

/**
 * 管理者メール送信詳細
 * 
 * @author salon
 *
 */
class maildetail extends ProcessBase {


    /**
     * コンストラクタ
     */
    function maildetail(){
        parent::ProcessBase();

        // ログインチェック
        LoginMember::checkLoginRidirect();

        //初期化
        $this->form_id = $GLOBALS["userData"]["form_id"];
        $this->db      = new DbGeneral;
        $this->o_entry = new Entry();
        $this->limit   = ROW_LIMIT;	//１ページ内の表示件数
        $this->send_id = isset($_REQUEST["send_id"]) ? $_REQUEST["send_id"] : "";
        $this->page_no = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;   // ページ番号
        $this->arrData = array();

        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_init.class.php');

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMngMaildetail")){
                $this->exClass->__constructMngMaildetail($this);
            }
        }
    }


	/**
	 * メイン処理
	 */	 
	function main(){
	    // テンプレート設定
	    $this->setTemplateMng($this);


		if($this->send_id == ""){
			$this->complete("処理に必要なパラメータが存在しません。");
		}


		//---------------------------------
		//メール情報取得
		//---------------------------------
		$this->arrData = $this->_getMail();
		if(!$this->arrData){
			$this->complete("対象データが存在しません。");
		}


        //------------------------------------
        //送信対象情報取得
        //------------------------------------
        list($all_count, $send_list) = $this->_getSendList($this->page_no, $this->limit);
        $this->assign("send_list", $send_list);

        //改ページリンク
        $this->pager = array("allcount"=>$all_count, "limit"=>$this->limit, "page"=>$this->page_no);


        //取得件数
        $this->assign("count",   $all_count);
        $this->assign("send_id", $this->send_id);
        $this->assign("va_mail", $this->arrData);


        // 親クラスに処理を任せる
        parent::main();
    }


    function complete($msg){
        $this->assign("va_mail", $msg);
        $this->_processTemplate = "Mng/mail/Mng_complete.html";
        parent::main();
    }


	/**
	 * 送信メール情報取得
	 * 	送信メールの内容を取得する
	 */
	function _getMail(){
		
		//SQL
    	$column = "*";
    	$from = "mail_history";
    	$where[] = "send_id = ".$this->db->quote($this->send_id);
 
    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);
    	
    	return $rs;		
		
	}	 

	/**
	 * メール送信詳細情報取得
	 * 	送信したユーザの一覧情報を取得する
	 * 
	 */
	function _getSendList($page = 1, $limit){
		
		$e_table = $this->o_entry->getTable($GLOBALS["userData"]["type"]);
		
		//SQL
    	$column = "a.send_id, a.mailaddress, a.result, b.e_user_id, b.edata1, b.edata2, b.edata3, b.edata4, b.edata5, b.edata6";
    	$from = "mail_sendentry a inner join ".$e_table." b on (a.eid = b.eid and b.form_id = ".$this->form_id.")";
    	$where[] = "a.send_id = ".$this->db->quote($this->send_id);
 		$orderby = "a.eid";


		//全体件数取得
		$count = $this->db->_getOne("COUNT(a.send_id)", $from, $where, __FILE__, __LINE__);	
		
		if(!$count){
			return array("0", "");
		}
		

		//一覧データ取得
		
		$offset = $limit * ($page - 1);
		
		$rs = $this->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
    	if(!$rs) {
    		return array($count, "");
    	}		
		
		return array($count, $rs);
	}	 


    function setTemplateMng($obj){
        $includeDir = TEMPLATES_DIR."Mng/mail/include/".$obj->form_id."/";

        //------------------------
        // 表示HTMLの設定
        //------------------------
        $this->_processTemplate = "Mng/mail/Mng_maildetail.html";
        $this->_title = "管理者ページ";

        // 配信先一覧拡張
        $tmpfile = $obj->form_id."maildetail.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("maildetail_inc_flg", 1);
            $obj->assign("tpl_maildetail", $tmpfile);
        }

        $obj->assign("includeDir", $includeDir);
    }
}

/**
 * メイン処理開始
 **/

$c = new maildetail();
$c->main();







?>