<?php

include('../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once('./function.php');
include_once('./entry/Mng_entry_csv.php');
include_once('./entry/Mng_payment_csv.php');
include_once('./entry/Mng_all_csv.php');
include(MODULE_DIR.'Download.class.php');
include_once('../../tgMdk/crypt.class.php');

define("DAY_START", 0);    // 開始日モード
define("DAY_END"  , 1);    // 終了日モード


/**
 * 管理者TOP
 *
 * @author salon
 *
 */
class index extends ProcessBase {

    public $limit = 10;
    public $searchkey = array('s_entry_no', 'e_entry_no', 'user_name', 'user_name_kana', 'status1', 'status2', 'status3', 'status4','syear', 'smonth', 'sday', 'eyear', 'emonth', 'eday','payment_method', 'payment_status', "entry_no", "country",'upd_syear', 'upd_smonth', 'upd_sday', 'upd_eyear', 'upd_emonth', 'upd_eday', "country_list", "limit", "page", "entry_way");
    public $sesskey = "mng_formlist";

    // 表示件数
    public $arrLimit = array(10, 50, 100, 500);

    public $sort_name = array(1 => "更新日", 2 => "登録番号");
    public $sort      = array(1 => "降順", 2 => "昇順");

    /**
     * コンストラクタ
     */
    function index(){
        parent::ProcessBase();

        // ログインチェック
        LoginMember::checkLoginRidirect();

        // 初期化
        $this->db        = new DbGeneral;
        $this->o_entry = new Entry;
        $this->arrForm = $_REQUEST;
        $this->arrErr  = array();
        $this->onload  = "";
        $this->formdata= $GLOBALS["userData"];
        $this->form_id = $this->formdata["form_id"];
        $this->objErr  = new Validate();
        $this->download= new Download();
        $this->db      = new DbGeneral();
        $this->o_form  = new Form;
        $this->o_itemini = new item_ini;
        $this->arrData   = array();

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        Usr_initial::setFormIni($this);
        Usr_initial::setLanguage($this, $GLOBALS["userData"]['lang']);

        $menu = Mng_function::makeMenu();
        $this->assign("va_menu",   $menu);
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }
        }
    }


	/**
	 * メイン処理
	 */
	function main(){
	    // テンプレート設定
	    $this->setTemplateMng($this);


		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));


		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;

		$eyear[$wk_year0] = $wk_year0;
		$eyear[$wk_year1] = $wk_year1;
		$eyear[$wk_year2] = $wk_year2;

		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;



		//月
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;

		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）

		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）

		//ソート順　リストボックス
		$this->assign("sort_name", $this->sort_name);
		$this->assign("sort"     , $this->sort);

		//-----------------------------
		$this->assign("arrLimit", $this->arrLimit);


		//-----------------------------
		//フォーム項目取得
		//-----------------------------
		$this->formitem = $this->o_entry->getFormItem($this->db, $this->form_id);
		if(!$this->formitem){
			Error::showErrorPage("フォーム項目情報の取得に失敗しました。");
		}

		$paper = "";	//論文フラグ
        foreach($this->formitem as $data){
            $wk_item[$data["item_id"]] = $data;
        }
        // フォーム項目の名前
        $formitemview = array();
        $formitemview["60"] = $wk_item["60"]["item_view"];
        $formitemview["114"] = $wk_item["114"]["item_view"];

        // ファイルアップロードダウンロード　添付ファイルフィールドの有無
        $upload_file_field = array();
        $upload_file_field["1"] = $wk_item["51"]["item_view"];
        $upload_file_field["2"] = $wk_item["52"]["item_view"];
        $upload_file_field["3"] = $wk_item["53"]["item_view"];

        $this->assign("formitemview", $formitemview);
        $this->assign("upload_file_field", $upload_file_field);


        //---------------------------------
        // アクション別処理
        //---------------------------------
        $this->mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        $actionName = $this->mode."Action";
        $exAction   = 'Mng_index_'.$actionName;

        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            $this->exClass->$exAction($this);
        }else{
            if(method_exists($this, $actionName)){
                $this->$actionName();
            }else{
                $this->defaultAction();
            }
        }



		$this->getSearchkey();
		if(count($this->arrErr) > 0){
			//エラーがあった場合はセッションクリア
			$GLOBALS["session"]->unsetVar($this->sesskey);
		}


        // エントリー一覧取得
        $this->getEntryList();


		if(!isset($this->arrForm["payment_method"])) $this->arrForm["payment_method"] = array();
		if(!isset($this->arrForm["payment_status"])) $this->arrForm["payment_status"] = array();
		if(!isset($this->arrForm["chg_status"])) $this->arrForm["chg_status"] = "";

        // 
        $form = array();
		$form["payment_method"] = SmartyForm::createRadioChecked("payment_method", $GLOBALS["method_J"], $this->arrForm["payment_method"], "checkbox", "form");
		$form["payment_status"] = SmartyForm::createRadioChecked("payment_status", $GLOBALS["paymentstatusList"], $this->arrForm["payment_status"], "checkbox", "form");
		$form["chg_status"] = SmartyForm::createCombo("chg_status", $GLOBALS["paymentstatusList"], $this->arrForm["chg_status"], "form", "↓選択");

		$this->assign("form", $form);
		$this->assign("arrData", $this->arrData);
		$this->assign("arrErr",  $this->arrErr);
		$this->assign("limit",   $this->limit);
		$this->assign("onload",  $this->onload);
        $this->assign("arrItemData", $this->arrItemData);


        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_index_premain')){
            $this->exClass->mng_index_premain($this);
        }

		// 親クラスに処理を任せる
		parent::main();
	}


    // ExtendからProcessBaseのmainを呼ぶため設置
    function basemain() {
        parent::main();
        exit;
    }


    function getEntryList(){
		$this->arrForm["page"] = (isset($this->arrForm["page"]) && $this->arrForm["page"] != "")
		                       ? $this->arrForm["page"]
		                       : 1;
		// ソートの初期値は登録番号の降順
        if(!isset($this->arrForm['sort']))      $this->arrForm['sort']      = 1;
        if(!isset($this->arrForm['sort_name'])) $this->arrForm['sort_name'] = 2;
        if(is_null($this->arrForm['limit']))    $this->arrForm['limit']     = $this->arrLimit[0];
        if(!isset($this->arrForm['limit']))     $this->arrForm['limit']     = $this->arrLimit[0];

		//検索条件にエラーが無い場合に一覧情報を取得
		if(count($this->arrErr) ==  0){
			list($this->arrData["count"], $this->arrData["list"]) = Mng_function::getListEntry($this, $this->arrForm["page"], $this->arrForm['limit']);
			$this->pager = array("allcount"=>$this->arrData["count"], "limit"=>$this->arrForm['limit'], "page"=>$this->arrForm["page"]);
		}
		else{
			$this->arrData["count"] = 0;
		}
    }


    /* アクション # デフォルト */
    function defaultAction(){
        $this->clearSearchkey();
        return;
    }


    /* アクション # 検索ボタン */
    function searchAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();

        $this->setSearchkey();

        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid($this->arrForm['limit']);
        $this->assign("arrEid", $arrEid);
        return;
    }


    /* アクション # クリアボタン */
    function clearAction(){
        $this->clearSearchkey();
        return;
    }


    /* アクション # ファイル一括ダウンロード */
    function file_downloadAction(){
        if(!isset($_POST["file_num"]) || $_POST["file_num"] == ""){
            $this->objErr->addErr("ファイルダウンロードに必要なパラメータが不足しています。", "file_num");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $cmd = "/usr/bin/php ".ROOT_DIR."bat/bat_file_archive.php ".$this->form_id." ".$_POST["file_num"]." > /dev/null &";
        system($cmd);

        $msg = $GLOBALS["userData"]["form_mail"]."へ完了メールを送信しました。";
        $this->onload = "window.alert('".$msg."');";
        return;
    }


    /* アクション # エントリーCSVダウンロード */
    function entry_csvAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();
        $this->setSearchkey();

        if(count($this->arrErr) != 0) return;

        $wo_csv = new entry_csv;

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $this->objErr->addErr($csv_data, "csv_data");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $this->download->csv($csv_data, "entry_".date('Ymdhis').".csv");
        exit;
    }


    /* アクション # 決済情報CSVダウンロード */
    function payment_csvAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();
        $this->setSearchkey();

        if(count($this->arrErr) != 0) return;

        $wo_csv = new payment_csv;

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $this->objErr->addErr($csv_data, "csv_data");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $this->download->csv($csv_data, "payment_".date('Ymdhis').".csv");
        exit;
    }


    /* アクション # エントリー+決済情報CSVダウンロード */
    function all_csvAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();
        $this->setSearchkey();

        if(count($this->arrErr) != 0) return;

        $wo_csv = new all_csv;

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $this->objErr->addErr($csv_data, "csv_data");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $this->download->csv($csv_data, "all_".date('Ymdhis').".csv");
        exit;
    }


    /* アクション # 戻るボタン */
    function backAction(){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();
        $this->assign("arrEid", $arrEid);
        return;
    }



    /**
     * 入力チェック
     *
     * @access public
     * @return object
     */
    function check(){
        $arrKeys = array(
                        array(array(    "syear",     "smonth",     "sday")  , array(    "eyear",    "emonth",      "eday"))
                      , array(array("upd_syear", "upd_smonth", "upd_sday")  , array("upd_eyear", "upd_emonth", "upd_eday"))
                   );
        $arrNames= array(
                        array("受付期間（開始）", "受付期間（終了）")
                      , array("更新期間（開始）", "更新期間（終了）")
                   );
        $arrKey  = array(
                        array(    "search_sday",     "search_eday")
                      , array("search_upd_sday", "search_upd_eday")
                   );

        foreach($arrKeys as $_key => $_arrKeys){
            $errors = array();
            foreach($_arrKeys as $mode => $keys){
                // 入力日付のチェック
                $name = $arrNames[$_key][$mode];
                $key  = $arrKey[$_key][$mode];
                $errors[] = $this->check_date($this->objErr, $this->arrForm, $keys, $name, $key, $mode);
            }

            // 開始日、終了日の妥当性チェック
            if($errors[0] && $errors[1]){
                // 開始、終了どちらも入力あり
                $vals = array();
                foreach($arrKey[$_key] as $key){
                    $vals[] = $this->arrForm[$key];
                }
                $names = array();
                foreach($arrNames[$_key] as $name){
                    $names[] = $name;
                }
                if(strlen($vals[0])> 0 && strlen($vals[1]) > 0){
                    if(!$this->objErr->isDateAgo($vals[0], $vals[1])) {
                        $this->objErr->addErr("{$names[1]}は、{$names[0]}よりも未来に設定して下さい。", $key1);
                    }
                }
            }
        }


        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_index_check')){
            $this->exClass->mng_index_check($this);
        }

        return $this->objErr->_err;
    }



    /**
     * 日付の開始または終了に関する共通エラーチェック用の関数
     *      入力チェックokであれば$keyに年月日にした値を格納
     * 
     * @param  $objErr  object  Validate
     * @param  $arrForm array   入力値の連想配列
     * @param  $keys    array   年月日それぞれ入力値に対するのキー配列
     * @param  $name    string  項目名
     * @param  $key     string  キー配列の総合キー
     * @param  $mode    integer 処理モード(0;開始日, 1;終了日)
     * @return boole    false:エラー発生
     * 
     */
    function check_date($objErr, &$arrForm, $keys, $name, $key, $mode=DAY_START){
        $error = true; // エラーフラグ

        if($objErr->isInputwhich($arrForm, $keys)){
            // 年入力
            if(strlen($arrForm[$keys[0]]) > 0){
                // 開始日モード:月日が未入力の場合は1にする
                if($mode==0){
                    if(strlen($arrForm[$keys[1]]) == 0) $arrForm[$keys[1]] = 1;
                    if(strlen($arrForm[$keys[2]]) == 0) $arrForm[$keys[2]] = 1;
                }
                // 終了日モード:月日が未入力の場合は月末の最終日にする
                if($mode==1){
                    if(strlen($arrForm[$keys[1]]) == 0) $arrForm[$keys[1]] = 12;
                    if(strlen($arrForm[$keys[2]]) == 0) $arrForm[$keys[2]] = date("t", mktime(0, 0, 0, 12, 1, $arrForm[$keys[0]]));
                }
                // 検索条件を更新
                $_REQUEST[$keys[1]] = $arrForm[$keys[1]];
                $_REQUEST[$keys[2]] = $arrForm[$keys[2]];
            }
            // 年月日の入力不備
            if(!$objErr->isInputAll($arrForm, $keys)){
                $objErr->addErr($name."の年月日すべてを指定してください。", $key);
                $error = false;

            // 年月日を全て入力
            }else{
                //妥当姓チェック（存在しない日付の場合はエラー）
                $year = $arrForm[$keys[0]];
                $month= $arrForm[$keys[1]];
                $day  = $arrForm[$keys[2]];

                // 日付の妥当性チェック
                if(!checkdate((int)$month, (int)$day, (int)$year)){
                    $objErr->addErr($name."に存在しない日付が指定されています。", $key);
                    $error = false;
                // 入力ok!!
                }else{
                    $arrForm[$key] = GeneralFnc::convTimestamp($year, $month, $day);
                    $_REQUEST[$key]= $arrForm[$key]; // 検索対策
                }
            }
        }
        return $error;
    }


	function setSearchkey() {

		$GLOBALS["session"]->unsetVar($this->sesskey);

		foreach($this->searchkey as $val) {
			if(isset($_REQUEST[$val])) {
				if(is_array($_REQUEST[$val])) {
					$param[$val] = $_REQUEST[$val];
				} else {
					$param[$val] = trim($_REQUEST[$val]);
				}
			} else {
				$param[$val] = "";
			}
		}
		if($param['limit'] == ""){
		    $param['limit'] = $this->limit;
		}

		$GLOBALS["session"]->setVar($this->sesskey, $param);

	}

	function clearSearchkey() {

		$GLOBALS["session"]->unsetVar($this->sesskey);
        $GLOBALS["session"]->unsetVar("eid");

		foreach($this->searchkey as $key) {
			$this->arrForm[$key] = "";
		}
	    $this->arrForm['limit'] = $this->limit;
	}

	function getSearchkey() {

		if(!$GLOBALS["session"]->issetVar($this->sesskey)) return;

		$sessvar = $GLOBALS["session"]->getVar($this->sesskey);

		foreach($this->searchkey as $key) {
			if(isset($sessvar[$key])) {
				$this->arrForm[$key] = $sessvar[$key];
			} else {
				$this->arrForm[$key] = "";
			}
		}
		if($this->arrForm['limit'] == ""){
		    $this->arrForm['limit'] = $this->limit;
		}
	}


    function setTemplateMng($obj, $includeDir="") {
        if(strlen($includeDir) == 0){
            $includeDir = TEMPLATES_DIR."Mng/entry/include/".$obj->form_id."/";
        }

        //------------------------
        // 表示HTMLの設定
        //------------------------
        $this->_processTemplate = "Mng/entry/Mng_formlist.html";
        $this->_title  = "管理者ページ";


        // 検索拡張
        $tmpfile = $obj->form_id."search.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("search_inc_flg", 1);
            $obj->assign("tpl_search", $tmpfile);
        }


        // 一括処理拡張
        $tmpfile = $obj->form_id."operation.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("operation_inc_flg", 1);
            $obj->assign("tpl_operation", $tmpfile);
        }


        // エントリー一覧拡張
        $tmpfile = $obj->form_id."entrylist.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("entrylist_inc_flg", 1);
            $obj->assign("tpl_entrylist", $tmpfile);
        }


        $obj->assign("includeDir", $includeDir);
    }


}

/**
 * メイン処理開始
 **/

$c = new index();
$c->main();







?>