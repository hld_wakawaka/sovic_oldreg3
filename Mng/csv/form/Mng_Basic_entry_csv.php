<?php

include('../../../application/cnf/include.php');
include('../../../application/module/custom/Entry.class.php');
include_once('../../../application/module/custom/Form.class.php');
include_once('../../function.php');
include_once('../../entry/Mng_entry_csv.php');
include_once('../../entry/Mng_payment_csv.php');
include_once('../../entry/Mng_all_csv.php');
include('../../../application/module/Download.class.php');
include_once('../../../../tgMdk/crypt.class.php');


/**
 * 管理者TOP
 *
 * @author salon
 *
 */
class Mng_Basic_entry_csv extends ProcessBase {

    /**
     * コンストラクタ
     */
    function __construct(){
        parent::ProcessBase();

        // 表示HTMLの設定
        $this->_title = "エントリーCSVダウンロード";
        $this->_processTemplate = "Mng/csv/Mng_csvBasic_entry_csv.html";
    }


    /**
     * メイン処理
     */
    function main(){
        $form_id = $this->getFormId();
        $isLogin = $this->doLogin($form_id);
        if(!$isLogin){
            Error::showErrorPage("ワンタイムログインに失敗しました。");
        }

        //--------------------------------
        //初期化
        //--------------------------------
        $arrErr= array();

        $objEntry = new Entry;
        $this->objErr = new Validate;
        $this->download = new Download;
        $this->db = new DbGeneral;
        $this->arrForm = $_REQUEST;

        //-----------------------------
        //フォーム項目取得
        //-----------------------------
        $formitem = $objEntry->getFormItem($this->db, $form_id);
        if(!$formitem){
            Error::showErrorPage("フォーム項目情報の取得に失敗しました。");
        }


        // フォームの初期値を背亭する
        $this->setRequestForm($form_id);


        //---------------------------------
        //検索条件　日付の妥当性チェック
        //    存在しない日付の場合はエラー
        //---------------------------------
        if(count($arrErr) == 0){
            $wo_csv = new entry_csv;

            list($wb_ret, $csv_data) = $wo_csv->main();
            if(!$wb_ret){
                $this->objErr->addErr($csv_data, "csv_data");
                $arrErr = $this->objErr->_err;

            }
            else{
                $this->download->csv($csv_data, "entry_".date('Ymdhis').".csv");
                exit;
            }
        }


        $this->assign("arrErr", $arrErr);

        // 親クラスに処理を任せる
        parent::main();
    }



    /**
     * デストラクタ
     */
    function doLogout(){
        // CSVダウンロードだけの一時的なログインなので
        // 必ずログアウトさせる
//        LoginMember::doLogout();
		$GLOBALS["session"]->unsetVar("isLogin");
		$GLOBALS["session"]->unsetVar("userData");
        $GLOBALS["session"]->unsetVar("isBasic");
    }



    /**
     * csvダウンロード設定ファイルからform_idを取得する
     * 
     */
    function getFormId(){
        $form_id = NULL;

        // 設定ファイルを読み込む
        $filePath = "./form_id";
        if(file_exists($filePath)){
            if(!($fp = fopen($filePath, "r" ))) {
                return NULL;
            }

            //ファイル読み込み
            $temp = "";
            while(!feof($fp)) {
               $temp .= fgets($fp);
            }

            //ファイルを閉じる
            fclose ($fp);
            $form_id = $temp;
            $form_id = str_replace(array("\r\n", "\r"), "", $form_id);
        }

        return trim($form_id);
    }



    function setRequestForm($form_id){
        $_REQUEST['s_entry_no']     = "";
        $_REQUEST['e_entry_no']     = "";

        $_REQUEST['user_name']      = "";
        $_REQUEST['user_name_kana'] = "";

        $_REQUEST['syear']          = "";
        $_REQUEST['smonth']         = "";
        $_REQUEST['sday']           = "";

        $_REQUEST['eyear']          = "";
        $_REQUEST['emonth']         = "";
        $_REQUEST['eday']           = "";

        $_REQUEST['upd_syear']      = "";
        $_REQUEST['upd_smonth']     = "";
        $_REQUEST['upd_sday']       = "";

        $_REQUEST['upd_eyear']      = "";
        $_REQUEST['upd_emonth']     = "";
        $_REQUEST['upd_eday']       = "";

        $_REQUEST['sort_name']      = 1;
        $_REQUEST['sort']           = 1;

        $_REQUEST['form_id']        = $form_id;
    }



    /**
     * 対象のformへログイン処理をする
     * 
     */
    function doLogin($form_id=NULL){
        $db = new DbGeneral();
        $where[] = "form_id = ".$db->quote($form_id);
        $memberData = $db->getData("*", "v_user", $where, __FILE__, __LINE__);

        if($memberData) {
            $GLOBALS["session"]->setVar("isLogin",  true);
            $GLOBALS["session"]->setVar("userData", $memberData);
            $GLOBALS["session"]->setVar("isBasic",  true);
            
            $GLOBALS["userData"] = $memberData;
            return true;
        }
        return false;
    }



    function _chkTerm(){
//        include_once(MODULE_DIR.'entry_ex/Usr_check.class.php');
//        return Usr_Check::_chkTerm($this);
        return true;
    }

}


/**
 * メイン処理開始
 **/
$c = new Mng_Basic_entry_csv();
register_shutdown_function(array($c, 'doLogout'));
$c->main();







?>