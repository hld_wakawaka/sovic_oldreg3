<?php

include('../../application/cnf/include.php');
include_once('../function.php');

/**
 * 管理者メール送信履歴
 * 
 * @author salon
 *
 */
class csvBasic extends ProcessBase {

    var $form_id;

    // 基本パス
    var $base_dir;
    // フォーム専用パス
    var $form_dir;

    // CSVダウンロード用URL
    var $CSVDL_URL;

    /**
     * コンストラクタ
     */
    function __construct(){
        parent::ProcessBase();

        //-------------------------------
        // ログインチェック
        //-------------------------------
        LoginMember::checkLoginRidirect();

        // アクセス拒否
        if($GLOBALS["userData"]['use_csvbasic'] != 0){
            Error::showErrorPage("CSVダウンロードBasic認証設定は許可されていません。");
        }


        // 表示HTMLの設定
        $this->_processTemplate = "Mng/csv/Mng_csvBasic.html";
        $this->_title = "管理者ページ";


        //-------------------------------
        //ログイン者情報
        //-------------------------------
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        //-------------------------------
        //管理者メニュー
        //-------------------------------
        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);


        //-------------------------------
        //インスタンス生成
        //-------------------------------
        $this->db = new DbGeneral;
        $this->objErr = New Validate;


        $this->form_id = $GLOBALS["userData"]['form_id'];

        // 基本パス
        $this->base_dir = ROOT_DIR."Mng/csv/form/";
        // フォーム専用パス
        $this->form_dir = ROOT_DIR."Mng/csv/form". $this->form_id ."/";

        $this->CSVDL_URL = MNG_URL."csv/form". $this->form_id ."/Mng_Basic_entry_csv.php";
    }


    /**
     * メイン処理
     */
    function main(){
        //----------------------
        //アクション取得
        //----------------------
        $ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

        // 入力
        if($ws_action == "input"){
            $this->arrForm = GeneralFnc::convertParam($this->_init(), $_POST);
            $arrErr        = $this->_check($this->arrForm);

            if(count($arrErr) == 0){
                // ファイル作成
                $this->createCSVDir();

                // 設定ファイルの書き込み
                $this->writeCSVFile($this->arrForm);
            }

        // 削除
        }elseif($ws_action == "delete"){
            $this->arrForm = GeneralFnc::convertParam($this->_init(), $_POST);
            $id = isset($_POST['id']) ? $_POST['id'] : "";
            if($id != ""){
                unset($this->arrForm['user'][$id]);
                unset($this->arrForm['pass'][$id]);

                // 設定ファイルの書き込み
                $this->writeCSVFile($this->arrForm);
            }

            // 登録数が0 -> CSVダウンロードできない
            // 認証可能なユーザがない状態で認証がかかっている状態
//            if(count($this->arrForm['user']) == 0){
//            }
        }

        // 初期
        elseif($ws_action == ""){
            // htpasswd読み込み
            $htpass_dir = $this->form_dir. ".htpasswd";
            if(file_exists($htpass_dir)){
                $htpasswd = GeneralFnc::readFile($htpass_dir);
                $htpasswd = explode("\n", str_replace(array("\r\n", "\r"), "\n", $htpasswd));

                foreach($htpasswd as $_key => $usertopass){
                    if(strlen($usertopass) == 0) continue;

                    $arrData = explode(":", $usertopass);
                    $arrForm['user'][$_key] = $arrData[0];
                    $arrForm['pass'][$_key] = $arrData[1];
                }
                $this->arrForm = $arrForm;
            }
        }


        $this->assign("isCSV",     file_exists($this->form_dir));
        $this->assign("CSVDL_URL", $this->CSVDL_URL);
        $this->assign("arrErr",    $arrErr);


        // 親クラスに処理を任せる
        parent::main();
    }



    function _init(){
        $key   = array();
        // 入力
        $key[] = array("username", "ユーザ名",   array(),   array(),    "aKV",  0);
        $key[] = array("password", "パスワード", array(),   array(),    "aKV",  0);

        // 登録
        $key[] = array("user", "ユーザ名",   array(),   array(),    "aKV",  0);
        $key[] = array("pass", "パスワード", array(),   array(),    "aKV",  0);

        return $key;
    }



    function _check(&$arrForm=array()){
        $objErr = New Validate;

        $user = $arrForm['username'];
        $pass = $arrForm['password'];

        //----------------------------
        // 入力情報
        //----------------------------
        // 必須チェック
        if(!$objErr->isNull($user)){
            $objErr->_err['user1'] = "ユーザ名を入力してください。";
        }
        if(!$objErr->isNull($pass)){
            $objErr->_err['pass1'] = "パスワードを入力してください。";
        }

        // 半角英数チェック
        if(!$objErr->isAlphaNumeric($user)){
            $objErr->_err['user2'] = "ユーザ名は半角英数で入力してください。";
        }
        if(!$objErr->isAlphaNumeric($pass)){
            $objErr->_err['pass2'] = "パスワードは半角英数で入力してください。";
        }

        // 同名チェック
        if(in_array($user, $arrForm['user'])){
            $objErr->_err['user3'] = "ユーザ名が重複しています。";
        }


        // エラーが発生していない
        if(count($objErr->_err) == 0){
            // 入力情報リセット
            $arrForm['username'] = "";
            $arrForm['password'] = "";

            // 登録
            $arrForm['user'][] = $user;
            $arrForm['pass'][] = crypt($pass, 'mg');
        }


        //----------------------------
        // 登録情報
        //----------------------------
        // 入力数
        $inputNum = 0;

        $unsetArray = array();

        foreach($arrForm['user'] as $_key => $_user){
            $user = $_user;
            $pass = $arrForm['pass'][$_key];

            // 入力がある場合
            if(strlen($user.$pass) > 0){
                $inputNum++;
                // 必須チェック
                if(!$objErr->isNull($user)){
                    $unsetArray[] = $_key;
                }
                if(!$objErr->isNull($pass)){
                    $unsetArray[] = $_key;
                }

//                // 半角英数チェック
//                if(!$objErr->isAlphaNumeric($user)){
//                    $unsetArray[] = $_key;
//                }
//                if(!$objErr->isAlphaNumeric($pass)){
//                    $unsetArray[] = $_key;
//                }

            // どちらも空の場合
            }else{
                $unsetArray[] = $_key;
            }
        }


        if($inputNum == 0){
            unset($arrForm['user']);
            unset($arrForm['pass']);
        }

        if(count($unsetArray) > 0){
            foreach($unsetArray as $_key => $_unsetKey){
                unset($arrForm['user'][$_unsetKey]);
                unset($arrForm['pass'][$_unsetKey]);
            }
        }

        return $objErr->_err;
    }



    // CSVダウンロード用ファイルを複製する
    function createCSVDir(){
        // 必要ファイル群
        $requireFiles = array("form_id", ".htaccess", ".htpasswd", "Mng_Basic_entry_csv.php");

        // ディレクトリがなければ作成
        GeneralFnc::createDir($this->form_dir);

        // 必要ファイルをコピー
        foreach($requireFiles as $requireFile){
            if(!file_exists($this->form_dir. $requireFile)){
                copy($this->base_dir.$requireFile, $this->form_dir.$requireFile);

                // htpasswdのパス設定[htaccess]
                if($requireFile == ".htaccess"){
                    $htaccess = GeneralFnc::readFile($this->form_dir.".htaccess");
                    $htaccess = str_replace("[htpasswd_form]", $this->form_dir, $htaccess);
                    $htaccess = str_replace("[form_id]"      , $this->form_id,  $htaccess);
                    $this->writeFile($this->form_dir.".htaccess", $htaccess);
                }

                // フォームIDセット
                if($requireFile == "form_id"){
                    GeneralFnc::writeFile($this->form_dir."form_id", $this->form_id);
                }
            }
        }
    }



    // ファイルを書き込む
    function writeCSVFile($arrForm){
        // htpasswd書き込み
        $htpass_dir = $this->form_dir. ".htpasswd";
        $htpasswd = GeneralFnc::readFile($htpass_dir);

        $buf = "";
        foreach($arrForm['user'] as $_key => $_user){
            $user = $_user;
            $pass = $arrForm['pass'][$_key];
            
            $buf .= $user .":". $pass ."\n";
        }
        $this->writeFile($htpass_dir, $buf);
    }



	// ファイル書き込み関数
    // @see GeneralFunc.class#writeFile
	function writeFile($fileName, $message) {

		// 書き込み先ファイルオープン
		$fp = fopen($fileName, "w+");
		// ファイルをロック
		flock($fp, LOCK_EX);

		// ファイルへの書き込み
		fwrite($fp, $message);

		// ロック解除
		flock($fp, LOCK_UN);
		// ファイルをクローズ
		fclose($fp);
	}

}

/**
 * メイン処理開始
 **/

$c = new csvBasic();
$c->main();







?>