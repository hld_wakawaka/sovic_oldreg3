<?php
/**
 * 管理者　function
 * 	
 * @package packagename
 * @subpackage Mng
 * @author salon
 *
 */
class Mng_function{
	
	
	/**
	 * 管理者メニュー生成
	 * 
	 * @access public
	 * @return array
	 */	
	static function makeMenu(){
		
		$menu[0] = array("menu_name" => "エントリー管理", "prg" => APP_ROOT."Mng/index.php");
		//$menu[1] = array("menu_name" => "会議管理", "prg" => APP_ROOT."Mng/meeting/Mng_meetinglist.php");
		$menu[2] = array("menu_name" => "メール送信履歴", "prg" => APP_ROOT."Mng/mail/Mng_mailhistory.php");

        // 
        if($GLOBALS["userData"]['use_csvbasic'] == 0){
    		$menu[3] = array("menu_name" => "CSVDL-Basic認証設定"
    		               , "prg"       => APP_ROOT."Mng/csv/Mng_csvBasic.php");
        }

        if($GLOBALS['userData']['type'] == "3" && $GLOBALS['userData']['credit'] == "2"){
    		$menu[4] = array("menu_name" => "決済ログ", "prg" => APP_ROOT."Mng/entry/Mng_entry_error.php");	
        }

		if($GLOBALS["userData"]["pass_chg_flg"] == "1") {
			$menu[5] = array("menu_name" => "パスワード変更", "prg" => APP_ROOT."Mng/account/edit.php");
		}
		$menu[6] = array("menu_name" => "ログアウト", "prg" => APP_ROOT."Mng/login/index.php?mode=logout");	
		
		
		return $menu;
		
	}
	

	/**
	 * 年月日　リストボックス生成
	 * 
	 * @access public
	 * @return array
	 */	
	function makeDateListBox(){
		
		//------------------
		//年
		//------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));

		$year[$wk_year0] = $wk_year0;
		$year[$wk_year1] = $wk_year1;
		$year[$wk_year2] = $wk_year2;		


		//------------------
		//月
		//------------------
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$month[$wk_month] = $wk_month;
		}
		
		//-------------------
		//日
		//-------------------
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$day[$wk_day] = $wk_day;
		}					

		return array($year, $month, $day);				
		
	}
	

	/**
	 * 年月日　妥当性チェック
	 * 
	 * @access public
	 * @param array 
	 * @return array
	 */
	function check_date($param){
		
        // 少なくともどれか一つが入力されている。
        if(($param[1] != "") || ($param[2] != "") || ($param[3] != "")) {
       	
            // 年月日のどれかが入力されていない。
            if(!(strlen($param[1]) > 0 && strlen($param[2]) > 0 && strlen($param[3]) > 0)) {
                $error_msg = $param[0] . "はすべての項目を入力して下さい。";
               
                return array(false,$error_msg);
                
            } 


            if ( ! checkdate((int)$param[2], (int)$param[3], (int)$param[1])) {
                $error_msg = $param[0] . "が正しくありません。";
                return array(false,$error_msg);
            }
 
        }
        
        return array(true,"");		
		
	}
	
	
	/**
	 * ファイル取得処理
	 * 
	 * @param string 取得先ディレクトリ
	 * @return array 取得ファイル名
	 */
	function getFiles($ps_dir){
		
		//取得ファイル名格納変数
		$wa_files = array();
	
		if( is_readable($ps_dir) ){
			@$wo_handle = opendir($ps_dir);
		} else {
			$wo_handle = "";
		}
			
		while ( $wo_handle != '' && $ws_wk_file = readdir($wo_handle) ) {
			if( $ws_wk_file != '.' && $ws_wk_file != '..' ) {
				if(is_file($ps_dir."/".$ws_wk_file)){
					//画面表示用にファイル名を配列に格納する
					array_push($wa_files, $ws_wk_file);					
				}
			}
		}
		
		if($wo_handle !=""){
			closedir($wo_handle);
		}

		return $wa_files;		
	}	 
	
	
	/**
	 * ディレクトリ削除
	 * 
	 * @param string 対象ディレクトリ
	 * @return bool
	 */
	function deleteDir($ps_dir){
		
		if (is_dir($ps_dir)) {
			$wo_dir_h = opendir($ps_dir);
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {
					if (is_dir($ps_dir . "/" . $ws_file)) {
					    $this->deleteDir($ps_dir . "/" . $ws_file);
					}
					else if (is_file($ps_dir . "/" . $ws_file)) {
					    unlink($ps_dir . "/" . $ws_file);
					}
				}
			}
			closedir($wo_dir_h);
			return rmdir($ps_dir);
		}
		else {
		    return true;
		}
		
	}	 	
	
	/**
	 * ファイル削除
	 * 	指定したディレクトリ内のファイルを削除する
	 * 
	 * @param string 対象ディレクトリ
	 * @return bool
	 */
	function deleteFile($ps_dir){

		if (is_dir($ps_dir)) {
			$wo_dir_h = opendir($ps_dir);
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {
					if (is_file($ps_dir . "/" . $ws_file)) {
					    unlink($ps_dir . "/" . $ws_file);
					}
				}
			}
			closedir($wo_dir_h);
			return true;
		}
		else {
		    return true;
		}
		
	}


    /**
     * 登録日、更新日の取得
     * @see Usr/form/function#getEntryDate
     */
    function getEntryDate($po_db, $pn_id){

        $column = "to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday";
        $from = "entory_r";
        $where[] = "del_flg = 0";
        $where[] = "eid = ".$po_db->quote($pn_id);

        $rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

        return $rs;
    }

    /**
     * 文字列置換
     * @see Usr/form/function#wordReplace
     */
    function wordReplace($ps_val, $pa_param){


        $ret_val = "";

        if($ps_val == ""){
            return $ret_val;
        }


        $ret_val = $ps_val;
        $ret_val = ereg_replace("_ID_", $pa_param["ID"], $ret_val);
        $ret_val = ereg_replace("_PASSWORD_", $pa_param["PASSWORD"], $ret_val);
        $ret_val = ereg_replace("_INSERT_DAY_", $pa_param["INSERT_DAY"], $ret_val);
        $ret_val = ereg_replace("_UPDATE_DAY_", $pa_param["UPDATE_DAY"], $ret_val);
        $ret_val = ereg_replace("_SEI_", $pa_param["SEI"], $ret_val);

        $ret_val = ereg_replace("_MEI_", $pa_param["MEI"], $ret_val);
        $ret_val = ereg_replace("_MIDDLE_", $pa_param["MIDDLE"], $ret_val);

        //敬称が入力されていない場合
        if($pa_param["TITLE"] == ""){
            $ret_val = str_replace(array("_TITLE_"), "", $ret_val);
        }
        $ret_val = ereg_replace("_TITLE_", $pa_param["TITLE"], $ret_val);

        $ret_val = ereg_replace("_SECTION_", $pa_param["SECTION"], $ret_val);

        return $ret_val;
    }


    /**
     * eid#checkboxの保持
     * 改ページリンクや戻るボタンに対応
     * @param  void
     * @return array $arrEid
     */
    function gf_set_eid($limit=""){
        $arrEid = $GLOBALS["session"]->getVar("eid");
        $pre_page = !isset($_POST["pre_page"]) ? "0" : $_POST["pre_page"];

        if($pre_page > 0){
            $arrEid[$pre_page] = $_POST['eid'];

            // ページ番号でソート
            $keys = array_keys($arrEid);
            asort($keys);
            foreach($arrEid as $key => $row){
                $foo[$key] = $key;
            }
            array_multisort($foo,SORT_ASC,$arrEid);
            $arrEid = array_combine($keys, $arrEid);
            //------

            // 前回と異なる表示件数
            if($limit != "" && $limit != $_POST['pre_limit']){
                // ページ数を振り直す
                $newArrEid = array();

                $count = max(array_keys($arrEid));

                // 前回より多い表示件数の場合 # 10->50etc
                if($limit > $_POST['pre_limit']){
                    $index = $limit/$_POST['pre_limit'];

                    for($i=1; $i<=$count; $i++){
                        if(!isset($arrEid[$i])) continue;

                        // ページ数を再計算
                        $page_no = floor(($i-1)/$index)+1;

                        if(!isset($newArrEid[$page_no])) $newArrEid[$page_no] = array();
                        $newArrEid[$page_no] = (array)$newArrEid[$page_no] + (array)$arrEid[$i];
                    }
                    $arrEid = $newArrEid;

                // 前回より少ない表示件数の場合 # 50->10etc
                }else{
                    $index = $_POST['pre_limit']/$limit;

                    for($i=1; $i<=$count; $i++){
                        if(!isset($arrEid[$i])) continue;

                        for($j=0; $j<$index; $j++){
                            $key = ($i-1)*$index+($j+1);
                            $newArrEid[$key] = array_slice($arrEid[$i], $j*$limit, $limit, true);
                        }
                    }
                    $arrEid = $newArrEid;
                }
            }
            $GLOBALS["session"]->setVar("eid", $arrEid);
        }

        return $arrEid;
    }


    /* エントリー一覧取得 */
    function getListEntry($obj, $page=1, $limit=ROW_LIMIT) {
        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_getListEntry")){
            return $obj->exClass->Mng_getListEntry($obj, $page, $limit);
        }

        $form_id   = $obj->form_id;
        $condition = $obj->arrForm;

        //フォーム頭文字の長さ
        $len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

        $from      = Mng_function::buildFrom($obj);
        $column    = Mng_function::buildColumn($obj);
        $condition = Mng_function::buildCondition($obj, $condition);
        $where     = Mng_function::buildWhere($obj, $condition);
        $orderby   = Mng_function::buildOrder($obj, $condition);
        $offset = $limit * ($page - 1);


        // エントリー数を取得
        $count = $obj->db->_getOne("COUNT(eid)", $from, $where, __FILE__, __LINE__);
        if(!$count) return array("0", "");

        // エントリー一覧取得
        $arrData = $obj->db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);
        if(!$arrData) return array($count, "");

        return array($count, $arrData);
    }

    function buildFrom($obj){
        $from = "(SELECT e.*, p.order_id, p.payment_method, p.payment_status, p.price FROM entory_r e LEFT JOIN payment p ON e.eid = p.eid) as v_entry";

        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildFrom")){
            $from = $obj->exClass->Mng_buildFrom($obj, $from);
        }
        return $from;
    }


    function buildColumn($obj){
        $column = "*";
        $column.= ", (select c_company from payment where v_entry.eid = payment.eid) as c_company";
        $column.= ", (select c_number  from payment where v_entry.eid = payment.eid) as c_number";
        $column.= ", regexp_replace(e_user_id, '^((.*?)-)+', '') as entry_no";
        //$column.= ", substr(e_user_id, length(e_user_id)-4 ,5) as entry_no";

        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildColumn")){
            $column = $obj->exClass->Mng_buildColumn($obj, $column);
        }

        return $column;
    }


    function buildCondition($obj, $condition){
        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildCondition_before")){
            $condition = $obj->exClass->Mng_buildCondition_before($obj, $condition);
        }

		$condition["user_name"]	= isset($condition["user_name"])	? $condition["user_name"]	: "";
		$condition["status1"]	= isset($condition["status1"])		? $condition["status1"]		: "";
		$condition["status2"]	= isset($condition["status2"])		? $condition["status2"]		: "";
		$condition["status3"]	= isset($condition["status3"])		? $condition["status3"]		: "";
		$condition["status4"]	= isset($condition["status4"])		? $condition["status4"]		: "";
    	$condition["syear"] 	= isset($condition["syear"])		? $condition["syear"]		: "";
    	$condition["smonth"]	= isset($condition["smonth"])		? $condition["smonth"]		: "";
    	$condition["sday"]		= isset($condition["sday"])			? $condition["sday"]		: "";
    	$condition["eyear"]		= isset($condition["eyear"])		? $condition["eyear"]		: "";
    	$condition["emonth"]	= isset($condition["emonth"])		? $condition["emonth"]		: "";
    	$condition["eday"]		= isset($condition["eday"])			? $condition["eday"]		: "";
    	$condition["payment_method"]	= isset($condition["payment_method"])	? $condition["payment_method"]	: "";
    	$condition["payment_status"]	= isset($condition["payment_status"])	? $condition["payment_status"]	: "";

		$condition["s_entry_no"]	= isset($condition["s_entry_no"])	? $condition["s_entry_no"]	: "";
		$condition["e_entry_no"]	= isset($condition["e_entry_no"])	? $condition["e_entry_no"]	: "";
		$condition["country"]	= isset($condition["country"])	? $condition["country"]	: "";
    	$condition["upd_syear"] 	= isset($condition["upd_syear"])		? $condition["upd_syear"]		: "";
    	$condition["upd_smonth"]	= isset($condition["upd_smonth"])		? $condition["upd_smonth"]		: "";
    	$condition["upd_sday"]		= isset($condition["upd_sday"])			? $condition["upd_sday"]		: "";
    	$condition["upd_eyear"]		= isset($condition["upd_eyear"])		? $condition["upd_eyear"]		: "";
    	$condition["upd_emonth"]	= isset($condition["upd_emonth"])		? $condition["upd_emonth"]		: "";
    	$condition["upd_eday"]		= isset($condition["upd_eday"])			? $condition["upd_eday"]		: "";

		$condition["user_name_kana"]	= isset($condition["user_name_kana"])	? $condition["user_name_kana"]	: "";
        $condition["country_list"]   = isset($condition["country_list"])  ? $condition["country_list"] : "";

    	$condition["edata10"] = isset($condition["edata10"]) ? $condition["edata10"] : "";


		$condition["sort_name"]	= isset($condition["sort_name"])	? $condition["sort_name"]	: "";
		$condition["sort"]	= isset($condition["sort"])	? $condition["sort"]	: "";
		$condition["invalid"] = isset($condition["invalid"])	? $condition["invalid"]	: 0;

        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildCondition_after")){
            $condition = $obj->exClass->Mng_buildCondition_after($obj, $condition);
        }

        return $condition;
    }


    function buildWhere($obj, $condition){
        $where   = array();
        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildWhere_before")){
            $where = $obj->exClass->Mng_buildWhere_before($obj, $condition);
        }

		$where[] = "v_entry.del_flg = 0";
		if($condition["invalid"] == "0") {
			$where[] = "invalid_flg = 0";
		} else {
			if($condition["invalid_flg1"] == 1 && $condition["invalid_flg2"] == 1) {
			} else {
				if($condition["invalid_flg1"] == "1") {
					$where[] = "invalid_flg = 0";
				} else if($condition["invalid_flg2"] == "1") {
					$where[] = "invalid_flg = 1";
				}
			}
		}
		$where[] = "v_entry.form_id = ".$obj->db->quote($GLOBALS["userData"]["form_id"]);

		//　応募者
		if($condition["user_name"] != "") {

            $condition["user_name"] = mb_convert_kana($condition["user_name"], "naKV", "utf8");
            $condition["user_name"] = strtolower($condition["user_name"]);


			$arrval = GeneralFnc::customExplode($condition["user_name"]);

			if(count($arrval) > 0) {

				foreach($arrval as $val) {


                    $subwhere[] = "((lower(translate(edata1 ||edata2 ,'－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$obj->db->quote("%".$val."%").") or (lower(translate(edata7 ,'－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$obj->db->quote("%".$val."%")."))";

					//$subwhere[] = "edata1 like '%".$val."%'";

				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

		// ステータス
        $subwhere = array();
		if($condition["status1"] == "1") $subwhere[] = "v_entry.status = 1";
		if($condition["status2"] == "2") $subwhere[] = "v_entry.status = 0";
		if($condition["status3"] == "3") $subwhere[] = "v_entry.status = 2";
		if($condition["status4"] == "4") $subwhere[] = "v_entry.status = 99";

        // ステータスに関するフィルタ追加
        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildWhere_status_filter")){
            $subwhere = $obj->exClass->Mng_buildWhere_status_filter($obj, $condition, $subwhere);
        }

        if(count($subwhere) > 0) {
			$where[] = "(".implode(" or ", $subwhere).")";
		}
        unset($subwhere);


		// 受付期間
		if($condition["syear"] > 0) {
			if($condition["smonth"] == "") $condition["smonth"] = "1";
			if($condition["sday"] == "") $condition["sday"] = "1";
			$sdate = sprintf("%04d", $condition["syear"])."-".sprintf("%02d", $condition["smonth"])."-".sprintf("%02d", $condition["sday"]);
			$sdate.= " 00:00:00";
			$where[] = "v_entry.rdate >= ".$obj->db->quote($sdate);
		}
		if($condition["eyear"] > 0) {
			if($condition["emonth"] == "") $condition["emonth"] = "12";
			if($condition["eday"] == "") {
				$edate = date("Y-m-d", mktime(0, 0, 0, $condition["emonth"]+1, 0, $condition["eyear"]));
			} else {
				$edate = sprintf("%04d", $condition["eyear"])."-".sprintf("%02d", $condition["emonth"])."-".sprintf("%02d", $condition["eday"]);
			}

			$edate.= " 23:59:59";
			$where[] = "v_entry.rdate <= ".$obj->db->quote($edate);
		}

		// 支払方法
		if(is_array($condition["payment_method"])) {
			if(count($condition["payment_method"]) > 0) {
				foreach($condition["payment_method"] as $val) {
					$subwhere[] = "payment_method = ".$obj->db->quote($val);
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}
		// 支払ステータス
		if(is_array($condition["payment_status"])) {
			if(count($condition["payment_status"]) > 0) {
				foreach($condition["payment_status"] as $val) {
					$subwhere[] = "payment_status = ".$obj->db->quote($val);
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}
		}

		//登録No 開始
		if($condition["s_entry_no"] != "") {

            $condition["s_entry_no"] = mb_convert_kana($condition["s_entry_no"], "naKV", "utf8");
            $condition["s_entry_no"] = strtolower($condition["s_entry_no"]);

			//桁数を0埋め５桁にする
			$s_entry_no = sprintf("%05d", $condition["s_entry_no"]);
			//$where[]  = "substr(e_user_id, ".$len_formhead.") >= '".$s_entry_no."'";

			//$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) >= ".$obj->db->quote($s_entry_no);
			$where[]  = "regexp_replace(e_user_id, '^((.*?)-)+', '') >= ".$obj->db->quote($s_entry_no);
        }

		//登録No 終了
		if($condition["e_entry_no"] != "") {

            $condition["e_entry_no"] = mb_convert_kana($condition["e_entry_no"], "naKV", "utf8");
            $condition["e_entry_no"] = strtolower($condition["e_entry_no"]);

			//桁数を0埋め５桁にする
			$e_entry_no = sprintf("%05d", $condition["e_entry_no"]);
			//$where[]  = "substr(e_user_id, ".$len_formhead.") <= '".$e_entry_no."'";

			//$where[]  = "substr(e_user_id, length(e_user_id)-4 ,5) <= ".$obj->db->quote($e_entry_no);
			$where[]  = "regexp_replace(e_user_id, '^((.*?)-)+', '') <= ".$obj->db->quote($e_entry_no);
		}

		//国名
		if($condition["country"] != "") {
			//$where[] = "edata60 like '%".$condition["country"]."%'";

            $condition["country"] = strtolower($condition["country"]);
            $where[] = "(lower(translate(edata60 ,'－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$obj->db->quote("%".$condition["country"]."%").")";
		}

        //country
        if($condition["country_list"] != "") {
            $where[] = "edata114 = ".$obj->db->quote($condition["country_list"]);
        }

		// 受付期間
		if($condition["upd_syear"] > 0) {
			if($condition["upd_smonth"] == "") $condition["upd_smonth"] = "1";
			if($condition["upd_sday"] == "") $condition["upd_sday"] = "1";
			$upd_sdate = sprintf("%04d", $condition["upd_syear"])."-".sprintf("%02d", $condition["upd_smonth"])."-".sprintf("%02d", $condition["upd_sday"]);
			$upd_sdate.= " 00:00:00";
			$where[] = "v_entry.udate >= ".$obj->db->quote($upd_sdate);
		}
		if($condition["upd_eyear"] > 0) {
			if($condition["upd_emonth"] == "") $condition["upd_emonth"] = "12";
			if($condition["upd_eday"] == "") {
				$upd_edate = date("Y-m-d", mktime(0, 0, 0, $condition["upd_emonth"]+1, 0, $condition["upd_eyear"]));
			} else {
				$upd_edate = sprintf("%04d", $condition["upd_eyear"])."-".sprintf("%02d", $condition["upd_emonth"])."-".sprintf("%02d", $condition["upd_eday"]);
			}

			$upd_edate.= " 23:59:59";
			$where[] = "v_entry.udate <= ".$obj->db->quote($upd_edate);
		}

		//カナ
		if($condition["user_name_kana"] != "") {
			$arrval = GeneralFnc::customExplode($condition["user_name_kana"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata3 like ".$obj->db->quote("%".$val."%");
					$subwhere[] = "edata4 like ".$obj->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}

		}

        // 所属
		if($condition["edata10"] != "") {
			$arrval = GeneralFnc::customExplode($condition["edata10"]);
			if(count($arrval) > 0) {
				foreach($arrval as $val) {
					$subwhere[] = "edata10 like ".$obj->db->quote("%".$val."%");
				}
				$where[] = "(".implode(" or ", $subwhere).")";
				unset($subwhere);
			}
		}

        // 登録経路
        $key = 'entry_way';
        if(isset($condition[$key]) && strlen($condition[$key]) > 0){
            $where[] = "entry_way = ".$obj->db->quote($condition[$key]);
        }

        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildWhere_after")){
            $where = $obj->exClass->Mng_buildWhere_after($obj, $condition, $where);
        }

		return $where;
    }


    function buildOrder($obj, $condition){
		$wk_order_by = "udate desc";
        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildOrder_before")){
            $wk_order_by = $obj->exClass->Mng_buildOrder_before($obj, $condition);
        }

		if($condition["sort_name"] != "" && $condition["sort"] != ""){
			$wk_order_by  = "";
			if($condition["sort_name"] == "2"){
				$wk_order_by .= "entry_no ";
			}
			else{
				$wk_order_by .= "udate ";
			}

			if($condition["sort"] == "1"){
				$wk_order_by .= "desc";
			}
			else{
				$wk_order_by .= "asc";
			}
		}

        if(is_object($obj->exClass) && method_exists($obj->exClass, "Mng_buildOrder_after")){
            $wk_order_by = $obj->exClass->Mng_buildOrder_after($obj, $condition, $wk_order_by);
        }

		return $wk_order_by;
    }



    /* エントリー詳細取得 */
    function getEntry($obj){
        if(!isset($obj->eid)) return;
        if(!isset($obj->form_id)) return;
        if(!isset($obj->db))      $obj->db      = new DBGeneral;
        if(!isset($obj->o_entry)) $obj->o_entry = new Entry;
        if(!isset($obj->formdata))$obj->formdata = $GLOBALS["userData"];

        // カスタムフィルタ：処理前
        if(is_object($obj->exClass) && method_exists($obj->exClass, 'mng_detail_getEntry_before')){
            $obj->exClass->mng_detail_getEntry_before($obj);
        }

        $obj->arrData = $obj->o_entry->getRntry_r($obj->db, $obj->eid, $obj->form_id);
        $obj->payment_detail = array();
        $obj->arrAff         = array();

        // 共著者取得
        if($obj->arrData["edata29"] == "1") {
            $obj->arrAff = $obj->o_entry->getRntry_aff($obj->db, $obj->eid);
        }


        // 参加登録の場合は決済情報を取得
        if($obj->formdata["type"] == "3"){
            $obj->arrPayment = $obj->o_entry->getAuthorize($obj->db, $obj->eid);
            if($obj->arrPayment["c_number"] != "") {
                $obj->arrPayment["c_number"] = CryptClass::crypt_decode($obj->arrPayment["c_number"]);
                $obj->arrPayment["c_scode"]  = CryptClass::crypt_decode($obj->arrPayment["c_scode"]);
                $obj->arrPayment["c_date"]   = CryptClass::crypt_decode($obj->arrPayment["c_date"]);
            }

            //--------------------------------
            //明細情報取得
            //--------------------------------
            $wk_payment_detail = $obj->o_entry->getPaymentDetail($obj->db, $obj->eid, "", $obj->hide_fee_area);
            if($wk_payment_detail){
                $obj->payment_detail = $wk_payment_detail;

                if(count($obj->payment_detail) > 0){
                    foreach($obj->payment_detail as $key => $data){
                        if(strpos($data["name"], "事前登録料金") === false) continue;

                        if($obj->arrPayment['csv_price_head0'] != ""){
                            $obj->payment_detail[$key]["name"] = str_replace("事前登録料金", $obj->arrPayment['csv_price_head0'], $data["name"]);
                        }
                    }
                }
            }
        }



        // フォーム項目を配列とする特殊項目
        if(is_object($obj->exClass) && !empty($obj->formarray)){
            foreach($obj->formarray as $_key => $item_id){
                $key = 'edata'.$item_id;

                $obj->arrData[$key] = unserialize($obj->arrData[$key]);
                foreach($obj->arrData[$key] as $_key => $str){
                    $str = str_replace("[:n:]", "\n", $str);   // 改行コード統一
                    $str = stripslashes($str);                 // クォート
                    $obj->arrData[$key][$_key] = $str;
                }
            }
        }


        // カスタムフィルタ：処理後
        if(is_object($obj->exClass) && method_exists($obj->exClass, 'mng_detail_getEntry_after')){
            $obj->exClass->mng_detail_getEntry_after($obj);
        }
    }



    // 値のチェック
    function checkParam($arrForm) {

        // eidは数字の配列でなければならない
        if(isset($arrForm["eid"])) {

            if(is_array($arrForm["eid"])) {
                foreach($arrForm["eid"] as $id) {
                    if(!is_numeric($id)) return false;
                }
            } else {
                return false;
            }

        }

        return true;
    }

    function getEids(&$arrForm){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();

        $arrForm['eid'] = array();
        foreach($arrEid as $_page => $_arrEid){
            foreach($_arrEid as $_eid => $_checked){
                if($_checked != 1) continue;

                $key = $_page ."-". $_eid;
                $arrForm['eid'][$key] = $_eid;
            }
        }
    }

}
?>
