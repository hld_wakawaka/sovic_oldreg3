<?php

include('../../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once('../function.php');
include_once('./Mng_entry_csv.php');
include_once('./Mng_payment_csv.php');
include_once('./Mng_all_csv.php');
include(MODULE_DIR.'Download.class.php');
include_once(TGMDK_ORG_DIR.'/crypt.class.php');


/**
 * 管理者TOP
 *
 * @author salon
 *
 */
class Entry_Error extends ProcessBase {

    public $limit = 10;
    public $searchkey = array('s_entry_no', 'e_entry_no', 'user_name', 'user_name_kana', 'status1', 'status2', 'status3', 'status4','syear', 'smonth', 'sday', 'eyear', 'emonth', 'eday','payment_method', 'payment_status', "entry_no", "country",'upd_syear', 'upd_smonth', 'upd_sday', 'upd_eyear', 'upd_emonth', 'upd_eday', "country_list", "limit", "page");
    public $sesskey = "mng_formlist";

    // 表示件数
    public $arrLimit = array(10, 50, 100, 500);

    /**
     * コンストラクタ
     */
    function Entry_Error(){
        parent::ProcessBase();

        // ログインチェック
        LoginMember::checkLoginRidirect();

        // 初期化
        $this->o_entry = new Entry;
        $this->arrForm = $_REQUEST;
        $this->arrErr  = array();
        $this->onload  = "";
        $this->formdata= $GLOBALS["userData"];
        $this->form_id = $this->formdata["form_id"];
        $this->objErr  = new Validate();
        $this->download= new Download();
        $this->db      = new DbGeneral();
        $this->o_form  = new Form;
        $this->o_itemini = new item_ini;
        $this->arrData   = array();

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        Usr_initial::setFormIni($this);
        Usr_initial::setLanguage($this, $GLOBALS["userData"]['lang']);

        $menu = Mng_function::makeMenu();
        $this->assign("va_menu",   $menu);
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }
        }
    }


	/**
	 * メイン処理
	 */
	function main(){
	    // テンプレート設定
	    $this->setTemplateMng($this);


		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));


		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;

		$eyear[$wk_year0] = $wk_year0;
		$eyear[$wk_year1] = $wk_year1;
		$eyear[$wk_year2] = $wk_year2;

		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;



		//月
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;

		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）

		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）

		//-----------------------------
		//ソート順　リストボックス
		//-----------------------------
		$sort_name["1"] = "更新日";
		$sort_name["2"] = "登録番号";

		$sort["1"] = "降順";
		$sort["2"] = "昇順";

		$this->assign("sort_name", $sort_name);
		$this->assign("sort", $sort);

		//-----------------------------
		$this->assign("arrLimit", $this->arrLimit);


		//-----------------------------
		//フォーム項目取得
		//-----------------------------
		$this->formitem = $this->o_entry->getFormItem($this->db, $this->form_id);
		if(!$this->formitem){
			Error::showErrorPage("フォーム項目情報の取得に失敗しました。");
		}

		$paper = "";	//論文フラグ
        foreach($this->formitem as $data){
            $wk_item[$data["item_id"]] = $data;
        }
        // フォーム項目の名前
        $formitemview = array();
        $formitemview["60"] = $wk_item["60"]["item_view"];
        $formitemview["114"] = $wk_item["114"]["item_view"];

        // ファイルアップロードダウンロード　添付ファイルフィールドの有無
        $upload_file_field = array();
        $upload_file_field["1"] = $wk_item["51"]["item_view"];
        $upload_file_field["2"] = $wk_item["52"]["item_view"];
        $upload_file_field["3"] = $wk_item["53"]["item_view"];

        $this->assign("formitemview", $formitemview);
        $this->assign("upload_file_field", $upload_file_field);


        //---------------------------------
        // アクション別処理
        //---------------------------------
        $this->mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        $actionName = $this->mode."Action";
        $exAction   = 'Mng_index_'.$actionName;

        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            $this->exClass->$exAction($this);
        }else{
            if(method_exists($this, $actionName)){
                $this->$actionName();
            }else{
                $this->defaultAction();
            }
        }



		$this->getSearchkey();
		if(count($this->arrErr) > 0){
			//エラーがあった場合はセッションクリア
			$GLOBALS["session"]->unsetVar($this->sesskey);
		}


        // エントリー一覧取得
        $this->getEntryList();


		if(!isset($this->arrForm["payment_method"])) $this->arrForm["payment_method"] = array();
		if(!isset($this->arrForm["payment_status"])) $this->arrForm["payment_status"] = array();
		if(!isset($this->arrForm["chg_status"])) $this->arrForm["chg_status"] = "";

        // 
        $form = array();
		$form["payment_method"] = SmartyForm::createRadioChecked("payment_method", $GLOBALS["method_J"], $this->arrForm["payment_method"], "checkbox", "form");
		$form["payment_status"] = SmartyForm::createRadioChecked("payment_status", $GLOBALS["paymentstatusList"], $this->arrForm["payment_status"], "checkbox", "form");
		$form["chg_status"] = SmartyForm::createCombo("chg_status", $GLOBALS["paymentstatusList"], $this->arrForm["chg_status"], "form", "↓選択");

		$this->assign("form", $form);
		$this->assign("arrData", $this->arrData);
		$this->assign("arrErr",  $this->arrErr);
		$this->assign("limit",   $this->limit);
		$this->assign("onload",  $this->onload);
        $this->assign("arrItemData", $this->arrItemData);
        // 決済ステータス
        $this->assign('arrStatus', array(0=>'成功', 1=>'離脱/応募中', 2=>'離脱/応募中', 3=>'データ送受信失敗'));


        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_index_premain')){
            $this->exClass->mng_index_premain($this);
        }

		// 親クラスに処理を任せる
		parent::main();
	}


    // ExtendからProcessBaseのmainを呼ぶため設置
    function basemain() {
        parent::main();
        exit;
    }


    function getEntryList(){
		$this->arrForm["page"] = (isset($this->arrForm["page"]) && $this->arrForm["page"] != "")
		                       ? $this->arrForm["page"]
		                       : 1;
		// ソートの初期値は登録番号の降順
        if(!isset($this->arrForm['sort']))      $this->arrForm['sort']      = 1;
        if(!isset($this->arrForm['sort_name'])) $this->arrForm['sort_name'] = 1;
        if(!isset($this->arrForm['sort']))      $this->arrForm['sort']      = 1;
        $this->arrForm['invalid'] = 1;
        $this->arrForm["payment_method"][] = 1;

		//検索条件にエラーが無い場合に一覧情報を取得
		if(count($this->arrErr) ==  0){
			list($this->arrData["count"], $this->arrData["list"]) = Mng_function::getListEntry($this, $this->arrForm["page"], $this->arrForm['limit']);
			$this->pager = array("allcount"=>$this->arrData["count"], "limit"=>$this->arrForm['limit'], "page"=>$this->arrForm["page"]);
		}
		else{
			$this->arrData["count"] = 0;
		}
    }


    /* アクション # デフォルト */
    function defaultAction(){
        $this->clearSearchkey();
        return;
    }


    /* アクション # 検索ボタン */
    function searchAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();

        $this->setSearchkey();

        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid($this->arrForm['limit']);
        $this->assign("arrEid", $arrEid);
        return;
    }


    /* アクション # クリアボタン */
    function clearAction(){
        $this->clearSearchkey();
        return;
    }


    /* アクション # ファイル一括ダウンロード */
    function file_downloadAction(){
        if(!isset($_POST["file_num"]) || $_POST["file_num"] == ""){
            $this->objErr->addErr("ファイルダウンロードに必要なパラメータが不足しています。", "file_num");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $cmd = "/usr/bin/php ".ROOT_DIR."bat/bat_file_archive.php ".$this->form_id." ".$_POST["file_num"]." > /dev/null &";
        system($cmd);

        $msg = $GLOBALS["userData"]["form_mail"]."へ完了メールを送信しました。";
        $this->onload = "window.alert('".$msg."');";
        return;
    }


    /* アクション # エントリーCSVダウンロード */
    function entry_csvAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();
        $this->setSearchkey();

        if(count($this->arrErr) != 0) return;

        $wo_csv = new entry_csv;

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $this->objErr->addErr($csv_data, "csv_data");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $this->download->csv($csv_data, "entry_".date('Ymdhis').".csv");
        exit;
    }


    /* アクション # 決済情報CSVダウンロード */
    function payment_csvAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();
        $this->setSearchkey();

        if(count($this->arrErr) != 0) return;

        $wo_csv = new payment_csv;

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $this->objErr->addErr($csv_data, "csv_data");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $this->download->csv($csv_data, "payment_".date('Ymdhis').".csv");
        exit;
    }


    /* アクション # エントリー+決済情報CSVダウンロード */
    function all_csvAction(){
        // 検索条件 日付の妥当性チェック
        $this->arrErr = $this->check();
        $this->setSearchkey();

        if(count($this->arrErr) != 0) return;

        $wo_csv = new all_csv;

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $this->objErr->addErr($csv_data, "csv_data");
            $this->arrErr = $this->objErr->_err;
            return;
        }

        $this->download->csv($csv_data, "all_".date('Ymdhis').".csv");
        exit;
    }


    /* アクション # 戻るボタン */
    function backAction(){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();
        $this->assign("arrEid", $arrEid);
        return;
    }



	/**
	 * 入力チェック
	 *
	 * @access public
	 * @return object
	 */
	function check(){

		//妥当姓チェック（存在しない日付の場合はエラー）
		if(($this->arrForm["syear"] != "") && ($this->arrForm["smonth"] != "") && ($this->arrForm["sday"] != "") ){
			if(!checkdate((int)$this->arrForm["smonth"], (int)$this->arrForm["sday"], (int)$this->arrForm["syear"])){
				$this->objErr->addErr("受付期間（開始）に存在しない日付が指定されています。", "search_sday");
			}

		}

		if($this->arrForm["eyear"] != "" && $this->arrForm["emonth"] != "" && $this->arrForm["eday"] != ""){

			if(!checkdate((int)$this->arrForm["emonth"], (int)$this->arrForm["eday"], (int)$this->arrForm["eyear"])){
				$this->objErr->addErr("受付期間（終了）に存在しない日付が指定されています。", "search_eday");
			}
		}

		return $this->objErr->_err;
	}


	function setSearchkey() {

		$GLOBALS["session"]->unsetVar($this->sesskey);

		foreach($this->searchkey as $val) {
			if(isset($_REQUEST[$val])) {
				if(is_array($_REQUEST[$val])) {
					$param[$val] = $_REQUEST[$val];
				} else {
					$param[$val] = trim($_REQUEST[$val]);
				}
			} else {
				$param[$val] = "";
			}
		}
		if($param['limit'] == ""){
		    $param['limit'] = $this->limit;
		}

		$GLOBALS["session"]->setVar($this->sesskey, $param);

	}

	function clearSearchkey() {

		$GLOBALS["session"]->unsetVar($this->sesskey);

		foreach($this->searchkey as $key) {
			$this->arrForm[$key] = "";
		}
	    $this->arrForm['limit'] = $this->limit;
	}

	function getSearchkey() {

		if(!$GLOBALS["session"]->issetVar($this->sesskey)) return;

		$sessvar = $GLOBALS["session"]->getVar($this->sesskey);

		foreach($this->searchkey as $key) {
			if(isset($sessvar[$key])) {
				$this->arrForm[$key] = $sessvar[$key];
			} else {
				$this->arrForm[$key] = "";
			}
		}
		if($this->arrForm['limit'] == ""){
		    $this->arrForm['limit'] = $this->limit;
		}
	}


    function setTemplateMng($obj) {
        $includeDir = TEMPLATES_DIR."Mng/entry/include/".$obj->form_id."/";

        //------------------------
        // 表示HTMLの設定
        //------------------------
        $this->_processTemplate = "Mng/entry/Mng_formlist_error.html";
        $this->_title  = "管理者ページ";


        // 検索拡張
        $tmpfile = $obj->form_id."search.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("search_inc_flg", 1);
            $obj->assign("tpl_search", $tmpfile);
        }


        // 一括処理拡張
        $tmpfile = $obj->form_id."operation.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("operation_inc_flg", 1);
            $obj->assign("tpl_operation", $tmpfile);
        }


        // エントリー一覧拡張
        $tmpfile = $obj->form_id."entrylist.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("entrylist_inc_flg", 1);
            $obj->assign("tpl_entrylist", $tmpfile);
        }


        $obj->assign("includeDir", $includeDir);
    }


}

/**
 * メイン処理開始
 **/

$c = new Entry_Error();
$c->main();







?>