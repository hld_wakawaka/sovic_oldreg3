<?php


include_once(MODULE_DIR.'custom/Form.class.php');
include_once(TGMDK_ORG_DIR.'/crypt.class.php');

/**
 * 管理者　決済CSVダウンロード
 *
 * @author salon
 *
 */
class payment_csv{

    // Mng_all_csvで利用する可能性のある変数を定義
    // 明細表示フラグ
    public $payment_csv_price_disp = false;
    public $payment_csv_price_head = "参加費";

    /**
     * コンストラクタ
     */
    function payment_csv(){
        LoginMember::checkLoginRidirect();

        //----------------------------
        //インスタンス
        //----------------------------
        $this->db       = new DbGeneral;
        $this->o_crypt  = new CryptClass;
        $this->objEntry = new Entry;
        $this->o_form   = new Form();

        //------------------------------
        //初期化
        //------------------------------
        $this->arrErr = "";
        $this->delimiter = "\t";


        //フォームパラメータ
        $this->arrForm = $_REQUEST;

        //--------------------------------------------
        //定数情報取得
        //--------------------------------------------
        $lang = ($GLOBALS["userData"]["lang"] == "1") ? "J" : "E";
        $this->wa_jpo1       = $GLOBALS["jpo1_".$lang];       //支払回数
        $this->wa_method     = $GLOBALS["method_".$lang];     //支払方法
        $this->wa_card_type  = $GLOBALS["card_type_".$lang];  //カードの種類
        $this->wa_pay_status = $GLOBALS["paymentstatusList"]; //決済ステータス


        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($GLOBALS["userData"]["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
        }

        // フォーム管理者用設定の読み込み
        if(method_exists($this->exClass, "__constructMng")){
            $this->exClass->__constructMng($this);
        }


        //--------------------------------
        //フォーム情報取得
        //--------------------------------
        //その他決済項目
        $this->wa_ather_price = array("-1"=>"カテゴリー");
        if($this->o_form->formData["ather_price"] != ""){
            $wk_price = explode("\n", $this->o_form->formData["ather_price"]);

            foreach($wk_price as $key => $data){
                $data = str_replace(array("　"," "), "|",trim($data));
                $wk_price_data = explode("|", $data);
                $this->wa_ather_price[$key] = $wk_price_data[0];
            }
        }
        
    	// カスタマイズテンプレート設定の読み込み @ autoload
        Config::loadSection($this);
    }



	/**
	 * メイン処理
	 */
	function main(){
	    $obj = $this;
        if(is_object($this->exClass) && method_exists($this->exClass, "payment_csv_main")){
            return $this->exClass->payment_csv_main($this);
        }

		//--------------------------------
		//対象データ取得
		//--------------------------------
		//データ取得（決済情報）
		list($wb_ret, $list) = $obj->_getData($obj);
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		$obj->header = $obj->payMakeHeader($obj);

		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$obj->csvdata = "";

		foreach($list as $data){
			//支払明細情報取得
            $payment_detail = $obj->objEntry->getPaymentDetail($obj->db, $data["entry_id"], "", $obj->hide_fee_area);
			$obj->csvdata .= $obj->payMakeData($obj, $data, $payment_detail);
		}


		//ヘッダとデータを連結して出力
		$exp_data = $obj->header.$obj->csvdata;

        // 日本語フォームの場合
        if($GLOBALS["userData"]["lang"] == LANG_JPN){
    		$exp_data = mb_convert_encoding($exp_data, "SJIS", "UTF8");
        }

		return array(true, $exp_data);
	}



    /**
     * 対象データ取得
     */
    function _getData($obj){
        if(is_object($this->exClass) && method_exists($this->exClass, "payment_csv_getData")){
            return $this->exClass->payment_csv_getData($obj);
        }

        //$table = $obj->objEntry->getTable($GLOBALS["userData"]["type"]);

        //フォーム頭文字の長さ
        $len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

        $column = "a.eid as entry_id, a.edata1, a.edata2, a.edata3, a.edata4, a.edata5, a.edata6, a.edata7, regexp_replace(e_user_id, '^((.*?)-)+', '') as entry_no";
        $column .= ",b.*, to_char(b.rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(b.udate, 'yyyy/mm/dd hh24:mi:ss') as upday";

        $from = "entory_r a left join payment b on a.form_id = b.form_id and a.eid = b.eid and a.del_flg = 0 and b.del_flg=0";
        $from .= " and a.form_id = ".$GLOBALS["userData"]["form_id"];
        $where[] = "a.form_id = ".$obj->db->quote($GLOBALS["userData"]["form_id"]);
        $where[] = "a.del_flg = 0";
        $where[] = "a.invalid_flg = 0";



        //　応募者
        if($obj->arrForm["user_name"] != "") {
            $arrval = GeneralFnc::customExplode($obj->arrForm["user_name"]);
            if(count($arrval) > 0) {
                foreach($arrval as $val) {
                    $subwhere[] = "a.edata1 like ".$obj->db->quote("%".$val."%");
                }
                $where[] = "(".implode(" or ", $subwhere).")";
                unset($subwhere);
            }

        }

        // ステータス
        if(isset($obj->arrForm["status1"]) && $obj->arrForm["status1"] == "1") $subwhere[] = "a.status = 1";
        if(isset($obj->arrForm["status2"]) && $obj->arrForm["status2"] == "2") $subwhere[] = "a.status = 0";
        if(isset($obj->arrForm["status3"]) && $obj->arrForm["status3"] == "3") $subwhere[] = "a.status = 2";
        if(isset($obj->arrForm["status4"]) && $obj->arrForm["status4"] == "4") $subwhere[] = "a.status = 99";


        if(isset($subwhere)) {
            $where[] = "(".implode(" or ", $subwhere).")";
            unset($subwhere);
        }

        // 受付期間
        if($obj->arrForm["syear"] > 0) {
            if($obj->arrForm["smonth"] == "") $obj->arrForm["smonth"] = "1";
            if($obj->arrForm["sday"] == "") $obj->arrForm["sday"] = "1";
            $sdate = sprintf("%04d", $obj->arrForm["syear"])."-".sprintf("%02d", $obj->arrForm["smonth"])."-".sprintf("%02d", $obj->arrForm["sday"]);
            $where[] = "a.rdate >= ".$obj->db->quote($sdate)."";
        }
        if($obj->arrForm["eyear"] > 0) {
            if($obj->arrForm["emonth"] == "") $obj->arrForm["emonth"] = "12";
            if($obj->arrForm["eday"] == "") {
                $edate = date("Y-m-d", mktime(0, 0, 0, $obj->arrForm["emonth"]+1, 0, $obj->arrForm["eyear"]));
            } else {
                $edate = sprintf("%04d", $obj->arrForm["eyear"])."-".sprintf("%02d", $obj->arrForm["emonth"])."-".sprintf("%02d", $obj->arrForm["eday"]);
            }

            $where[] = "a.rdate <= ".$obj->db->quote($edate)."";
        }


        // 支払方法
        if(isset($obj->arrForm["payment_method"])){
            if(is_array($obj->arrForm["payment_method"])) {
                if(count($obj->arrForm["payment_method"]) > 0) {
                    foreach($obj->arrForm["payment_method"] as $val) {
                        $subwhere[] = "b.payment_method = ".$obj->db->quote($val);
                    }
                    $where[] = "(".implode(" or ", $subwhere).")";
                    unset($subwhere);
                }

            }
        }


        // 支払ステータス
        if(isset($obj->arrForm["payment_status"])){
            if(is_array($obj->arrForm["payment_status"])) {
                if(count($obj->arrForm["payment_status"]) > 0) {
                    foreach($obj->arrForm["payment_status"] as $val) {
                        $subwhere[] = "b.payment_status = ".$obj->db->quote($val);
                    }
                    $where[] = "(".implode(" or ", $subwhere).")";
                    unset($subwhere);
                }
            }
        }


        //登録No 開始
        if($obj->arrForm["s_entry_no"] != "") {
            //桁数を0埋め５桁にする
            $s_entry_no = sprintf("%05d", $obj->arrForm["s_entry_no"]);
            $where[]  = "regexp_replace(e_user_id, '^((.*?)-)+', '') >= ".$obj->db->quote($s_entry_no)."";

        }

        //登録No 終了
        if($obj->arrForm["e_entry_no"] != "") {
            //桁数を0埋め５桁にする
            $e_entry_no = sprintf("%05d", $obj->arrForm["e_entry_no"]);
            $where[]  = "regexp_replace(e_user_id, '^((.*?)-)+', '') <= ".$obj->db->quote($e_entry_no)."";

        }



        //国
        if(isset($obj->arrForm["country"])){
            if($obj->arrForm["country"] != "") {
                $where[] = "edata60 like ".$obj->db->quote("%".$obj->arrForm["country"]."%");
            }
        }

        // 受付期間
        if($obj->arrForm["upd_syear"] > 0) {
            if($obj->arrForm["upd_smonth"] == "") $obj->arrForm["upd_smonth"] = "1";
            if($obj->arrForm["upd_sday"] == "") $obj->arrForm["upd_sday"] = "1";
            $upd_sdate = sprintf("%04d", $obj->arrForm["upd_syear"])."-".sprintf("%02d", $obj->arrForm["upd_smonth"])."-".sprintf("%02d", $obj->arrForm["upd_sday"]);
            $where[] = "a.udate >= ".$obj->db->quote($upd_sdate)."";
        }
        if($obj->arrForm["upd_eyear"] > 0) {
            if($obj->arrForm["upd_emonth"] == "") $obj->arrForm["upd_emonth"] = "12";
            if($obj->arrForm["upd_eday"] == "") {
                $upd_edate = date("Y-m-d", mktime(0, 0, 0, $obj->arrForm["upd_emonth"]+1, 0, $obj->arrForm["upd_eyear"]));
            } else {
                $upd_edate = sprintf("%04d", $obj->arrForm["upd_eyear"])."-".sprintf("%02d", $obj->arrForm["upd_emonth"])."-".sprintf("%02d", $obj->arrForm["upd_eday"]);
            }

            $where[] = "a.udate <= ".$obj->db->quote($upd_edate)."";
        }

        //カナ
        if($obj->arrForm["user_name_kana"] != "") {
            $arrval = GeneralFnc::customExplode($obj->arrForm["user_name_kana"]);
            if(count($arrval) > 0) {
                foreach($arrval as $val) {
                    $subwhere[] = "edata3 like ".$obj->db->quote("%".$val."%");
                }
                $where[] = "(".implode(" or ", $subwhere).")";
                unset($subwhere);
            }

        }

        //------------------------------------
        //表示順
        //------------------------------------
        $wk_order_by = "a.udate desc";

        if($obj->arrForm["sort_name"] != "" && $obj->arrForm["sort"] != ""){
            $wk_order_by  = "";
            if($obj->arrForm["sort_name"] == "2"){
                $wk_order_by .= "entry_no ";
            }
            else{
                $wk_order_by .= "a.udate ";
            }

            if($obj->arrForm["sort"] == "1"){
                $wk_order_by .= "desc";
            }
            else{
                $wk_order_by .= "asc";
            }
        }


        $orderby = $wk_order_by;
        //$orderby = "a.eid";

        $list_data = $obj->db->getListData($column, $from, $where, $orderby);


        if(!$list_data){
            return array(false, "決済情報の取得に失敗しました。");
        }

        return array(true, $list_data);
    }




    /**
     * CSVヘッダ生成
     */
    function payMakeHeader($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "payment_payMakeHeader")){
            return $this->exClass->payment_payMakeHeader($obj, $all_flg);
        }

        $header   = array();
        // payment_csvのみ出力
        if(!$all_flg){
            $header[] = "応募者氏名";
            $header[] = "応募者氏名（カナ）";
        }
        $header[] = "取引ID";
        $header[] = "お支払い方法";
        $header[] = "ステータス";

        $header[] = "支払期限";
        $header[] = "クレジット番号";
        $header[] = "カード会社";
        $header[] = "カード名義人";
        $header[] = "有効期限";
        $header[] = "クレジット支払い区分";
        $header[] = "クレジット支払い回数";
//        $header[] = "セキュリティコード";

        $header[] = "お振込み人名義";
        $header[] = "お支払銀行名";
        $header[] = "お振込み日";

        $header[] = "コンビニ　ショップコード";
        $header[] = "姓";
        $header[] = "名";
        $header[] = "セイ";
        $header[] = "メイ";
        $header[] = "電話番号";
        $header[] = "メールアドレス";
        $header[] = "郵便番号";
        $header[] = "住所";
        $header[] = "成約日";
        $header[] = "決済機関コード";
        //$header[] = "支払詳細";


        //------------------------------
        //決済項目
        //------------------------------
        foreach($obj->wa_ather_price as $data){
//            $head = GeneralFnc::smarty_modifier_del_tags($data);
            $head = strip_tags($data);
            $header[] = strip_tags(str_replace(array("_"), " ",$head));
            // 明細表示フラグ
            if($obj->payment_csv_price_disp){
                $header[] = $obj->payment_csv_price_head;
            }
        }

        $header[] = "合計金額";
        $header[] = "登録日";
        $header[] = "更新日";

        //生成
        $ret_buff  = implode($obj->delimiter, $header);
        $ret_buff .="\n";

        return $ret_buff;
    }



    /**
     * CSV出力データ生成
     */
    function payMakeData($obj, $pa_param, $pa_detail, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "payment_payMakeData")){
            return $this->exClass->payment_payMakeData($obj, $pa_param, $pa_detail, $all_flg);
        }

        $ret_buff = "";
        $wk_buff = "";

        // payment_csvのみ出力
        if(!$all_flg){
            //応募者氏名
            $wk_buff[] =  "\"".$pa_param["edata1"]."　".$pa_param["edata2"]. "\"";

            //応募者氏名（カナ）
            $wk_buff[] =  "\"".$pa_param["edata3"]."　".$pa_param["edata4"]. "\"";
        }

        //取引ID
        $wk_buff[] = "\"".$pa_param["order_id"]."\"";

        //お支払い方法
        $wk_buff[] = ($pa_param["payment_method"] != "") ? "\"".$obj->wa_method[$pa_param["payment_method"]]."\"" : "";

        //ステータス
        $wk_buff[] = ($pa_param["payment_status"] != "") ? "\"".$obj->wa_pay_status[$pa_param["payment_status"]]."\"" : "";



        //支払期限
        $wk_buff[] = "\"".$pa_param["limit_date"]."\"";

        //クレジット番号
        $wk_buff[] = ($pa_param["c_number"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_number"])."\"" : "";

        //カード会社
        $wk_buff[] = ($pa_param["c_company"] != "") ? "\"".$obj->wa_card_type[$pa_param["c_company"]]."\"" : "";

        //カード名義人
        $wk_buff[] = "\"".$pa_param["c_holder"]."\"";

        //有効期限
        $wk_buff[] = ($pa_param["c_date"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_date"])."\"" : "";


        //クレジット支払い方法
        $wk_buff[] = ($pa_param["c_method"] != "") ? "\"".$obj->wa_jpo1[$pa_param["c_method"]]."\"" : "";

        //クレジット支払い回数
        $wk_buff[] = "\"".$pa_param["c_split"]."\"";

        //セキュリティコード
//        $wk_buff[] = ($pa_param["c_scode"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_scode"])."\"" : "";


        //お振込み人名義
        $wk_buff[] = "\"".$pa_param["lessee"]."\"";

        //お支払銀行名
        $wk_buff[] = "\"".$pa_param["bank"]."\"";


        //お振込み日
        $wk_buff[] = "\"".$pa_param["closure"]."\"";


        //コンビニ　ショップコード
        $wk_buff[] = "";

        //姓
        $wk_buff[] = "\"".$pa_param["name1"]."\"";

        //名
        $wk_buff[] = "\"".$pa_param["name2"]."\"";

        //セイ
        $wk_buff[] =  "\"".$pa_param["kana1"]."\"";

        //メイ
        $wk_buff[] = "\"".$pa_param["kana2"]."\"";

        //電話番号
        $wk_buff[] = "\"".$pa_param["tel"]."\"";

        //メールアドレス
        $wk_buff[] = "\"".$pa_param["mail"]."\"";


        //郵便番号
        $wk_buff[] = "\"".$pa_param["zip"]."\"";

        //住所
        $wk_buff[] = "\"".$pa_param["addr"]."\"";

        //成約日
        $wk_buff[] = "\"".$pa_param["sold_date"]."\"";

        //決済機関コード
        $wk_buff[] = "\"".$pa_param["bank_code"]."\"";


        //支払詳細
        $wk_detail = "";

        if($pa_detail){
            $wk_buff = array_merge($wk_buff, $obj->pay_c($obj, $pa_detail));
        }else{
            foreach($obj->wa_ather_price as $ather_name){
                $wk_buff[] = "";
            }
        }



        //金額
        $wk_buff[] = "\"".$pa_param["price"]."\"";



        //登録日
        $wk_buff[] = ($pa_param["insday"] != "") ? "\"".$pa_param["insday"]."\"" : "";

        //更新日
        $wk_buff[] = ($pa_param["upday"] != "") ? "\"".$pa_param["upday"]."\"" : "";


        $ret_buff = implode($obj->delimiter, $wk_buff);
        $ret_buff .= "\n";
        return $ret_buff;
    }



    // 金額、その他決済項目のCSV整形
    function pay_c($obj, $pa_detail){
        if(is_object($this->exClass) && method_exists($this->exClass, "payment_pay_c")){
            return $this->exClass->payment_pay_c($obj, $pa_detail);
        }

        $isSet = array();
        foreach($obj->wa_ather_price as $key => $ather_name){
            $quantity = "";
            $match_flg = "";
            // 金額表示フラグ
            $price_flg = "";
            $price     = "";

            foreach($pa_detail as $_key => $data){
                // バッファ済みのキーは判定しない
                if(in_array($_key, $isSet)) continue;

                // 金額
                if($data['type'] == 0){
                    $from = "payment";
                    $column = "csv_price_head0, csv_ather_price_head0";
                    $where = "eid = ".$obj->db->quote($data['eid'])."";
                    $csv_header =  $obj->db->getData($column, $from, $where, __FILE__, __LINE__);

                    $pos = strpos($data["name"], "事前登録料金");
                    if($pos !== false){
                        $quantity = $data["name"];

                        // CSV用に整形--------------------------------------------------------------
                        if($csv_header['csv_price_head0'] != ""){
                            $quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
                        }
                        $match_flg = "1";

                        $price_flg = "1";
                        $price = $data['price'];
                        break 1;
                    }

                // その他決済
                }elseif($key== $data["key"]){
                    $quantity = $data["quantity"];
                    $match_flg = "1";

                    $price_flg = "1";
                    $price = $data['price'];
                    break 1;
                }
            }


            if($match_flg == "1"){
                $wk_buff[] = "\"".GeneralFnc::smarty_modifier_del_tags($quantity)."\"";
                // 明細表示フラグ
                if($obj->payment_csv_price_disp && $price_flg == "1"){
                    $wk_buff[] = "\"".$price."\"";
                }

                // バッファ済みのキーをセット
                $isSet[] = $_key;
                $isSet[] = $_key."t";

            }else{
                $wk_buff[] = "\"\"";
                // 明細表示フラグ
                if($obj->payment_csv_price_disp){
                    $wk_buff[] = "\"\"";
                }
            }
        }

        return $wk_buff;
    }
}


?>