<?php

include_once(MODULE_DIR.'custom/Form.class.php');
include_once(TGMDK_ORG_DIR.'/crypt.class.php');
include_once('./entry/Mng_entry_csv.php');
include_once('./entry/Mng_payment_csv.php');

/**
 * 管理者　決済CSVダウンロード
 *
 * @author salon
 *
 */
class all_csv {


    /**
     * コンストラクタ
     */
    function all_csv(){
        LoginMember::checkLoginRidirect();

        //----------------------------
        //インスタンス
        //----------------------------
        $this->objentryCSV = new entry_csv();
        $this->objpayCSV   = new payment_csv();
        $this->db        = $this->objentryCSV->db;
        $this->o_entry   = $this->objentryCSV->o_entry;
        $this->o_itemini = $this->objentryCSV->o_itemini;
        $this->objErr    = $this->objentryCSV->objErr;
		$this->download  = $this->objentryCSV->download;

        $this->o_crypt  = $this->objpayCSV->o_crypt;
        $this->objEntry = $this->objpayCSV->objEntry;
        $this->o_form   = $this->objpayCSV->o_form;

        //------------------------------
        //初期化
        //------------------------------
        $this->arrErr    = $this->objentryCSV->arrErr;
        $this->delimiter = $this->objentryCSV->delimiter;
        $this->arrItemData = $this->objentryCSV->arrItemData;
        $this->itemData    = $this->objentryCSV->itemData;
        $this->loop        = $this->objentryCSV->loop;
        $this->delimiter   = $this->objentryCSV->delimiter;
        $this->wa_ather_price = $this->objpayCSV->wa_ather_price;

        //--------------------------------------------
        //定数情報取得
        //--------------------------------------------
        $this->wa_contact  = $this->objentryCSV->wa_contact;
        $this->wa_kaiin    = $this->objentryCSV->wa_kaiin;
        $this->wa_coAuthor = $this->objentryCSV->wa_coAuthor;

        $lang = ($GLOBALS["userData"]["lang"] == "1") ? "J" : "E";
        $this->wa_jpo1       = $GLOBALS["jpo1_".$lang];       //支払回数
        $this->wa_method     = $GLOBALS["method_".$lang];      //支払方法
        $this->wa_card_type  = $GLOBALS["card_type_".$lang];  //カードの種類
        $this->wa_pay_status = $GLOBALS["paymentstatusList"]; //決済ステータス

        //フォームパラメータ
        $this->arrForm = $this->objentryCSV->arrForm;

        // エントリーCSV用のメンバ変数を定義
        $objentryVal = get_class_vars(get_class($this->objentryCSV));
        if(is_array($objentryVal)){
            foreach($objentryVal as $_variable => $_val){
                if(isset($this->{$_variable})) continue;
                $this->{$_variable} = $_val;
            }
        }
        // 決済CSV用のメンバ変数を定義
        $objpayVal = get_class_vars(get_class($this->objpayCSV));
        if(is_array($objpayVal)){
            foreach($objpayVal as $_variable => $_val){
                if(isset($this->{$_variable})) continue;
                $this->{$_variable} = $_val;
            }
        }


        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($GLOBALS["userData"]["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }
        }
        
    	// カスタマイズテンプレート設定の読み込み @ autoload
        Config::loadSection($this);
    }


	/**
	 * メイン処理
	 */
	function main(){
	    $obj = $this;
        if(is_object($this->exClass) && method_exists($this->exClass, "all_csv_main")){
            return $this->exClass->all_csv_main($this);
        }

		//--------------------------------
		//対象データ取得
		//--------------------------------
		//データ取得（エントリー情報）
		list($wb_ret, $list) = $obj->objentryCSV->_getData($obj);
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		//エントリー情報ヘッダー
		$obj->entry_header = $obj->objentryCSV->entryMakeHeader($obj, true);

		//決済情報ヘッダー
		$obj->payment_header = $obj->objpayCSV->payMakeHeader($obj, true);


		//エントリー情報ヘッダーの改行を削除
		$obj->entry_header = str_replace(array("\n\r","\r","\n"), "", $obj->entry_header);

		//連結
		$obj->header =$obj->entry_header.$obj->delimiter.$obj->payment_header;



		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$obj->csvdata = "";
		$obj->entry_csvdata = "";
		$obj->payment_csvdata = "";

		//エントリー情報ループ
		foreach($list as $data){

			//----------------------------
			//エントリー情報CSVデータ生成
			//----------------------------
			$wk_entry_csvdata = $obj->objentryCSV->entryMakeData($obj, $data, true);

			//決済情報取得
			list($wb_ret, $wk_paymentdata) = $obj->_getPaymentData($obj, $data["eid"]);
			if(!$wb_ret){
				//終了処理
				return array(false, "決済情報の取得に失敗しました。");
			}


			//支払明細情報取得
            $payment_detail = $obj->o_entry->getPaymentDetail($obj->db, $data["eid"], "", $obj->hide_fee_area);

			//-----------------------------
			//決済情報CSVデータ生成
			//-----------------------------
			$wk_payment_csvdata = $obj->objpayCSV->payMakeData($obj, $wk_paymentdata[0], $payment_detail, true);


			//-----------------------------
			//エントリー情報と決済情報を連結
			//-----------------------------
			$wk_entry_csvdata = str_replace(array("\n\r","\r","\n"), "", $wk_entry_csvdata);
			$obj->csvdata .=$wk_entry_csvdata.$obj->delimiter.$wk_payment_csvdata;



		}



		//ヘッダとデータを連結して出力
		$exp_data = $obj->header.$obj->csvdata;

        // 日本語フォームの場合
        if($GLOBALS["userData"]["lang"] == LANG_JPN){
    		$exp_data = mb_convert_encoding($exp_data, "SJIS", "UTF8");
        }

		return array(true, $exp_data);



	}


	/**
	 * 対象データ取得
	 */
	function _getPaymentData($obj, $pn_eid){
        if(is_object($this->exClass) && method_exists($this->exClass, "all_csv_getPaymentData")){
            return $this->exClass->all_csv_getPaymentData($obj, $pn_eid);
        }


		$column = "a.eid, a.edata1, a.edata2, a.edata3, a.edata4, a.edata5, a.edata6, a.edata7";
		$column .= ",b.*, to_char(b.rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(b.udate, 'yyyy/mm/dd hh24:mi:ss') as upday";

		$from = "entory_r a left join payment b on a.form_id = b.form_id and a.eid = b.eid and a.del_flg = 0 and b.del_flg=0";
		$from .= " and a.form_id = ".$obj->db->quote($GLOBALS["userData"]["form_id"]);
		$where[] = "a.eid = ".$pn_eid;

		$orderby = "a.eid";

		$list_data = $obj->db->getListData($column, $from, $where, $orderby);
		if(!$list_data){
			return array(false, "決済情報の取得に失敗しました。");
		}

		return array(true, $list_data);
	}


    // グループ1ヘッダ
    function entryMakeHeader1($obj, $all_flg){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader1")){
            return $this->exClass->entry_csv_entryMakeHeader1($obj, $all_flg);
        }
        return $obj->objentryCSV->entryMakeHeader1($obj, $all_flg);
    }


    // グループ2ヘッダ
    function entryMakeHeader2($obj, $all_flg){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader2")){
            return $this->exClass->entry_csv_entryMakeHeader2($obj, $all_flg);
        }
        return $obj->objentryCSV->entryMakeHeader2($obj, $all_flg);
    }


    // グループ3ヘッダ
    function entryMakeHeader3($obj, $all_flg){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader3")){
            return $this->exClass->entry_csv_entryMakeHeader3($obj, $all_flg);
        }
        return $obj->objentryCSV->entryMakeHeader3($obj, $all_flg);
    }


    // グループ1 データ生成
    function entryMakeData1($obj, $pa_param, $all_flg){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData1")){
            return $this->exClass->entry_csv_entryMakeData1($obj, $pa_param, $all_flg);
        }
        return $obj->objentryCSV->entryMakeData1($obj, $pa_param, $all_flg);
    }


    // グループ2 データ生成
    function entryMakeData2($obj, $pa_param, $all_flg){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData2")){
            return $this->exClass->entry_csv_entryMakeData2($obj, $pa_param, $all_flg);
        }
        return $obj->objentryCSV->entryMakeData2($obj, $pa_param, $all_flg);
    }


    // グループ3 データ生成
    function entryMakeData3($obj, $pa_param, $all_flg){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData3")){
            return $this->exClass->entry_csv_entryMakeData3($obj, $pa_param, $all_flg);
        }
        return $obj->objentryCSV->entryMakeData3($obj, $pa_param, $all_flg);
    }


    // 金額、その他決済項目のCSV整形
    function pay_c($obj, $pa_detail){
        if(is_object($this->exClass) && method_exists($this->exClass, "payment_pay_c")){
            return $this->exClass->payment_pay_c($obj, $pa_detail);
        }
        return $obj->objpayCSV->pay_c($obj, $pa_detail);
    }

}







?>