<?php

include('../../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include_once('../function.php');

/**
 * 管理者　一括投稿編集
 *
 * @author salon
 *
 */
class plural extends ProcessBase {

    public $actionName;
    public $exec_name;

    /**
     * コンストラクタ
     */
    function plural(){
        parent::ProcessBase();

        // ログインチェック
        LoginMember::checkLoginRidirect();

        // 初期化
        $this->db = new DbGeneral();
        $this->o_entry   = new Entry;
        $this->formdata  = $GLOBALS["userData"];
        $this->form_id   = $this->formdata["form_id"];
        $this->arrForm   = $_REQUEST;
        $this->o_itemini = new item_ini;
        $this->o_form    = new Form;

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        Usr_initial::setFormIni($this);


        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);
        $this->assign("user_name", $GLOBALS["userData"]["user_name"]);

        // ステータスの表記を設定
        $GLOBALS["entryStatusList"]["0"] = "初期状態に戻す";
        $GLOBALS["entryStatusList"]["5"] = "ステータス変更";

        // 外部クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }
            // 一括処理の設定
            if(method_exists($this->exClass, "__constructMngPlural")){
                $this->exClass->__constructMngPlural($this);
            }
        }
    }


    /* メイン処理 */
    function main(){
        // テンプレート設定
        $this->setTemplateMng($this);

        // アクセスチェック
        if(!Mng_function::checkParam($this->arrForm)){
            Error::showErrorPage("不正なアクセスです。");
            exit;
        }
        // 処理対象のエントリーIDを取得
        Mng_function::getEids($this->arrForm);
        // パラメータチェック
        if(!isset($this->arrForm["eid"]) || !count($this->arrForm["eid"]) > 0) {
            $this->complete("エントリーが指定されていません。");
        }


        // 不明なステータス
        if(!in_array($this->arrForm["set_stat"], array_keys($GLOBALS["entryStatusList"]))){
            Error::showErrorPage("処理に必要なデータの取得に失敗しました。");
            exit;
        }

        // 一括処理種別の設定
        $this->setAction($this);

        $this->assign("exec_name", $this->exec_name);
        $this->_title = "一括処理（".$this->exec_name."）";


        //---------------------------------
        // アクション別処理
        //---------------------------------
        $this->mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        $actionName = $this->mode."Action";
        $exAction   = 'Mng_plural_'.$actionName;

        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            $this->exClass->$exAction($this);
        }else{
            if(method_exists($this, $actionName)){
                $this->$actionName();
            }else{
                $this->defaultAction();
            }
        }

        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_plural_premain')){
            $this->exClass->mng_plural_premain($this);
        }

        // 親クラスに処理を任せる
        parent::main();
    }


    function setTemplateMng($obj) {
        //------------------------
        // 表示HTMLの設定
        //------------------------
        $this->_processTemplate = "Mng/entry/Mng_edit_plural.html";
        $this->_title = "管理者ページ";
    }


    /* 一括処理種別の設定 */
    function setAction($obj){
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_plural_setAction')){
            return $this->exClass->mng_plural_setAction($this);
        }

        $status = intval($obj->arrForm["set_stat"]);

        // 削除
        if($status == 4){
            $obj->exec_name  = "削除";
            $obj->actionName = "delEntry";
            return;
        }

        // 支払いステータスの変更
        if($status == 5){
            $obj->exec_name  = "ステータス変更";
            $obj->actionName = "chgPaymentStatus";

            if(!isset($obj->arrForm["chg_status"]) || $obj->arrForm["chg_status"] == "") {
                $obj->complete("変更するステータスを選択して下さい。");
            }

            if(!is_numeric($obj->arrForm["chg_status"])) {
                Error::showErrorPage("処理に必要なデータの取得に失敗しました。");
                exit;
            }

            $msg = "変更するステータス：".$GLOBALS["paymentstatusList"][$obj->arrForm["chg_status"]]."<br /><br />";
            $obj->assign("freemsg", $msg);
            return;
        }


        // 採択
        // 初期状態
        // 不採択
        // キャンセル
        $obj->exec_name  = $GLOBALS["entryStatusList"][$status];
        $obj->actionName = "chgStatus";
    }


    function completeAction(){
        if(is_object($this->exClass) && method_exists($this->exClass, $this->actionName)){
            $rs = $this->exClass->{$this->actionName}($this, $this->arrForm["set_stat"]);
        }else{
            $rs = $this->{$this->actionName}($this->arrForm["set_stat"]);
        }

        // 完了メッセージ
        if(!$rs) {
            $this->complete("エントリーの".$this->exec_name."処理に失敗しました。");
        } else {
            $this->complete("エントリーの".$this->exec_name."処理を実行しました。");
        }
    }


    function defaultAction(){
        $arrData = $this->o_entry->getListFromId($this->arrForm["eid"]);
        $this->assign("arrData", $arrData);

        if($arrData === false){
            $this->complete("エントリーが指定されていません。");
        }
    }


    function complete($msg) {
        $this->assign("msg", $msg);
        $this->_processTemplate = "Mng/Mng_complete.html";
        parent::main();
        exit;
    }


    // ステータスの変更
    function chgStatus($new_stat) {
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_plural_chgStatus')){
            $this->exClass->mng_plural_chgStatus($this, $new_stat);
        }

        $objDb = new DbGeneral();

        $param = array();
        $param["status"] = $new_stat;
        $param["udate"] = "NOW";
        // キャンセルの場合はキャンセル日を更新
        if($new_stat == 99){
            $param["cancel_date"] = "NOW";
        }
        // 初期状態に戻す時はキャンセル日をリセット
        if($new_stat == 0){
            $param["cancel_date"] = NULL;
        }

        $from  = "entory_r";
        $where = array();
        $where[] = "eid in (".implode(",", $this->arrForm["eid"]).")";
        $rs = $objDb->update($from, $param, $where);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

        if(!$rs) return false;
        return true;
    }


    // 削除
    function delEntry($dummy="") {
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_plural_delEntry')){
            $this->exClass->mng_plural_delEntry($this);
        }

        $objDb = new DbGeneral();

        $param = array();
        $param["del_flg"] = "1";
        $param["udate"] = "NOW";

        $from  = "entory_r";
        $where = array();
        $where[] = "eid in (".implode(",", $this->arrForm["eid"]).")";
        $rs = $objDb->update($from, $param, $where);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

        if(!$rs) return false;
        return true;
    }


    // 支払いステータスの変更
    function chgPaymentStatus($dummy="") {
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_plural_chgPaymentStatus')){
            $this->exClass->mng_plural_chgPaymentStatus($this);
        }

        $objDb = new DbGeneral();

        $param = array();
        $param["payment_status"] = $this->arrForm["chg_status"];
        $param["udate"] = "NOW";

        $from  = "payment";
        $where = array();
        $where[] = "eid in (".implode(",", $this->arrForm["eid"]).")";
        $rs = $objDb->update($from, $param, $where);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

        if(!$rs) return false;
        return true;
    }

}

/**
 * メイン処理開始
 **/

$c = new plural();
$c->main();







?>