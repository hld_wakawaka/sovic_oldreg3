<?php

include('../../application/cnf/include.php');
include(MODULE_DIR.'custom/Entry.class.php');
include(MODULE_DIR.'Download.class.php');
include(TGMDK_ORG_DIR.'/crypt.class.php');
include_once('../function.php');

/**
 * 管理者TOP
 *
 * @author salon
 *
 */
class entrydetail extends ProcessBase {

    /**
     * コンストラクタ
     */
    function entrydetail(){
        /** ログインチェック */
        LoginMember::checkLoginRidirect();

        $this->eid = isset($_GET["eid"]) ? $_GET["eid"] : "0";
        if(!is_numeric($this->eid) || !$this->eid > 0){
            $this->complete("エントリーが指定されていません。");
        }

        parent::ProcessBase();

        //-------------------------------
        // 初期化
        //-------------------------------
        $this->_title   = "管理者ページ";
        $this->formdata = $GLOBALS["userData"];
        $this->form_id  = $this->formdata["form_id"];
        $this->arrErr   = array();
        $this->onload   = "";

        //-------------------------------
        //ログイン者情報
        //-------------------------------
        $this->assign("user_name", $this->formdata["user_name"]);

        //-------------------------------
        //管理者メニュー取得
        //-------------------------------
        $menu = Mng_function::makeMenu();
        $this->assign("va_menu", $menu);

        // インスタンス生成
        $this->o_entry   = new Entry;
        $this->o_itemini = new item_ini;
        $this->db        = new DBGeneral;
        $this->o_form    = new Form;
        $this->objErr    = New Validate;
        $this->arrForm   = $this->o_form->get($this->form_id);
        $this->_processTemplate = "Mng/entry/Mng_entry_detail.html";


        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_init.class.php');
        Usr_initial::setFormIni($this);
        Usr_initial::setFormData($this);
        Usr_initial::readCustomfiles($this, $this->form_id);
        Usr_initial::setLanguage($this, $GLOBALS["userData"]["lang"]);


        // 拡張クラス読み込み
        $this->exClass = null;
        $isOverride = parent::isOverrideClass($this->form_id, $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }
            // 詳細画面
            if(method_exists($this->exClass, "__constructMngDetail")){
                $this->exClass->__constructMngDetail($this);
            }

            // 項目設定
            if(method_exists($this->exClass, "setFormIni")){
                $this->exClass->setFormIni($this);
            }
        }
        // 外部クラスを読み込み
        $this->svClass = null;
        $isOverride = ProcessBase::isExternalClass($this->form_id, $s);
        if($isOverride && is_object($s)) {
            $this->svClass = $s;
        }
        
        $this->sortFormIni();
        
        
        // @see Usr_initial::setTemplate
        // お支払確認ページの表示/非表示 : デフォルトは表示
        $payment_confirm_disp_flg = "1";
        if(in_array($this->form_id, $this->payment_not)){
            $payment_confirm_disp_flg = "";
        }
        $this->assign("payment_confirm_disp_flg", $payment_confirm_disp_flg);

        $this->assign("formItem",    $this->itemData);    // 項目マスタ
        $this->assign("arrItemData", $this->arrItemData);
    }


    /**
     * メイン処理
     */
    function main(){
    	// カスタマイズテンプレート設定の読み込み @ autoload
        Config::loadSection($this);
        
        // エントリー情報取得
        $this->getEntry();


        //---------------------------------
        // アクション別処理
        //---------------------------------
        $this->mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        $actionName = $this->mode."Action";
        $exAction   = 'Mng_detail_'.$actionName;

        if(is_object($this->exClass) && method_exists($this->exClass, $exAction)){
            $this->exClass->$exAction($this);
        }else{
            if(method_exists($this, $actionName)){
                $this->$actionName();
            }else{
                $this->defaultAction();
            }
        }


        $this->assign("arrData",        $this->arrData);
        $this->assign("arrAff",         $this->arrAff);
        $this->assign("arrPayment",     $this->arrPayment);
        $this->assign("payment_detail", $this->payment_detail);
        $this->assign("arrErr",         $this->arrErr);
        $this->assign("form_id",        $this->form_id);
        $this->assign("onload",         $this->onload);

        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'mng_detail_premain')){
            $this->exClass->mng_detail_premain($this);
        }

        // 親クラスに処理を任せる
        parent::main();
    }


    /**
     * フォーム項目の並び順変更
     */
    function sortFormIni(){
        // 外部クラスを優先
        if(is_object($this->svClass) && method_exists($this->svClass, __FUNCTION__)){
            return $this->svClass->sortFormIni($this);
        }
        // 外部クラスの呼び出し
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->sortFormIni($this);
        }
    }


    function defaultAction(){}

    function downloadAction(){
        $down_num = $_REQUEST["down_num"];
        if($down_num == "") return;

        //ダウンロードファイル
        $this->uploadDir = UPLOAD_PATH."Usr/form".$this->form_id."/".$this->eid."/";    //ファイルアップロードディレクトリ
        $this->file_name =$this->uploadDir.$this->arrData["edata".$down_num];

        if(is_file($this->file_name)){

            //ダウンロード実行
            $wo_download = new Download();
            $wo_download->file($this->file_name, basename($this->file_name) , "");
            exit;
        }
        else{
            $this->objErr->addErr("対象ファイルが存在しません。", "edata".$down_num);
            $this->arrErr = $this->objErr->_err;
        }
    }


    function getEntry(){
        Mng_function::getEntry($this);
    }


    function complete($msg) {
        $this->assign("msg", $msg);
        $this->_processTemplate = "Mng/Mng_complete.html";
        parent::main();
        exit;

    }


    function _chkTerm(){
//        include_once(MODULE_DIR.'entry_ex/Usr_check.class.php');
//        return Usr_Check::_chkTerm($this);
        return true;
    }

}


/**
 * メイン処理開始
 **/
$c = new entrydetail();
$c->main();







?>