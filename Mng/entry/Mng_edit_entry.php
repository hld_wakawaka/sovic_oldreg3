<?php

include('../../application/cnf/include.php');

/**
 * 管理者　エントリー登録・編集
 * 	
 * 
 * @subpackage Mng
 * @author salon
 *
 */
class formbase extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function formbase(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$su_menu[0] = array("menu_name" => "エントリー管理", "prg" => "index.php");
		$su_menu[1] = array("menu_name" => "会議管理", "prg" => "Mng_meetinglist.php");
		$su_menu[2] = array("menu_name" => "メール送信履歴", "prg" => "Mng_mailhistory.php");
		$su_menu[3] = array("menu_name" => "パスワード変更", "prg" => "");
		
		$this->assign("va_menu", $su_menu);

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["act"]) ? $_REQUEST["act"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;


		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Mng/entry/Mng_edit_entry.html";
		$this->_title = "管理者ページ";


		//---------------------------------
		//フォームの種類
		//---------------------------------


		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){
			
			//----------------------
			//確認画面表示
			//----------------------
			case "confirm":
				$this->_processTemplate = "Mng/entry/Mng_edit_entry_confirm.html";
			break;
		
			//----------------------
			//登録・編集処理
			//----------------------

			case "complete":
				$this->_processTemplate = "Mng/Mng_complete.html";

				//完了画面表示
				
				//完了メッセージ
				if($this->arrForm["mode"] == "1"){
					$wk_mode="登録しました。";
					$this->_title = "エントリー情報";
				}
				else{
					$wk_mode = "編集しました。";
					$this->_title = "エントリー情報";
				}
				$this->assign("msg", "エントリー情報を".$wk_mode);

			break;	
			
			/**
			 * 初期表示時
			 */
			default:
			
				if($_REQUEST["entry_id"] == ""){
					$wk_base = array();
				}
				else{

					//ダミーデータ
					$wk_base = array(
									"form_kbn" => isset($_REQUEST["form_kbn"]) ? $_REQUEST["form_kbn"] : "1"
									,"reference" => "東京都港区XXXXXXXX　3-1-1\n　XXXXXX学会事務局\n\nTEL:03-123-4567"
									,"materials" => "1"
									,"item1" => "応募　太郎"
									,"item2" => "おうぼ　たろう"
									,"item3" => "Obo Tarou"
									,"item5" => "1"
									,"item6" => "test@aaa.co.jp"
									,"item7" => "065-0042"
									,"item8" => "北海道札幌市東区本町"
									,"item9" => "001-782-6160"
									,"item10" => ""
									,"item11" => "001-782-4850"
									,"item12" => "クラーク病院整形外科"
									,"item13" => "Dept. of Orthop. Surg., CLARK Hosptal"
									,"item14" => "クラーク病院整形"
									,"item15" => "CLARK Hosptal"
									,"item16" => ""
									,"item17" => ""
									,"item18" => ""
									,"item19" => ""
									,"item20" => ""
									,"item21" => "共著者　はなこ"
									,"item22" => "きょうちょしゃ　はなこ"
									,"item23" => ""
									,"item24" => "2"
									,"item25" => "XXXX大学病院"
									,"item26" => ""
									,"item27" => "候補遺伝子による後縦靭帯骨化症の広範囲原因遺伝子検索"
									,"item28" => "Extermal fixation for fractures in children."
									,"item29" => "論文。論文。論文。論文。論文。論文。論文。論文。論文。"
									
									);
									
//					if($_REQUEST["entry_id"] == "9"){
//						//$wk_base["form_kbn"] = "3";
//						$wk_base["form_name"] = "第20回　○○○○○○学会　参加受けフォーム";
//						$wk_base["comment"] = "必要事項を記入して、お申込みください。";
//					}					
					
				}

				$this->arrForm = $wk_base;				
				$this->arrForm["entry_id"] = $_REQUEST["entry_id"];
									
				
					
			 break;
			
			
			
		}		

	

		
		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 



}

/**
 * メイン処理開始
 **/

$c = new formbase();
$c->main();







?>