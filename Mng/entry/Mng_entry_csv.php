<?php



/**
 * 管理者　CSVダウンロード
 *
 * @author salon
 *
 */
class entry_csv{


	/**
	 * コンストラクタ
	 */
	function entry_csv(){
		LoginMember::checkLoginRidirect();

		//----------------------------
		//インスタンス
		//----------------------------
		$this->o_entry   = new Entry;
		$this->o_itemini = new item_ini();
		$this->objErr    = new Validate;
		$this->db        = new DbGeneral;
		$this->download  = new Download;

		//------------------------------
		//初期化
		//------------------------------
		$this->loop      = 15;
		$this->arrErr    = "";
		$this->delimiter = "\t";
        $this->formdata = $GLOBALS["userData"];
        $this->form_id  = $this->formdata["form_id"];
        $this->fix      = "\"";

		//--------------------------------------------
		//定数情報取得
		//--------------------------------------------
		if($GLOBALS["userData"]["lang"] == LANG_ENG) require_once(ROOT_DIR."application/cnf/en.php");

		//フォームパラメータ
		$this->arrForm = $_REQUEST;

        // 項目初期化クラスを読み込み
        include_once(MODULE_DIR.'entry_ex/Usr_initial.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');
        include_once(MODULE_DIR.'entry_ex/Config.php');
        Usr_initial::setFormIni($this);
        Usr_initial::setLanguage($this, $GLOBALS["userData"]["lang"]);

        // 拡張クラス読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($GLOBALS["userData"]["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMng")){
                $this->exClass->__constructMng($this);
            }

            // フォーム管理者用設定の読み込み
            if(method_exists($this->exClass, "__constructMngCSV")){
                $this->exClass->__constructMngCSV($this);
            }

            // 項目設定
            if(method_exists($this->exClass, "setFormIni")){
                $this->exClass->setFormIni($this);
            }
        }
        // 外部クラスを読み込み
        $this->svClass = null;
        $isOverride = ProcessBase::isExternalClass($GLOBALS["userData"]["form_id"], $s);
        if($isOverride && is_object($s)) {
            $this->svClass = $s;
        }
        
        $this->sortFormIni();

        // カスタマイズテンプレート設定の読み込み
        Config::load($this);
    }


	/**
	 * メイン処理
	 */
	function main(){
	    $obj = $this;
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_main")){
            return $this->exClass->entry_csv_main($this);
        }

		//--------------------------------
		//対象データ取得
		//--------------------------------
		//データ取得（エントリー情報）
		list($wb_ret, $list) = $obj->_getData($obj);
		if(!$wb_ret){
			//終了処理
			return array(false, "出力可能なデータはありません。");
		}


		//-----------------------------
		//CSVヘッダ部分
		//-----------------------------
		$obj->header = $obj->entryMakeHeader($obj);



		//-----------------------------
		//CSVデータ部分
		//-----------------------------
		$obj->csvdata = "";
		foreach($list as $data){
			$obj->csvdata .= $obj->entryMakeData($obj, $data);
		}


		//ヘッダとデータを連結して出力
		$exp_data = $obj->header.$obj->csvdata;

        // 日本語フォームの場合
        if($GLOBALS["userData"]["lang"] == LANG_JPN){
    		$exp_data = mb_convert_encoding($exp_data, "SJIS", "UTF8");
        }

		return array(true, $exp_data);
	}



    /**
     * フォーム項目の並び順変更
     */
    function sortFormIni(){
        // 外部クラスを優先
        if(is_object($this->svClass) && method_exists($this->svClass, __FUNCTION__)){
            return $this->svClass->sortFormIni($this);
        }
        // 外部クラスの呼び出し
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->sortFormIni($this);
        }
    }


	/**
	 * 対象データ取得
	 */
	function _getData($obj){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_getData")){
            return $this->exClass->entry_csv_getData($obj);
        }

		//フォーム頭文字の長さ
		$len_formhead = strlen($GLOBALS["userData"]["head"])  + 1;

		$column = "v_entry.*, to_char(v_entry.rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(v_entry.udate, 'yyyy/mm/dd hh24:mi:ss') as upday,  substr(v_entry.e_user_id, length(v_entry.e_user_id)-4 ,5) as entry_no, to_char(cancel_date, 'yyyy/mm/dd hh24:mi:ss') as cancelday";
        $from      = Mng_function::buildFrom($obj);
        $condition = Mng_function::buildCondition($obj, $obj->arrForm);
        $where     = Mng_function::buildWhere($obj, $condition);

		//------------------------------------
		//表示順
		//------------------------------------
		$wk_order_by = "udate desc";

		if($obj->arrForm["sort_name"] != "" && $obj->arrForm["sort"] != ""){
			$wk_order_by  = "";
			if($obj->arrForm["sort_name"] == "2"){
				$wk_order_by .= "entry_no ";
			}
			else{
				$wk_order_by .= "udate ";
			}

			if($obj->arrForm["sort"] == "1"){
				$wk_order_by .= "desc";
			}
			else{
				$wk_order_by .= "asc";
			}
		}


		$orderby = $wk_order_by;
		//$orderby = "eid";

        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_getData_filter")){
            list($column, $from, $where, $orderby) = $this->exClass->entry_csv_getData_filter($obj, $column, $from, $where, $orderby);
        }

		$list_data = $obj->db->getListData($column, $from, $where, $orderby);
		if(!$list_data){
			return array(false, "");
		}


		//共著者情報を表示する場合
		//エントリーに紐づく共著者情報を取得
		foreach($list_data as $data){

			//共著者以外のデータ
			$wk_entory[$data["eid"]] = $data;

			$wa_aff = $obj->o_entry->getRntry_aff($obj->db, $data["eid"]);

			if(!$wa_aff){
				$wk_entory[$data["eid"]]["chosya"] = array();
			}
			else{
				$wk_entory[$data["eid"]]["chosya"] = $wa_aff;
			}
		}

        // 整形用フィルタ
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_getData_after")){
            return $this->exClass->entry_csv_getData_after($obj, $wk_entory, $where, $orderby);
        }

		return array(true, $wk_entory);


	}




    /** CSVヘッダ生成 */
    function entryMakeHeader($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader")){
            return $this->exClass->entry_csv_entryMakeHeader($obj, $all_flg);
        }

        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        // グループ1ヘッダ
        $groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);

        // グループ2ヘッダ
        $groupHeader[2] = $obj->entryMakeHeader2($obj, $all_flg);

        // グループ3ヘッダ
        $groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);


        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[] = $obj->fix.$GLOBALS["csv"]["status"].$obj->fix;
        $header[] = $obj->fix.$GLOBALS["csv"]["rdate"] .$obj->fix;
        $header[] = $obj->fix.$GLOBALS["csv"]["udate"] .$obj->fix;

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }


    /** CSVヘッダ-グループ1生成 */
    function entryMakeHeader1($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader1")){
            return $this->exClass->entry_csv_entryMakeHeader1($obj, $all_flg);
        }

        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


    /** CSVヘッダ-グループ2生成 */
    function entryMakeHeader2($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader2")){
            return $this->exClass->entry_csv_entryMakeHeader2($obj, $all_flg);
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $loop_cnt = $obj->loop +1;

            for($i=1; $i < $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = strip_tags($_data["item_name"]);

                    $prefix = $obj->fix."共著者".$i." ";

                    $item_id = $_data["item_id"];
                    switch($item_id){
                            case 32:
                                $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                                break;

                            // 共著者　姓名
                            case 33:
                                $groupHeader[$group][] =  $prefix."姓名".$obj->fix;
                                break;

                            // 共著者　姓名カナ
                            case 35:
                                $groupHeader[$group][] =  $prefix."姓名（カナ）".$obj->fix;
                                break;

                            case 34:
                            case 36:
                                break;

                            default:
                                $groupHeader[$group][] =  $prefix.$name.$obj->fix;
                                break;
                    }
                }
            }
        }
        return $groupHeader[$group];
    }


    /** CSVヘッダ-グループ3生成 */
    function entryMakeHeader3($obj, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeHeader3")){
            return $this->exClass->entry_csv_entryMakeHeader3($obj, $all_flg);
        }

        $group = 3;
        $groupHeader = array();
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


    /** CSV出力データ生成 */
    function entryMakeData($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData")){
            return $this->exClass->entry_csv_entryMakeData($obj, $pa_param, $all_flg);
        }
        
        foreach($pa_param as $key=>$data) {
            // 特殊 # 配列の項目の対応
            $item_id = substr($key, 5);
            if(isset($this->formarray) && !empty($this->formarray)){
                if(in_array($item_id, $this->formarray)) continue;
            }
            $pa_param[$key] = str_replace('"', '""', $data);
        }


        // グループ別に生成
        $groupBody = array("1" => array(), "2" => array(), "3" => array());

        // グループ1 データ生成
        $groupBody[1] = $obj->entryMakeData1($obj, $pa_param, $all_flg);

        // グループ2 データ生成
        $groupBody[2] = $obj->entryMakeData2($obj, $pa_param, $all_flg);

        // グループ3 データ生成
        $groupBody[3] = $obj->entryMakeData3($obj, $pa_param, $all_flg);


        $body = $groupBody[1];
        if(count($groupBody[3]) > 0) $body = array_merge($body, $groupBody[3]);
        if(count($groupBody[2]) > 0 && !$all_flg) $body = array_merge($body, $groupBody[2]);
        //状態
        $body[] = ($pa_param["status"] == "0") ? "-" : $obj->fix.$GLOBALS["entryStatusList"][$pa_param["status"]].$obj->fix;
        $body[] = $obj->fix.$pa_param["insday"].$obj->fix;    // 登録日
        $body[] = $obj->fix.$pa_param["upday"] .$obj->fix;    // 更新日

        $ret_buff .= implode($obj->delimiter, $body);
        $ret_buff .= "\n";

        return $ret_buff;
    }


    /** CSV出力データ グループ1生成 */
    function entryMakeData1($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData1")){
            return $this->exClass->entry_csv_entryMakeData1($obj, $pa_param, $all_flg);
        }

        $group = 1;
        $groupBody[$group] = array();
        $groupBody[$group][] = $pa_param["entry_no"];
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            $methodname = "csvfunc".$item_id;
            // カスタマイズあり
            if(method_exists($this->exClass, $methodname)) {
                $wk_body = $this->exClass->$methodname($obj, $group, $pa_param, $item_id);

            // カスタマイズなし
            } else {
                // 任意項目
                if($_data["controltype"] == "1"){
                    $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array("", ","), true);
                    $wk_body = trim($wk_body, ",");

                // 標準項目
                }else{
                    switch($item_id){
                        //会員・非会員
                        case 8:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_kaiin[$pa_param["edata".$item_id]] : "";
                            break;
                        //連絡先
                        case 16:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_contact[$pa_param["edata".$item_id]] : "";
                            break;
                        //都道府県
                        case 18:
                            //英語フォームの場合
                            if($GLOBALS["userData"]["lang"] == "2"){
                                $wk_body =  ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            }
                            else{
                                $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["prefectureList"][$pa_param["edata".$item_id]] : "";
                            }
                            break;
                        //共著者の有無
                        case 29:
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";
                            break;

                        //共著者の所属機関
                        case 31:
                            $wk_kikan_names = array();
                            $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);

                            $i =  "";
                            $set_select = array();
                            foreach($wk_kikanlist as $num => $val){
                                $i = $num+1;
                                $set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
                                $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
                            }
                            $wk_body = str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names));
                            break;

                        //Mr. Mis Dr
                        case 57:
                            if($pa_param["edata".$item_id] != ""){
                                $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
                            }
                            else{
                                $wk_body = "";
                            }
                            break;
                        //国名リストボックス
                        case 114:
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["country"][$pa_param["edata".$item_id]] : "";
                            break;
                        default:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            break;
                    }
                }
            }
            $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
        }
        return $groupBody[$group];
    }


    /** CSV出力データ グループ2生成 */
    function entryMakeData2($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData2")){
            return $this->exClass->entry_csv_entryMakeData2($obj, $pa_param, $all_flg);
        }

        $group = 2;
        $groupBody = array();
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $cnt      = count($pa_param["chosya"]);
            $loop_cnt = $obj->loop +1;

            for($i=1; $i < $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    $item_id = $_data["item_id"];

                    // 共著者の員数を越えた場合はNULL埋め
                    if($cnt < $i){
                        if($item_id == "34") continue;
                        if($item_id == "36") continue;

                        $groupBody[$group][] = $obj->fix.$obj->fix;
                        continue;
                    }

                    $wk_body = "";
                    $chosya  = $pa_param["chosya"][$i-1];

                    // 任意項目
                    if($_data["controltype"] == "1"){
                        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array("", ","), true);
                        $wk_body = trim($wk_body, ",");

                    // 標準項目
                    }else{
                        switch($item_id){
                            //共著者所属期間
                            case 32:
                                if(count($set_select) > 0){
                                    if($chosya["edata".$item_id] != ""){
                                        $chkeck = explode("|", $chosya["edata".$item_id]);
                                        $chkeck_names = array();

                                        foreach($chkeck as $val){
                                            if($val != "" && array_key_exists($val, $set_select)){
                                                $chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $set_select[$val]);
                                            }
                                        }

                                        if(count($chkeck_names) > 0){
                                            $wk_aaa =   implode(",", $chkeck_names);
                                        }

                                    }
                                    else{
                                        $wk_aaa = "";
                                    }

                                    $wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            //共著者姓名
                            case 33:
                                $cyosya_name = "";
                                $wk_mark = array();
                                if($chosya["edata32"] != ""){
                                    $chkeck = explode("|", $chosya["edata32"]);
                                    foreach($chkeck as $val){
                                        $wk_mark[] = $val.")";
                                    }
                                    $cyosya_name = implode(", ", $wk_mark);

                                }
                                $cyosya_name .= $chosya["edata".$item_id]." ".$chosya["edata34"];
                                $wk_body = $cyosya_name;
                                break;

                            //共著者姓名（カナ）
                            case 35:
                                $wk_body = $chosya["edata".$item_id]." ".$chosya["edata36"];
                                break;

                            case 34:
                            case 36:
                                break;


                            //会員・非会員
                            case 41:
                                $wk_body = ($chosya["edata".$item_id] != "") ? $obj->wa_kaiin[$chosya["edata".$item_id]] : "";
                                break;

                            //Mr. Mis Dr
                            case 61:
                                if($chosya["edata".$item_id] != ""){
                                    $wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            case 46:
                            case 47:
                            default:
                                $wk_body = $chosya["edata".$item_id];
                                break;
                        }
                    }
                    if(strlen($wk_body) == 0) continue;
                    $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
                }
            }
        }
        return $groupBody[$group];
    }


    /** CSV出力データ グループ3生成 */
    function entryMakeData3($obj, $pa_param, $all_flg=false){
        if(is_object($this->exClass) && method_exists($this->exClass, "entry_csv_entryMakeData3")){
            return $this->exClass->entry_csv_entryMakeData3($obj, $pa_param, $all_flg);
        }

        $group = 3;
        $groupBody = array();
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            $methodname = "csvfunc".$item_id;
            // カスタマイズあり
            if(method_exists($this->exClass, $methodname)) {
                $wk_body = $this->exClass->$methodname($obj, $group, $pa_param, $item_id);

            // カスタマイズなし
            } else {
                // 任意項目
                if($_data["controltype"] == "1"){
                    $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array("", ","), true);
                    $wk_body = trim($wk_body, ",");

                // 標準項目
                }else{
                    switch($item_id){
                        //発表形式
                        case 48:
                            $keisiki = "";
                            if($pa_param["edata".$item_id] != ""){
                                if($obj->itemData[$item_id]["select"]){
                                    $keisiki = $obj->itemData[$item_id]["select"][$pa_param["edata".$item_id]];
                                }
                            }
                            $wk_body = str_replace(array("\r\n","\n","\r"), '', $keisiki);
                            break;

                        default:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            break;
                    }
                }
            }
            $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
        }
        return $groupBody[$group];
    }


    function _chkTerm(){
//        include_once(MODULE_DIR.'entry_ex/Usr_check.class.php');
//        return Usr_Check::_chkTerm($this);
        return true;
    }

}


?>