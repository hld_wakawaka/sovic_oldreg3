<?php

include('../../application/cnf/include.php');

class login extends ProcessBase {

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function login() {
		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 * @access	public
	 */
	function main() {
	
		$this->arrForm = $_REQUEST;
		$this->arrForm['returnurl'] = isset($_REQUEST['return']) ? $_REQUEST['return'] : "";
		if(strlen($_REQUEST['return']) > 0){
		    $this->arrForm['returnurl'] .= isset($_REQUEST['form_id']) ? "&form_id=".$_REQUEST['form_id'] : "";
		}
		
		// 表示HTMLの設定
		$this->_processTemplate = "Mng/login/index.html";
		$this->_title = "管理者ログイン";
		
		$form_id = $GLOBALS["userData"]["form_id"];

		switch($_REQUEST["mode"]) {
		
			case "login":
			
				$arrErr = $this->_check();
				
				if(count($arrErr) > 0) {
					break;
				}
				
				//ログイン処理
				$err = LoginMember::doLogin($_REQUEST["login_id"], $_REQUEST["password"]);
				
				if(!empty($err)) {
					//エラーの場合
					$arrErr = $err;
					$GLOBALS["log"]->write_log("mng_login failed", $_REQUEST["login_id"]);
					
				} else {
					
					/*
					if($this->arrForm["save"] == "1") {
						$this->_insert();
					} else {
						$this->_del();
					}*/
					
					$GLOBALS["log"]->write_log("mng_login Success", $GLOBALS["userData"]["form_id"]);
				
					if($this->arrForm['returnurl'] == "") {
						header("location: ".MNG_URL);
					} else {
						header("location: ".$this->arrForm['returnurl']);
						$GLOBALS["session"]->unsetVar('returnurl');
					}
					exit;
				
				}
			
				break;
				
			case "logout":
			
				LoginMember::doLogout();
				
				unset($this->arrForm);
				
				/*
				if (isset($_COOKIE[session_name()])) {
					setcookie(session_name(), '', time()-42000, '/');
				}
				*/
				
				//$this->_get();
				
				$this->_title = "ログアウト";
				
				$GLOBALS["log"]->write_log("mng_logout", form_id);
				
				$this->assign("msg", "ログアウト完了。ご利用ありがとうございました。");
				
				break;
			
			default:
				//$this->_get();
	
				// 表示HTMLの設定
				$this->_processTemplate = "Mng/login/index.html";
				
				$this->assign("msg", "ID、パスワードを入力して、ログインしてください。");
			
				break;
			
		}
		
		$this->assign("arrErr", $arrErr);
		
		// 親クラスに処理を任せる
		parent::main();
		
	}
	
	function _check() {
	
		$objErr = New Validate;
	
		if(!$objErr->isNull($this->arrForm["login_id"])) {
			$objErr->addErr(sprintf(ERR_REQUIRE_INPUT_PARAM, "ログインID"), "loginid");
		}
		if(!$objErr->isNull($this->arrForm["password"])) {
			$objErr->addErr(sprintf(ERR_REQUIRE_INPUT_PARAM, "パスワード"), "password");
		}
		
		return $objErr->_err;
	
	}
	
	function _insert() {
		
		$db = new DbGeneral;
		
		$uniq_key = session_id();
		
		$param["name"] = $this->arrForm["login_id"];
		$param["phrase"] = $this->arrForm["password"];
		$param["key"] = $uniq_key;
		
		$db->insert("sess_log_admin", $param, __FILE__, __LINE__);
		
		setcookie("irssa", $uniq_key, time() + 30 * 24 * 3600);	//30日後
		
		$where[] = "key <> ".$db->quote($uniq_key)."";
		$where[] = "name = ".$db->quote($this->arrForm["login_id"])."";
		$where[] = "phrase = ".$db->quote($this->arrForm["password"])."";
		
		$db->delete("sess_log_admin", $where, __FILE__, __LINE__);
		
		return;
	}
	
	function _get() {
		
		if(!isset($_COOKIE["irssa"])) {
			return;
		}
		
		$db = new DbGeneral;
		
		$where[] = "key = ".$db->quote($_COOKIE["irssa"])."";
		$where[] = "rdate > ".$db->quote(date('Y-m-d H:i:s', strtotime("-30 day")))."";
		
		$rs = $db->getData("*", "sess_log_admin", $where, __FILE__, __LINE__);
		
		if(!$rs) return;
		
		$this->arrForm["login_id"] = $rs["name"];
		$this->arrForm["password"] = $rs["phrase"];
		$this->arrForm["save"] = "1";
		
		return;
		
	}
	
	function _del() {
		
		if(!isset($_COOKIE["irssa"])) {
			return;
		}
		
		$db = new DbGeneral();
		
		$where[] = "key = ".$db->quote($_COOKIE["irssa"])."";
		
		$rs = $db->delete("sess_log_admin", $where, __FILE__, __LINE__);
		
		setcookie("irssa");
		
		return;
	}

}

/**
 * メイン処理開始
 **/

$c = new login();
$c->main();

?>