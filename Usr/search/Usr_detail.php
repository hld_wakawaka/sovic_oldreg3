<?php

include('../../application/cnf/include.php');
include('../form/function.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once(MODULE_DIR.'custom/Entry.class.php');

/**
 * 検索ページ
 *   inluce/form[].phpのみを対象
 * 
 * @author hld.doi
 *
 */
class search extends ProcessBase {

    var $expandpreffix = "Usr_Detail_";

    /**
     * コンストラクタ
     */
    function search(){
        // CITE JAPANで利用を想定 # あとから対応できるように明示的に指定する
        $_GET['form_id'] = 55;
        $_REQUEST['form_id'] = 55;
        $_POST['form_id'] = 55;


        parent::ProcessBase();

        //初期化
        $this->arrErr    = array();
        $this->admin_flg = "";   // 管理者モードフラグ
        $this->onload    = "";

        //インスタンス生成
        $this->o_form    = new Form();
        $this->o_entry   = new Entry();
        $this->objErr    = New Validate;
        $this->db        = new DbGeneral;
        $this->o_itemini = new item_ini();
        $this->arrForm   = $_POST;
        $this->eid       = isset($_GET['eid']) ? $_GET['eid'] : NULL;

        // 強制的にメンテナンスとするフォーム
        $this->mente_form = array();

        // フォーム項目を配列とする特殊項目
        $this->formarray = array();

        // jQuery読み込みフラグ
        $this->useJquery = false;

        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }

        // アップロードファイルの添付IDと添え字
        Usr_init::_initFile($this);

        // 外部クラスを読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($this->o_form->formData["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
        }

        // 将来的に利用
        if(is_null($this->exClass)){
            Error::showErrorPage(ERR_DB_SYSTEM);
        }

        // 共通で実行するコンストラクタ
        $method = $this->expandpreffix.'construct';
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            $this->exClass->$method($this);
        }
    }


    /**
     * メイン処理
     */
    function main(){
        if(!isset($this->arrForm["mode"])) $this->arrForm["mode"] = "default";

        // 処理アクション
        $actionName = $this->expandpreffix.$this->arrForm["mode"]."Action";
        if(is_object($this->exClass) && method_exists($this->exClass, $actionName)){
            $this->exClass->$actionName($this);
        }else{
            Error::showErrorPage(ERR_DB_SYSTEM);
        }


        $this->assign("arrErr",  $this->arrErr);
        $this->assign("form_id", $GLOBALS["form"]->formData["form_id"]);

        // 親クラスをする前の前処理
        $method = $this->expandpreffix.'premain';
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            $this->exClass->$method($this);
        }

        // 親クラスに処理を任せる
        parent::main();
    }


}

/**
 * メイン処理開始
 **/

$c = new search();
$c->main();






?>