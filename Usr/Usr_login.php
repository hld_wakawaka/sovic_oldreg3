<?php

include('../application/cnf/include.php');

/**
 * 利用者ログイン
 * 
 * @author salon
 *
 */
class login extends ProcessBase {

    public $expandpreffix = 'usr_login_';
    public $arrErr    = array();
    public $useJquery = false;

    /**
     * コンストラクタ
     */
    function login(){
        parent::ProcessBase();

        $this->_title = $GLOBALS["form"]->formData["lang"] == "2" ? "Login" : "ログイン";
        $this->_processTemplate = "Usr/Usr_login.html";

        $this->arrForm = $_REQUEST;
        if(!$GLOBALS["form"]->isForm) Error::showErrorPage("ページが存在しません。");

        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }

        // 外部クラスを読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($GLOBALS["form"]->formData["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
        }

        // 親クラスをする前の前処理
        $method = $this->expandpreffix.'construct';
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            $this->exClass->$method($this);
        }

        // jQuery読み込みフラグ
        $this->assign('useJquery',   $this->useJquery);

        // @deprecated Usr_initial::commonAssign
        $this->assign("formWord", $GLOBALS["form"]->formWord);

        $url = './form/Usr_entry.php?form_id='.$GLOBALS["form"]->formData["form_id"];
        $login_msg = '※ID、パスワードをお持ちでない方は&nbsp;<a href="'.$url.'">こちら</a>&nbsp;から';
        $this->assign("login_msg", $login_msg);
    }


    /**
     * メイン処理
     */
    function main(){
        // メンテナンス期間チェック
        $this->isMaintenance();

        // 受付期間チェック
        $this->checkValidDate();

        if(!isset($this->arrForm["mode"])) $this->arrForm["mode"] = "";

        // 処理アクション
        $actionName = $this->arrForm["mode"]."Action";
        if(method_exists($this, $actionName)){
            $this->$actionName();
        }else{
            $this->defaultAction();
        }


        $this->assign("arrErr",  $this->arrErr);
        $this->assign("form_id", $GLOBALS["form"]->formData["form_id"]);

        // 親クラスをする前の前処理
        $method = $this->expandpreffix.'premain';
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            $this->exClass->$method($this);
        }

        // 親クラスに処理を任せる
        parent::main();
    }


    /* Action # ログイン */
    function loginAction(){
        // includeを優先する
        $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this);
        }

        $this->arrErr = $this->_check();
        if(count($this->arrErr) > 0) return;

        // ログイン処理
        $err = LoginEntry::doLogin($GLOBALS["form"]->formData["lang"], $_REQUEST["login_id"], $_REQUEST["password"]);

        // エラーの場合
        if(!empty($err)) {
            $this->arrErr[] = $err;
            $GLOBALS["log"]->write_log("euser_login failed", $_REQUEST["login_id"]);
            return;
        }

        $GLOBALS["log"]->write_log("euser_login Success", $GLOBALS["entryData"]["eid"]);

        $url = "./form/Usr_entry.php?form_id=".$GLOBALS["form"]->formData["form_id"];
        header("location: ".$url);
        exit;
    }


    /* Action # ログアウト */
    function logoutAction(){
        // includeを優先する
        $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this);
        }

        $eid = $GLOBALS["entryData"]["eid"];

        LoginEntry::doLogout();
        unset($this->arrForm);

        $this->arrForm = array();
        $this->_title  = $GLOBALS["form"]->formData["lang"] == "2" ? "Logout" : "ログアウト";

        $GLOBALS["log"]->write_log("euser_logout", $eid);

        $this->assign("msg", $GLOBALS["msg"]["msg_logout"]);
    }


    /* Action # デフォルト */
    function defaultAction(){
        // includeを優先する
        $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this);
        }

        $this->arrErr = $GLOBALS["session"]->getVar("error");
        $this->assign("msg", $GLOBALS["msg"]["msg_login"]);
    }


    function _check() {
        // includeを優先する
        $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this);
        }

        $objErr = New Validate;

        if(!$objErr->isNull($this->arrForm["login_id"])) {
            $objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $GLOBALS["msg"]["login_id"]));
        }
        if(!$objErr->isNull($this->arrForm["password"])) {
            $objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $GLOBALS["msg"]["login_passwd"]));
        }

        return $objErr->_err;
    }


    // 受付期間のチェック
    function checkValidDate(){
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, 'usr_login_checkValidDate')){
            return $this->exClass->usr_login_checkValidDate($this);
        }

        $GLOBALS["form"]->checkValidDate();
    }


    // @see Usr_initial::showMenteform
    function isMaintenance(){
        $isMainte = false;
        $date1 = strtotime($GLOBALS["form"]->formData["maintenance_date1"]);
        $date2 = strtotime($GLOBALS["form"]->formData["maintenance_date2"]);
        $now   = strtotime(date("Y-m-d H:i:s"));
        // メンテナンス時刻設定あり
        if(0 < strlen($date1) || 0 < strlen($date2)){
            if(0 < strlen($date1) && 0 < strlen($date2)){
                if($date1 <= $now && $now <= $date2){
                    $isMainte = true;
                }
            }
            if(0 < strlen($date1) && strlen($date2) == 0){
                if($date1 <= $now){
                    $isMainte = true;
                }
            }
            if(strlen($date1) == 0 && 0 < strlen($date2)){
                if($now <= $date2){
                    $isMainte = true;
                }
            }
        }

        if($isMainte){
            // メンテナンス開始日時
            $mdate1 = strlen($GLOBALS["form"]->formData["maintenance_date1"]) > 0
                    ? date("Y年m月d日 H時i分", strtotime($GLOBALS["form"]->formData["maintenance_date1"]))
                    : '';

            // メンテナンス終了日時
            $mdate2 = strlen($GLOBALS["form"]->formData["maintenance_date2"]) > 0
                    ? date("Y年m月d日 H時i分", strtotime($GLOBALS["form"]->formData["maintenance_date2"]))
                    : '未定';

            $mainteMsg = sprintf($GLOBALS["msg"]["maintenance"], $mdate1, $mdate2);
            Error::showErrorPage($mainteMsg);
        }
    }

}

/**
 * メイン処理開始
 **/

$c = new login();
$c->main();






?>