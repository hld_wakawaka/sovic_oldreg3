<?php

/**
 * reg3form12番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.01.24
 * 
 */
class Usr_Entry12 {

    function __construct($obj){
        // 支払方法を削除
        unset($GLOBALS["method_J"][1]);
        unset($GLOBALS["method_J"][2]);
    }


    /** 開発用のデバッグ関数 */
    function developfunc($obj) {
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody(1396, "11", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * 1ページ目 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init1($obj){
        $keys = Usr_init::_init1($obj);

        $key = NULL;
        $c = count($keys);
        for($i=0; $i<$c; $i++){
            if($keys[$i][0] == "method"){
                $key = $i;
                break;
            }
        }
        // お支払方法を削除
        if(!is_null($key)){
            unset($keys[$key]);
        }

        return $keys;
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        Usr_Check::_check1($obj);

        // 会員番号は10桁制限
        $len = strlen($obj->arrParam["edata26"]);
        if($len > 0 && $len != 10){
            $obj->objErr->addErr("会員番号は10桁で入力してください。", "edata26");
        }
        if($len == 10){
            if($obj->arrParam["amount"] != 0){
                $obj->objErr->addErr("参加費は会員を選択してください。", "amount");
            }
        }

        // 会員の方は会員番号を必須
        if($obj->arrParam["amount"] == 0){
            if(strlen($obj->arrParam["edata26"]) == 0){
                $name = Usr_init::getItemInfo($obj, "26");
                $method = Usr_init::getItemErrMsg($obj, "26");
                $obj->objErr->addErr(sprintf($method, $name), "edata26");
            }
        }
    }


    /** ページ遷移ベース */
    function pageAction($obj) {
        Usr_pageAction::pageAction($obj);

        // 合計金額#参加登録で決済利用
        $obj->total_price = ($obj->eid == "")
                          ? Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price)
                          : $GLOBALS["session"]->getVar("ss_total_payment");
        $obj->assign("total_price", $obj->total_price);
        $obj->arrForm["total_price"] = $obj->total_price;
    }


    /** 1ページ目 */
    function pageAction1($obj) {
        $fix_flg = "";

        // 学生：3ページ目を使用
        if($obj->arrParam["amount"] == 2){
            $obj->block = "3";
            $fix_flg = "1";
            $obj->_processTemplate = "Usr/form/Usr_entry.html";
        }

        //上記のいずれも該当しなかった場合
        if($fix_flg == ""){
            $obj->block = "4";
            $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
        }

        // お支払いに関する表記は非表示
        $obj->assign("payment_confirm_disp_flg", 0);
    }


    /** 戻るボタン */
    function backAction($obj) {
        Usr_pageAction::backAction($obj);

        $obj->block = 1;
        $obj->_processTemplate = "Usr/form/Usr_entry.html";

        // 確認ページからの戻り
        if($obj->wk_block == 4){
            // 学生：3ページ目を使用
            if($obj->arrForm["amount"] == 2){
                $obj->block = "3";
            }
        }
    }


    /** 確認ページ */
    function confirmAction($obj) {
        Usr_pageAction::confirmAction($obj);

        // お支払いに関する表記は非表示
        $obj->assign("payment_confirm_disp_flg", 0);
        var_dump("aa");
    }


    function makePaymentBody($obj, $exec_type){
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //支払合計
        if($exec_type == "1"){
            $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        }else{
            $total = $GLOBALS["session"]->getVar("ss_total_payment");
        }

        $obj->point_mark = "■";
        $body_pay = "\n\n【お支払情報】\n\n";
        $body_pay .= $obj->point_mark."金額: \n";

        // Fee
        $body_pay .= $obj->makePaymentBody1($exec_type);

        //その他決済がある場合
        $body_pay .= $obj->makePaymentBody2($exec_type);

        // 合計金額
        $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";


        return $body_pay;
    }

}
