<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry80 {


//    /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj)
    {
       $obj->ishideGdDate = $this->checkhideGdDate($obj);
       $obj->ishideCeDate = $this->checkhideCeDate($obj);
       $obj->isHideItem   = $this->checkhideItem($obj);
    }
    
    
    /**
     * [reg3-form80] 見積No.13:エリアの非表示設定(設定ファイル)
     * 非表示設定ファイルは「hideGdDate.txt」非表示しない場合は、hideGdDate1.txt」等別のファイル名にしておくか、別のパスに移す
     * 
     * */
    function checkhideGdDate($obj)
    {
        if(isset($_GET["admin_flg"]) && strlen($_GET["admin_flg"]) > 0) return false;
        
        // 今日の日付を取得
        $today = date("Y-m-d H:i");
        
        // 非表示設定ファイルの取得
        $config = ROOT_DIR.'customDir/80/external/display/hideGdDate.txt';
        if(file_exists($config)) {
            $arrConfig = parse_ini_file($config);
        
            return strtotime($today) > strtotime($arrConfig['hideGdDate']);
        }
        return false;
   }


   /**
     * [reg3-form80] 見積No.13:エリアの非表示の対象項目
     * 
     * */
    function itemhideGd()
    {
        // 非表示対象項目
        return array(122,123,124,125,126,127);
    }



    /**
     * // [reg3-form80] 見積No.16:エリアの非表示設定(設定ファイル)
     * 非表示設定ファイルは「hideCeDate.txt」非表示しない場合は、hideCeDate1.txt」等別のファイル名にしておくか、別のパスに移す
     * 
     * */
    function checkhideCeDate($obj)
    {
        if(isset($_GET["admin_flg"]) && strlen($_GET["admin_flg"]) > 0) return false;
        
        // 今日の日付を取得
        $today = date("Y-m-d H:i");
        
        // 非表示設定ファイルの取得
        $config2 = ROOT_DIR.'customDir/80/external/display/hideCeDate.txt';
        if(file_exists($config2)) {
            $arrConfig = parse_ini_file($config2);

            return strtotime($today) > strtotime($arrConfig['hideCeDate']);
        }
        return false;
    }


   /**
     * [reg3-form80] 見積No.16:エリアの非表示の対象項目
     * 
     * */
    function itemhideCe()
    {
       // 非表示対象項目
        return array(128,129,130);
    }


    /**
     * No.23:特定の期間中はHopeId,Name項目以外は非表示
     * 
     * */
    function checkhideItem($obj)
    {
        // 影響範囲をUsrのみに限定
        if(PAGE_RANK != "usr") return false;
        
        if(isset($_GET["admin_flg"]) && strlen($_GET["admin_flg"]) > 0) return false;
        
        // 今日の日付を取得
        $today = date("Y-m-d H:i");
        
        // 非表示設定ファイルの取得
        $config = ROOT_DIR.'customDir/80/external/display/hideItem.txt';
        if(file_exists($config)) {
            $arrConfig = parse_ini_file($config);
        
            return strtotime($today) > strtotime($arrConfig['hideItem']);
        }
        return false;
   }


   /**
     * No.23:特定の期間中はHopeId,Name項目以外は非表示
     *   # 影響を受けない項目マスタ
     * 
     * */
    function itemNohide()
    {
        return array(26,  2,  7,  1);
    }


//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    function sortFormIni($obj) {
        $arrGroup1 = & $obj->arrItemData[1];
        
        // ソート順
        $arrSort = array(
            26,  2,  7,  1, 57, 28,  8, 63, 64, 16, 69, 70, 71, 59, 10, 18, 13, 14,114, 60, 72, 73, 24, 74, 58, 17, 27, 75, 76, 21,77,25,78,79
            ,80,81,82,83,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144
        );
        if($obj->isHideItem){
            $arrSort = $this->itemNohide();
        }
        
        // No.7 # 入れ替え
        $array = array();
        foreach($arrSort as $_key => $_item_id){
            $array[$_item_id] = $arrGroup1[$_item_id];
        }
        $arrGroup1 = $array;
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj) {
        // [reg3-form80] 見積No9 項目「Date of birth」の値について[{年}Year {月}Month {日}Date]のレイアウトで表示
        $birthDates = array(
            63,64,69
        );
        $obj->assign("birthDateItems", $obj->arrItemData[1]);
        $obj->assign("birthDates", $birthDates);
        
        // [reg3-form80] 見積No9 項目「Date of birth」の値について、項目64,69は非表示(Usr入力、Usr確認)
        $obj->arrItemData[1][64]['disp'] = 1;
        $obj->arrItemData[1][69]['disp'] = 1;
        
        
        // [reg3-fotm80] 見積No.21 標準項目「Prefix」の選択肢から「Prof.」を非表示
        $obj->arrItemData[1][57]['item_type'] = "2";
        $obj->arrItemData[1][57]['select'] = array(
                1 => "Prof.",
                2 => "Dr.",
                3 => "Mr.",
                4 => "Ms."
        );
        $obj->assign("_formItem", $obj->arrItemData[1][57]);
        
        
        //---------------------------------------------------
        // [reg3-form80] 見積No.13:エリアの非表示設定
        //---------------------------------------------------
        $gdKey = $this->itemhideGd();
        if($obj->ishideGdDate){
           foreach($gdKey as $item_id) {
               $obj->arrItemData[1][$item_id]['disp'] = '1';
               unset($obj->arrItemData[1][$item_id]);
           }
       }
       
        //---------------------------------------------------
        // [reg3-form80] 見積No.16:エリアの非表示設定
        //---------------------------------------------------
        $gdKey2 = $this->itemhideCe();
        if($obj->ishideCeDate){
            foreach($gdKey2 as $item_id) {
                $obj->arrItemData[1][$item_id]['disp'] = '1';
            }
        }
    }


    function setMoneyPaydata($obj)
    {
        Usr_initial::setMoneyPaydata($obj);
        
        //---------------------------------------------------
        // 追加No.1 # ファイル1を利用可能にする
        //---------------------------------------------------
        // 決済なしのフォームであればDBに保存されないためファイル1が表示されるようにダミーデータを設定
        if(method_exists($obj, "assign")){
            $obj->filecheck[] = "999";
            $obj->assign("filecheck", $obj->filecheck);
        }
    }


    /**
     * セッションパラメータ取得
     * 画面入力データをセッションから取得する
     */
    function getDispSession($obj){
        $obj->arrForm = array_merge($GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
        $obj->arrForm = array_merge($obj->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));
        $obj->arrForm = array_merge($obj->arrForm, (array)$GLOBALS["session"]->getVar("form_parampay"));

        if(isset($obj->arrForm["edata31"])) {
            $wa_kikanlist = $obj->_makeListBox($obj->arrForm["edata31"]);
            $obj->arrForm["kikanlist"] = $wa_kikanlist;
        }
        
        //---------------------------------------------------
        // 追加No.1 # ファイル1を利用可能にする
        //---------------------------------------------------
        // 決済なしのフォームであればDBに保存されないためファイル1が表示されるようにダミーデータを設定
        $obj->arrForm["amount"] = "999";
        $obj->assign("arrForm", $obj->arrForm);
    }


    function __constructMngDetail($obj){
        // [reg3-form80] 見積No9 項目「Date of birth」の値について、項目64,69は非表示(Mng詳細)
        $obj->arrItemData[1][64]['disp'] = '1';
        $obj->arrItemData[1][69]['disp'] = '1';
    }


/*
    function __constructMngCSV($obj) {
        // [reg3-form80] 見積No9 項目「Date of birth」の値について、項目64,69は非表示(CSV)
        $obj->arrItemData[1][64]['item_view'] = '1';
        $obj->arrItemData[1][69]['item_view'] = '1';
    }
*/

    // エラーチェック # ブロック1
    function _check1($obj) {
        //---------------------------------------------------
        // [reg3-form80] 見積No.13:非表示項目のエラーチェックを行わない
        //---------------------------------------------------
        $gdKey = $this->itemhideGd();
        if($obj->ishideGdDate){
            foreach($gdKey as $item_id) {
                unset($obj->arrItemData[1][$item_id]);
            }
        }
       
        //---------------------------------------------------
        // [reg3-form80] 見積No.16:エリアの非表示設定
        //---------------------------------------------------
        $gdKey2 = $this->itemhideCe();
        if($obj->ishideCeDate){
            foreach($gdKey2 as $item_id) {
                unset($obj->arrItemData[1][$item_id]);
            }
        }
       
       Usr_Check::_check1($obj);
       
       $group_id = 1;
       
       // Fee
       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
       
        //---------------------------------------------------
        // [reg3-form80] 見積No.11 「Country/Area」項目で『Japan』以外を選択時、Flight Scheduleエリアを必須
        //---------------------------------------------------
        $item_id = 75;
        $key = "edata" . $item_id;
        
        if($obj->arrParam [$key] != '') {
            if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] != '8') {
                $array = array(81, 82, 83, 115, 116, 117, 118, 119, 120, 121);
                foreach($array as $_key => $item_id){
                    $key = "edata" . $item_id;
                    if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                        $name = Usr_init::getItemInfo($obj, $item_id);
                        $method = Usr_init::getItemErrMsg($obj, $item_id);
                        $obj->objErr->addErr(sprintf($method, $name), $key);
                    }
                }
            }
        }
       
       
       // [reg3-form80] 見積No.14「帯名：Group Discussion」追加エラー設定 > 1st choice ～ 3rd choice
       
       // チェック対象項目
       $ch_item_id = array(122,124,126);
       
       // 対象項目の未入力チェック
       foreach($ch_item_id as $item_id) {
           $nullCheck[] = $obj->arrParam ["edata".$item_id] != "" ? 'T' : 'F';
       }
       
       $nullCheckCount = array_count_values($nullCheck);
       
       $cKey1_1 = "edata".$ch_item_id[0];
       $cKey1_2 = "edata".$ch_item_id[1];
       $cKey1_3 = "edata".$ch_item_id[2];
       
       // 未入力じゃない項目が２つ以上あればエラーチェック
       if($nullCheckCount['T'] >= 2) {
           if($obj->arrParam [$cKey1_1] == $obj->arrParam [$cKey1_2] ||
              $obj->arrParam [$cKey1_2] == $obj->arrParam [$cKey1_3] ||
              $obj->arrParam [$cKey1_1] == $obj->arrParam [$cKey1_3])
           {
               $obj->objErr->addErr("Each lecturer can be selected only once.", $cKey1_1);
           }
       }
       
       
       // [reg3-form80] 「帯名： Culture Experience Program」追加エラー設定 > 1st choice ～ 3rd choice
       
       // チェック対象項目
       $ch_item_id2 = array(128,129,130);
       
       // 対象項目の未入力チェック
       foreach($ch_item_id2 as $item_id) {
           $nullCheck2[] = $obj->arrParam ["edata".$item_id] != "" ? 'T' : 'F';
       }
       
       $nullCheckCount2 = array_count_values($nullCheck2);
       
       $cKey2_1 = "edata".$ch_item_id2[0];
       $cKey2_2 = "edata".$ch_item_id2[1];
       $cKey2_3 = "edata".$ch_item_id2[2];
       
       // 未入力じゃない項目が２つ以上あればエラーチェック
       if($nullCheckCount2['T'] >= 2) {
           if($obj->arrParam [$cKey2_1] == $obj->arrParam [$cKey2_2] ||
           $obj->arrParam [$cKey2_2] == $obj->arrParam [$cKey2_3] ||
           $obj->arrParam [$cKey2_1] == $obj->arrParam [$cKey2_3])
           {
               $obj->objErr->addErr("Each program can be selected only once.", $cKey2_1);
           }
       }
       
       
       // [reg3-form80] 
   }
   
   function _check3($obj){
       $group_id = 3;
   
       //ファイルアップロードチェック
       foreach($obj->arrfile as $item_id => $n){
           //$obj->_checkfile($obj->arrParam, $group_id, $item_id, "1");
           $pa_formparam = $obj->arrParam;
           $mode = "1";
           //一般ユーザモードの場合にチェック
           if($obj->admin_flg != "") return;
           
           // 1ページ目の入力情報
           $arrForm = $GLOBALS["session"]->getVar("form_param1");
           
           // 非表示はチェックしない
           if($obj->arrItemData[$group_id][$item_id]["disp"] == "1") return;
           
           $key = "edata".$item_id;
           
           // 必須チェック
           if($mode == "1" && ($obj->arrItemData[$group_id][$item_id]["need"] == "1" || ($item_id == "51" && in_array($arrForm["amount"], $obj->filecheck)))){
               // 未指定
               if($pa_formparam["hd_file".$item_id] == "" && $_FILES[$key]["size"] == 0){
                   $msg = ($obj->formdata["lang"] == LANG_JPN)
                   ? $obj->arrItemData[$group_id][$item_id]['strip_tags']."を指定してください。"
                       : "Specify ".$obj->arrItemData[$group_id][$item_id]['strip_tags'].".";
                   $obj->objErr->addErr($msg, $key);
               }
           
               // 必須項目を削除し新しいファイルを未指定
               if($_FILES[$key]["size"] == 0 && isset($pa_formparam["file_del".$item_id])
                   && $pa_formparam["file_del".$item_id] == 1
                   && (
                       $pa_formparam["hd_file".$item_id] == $pa_formparam["n_data".$item_id]
                       || $item_id == "51" && in_array($arrForm["amount"], $obj->filecheck)
                   )
               ){
                   $msg = ($obj->formdata["lang"] == LANG_JPN)
                   ? $obj->arrItemData[$group_id][$item_id]['strip_tags']."は必須です。削除する場合は、別のファイルを指定してください。"
                       : $obj->arrItemData[$group_id][$item_id]['strip_tags']." must be included. Select another file for deletion.";
                   $obj->objErr->addErr($msg, $key);
               }
           }
           
           //サイズチェックを行う場合
           $pos = strstr($obj->arrItemData[$group_id][$item_id]["item_check"],"1");
           if($mode == "" && $pos !== false){
               if($obj->arrItemData[$group_id][$item_id]["item_len"] < $_FILES[$key]["size"]){
                   if($obj->formdata["lang"] == LANG_JPN){
                       $msg = $obj->arrItemData[$group_id][$item_id]['strip_tags']."が".$obj->arrItemData[$group_id][$item_id]["item_len"]."byteを超えています。";
                   }
                   else{
                       $msg = $obj->arrItemData[$group_id][$item_id]['strip_tags']." cannot exceed ".$obj->arrItemData[$group_id][$item_id]["item_len"]." byte.";
                   }
           
           
                   $obj->objErr->addErr($msg, $key);
               }
           }
           
           // 拡張子チェック
           if($mode == "1" && strlen($obj->arrItemData[$group_id][$item_id]['item_select']) > 0){
               $arrExt = Usr_function::gf_to_array_unique_trim($obj->arrItemData[$group_id][$item_id]['item_select']);
               if(!Validate::checkExt($_FILES[$key], $arrExt)){
                   $strExt = implode(", ", $arrExt);
           
                   $msg = ($obj->formdata["lang"] == LANG_JPN)
                   ? $obj->arrItemData[$group_id][$item_id]['strip_tags']."の拡張子は「".$strExt."」でアップロードしてください。"
                       : "Please upload ". $obj->arrItemData[$group_id][$item_id]['strip_tags']." in ". preg_replace("/,(.*?)$/u", " or $1", $strExt) ." file."
                           ;
           
                           $obj->objErr->addErr($msg, $key);
               }
           }
           
       }
       
       // 管理者はしない
       if($obj->admin_flg == "1") return;
       
       //
   
       $obj->objErr->check($obj->_init($group_id), $obj->arrParam);
       $obj->_checkNini($group_id);
   }
   
   
   // ------------------------------------------------------
   // ▽MAILカスタマイズ
   // ------------------------------------------------------
   function makeFormBody($obj, $exec_type){
       $arrbody = array();
   
       // 言語別設定
       $obj->point_mark = ($obj->formdata["lang"] == LANG_JPN) ? "■" : "*";
   
       
       // [reg3-form80] 見積No9 項目「Date of birth」の値について、項目64,69は非表示(Usr新規、Usr編集メール)
       $obj->arrItemData[1][64]['item_mail'] = '1';
       $obj->arrItemData[1][69]['item_mail'] = '1';
       
        //---------------------------------------------------
        // [reg3-form80] 見積No.13:非表示項目のエラーチェックを行わない
        //---------------------------------------------------
        // 非表示対象項目
        $gdKey = $this->itemhideGd();
        if($obj->ishideGdDate){
            foreach($gdKey as $item_id) {
                $obj->arrItemData[1][$item_id]['item_mail'] = '1';
            }
        }
       
        //---------------------------------------------------
        // [reg3-form80] 見積No.16:エリアの非表示設定
        //---------------------------------------------------
        $gdKey2 = $this->itemhideCe();
        if($obj->ishideCeDate){
            foreach($gdKey2 as $item_id) {
                $obj->arrItemData[1][$item_id]['item_mail'] = '1';
            }
        }
       
       
       // 編集時は更新の前後を表示する
       if($exec_type == 2 && $obj->editmail_diff){
           // 編集の前後の差分
           $obj->arrDiff = Usr_entryDB::getParamEditDiff($obj);
   
           // グループ1
           $obj->makeDiffBodyGroup1($arrbody, $exec_type);
   
           // グループ2
           $obj->makeDiffBodyGroup2($arrbody, $exec_type);
   
           // グループ3
           $obj->makeDiffBodyGroup3($arrbody, $exec_type);
   
   
           return $arrbody;
       }
   
   
       // グループ1
       $obj->makeBodyGroup1($arrbody, $exec_type);
   
       // グループ2
       $obj->makeBodyGroup2($arrbody, $exec_type);
   
       // グループ3
       $obj->makeBodyGroup3($arrbody, $exec_type);
   
   
       return $arrbody;
   }
   
   
   // [reg3-form80] 見積No9 項目「Date of birth」の値について[{年}Year {月}Month {日}Date]のレイアウトで表示(Usr新規メール、Usr編集メール)
    function mailfunc63($obj, $item_id, $name, $i=null) {
        $group = 1;
        $item_id = 63;
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key])."Year ";
        
        if(isset($obj->arrForm["edata64"])) {
            $value .= Usr_Assign::nini($obj, $group, 64, $obj->arrForm["edata64"])."Month ";
        }
        
        if(isset($obj->arrForm["edata69"])) {
            $value .= Usr_Assign::nini($obj, $group, 69, $obj->arrForm["edata69"])."Date";
        }

        $str  = "";
        $str .= Config::assign_obi(array("mode"=>"mail", "item_id"=>$item_id), $obj->_smarty);
        $str .= $obj->point_mark.$name.": ".$value."\n";

        return $str;
    }
    
    
/*
    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------
    // [reg3-form80] 見積No9 項目「Date of birth」の値について[{年}Year {月}Month {日}Date]のレイアウトで表示(CSV)
    function csvfunc63($obj, $group, $pa_param, $item_id) {
        
        $wk_body1 = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
                " ",
                ","
        ), true);
        
        if($wk_body1 != "") {
            $wk_body1 = trim($wk_body1, ",")."Year ";
        }
        
        
        $item_id = 64;
        $wk_body2 = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
                " ",
                ","
        ), true);
        
        if($wk_body2 != "") {
            $wk_body2 = trim($wk_body2, ",")."Month ";
        }
        
        
        $item_id = 69;
        $wk_body3 = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
                " ",
                ","
        ), true);
        
        if($wk_body3 != "") {
            $wk_body3 = trim($wk_body3, ",")."Date ";
        }
        
        
        $wk_body = $wk_body1;
        $wk_body .= $wk_body2;
        $wk_body .= $wk_body3;
        
        return $wk_body;
    }
*/


    // ------------------------------------------------------
    // ▽ Usrログイン
    // ------------------------------------------------------
    function usr_login_construct($obj)
    {
        $mode = isset($obj->arrForm["mode"]) ? $obj->arrForm["mode"] : "";
        if($mode != "login") return;
        
        // 自動でフォーム頭文字を付与
        $_REQUEST['login_id'] = $GLOBALS["form"]->formData['head'] ."-". $_REQUEST['login_id'];
    }


}
