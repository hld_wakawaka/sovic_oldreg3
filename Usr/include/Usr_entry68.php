<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."application/model/Usr_entry_quota.model.php");

class Usr_Entry68 extends AbstraceQuota {

    // ランチョンセミナー及びモーニングセミナーのグループ
    public $arrLS = array('ather_price4', 'ather_price5', 'ather_price6', 'ather_price7');
    public $arrMS = array('ather_price8', 'ather_price9', 'ather_price10');


    function __construct(){
        //----------------------
        // 応募制限設定をロード
        //----------------------
        $form_id = 68;
        parent::onStart($form_id);

        //--------------------------
        // パッケージ機能
        //--------------------------
        $this->includeDir = TEMPLATES_DIR."../customDir/{$form_id}/package/templates/Usr/";
        if (method_exists($obj, "assign")) {
            $obj->assign("includeDir", $this->includeDir);
        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
//          // メールデバッグ
//          $obj->ins_eid = 1615;
//          print "--------------------<pre style='text-align:left;'>";
//          print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//          print "</pre><br/><br/>";
    }


    // 外部テンプレートを読み込み直す
    function setTemplate($obj){
        // パッケージ機能の一環
        Usr_initial::setTemplate($obj, $this->includeDir);
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj) {
        // 定員数チェック
        $this->checkQuota($obj, $this);
        parent::onDestroy($obj);
    }


    /** 完了ページ */
    function completeAction($obj) {
        // 定員数チェック
        if($this->checkQuota($obj, $this)) return;
        return Usr_pageAction::completeAction($obj);
    }


    /**
     * 応募制限エラーに引っかかった場合のエラーメッセージのバインド
     * 
     * @param  string  $_key 対象のキー(fee,ather_price,edata)
     * @param  integer $buy  応募枚数
     * @param  integer $save 残り応募可能枚数
     * @param  integer $over 超過枚数
     * @return string  エラーメッセージ
     * */
    function bindQuotaError($_key, $buy, $save, $over){
        return "定員に達したため締め切りました。";
    }


    // No.6 # 0円の表記を変更する
    function setMoneyPaydata($obj){
        Usr_initial::setMoneyPaydata($obj);

        foreach($obj->wa_ather_price as $_key => $_other){
            if($_other['p1'] == 0){
                $obj->wa_ather_price[$_key]['p1'] = "無料／弁当つき";
                if(in_array($_key, array(8,9,10))){
                    $obj->wa_ather_price[$_key]['p1'] = "無料／軽食つき";
                }
            } 
        }

        if(method_exists($obj, "assign")){
            $obj->assign("va_ather_price", $obj->wa_ather_price);
        }
    }


    // エラーチェック # ブロック1
    function _check1($obj) {
        Usr_Check::_check1($obj);

        $group_id = 1;

        // Fee
        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : - 1;
        // その他決済
        $other  = strlen($obj->arrParam['ather_price0']) > 0 ? intval($obj->arrParam['ather_price0']) : - 1;

        // No.8 # 金額「A会員」かつその他決済「抄録集」を選択するとエラー
        if($amount == 0 && $other > 0){
            $obj->objErr->addErr('A（正）会員の方につきましては、抄録集は年会費に含まれておりますのでお申込の必要はありません。', 'amount');
        }

        // No. 10 # 複数あるランチョンセミナーは複数選択不可
        if(count(array_filter(array_intersect_key($obj->arrParam, array_flip($this->arrLS)))) > 1){
            $obj->objErr->addErr('数に限りがございますのでお1人様1ランチョンセミナーのみしかご選択いただけません。', 'ather_price4');
        }

        // No. 11 # 複数あるモーニングセミナーは複数選択不可
        if(count(array_filter(array_intersect_key($obj->arrParam, array_flip($this->arrMS)))) > 1){
            $obj->objErr->addErr('数に限りがございますのでお1人様1モーニングセミナーのみしかご選択いただけません。', 'ather_price8');
        }

        // No.14 # 弁当とセミナーで同じ日付の選択を制御
        // 8/29開催マッピング
        $arrMap29 = array('ather_price2', 'ather_price4', 'ather_price5');
        if(count(array_filter(array_intersect_key($obj->arrParam, array_flip($arrMap29)))) > 1){
            $obj->objErr->addErr('お弁当を申し込まれた日とランチョンセミナー（無料/弁当つき）の日が重複しております。どちらか1つのみご選択ください。', 'ather_price2_');
        }
        // 8/30開催マッピング
        $arrMap30 = array('ather_price3','ather_price6','ather_price7');
        if(count(array_filter(array_intersect_key($obj->arrParam, array_flip($arrMap30)))) > 1){
            $obj->objErr->addErr('お弁当を申し込まれた日とランチョンセミナー（無料/弁当つき）の日が重複しております。どちらか1つのみご選択ください。', 'ather_price3_');
        }
    }

}
