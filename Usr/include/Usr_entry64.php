<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry64 {

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj) {
         // jQueryを読み込む
         $obj->useJquery = true;
    }

//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }



    // エラーチェック # ブロック1
    function _check1($obj) {
        $group_id = 1;

        // No.2 # 参加情報が「許可する」場合は掲載情報を必須設定
        // [reg3-form64] 参加者情報で「許可する」を選択した場合、「掲載情報」のチェックを必須にする
        $item_id = 63;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1) {
            $_item_id = 64;
            $key2     = "edata".$_item_id;
            $check_ids = $obj->arrItemData[1][64]['select'];
            $appear_flg = "";
            foreach ($check_ids as $check_id => $check_name){
                if (Usr_init::isset_ex($obj, $group_id, $_item_id) && $obj->arrParam[$key2.$check_id] == $check_id){
                    $appear_flg = true;
                    break;
                } else {
                    $appear_flg = false;
                }
            }
            
            if ($appear_flg === false) {
                $name = Usr_init::getItemInfo($obj, $_item_id);
                $method = Usr_init::getItemErrMsg($obj, $_item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key2);
            }
        }

        
        // [reg3-form64]:（グループ1 > 追加エラー設定）掲載情報の住所を選択した場合「掲載用住所：*」を必須入力
        $item_id = 64;
        $key     = "edata".$item_id;
        $target_id = array(69,70,71);
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key."1"] == 1){
            foreach($target_id as $_item_id) {
                $key = "edata" . $_item_id;
                if (Usr_init::isset_ex($obj, $group_id, $_item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                    $name = Usr_init::getItemInfo($obj, $_item_id);
                    $method = Usr_init::getItemErrMsg($obj, $_item_id);
                    $obj->objErr->addErr(sprintf($method, $name), $key);
                }
            }
        }
        
        
        // [reg3-form64]:（グループ1 > 追加エラー設定）掲載情報のTELを選択した場合「掲載用電話番号」を必須入力
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key."2"] == 2){
            $_item_id = 72;
            $key = "edata" . $_item_id;
            if (Usr_init::isset_ex($obj, $group_id, $_item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                $name = Usr_init::getItemInfo($obj, $_item_id);
                $method = Usr_init::getItemErrMsg($obj, $_item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }
        
        
        // [reg3-form64]:（グループ1 > 追加エラー設定）掲載情報のE-mailを選択した場合「掲載用E-mailアドレス」を必須入力
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key."3"] == 3){
            $_item_id = 73;
            $key = "edata" . $_item_id;
            if (Usr_init::isset_ex($obj, $group_id, $_item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                $name = Usr_init::getItemInfo($obj, $_item_id);
                $method = Usr_init::getItemErrMsg($obj, $_item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }


        Usr_Check::_check1($obj);
    }



    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    function sortFormIni($obj) {
        $arrGroup1 = & $obj->arrItemData [1];
        
        // 入れ替え
        $array = array ();
        foreach ( $arrGroup1 as $key => $data ) {
            switch ($key) {
                case 1 :
                    $array [26] = $arrGroup1 [26];
                    $array [$key] = $data;
                    break;

                case 26 : // etc1
                    break;

                default :
                    $array [$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }

}
