<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."customDir/63/package/Mng_entry63.php");

class Usr_Entry63 extends Mng_Entry63 {

//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj) {
        //--------------------------
        // パッケージ機能
        //--------------------------
        if (method_exists($obj, "assign")) {
            $form_id = 63;
            $includeDir = $obj->_smarty->template_dir."../customDir/{$form_id}/package/templates/Usr/";
            $obj->assign("includeDir", $includeDir);
        }
    }
    
    
    
    /**
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    function sortFormIni($obj) {
        $arrGroup1 = & $obj->arrItemData[1];
        
        // 入れ替え
        $array = array ();
        foreach ( $arrGroup1 as $key => $data ) {
            switch ($key) {
                // No.1 # 申し込み種別を1番上に項目移動
                case 1 :
                    $array [26] = $arrGroup1 [26];
                    $array [$key] = $data;
                    break;
                
                case 26 : // etc1
                    break;
                
                default :
                    $array [$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }
    
    
    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------
    
    /**
     * No.2 # 卒業年月に関するレイアウトカスタマイズ
     */
    function mailfunc28($obj, $item_id, $name, $i = null) {
        $group = 1;
        
        $item_id = 28;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value28 = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
        if(strlen($value28) > 0) $value28 = "西暦 ".$value28.$obj->arrItemData[$group][$item_id]['item_memo']." ";
        
        $item_id = 63;
        $key = "edata" . $item_id . $i;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value63 = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
        if(strlen($value63) > 0) $value63 .= $obj->arrItemData[$group][$item_id]['item_memo']." ";
        
        $str = $obj->point_mark . $name . ": " . $value28.$value63. "\n";
        return $str;
    }

}
