<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry34 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

//        // jQueryを読み込む
//        $obj->useJquery = true;
//
//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    /**
     * メールアドレス、氏名重複チェック
     *
     */
    function checkRepeat($obj, $pa_formparam){
        if($pa_formparam["edata25"] == "" || $pa_formparam["edata1"] == ""){
            return true;
        }

        $column = "*";
        $from = "entory_r";
        $where[] = "form_id = ".$obj->db->quote($obj->o_form->formData["form_id"]);
        $where[] = "del_flg = 0";
        // 無効なエントリーは考慮しない
        $where[] = "invalid_flg = 0";

        //メールアドレス
        $where[] = "edata25 = ".$obj->db->quote($pa_formparam["edata25"]);
        $where[] = "edata1 = ".$obj->db->quote($pa_formparam["edata1"]);

        //更新の場合
        if($obj->eid != ""){
            $where[] = "eid <> ".$obj->db->quote($obj->eid);
        }

        $rs = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!$rs){
            return true;
        }
        return false;
    }


//    // エラーチェック # ブロック1
//    function _check1($obj){
//        Usr_Check::_check1($obj);
//
//        $group_id = 1;
//
//        // Fee
//        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;
//
//
//        // xxで1番目の項目を選択したらテキスト必須
//        $item_id = 26;
//        $key = "edata".$item_id;
//        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
//            $item_id = 27;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//
//
//        //-------------------------------
//        // AccompanyingPersonの数
//        //-------------------------------
//        $cnt = 0;
//
//        // AccompanyingPerson 1
//        $keys = array('edata64', 'edata69', 'edata70', 'edata71');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 1'), 'edata64');
//            }else{
//                $cnt++;
//            }
//        }
//
//        // AccompanyingPerson 2
//        $keys = array('edata72', 'edata73', 'edata74', 'edata75');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 2'), 'edata72');
//            }else{
//                $cnt++;
//            }
//        }
//    }
//
//
//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    
    // [form34]項目移動。任意項目「当日17:15～の懇親会にご参加されますか ＊」を項目「いずれの団体からのご案内でしょうか ＊」の次に項目移動
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 1:
                    $array[10]  = $arrGroup1[10];
                    $array[11]  = $arrGroup1[11];
                    $array[17]  = $arrGroup1[17];
                    $array[19]  = $arrGroup1[19];
                    $array[21]  = $arrGroup1[21];
                    $array[24]  = $arrGroup1[24];
                    $array[73]  = $arrGroup1[73];
                    $array[74]  = $arrGroup1[74];
                    $array[1]   = $arrGroup1[1];
                    $array[2]   = $arrGroup1[2];
                    $array[3]   = $arrGroup1[3];
                    $array[26]  = $arrGroup1[26];
                    $array[27]  = $arrGroup1[27];
                    $array[25]  = $arrGroup1[25];
                    $array[28]  = $arrGroup1[28];
                    $array[63]  = $arrGroup1[63];
                    $array[64]  = $arrGroup1[64];
                    $array[69]  = $arrGroup1[69];
                    $array[70]  = $arrGroup1[70];
                    $array[71]  = $arrGroup1[71];
                    $array[72]  = $arrGroup1[72];
                    break;

                case 10:
                case 11:
                case 17:
                case 19:
                case 21:
                case 24:
                case 73:
                case 1 :
                case 2 :
                case 3 :
                case 26:
                case 27:
                case 25:
                case 28:
                case 63:
                case 64:
                case 69:
                case 70:
                case 71:
                case 72:
                    break;
                    
                case 74:
                	break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



//    // ------------------------------------------------------
//    // ▽メールカスタマイズ
//    // ------------------------------------------------------
//
//    /**
//     * 任意
//     */

    // [form34]帯名「[お申込者様]」の表示
    function mailfunc1($obj, $item_id, $name, $i=null) {
   //     $group = [1];
    	$group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

//        if ( $obj->point_mark.$name == "■ご氏名") {
        	$str = "\n\n【お申込者様】\n\n".$obj->point_mark.$name.": ".$value."\n";
//        }else{
//        	$str = $obj->point_mark.$name.": ".$value."\n";
//        }
        
//        $str = $obj->point_mark.$name.": ".$value."\n";
        return $str;
    }



//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
