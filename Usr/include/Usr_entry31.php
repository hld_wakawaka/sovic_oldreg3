<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry31 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

//        // jQueryを読み込む
//        $obj->useJquery = true;
//
//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        //-----------------------------------------------
        // 同伴者に関するエラーチェック
        //-----------------------------------------------
        // Accompanying Persion
        $count = 0;

        // 74 # Family Name
        $item_id = 74;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // 79 # Family Name
        $item_id = 79;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // Fee:AccompanyingPersion
        $i   = 0;
        $key = 'ather_price0';
        $accompanying = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;

        // Enter Accompanying Person
        // 同伴者1または2のどちらかで「Family Name」の入力があった場合はFee:AccompanyingPersionを必須
        $error_flg = false;
        if($count > 0 && $accompanying == 0){
            $name   = $obj->wa_ather_price[$i]['name'];
            $method = $GLOBALS["msg"]["err_require_select"];
            $obj->objErr->addErr(sprintf($method, $name), $key);
            $error_flg = true;
        }
        // Fee:AccompanyingPersionの購入があった場合、同伴者入力がないとエラー
        if($count == 0 && $accompanying > 0){
            $name   = 'Accompanying person(s)';
            $method = $GLOBALS["msg"]["err_require_input"];
            $obj->objErr->addErr(sprintf($method, $name), $key);
            $error_flg = true;
        }
        // 「同伴者の入力数」=金額の「Accompanying persion(s)」でないとエラー
        if($count != $accompanying && $accompanying > 0 && !$error_flg){
            $obj->objErr->addErr("Number of accompanying person's name you enter and the number of accompanying person fee you select should be the same.", $key);
        }
    }


//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }
//
//
//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     **/
//    function sortFormIni($obj){
//        $arrGroup1 =& $obj->arrItemData[1];
//
//        // 入れ替え
//        $array = array();
//        foreach($arrGroup1 as $key => $data){
//            switch($key){
//                case 7:     // middle name
//                    $array[$key]= $data;
//                    $array[26]  = $arrGroup1[26];
//                    $array[27]  = $arrGroup1[27];
//                    $array[28]  = $arrGroup1[28];
//                    $array[63]  = $arrGroup1[63];
//                    $array[64]  = $arrGroup1[64];
//                    break;
//
//                case 26:    // etc1
//                case 27:    // etc2
//                case 28:    // etc3
//                case 63:    // etc4
//                case 64:    // etc5
//                    break;
//
//                default:
//                    $array[$key] = $data;
//                    break;
//            }
//        }
//        $arrGroup1 = $array;
//    }
//
//
//
    //------------------------------------------------
    // ▽ メールカスタマイズ
    //------------------------------------------------

    /** AccompanyingPerson1 # mailfunc[AccompanyingPerson1の最初のitem_id] */
    function mailfunc73($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }


    /** AccompanyingPerson2 # mailfunc[AccompanyingPerson2の最初のitem_id] */
    function mailfunc78($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }



//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



    //------------------------------------------------
    // ▽ CSVカスタマイズ
    //------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $prefix = "";
            $item_id = $_data['item_id'];
            // AccompanyingPerson 1
            if(in_array($item_id, array(73,74,75,76,77))){     // AccompanyingPerson1の関連項目
                $prefix = "AccompanyingPerson 1 ";
            }
            // AccompanyingPerson 2
            if(in_array($item_id, array(78,79,80,81,82))){     // AccompanyingPerson2の関連項目
                $prefix = "AccompanyingPerson 2 ";
            }

            $groupHeader[$group][] = $obj->fix.$prefix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
