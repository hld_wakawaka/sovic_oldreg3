<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry24 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        $obj->db = new DbGeneral();

        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        // jQueryを読み込む
        $obj->useJquery = true;

        // 支払方法 # 銀行振込で固定
        unset($GLOBALS["method_E"][2]);
        // 支払方法 # クレジットカードの選択済みで固定
        $_REQUEST['method'] = 1;

        // 定員に達すると選択できないようにする
        $this->arrDisabled24 = array();  // 金額キーと残り応募可能な人数 #マイナスだとエラー

        // 定員数を取得
        $arrQuota = $this->getQuota($obj);
        if(count($arrQuota) > 0){
            $total = $this->getTotal($obj);

            foreach($total as $_key => $_val){
                if(strlen($_val) == 0) $_val = 0;
                if(!isset($arrQuota[$_key])) continue;

                // 定員 - 現在の応募者数 = 残り応募可能な数
                $this->arrDisabled24[$_key] = $arrQuota[$_key] - $_val;
            }
        }
        if(method_exists($obj, "assign")){
            $obj->assign("arrDisabled24", $this->arrDisabled24);
        }

//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* その他決済の各定員数を取得 */
    function getQuota($obj){
        $arrQuota = array();

        $quota1 = 'ather_price*_*';
        $quota2 = 'ather_price([0-9]+)_([0-9]+)';

        $form_id   = $obj->o_form->formData['form_id'];
        $customDir = ROOT_DIR."customDir/".$form_id."/fee/";
        $ex = glob($customDir.$quota1);
        if(is_array($ex)){
        foreach($ex as $_key => $script){
                if(preg_match("/".$quota2."$/u", $script, $match) !== 1) continue;

                $arrQuota['ather_price'.$match[1]] = $match[2];
            }
        }
        return $arrQuota;
    }


    /* その他決済の集計データ */
    function getTotal($obj){
        //集計データ
        $total = array();

        $column  = " sum(cnt1) as ather_price1";

        $from = "(";
        $from .= " select";
        $from .= "  SUM(CASE WHEN payment_detail.key = 1 THEN quantity END) AS cnt1";
        $from .= " ";
        $from .= " ";
        $from .= " from payment_detail";
        $from .= " LEFT OUTER JOIN entory_r";
        $from .= " on payment_detail.eid = entory_r.eid";
        $from .= " ";
        $from .= " where";
        $from .= "      payment_detail.type = 1";
        $from .= "  and payment_detail.del_flg = 0";
        $from .= "  and entory_r.form_id = 24";
        $from .= "  and entory_r.del_flg = 0";
        $from .= "  and entory_r.invalid_flg = 0";
        $from .= " ";
        $from .= " group by payment_detail.key";
        $from .= ") AS cnt";

        $rs = $obj->db->getListData($column, $from);
        if(isset($rs[0])){
            $total = $rs[0];
        }

        return $total;
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        $obj->arrForm['method'] = 1;

        $this->checkQuota($obj);
    }


    /** 完了ページ */
    function completeAction($obj) {
        if($this->checkQuota($obj)) return;

        return Usr_pageAction::completeAction($obj);
    }


    function checkQuota($obj){
        // 定員数を常にチェックする
        if(count($this->arrDisabled24) == 0) return;

        // 1ページ目の入力情報
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");

        $error = false;
        foreach($this->arrDisabled24 as $_key => $quota){
            $buy = $arrForm1[$_key];
            if($buy == 0) continue;

            $over= $quota-$buy; // 応募可能件数 - 購入枚数 = 0以上であればok
            if($over >= 0) continue;

            // overがマイナスで定員オーバー
            // 購入枚数を考慮した残り応募可能枚数
            $over= abs($over);
            $save= $buy - $over;

            $msg = ""; // エラーメッセージ
            switch($save){
                case 0:
                    $msg = "Congress Dinner ticket which you selected has already been closed because it reached its maximum capacity. ";
                    break;

                case 1:
                    $msg = "Only one ticket is available for purchase.";
                    break;

                default:
                    $msg = "Only two tickets are available for purchase.";
                    break;
            }
            $obj->arrErr[$_key] = $msg;
//            print "購入枚数：".$buy." / 残り：".$save." / オーバー：".$over;
            $error = true;
        }
//        print "----<pre>";
//        print_r($this->arrDisabled24);
//        print "</pre><br/><br/>";

        if($error){
            $obj->block = "1";
            $obj->_processTemplate =  "Usr/form/Usr_entry.html";

            $obj->assign("arrErr",  $obj->arrErr);
            $obj->assign("block",   $obj->block);
        }
        return $error;
    }


    /* デバッグ用 */
    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        // Member項目の入力数
        $count = 0;

        // 26 # JSNFS Membership No.
        $item_id = 26;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // 27 # Name of Supporting society of which you are a member
        $item_id = 27;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        //-----------------------------------------------
        // 17ヶ国に関するエラーチェック
        //-----------------------------------------------
        // Fee
        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;

        // 指定の17ヶ国
        // 18  Bangladesh
        // 44  China
        // 96  India
        // 97  Indonesia
        // 98  Iran
        // 111 Korea
        // 116 Lebanon
        // 126 Malaysia
        // 159 Pakistan
        // 166 Philippines
        // 193 Singapore
        // 200 Sri Lanka
        // 208 Taiwan
        // 210 Thailand
        // 233 Viet Nam
        // 93  Hong Kong
        // 138 Mongolia
        $chkCountry = array(18,44,96,97,98,111,116,126,159,166,193,200,208,210,233,93,138);

        $item_id = 114;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
            // 17ヶ国
            if(in_array($obj->arrParam[$key], $chkCountry)){
                // 17ヶ国かつ★5を選択
                if(in_array($amount, array(1,3))){
                    $obj->objErr->addErr('Select "Member Discount" or "1-Day Registration" for Fee.', 'amount');
                }
            // 17ヶ国でない
            }else{
                // 17ヶ国でないかつ★4
                if(in_array($amount, array(0,2))){
                    // 17ヶ国でない場合は★2、★3のいずれかまたは両方の入力が必須
                    if($count == 0){
                        $obj->objErr->addErr('Enter "JSNFS Membership No.", or select "Name of Supporting society of which you are a member".', 'edata26-27');
                    }
                }
            }
        }

        // Member項目を入力かつ(★5)Non-member選択
        if($count > 0 && in_array($amount, array(1,3))){
            $obj->objErr->addErr('Select "Member Discount" or "1-Day Registration" for Fee.', 'edata26-edata27-2');
        }


        //-----------------------------------------------
        // 同伴者に関するエラーチェック
        //-----------------------------------------------
        // Accompanying Persion
        $count = 0;

        // 74 # Family Name
        $item_id = 74;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // 79 # Family Name
        $item_id = 79;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // other price 1
        $key = 'ather_price0';
        $other1 = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;

        // Enter Accompanying Person
        // 同伴者1または2のどちらかで「Family Name」の入力があった場合は★7を必須
        $error_flg = false;
        if($count > 0 && $other1 == 0){
            $obj->objErr->addErr("Select for Accompanying person fee.", $key);
            $error_flg = true;
        }
        // ★7の購入があった場合、同伴者入力がないとエラー
        if($count == 0 && $other1 > 0){
            $obj->objErr->addErr("Enter Accompanying person's name.", $key);
            $error_flg = true;
        }
        // 「同伴者の入力数」=金額の「Accompanying persion(s)」でないとエラー
        if($count != $other1 && $other1 > 0 && !$error_flg){
            $obj->objErr->addErr("Number of accompanying person's name you enter and the number of accompanying person fee you select should be the same.", $key);
        }


        //-----------------------------------------------
        // その他エラーチェック
        //-----------------------------------------------
        // 26 # JSNFS Membership No.の10桁制限
        $item_id = 26;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
            if(strlen($obj->arrParam[$key]) != 10 || substr($obj->arrParam[$key], 0, 3) != "009"){
                $name   = Usr_init::getItemInfo($obj, $item_id);
                $obj->objErr->addErr('JSNFS Membership No. that you entered is not correct.', $key);
            }
        }

        // 27 # Name of Supporting society of which you are a member
        $item_id = 27;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
            $item_id = 28; // 必須 # Supporting Society's Membership No.
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name   = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }
    }


    // エラーチェック # ブロック3
    function _check3($obj){
        Usr_Check::_check3($obj);

        // 54(radio)で'yes'を選択したら、54(テキスト)を必須
        $group_id = 3;
        $item_id  = 54;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
            $item_id = 55;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name   = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }


        // Fee
        $arrForm = $GLOBALS["session"]->getVar("form_param1");
        $amount = strlen($arrForm['amount']) > 0 ? intval($arrForm['amount']) : -1;
        // Questionnaire設問1は1-Dayの場合に「yes」を選択するとエラー
        if(in_array($amount, array(4,5,6,7,8,9,10,11))){
            $item_id = 54;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $item_id) && in_array($obj->arrParam[$key], array(1,2))){
                $obj->objErr->addErr("Presenters must register for regular delegate category.", $key);
                unset($obj->objErr->_err['edata55']);
            }
        }
    }


//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     **/
//    function sortFormIni($obj){
//        $arrGroup1 =& $obj->arrItemData[1];
//
//        // 入れ替え
//        $array = array();
//        foreach($arrGroup1 as $key => $data){
//            switch($key){
//                case 7:     // middle name
//                    $array[$key]= $data;
//                    $array[26]  = $arrGroup1[26];
//                    $array[27]  = $arrGroup1[27];
//                    $array[28]  = $arrGroup1[28];
//                    $array[63]  = $arrGroup1[63];
//                    $array[64]  = $arrGroup1[64];
//                    break;
//
//                case 26:    // etc1
//                case 27:    // etc2
//                case 28:    // etc3
//                case 63:    // etc4
//                case 64:    // etc5
//                    break;
//
//                default:
//                    $array[$key] = $data;
//                    break;
//            }
//        }
//        $arrGroup1 = $array;
//    }



    //------------------------------------------------
    // ▽ メールカスタマイズ
    //------------------------------------------------

    function mailfunc26($obj, $item_id, $name) {
        $str  = "\n";
        $str .= "[Membership Information]\n";
        $str .= "\n";
        $str .= $obj->mailfuncNini(1, $item_id, $name);

        return $str;
    }

    /** AccompanyingPerson1 # mailfunc[AccompanyingPerson1の最初のitem_id] */
    function mailfunc73($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }


    /** AccompanyingPerson2 # mailfunc[AccompanyingPerson2の最初のitem_id] */
    function mailfunc78($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }


    /** Questionnaire 1 */
    function mailfunc55($obj, $item_id, $name) { return ''; }
    function mailfunc54($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str .= $obj->point_mark.$name.": ";

        $group   = 3;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        if($obj->arrForm[$key] == 1){
            $item_id = 55;
            $key = "edata".$item_id;
            $str .= ":";
            $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        }

        $str.= "\n";
        return $str;
    }


    function makePaymentBody($obj, $exec_type){
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //支払合計
        if($exec_type == "1"){
            $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        }else{
            $total = $GLOBALS["session"]->getVar("ss_total_payment");
        }


        if($obj->formdata["lang"] == LANG_JPN){
            $obj->point_mark = "■";
            $body_pay = "\n\n【お支払情報】\n\n";
        }else{
            $obj->point_mark = "*";
            $body_pay = "\n\n[Payment Information]\n\n";
        }

        //$body_pay .= "\n\n";
        if($obj->formdata["lang"] == LANG_JPN){
            $body_pay .= $obj->point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
            $body_pay .= $obj->point_mark."金額: \n";
        }
        else{
            if($total > 0){
                $body_pay .= ($obj->arrForm["method"] != "3") 
                            ? $obj->point_mark."Payment:".$obj->wa_method[$obj->arrForm["method"]]."\n"
                            : $obj->point_mark."Payment:Cash on-site\n";
            }
            $body_pay .= $obj->point_mark."Amount of Payment:\n";
        }


        // Fee
        $body_pay .= $obj->makePaymentBody1($exec_type);

        //その他決済がある場合
        $body_pay .= $obj->makePaymentBody2($exec_type);


        if($obj->formdata["lang"] == LANG_JPN){
            $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
        }
        else{
            $body_pay .="      Amount of Total Payment:".number_format($total).$obj->yen_mark."\n\n";
        }


        // お支払情報のメール生成
        $body_pay .= $obj->makePaymentMethodBody($total);

        return $body_pay;
    }


    /** 金額のメール本文 */
    function makePaymentBody1($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //新規の場合
        if($exec_type == "1"){
            foreach($obj->wa_price as $p_key => $data){
                if(!isset($obj->arrForm["amount"])) continue;
                if($p_key == $obj->arrForm["amount"]){
                    $price = $data["p1"];
                    if(is_numeric($price)){
                        $price = number_format($data["p1"]);
                        $price = ($obj->formdata["lang"] == LANG_JPN)
                               ? $price."円"
                               : $price.$obj->yen_mark;
                    }

                    // タグを除去
                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                    $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["pricetype"] == "2"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .= "　　　".$data["name"]."：".$price."\n";
                        }
                        else{
                            $body_pay .= "      ".$data["name"].":".$price."\n";
                        }
                    }
                    else{
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .=  "　　　".$obj->formdata["csv_price_head0"]."：".$price."\n";
                        }
                        else{
                            $body_pay .=  "      ".$obj->formdata["csv_price_head0"].":".$price."\n";
                        }
                    }
                    break;
                }
            }
        }

        //更新の場合
        if($exec_type == "2"){
            //全てのその他決済で項目を生成しているため、出力は不要
        }
        return $body_pay;
    }


    /** その他決済のメール本文 */
    function makePaymentBody2($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //その他決済がある場合
        if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

            //新規の場合
            if($exec_type == "1"){
                foreach($obj->wa_ather_price  as $p_key => $data ){
                    if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                    if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
                        $price = $data["p1"];

                        // Free, - => 0
                        if(!is_numeric($data["p1"])){
                            $data["p1"] = 0;
                        }
                        $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                        // タグを除去
                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                        $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = number_format($price)."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = number_format($price).$obj->yen_mark;
                            }
                            $body_pay .="      ".$data["name"].":".number_format($sum).$obj->yen_mark."  (".$price." ×".$obj->arrForm["ather_price".$p_key].")\n";
                        }
                    }
                }
                
            }

            //更新の場合
            if($exec_type == "2"){
                //更新の場合は、登録済みのデータから引っ張ってくる
                $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);

                foreach($wa_payment_detail as $data){
                    $price = $data["price"];
                    // Free, - => 0
                    if(!is_numeric($data["price"])){
                        $data["price"] = 0;
                    }
                    $sum = $data["price"]*$data["quantity"];

                    // タグを除去
                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                    $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["lang"] == LANG_JPN){
                        if(is_numeric($price)){
                            $price = $price."円";
                        }
                        $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                    }
                    else{
                        if(is_numeric($price)){
                            $price = $price.$obj->yen_mark;
                        }
                        $body_pay .="      ".$data["name"].":".number_format($sum).$obj->yen_mark;
                    }

                    // その他決済の場合は内訳を後ろにつける
                    if($data["type"] == "1"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .="　（".$price." ×".$data["quantity"]."）";
                        }
                        else{
                            $body_pay .="  (".$price." ×".$data["quantity"].")";
                        }
                    }
                    $body_pay .= "\n";
                }
            }
        }
        return $body_pay;
    }


    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        
        //--------------------------------
        //置換文字列
        //--------------------------------
        //文言設定がある場合
        $obj->wa_replaceStr= array();
        $obj->wa_replaceStr["ID"] = $user_id;
        $obj->wa_replaceStr["SEI"] = $obj->arrForm["edata1"];
        $obj->wa_replaceStr["MEI"] = $obj->arrForm["edata2"];
        $obj->wa_replaceStr["MIDDLE"]= isset($obj->arrForm["edata7"]) ? $obj->arrForm["edata7"] : "";

        if(!isset($obj->arrForm["edata57"])) $obj->arrForm["edata57"] = "";
        $wk_title = ($obj->arrForm["edata57"] == "") ? "" : $GLOBALS["titleList"][$obj->arrForm["edata57"]];
        $obj->wa_replaceStr["TITLE"]= $wk_title;

        $obj->wa_replaceStr["SECTION"]= isset($obj->arrForm["edata10"]) ? $obj->arrForm["edata10"] : "";

//        if($obj->formdata["type"] != "3"){
            if($obj->formdata["edit_flg"] == "1"){
                $obj->wa_replaceStr["PASSWORD"]= $passwd;
            }
//        }


        //新規の場合
        $ret_str = "";
        if($exec_type == "1"){
            //$wk_day = $obj->getEntryDate($obj->ins_eid);
            $wk_day = Usr_function::getEntryDate($obj->db, $obj->ins_eid);
            $obj->wa_replaceStr["INSERT_DAY"]= date('m/d/Y H:i:s', strtotime($wk_day["insday"]));
            $obj->wa_replaceStr["UPDATE_DAY"]= "";

//            $ret_str = $obj->wordReplace($obj->formWord["word3"], $obj->wa_replaceStr);
            $ret_str = Usr_function::wordReplace($obj->formWord["word3"], $obj->wa_replaceStr);

        }
        //更新の場合
        if($exec_type == "2"){
            //$wk_day = $obj->getEntryDate($obj->eid);
            $wk_day = Usr_function::getEntryDate($obj->db, $obj->eid);

            $obj->wa_replaceStr["INSERT_DAY"]= $wk_day["insday"];
            $obj->wa_replaceStr["UPDATE_DAY"]= $wk_day["upday"];

//            $ret_str = $obj->wordReplace($obj->formWord["word4"], $obj->wa_replaceStr);
            $ret_str = Usr_function::wordReplace($obj->formWord["word4"], $obj->wa_replaceStr);

        }

        return $ret_str;
    }



    //------------------------------------------------
    // ▽ CSVカスタマイズ
    //------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $prefix = "";
            $item_id = $_data['item_id'];
            // AccompanyingPerson 1
            if(in_array($item_id, array(73,74,75,76,77))){     // AccompanyingPerson1の関連項目
                $prefix = "AccompanyingPerson 1 ";
            }
            // AccompanyingPerson 2
            if(in_array($item_id, array(78,79,80,81,82))){     // AccompanyingPerson2の関連項目
                $prefix = "AccompanyingPerson 2 ";
            }

            $groupHeader[$group][] = $obj->fix.$prefix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }



}
