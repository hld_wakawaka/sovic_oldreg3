<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *
 */

define("__FORM55_MSG_01_", "新規登録は受け付けていません。ごめんね！");

class Usr_Entry55 {

    // ----------------------------------------
    // カスタマイズメモ
    // ----------------------------------------

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj) {
        $obj->form_id = 55;

        /**
         * 登録種別によってチェック項目を切り替える
         *   エラーチェックとメールで利用
         *   edata27:1 # 日本語と英語
         *   edata27:2 # 英語
         */
        $obj->arrSearchType = array();
        $obj->arrSearchType[1] = array(81,82,83,115,116,117,118,119,120,121,122,123,124,142,28,63,64,69,70,71,72,73,74,75,76,77,78,79,80,141);
        $obj->arrSearchType[2] = array(81,82,83,115,116,117,118,119,120,121,122,123,124,142);

        // ログインクラス
        if(get_class($obj) == "login"){
            $_POST['login_id']    = $GLOBALS["form"]->formData["head"] ."-". $_POST['login_id'];
            $_REQUEST['login_id'] = $GLOBALS["form"]->formData["head"] ."-". $_REQUEST['login_id'];
        }

//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example'
//             ));
//         }
//
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
    }

    /* デバッグ用 */
    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 2));
//         print "</pre><br/><br/>";
    }

    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj) {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        if (method_exists($obj, "assign")) {
            // ログインIDをブース番号にする
            // フォーム頭文字の長さ # 「-」連結分+1する
            $len_formhead = strlen($obj->formdata["head"])+1;
            $GLOBALS["entryData"]['e_user_id'] = substr($GLOBALS["entryData"]['e_user_id'], $len_formhead);
            $obj->assign("va_userdata", $GLOBALS["entryData"]);

            // 小間番号 = ブース番号
            $obj->arrForm['edata26'] = $GLOBALS["entryData"]['e_user_id'];

            // エラーメッセージにインデントなどをつける
            if(count($obj->objErr->_err) > 0){
                $newErr = array();
                $obi_jp = false;
                $obi_en = false;
                $obi_ta = false;
                foreach($obj->objErr->_err as $edata => $err){
                    $item_id = str_replace("edata", "", $edata);

                    // 日本語エラー
                    if(in_array($item_id, array(28,63,64,69,70,71,72,73,74,75,76,77,78,79,80,141))){
                        if(!$obi_jp){
                            $newErr["edata{$item_id}__"] = "　";
                            $newErr["edata{$item_id}_"]  = "日本語ページ掲載情報";
                        }
                        $obi_jp = true;
                        $err = "　".$err;
                    }
                    // 英語エラー
                    if(in_array($item_id, array(81,82,83,115,116,117,118,119,120,121,122,123,124,142))){
                        if(!$obi_en){
                            $newErr["edata{$item_id}__"] = "　";
                            $newErr["edata{$item_id}_"]  = "英語ページ掲載情報";
                        }
                        $obi_en = true;
                        $err = "　".$err;
                    }
                    // 担当者
                    if(in_array($item_id, array(10,13,1,25,21,24,138,139,140))){
                        if(!$obi_ta){
                            $newErr["edata{$item_id}__"] = "　";
                            $newErr["edata{$item_id}_"]  = "ご担当者";
                        }
                        $obi_ta = true;
                        $err = "　".$err;
                    }
                    $newErr[$edata] = $err;
                }
                // 配列の最初の空白を削除する
                $firstKey = key(array_slice($newErr, 0, 1, true));
                if($newErr[$firstKey] == "　") unset($newErr[$firstKey]);
                $obj->assign("arrErr", $newErr);
            }
        }
        // ボタンの名称を日英併記にする
        $GLOBALS["msg"]["login_id"]				= "Login ID";
        $GLOBALS["msg"]["btn_logout"]			= "Logout";
        $GLOBALS["msg"]["btn_return"]			= "戻る / back";
        $GLOBALS["msg"]["btn_next"]				= "次へ / next";
        $GLOBALS["msg"]["btn_regist"]			= "登録 / register";
        $GLOBALS["msg"]["btn_update"]			= "更新 / update";
        $GLOBALS["msg"]["btn_send"]				= "送信 / register";
        // ブラウザバックの注意書き
        $GLOBALS["msg"]["browser_back"]	= "※ブラウザの戻るボタンは使用しないでください。<br>*Please do not use the \"Back\" button on your browser.";
    }



    /**
     * 簡易認証
     *
     * authディレクトリ内に[form]+form_idのファイルが存在すれば簡易認証を行う
     *
     * @access private
     *
     */
    function pf_auth($obj) {
        // No.1 # 新規登録は受け付けない
        $obj->AuthAction();
        if(strlen($obj->eid) == 0 ) header("location: ".URL_ROOT."Usr/Usr_login.php?form_id=".$obj->form_id);
        return;
    }


    /**
     * 管理者モードか一般モードか判断し、各処理を行う
     */
    function AuthAction($obj) {
        //------------------------------------
        //一般利用者モード
        //------------------------------------
        if(!isset($_GET["admin_flg"])) $_GET["admin_flg"] = "";
        $obj->admin_flg = $_GET["admin_flg"] != "" ? "1" : "";

        if($obj->admin_flg == ""){

            //-------------------------------
            //エントリー可能期間チェック
            //-------------------------------
            $obj->_chkPeriod();


            //------------------------
            //新規　or 更新判別
            //------------------------
            //ユーザのセッション情報があるか？
            if(isset($GLOBALS["entryData"]["eid"])){
                $obj->eid = $GLOBALS["entryData"]["eid"];
                $obj->assign("va_userdata", $GLOBALS["entryData"]);    //ユーザのデータ
            }
            else{
                $obj->eid = "";
            }

            //----------------------------------------------
            //ログイン認証
            //----------------------------------------------
            if(isset($_REQUEST["eid"]) && (trim($_REQUEST["eid"]) != "")){

                //ログインチェック
                if (!$GLOBALS["session"]->getVar("isLoginEntry")) {
                    $msg = "一定時間以上サーバとの通信が行われなかったためログアウトいたしました。 <br/>You have been logged out because your session has expired. ";
                    Error::showErrorPage($msg);
                }
            }
        }
        //------------------------------------
        //管理者モード
        //------------------------------------
        else{
            // ログインチェック
            $url = SSL_URL_ROOT."Usr/Usr_login.php?form_id=".$obj->form_id;
            LoginMember::checkLoginRidirect($url, false);

            $obj->eid = isset($_GET["eid"]) ? $_GET["eid"] : "";
            if($obj->eid != "" && !is_Numeric($obj->eid)) {
                Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
            }

            if($obj->admin_flg != "" && !is_Numeric($obj->admin_flg)) {
                Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
            }

            //ユーザのセッション情報があるか？
            if(isset($GLOBALS["entryData"]["eid"])){
                $obj->assign("va_userdata", $GLOBALS["entryData"]);    //ユーザのデータ
            }
        }

        $obj->assign("eid", $obj->eid);        //エントリーID
    }


    /** 初期表示 */
    function defaultAction($obj) {
        Usr_pageAction::defaultAction($obj);
            // 編集ページも認証を行う
        //---------------------------------
        //プライバシーポリシー同意画面
        //---------------------------------
        if($obj->formdata["agree"] == "3" || $obj->formdata["agree"] == "4"){
            //別画面で同意画面を表示する
            if($obj->formdata["lang"] == LANG_JPN){
                $obj->_title = $obj->formdata["form_name"]." 同意書";
            }else{
                $obj->_title = $obj->formdata["form_name"]." Privacy Policy";
            }
            // 共通テンプレート
            $obj->_processTemplate = "Usr/form/Usr_entry_agree.html";
        }

        //初期値
        if($obj->formdata["agree"] != "0"){
            $obj->arrForm["agree"] = "";
        }

        if($GLOBALS["session"]->getVar("startmsg")) {
            $obj->arrErr = $GLOBALS["session"]->getVar("startmsg");
            $GLOBALS["session"]->unsetVar("startmsg");
        }
    }


    /** プライバシーポリシー */
    function agreeAction($obj) {
        //同意が必要な場合
        if(in_array($obj->formdata["agree"], array(3, 4))){
            if($_REQUEST["agree"] == ""){
                $msg = "以下をご覧になり、同意の上、お進みください。 / Please read the agreement and proceed further only if you agree. ";

                $obj->objErr->addErr($msg, "agree");
                $obj->arrErr = $obj->objErr->_err;

                $obj->_processTemplate = "Usr/form/Usr_entry_agree.html";
                $obj->_title = ($obj->formdata["lang"] == LANG_JPN)
                ? $obj->formdata["form_name"]." 同意書"
                        : $obj->formdata["form_name"]." Privacy Policy";
            }
        }
        $obj->block = "1";
        // セッションパラメータ取得
        $obj->getDispSession();
    }



    /** フォーム項目の取得及び、整形 */
    function setFormIni($obj, $init=true) {
        // 初期化するフラグ
        // 検索ページからは呼び出されないためfalseを引数に指定する
        if($init) Usr_initial::setFormIni($obj);

        // 英語出展者 # az順
        for($i=1; $i<=45; $i++){
            unset($obj->arrItemData[1][82]['select'][$i]);
        }
        if (isset($obj->_smarty)) {
            $obj->assign("arrItemData", $obj->arrItemData);
        }
    }



    function _initNini($obj, &$key, $item_id){
        /**
         * ひらがな・カタカナは全角、英数字は半角文字にコンバートする[日本語掲載ページを対象]
         *  ・会社所在地  # edata71
         *  ・出展品目    # edata75, 76,77,78,79
         *  ・見どころ   # edata80
         * */
        if(in_array($item_id, array(71, 75, 76, 77, 78, 79))){
            $tmp = array();
            Usr_init::_initNini($obj, $tmp, $item_id);
            $tmp[0][4] = "nKV";
            array_push($key, $tmp[0]);
            return $key;
        }else{
            return Usr_init::_initNini($obj, $key, $item_id);
        }
    }


    // エラーチェック # ブロック1
    function _check1($obj) {
//        $obj->admin_flg = "";

        // 出展ページが英語の場合は「日本語」に関するエラーの必須から外す
        // 日本語 # 28,63,64,69,70,71,72,73,74,75,76,77,78,79,80,141
        $group_id = 1;
        $edata27 = $obj->arrParam['edata27'];
        if($edata27 == 2){
            // 出展者が英語の場合はエラーメッセージを英語表記
            require_once(ROOT_DIR."application/cnf/en.php");

            // 日本語に関するエラーの必須を外す
            foreach(array(28,63,64,69,70,71,72,73,74,75,76,77,78,79,80,141) as $item_id){
                $itemCheck = explode("|", $obj->itemData[$item_id]["item_check"]);
                $nullCheck = array_search("0", $itemCheck);
                if($nullCheck !==  false){
                    unset($itemCheck[$nullCheck]);
                    $obj->itemData[$item_id]["item_check"] = implode('|', $itemCheck);
                }
            }

            // 英語項目と担当者の名称を変更する
            foreach(array(81,82,83,115,116,117,118,119,120,121,122,123,124,142, 10,13,1,25,21,24,138,139,140) as $item_id){
                if(strpos($obj->itemData[$item_id]["item_name"], "<br>") !== false){
                    $tmp = explode("<br>", $obj->itemData[$item_id]["item_name"]);
                    $obj->itemData[$item_id]["strip_tags"]               = strip_tags($tmp[1]);
                    $obj->arrItemData[$group_id][$item_id]["strip_tags"] = strip_tags($tmp[1]);
                }
            }
        }

        //-----------------------------
        // パスワード変更
        //-----------------------------
        $item_id = 138;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
            // 現在のパスワードの妥当性チェック
            if(!$obj->db->exists("entory_r", "form_id = ? and eid = ? and e_user_passwd = ?", array($obj->form_id, $obj->eid, $obj->arrParam[$key]))){
                $obj->objErr->_err[$key] = "現在のパスワードに誤りがあります。";
            }else{
                // 変更用パスワードを必須
                foreach(array(139, 140) as $item_id){
                    $obj->itemData[$item_id]["item_check"] = (strlen($obj->itemData[$item_id]["item_check"]) > 0)
                    ? $obj->itemData[$item_id]["item_check"]."|0"
                            : "0";
                }
                // 変更後のパスワードの不一致
                if(strlen($obj->arrParam['edata139']) > 0){
                    if($obj->arrParam['edata139'] != $obj->arrParam['edata140']){
                        $obj->objErr->_err[$key] = "変更後のパスワードが異なっています。";
                    // 半角英数チェック
                    }else{
                        // 長さチェック(6-12文字制限)
                        if(!$obj->objErr->isLenMinMax($obj->arrParam['edata139'], 6, 12)) {
                            $name = Usr_init::getItemInfo($obj, 139);
                            $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_strlen_between"], $name, 6, 12), $key);
                        }

                        // 半角英数記号のチェックではなく半角英数チェックをする
                        if(!$obj->objErr->isAlphaNumeric($obj->arrParam['edata139'])){
                            $name = Usr_init::getItemInfo($obj, 139);
                            $obj->objErr->_err[$key] = sprintf($GLOBALS["msg"]["err_han_alphanumeric"], $name);
                        }
                    }
                }
            }
        }

        // 標準エラーチェック
        Usr_Check::_check1($obj);
    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    function sortFormIni($obj) {
        $arrGroup1 = & $obj->arrItemData [1];

        // 入れ替え
        $array = array ();
        foreach ( $arrGroup1 as $key => $data ) {
            switch ($key) {
                case 137 :
                    $array[$key] = $data;
                    $array[2]  = $arrGroup1[2];
                    $array[10] = $arrGroup1[10];
                    $array[13] = $arrGroup1[13];
                    $array[1]  = $arrGroup1[1];
                    $array[25] = $arrGroup1[25];
                    $array[21] = $arrGroup1[21];
                    $array[24] = $arrGroup1[24];
                    break;

                case 71:
                    $array[$key] = $data;
                    $array[141]  = $arrGroup1[141];
                    break;

                case 116:
                    $array[$key] = $data;
                    $array[142]  = $arrGroup1[142];
                    break;

                case 1:
                case 2:
                case 10:
                case 13:
                case 21:
                case 24:
                case 25:
                case 141:
                case 142:
                    break;

                default :
                    $array [$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }


    /** エントリー情報更新 # パラメータ */
    function makeDbParam($obj){
        $param = Usr_entryDB::makeDbParam($obj);
        // パスワード更新
        if(strlen($obj->arrForm['edata139']) > 0){
            $param['e_user_passwd'] = $obj->arrForm['edata139'];
            $param['edata138']      = '';
            $param['edata139']      = '';
            $param['edata140']      = '';
        }

        return $param;
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /*
     * 完了メールの送信
     */
    function sendCompleteMail($obj, $user_id, $passwd, $exec_type=1) {

        if(in_array($obj->form_id, $obj->sendmail_not)) return;

        // CiteJapanはUTF-8送信
        $isUTF8 = true;

        //本文
        $ws_mailBody = $obj->makeMailBody($user_id, $passwd, $exec_type);

        // 登録者へのメール
        if($obj->admin_flg == "" && $obj->arrForm["edata25"] != ""){
            $obj->o_mail->SendMail($obj->arrForm["edata25"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"], $obj->formWord["word13"], $isUTF8);
        }

        // 管理者へのメール
        $obj->o_mail->SendMail($obj->formdata["form_mail"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"],$obj->formWord["word13"], $isUTF8);
        return;
    }


    function makeMailBody($obj, $user_id="", $passwd="", $exec_type=""){
        // カスタマイズテンプレート設定の読み込み
        Config::load($obj);

        // メール上部コメントの作成
        $head_comment = $obj->makeMailBody_header($user_id, $passwd, $exec_type);

        // 入力フォームのメール本文
        $arrbody = $obj->makeFormBody($exec_type);


        //----------------------------------------
        //本文生成
        //----------------------------------------
        $body  = "";
        $body .= $head_comment."\n\n";
        $body .= $arrbody[1];
        if($obj->formdata["group2_use"] != "1") $body .= $arrbody[2];
        if($obj->formdata["group3_use"] != "1") $body .= $arrbody[3];
        if($obj->formdata["kessai_flg"] == "1") $body .= $obj->makePaymentBody($exec_type);
//        $body .= $obj->makeEditBody($user_id, $passwd);

        return str_replace(array("\r\n", "\r"), "\n", $body);
    }



    function makeBodyGroup1($obj, &$arrbody, $exec_type){
        $group = 1;
        $arrbody[$group] = "";
        return;
        // 下記登録内容はメールに出力しない

        $edata27 = $obj->arrForm['edata27'];
        $non_edata27 = ($edata27 == 1) ? 2 : 1;

        // 登録種別に対応していない掲載ページを非表示とする
        foreach($obj->arrSearchType[$non_edata27] as $item_id){
            $obj->arrItemData[$group][$item_id]['item_mail'] = 1;
        }
        // 登録種別に対応した掲載ページを表示する
        foreach($obj->arrSearchType[$edata27] as $item_id){
            $obj->arrItemData[$group][$item_id]['item_mail'] = '';
        }


        // グループ1
        $group = 1;

        $body_obi  = ""; // 帯
        $body_main = ""; // 本文

        // 言語別設定
        if($obj->formdata["lang"] == LANG_JPN){
            $body_obi = ($obj->formdata["group1"] != "") ? "【".$obj->formdata["group1"]."】\n\n" : "";
        }else{
            $body_obi = ($obj->formdata["group1"] != "") ? "[".$obj->formdata["group1"]."]\n\n" : "";
        }

        // メールに出力する項目数
        $count = 0;
        foreach($obj->arrItemData[$group] as $_key => $data){
            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;
            $count++;

            $key = $data["item_id"];

            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            if(!isset($obj->arrForm["edata".$key])){
                $obj->arrForm["edata".$key] = "";
            }

            $methodname = "mailfunc".$key;
            if(method_exists($obj, $methodname)) {
                $body_main .= $obj->$methodname($name);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $body_main .= $obj->exClass->$methodname($obj, $key, $name);

                }else{
                    if($data["controltype"] == "1") {
                        $body_main .= $obj->mailfuncNini($group, $key, $name);    //任意
                    } else {
                        $body_main .= Config::assign_obi(array("mode"=>"mail", "item_id"=>$key), $obj->_smarty);
                        $body_main .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key]."\n";
                    }
                }


            }
        }

        /**
         * 帯と本文を結合
         *    出力項目数が0の場合は帯も非表示とする
         * */
        $arrbody[$group] = "";
        if($count){
            // グループ1帯は非表示
            $arrbody[$group] = $body_main;
        }
    }


    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $body_header = Usr_mail::makeMailBody_header($obj, $user_id, $passwd, $exec_type);

        // 削除するタグ名を設定
        $tag = "";
        // 日本語・英語
        if($obj->arrForm["edata27"] == 1){
            $tag = "en";
        }else{
            // 英語のみ
            if($obj->arrForm["edata27"] == 2){
                $tag = "jp";
            }
        }

        // タグ除去
        if(strlen($tag) > 0){
            $pattern = sprintf("!<%s.*?>.*?</%s>!ims", $tag, $tag);
            $body_header = preg_replace($pattern, "", $body_header);
        }
        $body_header = strip_tags($body_header);

        return $body_header;
    }



    /** 編集完了メールの送信 */
    function sendEditMail($obj, $user_id, $passwd) {
        // ログインIDをブース番号にする
        // フォーム頭文字の長さ # 「-」連結分+1する
        $len_formhead = strlen($obj->formdata["head"])+1;
        $user_id = substr($user_id, $len_formhead);
        return Usr_mail::sendEditMail($obj, $user_id, $passwd);
    }


    /** 出展品目 */
    function mailfunc75($obj, $item_id, $name, $i = null) {
        $group = 1;

        $str  = $obj->point_mark . $name . ":\n";

        foreach(array(75,76,77,78,79) as $item_id){
            $key = "edata" . $item_id;
            if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
            $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
            $value = trim($value);
            if(strlen($value) > 0) $str .= " ".$value."\n";
        }

        return $str;
    }


    /** 出展品目 */
    function mailfunc119($obj, $item_id, $name, $i = null) {
        $group = 1;

        $str  = $obj->point_mark . $name . ":\n";

        foreach(array(119,120,121,123,124) as $item_id){
            $key = "edata" . $item_id;
            if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
            $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
            $value = trim($value);
            if(strlen($value) > 0) $str .= " ".$value."\n";
        }

        return $str;
    }


    /** 出展品目 */
    function mailfunc132($obj, $item_id, $name, $i = null) {
        $group = 1;

        $str  = $obj->point_mark . $name . ":\n";

        foreach(array(132,133,134,135,136) as $item_id){
            $key = "edata" . $item_id;
            if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
            $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
            $str .= " ".trim($value)."\n";
        }

        return $str;
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------
    function __constructMngCSV($obj){
        // CSVはUTF-8出力
        $GLOBALS["userData"]["lang"] = LANG_ENG;
    }

    // ブース番号
    function csvfunc26($obj, $group, $pa_param, $item_id) {
        return $pa_param['entry_no'];
    }


    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function __constructMng($obj){
        $mode = "mode{$obj->form_id}";

        // assignする場合
        if (method_exists($obj, "assign")) {
            $menu = Mng_function::makeMenu();
            $menu[1] = array();
            $menu[1]['menu_name'] = "CSV取り込み";
            $menu[1]['prg']       = APP_ROOT."Mng/index.php?{$mode}=csvimport";
            ksort($menu);
            $obj->assign("va_menu", $menu);
        }

        // モード別の処理
        if(isset($_GET[$mode])){
            // CSVインポート
            if(method_exists($this, "csvimport")){
                $this->csvimport($obj);
            }
        }
        // 見積もり2と5:検索条件拡張のため、検索条件に「小間番号」,「出展社名」と「出展社名(英語表記)」を追加
        $obj->searchkey[] = "booth_no";
        $obj->searchkey[] = "edata1";
        $obj->searchkey[] = "edata28";
        $obj->searchkey[] = "edata81";
	}


    /**
     * エントリー一覧で取得するカラムを拡張
     * */
    function Mng_buildColumn($obj, $column){
        // フォーム頭文字の長さ # 「-」連結分とシーケンス分で+2する
        $len_formhead = strlen($GLOBALS["userData"]["head"])+2;

        $column = "*";
        $column.= ", (select c_company from payment where v_entry.eid = payment.eid) as c_company";
        $column.= ", (select c_number  from payment where v_entry.eid = payment.eid) as c_number";
        $column.= ", substr(e_user_id, {$len_formhead},length(e_user_id)) as entry_no";

        return $column;
    }

    // 見積もり2「エントリー検索 > 検索条件」:小間番号で検索できるように、取得したフォームから得た小間番号の値にフォームの頭文字を追加。そしてSQL文を作成し検索
    function Mng_buildWhere_after($obj, $condition, $where) {
    	if ($condition['booth_no'] != "") {

    		// [form55]小間番号（ブース番号）による曖昧検索を行うため、カラム「e_user_id」からフォーム頭文字を外して得た値の中に、小間番号を含むレコードを取得するSQL文を作成
    		$cnt = mb_strlen($obj->formdata["head"]);
    		$where[] = "substr(e_user_id, $cnt) like ".$obj->db->quote("%".$condition['booth_no']."%");
    	}

    	// 見積もり5「エントリー検索 > 検索条件」:出展者名、出展者名（英語表記）で曖昧検索できるように、取得したフォームから値を得て曖昧検索のSQL文を作成
    	if ($condition['edata28'] != "") {
    		//    		$edata28 = trim($obj->db->quote($condition['edata28']),"'");
    		//    		$where[] = "edata28 like '%$edata28%'";

    		// 見積もり5「エントリー検索 > 検索条件」:出展者名曖昧検索について以下のように修正
    		$where[] ="edata28 like ".$obj->db->quote("%".$condition['edata28']."%");
    	}

    	if ($condition['edata81'] != "") {
    		//    		$edata81 = trim($obj->db->quote($condition['edata81']),"'");
    		//    		$where[] = "edata81 like '%$edata81%'";

    		// 見積もり5「エントリー検索 > 検索条件」:出展者名曖昧検索（英語表記）について以下のように修正
    		$where[] ="edata81 like ".$obj->db->quote("%".$condition['edata81']."%");
    	}


    	// [form55] 「エントリー検索 > 検索条件」:管理画面における検索条件のカスタマイズ。ご担当者名で曖昧検索できるようにする
    	if ($condition['edata1'] != "") {

    		$condition["edata1"] = mb_convert_kana($condition["edata1"], "naKV", "utf8");
    		$condition["edata1"] = strtolower($condition["edata1"]);

    		$where[] ="edata1 ilike ".$obj->db->quote("%".$condition['edata1']."%");

    	}
    	return $where;
    }

    /** エントリー詳細 */
    function mng_detail_getEntry_after($obj){
        // ログインIDをブース番号にする
        // フォーム頭文字の長さ # 「-」連結分+1する
        $len_formhead = strlen($GLOBALS["userData"]["head"])+1;
        $obj->arrData['e_user_id'] = substr($obj->arrData['e_user_id'], $len_formhead);
    }


    /** 一括メール送信 */
    function Mng_mailsend_replaceStr_after($obj, $body, $arrForm, &$wa_replaceStr){
        // ログインIDをブース番号にする
        // フォーム頭文字の長さ # 「-」連結分+1する
        $len_formhead = strlen($GLOBALS["userData"]["head"])+1;
        $wa_replaceStr["ID"] = substr($arrForm['e_user_id'], $len_formhead);
    }


    function entry_csv_getData_filter($obj, $column, $from, $where, $orderby){
        // フォーム頭文字の長さ # 「-」連結分とシーケンス分で+2する
        $len_formhead = strlen($GLOBALS["userData"]["head"])+2;

        $col = array();
        $col[] = "*";
        $col[] = "to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday";
        $col[] = "to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday";
        $col[] = "substr(e_user_id, {$len_formhead},length(e_user_id)) as entry_no";
        $col[] = "to_char(cancel_date, 'yyyy/mm/dd hh24:mi:ss') as cancelday";
        $column = implode(",", $col);

        return array($column, $from, $where, $orderby);
    }


    /** No.2 # 登録データの取り込み */
    function csvimport($obj){
        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }

        $includeDir = TEMPLATES_DIR."Usr/include/".$obj->form_id."/";

        $db = new DbGeneral;

        $obj->_title  = "CSV取り込み";
        $obj->assign("_title", $obj->title);
        $obj->_processTemplate = $includeDir."Mng/Mng_Csvimport.html";

        $obj->arrForm = array(); // 入力パラメータ
        $obj->arrErr  = array(); // エラーメッセージ

        // 種別マスタ # キーと値はSysと同じに設定してある
        $arredata27 = array(1=>"英語・日本語", 2=>"英語");

        // CSVの一時保存先
        $customDir = ROOT_DIR.'customDir/'.$obj->form_id."/";
        $workDir   = $customDir."csvimport/";
        if(!is_dir($workDir)){
            mkdir($workDir, 0777, true);
            chmod($workDir, 0777);
        }

        $action = isset($_POST['mode']) ? $_POST['mode'] : "";
        if($action == "complate"){
            while(true){
                // おまじない
                if(!isset($_FILES["upfile"])){
                    $obj->arrErr['upfile'] = "取り込むCSVをアップロードしてください。"; break;
                }

                $tmp = $_FILES['upfile'];
                if($tmp["size"] == 0){
                    $obj->arrErr['upfile'] = "取り込むCSVをアップロードしてください。"; break;
                }

                if($tmp["error"] > 0){
                    $obj->arrErr['upfile'] = "取り込むCSVをアップロードしてください。"; break;
                }

                // ファイルコピー
                $copy_file = date("YmdHis")."_".$tmp["name"];
                $obj->arrForm['upfile'] = $copy_file;
                $rs = @move_uploaded_file($tmp["tmp_name"], $workDir.$copy_file);
                if(!$rs){
                    $this->arrParam['upfile'] = "";
                    $obj->arrErr['upfile'] = "ファイルの一次保存に失敗しました。"; break;
                }

                // CSVヘッダ情報
                // 0フォームの名前,  1項目名, 2長さ配列, 3チェック配列, 4変換, 5DB登録
                $key   = array();
                $key[] = array("e_user_id"    , "ブース番号",  array(1, 7)   , array("TOKUSYU", "NULL", "LEN")           ,   "", 1);
                $key[] = array("e_user_passwd", "パスワード",  array()       , array("TOKUSYU", "NULL")                  ,  "a", 1);
                $key[] = array("edata27"      , "種別"     ,  array()       , array("TOKUSYU", "NULL")                  ,"nKV", 1);
                Usr_init::_initNini($obj, $key, 28);    // 出展社名 # 日本語
                Usr_init::_initNini($obj, $key, 81);    // 出展社名 # 英語
                //--------------------------
                // 出展者のNULLチェックを外す
                //--------------------------
                $nullkey = array_search("NULL", $key[3][3]);
                if($nullkey !== false) unset($key[3][3][$nullkey]);

                $nullkey = array_search("NULL", $key[4][3]);
                if($nullkey !== false) unset($key[4][3][$nullkey]);

                // 入力キー配列
                $arrkey = array();
                foreach($key as $_key => $_arrkey){
                    $arrkey[$_key] = $_arrkey[0];
                }

                // CSVファイルのオープン
                $fp = fopen($workDir.$obj->arrForm['upfile'], 'r');
                if(!$fp){
                    $this->arrParam['upfile'] = "";
                    $obj->arrErr['upfile'] = "アップロードファイルの読み込みに失敗しました。"; break;
                }

                $db->begin();   // トランザクション開始

                $line_count = 0;    // 読み込み行数(ヘッダ、空行もカウント)
                $exce_count = 0;    // 取り込み成功行数
                $erro_count = 0;    // 失敗行数
                while (!feof($fp)) {
                    $arrCSV = fgetcsv($fp, CSV_LINE_MAX);

                    $line_count++;  // 読み込み行数カウント

                    // ヘッダ行
                    if ($line_count == 1) {
                        //---------------------
                        // ヘッダチェック
                        //---------------------
                        // カラム数の不一致
                        if(count($arrCSV) != count($key)){
                            $obj->arrErr['csv_header'] = "CSVヘッダを確認してください。カラムが".count($arrCSV)."個検出されました。"; break 2;
                        }
                        // ヘッダ
                        foreach($key as $_key => $_arrkey){
                            $column = $_arrkey[1];
                            $error_column_flg = false;
                            if($column != $arrCSV[$_key]){
                                $error_column_flg = true;
                                $obj->arrErr['csv_header_'.$_key] = "CSVヘッダを確認してください。[カラム:".$column."を指定してください。]";;
                            }
                        }
                        if($error_column_flg) break 2;
                        continue 1;
                    }

                    // 空行はスキップ
                    if (empty($arrCSV)) {
                        continue;
                    }

                    // 入力キーに対応した連想配列に変換
                    $arrCSV = array_combine($arrkey, $arrCSV);
                    $arrCSV= GeneralFnc::convertParam($key, $arrCSV);

                    //---------------------
                    // エラーチェック
                    //---------------------
                    $objErr    = New Validate();
                    $objErr->check($key, $arrCSV);
                    $arrErr = $objErr->_err;
                    // パスワード # 長さチェック
                    $passwd = $arrCSV['e_user_passwd'];
                    if(strlen($passwd) > 0 && strlen($passwd) != PASSWD_LEN){
                        $arrErr['e_user_passwd_01'] = "パスワードは".PASSWD_LEN."文字で入力してください。[{$passwd}]";
                    }
                    // パスワード # 半角英数チェック(標準だと半角英数+記号を許可するため)
                    if(!$objErr->isAlphaNumeric($passwd)){
                        $arrErr['e_user_passwd_02'] = "パスワードは半角英数で入力してください。[{$passwd}]";
                    }
                    // 種別は完全一致チェックを行う # フォーム設定より選択肢を取得する
                    $edata27 = $arrCSV['edata27'];
                    if(!in_array($edata27, $arredata27)){
                        $arrErr['edata27'] = "種別は「英語・日本語」または「英語」のいずれかを入力してください。[{$edata27}]";
                    // 種別に対応した出展社名の必須チェック
                    }else{
                        $arrCSV["edata27"] = array_search($edata27, $arredata27);
                        // 出展社名：日本語 # edata28
                        // 出展社名：英語  # edata81
                        // 必須のキーをセット
                        $arrnull = array();
                        switch($arrCSV["edata27"]){
                            // 日本語と英語に出展
                            case 1:
                                $arrnull[3] = 'edata28';
                                $arrnull[4] = 'edata81';
                                break 1;

                            // 英語に出展
                            case 2:
                                $arrnull[4] = 'edata81';
                                break 1;
                        }
                        // 必須チェック
                        foreach($arrnull as $_key => $_edata){
                            if(!$objErr->isNull($arrCSV[$_edata])){
                                $name = $key[$_key][1];
                                $arrErr[$_edata] = sprintf($GLOBALS["msg"]["err_require_input"], $name);
                            }
                        }
                    }
                    // 既存データとの整合性チェック
                    $e_user_id = $obj->formdata["head"]."-".$arrCSV["e_user_id"];
                    if($db->exists("entory_r", "e_user_id = ? and form_id = ?", array($e_user_id, $obj->form_id))){
                        $arrErr["e_user_id_02"] = "登録Noが重複しています。[{$e_user_id}]";
                    }
                    // 入力エラー発生
                    if(count($arrErr) > 0){
                        $obj->arrErr["csv_body".$line_count] = "・{$line_count}行目エラー検出";
                        foreach($arrErr as $_key => $_err){
                            $obj->arrErr["csv_body".$line_count."_".$_key] = "　".$_err;
                        }
                        $erro_count++;
                        continue 1;
                    }

                    // 取り込み処理開始
                    $date = date("Y-m-d H:i:s");
                    $param = $arrCSV;
                    $param["rdate"]     = $date;
                    $param["udate"]     = $date;
                    $param["form_id"]   = $obj->form_id;
                    $param["eid"]       = $GLOBALS["db"]->getOne("select nextval('entory_r_eid_seq')");
                    $param["e_user_id"] = $e_user_id;
                    $param['entry_way'] = ENTRY_WAY_ADMIN;

                    $rs = $db->insert("entory_r", $param);
                    if(!$rs){
                        $erro_count++;
                    }else{
                        $exce_count++;
                    }
                }
                // トランザクションを閉じる
                if(count($arrErr) > 0){
                    $db->rollback();
                }else{
                    $db->commit();
                    $obj->onload = "window.alert('成功：{$exce_count}件、失敗：{$erro_count}件');";
                    $obj->assign("onload", $obj->onload);
                }
                break;
            }
        }

        $obj->assign("arrErr", $obj->arrErr);
        $obj->basemain();
        exit;
    }



    // ------------------------------------------------------
    // ▽出展者検索ページ
    // ------------------------------------------------------

    public $search_searchkey = array('s_entry_no', 'e_entry_no', 'user_name', 'user_name_kana', 'status1', 'status2', 'status3', 'status4','syear', 'smonth', 'sday', 'eyear', 'emonth', 'eday','payment_method', 'payment_status', "entry_no", "country",'upd_syear', 'upd_smonth', 'upd_sday', 'upd_eyear', 'upd_emonth', 'upd_eday', "country_list", "limit", "page", "entry_way");
    public $search_sesskey = "mng_formlist";

    // 検索ページ # コンストラクタ
    function Usr_Search_construct($obj){
        $obj->_title = "出展者検索 | 第7回化粧品産業技術展 CITE Japan 2015";

        // フォーム情報を取得
        Usr_initial::setFormData($obj);
        Usr_initial::setFormIni($obj);
        $this->setFormIni($obj, false);

        // 言語が指定されていない場合は「日本語」
        $obj->searchLang   = (isset($_GET['lang'])) ? $_GET['lang'] : LANG_JPN;
        if(!in_array($obj->searchLang, array(LANG_JPN, LANG_ENG))) $obj->searchLang = LANG_JPN;
        $obj->item_id_lang = ($obj->searchLang == LANG_JPN) ? 63 : 82;

        $obj->assign("form_id"   , $obj->form_id);
        $obj->assign("searchLang", $obj->searchLang);
        $obj->assign("search_cf" , $obj->arrItemData[1][$obj->item_id_lang]['select']);

        // 五十音az順のマッピング
$arrMap = array();
$arrMap[0]  = array(array("ja_a" , "ア"), array( 1, 2, 3, 4, 5));
$arrMap[1]  = array(array("ja_ka", "カ"), array( 6, 7, 8, 9,10));
$arrMap[2]  = array(array("ja_sa", "サ"), array(11,12,13,14,15));
$arrMap[3]  = array(array("ja_ta", "タ"), array(16,17,18,19,20));
$arrMap[4]  = array(array("ja_na", "ナ"), array(21,22,23,24,25));
$arrMap[5]  = array(array("ja_ha", "ハ"), array(26,27,28,29,30));
$arrMap[6]  = array(array("ja_ma", "マ"), array(31,32,33,34,35));
$arrMap[7]  = array(array("ja_ya", "ヤ"), array(36,37,38));
$arrMap[8]  = array(array("ja_ra", "ラ"), array(39,40,41,42,43));
$arrMap[9]  = array(array("ja_ra", "ワ"), array(44,45));
$arrMap[10] = array(array("en_a", "A"), array(46));
$arrMap[11] = array(array("en_b", "B"), array(47));
$arrMap[12] = array(array("en_c", "C"), array(48));
$arrMap[13] = array(array("en_d", "D"), array(49));
$arrMap[14] = array(array("en_e", "E"), array(50));
$arrMap[15] = array(array("en_f", "F"), array(51));
$arrMap[16] = array(array("en_g", "G"), array(52));
$arrMap[17] = array(array("en_h", "H"), array(53));
$arrMap[18] = array(array("en_i", "I"), array(54));
$arrMap[19] = array(array("en_j", "J"), array(55));
$arrMap[20] = array(array("en_k", "K"), array(56));
$arrMap[21] = array(array("en_l", "L"), array(57));
$arrMap[22] = array(array("en_m", "M"), array(58));
$arrMap[23] = array(array("en_n", "N"), array(59));
$arrMap[24] = array(array("en_n", "O"), array(60));
$arrMap[25] = array(array("en_n", "P"), array(61));
$arrMap[26] = array(array("en_n", "Q"), array(62));
$arrMap[27] = array(array("en_n", "R"), array(63));
$arrMap[28] = array(array("en_n", "S"), array(64));
$arrMap[29] = array(array("en_n", "T"), array(65));
$arrMap[30] = array(array("en_n", "U"), array(66));
$arrMap[31] = array(array("en_n", "V"), array(67));
$arrMap[32] = array(array("en_n", "W"), array(68));
$arrMap[33] = array(array("en_n", "X"), array(69));
$arrMap[34] = array(array("en_n", "Y"), array(70));
$arrMap[35] = array(array("en_n", "Z"), array(71));
        $obj->assign("arrMap", $arrMap);
    }


    // 検索ページ # 初期表示
    function Usr_Search_defaultAction($obj){
        include_once(ROOT_DIR.'Mng/function.php');

        //検索条件にエラーが無い場合に一覧情報を取得
        if(count($obj->arrErr) ==  0){
            $obj->arrData["list"] = $this->getExhibitEntry($obj, $obj->searchLang);
        }else{
            $obj->arrData["list"]  = array();
        }

        // 五十音az順別の出展者配列
        $arrData = array();
        foreach($obj->arrData["list"]as $_arrData){
            $sort = $_arrData['exhibit_table'];
            $arrData[$sort][] = array_filter($_arrData);
        }
        $obj->arrData['list'] = $arrData;

        $obj->_processTemplate = "Usr/include/55/search/Usr_search.html";
        $obj->assign("arrData", $obj->arrData);
    }


    function Usr_Search_searchAction($obj){
        $this->Usr_Search_defaultAction($obj);

        $word = ($obj->searchLang == LANG_JPN) ? 'フリーワード' : 'Keyword';

        // 検索情報
        // 0フォームの名前,  1項目名, 2長さ配列, 3チェック配列, 4変換, 5DB登録
        $key   = array();
        $key[] = array("free_word" , $word,  array(), array("TOKUSYU"), ""); // nKV
        $key[] = array("mode"      , 'mode', array(), array("TOKUSYU"), ""); // nKV

        // 入力キーに対応した連想配列に変換
        $obj->arrForm = GeneralFnc::convertParam($key, $obj->arrForm);

        //---------------------
        // エラーチェック
        //---------------------
        $objErr    = New Validate();
        $objErr->check($key, $arrCSV);
        $obj->arrErr = $objErr->_err;

        $obj->arrData["search"] = (strlen($obj->arrForm['free_word']) > 0)
                                ? $this->getExhibitEntry($obj, $obj->searchLang, $obj->arrForm)
                                : array();
        $obj->assign("arrData", $obj->arrData);
    }


    function getExhibitEntry($obj, $lang, $condition){
        // 追加条件
        $where = ''; $arrVal = array();
        if(strlen($condition['free_word']) > 0){
            $where = 'and exhibit_word like ?';
            $arrVal[] = "%{$condition['free_word']}%";
        }

        // 英語検索ページ
        if($lang == LANG_ENG){
            $sql = <<< __SQL__
select
    *
from
    (
    select
        form_id
      , eid
      , edata27
      , edata81 as exhibit_name
      , edata82 as exhibit_table
      , edata81 || ' ' || coalesce(edata119, '') || ' ' || coalesce(edata120, '') || ' ' || coalesce(edata121, '') || ' ' || coalesce(edata122, '') || ' ' || coalesce(edata123, '') || ' ' || coalesce(edata124, '') as exhibit_word
      , del_flg
      , invalid_flg
      , rdate
      , udate

    from
        entory_r
    ) as entry_r

where
      del_flg = 0
  and invalid_flg = 0
  and form_id = '55'
  and rdate <> udate
  {$where}

order by exhibit_table asc, udate desc;
__SQL__;
            return $obj->db->query($sql, $arrVal);

        // 日本語検索ページ
        }else{
            // 五十音
            $sql = <<< __SQL__
select
    *

from
    (
    select
        form_id
      , eid
      , case when edata27 = '1' then edata28 else edata81 end as exhibit_name
      , case when edata27 = '1' then edata63 else edata82 end as exhibit_table
      , case when edata27 = '1' then
            edata28 || ' ' || coalesce(edata75, '') || ' ' || coalesce(edata76, '') || ' ' || coalesce(edata77, '') || ' ' || coalesce(edata78, '') || ' ' || coalesce(edata79, '')
          else
            edata81 || ' ' || coalesce(edata119, '') || ' ' || coalesce(edata120, '') || ' ' || coalesce(edata121, '') || ' ' || coalesce(edata122, '') || ' ' || coalesce(edata123, '') || ' ' || coalesce(edata124, '')
          end as exhibit_word
      , del_flg
      , invalid_flg
      , rdate
      , udate

    from
        entory_r
    ) as entry_r

where
      del_flg = 0
  and invalid_flg = 0
  and form_id = '55'
  and rdate <> udate
  {$where}

order by exhibit_table asc, udate desc;
__SQL__;

            return $obj->db->query($sql, $arrVal);
        }
    }


    // 検索ページ # コンストラクタ
    function Usr_Detail_construct($obj){
        $this->Usr_Search_construct($obj);
    }


    function Usr_Detail_defaultAction($obj){
        // 言語が指定されていない場合は「日本語」
        $obj->searchLang   = (isset($_GET['lang'])) ? $_GET['lang'] : LANG_JPN;
        if(!in_array($obj->searchLang, array(LANG_JPN, LANG_ENG))) $obj->searchLang = LANG_JPN;


        try{
            //応募データ取得
            $obj->arrData = $obj->o_entry->getRntry_r($obj->db, $obj->eid, $obj->form_id);
            if(!$obj->arrData) throw new Exception("不正な値を検出");
            // ログインIDをブース番号にする
            // フォーム頭文字の長さ # 「-」連結分+1する
            $len_formhead = strlen($obj->formdata["head"])+1;
            $obj->arrData['e_user_id'] = substr($obj->arrData['e_user_id'], $len_formhead);
            // 同一ブース番号で複数の出展がある場合を考慮
            if(strpos($obj->arrData['e_user_id'], "#") !== false){
                $tmp = explode("#", $obj->arrData['e_user_id']);
                $obj->arrData['e_user_id'] = $tmp[0];
            }

        }catch(Exception $e){
            $obj->arrData = NULL;
        }

        $obj->assign("arrData"   , $obj->arrData);
        $obj->assign("searchLang", $obj->searchLang);
        $obj->_processTemplate = "Usr/include/55/search/Usr_detail.html";
    }


    // ------------------------------------------------------
    // ▽ログイン画面
    // ------------------------------------------------------

    function usr_login_construct($obj){
        require_once(ROOT_DIR."application/cnf/en.php");
        $GLOBALS["msg"]["btn_login"] = "Login";
        $GLOBALS["msg"]["login_id"]  = "Login ID (Booth  No.)";

        $GLOBALS["msg"]["msg_login"]            = "パスワードを入力してください";
        $GLOBALS["msg"]["err_not_loginid"]      = "ブース番号又は、パスワードが間違っています。/ Incorrect login Booth No. or password.";
        $GLOBALS["msg"]["err_mistake_password"] = "ブース番号又は、パスワードが間違っています。/ Incorrect login Booth No. or password.";
        $GLOBALS["msg"]["account_same"]         = "既に同じIDでログイン中です。/ It is currently logged in with the same ID. ";

        $obj->_processTemplate = "Usr/include/55/Usr_login.html";
    }

    function usr_login__check($obj) {
        $objErr = New Validate;

        // ログインID入力不備
        if(!$objErr->isNull($obj->arrForm["login_id"])) {
            $objErr->addErr("ブース番号を入力してください。/ Enter Booth No.", $GLOBALS["msg"]["login_id"]);
        }
        // パスワード入力不備
        if(!$objErr->isNull($obj->arrForm["password"])) {
            $objErr->addErr($GLOBALS["msg"]["msg_login"], $GLOBALS["msg"]["login_passwd"]);
        }
        return $objErr->_err;
    }


    function usr_login_defaultAction($obj){
        $obj->arrErr = $GLOBALS["session"]->getVar("error");
        $obj->assign("msg", "IDと".$GLOBALS["msg"]["msg_login"]."<br>Enter your ID and Password to login.");
    }


    // ------------------------------------------------------
    // ▽パスワード再発行
    // ------------------------------------------------------

    // コンストラクタ
    function Usr_PasswdReset_construct($obj){
        require_once(ROOT_DIR."application/cnf/en.php");
        $obj->msg = "ブース番号とメールアドレスを入力してください。<br>Enter your booth no. (ID) and e-mail address";
        $GLOBALS["msg"]["passwd_user_id"]      = "Login ID (Booth No.)";
        $GLOBALS["msg"]["passwd_mailaddress"]  = "E-mail address";
        $GLOBALS["msg"]["passwd_btn"]          = "再発行 / Reissue";
        $GLOBALS["msg"]["passwd_err_not_auth"] = "ブース番号又は、パスワードが間違っています。/ Incorrect login Booth No. or password.";
        $obj->assign("extends_msg", "<br>Exhibitor Search Registration Form Password Reissue");
    }


    function Usr_PasswdReset_premain($obj){
        if(count($obj->arrErr) > 0){
            // エラーメッセージをバインド
            if(isset($obj->arrErr['user_id'])){
                $obj->arrErr['user_id'] = "ブース番号を入力してください。/ Enter Booth No.";
            }
            if(isset($obj->arrErr['mailaddress'])){
                $obj->arrErr['mailaddress'] = "メールアドレスを入力してください。/ Enter E-mail address.";
            }
            $obj->assign("arrErr", $obj->arrErr);
        }
    }

    /**
     * 登録Noとメールアドレスの妥当性チェックをして結果を返す
     *
     * @param  integer $form_id チェック対象のフォーム
     * @param  array   $arrForm チェックパラメータ
     * @return boolean true:存在する組
     */
    function Usr_PasswdReset_auth($obj, $form_id, $arrForm){
        $db = new DbGeneral(true);
        $arrVal  = array();
        $arrVal[] = $form_id;
        $arrVal[] = $GLOBALS["form"]->formData['head']."-".$arrForm['user_id'];
        $arrVal[] = $arrForm['mailaddress'];

        return $db->exists("entory_r", "form_id = ? and e_user_id = ? and edata25 = ? and del_flg = 0 and invalid_flg = 0", $arrVal);
    }


    /**
     * 登録Noに対応するメールアドレスへパスワードを通知する
     *   登録Noとメールアドレスの妥当性が確認されていることが前提
     *
     * @param  integer $form_id チェック対象のフォーム
     * @param  array   $arrForm チェックパラメータ
     * @return boolean
     */
    function Usr_PasswdReset_sendMail($obj, $form_id, $arrForm){
        $db = new DbGeneral(true);
        $arrVal  = array();
        $arrVal[] = $form_id;
        $arrVal[] = $GLOBALS["form"]->formData['head']."-".$arrForm['user_id'];
        $arrVal[] = $arrForm['mailaddress'];

        // パスワード取得
        $arrData = $db->getRow("eid, form_id, e_user_passwd, edata10", "entory_r", "form_id = ? and e_user_id = ? and edata25 = ? and del_flg = 0 and invalid_flg = 0", $arrVal);
        $eid     = $arrData["eid"];
        $passwd  = $arrData["e_user_passwd"];
        $name    = $arrData["edata10"];
        $mail    = $arrForm['mailaddress'];

        //-------------------------------
        // パスワード再発行通知メールの送信
        //-------------------------------
        $objMail = new Mail();
        // メールタイトル
        $subject = "Reissue of password / パスワード再発行通知メール";

        // メール本文
        $url = URL_ROOT."Usr/Usr_login.php?form_id=".$form_id;
        $body  = "";
        $body .= "(English follows Japanese)\n";
        $body .= "\n";
        $body .= "パスワード再発行を受け付けました。\n";
        $body .= "\n";
        $body .= "下記パスワードでログイン後、\n";
        $body .= "ご担当者情報内のパスワード変更用（現在のパスワード）入力欄に送信された仮パスワードを入力し、新しいパスワードへの変更を行って下さい。\n";
        $body .= "\n";
        $body .= "仮パスワード：{$passwd}\n";
        $body .= "\n";
        $body .= "(English shown below)\n";
        $body .= "The request for the reissue of your password has been received.\n";
        $body .= "\n";
        $body .= "Please use the following temporary password to log-in to the system.\n";
        $body .= "After you log-in to the system, please be sure to change the password by entering the necessary information in the “Contact information” section.\n";
         $body .= "\n";
//         $body .= "下記URLよりログイン後、応募情報の更新または任意のパスワードに変更することができます。\n";
//         $body .= "{$url}\n";
//         $body .= "\n";
        $body .= "--------------------------------------------------------------------\n";
        $body .= $obj->o_form->formData["contact"]."\n";
        $body .= "--------------------------------------------------------------------\n";
        $body  =  str_replace(array("\r\n", "\r"), "\n", $body);

        // 再発行があったことをログに残す
        log_class::printLog("パスワード再発行受付# form_id:{$form_id} eid:{$eid}");

        // メール送信処理
        $objMail->sendMail($arrForm['mailaddress'], $subject, $body, "CITE Japan Secretariat<".$obj->o_form->formData["form_mail"].">");

//        $obj->msg = "{$name}<{$mail}>";
        $obj->_processTemplate = "Usr/include/55/passwd/Usr_PasswdReset_complate.html";
    }



}
