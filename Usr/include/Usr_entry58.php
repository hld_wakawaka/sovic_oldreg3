<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry58 {

    // ----------------------------------------
    // カスタマイズメモ
    // ----------------------------------------

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
//        
//         // jQueryを読み込む
//         $obj->useJquery = true;
//        
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
//        
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
//        
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
    }


    /** 参加フォームのみ 金額、支払情報を生成 */
    function setMoneyPaydata($obj) {
        Usr_initial::setMoneyPaydata($obj);
        // 金額情報をクリアする
        $obj->wa_price = array();
    }


    /* デバッグ用 */
    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }


    function _check1($obj) {
        Usr_Check::_check1($obj);
        // Feeエラーを無視
        unset($obj->objErr->_err['amount']);

        //-----------------------------------------
        // No.3 # その他決済のどれかひとつを選択必須
        //-----------------------------------------
        $key = "ather_price";
        $val = array();         // その他決済項目で選択した項目
        $isNotStudent = false;  // Student以外を選択したフラグ
        $isStudent    = false;  // Studentを選択したフラグ
        $cntVLSI      = array();// その他決済1-4の選択項目
        for($i=0; $i<=9; $i++){
            $_key = $key.$i;
            if(!strlen($obj->arrParam[$_key]) > 0) continue;
            $val[] = $obj->arrParam[$_key];
            // not student
            if(!$isNotStudent){
                $isNotStudent = in_array($i, array(0, 2, 4, 6, 8, /*10,*/ 12));
            }
            // student
            if(!$isStudent){
                $isStudent = in_array($i, array(1, 3, 5, 7, 9, 11, 13));
            }
            // isMulti
            if(in_array($i, array(0, 1, 2, 3))){
                $cntVLSI[] = $obj->arrParam[$_key];
            }
        }
        if(count($val) == 0){
            $obj->objErr->addErr("Please select Fee table.", 'ather_price0');
        }

        while(true){
            if(count($val) == 0) break;

            //-----------------------------------------
            // No.4 # Studentとそれ以外は同時選択不可
            //-----------------------------------------
            if($isNotStudent && $isStudent){
                $obj->objErr->addErr('Student fees and general (non-student) fees can not be selected at the sametime. Student fees can only be selected by students.', 'ather_price0');
            }

            //-----------------------------------------
            // No.5 # 料金テーブルシートの1-4は複数選択できない
            //-----------------------------------------
            if(count($cntVLSI) > 1){
                $obj->objErr->addErr("Select only 1 item for \"Symposia on VLSI Technology and Circuits\".", 'ather_price0');
            }
            break;
        }
    }


    function _check3($obj) {
        $item_id = 55;
        $key     = "edata".$item_id;
        //-----------------------------------------
        // No.7 # 設問2が「Yes」ならば設問2の関連項目を必須
        //-----------------------------------------
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
            foreach(array(56, 67, 68, 99) as $item_id){
                $obj->itemData[$item_id]["item_check"] = (strlen($obj->itemData[$item_id]["item_check"]) > 0)
                ? $obj->itemData[$item_id]["item_check"]."|0"
                        : "0";
            }
        }

        //-----------------------------------------
        // No.10 # 設問は3は「Symposia on VLSI Technology and Circuits* with Technology Digest」および「Symposia on VLSI Technology and Circuits* with Circuits Digest」で必須
        //-----------------------------------------
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        $isQ3require = strlen($arrForm1['ather_price0']) > 0 || strlen($arrForm1['ather_price2']) > 0 || strlen($arrForm1['ather_price1']) > 0 || strlen($arrForm1['ather_price3']) > 0;
        if($isQ3require){
            foreach(array(100, 101) as $item_id){
                $obj->itemData[$item_id]["item_check"] = (strlen($obj->itemData[$item_id]["item_check"]) > 0)
                ? $obj->itemData[$item_id]["item_check"]."|0"
                        : "0";
            }
        }

        Usr_Check::_check3($obj);

        //-----------------------------------------
        // No.8 # 設問2が「No」 ならば設問2の関連項目を入力するとエラー
        //-----------------------------------------
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 2){
            foreach(array(56, 67, 68, 99) as $item_id){
                $key = "edata".$item_id;
                if(strlen($obj->arrParam[$key]) > 0){
                    $name = Usr_init::getItemInfo($obj, $item_id);
                    $obj->objErr->addErr("Not Enter ". $name. ".", $key);
                    $obj->arrParam[$key] = "";
                }
            }
        }

        //-----------------------------------------
        // No.11 # 設問3は上記以外の金額の場合に選択してはいけない
        //-----------------------------------------
        if(!$isQ3require){
            foreach(array(100, 101) as $item_id){
                $key = "edata".$item_id;
                if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
                    $name = Usr_init::getItemInfo($obj, $item_id);
                    $obj->objErr->addErr("Not Enter ". $name. ".", $key);
                    $obj->arrParam[$key] = "";
                }
            }
        }
    }


    // [form58[reg3]]_追加調整：メール本文の修正
    
    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makePaymentBody($obj, $exec_type){
    	// 決済なし
    	if($obj->formdata["kessai_flg"] != "1") return "";
    
    	//支払合計
    	if($exec_type == "1"){
    		$total = Usr_function::_setTotal(array(), $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
    	}else{
    		$total = $GLOBALS["session"]->getVar("ss_total_payment");
    	}
    	
    	$total = $total;
    
    
    	if($obj->formdata["lang"] == LANG_JPN){
    		$obj->point_mark = "■";
    		$body_pay = "\n\n【お支払情報】\n\n";
    	}else{
    		$obj->point_mark = "*";
    		$body_pay = "\n\n[Payment Information]\n\n";
    	}
    
    	//$body_pay .= "\n\n";
    	if($obj->formdata["lang"] == LANG_JPN){
    		// お支払確認ページの表示/非表示 : デフォルトは表示
    		if(!in_array($obj->form_id, $obj->payment_not)){
    			$body_pay .= $obj->point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
    		}
    		$body_pay .= $obj->point_mark."金額: \n";
    	}
    	else{
    		if($total > 0){
    			// お支払確認ページの表示/非表示 : デフォルトは表示
    			if(!in_array($obj->form_id, $obj->payment_not)){
    				$body_pay .= ($obj->arrForm["method"] != "3")
    				? $obj->point_mark."Payment: ".$obj->wa_method[$obj->arrForm["method"]]."\n"
    						: $obj->point_mark."Payment :Cash on-site\n";
    			}
    		}
    		$body_pay .= $obj->point_mark."Amount of Payment:\n";
    	}
    
    
    	// Fee
    	$body_pay .= $obj->makePaymentBody1($exec_type);
    
    	//その他決済がある場合
    	$body_pay .= $obj->makePaymentBody2($exec_type);
    
    
    	if($obj->formdata["lang"] == LANG_JPN){
    		$body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
    	}
    	else{
    		$body_pay .="      Amount of Total Payment:".$obj->yen_mark.number_format($total)."\n\n";
    	}
    
    
    	// お支払情報のメール生成
    	// お支払確認ページの表示/非表示 : デフォルトは表示
    	if(!in_array($obj->form_id, $obj->payment_not)){
    		$body_pay .= $obj->makePaymentMethodBody($total);
    	}
    
    	return $body_pay;
    }
    
    /** 金額のメール本文 */
    function makePaymentBody1($obj, $exec_type){
        return "";
    }
    
    function makePaymentBody2($obj, $exec_type){
    	$body_pay = "";
    	// 決済なし
    	if($obj->formdata["kessai_flg"] != "1") return "";
    
    	//その他決済がある場合
    	if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){
    
    		//新規の場合
    		if($exec_type == "1"){
    			foreach($obj->wa_ather_price  as $p_key => $data ){
    				if(!isset($obj->arrForm["ather_price".$p_key])) continue;
    				if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
    					$price = $data["p1"];
    
    					// Free, - => 0
    					if(!is_numeric($data["p1"])){
    						$data["p1"] = 0;
    					}
    					$sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];
    
    					// タグを除去
    					//                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
    					$data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除
    
    					if($obj->formdata["lang"] == LANG_JPN){
    						if(is_numeric($price)){
    							$price = number_format($price)."円";
    						}
    						$body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
    					}
    					else{
    						if(is_numeric($price)){
    							$price = $obj->yen_mark.number_format($price);
    						}
    						$body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum)."  (".$price." ×".$obj->arrForm["ather_price".$p_key].")\n";
    					}
    				}
    			}
    
    		}
    
    		//更新の場合
    		if($exec_type == "2"){
    			//更新の場合は、登録済みのデータから引っ張ってくる
    			$wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);
    
    			foreach($wa_payment_detail as $data){
    				$price = $data["price"];
    				// Free, - => 0
    				if(!is_numeric($data["price"])){
    					$data["price"] = 0;
    				}
    				$sum = $data["price"]*$data["quantity"];
    
    				// タグを除去
    				//                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
    				$data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除
    
    				if($obj->formdata["lang"] == LANG_JPN){
    					if(is_numeric($price)){
    						$price = $price."円";
    					}
    					$body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
    				}
    				else{
    					if(is_numeric($price)){
    						$price = $obj->yen_mark.$price;
    					}
    					$body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum);
    				}
    
    				// その他決済の場合は内訳を後ろにつける
    				if($data["type"] == "1"){
    					if($obj->formdata["lang"] == LANG_JPN){
    						$body_pay .="　（".$price." ×".$data["quantity"]."）";
    					}
    					else{
    						$body_pay .="  (".$price." ×".$data["quantity"].")";
    					}
    				}
    				$body_pay .= "\n";
    			}
    		}
    	}
    	return $body_pay;
    }
    
    /** メール本文生成 お支払情報 */
    function makePaymentMethodBody($obj, $total){
    	$body_pay = "";
    	if(!$total > 0) return $body_pay;
    
    	$actionName = "makePaymentMethodBody". $obj->arrForm["method"];
    	if(method_exists($obj, $actionName) && strlen($obj->arrForm["method"]) > 0){
    		$body_pay = $obj->$actionName($total);
    	}
    
    	return $body_pay;
    }
    
    
    function makePaymentMethodBody1($obj, $total){
    	$body_pay = "";
    	if(!$total > 0) return $body_pay;
    	// オンライン決済のみ本文を生成する
    	if($obj->formdata["credit"] != "2") return $body_pay;
    
    	if($obj->formdata["lang"] == LANG_JPN){
    		$wk_pay_card= "カード会社";
    		$wk_pay_card_num = "クレジットカード番号";
    		$wk_pay_card_limit = "有効期限";
    		$wk_pay_card_meigi = "カード名義人";
    		$wk_pay_month = "月";
    		$wk_pay_year = "年";
    	}
    	else{
    		$wk_pay_card= "Card Type";
    		$wk_pay_card_num = "Card Number";
    		$wk_pay_card_limit = "Expiration Date";
    		$wk_pay_card_meigi = "Card Holder's Name";
    		$wk_pay_month = "Month";
    		$wk_pay_year = "Year";
    	}
    
    	$body_pay .=$obj->point_mark.$wk_pay_card.":".$obj->wa_card_type[$obj->arrForm["card_type"]]."\n";
    	$body_pay .=$obj->point_mark.$wk_pay_card_num.":";
    
    	// カード番号をマスクする桁数
    	$a = 2;
    	switch($obj->arrForm["card_type"]){
    		//VISA
    		//MasterCard
    		//JCB
    		case 1:
    		case 2:
    		case 5:
    			$a = 16 -2;
    			break;
    
    			//Diners Club
    		case 3:
    			$a = 14 -2;
    			break;
    
    			//AMEX
    		case 4:
    			$a = 15 -2;
    			break;
    	}
    	$card = "";
    	$body_pay .= sprintf("%'*".$a."s", $card).substr($obj->arrForm["cardNumber"],-2)."\n";
    	$body_pay .= $obj->point_mark.$wk_pay_card_limit.":".$obj->arrForm["cardExpire1"].$wk_pay_month."　".$obj->arrForm["cardExpire2"].$wk_pay_year."\n";
    	$body_pay .= $obj->point_mark.$wk_pay_card_meigi.":".$obj->arrForm["cardHolder"]."\n";
    
    	return $body_pay;
    }
    
    
    function makePaymentMethodBody2($obj, $total){
    	$body_pay = "";
    	if(!$total > 0) return $body_pay;
    
    	if($obj->formdata["lang"] == LANG_JPN){
    		$wk_pay_furikomi= "お振込み人名義";
    		$wk_pay_bank = "お支払銀行名";
    		$wk_pay_furikomibi = "お振込み日";
    	}
    	else{
    		$wk_pay_furikomi= "Name of remitter";
    		$wk_pay_bank = "Name of bank which you will remit";
    		$wk_pay_furikomibi = "Date of remittance";
    	}
    
    	$pay2key = array("lessee", "bank", "closure");
    	foreach($pay2key as $_key => $_val){
    		if(!isset($obj->arrForm[$_val])) $obj->arrForm[$_val] = "";
    	}
    
    	$body_pay .=$obj->point_mark.$wk_pay_furikomi.": ".$obj->arrForm["lessee"]."\n";
    	$body_pay .=$obj->point_mark.$wk_pay_bank.": ".$obj->arrForm["bank"]."\n";
    	$body_pay .=$obj->point_mark.$wk_pay_furikomibi.": ".$obj->arrForm["closure"]."\n";
    
    	return $body_pay;
    }

}
