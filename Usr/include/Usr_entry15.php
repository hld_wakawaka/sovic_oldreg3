<?php

/**
 * reg3form15番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2014.03.03
 * 
 */
class Usr_Entry15 {

    private $arrDisabled15 = array();

    function __construct($obj){
        // 支払方法を削除
        unset($GLOBALS["method_E"][2]);

        // assign限定処理
        if(!method_exists($obj, "assign")) return;

        // 定員に達すると選択できないようにする
        $this->arrDisabled15 = array();  // 定員に達したその他決済

        // 定員数を取得
        $arrQuota = $this->getQuota($obj);
        if(count($arrQuota) > 0){
            $total = $this->getTotal($obj);

            foreach($total as $_key => $_val){
                if(!isset($arrQuota[$_key])) continue;

                // 定員 <= 応募者数で対象の項目を選択不可
                if($arrQuota[$_key] <= $_val){
                    $this->arrDisabled15[$_key] = 1;
                }
            }
        }
        $obj->assign("arrDisabled15", $this->arrDisabled15);
    }


    /* その他決済の各定員数を取得 */
    function getQuota($obj){
        $arrQuota = array();

        $quota1 = 'ather_price*_*';
        $quota2 = 'ather_price([0-9]+)_([0-9]+)';

        $form_id   = $obj->o_form->formData['form_id'];
        $customDir = ROOT_DIR."customDir/".$form_id."/";
        $ex = glob($customDir.$quota1);
        if(is_array($ex)){
        foreach($ex as $_key => $script){
                if(preg_match("/".$quota2."$/u", $script, $match) !== 1) continue;

                $arrQuota['ather_price'.$match[1]] = $match[2];
            }
        }
        return $arrQuota;
    }


    /* その他決済の集計データ */
    function getTotal($obj){
        //集計データ
        $total = array();

        $column  = " sum(cnt1) as ather_price1";
        $column .= ",sum(cnt2) as ather_price2";
        $column .= ",sum(cnt3) as ather_price3";

        $from = "(";
        $from .= " select";
        $from .= "  COUNT(CASE WHEN payment_detail.key = 1 THEN 1 END) AS cnt1";
        $from .= " ,COUNT(CASE WHEN payment_detail.key = 2 THEN 1 END) AS cnt2";
        $from .= " ,COUNT(CASE WHEN payment_detail.key = 3 THEN 1 END) AS cnt3";
        $from .= " ";
        $from .= " ";
        $from .= " from payment_detail";
        $from .= " LEFT OUTER JOIN entory_r";
        $from .= " on payment_detail.eid = entory_r.eid";
        $from .= " ";
        $from .= " where";
        $from .= "      payment_detail.type = 1";
        $from .= "  and payment_detail.del_flg = 0";
        $from .= "  and entory_r.form_id = 15";
        $from .= "  and entory_r.del_flg = 0";
        $from .= "  and entory_r.invalid_flg = 0";
        $from .= " ";
        $from .= " group by payment_detail.key";
        $from .= ") AS cnt";

        $rs = $obj->db->getListData($column, $from, NULL, NULL);
        if(isset($rs[0])){
            $total = $rs[0];
        }

        return $total;
    }


    /** 開発用のデバッグ関数 */
    function developfunc($obj) {
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody(1585, "11", 1));
//        print "</pre><br/><br/>";
    }



    //--------------------------------
    // 1ページ目
    //--------------------------------
    function _check1($obj){
        Usr_Check::_check1($obj);

        //-------------------------------
        // AccompanyingPersonの数
        //-------------------------------
        $cnt = 0;

        // AccompanyingPerson 1
        $keys = array('edata64', 'edata69', 'edata70'/*, 'edata71'*/);
        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
                $method = $GLOBALS["msg"]["err_require_input"];
                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 1'), 'edata64');
            }else{
                $cnt++;
            }
        }

        // AccompanyingPerson 2
        $keys = array('edata72', 'edata73', 'edata74'/*, 'edata75'*/);
        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
                $method = $GLOBALS["msg"]["err_require_input"];
                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 2'), 'edata72');
            }else{
                $cnt++;
            }
        }

        if(intval($obj->arrParam['ather_price0']) != $cnt){
            $obj->objErr->addErr('The No. of Accompanying persons is incorrect.', 'ather_price0');
        }
    }


    //--------------------------------
    // 3ページ目
    //--------------------------------
    function _check3($obj){
        Usr_Check::_check3($obj);

        // 5) Dietary requirement
        if(Usr_Check::chkSelect($obj, 3, 68, 'Allergy')){
            $item_id = 99;
            if(!(strlen($obj->arrParam['edata'.$item_id]) > 0)){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }
    }


    function setMoneyPaydata($obj){
        Usr_initial::setMoneyPaydata($obj);

        // assign限定処理
        if(!method_exists($obj, "assign")) return;

        // 金額を1つに固定
        unset($obj->wa_price[1]);
        $obj->assign("va_price", $obj->wa_price);

        // その他決済を2つのテーブルに分ける
        $c = count($obj->wa_ather_price);
        $other_price1 = array_slice($obj->wa_ather_price, 0,  1, true);
        $other_price2 = array_slice($obj->wa_ather_price, 1, $c, true);

        $obj->assign("va_price1", $other_price1);
        $obj->assign("va_price2", $other_price2);
    }


    /** プライバシーポリシー */
    function agreeAction($obj) {
        // デフォルトでDelegateを選択済み
        $obj->arrForm['amount'] = "0";

        return Usr_pageAction::agreeAction($obj);
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        $this->checkQuota($obj);
    }


    /** 完了ページ */
    function completeAction($obj) {
        if($this->checkQuota($obj)) return;

        return Usr_pageAction::completeAction($obj);
    }


    function checkQuota($obj){
        // 定員数を常にチェックする
        if(count($this->arrDisabled15) == 0) return;

        // 1ページ目の入力情報
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");

        $error = false;
        foreach($this->arrDisabled15 as $_key => $_val){
            if(isset($arrForm1[$_key]) && $arrForm1[$_key] > 0){
                $error = true;
            }
        }

        if($error){
            $obj->arrErr['atherprice'] = "The tour which you have selected has been closed because it have reached its maximum capacity.";
            $obj->block = "1";
            $obj->_processTemplate =  "Usr/form/Usr_entry.html";

            $obj->assign("arrErr",  $obj->arrErr);
            $obj->assign("block",   $obj->block);
        }
        return $error;
    }



    //------------------------------------------------
    // ▽ メールカスタマイズ
    //------------------------------------------------

    /** AccompanyingPerson1 */
    function mailfunc64($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str  = "\n";
        $str .= "\n";
        $str .= "[Accompanying Person]\n";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }


    /** AccompanyingPerson2 */
    function mailfunc72($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }



    //------------------------------------------------
    // ▽ CSVカスタマイズ
    //------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $prefix = "";
            $item_id = $_data['item_id'];
            // AccompanyingPerson 1
            if(in_array($item_id, array(64,69,70,71))){
                $prefix = "AccompanyingPerson 1 ";
            }
            // AccompanyingPerson 2
            if(in_array($item_id, array(72,73,74,75))){
                $prefix = "AccompanyingPerson 2 ";
            }

            $groupHeader[$group][] = "\"".$prefix.$name."\"";
        }
        return $groupHeader[$group];
    }



    //------------------------------------------------
    // ▽ メールのカスタマイズ
    //------------------------------------------------

    function makeMailBody($obj, $user_id="", $passwd="", $exec_type=""){
        // メール上部コメントの作成
        $head_comment = $obj->makeMailBody_header($user_id, $passwd, $exec_type);

        // 入力フォームのメール本文
        $arrbody = $obj->makeFormBody();


        //----------------------------------------
        //本文生成
        //----------------------------------------
        $body  = "";
        $body .= $head_comment."\n\n";
        $body .= $arrbody[1];
        if($obj->formdata["group2_use"] != "1") $body .= $arrbody[2];
        if($obj->formdata["group3_use"] != "1") $body .= $arrbody[3];
        if($obj->formdata["kessai_flg"] == "1") $body .= $obj->makePaymentBody($exec_type);
        $body .= $obj->makeEditBody($user_id, $passwd, $exec_type);

        $body .= "\n";
        $body .= "Go back to IFM2014 website : http://www.ifm2014.com/index.html\n";
        $body .= "------------------------------------------------------------------------------\n";
        $body .= $obj->formdata["contact"];
        $body .= "\n------------------------------------------------------------------------------\n";

        return str_replace(array("\r\n", "\r"), "\n", $body);
    }



/**************************************************************************************
  その他決済項目の集計用SQL
***************************************************************************************

select 
 payment_detail.eid
,payment_detail.key


from payment_detail
LEFT OUTER JOIN entory_r
on payment_detail.eid = entory_r.eid

where
     payment_detail.type = 1
 and payment_detail.del_flg = 0
 and entory_r.form_id = 15
 and entory_r.del_flg = 0


===========================

select
 sum(cnt1) as ather_price1
,sum(cnt2) as ather_price2
,sum(cnt3) as ather_price3

from
 (
    select
     COUNT(CASE WHEN payment_detail.key = 1 THEN 1 END) AS cnt1
    ,COUNT(CASE WHEN payment_detail.key = 2 THEN 1 END) AS cnt2
    ,COUNT(CASE WHEN payment_detail.key = 3 THEN 1 END) AS cnt3
    
    
    from payment_detail
    LEFT OUTER JOIN entory_r
    on payment_detail.eid = entory_r.eid
    
    where
         payment_detail.type = 1
     and payment_detail.del_flg = 0
     and entory_r.form_id = 15
     and entory_r.del_flg = 0
    
    group by payment_detail.key
  ) as cnt

***************************************************************************************/

}
