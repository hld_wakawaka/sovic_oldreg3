<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */

class Usr_Entry77 {

   /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj) {
       // form_idの取得
       $form_id = $obj->o_form->formData ['form_id'];
       
       // [reg3-form77] form77ではお支払い情報（銀行振込やクレジット）は表示しない
       $obj->payment_not[] = $form_id;
       
       // jQueryを読み込む
       $obj->useJquery = true;
       
   }

   /* デバッグ用 */
   function developfunc($obj) {
        // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
   }


   //--------------------------------------
   // No.2 # ページスキップ
   //--------------------------------------
   // [reg3-form77]「見積No.2:お支払い方法の非表示」ページ遷移時にお支払い方法入力画面をスキップ
   /** 進むボタン */
   function pageAction1($obj) {

       if(method_exists($obj, "assign")){
           $obj->assign("va_ather_price", $obj->wa_ather_price);
       }
       
       Usr_pageAction::pageAction1($obj);
       
       // 確認画面へ遷移
       $obj->block = "4";
       $obj->_processTemplate = "Usr/form/Usr_entry_confirm.html";
   }

   /** 戻るボタン */
   function backAction($obj) {
       Usr_pageAction::backAction($obj);
       
       // 入力画面へ遷移
       $obj->block = 1;
       $obj->_processTemplate = "Usr/form/Usr_entry.html";
   }
   
   
   // ------------------------------------------------------
   // ▽メールカスタマイズ
   // ------------------------------------------------------
   
   // [reg3-form77]「見積No.2:お支払い方法の非表示」お支払い方法が非表示になっているか(メール)
   function makePaymentBody($obj, $exec_type){
       // 決済なし
       if($obj->formdata["kessai_flg"] != "1") return "";
        
       //支払合計
       if($exec_type == "1"){
           $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
       }else{
           $total = $GLOBALS["session"]->getVar("ss_total_payment");
       }
        
        
       if($obj->formdata["lang"] == LANG_JPN){
           $obj->point_mark = "■";
           $body_pay = "\n\n【お支払情報】\n\n";
       }else{
           $obj->point_mark = "*";
           $body_pay = "\n\n[Payment Information]\n\n";
       }
        
       //$body_pay .= "\n\n";
       if($obj->formdata["lang"] == LANG_JPN){
           $body_pay .= $obj->point_mark."金額: \n";
       }
       else{
           $body_pay .= $obj->point_mark."Amount of Payment:\n";
       }
        
        
       // Fee
       $body_pay .= $obj->makePaymentBody1($exec_type);
        
       //その他決済がある場合
       $body_pay .= $obj->makePaymentBody2($exec_type);
        
        
       if($obj->formdata["lang"] == LANG_JPN){
           $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
       }
       else{
           $body_pay .="      Amount of Total Payment:".$obj->yen_mark.number_format($total)."\n\n";
       }
        
       // お支払情報のメール生成
       // お支払確認ページの表示/非表示 : デフォルトは表示
       if(!in_array($obj->form_id, $obj->payment_not)){
           $body_pay .= $obj->makePaymentMethodBody($total);
       }
        
       return $body_pay;
   }
   
   // [reg3-form77]「見積No.1:Feeの非表示」Fee情報が非表示になっているか(メール)
   /** 金額のメール本文 */
   function makePaymentBody1($obj, $exec_type){
       $body_pay = "";
       // 決済なし
       if($obj->formdata["kessai_flg"] != "1") return "";
       
       return $body_pay;
   }
   
   
   // ------------------------------------------------------
   // ▽mng画面カスタマイズ
   // ------------------------------------------------------
   
   // [reg3-form77]「見積No.1:Feeの非表示」Fee情報が非表示になっているか(CSV)
   function __constructMngPaymentCSV($obj) {
       unset($obj->wa_ather_price["-1"]);
   }
   
   // 金額、その他決済項目のCSV整形
   function payment_pay_c($obj, $pa_detail){

       $isSet = array();
       foreach($obj->wa_ather_price as $key => $ather_name){
           $quantity = "";
           $match_flg = "";
           // 金額表示フラグ
           $price_flg = "";
           $price     = "";
   
           foreach($pa_detail as $_key => $data){
               // バッファ済みのキーは判定しない
               if(in_array($_key, $isSet)) continue;
                
   
               // 金額
               if($data['type'] == 0){
   
               // その他決済
               }elseif($key== $data["key"]){
                   $quantity = $data["quantity"];
                   $match_flg = "1";
   
                   $price_flg = "1";
                   $price = $data['price'];
                   break 1;
               }
           }
   
   
           if($match_flg == "1"){
               $wk_buff[] = "\"".GeneralFnc::smarty_modifier_del_tags($quantity)."\"";
               // 明細表示フラグ
               if($obj->payment_csv_price_disp && $price_flg == "1"){
                   $wk_buff[] = "\"".$price."\"";
               }

               // バッファ済みのキーをセット
               $isSet[] = $_key;
               $isSet[] = $_key."t";

           }else{
               $wk_buff[] = "\"\"";
               // 明細表示フラグ
               if($obj->payment_csv_price_disp){
                   $wk_buff[] = "\"\"";
               }
           }
       }
   
       return $wk_buff;
   }
}
