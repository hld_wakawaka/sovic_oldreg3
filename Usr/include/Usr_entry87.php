<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hunglead kawauchi
 *        
 */
class Usr_Entry87 {

//    /* 共通設定のoverride !Mngでも呼ばれる! */
//    function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
//        
//         // jQueryを読み込む
//         $obj->useJquery = true;
//        
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
//        
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
//        
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
//    }

//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }

//    /**
//     * ProcessBase::mainの前処理
//     */
   function premain($obj) {
       // Assignを直前で変更する場合や特定のエラーチェックで利用
   }

   // [reg3-form87] 「見積No.A2」:「Banquet: Student/Child」を選んだ場合、 「Regular」かつ「Banquet: Adult」を1枚以上選んでいないとエラー 
   // エラーチェック # ブロック1
   function _check1($obj) {
       Usr_Check::_check1($obj);
       
       $group_id = 1;
      
       $ather_price1 = strlen($obj->arrParam ['ather_price1']) > 0 ? intval($obj->arrParam ['ather_price1']) : - 1;
       $ather_price0 = strlen($obj->arrParam ['ather_price0']) > 0 ? intval($obj->arrParam ['ather_price0']) : - 1;
       $amount = $obj->arrParam ['amount'];
       
       if($amount == "0") {
           
           if ($ather_price1 > 0) {
               
               if (!($ather_price0 > 0)) {
                   $obj->objErr->addErr("Those who applies for “Regular” has to select at least one “Banquet: Adult” to purchase Banquet tickets.", "ather_price1");
               }
               
           }
           
       }
       
       
   }
   
}
