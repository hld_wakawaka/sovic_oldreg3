<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry20 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // jQueryを読み込む
        $obj->useJquery = true;

//        // form_idの取得
//        $form_id = $obj->o_form->formData['form_id'];
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1662;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('IFF-reg-00001', "5wyTXSHk", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        $item_id = 69;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
            $edata69 = intval($obj->arrParam['edata69']);

            // （A）のプルダウンで1～4を選択した場合は（B）の選択を必須にする。
            if(in_array($edata69, array(1, 2, 3, 4)) && strlen($obj->arrParam['edata81']) == 0){
                $item_id = 81;
                $key     = "edata".$item_id;
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }

            // （A）のプルダウンで5～8を選択した場合に（B）を選択したらエラー
            if(in_array($edata69, array(5, 6, 7, 8)) && strlen($obj->arrParam['edata81']) > 0){
                $item_id = 81;
                $key     = "edata".$item_id;
                $name = Usr_init::getItemInfo($obj, $item_id);
                $obj->objErr->addErr(sprintf('%sは選択しないでください', $name), $key);
            }
        }

        // xxで1番目の項目を選択したらテキスト必須
        $item_id = 71;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
            $edata71 = intval($obj->arrParam[$key]);
            switch($edata71){
                case 1: // IFF
                    // FBSの項目選択エラー
                    $item_id = 79;
                    $key     = "edata".$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                        $name = Usr_init::getItemInfo($obj, $item_id);
                        $obj->objErr->addErr(sprintf('%sは選択しないでください', $name), $key);
                    }


                    // IFFの項目の必須
                    $ids = array(72, 73);
                    foreach($ids as $_key => $item_id){
                        $key     = "edata".$item_id;
                        if(!Usr_init::isset_ex($obj, $group_id, $item_id)){
                            $name = Usr_init::getItemInfo($obj, $item_id);
                            $method = Usr_init::getItemErrMsg($obj, $item_id);
                            $obj->objErr->addErr(sprintf($method, $name), $key);
                        }
                    }
                    break;

                case 2: // FBS
                    // IFFの項目選択エラー
                    $ids = array(72, 73, 82);
                    foreach($ids as $_key => $item_id){
                        $key     = "edata".$item_id;
                        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                            $name = Usr_init::getItemInfo($obj, $item_id);
                            $obj->objErr->addErr(sprintf('%sは選択しないでください', $name), $key);
                        }
                    }
                    $item_id = 74;
                    $key     = "edata".$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
                        $name = Usr_init::getItemInfo($obj, $item_id);
                        $obj->objErr->addErr(sprintf('%sは入力しないでください', $name), $key);
                    }

                    // FBSの必須
                    $item_id = 79;
                    $key     = "edata".$item_id;
                    if(!Usr_init::isset_ex($obj, $group_id, $item_id)){
                        $name = Usr_init::getItemInfo($obj, $item_id);
                        $method = Usr_init::getItemErrMsg($obj, $item_id);
                        $obj->objErr->addErr(sprintf($method, $name), $key);
                    }
                    break;

                default: // non
                    break;
            }

        }
    }


   /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 10:
                    $array[83]  = $arrGroup1[83];
                    $array[$key]= $data;
                    $array[115] = $arrGroup1[115];
                    break;

                case 69:
                    $array[$key]= $data;
                    $array[81]  = $arrGroup1[81];
                    break;

                case 63:     // middle name
                    $array[$key]= $data;
                    $array[71]  = $arrGroup1[71];
                    break;

                case 20:     // middle name
                    $array[$key]= $data;
                    $array[80]  = $arrGroup1[80];
                    break;

                case 72:
                    $array[$key]= $data;
                    $array[82]  = $arrGroup1[82];
                    break;

                case 71:    // etc5
                case 79:    // etc5
                case 80:    // etc5
                case 81:
                case 82:
                case 83:
                case 115:
                    break;

                case 73:     // middle name
                    $array[$key]= $data;
                    $array[79]  = $arrGroup1[79];
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    // 氏名
    function mailfunc1($obj, $item_id, $name, $i=null) {
        $group = 1;

        if(!isset($obj->arrForm['edata1'])) $obj->arrForm['edata1'] = "";
        if(!isset($obj->arrForm['edata2'])) $obj->arrForm['edata2'] = "";
        $val1 = $obj->arrForm['edata1'];
        $val2 = $obj->arrForm['edata2'];

        $str = $obi.$obj->point_mark."氏名: ".$val1." ".$val2."\n";
        return $str;
    }
    function mailfunc2($obj, $item_id, $name, $i=null) {}


    // 氏名 # カナ
    function mailfunc3($obj, $item_id, $name, $i=null) {
        $group = 1;

        if(!isset($obj->arrForm['edata3'])) $obj->arrForm['edata3'] = "";
        if(!isset($obj->arrForm['edata4'])) $obj->arrForm['edata4'] = "";
        $val1 = $obj->arrForm['edata3'];
        $val2 = $obj->arrForm['edata4'];

        $str = $obi.$obj->point_mark."カナ: ".$val1." ".$val2."\n";
        return $str;
    }
    function mailfunc4($obj, $item_id, $name, $i=null) {}


    // 部署・役職
    function mailfunc12($obj, $item_id, $name, $i=null) {
        $group = 1;

        if(!isset($obj->arrForm['edata12'])) $obj->arrForm['edata12'] = "";
        if(!isset($obj->arrForm['edata13'])) $obj->arrForm['edata13'] = "";
        $val1 = $obj->arrForm['edata12'];
        $val2 = $obj->arrForm['edata13'];

        $str = $obi.$obj->point_mark."部署・役職名: ".$val1." ".$val2."\n";
        return $str;
    }
    function mailfunc13($obj, $item_id, $name, $i=null) {}


    // 住所
    function mailfunc17($obj, $item_id, $name, $i=null) {
        $group = 1;

        if(!isset($obj->arrForm['edata17'])) $obj->arrForm['edata17'] = "";
        if(!isset($obj->arrForm['edata18'])) $obj->arrForm['edata18'] = "";
        if(!isset($obj->arrForm['edata19'])) $obj->arrForm['edata19'] = "";
        if(!isset($obj->arrForm['edata20'])) $obj->arrForm['edata20'] = "";
        $val1 = $obj->arrForm['edata17'];
        $val2 = Usr_Assign::edata18($obj);
        $val3 = $obj->arrForm['edata19'];
        $val4 = $obj->arrForm['edata20'];
        $val5 = $obj->arrForm['edata80'];

        $str = $obi.$obj->point_mark."住所: 〒".$val1." ".$val2." ".$val3." ".$val4." ".$val5."\n";
        return $str;
    }
    function mailfunc18($obj, $item_id, $name, $i=null) {}
    function mailfunc19($obj, $item_id, $name, $i=null) {}
    function mailfunc20($obj, $item_id, $name, $i=null) {}
    function mailfunc80($obj, $item_id, $name, $i=null) {}


    // 職種 + その他
    // 追加1
    function mailfunc120($obj, $item_id, $name, $i=null) {
        $group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

        // その他
        $other = Usr_Assign::nini($obj, $group, 121, $obj->arrForm['edata121']);

        $obi = "\n【追加インビテーション希望者】\n\n";
        $obi.= "追加1\n";
        $str = $obi.$obj->point_mark.$name.": ".$value." ".$other."\n";
        return $str;
    }
    function mailfunc121($obj, $item_id, $name, $i=null) {}


    // 追加2
    function mailfunc124($obj, $item_id, $name, $i=null) {
        $group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

        // その他
        $other = Usr_Assign::nini($obj, $group, 125, $obj->arrForm['edata125']);

        $obi = "\n追加2\n";
        $str = $obi.$obj->point_mark.$name.": ".$value." ".$other."\n";
        return $str;
    }
    function mailfunc125($obj, $item_id, $name, $i=null) {}


    // 追加3
    function mailfunc128($obj, $item_id, $name, $i=null) {
        $group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

        // その他
        $other = Usr_Assign::nini($obj, $group, 129, $obj->arrForm['edata129']);

        $obi = "\n追加3\n";
        $str = $obi.$obj->point_mark.$name.": ".$value." ".$other."\n";
        return $str;
    }
    function mailfunc129($obj, $item_id, $name, $i=null) {}


    // 追加4
    function mailfunc132($obj, $item_id, $name, $i=null) {
        $group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

        // その他
        $other = Usr_Assign::nini($obj, $group, 133, $obj->arrForm['edata133']);

        $obi = "\n追加4\n";
        $str = $obi.$obj->point_mark.$name.": ".$value." ".$other."\n";
        return $str;
    }
    function mailfunc133($obj, $item_id, $name, $i=null) {}


    // 追加5
    function mailfunc136($obj, $item_id, $name, $i=null) {
        $group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

        // その他
        $other = Usr_Assign::nini($obj, $group, 137, $obj->arrForm['edata137']);

        $obi = "\n追加5\n";
        $str = $obi.$obj->point_mark.$name.": ".$value." ".$other."\n";
        return $str;
    }
    function mailfunc137($obj, $item_id, $name, $i=null) {}

    // 会社名
    function mailfunc83($obj, $item_id, $name, $i=null) {}
    function mailfunc115($obj, $item_id, $name, $i=null) {}

    function mailfunc10($obj, $item_id, $name, $i=null) {
        $group = 1;
        $edata83 = Usr_Assign::nini($obj, $group, 83, $obj->arrForm['edata83']);
        $edata10 = $obj->arrForm["edata".$item_id];
        $edata115= Usr_Assign::nini($obj, $group, 115, $obj->arrForm['edata115']);

        return sprintf($obj->point_mark.$name.": %s %s %s\n", $edata83, $edata10, $edata115);
    }



    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = "\"".$GLOBALS["csv"]["entryno"]."\"";

        // 業種その他
        $obj->arrItemData[$group][121]['item_view'] = 1;
        $obj->arrItemData[$group][125]['item_view'] = 1;
        $obj->arrItemData[$group][129]['item_view'] = 1;
        $obj->arrItemData[$group][133]['item_view'] = 1;
        $obj->arrItemData[$group][137]['item_view'] = 1;

        foreach($obj->arrItemData[$group] as $item_id => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            // 追加帯追加
            if(in_array($item_id, array(120, 121, 122, 123))) $name = "追加1 ".$name;
            if(in_array($item_id, array(124, 125, 126, 127))) $name = "追加2 ".$name;
            if(in_array($item_id, array(128, 129, 130, 131))) $name = "追加3 ".$name;
            if(in_array($item_id, array(132, 133, 134, 135))) $name = "追加4 ".$name;
            if(in_array($item_id, array(136, 137, 138, 139))) $name = "追加5 ".$name;

            $groupHeader[$group][] = "\"".$name."\"";
        }
        return $groupHeader[$group];
    }


    function csvfunc120($obj, $group, $pa_param, $item_id){
        $val = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return $val." ".$pa_param['edata121'];
    }
    function csvfunc124($obj, $group, $pa_param, $item_id){
        $val = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return $val." ".$pa_param['edata125'];
    }
    function csvfunc128($obj, $group, $pa_param, $item_id){
        $val = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return $val." ".$pa_param['edata129'];
    }
    function csvfunc132($obj, $group, $pa_param, $item_id){
        $val = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return $val." ".$pa_param['edata133'];
    }
    function csvfunc136($obj, $group, $pa_param, $item_id){
        $val = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return $val." ".$pa_param['edata137'];
    }

}
