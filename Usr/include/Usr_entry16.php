<?php

/**
 * reg3form16番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.04.17
 * 
 */
class Usr_Entry16 {

    public $stay;   /* 滞在可能日数 */
    public $person; /* 最大同伴者数 */

    function __construct($obj){
        // 滞在可能日数の取得
        $form_id   = $obj->o_form->formData['form_id'];
        $obj->stay = $this->getStay($form_id);

        // 最大同伴者数
        $obj->person = $this->getPersons($form_id);
        // 同伴者利用項目
        $obj->accompany = array(108, 109, 110, 111, 113, 145, 146, 147, 148, 151, 152);                 // 必須対象
        $obj->accompany2= array(108, 109, 110, 111, 112, 113, 145, 146, 147, 148, 149, 150, 151, 152);  // 全項目

        if(isset($obj->_smarty)){
            // 最大同伴者数
            $obj->assign("max_persons", $obj->person);

            // 修飾子を追加
            $obj->_smarty->register_modifier('form16_dateplus', array($this, 'form16_dateplus'));
            $obj->_smarty->register_modifier('form16_term99',   array($this, 'form16_term99'));
            $obj->_smarty->register_modifier('form16_dateindex',array($this, 'form16_dateindex'));
            $obj->_smarty->register_modifier('cnvDateFormat',   array($this, 'cnvDateFormat'));
        }

        // フォーム項目を配列にする特殊項目
        $obj->formarray = array(68, 99, 100, 101, 102, 103, 158, 106, 107);
        // △Schedule ▽AccompanyingPerson
        $obj->formarray = array_merge($obj->formarray, $obj->accompany2);

        // jQueryを読み込む
        $obj->useJquery = true;


        // 確認ページの文言変更
        $customDir = URL_ROOT."customDir/".$form_id."/";
        $img       = $customDir.'img/step_arrow.png';

        $GLOBALS['msg']['cofirm_title'] = 'Confirmation screen for your visa application';
        $GLOBALS['msg']['cofirm_desc1'] = '<span class="red"><b>STEP 1:<br/>'
                                         .'Your visa document will be made as it appears on your entry.<br/>'
                                         .'Please double check that all the information you entered is correct, print this screen and keep it until you receive the visa document from us.<br/>'
                                         .'</b></span>'
                                         .'<div class="btn m0"><input type="button" value="Print" onclick="window.print();" /></div>'
                                         .'<img src="'.$img.'" /><br/>';

        
        $GLOBALS['msg']['cofirm_desc2'] = '<b>STEP 2:<br/>'
                                         .'Your visa application has not been completed yet. <br/>'
                                         .'Please be sure to click the [register] button at the bottom to complete the procedure.<br/>'
                                         .'</b>';
    }


    // d/m/Y -> Y/m/d
    function cnvDateFormat($value, $sep="/"){
        $date  = explode($sep, $value);
        return date("Y-m-d", mktime(0, 0, 0, $date[1], $date[0], $date[2]));
    }


    // ------------------------------------------------------
    // ▽Smartyのカスタマイズ
    // ------------------------------------------------------

    // 日付を$interval日だけ経過させる
    function form16_dateplus($date, $interval=0){
        $date = $this->cnvDateFormat($date);
        return date("d/m/Y", strtotime($date." +".$interval." day"));
    }

    // Activity # 7/13-7/19の会期
    function form16_term99($value, $date){
        if(strlen($value) > 0) return $value;

        $date  = strtotime($this->cnvDateFormat($date));
        if($this->checkTerm99($date)){
            return 'Attend WCS2014';
        }

        return $value;
    }

    // htmlフォームで扱うキーを取得
    function form16_dateindex($date){
        // 先頭が日付だと「01」からはじまってしまうのでYmdの形式
        $date = $this->cnvDateFormat($date);
        return date("Ymd", strtotime($date));
    }


    //--------------------------------------------
    // 滞在可能日数を取得
    //--------------------------------------------
    public function getStay($form_id){
        $stay = 11; // デフォルトの滞在可能日数

        $stay1 = 'stayed/*';
        $stay2 = 'stayed\/([0-9]+)';
        $customDir = ROOT_DIR."customDir/".$form_id."/";

        $ex = glob($customDir.$stay1);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                if(preg_match("/".$stay2."$/u", $script, $match) === 1){
                    $stay = intval($match[1]);
                    break;
                }
            }
        }
        return $stay;
    }

    //--------------------------------------------
    // 最大同伴者数を取得
    //--------------------------------------------
    public function getPersons($form_id){
        $max = 2; // デフォルトの最大同伴者数

        $max1 = 'persons/*';
        $max2 = 'persons\/([0-9]+)';
        $customDir = ROOT_DIR."customDir/".$form_id."/";

        $ex = glob($customDir.$max1);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                if(preg_match("/".$max2."$/u", $script, $match) === 1){
                    $max = intval($match[1]);
                    break;
                }
            }
        }
        return $max;
    }


    /** 開発用のデバッグ関数 */
    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";

//        $obj->ins_eid = 1615;
//        $obj->sendRegistMail('VISA-00002', "rhfN2L3T");
    }

    function getEntry($obj){
        Usr_entryDB::getEntry($obj);

        // 特殊項目の対応 TODO 応急処置
        $arrForm = $GLOBALS["session"]->getVar("form_param3");
        foreach($obj->formarray as $item_id){
            $key = 'edata'.$item_id;
            $arrForm[$key] = unserialize($arrForm[$key]);
            foreach($arrForm[$key] as $_key => $str){
                $str = str_replace("[:n:]", "\n", $str);   // 改行コード統一
                $str = stripslashes($str);                 // クォート
                $arrForm[$key][$_key] = $str;
            }
        }
        $GLOBALS["session"]->setVar("form_param3", $arrForm);
        $obj->getDispSession();
    }


    /* 項目並び替え */
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 7:     // middle name
                    $array[$key]= $data;
                    $array[26]  = $arrGroup1[26];
                    $array[27]  = $arrGroup1[27];
                    $array[28]  = $arrGroup1[28];
                    $array[63]  = $arrGroup1[63];
                    $array[64]  = $arrGroup1[64];
                    break;

                case 26:    // etc1
                case 27:    // etc2
                case 28:    // etc3
                case 63:    // etc4
                case 64:    // etc5
                case 25:    // e-mail
                case 144:   // etc50
                case 79:    // etc16
                    break;

                case 72:    // etc9
                    $array[$key] = $data;
                    $array[25]   = $arrGroup1[25];
                    break;

                case 57:    // title
                    $array[144]  = $arrGroup1[144];
                    $array[$key] = $data;
                    break;

                case 80:    // etc17
                    $array[$key] = $data;
                    $array[79]   = $arrGroup1[79];
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }


    // ------------------------------------------------------
    // ▽簡易認証のカスタマイズ
    // ------------------------------------------------------

    /**
     * 簡易認証
     *
     */
    function pf_auth($obj) {
        $obj->AuthAction();       // 受付期間チェック

        $form_id   = $obj->o_form->formData["form_id"];
        $customDir = ROOT_DIR."customDir/".$form_id."/";
        $filename  = $customDir.'auth/entry_list.csv';

        if(!file_exists($filename)) return;

        // ログイン時は認証をスキップ
        if(isset($_GET["admin_flg"]) && strlen($_GET["admin_flg"]) > 0){
            $obj->eid = isset($_GET["eid"]) ? $_GET["eid"] : "";
        }
        if(strlen($obj->eid) > 0){
            // セッション内で1回認証していればスルー
            $auth_flg = $GLOBALS["session"]->getVar("auth");
            if($auth_flg == "1") return;

            $obj->getEntry();

            $arrForm = array();
            $arrForm["auth_id"]   = $obj->arrForm['edata144'];
            $arrForm["auth_mail"] = $obj->arrForm['edata25'];

            $GLOBALS["session"]->setVar("auth", "1");
            // RegistrationID, Emailを自動セット
            $GLOBALS["session"]->setVar("auth_pas",  $arrForm);
            $_REQUEST["mode"] = "";
            $obj->getDispSession();
            return;
        }

        // 登録者リスト取得
        $arrDataID   = array();
        $arrDataMail = array();

        $line_count = 0;  // 行数
        $fp = fopen($filename, 'r');
        if($fp !== false){
            while (!feof($fp)) {
                $line_count++;
                $arrCSV = fgetcsv($fp, 10000);

                if(empty($arrCSV)) continue;   // 空行はスキップ
                if($line_count == 1) continue; // ヘッダ行はスキップ

                array_push($arrDataID, $arrCSV[0]);
                array_push($arrDataMail, $arrCSV[1]);
            }
            fclose($fp);
        }
        if(empty($arrDataID)) return;


        // 簡易認証の外部テンプレート
        $includeDir = TEMPLATES_DIR."Usr/include/".$form_id."/";
        $tmpfile = $form_id."auth.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("auth_inc_flg", 1);
            $obj->assign("tpl_auth", $tmpfile);
        }

        // セッション内で1回認証していればスルー
        $auth_flg = $GLOBALS["session"]->getVar("auth");
        if($auth_flg == "1") return;

        // 入力値
        $mode = isset($_POST['mode']) ? $_POST['mode'] : '';
        $arrForm = array();
        $arrForm["auth_id"]   = isset($_REQUEST["auth_id"])   ? $_REQUEST["auth_id"] : "";
        $arrForm["auth_mail"] = isset($_REQUEST["auth_mail"]) ? $_REQUEST["auth_mail"] : "";

        if($mode == "login"){
            // 未入力
            if($arrForm["auth_id"] == "" || $arrForm["auth_mail"] == "") {
                $obj->assign("msg", "The registration ID/e-mail address combination you have entered cannot be found in our database.");

            // 認証開始
            }else{
                $key = array_search($arrForm["auth_id"], $arrDataID);
                if($key === false || $arrForm["auth_mail"] != $arrDataMail[$key]) {
                    $obj->assign("msg", "The registration ID/e-mail address combination you have entered cannot be found in our database.");

                // 認証成功
                }else{
                    // 同一RegistrationIDのチェック
                    $where = array();
                    array_push($where, 'form_id  = '.$obj->db->quote($form_id));
                    array_push($where, 'edata144 = '.$obj->db->quote($arrForm["auth_id"]));
                    array_push($where, 'del_flg  = 0');
                    array_push($where, 'invalid_flg = 0');
                    $exists = $obj->db->exists('entory_r', implode(' and ', $where), __FILE__, __LINE__);
                    // 重複登録
                    if($exists){
                        $obj->assign("msg", "Your application has been already received.");

                    // 初めての登録の方
                    }else{
                        $GLOBALS["session"]->setVar("auth", "1");
                        // RegistrationID, Emailを自動セット
                        $GLOBALS["session"]->setVar("auth_pas",  $arrForm);
                        $_REQUEST["mode"] = "";

                        $obj->getDispSession();
                        return;
                    }
                }
            }
        }

        $obj->arrForm = $arrForm;
        $obj->assign("form_id", $form_id);
        $obj->_processTemplate = "Usr/Usr_Basic.html";

        $obj->basemain();
        exit;
    }

    /* 簡易認証の入力値を引き継ぐ */
    function premain($obj){
        $auth = $GLOBALS["session"]->getVar("auth_pas");
        $obj->arrForm['edata144'] = $auth['auth_id'];
        $obj->arrForm['edata25']  = $auth['auth_mail'];

        // エラーメッセージをグループ化する
        $error_filter = array(
                            0 => array(  55,   56,   67) // Schedule最初の行 # Hotel Other
                           ,1 => array( 101,  102,  103) // Schedule滞在の行 # Hotel Other
                           ,2 => array( 999)             // Schedule滞在の行 # Activity
                           ,3 => array( 550,  560,  670) // Schedule最初の行 # Hotel
                           ,4 => array(1019, 1029, 1039) // Schedule滞在の行 # Hotel
                           ,5 => $obj->accompany         // 同伴者 # 必須項目
                        );

        if(!empty($obj->objErr->_err)){
            // グループ化したエラー配列
            $errorgroup = array();

            // エラーメッセージ原文
            $errorogirinal = array();

            foreach($error_filter as $_key => $_item_id){
                $errorgroup[$_key] = array();
                foreach($_item_id as $item_id){
                    $key = 'edata'.$item_id;
                    if(!isset($obj->objErr->_err[$key])) continue;

                    if(is_array($obj->objErr->_err[$key])){
                        foreach($obj->objErr->_err[$key] as $_day => $_error){
                            $errorgroup[$_key][$_day][] = $item_id;
                        }
                    }else{
                        array_push($errorgroup[$_key], $item_id);
                    }
                    $errorogirinal[$item_id] = $obj->objErr->_err[$key];
                    unset($obj->objErr->_err[$key]);
                }
            }
            $obj->assign("arrErr", $obj->objErr->_err);
            $obj->assign('errorgroup',    $errorgroup);
            $obj->assign('errorogirinal', $errorogirinal);
        }
    }



    // ------------------------------------------------------
    // ▽ページのカスタマイズ
    // ------------------------------------------------------

    /** プライバシーポリシー */
    function agreeAction($obj) {
        $form_id   = $obj->o_form->formData["form_id"];

        // 同一RegistrationIDのチェック
        $auth = $GLOBALS["session"]->getVar("auth_pas");
        $where = array();
        array_push($where, 'form_id  = '.$obj->db->quote($form_id));
        array_push($where, 'edata144 = '.$obj->db->quote($auth['auth_id']));
        array_push($where, 'del_flg  = 0');
        array_push($where, 'invalid_flg = 0');
        $exists = $obj->db->exists('entory_r', implode(' and ', $where), __FILE__, __LINE__);
        // 重複登録
        if($exists){
            $msg = "Your application has been already received.";
            $obj->objErr->addErr($msg, "agree");
            $obj->arrErr = $obj->objErr->_err;

            $obj->_processTemplate = "Usr/form/Usr_entry_agree.html";
            $obj->_title = $obj->formdata["form_name"]." Privacy Policy";

        // 初めての登録の方
        }else{
            return Usr_pageAction::agreeAction($obj);
        }
    }



    // ------------------------------------------------------
    // ▽エラーチェックのカスタマイズ
    // ------------------------------------------------------

    /* 
     * 会期期間内かチェック
     * @param  date $date Y-m-d
     * @return boolean true:会期期間内
     * 
     */
    function checkTerm99($date){
        $sdate = strtotime(date("2014-07-13 00:00:00"));
        $edate = strtotime(date("2014-07-19 23:59:59"));

        if($sdate <= $date && $date <= $edate){
            return true;
        }
        return false;
    }

    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        // Date of birthのエラーメッセージ
        $tmp = $obj->itemData[27]['strip_tags'];
        $obj->itemData[27]['strip_tags'] = "Date";
        Usr_Check::_check1($obj);
        $obj->itemData[27]['strip_tags'] = $tmp;

//        if(strlen($obj->admin_flg) > 0) return;

        $group_id = 1;

        // FaxNumber:エラーチェック
        if(Usr_init::isset_ex($obj, $group_id, 71) && Usr_init::isset_ex($obj, $group_id, 72)){
            // ラジオを選択したらテキスト必須
            $item_id = 72;
            $key = "edata".$item_id;
            if(strlen($obj->arrParam['edata71']) > 0 && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }

            // テキストを入力したらラジオ必須
            $item_id = 71;
            $key = "edata".$item_id;
            if(strlen($obj->arrParam['edata72']) > 0 && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }


        // カレンダーチェック
        $sdate = strtotime(date("2014-07-01"));
        $edate = strtotime(date("2014-07-31"));
        $c = 0; // カレンダーの数
        // Arrival Date in Japan
        $itemId1 = 73;
        $key1    = 'edata'.$itemId1;
        if(Usr_init::isset_ex($obj, $group_id, $itemId1)){
            $name = Usr_init::getItemInfo($obj, $itemId1);

            // d/m/Y -> Y/m/d
            $edata1 = $this->cnvDateFormat($obj->arrParam[$key1]);

            if(!$obj->objErr->isDate($edata1)){
                $obj->objErr->addErr(sprintf('%s that you have entered is not acceptable.', $name), $key1);
            }else{
                // 7月の日付かチェック
                $data = strtotime($edata1);
                if($sdate <= $data && $data <= $edate){
                    $c++; // 入力ok
                }else{
                    $obj->objErr->addErr(sprintf('%s that you have entered is not acceptable.', $name), $key1);
                }
            }
        }

        // Departure Date from Japan
        $itemId2 = 77;
        $key2    = 'edata'.$itemId2;
        if(Usr_init::isset_ex($obj, $group_id, $itemId2)){
            $name = Usr_init::getItemInfo($obj, $itemId2);

            // d/m/Y -> Y/m/d
            $edata2 = $this->cnvDateFormat($obj->arrParam[$key2]);

            if(!$obj->objErr->isDate($edata2)){
                $obj->objErr->addErr(sprintf('%s that you have entered is not acceptable.', $name), $key2);
            }else{
                // 7月の日付かチェック
                $data = strtotime($edata2);
                if($sdate <= $data && $data <= $edate){
                    $c++; // 入力ok
                }else{
                    $obj->objErr->addErr(sprintf('%s that you have entered is not acceptable.', $name), $key2);
                }
            }
        }

        // 日数チェック
        $stayed = 0; // 滞在日数
        $isStayed = true;
        if($c == 2){
            if(!isset($obj->objErr->_err[$key1]) && !isset($obj->objErr->_err[$key2])){
                // Arrival = Departure
                if(strtotime($edata1) == strtotime($edata2)){
                    $obj->objErr->addErr('Departure date should be later than the arrival date.', 'edata73');
                    $isStayed = false;

                // Arrival > Departure
                }elseif(!$obj->objErr->isDateAgo($edata1, $edata2)){
                    $name1 = Usr_init::getItemInfo($obj, $itemId1);
                    $name2 = Usr_init::getItemInfo($obj, $itemId2);
                    $obj->objErr->addErr('Departure date should be later than the arrival date.', $key1);
                    $isStayed = false;

                // Arrival < Departure
                }else{
                    // 滞在日数の計算
                    $datetime1 = new DateTime($edata1);
                    $datetime2 = new DateTime($edata2);
                    $interval = $datetime1->diff($datetime2);
                    /**
                     * 7/1 - 7/4 の差分が 3日で3泊4日,滞在日数は2日
                     * $stayedの値は2日なるように調整する
                     */
                    $stayed = intval($interval->days)-1;
                    // 滞在可能日数が範囲外
//                    if(!(2 <= ($stayed+2) )){
//                        $obj->objErr->addErr('Departure date should be more than 1 day after the arrival date.', 'edata73');
//                        $stayed = 0;
//                    }
                    // 代行登録はエラーチェックを行わない
                    if(!(($stayed+2) <= $obj->stay) && $obj->admin_flg == ""){
                        $obj->objErr->addErr('Period of stay can be 11 days at the maximum.', 'edata73');
                        $stayed = 0;
                        $isStayed = false;
                    }
                }
            }
        }
        // 滞在期間チェック
        // 会期期間外の滞在はエラー
        if($isStayed){
            $date1 = strtotime('2014-07-13 00:00:00');
            $date2 = strtotime('2014-07-19 23:59:59');

            $arrival   = strtotime($edata1);
            $departure = strtotime($edata2);

            $term1 = ($date1 <= $arrival   && $arrival   <= $date2);
            $term2 = ($date1 <= $departure && $departure <= $date2);

            // 会期期間内の滞在でない場合
            if(!($arrival <= $date1 && $date2 <= $departure)){
                if(!$term1 && !$term2){
                    // Arrivalが20日以降
                    if($date2 < $arrival){
                        $obj->objErr->addErr('Arrival date cannot be later than July 19.', 'edata73');

                    // Depatureが13日より前
                    }elseif($departure < $date1){
                        $obj->objErr->addErr('Departure date cannot be earlier than July 13.', 'edata73');

                    }else{
                        $obj->objErr->addErr('The term of WCS2014 is not selected.', 'edata73');
                    }
                    $stayed = 0;
                }
            }
        }



        // Country：Chinaチェック
        $item_id = 114;
        $key     = 'edata'.$item_id;
        $isChina = false; // Country:chinaフラグ
        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
            if(!isset($obj->objErr->_err[$key])){
                $data = $obj->arrParam[$key];
                $isChina = (strtolower($GLOBALS['country'][$data]) == 'china');

                // china以外は選択不可
                if(!$isChina){
                    // etc13 # Departure Airport in Your Own Country
                    $item_id = 76;
                    $key     = 'edata'.$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                        if($obj->objErr->isNull($obj->arrParam[$key])){
                            $name = Usr_init::getItemInfo($obj, $item_id);
                            $obj->objErr->addErr('Please do not enter the Departure Airport unless you are a resident of China.', $key);
                        }
                    }

                    // etc16 # Arrival Airport in Your Own Country
                    $item_id = 79;
                    $key     = 'edata'.$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                        if($obj->objErr->isNull($obj->arrParam[$key])){
                            $name = Usr_init::getItemInfo($obj, $item_id);
                            $obj->objErr->addErr('Please do not enter the Arrival Airport unless you are a resident of China.', $key);
                        }
                    }

                // chinaは必須
                }else{
                    // etc13 # Departure Airport in Your Own Country
                    $item_id = 76;
                    $key     = 'edata'.$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                        if(!$obj->objErr->isNull($obj->arrParam[$key])){
                            $name = Usr_init::getItemInfo($obj, $item_id);
                            $method = Usr_init::getItemErrMsg($obj, $item_id);
                            $method = str_replace("one(s)", "one", $method);
                            $obj->objErr->addErr(sprintf($method, $name), $key);
                        }
                    }

                    // etc16 # Arrival Airport in Your Own Country
                    $item_id = 79;
                    $key     = 'edata'.$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                        if(!$obj->objErr->isNull($obj->arrParam[$key])){
                            $name = Usr_init::getItemInfo($obj, $item_id);
                            $method = Usr_init::getItemErrMsg($obj, $item_id);
                            $method = str_replace("one(s)", "one", $method);
                            $obj->objErr->addErr(sprintf($method, $name), $key);
                        }
                    }
                }
            }
        }

        $obj->arrParam['edata142'] = (int)$isChina; // Country:chinaフラグ
        $obj->arrParam['edata143'] = $stayed;       // セッションに滞在日数をセット
        $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $obj->arrParam);
        $obj->getDispSession();
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check3($obj){
        Usr_Check::_check3($obj);

//        if(strlen($obj->admin_flg) > 0) return;

        $group_id = 3;

        // Scheduleデフォルト値
        $edata104 = NULL;    // 最初の行：Address
        $edata105 = NULL;    // 最初の行：Phone
        $edata106 = array(); // 滞在の行：Address
        $edata107 = array(); // 滞在の行：Phone

        $item_id = 99;
        $key     = 'edata'.$item_id;
//        if(!isset($obj->objErr->_err[$key])){
        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
            $str = $obj->arrParam[$key];
            if(in_array($item_id, $obj->formarray)){
                foreach($obj->arrParam[$key] as $_key => $_val){
                    // Ymd -> Y-m-d
                    $y = substr($_key, 0, 4);
                    $m = substr($_key, 4, 2);
                    $d = substr($_key, 6);
                    $date = date("Y-m-d", mktime(0, 0, 0, $m, $d, $y));
                    // 会期期間外の入力チェック
                    if(!$this->checkTerm99(strtotime($date))){
                        $str = $_val;
                        $str = strtolower(trim(preg_replace('/[\s　]+/u', '', $str)));
                        if(preg_match('/attendwcs2014/u', $str, $match) === 1){
                            $_day = date("Ymd", mktime(0, 0, 0, $m, $d, $y));
                            $obj->objErr->_err['edata999'][$_day] = 'WCS2014 is not held on this date.';
//                            $obj->objErr->addErr('WCS2014 is not held on this date.', 'edata99');
                        }
                    }
                }
            }
        }

        // Schedule最初の行でOtherを選択した場合は関連項目を必須
        if(Usr_Check::chkSelect($obj, 3, 54, 'other')){
            $keys    = array(55, 56, 67);
            foreach($keys as $item_id){
                $key     = 'edata'.$item_id;
                if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                    if(!$obj->objErr->isNull($obj->arrParam[$key])){
                        $name = Usr_init::getItemInfo($obj, $item_id);
                        $method = Usr_init::getItemErrMsg($obj, $item_id);
                        $obj->objErr->addErr('Enter Hotel Other.', $key);
                    }
                }
            }
        // Other以外を選択した場合は自動的にAddress,Hotelをセット
        }else{
            $val = $obj->arrParam['edata54'];
            $edata104 = $val;
            $edata105 = $val;

            // Otherの関連項目を入力していたらエラー
            $keys    = array(55, 56, 67);
            foreach($keys as $item_id){
                $key     = 'edata'.$item_id;
                if(Usr_init::isset_ex($obj, $group_id, $item_id)){
                    if($obj->objErr->isNull($obj->arrParam[$key])){
                        $obj->objErr->addErr('NOT enter other hotel information below.', $key.'0');
                    }
                }
            }
        }


        // Schedule滞在日数の行でOtherを選択した場合は関連項目を必須
        $item_id = 100;
        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
            $target = "other";
            $key = 'edata'.$item_id;
            // other関連項目
            $keys = array(101, 102, 103);

            $val = array_search($target, $obj->arrItemData[$group_id][$item_id]['select']);
            if($val === false) continue;

            foreach($obj->arrParam[$key] as $_day => $_val){
                if($obj->arrItemData[$group_id][$item_id]['item_type'] != Usr_init::INPUT_SELECT) continue;

                // other以外を選択した場合は自動的にAddress,Hotelをセット
                if($_val != $val){
                    $edata106[$_day] = $_val;
                    $edata107[$_day] = $_val;

                    // otherを選択したら関連項目を必須
                    foreach($keys as $_item_id){
                        $_key = 'edata'.$_item_id;
                        if(Usr_init::isset_ex($obj, $group_id, $_item_id)){
                            if($obj->objErr->isNull($obj->arrParam[$_key][$_day])){
                                $obj->objErr->_err[$_key.'9'][$_day] = 'NOT enter other hotel information below.';
                            }
                        }
                    }
                    continue;
                }

                // otherを選択したら関連項目を必須
                foreach($keys as $_item_id){
                    $_key = 'edata'.$_item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $_item_id)){
                        if(!$obj->objErr->isNull($obj->arrParam[$_key][$_day])){
                            $name = Usr_init::getItemInfo($obj, $_item_id);
                            $method = Usr_init::getItemErrMsg($obj, $_item_id);
                            $obj->objErr->_err[$_key][$_day] = sprintf($method, $name);
                        }
                    }
                }
            }
        }

        // Hotel Address, Phoneを自動セット
        $obj->arrParam['edata104'] = $edata104;
        $obj->arrParam['edata105'] = $edata105;
        $obj->arrParam['edata106'] = $edata106;
        $obj->arrParam['edata107'] = $edata107;
        $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $obj->arrParam);
        $obj->getDispSession();

        // 滞在日数の行が0日 # 13日に到着して14日に出発
        if($obj->arrForm['edata143'] == 0){
            // 滞在日数の行で発生したエラーを無視
            $arrstay = array(99, 100, 101, 102, 103);
            foreach($arrstay as $_key => $item_id){
                $key = 'edata'.$item_id;
                if(!isset($obj->objErr->_err[$key])) continue;

                unset($obj->objErr->_err[$key]);
            }
        }


        // 同伴者
        if(!(strlen($obj->person) > 0)) return;

        // 入力している項目を取得
        $arrForm = array();
        for($i=0; $i<$obj->person; $i++){
            foreach($obj->accompany as $_key => $item_id){
                $key = 'edata'.$item_id;
                if(!Usr_init::isset_ex($obj, $group_id, $item_id)) continue;
                if(!$obj->objErr->isNull($obj->arrParam[$key][$i])) continue;

                $arrForm[$i][$key] = $obj->arrParam[$key][$i];
            }
        }

        // 必須チェック
        // Date of birth / Month / Year. → Date
        $obj->itemData[145]['item_name'] = "Date";
        for($i=0; $i<$obj->person; $i++){
            if(!isset($arrForm[$i])) continue;

            foreach($obj->accompany as $_key => $item_id){
                $key = 'edata'.$item_id;
                if(!Usr_init::isset_ex($obj, $group_id, $item_id)) continue;

                if(!$obj->objErr->isNull($obj->arrParam[$key][$i])){
                    $name = Usr_init::getItemInfo($obj, $item_id);
                    // 個別表示用
                    $method = Usr_init::getItemErrMsg($obj, $item_id);
                    $obj->objErr->_err[$key][$i] = sprintf($method, $name);

//                    // 上部表示用
//                    $method = "Accompanying Person ".($i+1)." ".Usr_init::getItemErrMsg($obj, $item_id);
//                    $obj->objErr->_err[$key.$i] = sprintf($method, $name);
                }
            }
        }

        // 同伴者数カウント
        if(count($obj->objErr->_err) == 0){
            $obj->arrParam['edata156'] = count($arrForm);
            $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $obj->arrParam);
            $obj->getDispSession();
        }
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /** 
     * 登録完了メールの送信
     */
    function sendRegistMail($obj, $user_id, $passwd) {
        // 3Dセキュアの場合は稀にセッションがクリアされるので取り直す
        // おまじないで全パターンも取り直すように変更
        $obj->getEntry();
        // passwordを最新の状態から取り出す
        $ss_user = $GLOBALS["session"]->getVar("SS_USER");
        $passwd  = $ss_user["e_user_passwd"];

        //タイトル
        $obj->subject = ($obj->formdata["lang"] == LANG_JPN) ? "エントリー完了通知メール" : "Notice: Your registration has been completed";
        if($obj->formWord["word11"] != ""){
            $obj->subject = $obj->formWord["word11"];
        }

        $id = str_replace($obj->formdata["head"]."-", "", $user_id);
        $obj->subject = preg_replace('/'.preg_quote('_ID_').'/u', (string)$id, $obj->subject);

        // メール送信
        $obj->sendCompleteMail($user_id, $passwd, 1);
    }


    function makeBodyGroup1($obj, &$arrbody){
        $tmp = $obj->formdata["group1"];
        $obj->formdata["group1"] = 'Visa Applicant Information';
        Usr_mail::makeBodyGroup1($obj, $arrbody);
        $obj->formdata["group1"] = $tmp;
    }


    function mailfunc28($obj, $key, $name) { return ''; }   // etc3 # Month
    function mailfunc63($obj, $key, $name) { return ''; }   // etc4 # Year
    function mailfunc70($obj, $key, $name) { return ''; }   // etc7 # Tel
    function mailfunc72($obj, $key, $name) { return ''; }   // etc9 # Fax

    // etc27 # Date
    function mailfunc27($obj, $key, $name, $i=NULL) {
        $group = 1;
        $str = $obj->point_mark.$name.": ";

        // Date
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= 'Date '.trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]).'  ';
                }
                break;
        }

        // Month
        $str .= ' ';
        $key = 28;
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= 'Month '.trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]).'  ';
                }
                break;
        }

        // Year
        $str .= ' ';
        $key = 63;
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= 'Year '.trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]);
                }
                break;
        }

        $str.= "\n";
        return $str;
    }


    // etc6 # Tel
    function mailfunc69($obj, $key, $name, $i=null) {
        $group = 1;
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "2":
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]);

                    // Tel Number
                    $str .= ' ';
                    $key = 70;
                    $wk_key = "edata".$key.$i;
                    if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
                    switch($obj->arrItemData[$group][$key]["item_type"]) {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                            break;
                        default:
                            // テキストボックス
                            $str .= $obj->arrForm[$wk_key];
                            break;
                    }

                }
                break;
        }
        $str.= "\n";
        return $str;
    }


    // etc8 # Fax
    function mailfunc71($obj, $key, $name, $i=null) {
        $group = 1;
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "2":
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]);

                    // Fax Number
                    $str .= ' ';
                    $key = 72;
                    $wk_key = "edata".$key.$i;
                    if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
                    switch($obj->arrItemData[$group][$key]["item_type"]) {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                            break;
                        default:
                            // テキストボックス
                            $str .= $obj->arrForm[$wk_key];
                            break;
                    }

                }
                break;
        }

        $str.= "\n";
        return $str;
    }


    // etc10 # Arrival date in japan
    function mailfunc73($obj, $key, $name, $i=null) {
        $group = 1;
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str  = "\n[Arrival Flight Information]\n\n";
        $str .= $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
                
                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->arrItemData[$group][$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }

        $str.= "\n";
        return $str;
    }


    // etc14 # Departure date in japan
    function mailfunc77($obj, $key, $name, $i=null) {
        $group = 1;
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str  = "\n[Departure Flight Information]\n\n";
        $str .= $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
                
                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->arrItemData[$group][$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }

        $str.= "\n";
        return $str;
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    private $maxStay = 0;
    function entry_csv_getData_after($obj, $list, $where, $orderby){
        $max = $obj->db->getData('max(edata143) as stayed', 'entory_r', $where);
        if(!$max) $stayed = 0;
        $stayed = intval($max['stayed']);
        $this->maxStay = $stayed+2; // Arrival + stay + Departure

        return array(true, $list);
    }

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;

        // Date of birth # month, year
        $obj->arrItemData[$group][28]['item_view'] = 1;
        $obj->arrItemData[$group][63]['item_view'] = 1;
//        $obj->arrItemData[$group][142]['item_view'] = 1; // isChina
//        $obj->arrItemData[$group][143]['item_view'] = 1; // stayed

        $groupHeader[$group][] = "entry No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }

    /** CSV出力データ グループ1生成 */
    function csvfunc27($obj, $group, $pa_param, $item_id){
        $d = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]); $item_id = 28;
        $m = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]); $item_id = 63;
        $y = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return date("Y/m/d", mktime(0, 0, 0, $m, $d, $y));
    }
    /** CSV出力データ グループ1生成 # Arrival date in Japan */
    function csvfunc73($obj, $group, $pa_param, $item_id){
        $date = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return date_format(date_create($this->cnvDateFormat($date)), 'Y/m/d');
    }
    /** CSV出力データ グループ1生成 Departure date in Japan */
    function csvfunc77($obj, $group, $pa_param, $item_id){
        $date = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id]);
        return date_format(date_create($this->cnvDateFormat($date)), 'Y/m/d');
    }
    /** CSV出力データ グループ3生成 */
    function csvfunc145($obj, $group, $pa_param, $item_id, $i){
        if(strlen($pa_param['edata'.$item_id][$i]) == 0) return '';

        $d = Usr_Assign::nini($obj, $group, $item_id, $pa_param['edata'.$item_id][$i]); $item_id = 146;
        $m = Usr_Assign::nini($obj, $group, $item_id, $pa_param['edata'.$item_id][$i]); $item_id = 147;
        $y = Usr_Assign::nini($obj, $group, $item_id, $pa_param['edata'.$item_id][$i]);
        return date("Y/m/d", mktime(0, 0, 0, $m, $d, $y));
    }

    /** CSVヘッダ-グループ3生成 */
    function entry_csv_entryMakeHeader3($obj, $all_flg=false){
        $group = 3;

        // Scheduleは手動でデータを生成
        for($i=1; $i<=$obj->stay; $i++){
//            $d = ($i == 1) ? '到着日' : "{$i}日目";
            $d = "Day{$i}";
            $groupHeader[$group][] = "\"{$d}:month day\"";
            $groupHeader[$group][] = "\"{$d}:Activity\"";
            $groupHeader[$group][] = "\"{$d}:Hotel Name\"";
            $groupHeader[$group][] = "\"{$d}:Hotel Address\"";
            $groupHeader[$group][] = "\"{$d}:Hotel Tel\"";
        }

        // 同伴者
        $max = $obj->db->_getOne("max(cast(edata156 as integer))", "entory_r", "form_id = 16 and del_flg = 0 and edata156 <> '' and edata156 is not null", __FILE__, __LINE__);
        if(!$max) $obj->person = 2;
        if($obj->person < $max) $obj->person = $max;

        if((strlen($obj->person) > 0)){
            // Date of birth # month,yearは非表示
            $obj->arrItemData[$group][146]["item_view"] = 1;
            $obj->arrItemData[$group][147]["item_view"] = 1;

            for($i=1; $i<=$obj->person; $i++){
                foreach($obj->accompany2 as $_key => $item_id){
                    $_data = $obj->arrItemData[$group][$item_id];

                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = "AccompanyingPersion{$i} ".strip_tags($_data["item_name"]);

                    $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
                }
            }
        }

        return $groupHeader[$group];
    }

    /** CSV出力データ グループ3生成 */
    function entry_csv_entryMakeData3($obj, $pa_param, $all_flg=false){
        include_once(MODULE_DIR.'entry_ex/Usr_check.class.php');
        include_once(MODULE_DIR.'entry_ex/Usr_init.class.php');

        // フォーム項目を配列とする特殊項目
        if(!empty($obj->formarray)){
            foreach($obj->formarray as $_key => $item_id){
                $key = 'edata'.$item_id;
                $pa_param[$key] = unserialize($pa_param[$key]);
            }
        }

        $group = 3;
        $groupBody = array();

        //---------------------------
        // Schedule
        //---------------------------
        $arrForm = $obj->arrForm;
        $obj->arrForm = $pa_param;
        $arrData = $this->makeSchedule($obj);
        $obj->arrForm = $arrForm;

        // Scheduleは手動でデータを生成
        for($i=1; $i<=$obj->stay; $i++){
            if(!isset($arrData[$i])){
                $arrData[$i] = new stdClass();
                $arrData[$i]->day  = NULL;
                $arrData[$i]->act  = NULL;
                $arrData[$i]->name = NULL;
                $arrData[$i]->addr = NULL;
                $arrData[$i]->tel  = NULL;
            }
            $data = $arrData[$i];
            $data->act = preg_replace('/\r\n|\r|\n+/u', ' ', $data->act);
            if(!is_null($data->day)) $data->day = date_format(date_create($this->cnvDateFormat($data->day)), 'd F');

            /* 月日     */ $groupBody[$group][] = sprintf("\"%s\"", $data->day);
            /* Activity */ $groupBody[$group][] = sprintf("\"%s\"", $data->act);
            /* Name     */ $groupBody[$group][] = sprintf("\"%s\"", $data->name);
            /* Address  */ $groupBody[$group][] = sprintf("\"%s\"", $data->addr);
            /* Tel      */ $groupBody[$group][] = sprintf("\"%s\"", $data->tel);
        }

        if((strlen($obj->person) > 0)){
            for($i=0; $i<$obj->person; $i++){
                foreach($obj->accompany2 as $_key => $item_id){
                    $_data = $obj->arrItemData[$group][$item_id];

                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    if(!isset($pa_param['edata'.$item_id][$i])) $pa_param['edata'.$item_id][$i] = "";
                    if($item_id == 145){
                        $str = $this->csvfunc145($obj, $group, $pa_param, $item_id, $i);
                    }else{
                        $str = Usr_Assign::nini($obj, $group, $item_id, $pa_param['edata'.$item_id][$i]);
                    }
                    $groupBody[$group][] = $obj->fix.$str.$obj->fix;
                }
            }
        }

        return $groupBody[$group];
    }


    //--------------------------------------
    // ▽MngIndexカスタマイズ
    //--------------------------------------

    function Mng_index_prefAction($obj){
        $key = 'form16_list';
        if($_FILES[$key]["size"] == 0){
            $obj->arrErr[$key] = '登録者リストCSVを指定してください。';
            return;
        }

        if(!Validate::checkExt($_FILES[$key], array('csv'))){
            $obj->arrErr[$key] = '登録者リストは「CSV」を指定してください。';
            return;
        }

        if($_FILES[$key]["error"] > 0){
            $obj->arrErr[$key] = "ファイルのアップロードに失敗しました。";
            return;
        }


        $customDir = ROOT_DIR."customDir/".$obj->form_id."/";
        $filename  = $customDir.'auth/entry_list.csv';

        $rs = @move_uploaded_file($_FILES[$key]["tmp_name"], $filename);
        if(!$rs){
            $obj->arrErr[$key] = "ファイルのアップロードに失敗しました。";
            return;
        }

        $obj->onload = "window.alert('登録者リストを更新しました。');";
        return;
    }



    //---------------------------------------
    // ▽Mng 詳細カスタマイズ
    //---------------------------------------
    function __constructMngDetail($obj){
        $obj->_processTemplate = "Mng/entry/include/{$obj->form_id}/{$obj->form_id}Mng_entry_detail.html";
        $obj->assign("eid", $obj->eid);
    }


    // Mng 詳細 # 書類生成
    function Mng_detail_phpwordAction($obj, $isdownload=true){
        include_once(MODULE_DIR.'entry_ex/Mng_word.class.php');

        $type = isset($_POST['type']) ? intval($_POST['type']) : '';
        if(!Validate::isNumeric($type)){
            $obj->onload = "alert('処理モードの取得に失敗');";
            return;
        }

        // 登録No.の「No.」
        $id = substr($obj->arrData['e_user_id'], -5);

        // Tempalte
        $customDir = ROOT_DIR."customDir/".$obj->form_id."/word/";
        // Save
        $uploadDir = UPLOAD_PATH."Usr/form".$obj->form_id."/".$obj->eid."/word/";

        // 1:Cover Letter
        // 2:招へい理由書
        // 3:身元保証書
        // 4:日程表
        // 5:申請人名簿
        $template  = sprintf('template%s.docx', $type);
        switch($type){
            case 1: $savefile = sprintf('%s_%s_letter.docx',  $id, $type); break;
            case 2: $savefile = sprintf('%s_%s_shouhei.docx', $id, $type); break;
            case 3: $savefile = sprintf('%s_%s_mimoto.docx',  $id, $type); break;
            case 4: $savefile = sprintf('%s_%s_schedule.docx',$id, $type); break;
            case 5: $savefile = sprintf('%s_%s_meibo.docx',   $id, $type); break;
        }

        // Word生成
        $objWord = new Mng_Word();
        $objWord->setTemplate($template, $customDir)
                ->setSavefile($savefile, $uploadDir)
                ->setData($obj->arrData)
                ->createWord($obj);

        $wordfile = $uploadDir.$savefile;
        if(file_exists($wordfile) && $isdownload){
            $objDL = new Download();
            $objDL->file($wordfile, $savefile);
            exit;
        }

        // 生成エラー
        $obj->onload = "alert('書類の生成に失敗しました。');";
    }


    function wordfunc142($obj){ return '111'; }
    function wordfunc143($obj){ return '222'; }

    // FirstName
    function wordfunc2($obj){
        if(strlen($obj->arrForm["edata2"]) > 0) return $obj->arrForm["edata2"]." ";
        return "";
    }
    // MiddleName
    function wordfunc7($obj){
        if(strlen($obj->arrForm["edata7"]) > 0) return $obj->arrForm["edata7"]." ";
        return "";
    }
    // Date of birth # month
    function wordfunc28($obj){
        if(strlen($obj->arrForm["edata28"]) == 0) return "";
        $m = Usr_Assign::nini($obj, 1, 28, $obj->arrForm['edata28']);
        $d = Usr_Assign::nini($obj, 1, 27, $obj->arrForm['edata27']);
        $y = Usr_Assign::nini($obj, 1, 63, $obj->arrForm['edata63']);
        $date = $d."/".$m."/".$y;
        return date_format(date_create($this->cnvDateFormat($date)), 'F');
    }
    // 同伴者数
    function wordfunc156($obj){
        if(empty($obj->arrForm["edata156"])) return "";

        return $obj->arrForm["edata156"]." additional applicants";
    }

    function presaveword($obj, &$document){
        // 年齢
        $m = Usr_Assign::nini($obj, 1, 28, $obj->arrForm['edata28']);
        $d = Usr_Assign::nini($obj, 1, 27, $obj->arrForm['edata27']);
        $y = Usr_Assign::nini($obj, 1, 63, $obj->arrForm['edata63']);
        $date = $y.sprintf("%02d", intval($m)).sprintf("%02d", intval($d));
        $age = (int) ((date('Ymd')-intval($date))/10000);

        $document->setValue("age", $age);
    }

    // Schedule
    function makeDocumentGroup3(Mng_Word $objWord, $obj, &$document){
        // 滞在日数
        $stay = intval($obj->arrForm['edata143'])+2; // 滞在日数の行 + (到着,出発)

        $matrix   = array("row" => $stay, "col" => 4);
        $arrwidth = array(990, 3060, 2400, 3400);
        $arralign = array('center', 'left', 'left', 'left');
        $margin   = 140;

        $insertbody = '</w:tbl><w:p><w:r><w:br w:type="page"/></w:r></w:p><w:tbl>[THEAD]';
        $table    = $objWord->createTable($matrix, $arrwidth, $arralign, $margin, 6, $insertbody);

        //---------------------------
        // Schedule
        //---------------------------
        $arrData = $this->makeSchedule($obj);
        $contact = "Visa support Office of<w:br/>WCS2014<w:br/>TEL: 03-3219-3600";

        // Scheduleは手動でデータを生成
        for($i=1; $i<=$stay; $i++){
            if(!isset($arrData[$i])){
                $arrData[$i] = new stdClass();
                $arrData[$i]->day  = NULL;
                $arrData[$i]->act  = NULL;
                $arrData[$i]->name = NULL;
                $arrData[$i]->addr = NULL;
                $arrData[$i]->tel  = NULL;
            }
            $data = $arrData[$i];
            if(!is_null($data->day)) $data->day = date_format(date_create($this->cnvDateFormat($data->day)), 'd F');

            // 連続した改行は1つ
            $arrData[$i]->act  = htmlspecialchars($arrData[$i]->act);
            $arrData[$i]->act  = str_replace(array("\r\n", "\r", "\n"), "\n", $arrData[$i]->act);
            $arrData[$i]->act  = preg_replace('/(\n)+/us', '$1', $arrData[$i]->act);
            $arrData[$i]->act  = str_replace("\n", "<w:br/>", $arrData[$i]->act);
            $arrData[$i]->name = htmlspecialchars($arrData[$i]->name);
            $arrData[$i]->addr = htmlspecialchars($arrData[$i]->addr);
            $arrData[$i]->tel  = htmlspecialchars($arrData[$i]->tel);
        }

        extract($matrix);

        //ヘッダ
        $header = array('Date', 'Activity Plan' ,'Contact' ,'Accommodation');
        for($j=0; $j<$col; $j++){
            $table = str_replace("_HEAD_{$j}_", $header[$j], $table);
        }
        // データ
        for($i=0; $i<$row; $i++){
            $hotel = $arrData[$i+1]->name."<w:br/>".$arrData[$i+1]->addr."<w:br/>".$arrData[$i+1]->tel;
            /* 月日     */ $table = str_replace("_CELL_{$i}_0_", $arrData[$i+1]->day, $table);
            /* Activity */ $table = str_replace("_CELL_{$i}_1_", $arrData[$i+1]->act, $table);
            /* Contact  */ $table = str_replace("_CELL_{$i}_2_", $contact,            $table);
            /* Hotel    */ $table = str_replace("_CELL_{$i}_3_", $hotel  ,            $table);
        }

        $document->setValue("table", $table);


        // 同伴者
        $this->makeDocumentAccompanyingPersion($obj, $document);

        // 特殊項目はスキップ
        foreach($obj->formarray as $_key => $item_id){
            $obj->arrItemData[3][$item_id]['disp'] = 1;
        }
        $objWord->makeDocumentGroupBase($obj, $document, 3);
    }


    function makeSchedule($obj){
        require_once(MODULE_DIR.'entry_ex/Usr_check.class.php');
        require_once(MODULE_DIR.'entry_ex/Usr_assign.class.php');

        $group   = 3;
        $arrData = array();
 
        // 到着日
        $i = 1;
        $item_id = 54;
        $key     = 'edata'.$item_id;
        $obj->arrParam[$key] = $obj->arrForm[$key];
        if(Usr_Check::chkSelect($obj, $group, $item_id, 'other')){
            $item_id = 55;
            $name = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id]); $item_id = 56;
            $addr = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id]); $item_id = 67;
            $tel  = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id]);
        // other以外を選択
        }else{
            $item_id = 54;
            $name = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id]); $item_id = 104;
            $addr = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id]); $item_id = 105;
            $tel  = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id]);
        }
        $arrData[$i] = new stdClass();
        $arrData[$i]->day  = $obj->arrForm['edata73'];
        $arrData[$i]->act  = $obj->arrForm['edata157'];
        $arrData[$i]->name = $name;
        $arrData[$i]->addr = $addr;
        $arrData[$i]->tel  = $tel;

        // 滞在日数
        if(is_array($obj->arrForm['edata158']) && count($obj->arrForm['edata158']) > 0){
            foreach($obj->arrForm['edata158'] as $_key => $_day){
                $day = date_format(date_create($this->cnvDateFormat($_day)), 'Ymd');

                $i++;
                $arrData[$i] = new stdClass();
                $arrData[$i]->day  = $_day;
                $arrData[$i]->act  = $obj->arrForm['edata99'][$day];

                $item_id = 100;
                $key     = 'edata'.$item_id;
                $obj->arrParam[$key] = $obj->arrForm[$key][$day];
                if(Usr_Check::chkSelect($obj, $group, $item_id, 'other')){
                    $name = $obj->arrForm["edata101"][$day];
                    $addr = $obj->arrForm["edata102"][$day];
                    $tel  = $obj->arrForm["edata103"][$day];
                // other以外を選択
                }else{
                    $item_id = 100;
                    $name = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id][$day]); $item_id = 106;
                    $addr = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id][$day]); $item_id = 107;
                    $tel  = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm['edata'.$item_id][$day]);
                }

                $arrData[$i]->name = $name;
                $arrData[$i]->addr = $addr;
                $arrData[$i]->tel  = $tel;
            }
        }

        // 到着日
        $i++;
        $arrData[$i] = new stdClass();
        $arrData[$i]->day  = $obj->arrForm['edata77'];
        $arrData[$i]->act  = $obj->arrForm['edata159'];
        $arrData[$i]->name = NULL;
        $arrData[$i]->addr = NULL;
        $arrData[$i]->tel  = NULL;

        return $arrData;
    }


    function makeDocumentAccompanyingPersion($obj, &$document){
        // 1人あたりの同伴者のフォーマット
//        $space  = '<w:pPr><w:spacing w:before="1.3pt" w:after="0" /></w:pPr>';
//        $font   = '<w:rPr><w:rFonts w:ascii="Century" w:hAnsi="Century" w:cs="Century"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="ja-JP"/></w:rPr>';
//        $indent = '<w:pPr><w:tabs><w:tab w:val="left" w:pos="440"/></w:tabs><w:spacing w:before="26" w:after="0" w:line="253" w:lineRule="exact"/><w:ind w:left="100" w:right="-20"/></w:pPr>';
//
//        $msg = '<w:p>_SP_</w:p>';
//        $msg.= '<w:p>_SP__INDENT_<w:r>_FONT_<w:t>${i_INDEX_}</w:t></w:r><w:r>_FONT_<w:tab/><w:t>${n_INDEX_}</w:t></w:r><w:r>_FONT_<w:tab/><w:t>${edata148_INDEX_}</w:t></w:r></w:p>';
//        $msg.= '<w:p>_SP__INDENT_<w:r>_FONT_<w:tab/><w:t>${o_INDEX_}</w:t></w:r><w:r>_FONT_<w:tab/><w:t>${edata149_INDEX_}</w:t></w:r></w:p>';
//        $msg.= '<w:p>_SP_</w:p>';
//        $msg.= '<w:p>_SP__INDENT_<w:r>_FONT_<w:tab/><w:t>${f_INDEX_}</w:t></w:r><w:r>_FONT_<w:tab/><w:t>${edata111_INDEX_}${edata112_INDEX_}${edata110_INDEX_}</w:t></w:r><w:r><w:tab/><w:tab/></w:r><w:r>_FONT_<w:t>${edata113_INDEX_}</w:t></w:r></w:p>';
//        $msg.= '<w:p>_SP__INDENT_<w:r>_FONT_<w:tab/><w:t>${d_INDEX_}</w:t></w:r><w:r>_FONT_<w:tab/><w:t>${edata146_INDEX_} ${edata145_INDEX_}, ${edata147_INDEX_}</w:t></w:r><w:r>_FONT_<w:tab/><w:t>${age_INDEX_}</w:t></w:r></w:p>';
//        $msg.= '<w:p>_SP_</w:p>';
//        $msg.= '<w:p>_SP__INDENT_<w:r>_FONT_<w:tab/><w:t>${r_INDEX_}</w:t></w:r></w:p>';
//        $msg.= '<w:p>_SP__INDENT_<w:r>_FONT_<w:tab/><w:t>${c_INDEX_}</w:t></w:r></w:p>';
//        $msg.= '<w:p>_SP_</w:p>';
//        $msg = str_replace("_SP_",     $space, $msg);
//        $msg = str_replace("_FONT_",   $font,  $msg);
//        $msg = str_replace("_INDENT_", $indent,$msg);

        $tab   = '<w:r><w:tab/></w:r>';
        $font  = '<w:rFonts w:ascii="Century" w:hAnsi="Century" w:cs="Century"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="ja-JP"/>';
        $empty = '<w:tr><w:tc><w:tcPr><w:gridSpan w:val="2"/></w:tcPr><w:p></w:p></w:tc></w:tr>';
        $br_t = '<w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>';
        $br_l = '<w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>';
        $br_b = '<w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>';
        $br_r = '<w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>';
        $br_h = '<w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>';
        $br_v = '<w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>';
        $border= $br_t.$br_l.$br_b.$br_r.$br_h.$br_v;

        $msg = file_get_contents(ROOT_DIR."customDir/".$obj->form_id."/word/template5-1.txt");
        $msg = str_replace("_FONT_",   $font,  $msg);
        $msg = str_replace("_TAB_",    $tab,   $msg);
        $msg = str_replace("_EMPTY_",  $empty, $msg);
        $msg = str_replace("_BORDER_", $border,$msg);


        // 同伴者なし
        if(empty($obj->arrForm["edata156"])){
            $document->setValue("accompanying", '');
            return;
        }

        $accompanyingPersons = array();

        // 同伴者数
        $max_persons = intval($obj->arrForm["edata156"]);

        // 同伴者の枠を生成
        $accompanying = "";
        for($i=0; $i<$max_persons; $i++){
            $accompanying .= str_replace("_INDEX_", $i, $msg);
        }
        $document->setValue("accompanying", $accompanying);



        // 同伴者情報を埋め込み
        for($i=0; $i<$max_persons; $i++){
            // MiddleName
            $middleName = strlen($obj->arrForm['edata112'][$i]) == 0 ? '' : $obj->arrForm['edata112'][$i]." ";
            // Date of birth
            $m = Usr_Assign::nini($obj, 3, 146, $obj->arrForm['edata146'][$i]);
            $d = Usr_Assign::nini($obj, 3, 145, $obj->arrForm['edata145'][$i]);
            $y = Usr_Assign::nini($obj, 3, 147, $obj->arrForm['edata147'][$i]);
            $date = $d."/".$m."/".$y;
            $m = date_format(date_create($this->cnvDateFormat($date)), 'F');
            // age
            $date = $y.sprintf("%02d", intval($m)).sprintf("%02d", intval($d));
            $age = (int) ((date('Ymd')-intval($date))/10000);

            $document->setValue("i{$i}",        $i+2);  // 応募者を1とするため同行者は2からスタート
            $document->setValue("n{$i}",        'Nationality:');
            $document->setValue("edata148{$i}", htmlspecialchars(Usr_Assign::nini($obj, 3, 148, $obj->arrForm['edata148'][$i])));
            $document->setValue("o{$i}",        'Occupation:');
            $document->setValue("edata149{$i}", htmlspecialchars(Usr_Assign::nini($obj, 3, 149, $obj->arrForm['edata149'][$i])));
            $document->setValue("f{$i}",        'Full name:');
            $document->setValue("edata111{$i}", htmlspecialchars($obj->arrForm['edata111'][$i]." "));
            $document->setValue("edata112{$i}", htmlspecialchars($middleName));
            $document->setValue("edata110{$i}", htmlspecialchars($obj->arrForm['edata110'][$i]));
            $document->setValue("edata113{$i}", htmlspecialchars(Usr_Assign::nini($obj, 3, 113, $obj->arrForm['edata113'][$i])));
            $document->setValue("d{$i}",        'Date of birth:');
            $document->setValue("edata146{$i}", $m);
            $document->setValue("edata145{$i}", $d);
            $document->setValue("edata147{$i}", $y);
            $document->setValue("age{$i}",      "(Age: {$age})");
            $document->setValue("r{$i}",        'Relationship with the inviting person and guarantor:');
            $document->setValue("c{$i}",        "Chair of local organizing committee for the XVIII ISA World Congress of Sociology and applicant's family");
        }
    }


    // Mng 共通設定
    function __constructMng($obj){
        // 特殊項目に追加項目を追加
//        $obj->formarray = array_merge($obj->formarray, array(137, 138, 139));

        $obj->edata136url = array();
        $obj->edata136url[1] = "http://www.ups.com/content/jp/en/index.jsx";
        $obj->edata136url[2] = "http://www.post.japanpost.jp/int/ems/index_en.html";

        if(isset($obj->_smarty)){
            $obj->assign("edata136url", $obj->edata136url);
        }

        if(isset($obj->searchkey)){
            // 追加の検索キー
            if(isset($obj->arrItemData[1][135]['select'])){
                foreach($obj->arrItemData[1][135]['select'] as $_key => $item){
                    array_push($obj->searchkey, 'edata135'.$_key);
                }
                // Blank
                array_push($obj->searchkey, 'edata13599');
            }

            array_push($obj->searchkey, 'edata138sy', 'edata138sm', 'edata138sd', 'edata138ey', 'edata138em', 'edata138ed');
            array_push($obj->searchkey, 'send_sy', 'send_sm', 'send_sd', 'send_ey', 'send_em', 'send_ed');
            array_push($obj->searchkey, 'send');
        }
    }


    // Mng 詳細 # エントリー取得後フィルタ
    function mng_detail_getEntry_after($obj){
        // チェックボックス # 出力ステータス
        $wk_checked  = explode("|", $obj->arrData["edata135"]);
        foreach($wk_checked as $chk_val){
            $obj->arrData["edata135".$chk_val] = $chk_val;
        }
    }


    // Mng 詳細 # 保存ボタンクリック
    function Mng_detail_saveAction($obj){
        include_once(MODULE_DIR.'entry_ex/Usr_entry.class.php');

        // フォームパラメータ
        $keys = array(135,136,137,138,139,140);
        $arrForm = GeneralFnc::convertParam($this->_initOption($obj, $keys), $_REQUEST);

        $objErr = new Validate();
        $objErr->check($this->_initOption($obj, $keys), $arrForm);
        $obj->arrErr = $objErr->_err;

        $obj->arrForm = array_merge($obj->arrForm, (array)$arrForm);    // 入力チェック、DBパラメータ生成用
        $obj->arrData = array_merge($obj->arrData, (array)$arrForm);    // 表示用

        if(count($obj->arrErr) > 0) return;

        // 更新データ生成
        $tmp = array();
        foreach($keys as $_key => $item_id){
            $obj->arrItemData[1][$item_id]["disp"] = "";
            $tmp['edata'.$item_id] = '';
        }
        $param = Usr_entryDB::makeDbParam($obj);
        $param = array_intersect_key($param, $tmp);
        $param["udate"] = "NOW";

        $where = "eid = ".$obj->db->quote($obj->eid);
        $rs = $obj->db->update("entory_r", $param, $where, __FILE__, __LINE__);
        if(!$rs) {
            $obj->onload = "alert('登録情報の更新に失敗しました。');";
            return;
        }

        // indexへ戻りアラート表示
        $obj->onload = "f_back();";
        $GLOBALS["session"]->setVar("onload", "alert('登録情報の更新に成功しました。');");
    }


    function _initOption($obj, $keys){
        $key = array();

        foreach($keys as $_key => $item_id){
            Usr_init::_initNini($obj, $key, $item_id);
        }
        return $key;
    }

    /* Mng # エントリー一覧 # 戻るボタン */
    function Mng_index_backAction($obj){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();
        $obj->assign("arrEid", $arrEid);

        // 詳細画面からのjsOnload
        $obj->onload = $GLOBALS["session"]->getVar("onload");
        $GLOBALS["session"]->unsetVar("onload");

        // おまじない
        if(isset($obj->arrItemData[1][135]['select'])){
            foreach($obj->arrItemData[1][135]['select'] as $_key => $item){
                $_REQUEST['edata135'.$_key] = NULL;
                $obj->arrForm['edata135'.$_key] = NULL;
            }
        }
        return;
    }


    //----------------------------------
    // Mng # 一括処理カスタマイズ
    //----------------------------------
    function __constructMngPlural($obj){
        foreach($obj->arrItemData[1][135]['select'] as $val => $item){
            $GLOBALS["entryStatusList"]["135".$val] = $item;
        }
    }


    // 支払いステータスの変更
    function mng_plural_chgStatus($obj, $new_stat) {
        if(substr($new_stat, 0, 3) == "135"){
            $param = array();
            $param["edata135"] = substr($new_stat, 3);
            $param["udate"] = "NOW";

            $from  = "entory_r";
            $where = array();
            $where[] = "eid in (".implode(",", $obj->arrForm["eid"]).")";
            $rs = $obj->db->update($from, $param, $where, __FILE__, __LINE__);

            // チェック情報を破棄
            $GLOBALS["session"]->unsetVar("eid");

            if(!$rs) return false;
            return true;
        }

        $param = array();
        $param["status"] = $new_stat;
        $param["udate"] = "NOW";

        $from  = "entory_r";
        $where = array();
        $where[] = "eid in (".implode(",", $obj->arrForm["eid"]).")";
        $rs = $obj->db->update($from, $param, $where, __FILE__, __LINE__);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

        if(!$rs) return false;
        return true;
    }


    //----------------------------------------
    // ▽Mng # エントリー一覧
    //----------------------------------------
    /* アクション # ファイル一括ダウンロード */
    function Mng_index_file_downloadAction($obj){
        $tmp = $obj->arrForm;

        if(!isset($_POST["file_num"]) || $_POST["file_num"] == ""){
            $obj->objErr->addErr("ファイルダウンロードに必要なパラメータが不足しています。", "file_num");
            $obj->arrErr = $obj->objErr->_err;
            return;
        }

        // ファイルアップロード1-3一括
        if($_POST["file_num"] != "word"){
            $cmd = "/usr/bin/php ".ROOT_DIR."bat/bat_file_archive.php ".$obj->form_id." ".$_POST["file_num"]." > /dev/null &";
            system($cmd);

            $msg = $GLOBALS["userData"]["form_mail"]."へ完了メールを送信しました。";
            $obj->onload = "window.alert('".$msg."');";
            return;
        }

        $arrForm = $_POST;
        // アクセスチェック
        if(!Mng_function::checkParam($arrForm)){
            Error::showErrorPage("不正なアクセスです。");
            exit;
        }
        // 処理対象のエントリーIDを取得
        Mng_function::getEids($arrForm);
        // パラメータチェック
        if(!isset($arrForm["eid"]) || !count($arrForm["eid"]) > 0) {
            $obj->arrForm['mode'] = "";
            $obj->assign("msg", "エントリーが指定されていません。");
            $obj->_processTemplate = "Mng/Mng_complete.html";
            $obj->basemain();
            exit;
        }


        // word
        // 足りない類を生成する
        foreach($arrForm['eid'] as $_key => $eid){
            $obj->eid = $eid;
            Mng_function::getEntry($obj);
            for($i=1; $i<=5; $i++){
                $_POST['type'] = $i;
                $this->Mng_detail_phpwordAction($obj, false);
            }
        }
        $obj->eid = NULL;
        $obj->arrData = NULL;

        // zip処理
        $eids = implode(",", $arrForm['eid']);
        $cmd = "/usr/bin/php ".ROOT_DIR."bat/include/16/bat_file_archive16.php ".$obj->form_id." ".$_POST["file_num"]." ".$eids." > /dev/null &";
        system($cmd);

        $msg = $GLOBALS["userData"]["form_mail"]."へ完了メールを送信しました。";
        $obj->onload = "window.alert('".$msg."');";

        // リストア
        $obj->arrForm = $tmp;
        return;
    }


    /**
     * 入力チェック
      *
     * @access public
     * @return object
     */
    function mng_index_check($obj){

        //妥当姓チェック（存在しない日付の場合はエラー）
        $key = GeneralFnc::stdClass('{"y":"edata138sy", "m":"edata138sm", "d":"edata138sd"}');
        $keys1 = array("key"=>"search_edata138s", "name"=>"ビザ書類送付日（開始）", "keys"=>$key);

        $key = GeneralFnc::stdClass('{"y":"edata138ey", "m":"edata138em", "d":"edata138ed"}');
        $keys2 = array("key"=>"search_edata138e", "name"=>"ビザ書類送付日（終了）", "keys"=>$key);

        $arrkey = array($keys1, $keys2);
        foreach($arrkey as $_key => $keys){
            $key  = $keys["keys"];
            if(($obj->arrForm[$key->y] != "") && ($obj->arrForm[$key->m] != "") && ($obj->arrForm[$key->d] != "") ){
                if(!checkdate((int)$obj->arrForm[$key->m], (int)$obj->arrForm[$key->d], (int)$obj->arrForm[$key->y])){
                    $obj->objErr->addErr("{$keys['name']}に存在しない日付が指定されています。", $keys["key"]);
                }
            }
        }

        return $obj->objErr->_err;
    }

    function Mng_buildFrom($obj, $from){
        // 送信履歴の最新の日付を含める
        return '(SELECT * FROM mail_sendentry AS A1 
WHERE NOT EXISTS( 
SELECT * FROM mail_sendentry AS A2 
WHERE A1.eid = A2.eid AND A1.send_date < A2.send_date 
)) AS mail_sendentry  
RIGHT JOIN v_entry USING (eid)' ;
    }
//    // Mng # select分拡張
//    function Mng_buildColumn($obj, $column){
//        $column .= ",(select send_date from mail_sendentry where v_entry.eid = mail_sendentry.eid order by send_date desc limit 1) as send_date";
//        return $column;
//    }

    // Mng # 検索条件拡張
    function Mng_buildWhere_after($obj, $condition, $where){
        // 処理ステー他sう
        if(isset($obj->arrItemData[1][135]['select'])){
            $subwhere = array();
            // 処理ステータスを指定
            if(empty($condition["edata13599"])){
                foreach($obj->arrItemData[1][135]['select'] as $_key => $item){
                    if($condition["edata135".$_key] == $_key){
                        $subwhere[] = "edata135 = ".$obj->db->quote($_key);
                    }
                }
            // 処理ステータス # Blank
            }else{
                $subwhere[] = "edata135 = ''";
                $subwhere[] = "edata135 is null";
            }
            if(count($subwhere) > 0){
                $where[] = "(".implode(" or ", $subwhere).")";
                unset($subwhere);
            }
        }

        // ビザ書類送付日 # 開始日
        if($condition["edata138sy"] > 0) {
            if($condition["edata138sm"] == "") $condition["edata138sm"] = "1";
            if($condition["edata138sd"] == "") $condition["edata138sd"] = "1";
            $sdate = sprintf("%04d", $condition["edata138sy"])."-".sprintf("%02d", $condition["edata138sm"])."-".sprintf("%02d", $condition["edata138sd"]);
            $sdate.= " 00:00:00";

            $where[998] = "edata138 is not null";
            $where[999] = "edata138 <> ''";
            $where[] = "edata138::TIMESTAMP >= ".$obj->db->quote($sdate);
        }
        // ビザ書類送付日 # 終了日
        if($condition["edata138ey"] > 0) {
            if($condition["edata138em"] == "") $condition["edata138em"] = "12";
            if($condition["edata138ed"] == "") {
                $edate = date("Y-m-d", mktime(0, 0, 0, $condition["edata138em"]+1, 0, $condition["edata138ey"]));
            } else {
                $edate = sprintf("%04d", $condition["edata138ey"])."-".sprintf("%02d", $condition["edata138em"])."-".sprintf("%02d", $condition["edata138ed"]);
            }
            $edate.= " 23:59:59";

            $where[998] = "edata138 is not null";
            $where[999] = "edata138 <> ''";
            $where[] = "edata138::TIMESTAMP <= ".$obj->db->quote($edate);
        }

        // 送付通知日-通常 # 開始日
        if($condition["send_sy"] > 0) {
            if($condition["send_sm"] == "") $condition["send_sm"] = "1";
            if($condition["send_sd"] == "") $condition["send_sd"] = "1";
            $sdate = sprintf("%04d", $condition["send_sy"])."-".sprintf("%02d", $condition["send_sm"])."-".sprintf("%02d", $condition["send_sd"]);
            $sdate.= " 00:00:00";

            $where[] = "send_date >= ".$obj->db->quote($sdate);
        }
        // 送付通知日-通常 # 終了日
        if($condition["send_ey"] > 0) {
            if($condition["send_em"] == "") $condition["send_em"] = "12";
            if($condition["send_ed"] == "") {
                $edate = date("Y-m-d", mktime(0, 0, 0, $condition["send_em"]+1, 0, $condition["send_ey"]));
            } else {
                $edate = sprintf("%04d", $condition["send_ey"])."-".sprintf("%02d", $condition["send_em"])."-".sprintf("%02d", $condition["send_ed"]);
            }
            $edate.= " 23:59:59";
            $where[] = "send_date <= ".$obj->db->quote($edate);
        }

        // 送付通知-通常 # blank検索
        if($condition['send'] == 1){
            $where[] = "send_date is null";
        }

        return $where;
    }


    //----------------------------------------
    // Mng # メール送信履歴
    function Mng_mailhistory_buildFrom(){
        return "
        (
select
 *
,array_to_string(ARRAY(select eid from mail_sendentry where mail_history.send_id = mail_sendentry.send_id), ',') as eid
,array_to_string(
    ARRAY(
        select e_user_id||':'||edata1||edata2 as entry from entory_r where eid in (select eid from mail_sendentry where mail_history.send_id = mail_sendentry.send_id) order by entry asc
    )
    , ',') as entry
,array_to_string(
    ARRAY(
        select edata1||edata2 from entory_r where eid in (select eid from mail_sendentry where mail_history.send_id = mail_sendentry.send_id)
    )
    , ',') as user_name

,(select max(e_user_id) from (select substr(e_user_id, length(e_user_id)-4 ,5) as e_user_id from entory_r where eid in (select eid from mail_sendentry where mail_history.send_id = mail_sendentry.send_id)) as e_user_id) as max
,(select min(e_user_id) from (select substr(e_user_id, length(e_user_id)-4 ,5) as e_user_id from entory_r where eid in (select eid from mail_sendentry where mail_history.send_id = mail_sendentry.send_id)) as e_user_id) as min
,array_to_string(
    ARRAY(
        select substr(e_user_id, length(e_user_id)-4 ,5) as e_user_id from entory_r where eid in (select eid from mail_sendentry where mail_history.send_id = mail_sendentry.send_id)
    )
    , ',') as e_user_id

from
mail_history 

 where form_id = '16'
) as mail_history_v";
    }


    function Mng_mailhistory_buildWhere_after($obj, $where, $search_key){
        // 応募者名
        if(strlen($search_key['user_name']) > 0){
            $search_key["user_name"] = mb_convert_kana($search_key["user_name"], "naKV", "utf8");
            $search_key["user_name"] = strtolower($search_key["user_name"]);

            $arrval = GeneralFnc::customExplode($search_key["user_name"]);
            if(count($arrval) > 0) {
                foreach($arrval as $val) {
                    $subwhere[] = "((lower(translate(user_name, '－０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ', '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')) LIKE ".$obj->db->quote("%".$val."%")."))";
                }
                $where[] = "(".implode(" or ", $subwhere).")";
                unset($subwhere);
            }
        }

        // 登録No # 開始
        if($search_key["s_entry_no"] != "") {
            $search_key["s_entry_no"] = mb_convert_kana($search_key["s_entry_no"], "naKV", "utf8");
            $search_key["s_entry_no"] = strtolower($search_key["s_entry_no"]);
            $s_entry_no = sprintf("%05d", $search_key["s_entry_no"]);   // 0埋め5桁
        }

        // 登録No # 終了
        if($search_key["e_entry_no"] != "") {
            $search_key["e_entry_no"] = mb_convert_kana($search_key["e_entry_no"], "naKV", "utf8");
            $search_key["e_entry_no"] = strtolower($search_key["e_entry_no"]);
            $e_entry_no = sprintf("%05d", $search_key["e_entry_no"]);   // 0埋め5桁
        }


        // 登録No # 範囲指定
        if($search_key["e_entry_no"] != "" && $search_key["s_entry_no"] != "") {
            $s = intval($s_entry_no);
            $e = intval($e_entry_no);
            $subwhere = array();
            for($i=$s; $i<=$e; $i++){
                $subwhere[] = "e_user_id like ".$obj->db->quote("%".sprintf("%05d", $i)."%");
            }
            $where[] = "(".implode(" or ", $subwhere).")";
            unset($subwhere);

        }else{
            // 登録No # 開始
            if($search_key["s_entry_no"] != "") {
                $where[] = "max >= ".$obj->db->quote($s_entry_no);
            }

            // 登録No # 終了
            if($search_key["e_entry_no"] != "") {
                $where[] = "min <= ".$obj->db->quote($e_entry_no);
            }
        }
        return $where;
    }


//    function Mng_mailhistory_getList_after($obj, $count, &$arrData){
//        if(!$arrData) return array($count, "");
//
//        // エントリー情報取得
//        $eids = "";
//        foreach($arrData as $_key => &$_arrData){
//            $eids .= $_arrData['eid'].",";
//            $_arrData['eid'] = explode(",", $_arrData['eid']);
//        }
//        $array = explode(',', rtrim($eids, ','));
//        $array = array_unique($array, SORT_NUMERIC);
//
//        $where  = array();
//        $where[]= "form_id = ".$obj->db->quote($obj->form_id);
//        $where[]= "eid in(".implode(",", $array).")";
//
//        $arrData2 = array();
//        $rs = $obj->db->getListData('*', 'entory_r', $where);
//        foreach($rs as $_key => $_entry){
//            $eid = $_entry['eid'];
//            $arrData2[$eid] = array();
//            $arrData2[$eid]['uid']  = $_entry['e_user_id'];
//            $arrData2[$eid]['name'] = $_entry['edata1']." ".$_entry['edata2'];
//        }
//        $obj->assign("arrData2", $arrData2);
//    }


    //----------------------------------------------------
    // Mng # 一括メール送信
    //----------------------------------------------------
    function __constructMngMailSend($obj){
        $customDir = ROOT_DIR."customDir/".$obj->form_id."/mailtemplate/";

        // メールテンプレート取得
        $arrTemplate = array();

        $template1 = '*';
        $template2 = '[0-9]+';
        $ex = glob($customDir.$template1);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                if(preg_match("/".$template2."$/u", $script, $match) === 1){
                    $fp = fopen($script, 'r');
                    if($fp === false) continue;

                    $id = intval($match[0]);
                    $arrTemplate[$id] = array();
                    $arrTemplate[$id] = fgetcsv($fp, 10000);
                    fclose($fp);
                }
            }
        }
        $obj->arrTemplate = $arrTemplate;
        $obj->assign("arrTemplate", $obj->arrTemplate);
    }

    function Mng_mailsend_premain($obj){
        // 一括メール送信は「出力済」を対象とする
        $column = 'count(eid)';
        $from   = 'entory_r';
        $where  = array();
        $where[] = 'form_id = 16';
        $where[] = 'eid in ('.implode(',', $obj->arrForm['eid']).')';
        $where[] = "(edata135 <> ".$obj->db->quote(4)." or edata135 = '' or edata135 is null)";

        $rs = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
        $count = $rs['count'];
        if($count > 0){
            $obj->complete("処理ステータス「出力済」でない応募者が含まれています。");
        }
    }

    function Mng_mailsend_templateAction($obj){
        $type = "作成";
        // 編集 # テンプレートの内容を取得
        if(strlen($obj->arrForm['mail_template']) > 0){
            $type = "編集";
        }
        $obj->assign("type", $type);

        $id = $obj->arrForm['mail_template'];
        $obj->arrForm['mail_name'] = $obj->arrTemplate[$id][0];
        $obj->_processTemplate = "Mng/mail/include/16/16mailtemplate.html";
    }

    function Mng_mailsend_saveAction($obj){
        $type = "作成";
        // 編集 # テンプレートの内容を取得
        if(strlen($obj->arrForm['mail_template']) > 0){
            $type = "編集";
        }
        $obj->assign("type", $type);

        $customDir = ROOT_DIR."customDir/".$obj->form_id."/mailtemplate/";
        $obj->objErr = new Validate();

        // 入力チェック
        $keys = array("mail_name"   , "mail_ttl" , "mail_body");
        $name = array("テンプレート",  "タイトル", "本文");
        foreach($keys as $_key => $key){
            if(!$obj->objErr->isNull($obj->arrForm[$key])){
                $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $name[$_key]), $key);
            }
        }
        $obj->arrErr = $obj->objErr->_err;
        if(count($obj->arrErr) > 0){
            $obj->_processTemplate = "Mng/mail/include/16/16mailtemplate.html";
            return;
        }

        $format = "\"%s\"";
        $body = str_replace(array("\r\n", "\r"), "<:br:>", $obj->arrForm["mail_body"]);
        $body = str_replace('"', "<:q:>", $body);

        $template   = array();
        $template[] = sprintf($format, $obj->arrForm["mail_name"]);
        $template[] = sprintf($format, $obj->arrForm["mail_ttl"]);
        $template[] = sprintf($format, $body);
        $template = implode(",", $template);

        // 編集
        if(strlen($obj->arrForm['mail_template']) > 0){
            $file = $obj->arrForm['mail_template'];
            unlink($customDir.$file);

        // 新規
        }else{
            // 既存ファイルがないかチェックは行わず上書き
            $file = count($obj->arrTemplate)+1;
        }

        // ファイル書き込み
        GeneralFnc::writeFile($customDir.$file, $template);

        $obj->arrTemplate[] = $file;
        $obj->onload = "window.alert('テンプレートの保存が完了しました。');";
        $this->__constructMngMailSend($obj);
    }


    // Mng # 一括メール送信 # 置換文字列拡張
    function Mng_mailsend_replaceStr_after($obj, &$body, $arrForm, $wa_replaceStr){
        // 特殊項目の対応
        foreach($obj->formarray as $item_id){
            $key = 'edata'.$item_id;
            $arrForm[$key] = unserialize($arrForm[$key]);
        }

        // 追加の置換文字列
        $keys = array(136,137,138,139,140); // 135は除く
        $arrStr = array();
        foreach($keys as $_key => $item_id){
            $key = "edata".$item_id;
            
            $value = $arrForm[$key];
            if(is_array($value)){
                $value = $value[0];
            }
            $arrStr[$key] = Usr_Assign::nini($obj, 1, $item_id, $value);
        }

        // 追加の置換文字列
        // 135 _STATUS_  TODO 除外
        // 136 _SENDTYPE_
        // **  _URL_                *追跡確認URL
        // 137 _ITEM_NUMBER_
        // 138 _SEND_DATE_
        // 139 _EXPECTED_ARRIVAL_
        // 140 _MEMO_
        // **  _NOW_                *当日

//        $body = str_replace("_STATUS_"          , $arrStr["edata135"],$body);
        $body = str_replace("_SENDTYPE_"        , $arrStr["edata136"], $body);
        $body = str_replace("_URL_"             , $obj->edata136url[$arrForm['edata136']], $body);
        $body = str_replace("_ITEM_NUMBER_"     , $arrStr["edata137"], $body);
        $body = str_replace("_SEND_DATE_"       , $arrStr["edata138"], $body);
        $body = str_replace("_EXPECTED_ARRIVAL_", $arrStr["edata139"], $body);
        $body = str_replace("_MEMO_"            , $arrStr["edata140"], $body);
        $body = str_replace("_NOW_"             , date("Y/m/d")      , $body);
    }

}
