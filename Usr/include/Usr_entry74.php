<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry74 {

   function __construct($obj) {
   }

   function developfunc($obj) {
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
   }


   // [reg3-form74] 見積No.4：オプション料金で同伴者の料金②を選択している場合、ブロック③選択肢に 「参加する（同伴者と一緒に）」を追加
   function premain($obj) {

       $arrForm1 = $GLOBALS["session"]->getVar("form_param1");

       $key = 'ather_price0';
       $accompanying0 = isset($arrForm1[$key]) && strlen($arrForm1[$key]) > 0 ? intval($arrForm1[$key]) : 0;

       $key = 'ather_price1';
       $accompanying1 = isset($arrForm1[$key]) && strlen($arrForm1[$key]) > 0 ? intval($arrForm1[$key]) : 0;

       $accompanying = max(array($accompanying0, $accompanying1));

       if(method_exists($obj, "assign")){
           if(empty($accompanying)){
               unset($obj->arrItemData[3]['54']['select'][2]);
               $obj->assign("arrItemData", $obj->arrItemData);
           }
       }
   }
   
   
   // エラーチェック # ブロック1
   function _check1($obj) {
       Usr_Check::_check1($obj);
       $group_id = 1;

       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;

       // [reg3-form74]見積No2:同伴者情報で「姓」の入力があった場合はオプション料金の内、同伴者の料金②を必須 
       $item_id = 64;
       $key = "edata" . $item_id;
       $allKey2 = array("ather_price0","ather_price1");
       $appear_flg = false;

       if ($obj->arrParam [$key] != "") {
           foreach ($allKey2 as $key2){
               if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key2] == "1"){
                   $appear_flg = true;
                   break;
               } else {
                   $appear_flg = false;
               }
           }

           if ($appear_flg === false) {
               $obj->objErr->addErr("「同伴者」または「同伴者（18歳以下）」項目にて人数を選択ください。 ", $key2);
           }

       }
           
       // [reg3-form74]見積No2:オプション料金で同伴者の金額のラジオボタンを選択していたら、同伴者情報の入力を必須にする
       foreach ($allKey2 as $key2) {
           if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key2] == "1") {
               if ($obj->arrParam [$key] == "") {
                   $obj->objErr->addErr("同伴者情報を入力ください。", $key);
               }
           }
       }
       
       // [reg3-form74]見積No3:「同伴者」と「同伴者（18歳以下）」はどちらか1項目しか選択できないようにエラー設定をかける 
       if($obj->arrParam [$allKey2[0]] == "1" && $obj->arrParam [$allKey2[1]] == "1") {
           $obj->objErr->addErr("「同伴者」と「同伴者（18歳以下）」は、どちらか1項目しか選択できません。");
       }

   }
}
