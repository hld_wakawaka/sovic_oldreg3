<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld kawauchi
 *        
 */
class Usr_Entry102 {


   /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
//        
        // jQueryを読み込む
        $obj->useJquery = true;
//        
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
//        
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
//        
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
   }

//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }

   /**
    * ProcessBase::mainの前処理
    */
   function premain($obj) {
       // [reg3-102] T20：グループ1のテキストを非表示
       $obj->arrItemData[1][27]['disp'] = "1";
       $obj->arrItemData[1][28]['disp'] = "1";
       
       // [reg3-102] T20：グループ3のテキストを非表示
       $obj->arrItemData[3][55]['disp'] = "1";
   }
   
//    // 項目チェックの際は項目のdispフラグを考慮するため
//    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する
//    

    
   function _check1($obj) {
       Usr_Check::_check1($obj);
       
       $group_id = 1;
       
       // Fee
       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
       
       // [reg3-form102] E02・Fee> Member(JSAM/JTAMS)：Member(JSAM/JTAMS)を選んだ場合は、Your Membershipを選択していないとエラー
       if($amount == 1) {
           $item_id = 26;
           $key = "edata" . $item_id;
           if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
               $method = 'Please select your membership to apply for Member(JSAM/JTAMS) category.';
               $obj->objErr->addErr($method, $key);
           }
       }
       
       // [reg3-form102] E02・グループ1>Your Membership：Your Membershipに⼊力がある場合は、Member(JSAM/JTAMS)しか選べない
       $item_id = 26;
       $key = "edata" . $item_id;
       if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] != "") {
           if($amount != 1) {
               $method = 'Please select Member(JSAM/JTAMS) for fee if you are a member and filled in your membership no.';
               $obj->objErr->addErr($method, $key);
           }
       }
       
       
       // [reg3-form102] E01・グループ1>Your Membership：JSAM MemberとBoth JSAM and JTAMS Memberを選んだ場合、それぞれのテキスト⼊力を必須
       $ymKey = "edata26";
       if($obj->arrParam [$ymKey] != "1") {
           $item_id = 27;
           $key = "edata" . $item_id;
           if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
               $name = Usr_init::getItemInfo($obj, $item_id);
               $method = Usr_init::getItemErrMsg($obj, $item_id);
               $obj->objErr->addErr(sprintf($method, $name), $key);
           }
       }elseif ($obj->arrParam [$ymKey] != "3") {
           $item_id = 28;
           $key = "edata" . $item_id;
           if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
               $name = Usr_init::getItemInfo($obj, $item_id);
               $method = Usr_init::getItemErrMsg($obj, $item_id);
               $obj->objErr->addErr(sprintf($method, $name), $key);
           }
       }
       
   }
   
   
  // [reg3-form102] E01・グループ3>Questionnaire >1) Are you a presenter?：
  // Yesを選んだ場合、Abstract Reference No.のテキスト⼊力を必須
  function _check3($obj) {
      Usr_Check::_check3($obj);

      $group_id = 3;
      $item_id = 54;
      $target = 'Yes';
      if (Usr_Check::chkSelect($obj, $group_id, $item_id, $target)) {
          $item_id = 55;
          $key = "edata" . $item_id;
          if (Usr_init::isset_ex($obj, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
              $name = Usr_init::getItemInfo($obj, $item_id);
              $method = Usr_init::getItemErrMsg($obj, $item_id);
              $obj->objErr->addErr(sprintf($method, $name), $key);
          }
      }
  }
  
  
//      function sortFormIni($obj) {
//          $arrGroup1 = & $obj->arrItemData [1];
  
//          // 入れ替え
//          $array = array ();
//          foreach ( $arrGroup1 as $key => $data ) {
//              switch ($key) {
//                  case 7 : // middle name
//                      $array [$key] = $data;
//                      $array [26] = $arrGroup1 [26];
//                      $array [27] = $arrGroup1 [27];
//                      $array [28] = $arrGroup1 [28];
//                      $array [63] = $arrGroup1 [63];
//                      $array [64] = $arrGroup1 [64];
//                      break;
  
//                  case 26 : // etc1
//                  case 27 : // etc2
//                  case 28 : // etc3
//                  case 63 : // etc4
//                  case 64 : // etc5
//                      break;
  
//                  default :
//                      $array [$key] = $data;
//                      break;
//              }
//          }
//          $arrGroup1 = $array;
//      }
  

}
