<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *
 */
class Usr_Entry17 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){ }


    /** 1ページ目チェック */
    function _check1($obj){
    	Usr_Check::_check1($obj);
    	
        // other price
        $ids = array(5, 6, 7);
        foreach($ids as $_key => $id){
            $key = 'ather_price'.$id;
            $other = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;
            if($other > 0){
                $name = $obj->wa_ather_price[$id]['name'];
                $obj->objErr->addErr(sprintf("Don't select %s.", $name), $key);
            }
        }
//
//        // Student以外を選択 かつ Student用の選択肢を選択
//        if($obj->arrParam['amount'] != 1 && strlen($obj->arrParam['ather_price1']) > 0){
//            $obj->objErr->addErr("Don't Select AVEC party Ticket for Students / Accompanying Persons.", "ather_price1");
//        }
    }


    /** 3ページ目チェック */
    function _check3($obj){

    	Usr_Check::_check3($obj);

    	if(!isset($obj->arrParam['edata54'])) return;
    	if(!isset($obj->arrParam['edata55'])) $obj->arrParam['edata55'] = "";

		if($obj->arrParam['edata54'] == "1" && $obj->arrParam['edata55'] == "") {
			$item_id = '55';
			$key = 'edata'.$item_id;
			$name = Usr_init::getItemInfo($obj, $item_id);
			$method = Usr_init::getItemErrMsg($obj, $item_id);
			$obj->objErr->addErr(sprintf($method, $name), $key);
		}

		return;

    }


    /* 項目並び替え */
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 7:     // middle name
                    $array[$key]= $data;
                    $array[26]  = $arrGroup1[26];
                    $array[27]  = $arrGroup1[27];
                    break;

                case 26:    // etc1
                case 27:    // etc2
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



    //------------------------------------------------
    // ▽ メールカスタマイズ
    //------------------------------------------------

    /** AccompanyingPerson1 */
    function mailfunc73($obj, $key, $name, $i=null) {
    	$wk_key = "edata".$key.$i;
    	if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

    	$str  = "";
    	$str  = "\n";
    	$str .= "\n";
    	$str .= "[Accompanying Person]\n";
    	$str .= "\n";
    	$str .= "Accompanying Person 1\n";
    	$str .= "\n";
    	$str .= $obj->point_mark.$name.": ";

    	switch($obj->itemData[$key]["item_type"]) {
    		case "1":
    			// テキストエリア
    			$str .= "\n".$obj->arrForm[$wk_key];
    			break;
    		case "2":
    		case "4":
    			// ラジオ、セレクト
    			if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
    				$str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
    			}
    			break;
    		case "3":
    			// チェックボックス
    			if(!isset($obj->itemData[$key]["select"])) break;

    			foreach($obj->itemData[$key]["select"] as $n=>$val) {
    				if(!isset($obj->arrForm[$wk_key.$n])) continue;
    				if($obj->arrForm[$wk_key.$n] == $n) {
    					$str .= "\n".$obj->itemData[$key]["select"][$n];
    				}
    			}

    			break;
    		default:
    			// テキストボックス
    			$str .= $obj->arrForm[$wk_key];
    	}
    	$str.= "\n";
    	return $str;
    }


    /** AccompanyingPerson2 */
    function mailfunc78($obj, $key, $name, $i=null) {
    	$wk_key = "edata".$key.$i;
    	if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

    	$str  = "";
    	$str .= "\n";
    	$str .= "Accompanying Person 2\n";
    	$str .= "\n";
    	$str .= $obj->point_mark.$name.": ";

    	switch($obj->itemData[$key]["item_type"]) {
    		case "1":
    			// テキストエリア
    			$str .= "\n".$obj->arrForm[$wk_key];
    			break;
    		case "2":
    		case "4":
    			// ラジオ、セレクト
    			if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
    				$str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
    			}
    			break;
    		case "3":
    			// チェックボックス
    			if(!isset($obj->itemData[$key]["select"])) break;

    			foreach($obj->itemData[$key]["select"] as $n=>$val) {
    				if(!isset($obj->arrForm[$wk_key.$n])) continue;
    				if($obj->arrForm[$wk_key.$n] == $n) {
    					$str .= "\n".$obj->itemData[$key]["select"][$n];
    				}
    			}

    			break;
    		default:
    			// テキストボックス
    			$str .= $obj->arrForm[$wk_key];
    	}
    	$str.= "\n";
    	return $str;
    }

    //------------------------------------------------
    // ▽ CSVカスタマイズ
    //------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
    	$group = 1;
    	$groupHeader[$group][] = "登録No.";
    	foreach($obj->arrItemData[$group] as $_key => $_data){
    		// 表示する設定の場合出力
    		if($_data["item_view"] == "1") continue;

    		// 画面に表示する項目の名称
    		$name = strip_tags($_data["item_name"]);

    		$prefix = "";
    		$item_id = $_data['item_id'];
    		// AccompanyingPerson 1
    		if(in_array($item_id, array(73,74,75,76))){
    			$prefix = "AccompanyingPerson 1 ";
    		}
    		// AccompanyingPerson 2
    		if(in_array($item_id, array(78,79,80,81))){
    			$prefix = "AccompanyingPerson 2 ";
    		}

    		$groupHeader[$group][] = "\"".$prefix.$name."\"";
    	}
    	return $groupHeader[$group];
    }


}
