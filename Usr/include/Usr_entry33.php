<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry33 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

//        // jQueryを読み込む
//        $obj->useJquery = true;
//
//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }

        $GLOBALS["msg"]["browser_back"] = "<center>※ブラウザの戻るボタンは使用しないでください。</center><br/>".$GLOBALS["msg"]["browser_back"];
    }


    /* デバッグ用 */
    function developfunc($obj) {
       // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    function setFormIni($obj){
        Usr_initial::setFormIni($obj);

        // No.1 # 項目の非表示
        unset($obj->arrItemData[1][27]['select'][3]);
        unset($obj->arrItemData[1][27]['select'][4]);
        
        // No.2 # 並び替えをしてインデントをつける
        $tmp = $obj->arrItemData[1][26]['select'];
        $obj->arrItemData[1][26]['select'] = array();
        foreach(array(1,3,4,2) as $new_id){
            $obj->arrItemData[1][26]['select'][$new_id] = $tmp[$new_id];
        }
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
//        $obj->formWord["word38"] = "aa";
//        $obj->formWord["word38"] = '<span style="font-weight: bold;font-size: 17px;">登録確認画面<br/>'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';
        $GLOBALS['msg']['cofirm_desc_i'] = '<span style="font-weight: bold;font-size: 17px;">登録確認画面<br/>'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />内容確認後下記の"register"ボタンをクリックしてください。<br/>'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';
        $obj->assign("formWord",$obj->formWord);

        // [reg3-form33] 項目名「出展を検討しているゾーン」において、選択肢「FBS」「SPA Business Matching」を非表示
        unset($obj->arrItemData[1][27]['select'][5]);
        unset($obj->arrItemData[1][27]['select'][6]);
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        // ご訪問希望日時
        $keys = array('edata124', 'edata125');
        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
                $method = $GLOBALS["msg"]["err_require_input"];
                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 1'), 'edata124');
            }
        }
    }
//
//
//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 26:
                    $array[$key]= $data;
                    $array[124] = $arrGroup1[124];
                    $array[125] = $arrGroup1[125];
                    break;

                case 63:     // 会社カナ > 名前
                    $array[$key]= $data;
                    $array[1]   = $arrGroup1[1];
                    break;

                case 75:     // ビル > メール
                    $array[$key]= $data;
                    $array[25]  = $arrGroup1[25];
                    break;

                case   1:
                case  25:
                case 124:
                case 125:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makeBodyGroup1($obj, &$arrbody, $exec_type){
        // グループ1
        $group = 1;

        // 言語別設定
        if($obj->formdata["lang"] == LANG_JPN){
            $arrbody[1] = ($obj->formdata["group1"] != "") ? "【".$obj->formdata["group1"]."】\n\n" : "";
        }else{
            $arrbody[1] = ($obj->formdata["group1"] != "") ? "[".$obj->formdata["group1"]."]\n\n" : "";
        }


        foreach($obj->arrItemData[$group] as $_key => $data){
            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

            $key = $data["item_id"];

            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"].": \n";

            if(!isset($obj->arrForm["edata".$key])){
                $obj->arrForm["edata".$key] = "";
            }

            $methodname = "mailfunc".$key;
            if(method_exists($obj, $methodname)) {
                $arrbody[$group] .= $obj->$methodname($name);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $arrbody[$group] .= $obj->exClass->$methodname($obj, $key, $name);

                }else{
                    if($data["controltype"] == "1") {
                        $arrbody[$group] .= $obj->mailfuncNini($group, $key, $name);    //任意
                    } else {
                        $arrbody[$group] .= $obj->point_mark.$name." ".$obj->arrForm["edata".$key]."\n";
                    }
                }
            }
        }
    }


    /**
     * 任意
     */
    function mailfuncNini($obj, $group, $item_id, $name, $i=null) {
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array());

        $str  = "";
        $str .= Config::assign_obi(array("mode"=>"mail", "item_id"=>$item_id), $obj->_smarty);
        $str .= $obj->point_mark.$name." ".$value."\n";

        return $str;
    }


    /**
     * 任意
     */
    function mailfunc26($obj, $item_id, $name, $i=null) {
        $group = 1;

        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array());
        $str = $obj->point_mark.$name." ".$value."\n";

        $item_id = 124; // 第1希望
        $name    = Usr_init::getItemInfo($obj, $item_id, "item_name").":";
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array());
        $str .= " ".$name." ".$value."\n";

        $item_id = 125; // 第2希望
        $name    = Usr_init::getItemInfo($obj, $item_id, "item_name").":";
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array());
        $str .= " ".$name." ".$value."\n";

        return $str;
    }


    /**
     * 任意
     */
    function mailfunc71($obj, $item_id, $name, $i=null) {
        $group = 1;

        $name = "住所 / Address";
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        $str = $obj->point_mark.$name."\n 〒".$value;


        $item_id = 72;
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        $str .= " ".$value;


        $item_id = 73;
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        $str .= " ".$value."\n";


        $item_id = 74;
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        $str .= " ".$value."\n";


        $item_id = 75;
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        $str .= " ".$value."\n";

        return $str;
    }


//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
