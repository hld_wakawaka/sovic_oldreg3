<?php

/**
 * reg3番専用カスタマイズフォーム
*
* @subpackage Usr
* @author salon doishun
*
*/
class Usr_Entry62 {

	// ----------------------------------------
	// カスタマイズメモ
	// ----------------------------------------

	/* 共通設定のoverride !Mngでも呼ばれる! */
	function __construct($obj) {}

	/* デバッグ用 */
	function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
	}

	/**
	 * ProcessBase::mainの前処理
	 */
	function premain($obj) {
		// Assignを直前で変更する場合や特定のエラーチェックで利用
	}

	// 項目チェックの際は項目のdispフラグを考慮するため
	// Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する

	// エラーチェック # ブロック1
	function _check1($obj) {
		
		// [form62] 未入力のエラーチェック
// 			$obj->admin_flg="";
		
		// [form62] 横並びにした郵便番号、電話番号の中で他のフォームについてはエラーチェックを行わないようにする
		foreach(array(27,64,69,77,83,115) as $item_id){
			$itemCheck = explode("|", $obj->itemData[$item_id]["item_check"]);
			$nullCheck = array_search("0", $itemCheck);
			if($nullCheck !==  false){
				unset($itemCheck[$nullCheck]);
				unset($itemCheck[0]);
				$obj->itemData[$item_id]["item_check"] = implode('|', $itemCheck);
			}
		}
		
		// [form62] 「その他」を選択した場合、「その他」で表示される項目の必須チェックを回避する
		if($obj->arrParam["edata71"] != "2"){
			
			foreach(array(72,73,74,75,76,78,79,80,81,82,116) as $item_id){
				$itemCheck = explode("|", $obj->itemData[$item_id]["item_check"]);
				$nullCheck = array_search("0", $itemCheck);
				if($nullCheck !==  false){
					unset($itemCheck[$nullCheck]);
					$obj->itemData[$item_id]["item_check"] = implode('|', $itemCheck);
				}
			}
		}
		
		Usr_Check::_check1($obj);
	}

	/*
	 * 項目並び替え
	* 影響範囲：全て（入力、確認、メール、CSV、詳細）
	*/
	function sortFormIni($obj) {
		$arrGroup1 = & $obj->arrItemData [1];

		// 入れ替え
		$array = array ();
		foreach ( $arrGroup1 as $key => $data ) {
			switch ($key) {
			// [form62] 項目移動
				case 13 :
					$array [$key] = $data;
					break;
				case 18 :
				case 19 :
				case 20 :
					break;
				case 25 :
  					$array [26] = $arrGroup1 [26];
					$array [27] = $arrGroup1 [27];
					$array [18] = $arrGroup1 [18];
					$array [19] = $arrGroup1 [19];
					$array [20] = $arrGroup1 [20];
					$array [28] = $arrGroup1 [28];
					$array [63] = $arrGroup1 [63];
					$array [64] = $arrGroup1 [64];
					$array [69] = $arrGroup1 [69];
					$array [$key] = $data;
					break;
				case 26 : // etc1
				case 27 : // etc2
				case 28 : // etc3
				case 63 : // etc4
				case 64 : // etc5
				case 69 :
					break;

				default :
					$array [$key] = $data;
					break;
			}
		}
		$arrGroup1 = $array;
	}

	
	// ------------------------------------------------------
	// ▽メールカスタマイズ
	// ------------------------------------------------------

	/**
	 * 任意
	 */
	
	// [form62] 郵便番号である項目26,27を横並びで表示。郵便番号である項目76,77を横並びで表示。
	
	function mailfunc26($obj, $item_id, $name, $i = null) {
    	$group = 1;

    	$str  = $obj->point_mark . $name . ":";

        $key = "edata" . $item_id;
        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
        $value1 = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
        $value1 = trim($value1);
        	
        $value2 = Usr_Assign::nini($obj, $group, "27", $obj->arrForm ["edata27"]);
        $value2 = trim($value2);
        if(strlen($value1) > 0) $str .= " ".$value1."-".$value2;

    	return $str."\n";
	}
	
	// [form62] 項目27の住所は表示しない
	function mailfunc27($obj, $item_id, $name, $i = null) {}
	function mailfunc64($obj, $item_id, $name, $i = null) {}
	function mailfunc69($obj, $item_id, $name, $i = null) {}
	// [form62] 項目77の郵便番号は表示しない
	function mailfunc77($obj, $item_id, $name, $i = null) {}
	function mailfunc83($obj, $item_id, $name, $i = null) {}
	function mailfunc115($obj, $item_id, $name, $i = null) {}
	
	
	
	
	// [form62] 項目63,64,69にある電話番号の横並び{項目63}-{項目64}-{項目69}で表示。また、項目82,83,115にある電話番号の横並び{項目82}-{項目83}-{項目115}で表示
	// [form62] 項目63にある電話番号は完了メールに表示。その際{項目63}-{項目64}-{項目69}で表示
	function mailfunc63($obj, $item_id, $name, $i = null) {
		$group = 1;
		 
		$str  = $obj->point_mark . $name . ":";
		 
		$key = "edata" . $item_id;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value1 = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		$value1 = trim($value1);
		 
		$value2 = Usr_Assign::nini($obj, $group, "64", $obj->arrForm ["edata64"]);
		$value2 = trim($value2);
	
		$value3 = Usr_Assign::nini($obj, $group, "69", $obj->arrForm ["edata69"]);
		$value3 = trim($value3);
	
		if(strlen($value1) > 0) $str .= " ".$value1."-".$value2."-".$value3;
		 
		return $str."\n";
	}
	
	
	// [form62]その他を選択していなければ、その他会員情報の項目は完了メールに表示しない
	function mailfunc72($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		
		if($flg == "1"){
			return "";
		}
		
		$group =1;
			
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		
		$obj->assign("lang", $obj->o_form->formData["lang"]);
        $str  = Config::assign_obi(array("mode"=>"mail", "item_id"=>$item_id), $obj->_smarty);
		$str .= $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	
	function mailfunc73($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
	
		if($flg == "1"){
			return "";
		}
	
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
	
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	
	function mailfunc74($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	
	function mailfunc75($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	
	function mailfunc76($obj, $item_id, $name, $i = null) {
	
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		if($flg == "1"){
			return "";
		}
	
		$group = 1;
		$str  = $obj->point_mark . $name . ":";
	
		$key = "edata" . $item_id;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value1 = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		$value1 = trim($value1);
		 
		$value2 = Usr_Assign::nini($obj, $group, "77", $obj->arrForm ["edata77"]);
		$value2 = trim($value2);
		if(strlen($value1) > 0) $str .= " ".$value1."-".$value2;
		 
		return $str."\n";
	}
	
	
	function mailfunc78($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	function mailfunc79($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	function mailfunc80($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	function mailfunc81($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	// [form62] 項目82にある電話番号は完了メールに表示。その際{項目82}-{項目83}-{項目115}で表示
	function mailfunc82($obj, $item_id, $name, $i = null) {
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		if($flg == "1"){
			return "";
		}
		
		$group = 1;
		 
		$str  = $obj->point_mark . $name . ":";
		 
		$key = "edata" . $item_id;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value1 = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		$value1 = trim($value1);
		 
		$value2 = Usr_Assign::nini($obj, $group, "83", $obj->arrForm ["edata83"]);
		$value2 = trim($value2);
		
		$value3 = Usr_Assign::nini($obj, $group, "115", $obj->arrForm ["edata115"]);
		$value3 = trim($value3);
		
		if(strlen($value1) > 0) $str .= " ".$value1."-".$value2."-".$value3;
		 
		return $str."\n";
	}
	
	
	function mailfunc116($obj,$item_id,$name,$i = null){
		// [form62]その他を選択していなければ、76,77と82,83,115の項目は完了メールに表示しない
		$flg = Usr_Assign::nini($obj, $group, "71", $obj->arrForm ["edata71"]);
		$flg = trim($flg);
		 
		if($flg == "1"){
			return "";
		}
		 
		$group =1;
		 
		$key = "edata" . $item_id . $i;
		if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
		$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
		 
		$str = $obj->point_mark . $name . ": " . $value . "\n";
		return $str;
	}
	
	
	// ------------------------------------------------------
	// ▽CSVカスタマイズ
	// ------------------------------------------------------
	
	
	/** CSVヘッダ-グループ1生成 */
	function entry_csv_entryMakeHeader1($obj, $all_flg){
	    // 郵便場号2, 電話番号2,3
		foreach(array(27,77,64,69,83,115) as $key => $item_id){
	        $obj->arrItemData[1][$item_id]['item_view'] = 1;
	    }
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
        }
        return $groupHeader[$group];
	}
	
	
	// [form62] 郵便番号、電話番号などそれぞれまとめてハイフンつなぎで出力できるように、対象の項目ごとに以下のように関数に分けて設定
	function csvfunc26($obj, $group, $pa_param, $item_id) {
		$params = $pa_param["edata26"]."-".$pa_param["edata27"];
		$wk_body = Usr_Assign::nini($obj, $group, $item_id, $params, array (
				" ",
				","
		), true);
		$wk_body = trim($wk_body, ",");
		return $wk_body;
	}
	
	function csvfunc76($obj, $group, $pa_param, $item_id) {
		$params = $pa_param["edata76"]."-".$pa_param["edata77"];
		$wk_body = Usr_Assign::nini($obj, $group, $item_id, $params, array (
				" ",
				","
		), true);
		$wk_body = trim($wk_body, ",");
		return $wk_body;
	}
	
	function csvfunc63($obj, $group, $pa_param, $item_id) {
		$params = $pa_param["edata63"]."-".$pa_param["edata64"]."-".$pa_param["edata69"];
		$wk_body = Usr_Assign::nini($obj, $group, $item_id, $params, array (
					" ",
					","
		), true);
		$wk_body = trim($wk_body, ",");
		return $wk_body;
	}
	
	function csvfunc82($obj, $group, $pa_param, $item_id) {
		$params = $pa_param["edata82"]."-".$pa_param["edata83"]."-".$pa_param["edata115"];
		$wk_body = Usr_Assign::nini($obj, $group, $item_id, $params, array (
				" ",
				","
		), true);
		$wk_body = trim($wk_body, ",");
		return $wk_body;
	}
}
