<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author kawauchi
 *        
 */
class Usr_Entry73 {

    // 後期
    // [reg3-form73] 見積No8 後期の日程変更
//     public $aftertime = "2015-09-16 17:00:00";
    
    // 見積No8「後期の日程変更」の設定の解除
//     public $aftertime = "2015-09-16 13:00:00";
    public $aftertime = "2016-09-16 13:00:00";

   /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj) {
        // form_idの取得
        $form_id = $obj->o_form->formData ['form_id'];
       
        // jQueryを読み込む
        $obj->useJquery = true;
        
        if (isset($obj->_smarty)) {
            $obj->_smarty->assign("aftertime", $this->aftertime);
        }
        
        // [reg3-form73] 見積No14 追加カスタマイズ１：金額、その他決済項目のタグで囲まれたHTMLを削除
        if (isset($obj->_smarty)) {
            $obj->_smarty->register_modifier('del_tags73', array (
                $this,
                'smarty_modifier_del_tags'
            ));
        }
        

   }
//    
   /* デバッグ用 */
   function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
   }

      /**
       * ProcessBase::mainの前処理
       */
   function premain($obj) {
          // Assignを直前で変更する場合や特定のエラーチェックで利用
          
//           $key = array(541,542,543,544,545,546,547,548,551,552,553,554,555,556,557,558);
//           var_dump($obj->arrItemData[3]);
//           foreach($key as $item_id) {
//               $obj->arrItemData[3][$item_id]['disabled']['edata'.$item_id] = true;
//           }

      foreach($obj->arrItemData[3][54]['disable'] as $key => $value) {
          $obj->arrItemData[3][54]['disable'][$key] = true;
      }
      
      foreach($obj->arrItemData[3][55]['disable'] as $key => $value) {
          $obj->arrItemData[3][55]['disable'][$key] = true;
      }
      
       
       // [reg3-form73] 「見積No1」決済及びその他決済項⽬目の非表⽰
       unset($obj->wa_price[0]);
       unset($obj->wa_price[1]);
       unset($obj->wa_price[2]);
       unset($obj->wa_price[5]);
       $obj->assign("va_price", $obj->wa_price);
       
       unset($obj->wa_ather_price[1]);
       unset($obj->wa_ather_price[3]);
       unset($obj->wa_ather_price[4]);
       $obj->assign("va_ather_price", $obj->wa_ather_price);
       
//       // 項目名「Paper Manuscript Number 2」の非表示
//       $obj->arrItemData[1][28]['disp'] = '1';
//       $obj->assign("arrItemData",$obj->arrItemData);
   }
   
   // エラーチェック # ブロック1
   function _check1($obj) {
       Usr_Check::_check1($obj);
       
       $group_id = 1;
       
       $selectValues = array(
               
               // [reg3-form73] 見積No11：Fee仕様変更・新たに★1、★2、★3、★6の項目を追加。
               "errCheck1" => array(0,1,3,5,6,7,9),
               
               // [reg3-form73] 見積No11：Fee仕様変更・新たに★1、★2、★3、★6の項目を追加。
               "errCheck2" => array(2,4,8),

               // [reg3-form73] 見積No11：Fee仕様変更・新たに★1、★2、★3、★6の項目を追加。
               "errCheck3" => array(3,4,5,9),
               "errCheck4" => 1,
               "errCheck5" => array(3,4)
       );
       
       $key = "amount";
       if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] !== "") {
           
           // [reg3-form73] Feeの項目★1「IEEE Member」★2「IEEE-IES Member」★4「IEEE Student Member」★6「IEEE Life Member」を選択した場合はPartcipantの項目「IEEE MemberShipNo」が未入力だとエラーが発生するか
           if(in_array($obj->arrParam [$key],$selectValues["errCheck1"])) {
               $item_id = 26;
               $key = "edata" . $item_id;
               if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                   $obj->objErr->addErr('Enter "IEEE Membership Number".', $key);
               }
           }
           
           // [reg3-form73] Feeの項目★3「Non IEEE Member」★5「Student Non Member」を選択した時、Partcipantの項目「IEEE MemberShipNo」を入力するとエラーが発生するか
           if(in_array($obj->arrParam [$key],$selectValues["errCheck2"])) {
               $item_id = 26;
               $key = "edata" . $item_id;
               if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->objErr->isNull($obj->arrParam [$key])) {
                   $errMsg = 'If you have a IEEE Membership number, you can register "Member" category.';
                   $obj->objErr->addErr($errMsg,$key);
               }
           }
       }
       
       
       //-----------------------------
       // No.3
       //-----------------------------
       // [reg3-form73] Partcipantの項目「PaperManuscriptNumber1」を入力した時に、Feeの項目★4「IEEE Student Member」,5「Student Non Member」,6「IEEE Life Member」のいずれかを選択しているとエラーが発生するか
       // PaperManuscriptNumber1の入力状態
       
       
       $item_id = 27;
       $key = "edata" . $item_id;
       $isInputPaperManuscriptNumber1 = Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->objErr->isNull($obj->arrParam [$key]);
       // ★4,5,6の選択状態
       $_key = "amount";
       $isInputFee456 = in_array($obj->arrParam [$_key],$selectValues["errCheck3"]);
       // どちらかを入力
       if($isInputPaperManuscriptNumber1 || $isInputFee456) {
           // どちらも入力している
           if($isInputPaperManuscriptNumber1 && $isInputFee456){
               $errMsg = 'If you are author who registering a paper, please select "IEEE Member", "IEEE-IES Member" or "Non IEEE Member" for Registration Category.';
               $obj->objErr->addErr($errMsg,$key);
           }
       }
       
       
       
       //-----------------------------
       // No.4
       //-----------------------------
       // [reg3-form73] Participantの項目「PaperManuscriptNumber2」を入力した時、その他決済の項目「Additional Paper」の値を入力していないとエラーが発生するか
       // PaperManuscriptNumber2の入力状態
       
       
       $item_id = 28;
       $key = "edata" . $item_id;
       $isInputPaperManuscriptNumer2 = Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->objErr->isNull($obj->arrParam [$key]);
       // Additional Paperの入力状態
       $_key = "ather_price3";
       $isInputAdditionalPaper = $obj->arrParam [$_key] == $selectValues["errCheck4"];
       // どちらかを入力
       if($isInputPaperManuscriptNumer2 || $isInputAdditionalPaper){
           // 後期はAdditionalPaperが非表示になるのでエラーチェックを行わない
           if(!(strtotime(date("Y-m-d H:i:s")) >= strtotime($this->aftertime))){
               // PaperManuscriptNumber2を未入力
               if(!$isInputPaperManuscriptNumer2){
                   $errMsg = 'Enter "Paper Manuscript Number 2".';
                   $obj->objErr->addErr($errMsg,$key);
               }
               // Additional Paperを未入力
               if(!$isInputAdditionalPaper){
                   $errMsg = 'If you would like to submit two papers, please apply for "Additional paper".';
                   $obj->objErr->addErr($errMsg,$key);
               }
           }
       }
       
       
       
       //-----------------------------
       // No.5
       //-----------------------------
       // [reg3-form73] Feeの項目★4「IEEE Student Member」,5「Student Non Member」のいずれかを選択している時、その他決済の項目「Banquet for IEEE Member〜」を選択しているとエラーが発生するか
       // Fee45の選択状態
       // [reg3-form73] 見積No13 カスタマイズ解除のためコメントアウト
//        $key = "amount";
//        $isInputFee45 = in_array($obj->arrParam [$key], $selectValues["errCheck5"]);
//        // Banquet for IEEE〜の選択状態
//        $_key = "ather_price0";
//        $isInputBanquet = $obj->arrParam [$_key] == 1;
//        // どちらかを入力
//        if($isInputFee45 || $isInputBanquet){
//            // どちらも入力してる
//            if($isInputFee45 && $isInputBanquet){
//                $errMsg = 'If you are a student, please select "Banquet for Student, Accompanying guest" for banquet fee.';
//                $obj->objErr->addErr($errMsg,$key);
//            }
//        }
   }
   
   // [reg3-form73] 見積No14 追加カスタマイズ１：金額、その他決済項目のタグで囲まれたHTMLを削除
   function smarty_modifier_del_tags($string) {
   
       if($string == "") return;
   
       $pattern = "<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>(.*?)<\/(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>";
       $string = preg_replace("/".$pattern."/u", "", $string);
   
       return $string;
   }
   
   /**
    * CSVヘッダ生成
    */
   function payment_payMakeHeader($obj, $all_flg=false){
       
   
       $header   = array();
       // payment_csvのみ出力
       if(!$all_flg){
           $header[] = "応募者氏名";
           $header[] = "応募者氏名（カナ）";
       }
       $header[] = "取引ID";
       $header[] = "お支払い方法";
       $header[] = "ステータス";
   
       $header[] = "支払期限";
       $header[] = "クレジット番号";
       $header[] = "カード会社";
       $header[] = "カード名義人";
       $header[] = "有効期限";
       $header[] = "クレジット支払い区分";
       $header[] = "クレジット支払い回数";
       //        $header[] = "セキュリティコード";
   
       $header[] = "お振込み人名義";
       $header[] = "お支払銀行名";
       $header[] = "お振込み日";
   
       $header[] = "コンビニ　ショップコード";
       $header[] = "姓";
       $header[] = "名";
       $header[] = "セイ";
       $header[] = "メイ";
       $header[] = "電話番号";
       $header[] = "メールアドレス";
       $header[] = "郵便番号";
       $header[] = "住所";
       $header[] = "成約日";
       $header[] = "決済機関コード";
       //$header[] = "支払詳細";
   
   
       //------------------------------
       //決済項目
       //------------------------------
       foreach($obj->wa_ather_price as $data){
           //            $head = GeneralFnc::smarty_modifier_del_tags($data);
           
           // [reg3-form73] 見積No12修正 その他決済が「Banquet IEEE Member〜」の場合、CSVで非表示
           if(mb_strpos($data,'Banquet_for_IEEE_Member') !== FALSE) {
               continue;
           }
           
           $head = strip_tags($data);
           $header[] = strip_tags(str_replace(array("_"), " ",$head));
           // 明細表示フラグ
           if($obj->payment_csv_price_disp){
               $header[] = $obj->payment_csv_price_head;
           }
       }
   
       $header[] = "合計金額";
       $header[] = "登録日";
       $header[] = "更新日";
   
       //生成
       $ret_buff  = implode($obj->delimiter, $header);
       $ret_buff .="\n";
   
       return $ret_buff;
   }
   
   
    function payment_pay_c($obj, $pa_detail){

        $isSet = array();
        foreach($obj->wa_ather_price as $key => $ather_name){
            $quantity = "";
            $match_flg = "";
            // 金額表示フラグ
            $price_flg = "";
            $price     = "";
            
            // [reg3-form73] 見積No12修正 その他決済が「Banquet IEEE Member〜」の場合、CSVで非表示
            if(mb_strpos($ather_name,'Banquet_for_IEEE_Member') !== FALSE) {
                continue;
            }
    
            foreach($pa_detail as $_key => $data){
                // バッファ済みのキーは判定しない
                if(in_array($_key, $isSet)) continue;
    
                // 金額
                if($data['type'] == 0){
                    $from = "payment";
                    $column = "csv_price_head0, csv_ather_price_head0";
                    $where = "eid = ".$obj->db->quote($data['eid'])."";
                    $csv_header =  $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
    
                    $pos = strpos($data["name"], "事前登録料金");
                    if($pos !== false){
                        $quantity = $data["name"];
    
                        // CSV用に整形--------------------------------------------------------------
                        if($csv_header['csv_price_head0'] != ""){
                            $quantity = str_replace("事前登録料金", $csv_header['csv_price_head0'], $quantity);
                        }
                        $match_flg = "1";
    
                        $price_flg = "1";
                        $price = $data['price'];
                        break 1;
                    }
    
                // その他決済
                }elseif($key== $data["key"]){
                    $quantity = $data["quantity"];
                    $match_flg = "1";
    
                    $price_flg = "1";
                    $price = $data['price'];
                    break 1;
                }
            }
    
    
            if($match_flg == "1"){
                $wk_buff[] = "\"".GeneralFnc::smarty_modifier_del_tags($quantity)."\"";
                // 明細表示フラグ
                if($obj->payment_csv_price_disp && $price_flg == "1"){
                    $wk_buff[] = "\"".$price."\"";
                }
    
                // バッファ済みのキーをセット
                $isSet[] = $_key;
                $isSet[] = $_key."t";
    
            }else{
                $wk_buff[] = "\"\"";
                // 明細表示フラグ
                if($obj->payment_csv_price_disp){
                    $wk_buff[] = "\"\"";
                }
            }
        }
    
        return $wk_buff;
    }
    
/** 金額のメール本文 */
    function makePaymentBody1($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //新規の場合
        if($exec_type == "1"){
            foreach($obj->wa_price as $p_key => $data){
                if(!isset($obj->arrForm["amount"])) continue;
                if($p_key == $obj->arrForm["amount"]){
                    $price = $data["p1"];
                    if(is_numeric($price)){
                        $price = number_format($data["p1"]);
                        $price = ($obj->formdata["lang"] == LANG_JPN)
                               ? $price."円"
                               : $obj->yen_mark.$price."";
                    }

                    // タグを除去
                    // [reg3-form73] 見積No14 追加カスタマイズ１：金額、その他決済項目のタグで囲まれたHTMLを削除
                    $data["name"] = $this->smarty_modifier_del_tags($data["name"]);
//                     $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["pricetype"] == "2"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .= "　　　".$data["name"]."：".$price."\n";
                        }
                        else{
                            $body_pay .= "      ".$data["name"].":".$price."\n";
                        }
                    }
                    else{
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .=  "　　　".$obj->formdata["csv_price_head0"]."：".$price."\n";
                        }
                        else{
                            $body_pay .=  "      ".$obj->formdata["csv_price_head0"].":".$price."\n";
                        }
                    }
                    break;
                }
            }
        }

        //更新の場合
        if($exec_type == "2"){
            //全てのその他決済で項目を生成しているため、出力は不要
        }
        return $body_pay;
    }


    /** その他決済のメール本文 */
    function makePaymentBody2($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //その他決済がある場合
        if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

            //新規の場合
            if($exec_type == "1"){
                foreach($obj->wa_ather_price  as $p_key => $data ){
                    if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                    if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
                        $price = $data["p1"];

                        // Free, - => 0
                        if(!is_numeric($data["p1"])){
                            $data["p1"] = 0;
                        }
                        $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                        // タグを除去
                        // [reg3-form73] 見積No14 追加カスタマイズ１：金額、その他決済項目のタグで囲まれたHTMLを削除
                        $data["name"] = $this->smarty_modifier_del_tags($data["name"]);
//                         $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = number_format($price)."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = $obj->yen_mark.number_format($price);
                            }
                            $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum)."  (".$price." ×".$obj->arrForm["ather_price".$p_key].")\n";
                        }
                    }
                }
                
            }

            //更新の場合
            if($exec_type == "2"){
                //更新の場合は、登録済みのデータから引っ張ってくる
                $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);

                foreach($wa_payment_detail as $data){
                    $price = $data["price"];
                    // Free, - => 0
                    if(!is_numeric($data["price"])){
                        $data["price"] = 0;
                    }
                    $sum = $data["price"]*$data["quantity"];

                    // タグを除去
//                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                    $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["lang"] == LANG_JPN){
                        if(is_numeric($price)){
                            $price = $price."円";
                        }
                        $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                    }
                    else{
                        if(is_numeric($price)){
                            $price = $obj->yen_mark.$price;
                        }
                        $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum);
                    }

                    // その他決済の場合は内訳を後ろにつける
                    if($data["type"] == "1"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .="　（".$price." ×".$data["quantity"]."）";
                        }
                        else{
                            $body_pay .="  (".$price." ×".$data["quantity"].")";
                        }
                    }
                    $body_pay .= "\n";
                }
            }
        }
        return $body_pay;
    }
}
