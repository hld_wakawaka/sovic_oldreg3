<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry41 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        // jQueryを読み込む
        $obj->useJquery = true;

//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        // Fee
        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;

        // No.3 # ★6を選択したら★1「Name of society of which you are a member」を必須
        if(in_array($amount, array(1,2,3))){
            $item_id = 27;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $obj->objErr->addErr('Select "Name of society of which you are a member".', $key);
            }
        }

        // No.2 # ★1「Name of society of which you are a member」を選択
        $item_id = 27;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
            // ★6,★7,★8,Student以外を選択
            if(in_array($amount, array(0))){
                $obj->objErr->addErr('If you are a member, you can register "Member" category.', $key);
            }
            // No.4 # ★1かつ★6を選択した場合
            // ★1（A）～（C）の選択肢と★6（A）～（C）が同じ団体でないとエラー
            // 6(A) amount:1 => IIR(1)
            // 6(B) amount:2 => JSRAEE(2), ASHRAE(3), CSSJ(4), JSFST(5), JSLTM(6), JSME(7), SHASE(8), CAR(9), Japan Frozen Food Association(10), SAREK(11), TSHRAE(12)
            // 6(C) amount:3 => IIR(1)
            $arrMap = array();
            $arrMap[1] = array(1);
            $arrMap[2] = array(2,3,4,5,6,7,8,9,10,11,12);
            $arrMap[3] = array(1);

            // ★6を選択
            if(array_key_exists($amount, $arrMap)){
                if(!in_array($obj->arrParam[$key], $arrMap[$amount])){
                    $obj->objErr->addErr('Registration category you selected cannot be applied to the society of which you are a member.', $key);
                }
            }
        }

        // No.5 # ★1に関するエラーが発生していない場合は★2を必須
        $item_id = 27;
        $key = "edata".$item_id;
        if(!isset($obj->objErr->_err[$key]) && $obj->objErr->isNull($obj->arrParam[$key])){
            $item_id = 28;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
               $name   = Usr_init::getItemInfo($obj, $item_id);
               $method = Usr_init::getItemErrMsg($obj, $item_id);
               $obj->objErr->addErr(sprintf($method, $name), $key);
           }
        }

        // No.6 # ★3の（Ⅰ）（Ⅱ）の選択の場合、1day（★7）または2days（★8）登録はエラー
        $item_id = 63;
        $key = "edata".$item_id;
        $is_edata63 = Usr_init::isset_ex($obj, $group_id, $item_id) && in_array($obj->arrParam[$key], array(1,3));
        if($is_edata63 && in_array($amount, array(5, 6))){
            $obj->objErr->addErr('Presenters cannot register as 1day or 2days registration.', $key);
        }


        // No.7 # ★3の選択肢が（Ⅰ）（Ⅱ）の場合、入力必須(★4)
        $item_id = 64;
        $key = "edata".$item_id;
        if($is_edata63 && Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
            $name   = Usr_init::getItemInfo($obj, $item_id);
            $method = Usr_init::getItemErrMsg($obj, $item_id);
            $obj->objErr->addErr(sprintf($method, $name), $key);
        }


        // -------------------------------
        // AccompanyingPersonの数
        // -------------------------------
        $cnt = 0;

        // AccompanyingPerson 1
        $keys = array('edata74');
        if ($obj->objErr->isInputwhich($obj->arrParam, $keys)) {
            if ($obj->objErr->isInputAll($obj->arrParam, $keys)) {
                $cnt ++;
            }
        }

        // AccompanyingPerson 2
        $keys = array ('edata79');
        if ($obj->objErr->isInputwhich($obj->arrParam, $keys)) {
            if ($obj->objErr->isInputAll($obj->arrParam, $keys)) {
                $cnt ++;
            }
        }

        // その他決済 # 同伴者
        $other = strlen($obj->arrParam['ather_price0']) > 0 ? intval($obj->arrParam['ather_price0']) : 0;

        // No.8 # 同伴者入力と同伴者購入枚数の不一致でエラー
        if($cnt != $other){
            $obj->objErr->addErr("Number of accompanying person's name you enter and the number of accompanying person fee you select should be the same.", "ather_price0");
        }
    }


    // エラーチェック # ブロック3
    function _check3($obj){
        $group_id = 3;
        Usr_Check::_check3($obj);

        // Fee
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        $amount = strlen($arrForm1['amount']) > 0 ? intval($arrForm1['amount']) : -1;

        // No.13 # 1 Day登録者（★7）は設問1を必須
        if($amount == 5){
             $item_id = 54;
             $key = "edata".$item_id;
             $arrItemData = $obj->arrItemData[$group_id][$item_id];
             if(Usr_init::isset_ex($obj, $item_id) && $this->isNull($obj->arrParam, $group_id, $item_id, $arrItemData)){
                 $name   = Usr_init::getItemInfo($obj, $item_id);
                 $method = Usr_init::getItemErrMsg($obj, $item_id);
                 $obj->objErr->addErr(sprintf($method, $name), $key);
             }
        }

        // No.14 # 2 Day登録者（★8）は設問2を必須
        if($amount == 6){
            $item_id = 55;
            $key = "edata".$item_id;
            $arrItemData = $obj->arrItemData[$group_id][$item_id];
            if(Usr_init::isset_ex($obj, $item_id) && $this->isNull($obj->arrParam, $group_id, $item_id, $arrItemData)){
                $name   = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr('Select the 2 dates of your attendance.', $key);
            }
        }

        // No.14 # 設問2の選択数を2個に制限する
        if($amount == 6){
            $item_id = 55;
            $key = "edata".$item_id;
            $arrItemData = $obj->arrItemData[$group_id][$item_id];
            $checked = $this->countChecked($obj->arrParam, $group_id, $item_id, $arrItemData);
            if($checked > 0 && $checked != 2){
                $obj->objErr->addErr("Select the 2 dates of your attendance.", $key);
            }
        }
    }


    /**
     * 入力/選択しているかチェック
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return boolean true:入力状態, false:未入力
     *
     */
    function isNull($arrParam, $group_id, $item_id, $arrItemData){
        // チェックボックス
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            // 入力数をカウントするので反転
            return !(bool)$this->countChecked($arrParam, $group_id, $item_id, $arrItemData);
        }
    
        // その他
        // 標準関数の戻り値のbool値が逆なので反転
        return !Validate::isNull($arrParam["edata".$item_id]);
    }



    /**
     * チェックボックスの選択数を返す
     *    標準対応予定 TODO 要見直し(引数が使いづらい！)
     *
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return integer $checked     チェックボックスの選択数
     *                              チェックボックスでない場合は0
     */
    function countChecked($arrParam, $group_id, $item_id, $arrItemData){
        $checked =0;
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            $options = $arrItemData['select'];
            foreach($options as $key => $val){
                $wk_val = isset($arrParam["edata".$item_id.$key]) ? $arrParam["edata".$item_id.$key]: "";
                if($wk_val != "") $checked++;
            }
        }

        return $checked;
    }



    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 17 :
                    $array[$key]= $data;
                    $array[26]  = $arrGroup1[26];
                    break;

                case 26 :
                    break;

                default :
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $body_header = Usr_mail::makeMailBody_header($obj, $user_id, $passwd, $exec_type);
    
        $tag = "";
        // クレジットカード
        if($obj->arrForm["method"] == 1){
            $tag = "bank";
        }else
            // 銀行振込
            if($obj->arrForm["method"] == 2){
            $tag = "credit";
        }
    
        // タグ除去
        if(strlen($tag) > 0){
            $pattern = sprintf("!<%s.*?>.*?</%s>!ims", $tag, $tag);
            $body_header = preg_replace($pattern, "", $body_header);
        }
        $body_header = strip_tags($body_header);
    
        return $body_header;
    }
    

//
//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
