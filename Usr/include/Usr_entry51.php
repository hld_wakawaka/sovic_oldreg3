<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry51 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        // 1/5 No.1 # Titleのprofを削除s
        $tmp1 = $GLOBALS['titleList'][1];
        $tmp2 = $GLOBALS['titleList'][2];
        $tmp4 = $GLOBALS['titleList'][4];
        unset($GLOBALS['titleList'][1]);
        unset($GLOBALS['titleList'][2]);
        unset($GLOBALS['titleList'][4]);
        $GLOBALS['titleList'][5] = "Mrs.";
        $GLOBALS['titleList'][4] = $tmp4;
        $GLOBALS['titleList'][2] = $tmp2;
        $GLOBALS['titleList'][1] = $tmp1;


//        // jQueryを読み込む
//        $obj->useJquery = true;
//
//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
//         // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用

        $this->filter_section($obj);
    }


    /**
     * Config.filter # Usr_mail_makeMailBody
     * 
     * @param  object  $obj
     * @return void
     */
    function filter_section($obj){
        // 同伴者の数 # FamilyNameの入力の有無でカウント
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        $cnt = 0;
        foreach(array(78, 116) as $item_id){
            $key = "edata".$item_id;
            if($obj->objErr->isNull($arrForm1[$key])){
                $cnt++;
            }
        }
        // 同伴者なし
        if($cnt == 0){
            $obj->arrItemData[3][68]['disp'] = 1;
            if(method_exists($obj, "assign")){
                $obj->assign("arrItemData", $obj->arrItemData);
            }
        }
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        // Fee
        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;


        // xxで1番目の項目を選択したらテキスト必須
        $arrItemId = array(
                  "26:27:5:"
                , "82:83:5:Accompanying Person 1 "
                , "120:121:5:Accompanying Person 2 "
                , "127:128:5:Accompanying Person 3 "
        );
        foreach($arrItemId as $_key => $_item_id){
            $item_ids = explode(":", $_item_id);
            $val      = $item_ids[2];
            $prename  = $item_ids[3];

            // 「Dietary Requirements」で「Allergy or others」を選択
            $item_id = $item_ids[0];
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == $val){
                // 「— If Allergy or others, please fill in the details.」を必須
                $item_id = $item_ids[1];
                $key = "edata".$item_id;
                if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                    $name = Usr_init::getItemInfo($obj, $item_id);
                    $method = Usr_init::getItemErrMsg($obj, $item_id);
                    $obj->objErr->addErr(sprintf($method, $prename.$name), $key);
                }
            }
        }

        // 1/5 No.5 # Phone, mobil phoneのどちらかの入力を必須
        $keys = array('edata21', 'edata63');
        if(!$obj->objErr->isInputwhich($obj->arrParam, $keys)){
            $obj->objErr->addErr('Enter either "Phone" or " Mobile Phone".', $keys[0]);
        }

            // 1/8 
        $keys = array('edata64', 'edata73');
        if(!$obj->objErr->isInputwhich($obj->arrParam, $keys)){
            $obj->objErr->addErr('Enter either "Exchange/Organization Name" or " Other Exchange/Organization Name".', $keys[0]);
        }
    }


    // エラーチェック # ブロック3
    function _check3($obj){
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");

        // グループ1の「Invitation Letter」が「Yes」の場合
        // 関連項目を必須にする
        if(isset($arrForm1['edata28']) && $arrForm1['edata28'] == 2){
            foreach(array(100,101,102,103,104,105,106,107,108,109) as $item_id){
                $obj->itemData[$item_id]["item_check"] = (strlen($obj->itemData[$item_id]["item_check"]) > 0)
                ? $obj->itemData[$item_id]["item_check"]."|0"
                        : "0";
            }
        }

        $this->filter_section($obj);
        Usr_Check::_check3($obj);

        $item_id = 54;
        $key = "edata".$item_id;
        // Staying at Event's Hotel > Yes
        $item_id = 54;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 4){
            // If No, please fill in the Hotel name.を必須
            $item_id = 55;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }
    }


   /*
    * 項目並び替え
    * 影響範囲：全て（入力、確認、メール、CSV、詳細）
    **/
   function sortFormIni($obj){
       $arrGroup1 =& $obj->arrItemData[1];

       // 入れ替え
       $array = array();
       foreach($arrGroup1 as $key => $data){
           switch($key){
               case 21:
                   $array[$key]= $data;             // phone
                   $array[63]  = $arrGroup1[63];    // mobile phone
                   break;

               case 63:
               case 73:
                   break;

               case 57:
                   $array[64]  = $arrGroup1[64];
                   $array[73]  = $arrGroup1[73];
                   $array[69]  = $arrGroup1[69];
                   $array[70]  = $arrGroup1[70];
                   $array[71]  = $arrGroup1[71];
                   $array[72]  = $arrGroup1[72];
                   $array[$key]= $data;             // Title
                   break;

               case 64:
               case 69:
               case 70:
               case 71:
               case 72: break;

                default:
                   $array[$key] = $data;
                   break;
           }
       }
       $arrGroup1 = $array;



       // 入れ替え
       $arrGroup3 =& $obj->arrItemData[3];
       $array = array();
       foreach($arrGroup3 as $key => $data){
           switch($key){
               case 56:
                   $array[110] = $arrGroup3[110];
                   $array[$key]= $data;
                   break;
       
               case 110: break;
       
               default:
                   $array[$key] = $data;
                   break;
           }
       }
       $arrGroup3 = $array;
   }



//    // ------------------------------------------------------
//    // ▽メールカスタマイズ
//    // ------------------------------------------------------
//
//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
