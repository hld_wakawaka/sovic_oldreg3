<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 * 
 */
class Usr_Entry71
{

    /* デバッグ用 */
    function developfunc($obj)
    {
        // メールデバッグ
        // $obj->ins_eid = 1615;
        // print "--------------------<pre style='text-align:left;'>";
        // print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
        // print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する

    /**
     * メールアドレス、氏名重複チェック
     */
    function checkRepeat($obj, $pa_formparam)
    {
        if($pa_formparam["edata25"] == "" || $pa_formparam["edata1"] == ""){
            return true;
        }
        
        $column = "*";
        $from = "entory_r";
        $where[] = "form_id = " . $obj->db->quote($obj->o_form->formData["form_id"]);
        $where[] = "del_flg = 0";
        // 無効なエントリーは考慮しない
        $where[] = "invalid_flg = 0";
        
        // メールアドレス
        $where[] = "edata25 = " . $obj->db->quote($pa_formparam["edata25"]);
        $where[] = "edata1 = " . $obj->db->quote($pa_formparam["edata1"]);
        
        // 更新の場合
        if($obj->eid != ""){
            $where[] = "eid <> " . $obj->db->quote($obj->eid);
        }
        
        $rs = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!$rs){
            return true;
        }
        return false;
    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    
    // 項目移動。任意項目「当日17:15～の懇親会にご参加されますか ＊」を項目「いずれの団体からのご案内でしょうか ＊」の次に項目移動
    function sortFormIni($obj)
    {
        $arrGroup1 = & $obj->arrItemData[1];
        
        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 1:
                    $array[10] = $arrGroup1[10];
                    $array[11] = $arrGroup1[11];
                    $array[17] = $arrGroup1[17];
                    $array[19] = $arrGroup1[19];
                    $array[21] = $arrGroup1[21];
                    $array[24] = $arrGroup1[24];
                    $array[73] = $arrGroup1[73];
                    $array[74] = $arrGroup1[74];
                    $array[1] = $arrGroup1[1];
                    $array[2] = $arrGroup1[2];
                    $array[3] = $arrGroup1[3];
                    $array[26] = $arrGroup1[26];
                    $array[27] = $arrGroup1[27];
                    $array[25] = $arrGroup1[25];
                    $array[28] = $arrGroup1[28];
                    $array[63] = $arrGroup1[63];
                    $array[64] = $arrGroup1[64];
                    $array[69] = $arrGroup1[69];
                    $array[70] = $arrGroup1[70];
                    $array[71] = $arrGroup1[71];
                    $array[72] = $arrGroup1[72];
                    break;
                
                case 10:
                case 11:
                case 17:
                case 19:
                case 21:
                case 24:
                case 73:
                case 1:
                case 2:
                case 3:
                case 26:
                case 27:
                case 25:
                case 28:
                case 63:
                case 64:
                case 69:
                case 70:
                case 71:
                case 72:
                    break;
                
                case 74:
                    break;
                
                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }

    
    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------
    
    // 帯名「[お申込者様]」の表示
    function mailfunc1($obj, $item_id, $name, $i = null)
    {
        $group = 1;
        
        $key = "edata" . $item_id . $i;
        if(!isset($obj->arrForm[$key]))
            $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        
        $str = "\n\n【お申込者様】\n\n" . $obj->point_mark . $name . ": " . $value . "\n";
        
        return $str;
    }

}
