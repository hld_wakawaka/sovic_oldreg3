<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry18 {

    private $end_date  = '2014-05-01 00:00:00';
    private $edit_date = '2014-05-01 00:00:00';

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // jQueryを読み込む
        $obj->useJquery = true;

        // 編集完了時に利用 # 編集前の情報を保持するフラグ
        $obj->save_editbefore = true;

        // 編集完了メールに利用 # 更新の前後のみ記載する
        $obj->editmail_diff = true;

        $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
        $GLOBALS["form"]->formData['reception_date2'] = $this->end_date;  // 応募期間：終了
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date2; // 全体のクローズ
    }


    /* デバッグ用 */
    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1617;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('JATM-S-00003', "VRONnGqV", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用

        if(strlen($obj->eid) > 0){

            $obj->formWord['word14']    = 'セラー登録確認画面';
            $obj->formdata['form_desc'] = '<span class="red">＊は入力必須項目です。</span><br/>'
                                        . '編集完了後、『次へ』ボタンを押してください。<br/>'
                                        . '<br/>'
                                        . '編集をせずに閉じる場合は、右上の『ログアウト』をクリックしてからページを閉じてください。<br/>'
                                        . '<br/>'
                                        . '<span style="text-decoration: underline;">＞セラー参加マニュアルダウンロード</span><br/>'
                                        . '<ul style="margin:0;">'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_seller_manual.pdf">セラー参加マニュアル（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_seller_application.xls">エクセル版申込書No.1、No.2（Excel）</a></li>'
                                        . '</ul>'
                                        . '<br/>'
                                        . '<span style="text-decoration: underline;">＞参加バイヤー情報ダウンロード</span><br/>'
                                        . '&nbsp;&nbsp;※準備中（4月下旬更新予定）';

            // 編集時のみ
            if($_REQUEST["mode"] != "display"){
                $GLOBALS['msg']['browser_back'] .= '<br/>※編集をせずに終了する場合は、ページ右上の『ログアウト』をクリックしてからページを閉じてください。';
                $GLOBALS['msg']['cofirm_desc2']  = "<b>※この時点では編集は完了しておりません。<br />必ず「登録」ボタンを押してください。</b>";
            }

            if(method_exists($obj, "assign")){
                $obj->assign('formData', $obj->formdata);
                $obj->assign("formWord", $obj->formWord);
            }
        }

    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);
    }


    // エラーチェック # ブロック3
    function _check3($obj){
        Usr_Check::_check3($obj);
    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 15:     // 役職（英語）半角の下に所在地
                    $array[$key]= $data;
                    $array[26]  = $arrGroup1[26];
                    break;

                case 63:    // 意見交換会への参加意思確認の下に予定人数
                    $array[$key]= $data;
                    $array[74]  = $arrGroup1[74];
                    break;

                case 26:
                case 74:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makeBodyGroup1($obj, &$arrbody, $exec_type){
        $tmp = $obj->formdata["group1"];
        $obj->formdata["group1"] = 'お申込内容（抜粋）';
        Usr_mail::makeBodyGroup1($obj, $arrbody);
        $obj->formdata["group1"] = $tmp;
    }

    // 編集時
    function makeDiffBodyGroup1($obj, &$arrbody, $exec_type){
        // 編集時はメール非表示も表示する
        foreach($obj->arrItemData[1] as $item_id => &$arrItemData){
            if($arrItemData['disp'] == 1) continue;
            $arrItemData['item_mail'] = 0;
        }

        $tmp = $obj->formdata["group1"];
        $obj->formdata["group1"] = '編集内容';
        Usr_mail::makeDiffBodyGroup1($obj, $arrbody, $exec_type);
        $obj->formdata["group1"] = $tmp;
    }

    /*  編集用URLのお知らせ */
    function makeEditBody($obj, $user_id="", $passwd=""){ return ''; }

    function mailfunc74($obj, $key, $name) { return ''; }   // 予定人数
    function mailfunc69($obj, $key, $name) { return ''; }   // 職種#その他
    function mailfunc75($obj, $key, $name) { return ''; }   // 言語#その他

    function mailfunc63($obj, $key, $name, $i=null) {
        $group = 1;

        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $item = trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]);
                    $str .= $item;
                    if($item == '参加'){
                        // その他テキスト
                        $name = $obj->arrItemData[$group][74]['item_name'];
                        $memo = $obj->arrItemData[$group][74]['item_memo'];
                        $str .= ' '.$name.' '.$obj->mailfuncNiniBody($group, 74, $i).$memo;
                    }
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
                
                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->arrItemData[$group][$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
 
        $str.= "\n";
        return $str;
    }

    function mailfunc64($obj, $key, $name, $i=null) {
        $group = 1;

        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = '';
//        $str  = "\n【その他情報】\n\n";
        $str .= $obj->point_mark.$name.": ";
        $str .= Usr_Assign::nini($obj, $group, $key, $obj->arrForm["edata".$key]);
        // チェックボックス
        if($obj->arrItemData[$group][$key]["item_type"] == 3) {
            foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                if(!isset($obj->arrForm[$wk_key.$n])) continue;
                if($obj->arrForm[$wk_key.$n] == $n) {
                    // その他テキスト
                    if($obj->arrItemData[$group][$key]["select"][$n] == 'その他'){
                        $str .= ' '.$obj->mailfuncNiniBody($group, 69, $i);
                    }
                }
            }
        }
        $str.= "\n";
        return $str;
    }


    function mailfunc73($obj, $key, $name, $i=null) {
        $group = 1;

        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = '';
//        $str  = "\n【その他情報】\n\n";
        $str .= $obj->point_mark.$name.": ";
        $str .= Usr_Assign::nini($obj, $group, $key, $obj->arrForm["edata".$key]);
        // チェックボックス
        if($obj->arrItemData[$group][$key]["item_type"] == 3) {
            foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                if(!isset($obj->arrForm[$wk_key.$n])) continue;
                if($obj->arrForm[$wk_key.$n] == $n) {
                    // その他テキスト
                    if($obj->arrItemData[$group][$key]["select"][$n] == 'その他'){
                        $str .= ' '.$obj->mailfuncNiniBody($group, 75, $i);
                    }
                }
            }
        }
        $str.= "\n";
        return $str;
    }


    // ------------------------------------------------------
    // ▽ログインページカスタマイズ
    // ------------------------------------------------------

    function usr_login_premain($obj){
        $obj->assign('login_msg', '');


        $GLOBALS["form"]->formWord['word14'] = 'セラー登録確認画面';
        $obj->assign("formWord", $GLOBALS["form"]->formWord);
    }

    function usr_login_defaultAction($obj){
        $obj->arrErr = $GLOBALS["session"]->getVar("error");
        $obj->assign("msg", '事務局からの承認メールに記載されている、<br/>ID、パスワードを入力して、ログインしてください。');
    }

    /* 受付期間のチェック */
    function usr_login_checkValidDate($obj){
        $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
        $reception_date3 = $GLOBALS["form"]->formData['reception_date3'];

        $GLOBALS["form"]->formData['reception_date2'] = $reception_date3;
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date2;

        // 全体のクローズかチェック
    	$entry_status = $GLOBALS["form"]->checkValidDateSub();

	   	switch($entry_status) {
	   	    // 受付期間前
			case "1":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_start"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date1"])));
				if($GLOBALS["form"]->formWord["word26"] != ""){
					$ws_str = $GLOBALS["form"]->formWord["word26"];
				}
				Error::showErrorPage($ws_str);
				break;

            // 受付期間終了 # 完全にクローズ
			case "2":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_end"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date2"])));

				if($GLOBALS["form"]->formWord["word27"] != ""){
					$ws_str = $GLOBALS["form"]->formWord["word27"];
				}
				Error::showErrorPage("受付期間終了いたしました。");
				break;

            // 審査期間中
			case "3":
				Error::showErrorPage(sprintf($GLOBALS["msg"]["validDate_middle"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["secondary_reception_date"]))));
				break;
			default:
				break;
		}

        $GLOBALS["form"]->formData['reception_date2'] = $reception_date2;
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date3;


		if(!$GLOBALS["form"]->checkPropriety()) {
			Error::showErrorPage($GLOBALS["msg"]["err_login_status"]);
		}

		return;
    }


    /* ログイン処理 */
    function usr_login_loginAction($obj){
        $obj->arrErr = $obj->_check();
        if(count($obj->arrErr) > 0) return;

        // ログイン処理
        $err = LoginEntry::doLogin($GLOBALS["form"]->formData["lang"], $_REQUEST["login_id"], $_REQUEST["password"]);

        // エラーの場合
        if(!empty($err)) {
            $obj->arrErr[] = $err;
            $GLOBALS["log"]->write_log("euser_login failed", $_REQUEST["login_id"]);
            return;
        }


        // 編集可能期間内かチェック
        // 編集可能期間：受付開始～5/1まで
        $date1 = strtotime($GLOBALS["form"]->formData['reception_date1']);
        $date2 = strtotime($this->edit_date);
        $now   = strtotime(date("Y-m-d H:i:s"));
        $isCanEdit = ($date1 <= $now && $now < $date2);

        // 編集期間内
        if($isCanEdit){
            $GLOBALS["log"]->write_log("euser_login Success", $GLOBALS["entryData"]["eid"]);

            $url = "./form/Usr_entry.php?form_id=".$GLOBALS["form"]->formData["form_id"];
            header("location: ".$url);
            exit;
        }

        // 編集期間外かチェック
        // 編集期間外：5/1 ～ 全体クローズ
        $date3 = strtotime($GLOBALS["form"]->formData['reception_date3']);
        $isCanEditOut = ($now < $date3);

        // 編集期間外
        if($isCanEditOut){
            // 受付期間外は登録内容の確認ページへ遷移
            $GLOBALS["session"]->setVar("start", "1");
            $GLOBALS["log"]->write_log("euser_login 18term_out", $GLOBALS["entryData"]["eid"]);

            $eid     = $GLOBALS["entryData"]['eid'];
            $form_id = $GLOBALS["form"]->formData["form_id"];
            $js = "$('#frm #mode').val('display');";
            $js.= "$('#frm #eid' ).val($eid);";
            $js.= "$('#frm').attr('action', '/reg3/Usr/form/Usr_entry.php?form_id={$form_id}');";
            $js.= "$('#frm').submit();";
            $obj->assign("onload", $js);
            return;
        }

        // 全体クローズ
    }


    /** 初期表示 */
    function displayAction($obj) {
        Usr_pageAction::displayAction($obj);

        $GLOBALS['msg']['cofirm_title'] = '登録内容確認画面';
        $GLOBALS['msg']['cofirm_desc1'] = '<span class="red">登録内容の編集可能期間は終了しました。</span><br/>'
                                         .'今後の内容変更については、本ページ下部に記載されております、国内セラー事務局までお問合せください。<br/>'

                                        . '<br/>'
                                        . '<span style="text-decoration: underline;">＞セラー参加マニュアルダウンロード</span><br/>'
                                        . '<ul style="margin:0;">'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_seller_manual.pdf">セラー参加マニュアル（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_seller_application.xls">エクセル版申込書No.1、No.2（Excel）</a></li>'
                                        . '</ul>'
                                        . '<br/>'
                                        . '<span style="text-decoration: underline;">＞参加バイヤー情報ダウンロード</span><br/>'
                                        . '<p class="m0 red" style="margin-left:15px;">※ ファイルを開く際のパスワード：jaitm</p>'
                                        . '<ul style="margin:0;">'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_India.pdf">インド（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_Indonesia.pdf">インドネシア（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_Malaysia.pdf">マレーシア（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_Philippines.pdf">フィリピン（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_Singapore.pdf">シンガポール（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_Thailand.pdf">タイ（PDF）</a></li>'
                                        . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/18/JATM2014_BuyerList_Vietnam.pdf">ベトナム（PDF）</a></li>'
                                        . '</ul>'
//                                        . '<p class="m0">今後バイヤーが追加になった場合は、第2次アポイント前にリストを更新いたします。</p>'

                                         .'<style>'
                                         .'#confirm_desc3, #confirm_desc4 { display:none; }'
                                         .'</style>';

        $desc = '<span style="color:#000;">このページを閉じる場合は、下記ボタンで[ログアウト]をしてから閉じてください</span>'
               .'<div class="btn"><input type="button" value="ログアウト" onclick="f_logout();"></div>';

        $GLOBALS['msg']['cofirm_desc2']  = '<br/>'.$desc;
        $GLOBALS['msg']['display_desc1'] = '<div class="btn"><div>'.$desc.'</div></div>';
    }


    /** 応募期間チェック */
    function _chkPeriod($obj) {
        if(isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'display') return;

    	$entry_status = $GLOBALS["form"]->checkValidDateSub();

	   	switch($entry_status) {
	   	    // 受付期間前
			case "1":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_start"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date1"])));
				if($GLOBALS["form"]->formWord["word26"] != ""){
					$ws_str = $GLOBALS["form"]->formWord["word26"];
				}
				Error::showErrorPage($ws_str);
				break;

	   	    // 受付期間終了
			case "2":
			    // ログインしていたら受付終了期間でもok
                if(isset($GLOBALS["entryData"]["eid"])){
                    $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
                    $reception_date3 = $GLOBALS["form"]->formData['reception_date3'];

                    $GLOBALS["form"]->formData['reception_date2'] = $reception_date3;
                    $GLOBALS["form"]->formData['reception_date3'] = $reception_date2;
                    $obj->_chkPeriod();

                    $GLOBALS["form"]->formData['reception_date2'] = $reception_date2;
                    $GLOBALS["form"]->formData['reception_date3'] = $reception_date3;
                    return;
                }

				$ws_str = sprintf($GLOBALS["msg"]["validDate_end"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date2"])));
				if($GLOBALS["form"]->formWord["word27"] != ""){
					$ws_str = $GLOBALS["form"]->formWord["word27"];
				}
				Error::showErrorPage($ws_str);
				break;

	   	    // 審査機関中
			case "3":
				Error::showErrorPage(sprintf($GLOBALS["msg"]["validDate_middle"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["secondary_reception_date"]))));
				break;

			default:
				break;
		}

		return;
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    function csvfunc26($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }
    function csvfunc64($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }
    function csvfunc70($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }
    function csvfunc73($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }

}
