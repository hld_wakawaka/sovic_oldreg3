<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author kawauchi
 *        
 */

class Usr_Entry75 {


    /***************************
     * カスタマイズメモ
     * 必要な部分コピーして利用する
     ***************************/

   /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj) {
       
        // form_idの取得
        $form_id = $obj->o_form->formData ['form_id'];
       
        // jQueryを読み込む
        $obj->useJquery = true;
       
       
   }

//    /* デバッグ用 */
   function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
   }
   /**
    * ProcessBase::mainの前処理
    */
   function premain($obj) {
      // Assignを直前で変更する場合や特定のエラーチェックで利用
//       var_dump($GLOBALS['_SESSION']['sovic_reg3_local']['form_param1'][0]['amount']);

      // Feeで「Student：Domestic/Overseas」を選んだ場合に表示
      $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
      if(in_array($arrForm1['amount'], array(2,3))) {
         $obj->arrItemData[3][68]['disp'] = "";
      }else{
         $obj->arrItemData[3][68]['disp'] = "1";
      }
   }
   
   // エラーチェック # ブロック1
   function _check1($obj) {
      Usr_Check::_check1($obj);
       
       $group_id = 1;
       
       // Fee
       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
       
       // [reg3-form75]:<見積No1-3>:項目Postionは必須設定なので未入力のチェックを行う
       $item_id = 26;
       $key = "edata" . $item_id;
       $selectValues = array(
           "A" => array(1,2,3,4,5,6,7,8,9),
           "B" => array(10,11,12),
           "C" => array(13)
       );
       
       $feeValues = array(
           "A" => array(0,1),
           "B" => array(2,3),
           "C" => array(0,1,2,3)
       );
       
       // [reg3-form75]:<見積No1-3>:項目Postionの内、選んだ選択肢に応じてエラーチェックを受ける。
       if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] > 0) {
           foreach($selectValues as $_key => $selectValue) {
               
               if (in_array($obj->arrParam [$key], $selectValue)) {
                   $key = "amount";
                   if ($obj->arrParam [$key] === "" || !in_array($obj->arrParam [$key], $feeValues[$_key])) {
                       $method = "Please select the appropriate registration fee category to your position.";
                       $obj->objErr->addErr($method, $key);
                   }
               }
           }
       }
       
   }

   // エラーチェック # ブロック3
   function _check3($obj) {
       // No.8 # 必須設定を解除
       $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
       if(!in_array($arrForm1['amount'], array(2,3))) {
           $sep     = '|';
           $item_id = 68;
           $itemCheck = explode($sep, $obj->itemData[$item_id]["item_check"]);
           $nullCheck = array_search(0, $itemCheck);
           if($nullCheck !== false){
               unset($itemCheck[$nullCheck]);
               $obj->itemData[$item_id]["item_check"] = trim(implode($sep, $itemCheck), $sep);
           }
       }

       Usr_Check::_check3($obj);
       
       // [reg3-form75] 必須チェック
       
       
       // [reg3-form75] <見積No4> 項目[1) Are you a presenter ?]をチェックしたら、項目[Submission No.]を入力必須にする
       $group_id = 3;
       $item_id = 54;
       $key = "edata" . $item_id;
       $target = 'Yes - Already submitted.';


       if($obj->arrParam [$key] == 1) {
           $item_id = 55;
           $key = "edata" . $item_id;
           if (Usr_init::isset_ex($obj, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
               $name = Usr_init::getItemInfo($obj, $item_id);
               $method = Usr_init::getItemErrMsg($obj, $item_id);
               $obj->objErr->addErr(sprintf($method, $name), $key);
           }
       }
       
       
    }

   function setFormIni($obj) {
       
       Usr_initial::setFormIni($obj);
       
       // [reg3-form75] <見積No1>Positionの見出しを、それぞれ、Professional,Student,Professional/Studentに分ける
       $obj->item26 = array(1=>"Professional", 2=>"Student", 3=>"Professional/Student");
       
       
       $obj->arrItemData[1][26]['select2'] = array();
       $c = count($obj->arrItemData[1][26]['select']);
       foreach($obj->arrItemData[1][26]['select'] as $_key => $item){
           // Professional
           if($_key <= 9){
               $obj->arrItemData[1][26]['select2'][1][$_key] = $item;
               continue;
           }
           // Student
           if($_key > 12){
               $obj->arrItemData[1][26]['select2'][3][$_key] = $item;
               continue;
           }
           // Professional/Student
           $obj->arrItemData[1][26]['select2'][2][$_key] = $item;
       }
   
       if(isset($obj->_smarty)){
           $obj->assign("item26", $obj->item26);
           $obj->assign("arrItemData", $obj->arrItemData);
       }
       
   }
   
//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     */
   function sortFormIni($obj) {
       
       $arrGroup1 = & $obj->arrItemData [1];
       
       // 入れ替え
       $array = array ();
       foreach ( $arrGroup1 as $key => $data ) {
           
           // [reg3-form75]:<見積No1-2>:項目「Postion」を「Department」の下へ項目移動
           switch ($key) {
               case 13 : // middle name
                   $array [$key] = $data;
                   $array [26] = $arrGroup1 [26];
                   break;
               
               case 26 : // etc1
                   break;
               
               default :
                   $array [$key] = $data;
                   break;
           }
       }
       $arrGroup1 = $array;
       
       
   }
   
   
   // [reg3-form75]:<見積4>グループ３の項目での入力値の表示（メール）
   function mailfunc54($obj, $item_id, $name, $i = null){
       $group = 3;
   
       
       $key = "edata" . $item_id . $i;
       if(! isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
       $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
   
       if($obj->arrForm[$key] == 1){
           $item_id = 55;
           $key     = "edata".$item_id;
           $edata   = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
           $value = $value ." ". $edata;
       }
   
       $str = $obj->point_mark . $name . ": " . $value . "\n";
       return $str;
   }
   
   
   // No.8
   function mailfunc68($obj, $item_id, $name, $i = null){
       if(!in_array($obj->arrForm['amount'], array(2,3))) {
           return '';
       }
       
       $group = 3;
       
       $key = "edata" . $item_id . $i;
       if(! isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
       $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
       $str = $obj->point_mark . $name . ": " . $value . "\n";
       return $str;
   }
   
   
   // [reg3-form75] <見積No4> 「Yes - Already submitted.」をチェックしていたら「Submission No.:」の値もCSV表示
   function csvfunc54($obj, $group, $pa_param, $item_id) {
       
       $wk_body1 = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
               " ",
               ","
       ), true);
       
       $wk_body1 = trim($wk_body1, ",");
       $wk_body2 = "";
       
       $key = "edata".$item_id;
       if($pa_param[$key] == "1"){
           $item_id = 55;
           $wk_body2 = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
               " ",
               ","
           ), true);
       
           $wk_body2 = trim($wk_body2, ",");
       
           if($wk_body2 != "") {
               $wk_body2 = "Submission No.:".$wk_body2;
           }
       }
       
       $wk_body = $wk_body1 ." ". $wk_body2;
       return $wk_body;
       
   }
   

}
