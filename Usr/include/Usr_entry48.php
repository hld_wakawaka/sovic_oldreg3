<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry48 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

//        // jQueryを読み込む
//        $obj->useJquery = true;
//
//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        // お見積りNo.3
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");

        $key = 'ather_price0';
        $accompanying0 = isset($arrForm1[$key]) && strlen($arrForm1[$key]) > 0 ? intval($arrForm1[$key]) : 0;

        $key = 'ather_price1';
        $accompanying1 = isset($arrForm1[$key]) && strlen($arrForm1[$key]) > 0 ? intval($arrForm1[$key]) : 0;

        $accompanying = max(array($accompanying0, $accompanying1));
        
        if(method_exists($obj, "assign")){
            if(empty($accompanying)){
                unset($obj->arrItemData[3]['54']['select'][2]);
                $obj->assign($arrItemData, $obj->arrItemData);
            }
        }
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
       Usr_Check::_check1($obj);

       $group_id = 1;

       // Fee
       $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;

        //-----------------------------------------------
        // 同伴者に関するエラーチェック
        //-----------------------------------------------
        // Fee:AccompanyingPersion
        // 見積もりNo.2
        $key = 'ather_price0';
        $accompanying0 = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;

        $key = 'ather_price1';
        $accompanying1 = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;

        // 複数あるAccompanyingPersonを単一選択に制御
        if($accompanying0 > 0 && $accompanying1){
             $obj->objErr->addErr("“Accompanying Person” and ”Accompanying Person who is younger than 18” cannot select both. ", $key);
             return;
        }

        // 見積もりNo.1-2
        $accompanying = max(array($accompanying0, $accompanying1));

        // Accompanying Persion
        $count = 0;

        // 79 # Family Name
        $item_id = 69;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // Enter Accompanying Person
        // 同伴者1または2のどちらかで「Family Name」の入力があった場合はFee:AccompanyingPersionを必須
        $error_flg = false;
        if($count > 0 && $accompanying == 0){
            $name   = 'Accompanying person';
            $method = 'Select one for %s.';
            $obj->objErr->addErr(sprintf($method, $name), $key);
            $error_flg = true;
        }
        // Fee:AccompanyingPersionの購入があった場合、同伴者入力がないとエラー
        if($count == 0 && $accompanying > 0){
            $name   = 'Accompanying person';
            $method = $GLOBALS["msg"]["err_require_input"];
            $obj->objErr->addErr(sprintf($method, $name), $key);
            $error_flg = true;
        }
        // 「同伴者の入力数」=金額の「Accompanying persion(s)」でないとエラー
        if($count != $accompanying && $accompanying > 0 && !$error_flg){
            $obj->objErr->addErr("Number of accompanying person's name you enter and the number of accompanying person fee you select should be the same.", $key);
        }
    }



//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }
//
//
//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     **/
//    function sortFormIni($obj){
//        $arrGroup1 =& $obj->arrItemData[1];
//
//        // 入れ替え
//        $array = array();
//        foreach($arrGroup1 as $key => $data){
//            switch($key){
//                case 7:     // middle name
//                    $array[$key]= $data;
//                    $array[26]  = $arrGroup1[26];
//                    $array[27]  = $arrGroup1[27];
//                    $array[28]  = $arrGroup1[28];
//                    $array[63]  = $arrGroup1[63];
//                    $array[64]  = $arrGroup1[64];
//                    break;
//
//                case 26:    // etc1
//                case 27:    // etc2
//                case 28:    // etc3
//                case 63:    // etc4
//                case 64:    // etc5
//                    break;
//
//                default:
//                    $array[$key] = $data;
//                    break;
//            }
//        }
//        $arrGroup1 = $array;
//    }
//
//
//
    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $body_header = Usr_mail::makeMailBody_header($obj, $user_id, $passwd, $exec_type);
    
        $tag = "";
        // クレジットカード
        if($obj->arrForm["method"] == 1){
            $tag = "bank";
        }else
            // 銀行振込
            if($obj->arrForm["method"] == 2){
            $tag = "credit";
        }
    
        // タグ除去
        if(strlen($tag) > 0){
            $pattern = sprintf("!<%s.*?>.*?</%s>!ims", $tag, $tag);
            $body_header = preg_replace($pattern, "", $body_header);
        }
        $body_header = strip_tags($body_header);
    
        return $body_header;
    }
    


//
//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
