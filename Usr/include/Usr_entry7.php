<?php

/**
 * reg3form3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.12.09
 * 
 */
class Usr_Entry7 {

    function __construct($obj){
        // 支払方法をクレジット固定
        unset($GLOBALS["method_J"][2]);

        // Mng用の処理
        if(strpos($_SERVER['SCRIPT_NAME'], "Mng/entry/Mng_entry_detail.php") !== false){
            $this->setMoneyPaydata($obj);
        }

        $this->setItem27($obj);
    }


    /** 開発用のデバッグ関数 */
    function developfunc($obj) {
//        print "--------------------<pre style='text-align:left;'>";
//		print_r($obj->makeMailBody(1389, "11", 2));
//		print "</pre><br/><br/>";
    }


    function setMoneyPaydata($obj){
        Usr_initial::setMoneyPaydata($obj);

        // その他決済を2つに分ける
        $obj->wa_ather_price1  = array();
        $obj->wa_ather_price1[0] = $obj->wa_ather_price[0];
        $obj->wa_ather_price1[1] = $obj->wa_ather_price[1];

        $obj->wa_ather_price2  = array();
        $obj->wa_ather_price2[2] = $obj->wa_ather_price[2];
        $obj->wa_ather_price2[4] = $obj->wa_ather_price[4];
        $obj->wa_ather_price2[3] = $obj->wa_ather_price[3];
        $obj->wa_ather_price2[5] = $obj->wa_ather_price[5];

        // ヘッダ
        $obj->wa_ather_header    = array();
        $obj->wa_ather_header[0] = $obj->w_head['price_header'];
        $obj->wa_ather_header[1] = $obj->w_head['price_header'];
        $obj->wa_ather_header[2] = $obj->w_head['ather_price_header'];
        $obj->wa_ather_header[3] = $obj->w_head['ather_price_header'];
        $obj->wa_ather_header[4] = $obj->w_head['ather_price_header'];
        $obj->wa_ather_header[5] = $obj->w_head['ather_price_header'];

        if(method_exists($obj, "assign")){
            $obj->assign("va_ather_header",  $obj->wa_ather_header);
            $obj->assign("va_ather_price1",  $obj->wa_ather_price1);
            $obj->assign("va_ather_price2",  $obj->wa_ather_price2);
        }
    }


    function makePaymentBody($obj, $exec_type){

        if($obj->formdata["lang"] == LANG_JPN){
            $point_mark = "■";
            $body_pay = "\n\n【お支払情報】\n\n";
        }else{
            $point_mark = "*";
            $body_pay = "\n\n[Payment Information]\n\n";
        }

        //更新の場合は、登録済みのデータから引っ張ってくる
        if($exec_type == "2"){
            $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);
        }

        //支払合計
        if($exec_type == "1"){
            $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);

        }else{
            $total = $GLOBALS["session"]->getVar("ss_total_payment");
        }

        //-----------------------------------------
        // 決済情報
        //-----------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){

            //$body_pay .= "\n\n";
            if($obj->formdata["lang"] == LANG_JPN){
                $body_pay .= $point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
                $body_pay .= $point_mark."金額: \n";
            }
            else{
                if($total > 0){
                    $body_pay .= ($obj->arrForm["method"] != "3") 
                                ? $point_mark."Payment:".$obj->wa_method[$obj->arrForm["method"]]."\n"
                                : $point_mark."Payment:Cash on-site\n";
                }
                $body_pay .= $point_mark."Amount of Payment:\n";
            }


            //その他決済がある場合
            if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

                //新規の場合
                if($exec_type == "1"){
                    foreach($obj->wa_ather_price  as $p_key => $data ){
                        if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                        if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
                            $price = $data["p1"];

                            // Free, - => 0
                            if(!is_numeric($data["p1"])){
                                $data["p1"] = 0;
                            }
                            $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                            // タグを除去
                            $header = $obj->wa_ather_header[$p_key];
                            $data["name"] = GeneralFnc::smarty_modifier_del_tags($header.$data["name"]);

                            if($obj->formdata["lang"] == LANG_JPN){
                                if(is_numeric($price)){
                                    $price = $price."円";
                                }
                                $body_pay .="　　　".$data["name"]."：".number_format($sum)."円\n";
                            }
                            else{
                                if(is_numeric($price)){
                                    $price = "\\".$price;
                                }
                                $body_pay .="      ".$data["name"].":\\".number_format($sum)."\n";
                            }
                        }
                    }
                    
                }

                //更新の場合
                if($exec_type == "2"){
                    foreach($wa_payment_detail as $key => $data){
                        if($key == 0) continue;

                        $price = $data["price"];
                        // Free, - => 0
                        if(!is_numeric($data["price"])){
                            $data["price"] = 0;
                        }
                        $sum = $data["price"]*$data["quantity"];

                        // タグを除去
                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = $price."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = "\\".$price;
                            }
                            $body_pay .="      ".$data["name"].":\\".number_format($sum);
                        }

                        // その他決済の場合は内訳を後ろにつける
//                        if($data["type"] == "1"){
//                            if($obj->formdata["lang"] == LANG_JPN){
//                                $body_pay .="　（".$price." ×".$data["quantity"]."）";
//                            }
//                            else{
//                                $body_pay .="  （".$price." ×".$data["quantity"]."）";
//                            }
//                        }
                        $body_pay .= "\n";
                    }
                }
            }


            if($obj->formdata["lang"] == LANG_JPN){
                $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
            }
            else{
                $body_pay .="      Amount of Total Payment:\\".number_format($total)."\n\n";
            }


            // お支払情報のメール生成
            $body_pay .= $obj->makePaymentMethodBody($total);
        }
        return $body_pay;
    }



    /**
     * CSVヘッダ生成
     */
    function payment_payMakeHeader($obj, $all_flg=false){
        // その他決済の見出し
        $obj->wa_ather_header    = array();
        $obj->wa_ather_header[0] = $obj->o_form->formData['price_header'];
        $obj->wa_ather_header[1] = $obj->o_form->formData['price_header'];
        $obj->wa_ather_header[2] = $obj->o_form->formData['ather_price_header'];
        $obj->wa_ather_header[3] = $obj->o_form->formData['ather_price_header'];
        $obj->wa_ather_header[4] = $obj->o_form->formData['ather_price_header'];
        $obj->wa_ather_header[5] = $obj->o_form->formData['ather_price_header'];


        $header   = array();
        // payment_csvのみ出力
        if(!$all_flg){
            $header[] = "応募者氏名";
            $header[] = "応募者氏名（カナ）";
        }
        $header[] = "取引ID";
        $header[] = "お支払い方法";
        $header[] = "ステータス";

        $header[] = "支払期限";
        $header[] = "クレジット番号";
        $header[] = "カード会社";
        $header[] = "カード名義人";
        $header[] = "有効期限";
        $header[] = "クレジット支払い区分";
        $header[] = "クレジット支払い回数";
//        $header[] = "セキュリティコード";

        $header[] = "お振込み人名義";
        $header[] = "お支払銀行名";
        $header[] = "お振込み日";

        $header[] = "コンビニ　ショップコード";
        $header[] = "姓";
        $header[] = "名";
        $header[] = "セイ";
        $header[] = "メイ";
        $header[] = "電話番号";
        $header[] = "メールアドレス";
        $header[] = "郵便番号";
        $header[] = "住所";
        $header[] = "成約日";
        $header[] = "決済機関コード";
        //$header[] = "支払詳細";


        //------------------------------
        //決済項目
        //------------------------------
        $sort = array(0,1,2,4,3,5);
        $array = array();
        foreach($sort as $_key => $key){
            $array[$key] = $obj->wa_ather_price[$key];
        }
        $obj->wa_ather_price = $array;

        foreach($obj->wa_ather_price as $_key => $data){
            if($_key == -1) continue;

            $head = GeneralFnc::smarty_modifier_del_tags($obj->wa_ather_header[$_key].$data);
            $header[] = strip_tags(str_replace(array("_"), " ",$head));
        }

        $header[] = "合計金額";
        $header[] = "登録日";
        $header[] = "更新日";


        //生成
        $ret_buff  = implode($obj->delimiter, $header);
        $ret_buff .="\n";

        return $ret_buff;
    }



    // 金額、その他決済項目のCSV整形
    function payment_pay_c($obj, $pa_detail){
        $sort = array(0,1,2,4,3,5);
        $array = array();
        foreach($sort as $_key => $key){
            $array[$key] = $obj->wa_ather_price[$key];
        }
        $obj->wa_ather_price = $array;

        foreach($obj->wa_ather_price as $key => $ather_name){
            $quantity = "";
            $match_flg = "";

            if($key == -1) continue;

            foreach($pa_detail as $data){
                $from = "payment";
                $column = "csv_price_head0, csv_ather_price_head0";
                $where = "eid = ".$obj->db->quote($data['eid'])."";
                $csv_header =  $obj->db->getData($column, $from, $where, __FILE__, __LINE__);

                // 金額
                if($data['type'] == 0){
                    if(($ather_name == $data["name"]) || str_replace(array("_"), " ",$ather_name) == $data["name"]){
                        $quantity = $data["quantity"];
                        $match_flg = "1";
                        break 1;
                    }
                // その他決済
                }elseif($key== $data["key"]){
                    $quantity = $data["quantity"];
                    $match_flg = "1";
                    break 1;
                }
            }

            if($match_flg == "1"){
                $quantity = GeneralFnc::smarty_modifier_del_tags($quantity);
                $wk_buff[] = $quantity;
            }
            else{
                $wk_buff[] = "";
            }
        }
        return $wk_buff;
    }


    /**
     * 任意
     */
    function mailfunc55($obj, $key, $name, $i=null) {
        
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.":\n";
        
        //error_log("\n".$key." ".print_r($obj->itemData[$key], true), 3, LOG_DIR."test.log");

        $arrKey = array(56, 67, 68, 99, 100, 101);
        foreach($arrKey as $_key => $key){
            $wk_key = "edata".$key.$i;
            $item_name = strip_tags($obj->itemData[$key]["item_name"]);
            switch($obj->itemData[$key]["item_type"]) {
                case "2":
                case "4":
                    // ラジオ、セレクト
                    if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                        $str .= "　".$item_name.":".$obj->itemData[$key]["select"][$obj->arrForm[$wk_key]]."\n";
                    }
                    break;
            }
        }

        return $str;
    }


    /**
     * 任意
     */
    function  mailfunc56($obj, $key, $name, $i=null) { return ""; }
    function  mailfunc67($obj, $key, $name, $i=null) { return ""; }
    function  mailfunc68($obj, $key, $name, $i=null) { return ""; }
    function  mailfunc99($obj, $key, $name, $i=null) { return ""; }
    function mailfunc100($obj, $key, $name, $i=null) { return ""; }
    function mailfunc101($obj, $key, $name, $i=null) { return ""; }



    /**
     * CSVヘッダ生成
     */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){
        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = "\"".$name."\"";
        }

        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $loop_cnt = $obj->loop +1;

            for($i=1; $i < $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    // 画面に表示する項目の名称
                    $name = strip_tags($_data["item_name"]);

                    $prefix = "\"共著者".$i." ";

                    $item_id = $_data["item_id"];
                    switch($item_id){
                            case 32:
                                $groupHeader[$group][] =  $prefix.$name."\"";
                                break;

                            // 共著者　姓名
                            case 33:
                                $groupHeader[$group][] =  $prefix."姓名"."\"";
                                break;

                            // 共著者　姓名カナ
                            case 35:
                                $groupHeader[$group][] =  $prefix."姓名（カナ）"."\"";
                                break;

                            case 34:
                            case 36:
                                break;

                            default:
                                $groupHeader[$group][] =  $prefix.$name."\"";
                                break;
                    }
                }
            }
        }

        $group = 3;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            if($_data['item_id'] == 55){
                $name55 = $name;
                continue;
            }
            if(in_array($_data['item_id'], array(56, 67, 68, 99, 100, 101))){
                $name = $name55.$name;
            }

            $groupHeader[$group][] = "\"".$name."\"";
        }

        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[]="状態";
        $header[]="エントリー登録日";
        $header[]="エントリー更新日";

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }




    /**
     * CSV出力データ生成
     */
    function entry_csv_entryMakeData($obj, $pa_param, $all_flg=false){
        foreach($pa_param as $key=>$data) {
            $pa_param[$key] = str_replace('"', '""', $data);
        }


        // グループ別に生成
        $groupBody = array("1" => array(), "2" => array(), "3" => array());

        $group = 1;
        $groupBody[$group][] = $pa_param["entry_no"];
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            // 任意項目
            if($_data["controltype"] == "1"){
                $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), false);
                $wk_body = "\"".trim($wk_body, ",")."\"";

            // 標準項目
            }else{
                switch($item_id){
                    //会員・非会員
                    case 8:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? "\"".$obj->wa_kaiin[$pa_param["edata".$item_id]]."\"" : "\"\"";
                        break;
                    //連絡先
                    case 16:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? "\"".$obj->wa_contact[$pa_param["edata".$item_id]]."\"" : "\"\"";
                        break;
                    //都道府県
                    case 18:
                        //英語フォームの場合
                        if($GLOBALS["userData"]["lang"] == "2"){
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? "\"".$pa_param["edata".$item_id]."\"" : "\"\"";
                        }
                        else{
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? "\"".$GLOBALS["prefectureList"][$pa_param["edata".$item_id]]."\"" : "\"\"";
                        }
                        break;
                    //共著者の有無
                    case 29:
                        $wk_body =  ($pa_param["edata".$item_id] != "") ? "\"".$obj->wa_coAuthor[$pa_param["edata".$item_id]]."\"" : "";
                        break;

                    //共著者の所属機関
                    case 31:
                        $wk_kikan_names = array();
                        $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);

                        $i =  "";
                        $set_select = array();
                        foreach($wk_kikanlist as $num => $val){
                            $i = $num+1;
                            $set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
                            $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
                        }
                        $wk_body = "\"". str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names)). "\"";
                        break;

                    //Mr. Mis Dr
                    case 57:
                        if($pa_param["edata".$item_id] != ""){
                            $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
                        }
                        else{
                            $wk_body = "";
                        }
                        break;
                    //国名リストボックス
                    case 114:
                        $wk_body =  ($pa_param["edata".$item_id] != "") ? "\"".$GLOBALS["country"][$pa_param["edata".$item_id]]."\"" : "\"\"";
                        break;
                    default:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? "\"".$pa_param["edata".$item_id]."\"" : "";
                        break;
                }
            }
            $groupBody[$group][] = $wk_body;
        }


        $group = 3;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];
            if($item_id == 55) continue;

            // 任意項目
            if($_data["controltype"] == "1"){
                $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), false);
                $wk_body = "\"".trim($wk_body, ",")."\"";

            // 標準項目
            }else{
                switch($item_id){
                    //発表形式
                    case 48:
                        $keisiki = "";
                        if($pa_param["edata".$item_id] != ""){
                            if($obj->itemData[$item_id]["select"]){
                                $keisiki = "\"".$obj->itemData[$item_id]["select"][$pa_param["edata".$item_id]]."\"";
                            }
                        }
                        $wk_body = str_replace(array("\r\n","\n","\r"), '', $keisiki);
                        break;

                    default:
                        $wk_body = ($pa_param["edata".$item_id] != "") ? "\"".$pa_param["edata".$item_id]."\"" : "";
                        break;
                }
            }
            $groupBody[$group][] = $wk_body;
        }


        $group = 2;
        // 共著者の数だけ、共著者ヘッダを生成する
        if($obj->itemData[29]["item_view"] == "0"){
            $cnt      = count($pa_param["chosya"]);
            $loop_cnt = $obj->loop +1;

            for($i=1; $i < $loop_cnt; $i++ ){
                foreach($obj->arrItemData[$group] as $_key => $_data){
                    // 表示する設定の場合出力
                    if($_data["item_view"] == "1") continue;

                    $item_id = $_data["item_id"];

                    // 共著者の員数を越えた場合はNULL埋め
                    if($cnt < $i){
                        if($item_id == "34") continue;
                        if($item_id == "36") continue;

                        $groupBody[$group][] = "\"\"";
                        continue;
                    }

                    $wk_body = "";
                    $chosya  = $pa_param["chosya"][$i-1];

                    // 任意項目
                    if($_data["controltype"] == "1"){
                        $wk_body = $obj->_makeNini($obj, $group, $chosya["edata".$item_id], $item_id);

                    // 標準項目
                    }else{
                        switch($item_id){
                            //共著者所属期間
                            case 32:
                                if(count($set_select) > 0){
                                    if($chosya["edata".$item_id] != ""){
                                        $chkeck = explode("|", $chosya["edata".$item_id]);
                                        $chkeck_names = array();

                                        foreach($chkeck as $val){
                                            if($val != "" && array_key_exists($val, $set_select)){
                                                $chkeck_names[] = str_replace(array("\n\r","\r","\n"), "", $set_select[$val]);
                                            }
                                        }

                                        if(count($chkeck_names) > 0){
                                            $wk_aaa =   "\"". implode(",", $chkeck_names). "\"";
                                        }

                                    }
                                    else{
                                        $wk_aaa = "";
                                    }

                                    $wk_body = str_replace(array("\r\n","\n","\r"), '', trim($wk_aaa));
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            //共著者姓名
                            case 33:
                                $cyosya_name = "";
                                $wk_mark = array();
                                if($chosya["edata32"] != ""){
                                    $chkeck = explode("|", $chosya["edata32"]);
                                    foreach($chkeck as $val){
                                        $wk_mark[] = $val.")";
                                    }
                                    $cyosya_name = implode(", ", $wk_mark);

                                }
                                $cyosya_name .= $chosya["edata".$item_id]." ".$chosya["edata34"];
                                $wk_body = "\"".$cyosya_name. "\"";
                                break;

                            //共著者姓名（カナ）
                            case 35:
                                $wk_body = "\"".$chosya["edata".$item_id]." ".$chosya["edata36"]. "\"";
                                break;

                            case 34:
                            case 36:
                                break;


                            //会員・非会員
                            case 41:
                                $wk_body = ($chosya["edata".$item_id] != "") ? "\"".$obj->wa_kaiin[$chosya["edata".$item_id]]."\"" : "\"\"";
                                break;

                            //Mr. Mis Dr
                            case 61:
                                if($chosya["edata".$item_id] != ""){
                                    $wk_body = $GLOBALS["titleList"][$chosya["edata".$item_id]];
                                }
                                else{
                                    $wk_body = "";
                                }
                                break;

                            case 46:
                            case 47:
                            default:
                                $wk_body = "\"".$chosya["edata".$item_id]. "\"";
                                break;
                        }
                    }
                    if(strlen($wk_body) == 0) continue;

                    $groupBody[$group][] = $wk_body;
                }
            }
        }


        $body = $groupBody[1];
        if(count($groupBody[3]) > 0) $body = array_merge($body, $groupBody[3]);
        if(count($groupBody[2]) > 0 && !$all_flg) $body = array_merge($body, $groupBody[2]);
        //状態
        $body[] = ($pa_param["status"] == "0") ? "-" : "\"".$GLOBALS["entryStatusList"][$pa_param["status"]]."\"";
        $body[] = "\"".$pa_param["insday"]."\"";    // 登録日
        $body[] = "\"".$pa_param["upday"]."\"";     // 更新日

        $ret_buff .= implode($obj->delimiter, $body);
        $ret_buff .= "\n";

        return $ret_buff;
    }



    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        Usr_Check::_check1($obj);

        $cnt = 0;
        $ttl = 0;
        foreach($obj->arrParam as $_key => $_value){
            if(preg_match("/^ather_price([0-9+])$/u", $_key, $match) !== 1) continue;
            if($_value != 1) continue;

            $cnt++;
            $ttl += $match[1];
        }
        // 選択数に応じてエラーを分岐
        switch($cnt){
            case 4:
            case 3:
                $obj->objErr->addErr("選択した参加区分の組み合わせが正しくありません。", "ather_price0");
                break;

            case 2:
            case 1:
                // 選択パターンチェック
                if($cnt == 2 && $ttl %2 == 1){
                    $obj->objErr->addErr("選択した参加区分の組み合わせが正しくありません。", "ather_price0");
                    break;
                }

                // ISACA会員を選択したら国際会員番号,支部コードを必須
                if($obj->arrParam["ather_price0"] == 1 || $obj->arrParam["ather_price2"] == 1 || $obj->arrParam["ather_price4"] == 1){
                    $rEdata = array(26, 27);
                    foreach($rEdata as $_key){
                        $key  = "edata".$_key;
                        $name = Usr_init::getItemInfo($obj, $_key);

                        if(!$obj->objErr->isNull($obj->arrParam[$key])) {
                            $method = Usr_init::getItemErrMsg($obj, $_key);
                            $obj->objErr->addErr(sprintf($method, $name), $key);
                        }
                    }

                    // 国際会員番号の半角英数6桁チェック
                    $edata = "edata26";
                    if(isset($obj->arrParam[$edata]) && strlen($obj->arrParam[$edata]) > 0 && !isset($obj->objErr->_err[$edata])){
                        if(strlen($obj->arrParam[$edata]) != 6){
                            $name = Usr_init::getItemInfo($obj, 26);
                            $obj->objErr->addErr(sprintf("%sは、6桁で入力してください。", $name), $edata26);
                        }
                    }

                // 一般参加を選択したら国際会員番号,支部コードは入力不可
                }else{
                    $rEdata = array(26, 27);
                    foreach($rEdata as $_key){
                        $key  = "edata".$_key;
                        $name = Usr_init::getItemInfo($obj, $_key);

                        if($obj->objErr->isNull($obj->arrParam[$key])) {
                            $item_type = Usr_init::getItemInfo($obj, $_key, "item_type");
                            switch($item_type){
                                case Usr_init::INPUT_TEXT:
                                case Usr_init::INPUT_AREA:
                                    $method = "%sは入力しないでください。";
                                    break;
                                case Usr_init::INPUT_RADIO:
                                case Usr_init::INPUT_CHECK:
                                case Usr_init::INPUT_SELECT:
                                    $method = "%sは選択しないでください。";
                                    break;
                            }
                            $obj->objErr->addErr(sprintf("一般参加の方は".$method, $name), $key);
                        }
                    }
                }
                break;
        }
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check3($obj){
        Usr_Check::_check3($obj);

        $isErr = true;
        $arrKey = array(56, 67, 68, 99, 100, 101);
        foreach($arrKey as $_key => $key){
            $wk_key = "edata".$key;
            if(strlen($obj->arrParam[$wk_key]) > 0){
                $isErr = false;
                break;
            }
        }
        if($isErr){
            $name = Usr_init::getItemInfo($obj, 55);
            $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_require_select"], $name), "edata55");
        }
    }


    // 項目マスタ
    function setFormIni($obj) {
        Usr_initial::setFormIni($obj);

        $obj->arrItemData[1][27]['select'] = $obj->opt1+$obj->opt2+$obj->opt3+$obj->opt4_1+$obj->opt4_2+$obj->opt4_3+$obj->opt4_4+$obj->opt4_5+$obj->opt4_6+$obj->opt4_7+$obj->opt5;
    }


    function setItem27($obj){
        $obj->opt1 = array(
              "1" => "Japan-Tokyo : Tokyo Chapter (089)"
             ,"2" => "Japan-Osaka : Osaka Chapter (103)"
             ,"3" => "Japan-Nagoya : Nagoya Chapter (118)"
             ,"4" => "Japan-Fukuoka : Fukuoka Chapter (219)"
             ,"5" => "Bahrain : Bahrain Chapter (208)"
             ,"6" => "Bangladesh-Dhaka : Dhaka Chapter (207)"
             ,"7" => "China Hong Kong : China Hong Kong Chapter (064)"
             ,"8" => "India-Bangalore : Bangalore Chapter (138)"
             ,"9" => "India-Chennai : Chennai Chapter (099)"
            ,"10" => "India-Cochin : Cochin Chapter (176)"
            ,"11" => "India-Coimbatore : Coimbatore Chapter (155)"
            ,"12" => "India-Hyderabad : Hyderabad Chapter (164)"
            ,"13" => "India-Kolkata : Kolkata Chapter (165)"
            ,"14" => "India-Mumbai : Mumbai Chapter (145)"
            ,"15" => "India-New Delhi : New Delhi Chapter (140)"
            ,"16" => "India-Pune : Pune Chapter (159)"
            ,"17" => "India-Vijayawada : Vijayawada Chapter (200)"
            ,"18" => "Indonesia : Indonesia Chapter (123)"
            ,"19" => "Korea : Korea Chapter (107)"
            ,"20" => "Lebanon : Lebanon Chapter (181)"
            ,"21" => "Macao : Macao Chapter (190)"
            ,"22" => "Malaysia : Malaysia Chapter (093)"
            ,"23" => "Oman-Muscat : Muscat Chapter (168)"
            ,"24" => "Pakistan-Islamabad : Islamabad Chapter (224)"
            ,"25" => "Pakistan-Karachi : Karachi Chapter (148)"
            ,"26" => "Pakistan-Lahore : Lahore Chapter (196)"
            ,"27" => "Philippines-Manila : Manila Chapter (136)"
            ,"28" => "Saudi Arabia-Jeddah : Jeddah Chapter (163)"
            ,"29" => "Saudi Arabia-Riyadh : Riyadh Chapter (154)"
            ,"30" => "Singapore : Singapore Chapter (070)"
            ,"31" => "Sri Lanka : Sri Lanka Chapter (141)"
            ,"32" => "Taiwan : Taiwan Chapter (142)"
            ,"33" => "Thailand-Bangkok : Bangkok Chapter (109)"
            ,"34" => "UAE : UAE Chapter (150)"
        );

        $obj->opt2 = array(
             "35" => "Argentina-Buenos Aires : Buenos Aires Chapter (124)"
            ,"36" => "Argentina-Mendoza : Mendoza Chapter (144)"
            ,"37" => "Bolivia-LaPaz : LaPaz Chapter (173)"
            ,"38" => "Brazil-Brasília : Brasília Chapter (202)"
            ,"39" => "Brazil-Rio De Janeiro : Rio De Janeiro Chapter (203)"
            ,"40" => "Brazil-São Paulo : São Paulo Chapter (166)"
            ,"41" => "Chile-Santiago : Santiago Chapter (135)"
            ,"42" => "Colombia-Bogotá : Bogotá Chapter (126)"
            ,"43" => "Costa Rica-San José : San José Chapter (031)"
            ,"44" => "Dominican Republic-Santo Domingo : Santo Domingo Chapter (226)"
            ,"45" => "Ecuador-Quito : Quito Chapter (179)"
            ,"46" => "Guatemala-Guatemala City : Guatemala City Chapter (215)"
            ,"47" => "México-Guadalajara : Guadalajara Chapter (201)"
            ,"48" => "México-Mexico City : Mexico City Chapter (014)"
            ,"49" => "México-Monterrey : Monterrey Chapter (080)"
            ,"50" => "Panamá : Panamá Chapter (094)"
            ,"51" => "Paraguay-Asunción : Asunción Chapter (184)"
            ,"52" => "Perú-Lima : Lima Chapter (146)"
            ,"53" => "Puerto Rico : Puerto Rico Chapter (086)"
            ,"54" => "Uruguay-Montevideo : Montevideo Chapter (133)"
            ,"55" => "Venezuela : Venezuela Chapter (113)"
        );

        $obj->opt3 = array(
              "56" => "Austria : Austria Chapter (157)"
             ,"57" => "Belgium : Belgium Chapter (143)"
             ,"58" => "Bulgaria-Sofia : Sofia Chapter (189)"
             ,"59" => "Croatia : Croatia Chapter (170)"
             ,"60" => "Cyprus : Cyprus Chapter (210)"
             ,"61" => "Czech Republic : Czech Republic Chapter (153)"
             ,"62" => "Denmark : Denmark Chapter (096)"
             ,"63" => "Estonia : Estonia Chapter (162)"
             ,"64" => "Finland : Finland Chapter (115)"
             ,"65" => "France (Paris) : France (Paris) Chapter (075)"
             ,"66" => "Germany : Germany Chapter (104)"
             ,"67" => "Ghana-Accra : Accra Chapter (205)"
             ,"68" => "Greece-Athens : Athens Chapter (134)"
             ,"69" => "Hungary-Budapest : Budapest Chapter (125)"
             ,"70" => "Ireland : Ireland Chapter (156)"
             ,"71" => "Israel-Tel-Aviv : Tel-Aviv Chapter (040)"
             ,"72" => "Italy-Milan : Milan Chapter (043)"
             ,"73" => "Italy-Rome : Rome Chapter (178)"
             ,"74" => "Italy-Venice : Venice Chapter (216)"
             ,"75" => "Kenya : Kenya Chapter (158)"
             ,"76" => "Latvia : Latvia Chapter (139)"
             ,"77" => "Lithuania : Lithuania Chapter (180)"
             ,"78" => "Luxembourg : Luxembourg Chapter (198)"
             ,"79" => "Malta : Malta Chapter (186)"
             ,"80" => "Mauritius : Mauritius Chapter (211)"
             ,"81" => "Netherlands : Netherlands Chapter (097)"
             ,"82" => "Nigeria-Abuja : Abuja Chapter (185)"
             ,"83" => "Nigeria-Ibadan : Ibadan Chapter (222)"
             ,"84" => "Nigeria-Lagos : Lagos Chapter (149)"
             ,"85" => "Norway : Norway Chapter (074)"
             ,"86" => "Poland-Katowice : Katowice Chapter (220)"
             ,"87" => "Poland-Warsaw : Warsaw Chapter (218)"
             ,"88" => "Portugal-Lisbon : Lisbon Chapter (209)"
             ,"89" => "Romania : Romania Chapter (172)"
             ,"90" => "Russia-Moscow : Moscow Chapter (167)"
             ,"91" => "Slovak  Republic : Slovak  Republic Chapter (160)"
             ,"92" => "Slovenia : Slovenia Chapter (137)"
             ,"93" => "South Republic Africa : South Republic Africa Chapter (130)"
             ,"94" => "Spain-Barcelona : Barcelona Chapter (171)"
             ,"95" => "Spain-Madrid : Madrid Chapter (183)"
             ,"96" => "Spain-Valencia : Valencia Chapter (182)"
             ,"97" => "Sweden : Sweden Chapter (088)"
             ,"98" => "Switzerland : Switzerland Chapter (116)"
             ,"99" => "Tanzania : Tanzania Chapter (174)"
            ,"100" => "Tunisia-Tunis : Tunis Chapter (225)"
            ,"101" => "Turkey-Ankara : Ankara Chapter (217)"
            ,"102" => "Turkey-Istanbul : Istanbul Chapter (204)"
            ,"103" => "Uganda-Kampala : Kampala Chapter (199)"
            ,"104" => "UK-Central : Central Chapter (132)"
            ,"105" => "UK-London : London Chapter (060)"
            ,"106" => "UK-Northern England : Northern England Chapter (111)"
            ,"107" => "UK-Scotland : Scotland Chapter (175)"
            ,"108" => "UK-Winchester : Winchester Chapter (212)"
            ,"109" => "Ukraine-Kyiv : Kyiv Chapter (206)"
            ,"110" => "Zambia-Lusaka : Lusaka Chapter (223)"
        );


        $obj->opt4_1 = array(
             "111" => "Canada"
            ,"112" => "AB-Calgary : Calgary Chapter (121)"
            ,"113" => "AB-Edmonton : Edmonton Chapter (131)"
            ,"114" => "Atlantic Provinces : Atlantic Provinces Chapter (105)"
            ,"115" => "BC-Vancouver : Vancouver Chapter (025)"
            ,"116" => "BC-Victoria : Victoria Chapter (100)"
            ,"117" => "MB-Winnipeg : Winnipeg Chapter (072)"
            ,"118" => "ON-Ottawa Valley : Ottawa Valley Chapter (032)"
            ,"119" => "ON-Toronto : Toronto Chapter (021)"
            ,"120" => "PQ-Montreal : Montreal Chapter (036)"
            ,"121" => "PQ-Quebec City : Quebec City Chapter (091)"
        );

        $obj->opt4_2 = array(
             "122" => "Bermuda : Bermuda Chapter (147)"
            ,"123" => "Trinidad & Tobago : Trinidad & Tobago Chapter (106)"
        );

        $obj->opt4_3 = array(
             "124" => "IL-Chicago : Chicago Chapter (002)"
            ,"125" => "Central Indiana (Indianapolis) : Central Indiana (Indianapolis)  Chapter (056)"
            ,"126" => "Central Ohio (Columbus) : Central Ohio (Columbus)  Chapter (027)"
            ,"127" => "Illini (Springfield IL) : Illini (Springfield IL)  Chapter (077)"
            ,"128" => "Illowa-Illowa : Illowa  Chapter (169)"
            ,"129" => "Iowa (Des Moines) : Iowa (Des Moines)  Chapter (110)"
            ,"130" => "Kentuckiana (Louisville KY)  : Kentuckiana (Louisville KY)  Chapter (037)"
            ,"131" => "MI-Detroit : Detroit Chapter (008)"
            ,"132" => "Minnesota : Minnesota  Chapter (007)"
            ,"133" => "NE-Omaha-NE : Omaha-NE Chapter (023)"
            ,"134" => "Northwest Ohio : Northwest Ohio  Chapter (188)"
            ,"135" => "Northeast Ohio (Cleveland) : Northeast Ohio (Cleveland) Chapter (026)"
            ,"136" => "OH-Greater Cincinnati : Greater Cincinnati  Chapter (003)"
            ,"137" => "Western Michigan : Western Michigan  Chapter (038)"
            ,"138" => "WI (Milwaukee)-Kettle Moraine : Kettle Moraine  Chapter (057)"
        );


        $obj->opt4_4 = array(
             "139" => "Central Maryland (Baltimore) : Central Maryland (Baltimore) Chapter (024)"
            ,"140" => "Central New York(Syracuse) : Central New York(Syracuse) Chapter (029)"
            ,"141" => "CT-Greater Hartford : Greater Hartford Chapter (028)"
            ,"142" => "DC-National Capital Area : National Capital Area Chapter (005)"
            ,"143" => "New England : New England  Chapter (018)"
            ,"144" => "New Jersey : New Jersey Chapter (030)"
            ,"145" => "New York Metropolitan : New York Metropolitan Chapter (010)"
            ,"146" => "NY (Albany)-Hudson Valley : Hudson Valley Chapter (120)"
            ,"147" => "PA-Harrisburg : Harrisburg Chapter (045)"
            ,"148" => "PA-Philadelphia : Philadelphia Chapter (006)"
            ,"149" => "PA-Pittsburgh : Pittsburgh Chapter (013)"
            ,"150" => "Rhode Island : Rhode Island  Chapter (197)"
            ,"151" => "Western New York (Buffalo/Rochester) : Western New York (Buffalo/Rochester) Chapter (046)"
        );

        $obj->opt4_5 = array(
             "152" => "AL-Birmingham : Birmingham Chapter (065)"
            ,"153" => "Central Florida (Orlando) : Central Florida (Orlando)  Chapter (067)"
            ,"154" => "FL-Jacksonville : Jacksonville Chapter (058)"
            ,"155" => "FL-Tallahassee : Tallahassee Chapter (213)"
            ,"156" => "GA-Atlanta: Atlanta Chapter (039)"
            ,"157" => "Huntsville: Huntsville Chapter (221)"
            ,"158" => "Middle Tennessee (Nashville) : Middle Tennessee (Nashville) Chapter (102)"
            ,"159" => "NC-Charlotte:Charlotte Chapter (051)"
            ,"160" => "Research Triangle (Raleigh, NC) : Research Triangle (Raleigh, NC) Chapter (059)"
            ,"161" => "South Carolina Midlands (Columbia, SC) : South Carolina Midlands (Columbia, SC) Chapter (054)"
            ,"162" => "South Florida : South Florida Chapter (033)"
            ,"163" => "TN-Memphis : Memphis Chapter (048)"
            ,"164" => "Virginia : Virginia  Chapter (022)"
            ,"165" => "West Florida (Tampa) : West Florida (Tampa) Chapter (041)"
        );

        $obj->opt4_6 = array(
             "166" => "Central Arkansas (Little Rock) : Central Arkansas (Little Rock) Chapter (082)"
            ,"167" => "Central Oklahoma (OK City) : Central Oklahoma (OK City)  Chapter (049)"
            ,"168" => "CO-Denver : Denver Chapter (016)"
            ,"169" => "LA -Greater New Orleans : Greater New Orleans Chapter (061)"
            ,"170" => "LA-Baton Rouge : Baton Rouge Chapter (085)"
            ,"171" => "MO-Greater Kansas City : Greater Kansas City Chapter (087)"
            ,"172" => "MO -Springfield : Springfield Chapter (214)"
            ,"173" => "MO -St. Louis : St. Louis Chapter (011)"
            ,"174" => "New Mexico (Albuquerque) : New Mexico (Albuquerque)   Chapter (083)"
            ,"175" => "North Texas (Dallas)   : North Texas (Dallas)   Chapter (012)"
            ,"176" => "OK -Tulsa : Tulsa Chapter (034)"
            ,"177" => "San Antonio/So. Texas : San Antonio/So. Texas Chapter (081)"
            ,"178" => "TX-Austin : Austin Chapter (020)"
            ,"179" => "TX -Greater Houston Area : Greater Houston Area Chapter (009)"
        );

        $obj->opt4_7 = array(
             "180" => "AK-Anchorage : Anchorage Chapter (177)"
            ,"181" => "AZ-Phoenix : Phoenix Chapter (053)"
            ,"182" => "CA (Anaheim)-Orange County : Orange County Chapter (079)"
            ,"183" => "CA-Los Angeles : Los Angeles Chapter (001)"
            ,"184" => "CA-Sacramento : Sacramento Chapter (076)"
            ,"185" => "CA-San Diego : San Diego Chapter (019)"
            ,"186" => "CA-San Francisco : San Francisco Chapter (015)"
            ,"187" => "CA (Sunnyvale)-Silicon Valley : Silicon Valley Chapter (062)"
            ,"188" => "Hawaii (Honolulu) : Hawaii (Honolulu) Chapter (000)"
            ,"189" => "ID-Boise : Boise Chapter (042)"
            ,"190" => "NV-Las Vegas : Las Vegas Chapter (187)"
            ,"191" => "OR (Portland)-Willamette Valley : Willamette Valley Chapter (050)"
            ,"192" => "Utah (Salt Lake City) : Utah (Salt Lake City)  Chapter (071)"
            ,"193" => "WA (Olympia)-Mt. Rainier : Mt. Rainier Chapter (129)"
            ,"194" => "WA (Seattle)-Puget Sound : Puget Sound Chapter (035)"
        );

        $obj->opt5 = array(
             "195" => "Australia-Adelaide : Adelaide Chapter (068)"
            ,"196" => "Australia-Brisbane : Brisbane Chapter (044)"
            ,"197" => "Australia-Canberra : Canberra Chapter (092)"
            ,"198" => "Australia-Melbourne : Melbourne Chapter (047)"
            ,"199" => "Australia-Perth : Perth Chapter (063)"
            ,"200" => "Australia-Sydney : Sydney Chapter (017)"
            ,"201" => "New Zealand-Auckland : Auckland Chapter (050)"
            ,"202" => "New Zealand-Wellington : Wellington Chapter (021)"
            ,"203" => "Papua New Guinea : Papua New Guinea Chapter (030)"
        );

        if(method_exists($obj, "assign")){
            $obj->assign('opt1',   $obj->opt1);
            $obj->assign('opt2',   $obj->opt2);
            $obj->assign('opt3',   $obj->opt3);
            $obj->assign('opt4_1', $obj->opt4_1);
            $obj->assign('opt4_2', $obj->opt4_2);
            $obj->assign('opt4_3', $obj->opt4_3);
            $obj->assign('opt4_4', $obj->opt4_4);
            $obj->assign('opt4_5', $obj->opt4_5);
            $obj->assign('opt4_6', $obj->opt4_6);
            $obj->assign('opt4_7', $obj->opt4_7);
            $obj->assign('opt5',   $obj->opt5);
        }
    }

}
