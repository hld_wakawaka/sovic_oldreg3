<?php

/**
 * reg3form9番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.12.26
 * 
 */
class Usr_Entry9 {

    function __construct($obj){
        // 支払方法をクレジット固定
        unset($GLOBALS["method_E"][2]);
    }


    //------------------------------------------------
    // ▽ メールカスタマイズ
    //------------------------------------------------

    /** AccompanyingPerson1 */
    function mailfunc26($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str  = "\n";
        $str .= "\n";
        $str .= "[Accompanying Person]\n";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }


    /** AccompanyingPerson2 */
    function mailfunc72($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }



    //------------------------------------------------
    // ▽ CSVカスタマイズ
    //------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = "登録No.";
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $prefix = "";
            $item_id = $_data['item_id'];
            // AccompanyingPerson 1
            if(in_array($item_id, array(26,27,28,63))){
                $prefix = "AccompanyingPerson 1 ";
            }
            // AccompanyingPerson 2
            if(in_array($item_id, array(72,73,74,75))){
                $prefix = "AccompanyingPerson 2 ";
            }

            $groupHeader[$group][] = "\"".$prefix.$name."\"";
        }
        return $groupHeader[$group];
    }

}

?>