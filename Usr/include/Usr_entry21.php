<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry21 {

    // 新規受付の終了日時
    private $end_date  = '2014-05-15 09:00:00';
//    private $end_date  = '2014-04-03 11:00:00';


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        $obj->need_mark = '*Required';

        // 編集完了時に利用 # 編集前の情報を保持するフラグ
        $obj->save_editbefore = true;

        // 編集完了メールに利用 # 更新の前後のみ記載する
        $obj->editmail_diff = true;

        $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
        $GLOBALS["form"]->formData['reception_date2'] = $this->end_date;  // 応募期間：終了
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date2; // 全体のクローズ

        // edata57
        $GLOBALS['titleList'] = array();
        $GLOBALS['titleList'][1] = 'Mr.';
        $GLOBALS['titleList'][2] = 'Mrs.';
        $GLOBALS['titleList'][3] = 'Miss';
        $GLOBALS['titleList'][4] = 'Ms.';
    }


    /* デバッグ用 */
    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1618;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('form21-00004', "8VhOcI5f", 2));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        if(strlen($obj->eid) > 0){
            $GLOBALS['msg']['btn_logout'] = "Log-out";
            $GLOBALS['msg']['browser_back'] .= '<br/>*To close this page without any updating, click  [Log-out] on the upper right corner of the page before closing the window.';
            $GLOBALS['msg']['btn_regist']    = 'update';
            $GLOBALS['msg']['cofirm_desc1']  = "Please check all information below and click the [update] button at the bottom.";
            $GLOBALS['msg']['cofirm_desc2']  = "<b>*Your information has not been updated yet. <br />Please be sure to click the [update] button to complete the procedure.</b>";

            $obj->formdata['form_desc'] = '<b>Categories with "<span class="red">*Required</span>" are the required information.</b><br/>'
                                         .'<br/>'
                                         .'Update your information and click the [next] button.<br/>'
                                         .'To close this page without any updating, click  [Log-out] on the upper right corner of the page before closing the window. <br/>'
                                         .'<br/>'
                                         .'<span style="text-decoration: underline;">＞Seller Information Download</span><br/>'
                                         .'<span class="red">＊File Password：jaitm</span><br/>'
                                         . '<ul style="margin:0;">'
                                         . '<li><a target="_blank" href="https://www.evt-reg.jp/reg3/customDir/21/JATM2014_SellerList.pdf">Seller List as of May 14（PDF）</a></li>'
                                         . '</ul>';
            if(method_exists($obj, "assign")){
                $obj->assign('formData', $obj->formdata);
                $obj->assign("formWord", $obj->formWord);
            }
        }
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


//    // エラーチェック # ブロック1
//    function _check1($obj){
//        Usr_Check::_check1($obj);
//
//        //-------------------------------
//        // AccompanyingPersonの数
//        //-------------------------------
//        $cnt = 0;
//
//        // AccompanyingPerson 1
//        $keys = array('edata64', 'edata69', 'edata70', 'edata71');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 1'), 'edata64');
//            }else{
//                $cnt++;
//            }
//        }
//
//        // AccompanyingPerson 2
//        $keys = array('edata72', 'edata73', 'edata74', 'edata75');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 2'), 'edata72');
//            }else{
//                $cnt++;
//            }
//        }
//    }


    // エラーチェック # ブロック3
    function _check3($obj){
        Usr_Check::_check3($obj);

        // 数字チェック
        $keys = array(103, 104, 105);
        foreach($keys as $_key => $item_id){
            $key = 'edata'.$item_id;
            if(!$obj->objErr->isNumeric($obj->arrParam[$key])){
                $name = Usr_init::getItemInfo($obj, 103);
                $method = $GLOBALS["msg"]["err_numeric"];
                $obj->objErr->addErr(sprintf($method, $name), $key);
                break;
            }
        }

        $group_id = 3;
        $item_id  = 54; $key = 'edata'.$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id)){
            // 1st
            $choice1 = trim($obj->arrItemData[$group_id][$item_id]['select'][$obj->arrParam[$key]]);

            // 2nd
            $item_id = 55; $key = 'edata'.$item_id;
            if(!Usr_init::isset_ex($obj, $group_id, $item_id)) return;
            $choice2 = trim($obj->arrItemData[$group_id][$item_id]['select'][$obj->arrParam[$key]]);
            if(strlen($choice2) > 0){
                // 1st と 2nd で同じ項目を選択
                if($choice1 === $choice2){
                    $obj->objErr->addErr('Familiarization Trips: 1st choice and 2nd choice must be different courses.', $key);
                    return;
                }
            }

            // 3rd
            $item_id = 56; $key = 'edata'.$item_id;
            if(!Usr_init::isset_ex($obj, $group_id, $item_id)) return;
            $choice3 = trim($obj->arrItemData[$group_id][$item_id]['select'][$obj->arrParam[$key]]);
            if(strlen($choice3) > 0){
                // 3rd と 1st で同じ項目を選択
                if($choice3 === $choice1){
                    $obj->objErr->addErr('Familiarization Trips: 1st choice, 2nd choice and 3rd choice must be different courses.', $key);
                    return;
                }
                // 3rd と 2nd で同じ項目を選択
                if($choice3 === $choice2){
                    $obj->objErr->addErr('Familiarization Trips: 1st choice, 2nd choice and 3rd choice must be different courses.', $key);
                    return;
                }
            }
        }
    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 2:
                    $array[$key]= $data;            // First Name
                    $array[28]  = $arrGroup1[28];   // Country
                    break;

                case 58:
                    $array[63]  = $arrGroup1[63];   // Website (English)
                    $array[64]  = $arrGroup1[64];   // Website (original language)
                    $array[$key]= $data;            // Address
                    break;

                case 28:
                case 63:
                case 64:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makeMailBody($obj, $user_id="", $passwd="", $exec_type=""){
        // Outlook対策 # 文字を丸める
        $obj->arrItemData[3][68]['select'][9] = 'Industrial facilities, ...';

        $obj->contact_before = 'Regards,';
        $obj->contact_after  = 'This email is sent by automatic transmission with general information address. Please do not use it for a reply. For individual inquiry, personal communication, etc., please send to the exclusive mail address for buyers.';
        return Usr_mail::makeMailBody($obj, $user_id, $passwd, $exec_type);
    }

    /*  編集用URLのお知らせ */
    function makeEditBody($obj, $user_id="", $passwd=""){ return ''; }

    // 新規登録時
    function makeBodyGroup1($obj, &$arrbody, $exec_type){
        $tmp = $obj->formdata["group1"];
        $obj->formdata["group1"] = 'Your registration information (extract)';
        Usr_mail::makeBodyGroup1($obj, $arrbody, $exec_type);
        $obj->formdata["group1"] = $tmp;
    }

    // 編集時
    function makeDiffBodyGroup1($obj, &$arrbody, $exec_type){
        // グループ3に更新あり
        if(!empty($obj->arrDiff[3])){
            $GLOBALS["msg"]["edit_mail_non"] = "";
        }
        // 編集時はメール非表示も表示する
        foreach($obj->arrItemData[1] as $item_id => &$arrItemData){
            if($arrItemData['disp'] == 1) continue;
            $arrItemData['item_mail'] = 0;
        }

        $tmp = $obj->formdata["group1"];
        $obj->formdata["group1"] = 'Updated information';
        Usr_mail::makeDiffBodyGroup1($obj, $arrbody, $exec_type);
        $obj->formdata["group1"] = $tmp;
    }
    function makeDiffBodyGroup3($obj, &$arrbody, $exec_type){
        // 編集時はメール非表示も表示する
        foreach($obj->arrItemData[3] as $item_id => &$arrItemData){
            if($arrItemData['disp'] == 1) continue;
            $arrItemData['item_mail'] = 0;
        }

        $tmp = $obj->formdata["arrgroup3"];
        $obj->formdata["arrgroup3"] = '';
        Usr_mail::makeDiffBodyGroup3($obj, $arrbody, $exec_type);
        $obj->formdata["arrgroup3"] = $tmp;
    }


    function mailfunc27( $obj, $key, $name) { return ''; }   // Line of business # other
    function mailfunc99( $obj, $key, $name) { return ''; }   // Business category of the other party you want to talk business with # other
    function mailfunc104($obj, $key, $name) { return ''; }   // Date of birth # month
    function mailfunc105($obj, $key, $name) { return ''; }   // Date of birth # day

    function mailfunc26($obj, $key, $name, $i=null) {
        $group = 1;

        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "3":
                // チェックボックス
                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
                
                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n ".$obj->arrItemData[$group][$key]["select"][$n];
                    }
                }
                // otherテキスト
                $str .= "\n ".$obj->mailfuncNiniBody($group, 27, $i);
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }

        $str.= "\n";
        return $str;
    }

    function mailfunc68($obj, $key, $name, $i=null) {
        $group = 3;

        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "3":
                // チェックボックス
                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
                
                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n ".$obj->arrItemData[$group][$key]["select"][$n];
                    }
                }
                // otherテキスト
                $str .= "\n ".$obj->mailfuncNiniBody($group, 99, $i);
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }

        $str.= "\n";
        return $str;
    }

    function mailfunc103($obj, $key, $name, $i=null) {
        $group = 3;

        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "1":
            case "2":
            case "3":
            case "4":
                break;

            default:
                // テキストエリア
                $memo = $obj->arrItemData[$group][103]['item_memo'];
                $str .= $memo.''.$obj->mailfuncNiniBody($group, 103, $i);
                $str .= ' ';

                // otherテキスト
                $memo = $obj->arrItemData[$group][104]['item_memo'];
                $str .= $memo.''.$obj->mailfuncNiniBody($group, 104, $i);
                $str .= ' ';

                // otherテキスト
                $memo = $obj->arrItemData[$group][105]['item_memo'];
                $str .= $memo.''.$obj->mailfuncNiniBody($group, 105, $i);
                break;
        }

        $str.= "\n";
        return $str;
    }

    // Date of birth
    function mailfuncDiff104($obj, $item_id, $name, $diff){ return ''; }
    function mailfuncDiff105($obj, $item_id, $name, $diff){ return ''; }

    function mailfuncDiff103($obj, $item_id, $name, $diff){
        $group = 3;

        $b1 = "year".$obj->arrDiff[$group]['edata'.$item_id]->before;
        $a1 = "year".$obj->arrDiff[$group]['edata'.$item_id]->after;

        $item_id = 104;
        $b2 = "month".$obj->arrDiff[$group]['edata'.$item_id]->before;
        $a2 = "month".$obj->arrDiff[$group]['edata'.$item_id]->after;

        $item_id = 105;
        $b3 = "day".$obj->arrDiff[$group]['edata'.$item_id]->before;
        $a3 = "day".$obj->arrDiff[$group]['edata'.$item_id]->after;

        $str = $obj->point_mark.$name.": ".$b1.' '.$b2.' '.$b3."→".$a1.' '.$a2.' '.$a3;
        return $str;
    }

//    /**
//     * 任意
//     */
//    function mailfunc[i]($obj, $key, $name, $i=null) {
//        $group = '[group_id]';
//
//        $wk_key = "edata".$key.$i;
//        
//        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
//        
//        $str = $obj->point_mark.$name.": ";
//        
//        switch($obj->arrItemData[$group][$key]["item_type"]) {
//            case "1":
//                // テキストエリア
//                $str .= "\n".$obj->arrForm[$wk_key];
//                break;
//            case "2":
//            case "4":
//                // ラジオ、セレクト
//                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
//                    $str .= $obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]];
//                }
//                break;
//            case "3":
//                // チェックボックス
//                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
//                
//                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
//                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
//                    if($obj->arrForm[$wk_key.$n] == $n) {
//                        $str .= "\n".$obj->arrItemData[$group][$key]["select"][$n];
//                    }
//                }
//                
//                break;
//            default:
//                // テキストボックス
//                $str .= $obj->arrForm[$wk_key];
//        }
//
//        $str.= "\n";
//        return $str;
//    }


    // ------------------------------------------------------
    // ▽ログインページカスタマイズ
    // ------------------------------------------------------

    function usr_login_defaultAction($obj){
        $obj->arrErr = $GLOBALS["session"]->getVar("error");
        $obj->assign("msg", 'Enter your ID and password indicated on the confirmation mail<br/>from the Buyer\'s secretariat.');
    }

    /* 受付期間のチェック */
    function usr_login_checkValidDate($obj){
        $reception_date1 = $GLOBALS["form"]->formData['reception_date1'];
        $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
        $reception_date3 = $GLOBALS["form"]->formData['reception_date3'];

        // 編集期間：開始～全体クローズ
        $GLOBALS["form"]->formData['reception_date2'] = $reception_date3;
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date2;

        // 全体のクローズかチェック
    	$entry_status = $GLOBALS["form"]->checkValidDateSub();

	   	switch($entry_status) {
	   	    // 受付期間前
			case "1":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_start"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date1"])));
				if($GLOBALS["form"]->formWord["word26"] != ""){
					$ws_str = $GLOBALS["form"]->formWord["word26"];
				}
				Error::showErrorPage($ws_str);
				break;

            // 受付期間終了 # 完全にクローズ
			case "2":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_end"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date2"])));

				if($GLOBALS["form"]->formWord["word27"] != ""){
					$ws_str = $GLOBALS["form"]->formWord["word27"];
				}
				Error::showErrorPage("受付期間終了いたしました。");
				break;

            // 審査期間中
			case "3":
				Error::showErrorPage(sprintf($GLOBALS["msg"]["validDate_middle"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["secondary_reception_date"]))));
				break;
			default:
				break;
		}

        $GLOBALS["form"]->formData['reception_date2'] = $reception_date2;
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date3;


		if(!$GLOBALS["form"]->checkPropriety()) {
			Error::showErrorPage($GLOBALS["msg"]["err_login_status"]);
		}

		return;
    }


    /* ログイン処理 */
    function usr_login_loginAction($obj){
        $obj->arrErr = $obj->_check();
        if(count($obj->arrErr) > 0) return;

        // ログイン処理
        $err = LoginEntry::doLogin($GLOBALS["form"]->formData["lang"], $_REQUEST["login_id"], $_REQUEST["password"]);

        // エラーの場合
        if(!empty($err)) {
            $obj->arrErr[] = $err;
            $GLOBALS["log"]->write_log("euser_login failed", $_REQUEST["login_id"]);
            return;
        }


        // 編集可能期間内かチェック
        $reception_date1 = $GLOBALS["form"]->formData['reception_date1'];
        $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
        $reception_date3 = $GLOBALS["form"]->formData['reception_date3'];

        // 編集期間：開始～全体クローズ
        $GLOBALS["form"]->formData['reception_date2'] = $reception_date3;
        $GLOBALS["form"]->formData['reception_date3'] = $reception_date2;

        $date1 = strtotime($GLOBALS["form"]->formData['reception_date1']);
        $date2 = strtotime($GLOBALS["form"]->formData['reception_date2']);
        $now   = strtotime(date("Y-m-d H:i:s"));
        $isCanEdit = ($date1 <= $now && $now < $date2);

        // 編集期間内
        if($isCanEdit){
            $GLOBALS["log"]->write_log("euser_login Success", $GLOBALS["entryData"]["eid"]);

            $url = "./form/Usr_entry.php?form_id=".$GLOBALS["form"]->formData["form_id"];
            header("location: ".$url);
            exit;
        }

        // 全体クローズ
    }


    function _chkPeriod($obj) {
        // ユーザのセッション情報があるか？
        if(isset($GLOBALS["entryData"]["eid"])){
            $reception_date1 = $GLOBALS["form"]->formData['reception_date1'];
            $reception_date2 = $GLOBALS["form"]->formData['reception_date2'];
            $reception_date3 = $GLOBALS["form"]->formData['reception_date3'];

            $GLOBALS["form"]->formData['reception_date2'] = $reception_date3;
            $GLOBALS["form"]->formData['reception_date3'] = $reception_date2;
        }

        return $obj->o_form->checkValidData_Ex();
    }


    //------------------------------------------
    // ▽CSVカスタマイズ
    //------------------------------------------

    /** CSVヘッダ生成 */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){
        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        // グループ1ッダ
        $groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);

        // グループ2ヘッダ
        $groupHeader[2] = $obj->entryMakeHeader2($obj, $all_flg);

        // グループ3ヘッダ
        $groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);


        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[]="\"status\"";
        $header[]="\"regist date\"";
        $header[]="\"edit date\"";

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }


    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $arrItemData = $obj->arrItemData;
        $arrItemData[$group][27]['item_name'] = 'Line of business / Other';

        $groupHeader[$group][] = "\"entry No.\"";
        foreach($arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = "\"".$name."\"";
        }

        return $groupHeader[$group];
    }

    /** CSVヘッダ-グループ3生成 */
    function entry_csv_entryMakeHeader3($obj, $all_flg=false){
        $group = 3;
        $arrItemData = $obj->arrItemData;
        $arrItemData[$group][99]['item_name']  = 'Business category / Other';
        $arrItemData[$group][103]['item_name'] = 'DOB Year';

        $groupHeader = array();
        foreach($arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = "\"".$name."\"";
        }

        return $groupHeader[$group];
    }


    //------------------------------------------
    // ▽ステータス変更カスタマイズ
    //------------------------------------------

    function __constructMng($obj){
        $GLOBALS["entryStatusList"]["6"] = "承認";
    }

    function __constructMngCSV($obj){
        $GLOBALS["entryStatusList"]["6"] = "approval";
    }


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    function csvfunc26($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }
    function csvfunc67($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }
    function csvfunc68($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", "/"), false);
        $wk_body = trim($wk_body, "/");
        return $wk_body;
    }


}
