<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry45 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        // jQueryを読み込む
        $obj->useJquery = true;

//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        // 同行者あり
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        $ather_price0 = strlen($arrForm1['ather_price0']) > 0 ? intval($arrForm1['ather_price0']) : -1;
        // 同行者なしの場合は表示しない
        if(!($ather_price0 > 0)){
            $obj->arrItemData[3][99]['disp'] = 1;

        // 同行者ありの場合は必須
        }else{
            $obj->arrItemData[3][99]['need'] = 1;
        }

        // assignする場合
        if(method_exists($obj, "assign")){
            $obj->assign("arrItemData", $obj->arrItemData);
        }

        $this->filter_section($obj);
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        //-----------------------------------------------
        // 同伴者に関するエラーチェック
        //-----------------------------------------------
        // Accompanying Persion
        $count = 0;

        // 74 # Family Name
        $item_id = 74;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // 79 # Family Name
        $item_id = 79;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0) $count++;

        // Fee:AccompanyingPersion
        $i   = 0;
        $key = 'ather_price0';
        $accompanying = isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0 ? intval($obj->arrParam[$key]) : 0;

        // Enter Accompanying Person
        // 同伴者1または2のどちらかで「Family Name」の入力があった場合はFee:AccompanyingPersionを必須
        $error_flg = false;
        if($count > 0 && $accompanying == 0){
            $name   = $obj->wa_ather_price[$i]['name'];
            $method = $GLOBALS["msg"]["err_require_select"];
            $obj->objErr->addErr(sprintf($method, $name), $key);
            $error_flg = true;
        }
        // Fee:AccompanyingPersionの購入があった場合、同伴者入力がないとエラー
        if($count == 0 && $accompanying > 0){
            $name   = 'Accompanying person(s)';
            $method = $GLOBALS["msg"]["err_require_input"];
            $obj->objErr->addErr(sprintf($method, $name), $key);
            $error_flg = true;
        }
        // 「同伴者の入力数」=金額の「Accompanying persion(s)」でないとエラー
        if($count != $accompanying && $accompanying > 0 && !$error_flg){
            $obj->objErr->addErr("Number of accompanying person's name you enter and the number of accompanying person fee you select should be the same.", $key);
        }
    }


    // エラーチェック # ブロック3
    function _check3($obj){
        $this->filter_section($obj);

        $group_id = 3;
        Usr_Check::_check3($obj);

        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");

        // No.2
        // Fee
        $amount = strlen($arrForm1['amount']) > 0 ? intval($arrForm1['amount']) : -1;
        // 金額カテゴリが「IABSE Member」
        if($amount === 0){
            $item_id = 54;
            $key = "edata".$item_id;
            $arrItemData = $obj->arrItemData[$group_id][$item_id];
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && $this->isNull($obj->arrParam, $group_id, $item_id, $arrItemData)){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }else{
                // 10桁制限
                if(strlen($obj->arrParam[$key]) != 6){
                    $obj->objErr->addErr('This field only accept 6 digit numbers', $key);
                }
            }
        }

        // No.3
        // 同伴者あり
        if(!empty($arrForm1['ather_price0'])){
            $item_id = 99;
            $key = "edata".$item_id;
            $arrItemData = $obj->arrItemData[$group_id][$item_id];
            if($this->isNull($obj->arrParam, $group_id, $item_id, $arrItemData)){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }


        // 「would like to have lunch.」で「I do not need any lunches.」を選択
        if(isset($obj->arrParam['edata674']) && $obj->arrParam['edata674'] == 4){
            $notSelect = array("edata671", "edata672", "edata673");
            foreach($notSelect as $_key => $key){
                if(isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0){
                    $obj->objErr->addErr("When you select \"I do not need any lunches.\", don't select other item(s). ", $key);
                    break;
                }
            }
        }

        // 「your accompanying person(s) would like to have lunch」で「My accompanying person(s) do not need any lunches.」を選択
        if(isset($obj->arrParam['edata1024']) && $obj->arrParam['edata1024'] == 4){
            $notSelect = array("edata1021", "edata1022", "edata1023");
            foreach($notSelect as $_key => $key){
                if(isset($obj->arrParam[$key]) && strlen($obj->arrParam[$key]) > 0){
                    $obj->objErr->addErr("When you select \"My accompanying person(s) do not need any lunches\", don't select other item(s). ",  $key);
                    break;
                }
            }
        }
    }



    /**
     * Config.filter
     * 
     * @param  object  $obj
     * @return void
     * 
     */
    function filter_section($obj){
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        // 同伴者なし
        if(empty($arrForm1['ather_price0'])){
            $obj->arrItemData[3][102]['disp'] = 1;
            // エラーチェック対策
            if(($key = array_search(102, $obj->option_item[3])) !== false){
                unset($obj->option_item[3][$key]);
            }

            // 画面・メール対策
            if(method_exists($obj, "assign")){
                $obj->assign("arrItemData", $obj->arrItemData);
            }
        }
    }



    /**
     * 入力/選択しているかチェック
     *    標準対応予定
     * 
     * @param  arrray  $arrParam    対象グループの入力配列
     * @param  integer $group_id    対象グループ
     * @param  integer $item_id     チェック対象の項目ID
     * @param  array   $arrItemData 項目IDのアイテム情報配列
     * @return boolean true:入力状態, false:未入力
     * 
     */
    function isNull($arrParam, $group_id, $item_id, $arrItemData){
        // チェックボックス
        if($arrItemData['item_type'] == Usr_init::INPUT_CHECK){
            $options = $arrItemData['select'];
            $ok =0;
            foreach($options as $key => $val){
                $wk_val = isset($arrParam["edata".$item_id.$key]) ? $arrParam["edata".$item_id.$key]: "";
                if($wk_val != "") $ok++;
            }

            // 入力数をカウントするので反転
            return !(bool)$ok;
        }

        // その他
        // 標準関数の戻り値のbool値が逆なので反転
        return !Validate::isNull($arrParam["edata".$item_id]);
    }



    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup3 =& $obj->arrItemData[3];

        // 入れ替え
        $array = array();
        foreach($arrGroup3 as $key => $data){
            switch($key){
                case 67:
                    $array[$key] = $data;
                    $array[102]  = $arrGroup3[102];
                    break;

                case 68:
                    $array[$key] = $data;
                    $array[147]  = $arrGroup3[147];
                    break;

                case 102:
                case 147:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup3 = $array;
    }



    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $body_header = Usr_mail::makeMailBody_header($obj, $user_id, $passwd, $exec_type);

        $tag = "";
        // クレジットカード
        if($obj->arrForm["method"] == 1){
            $tag = "bank";
        }else
        // 銀行振込
        if($obj->arrForm["method"] == 2){
            $tag = "credit";
        }

        // タグ除去
        if(strlen($tag) > 0){
            $pattern = sprintf("!<%s.*?>.*?</%s>!ims", $tag, $tag);
            $body_header = preg_replace($pattern, "", $body_header);
        }
        $body_header = strip_tags($body_header);

        return $body_header;
    }

    /** AccompanyingPerson1 # mailfunc[AccompanyingPerson1の最初のitem_id] */
    function mailfunc73($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }


    /** AccompanyingPerson2 # mailfunc[AccompanyingPerson2の最初のitem_id] */
    function mailfunc78($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }

//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $prefix = "";
            $item_id = $_data['item_id'];
            // AccompanyingPerson 1
            if(in_array($item_id, array(73,74,75,76))){     // AccompanyingPerson1の関連項目
                $prefix = "AccompanyingPerson 1 ";
            }
            // AccompanyingPerson 2
            if(in_array($item_id, array(78,79,80,81))){     // AccompanyingPerson2の関連項目
                $prefix = "AccompanyingPerson 2 ";
            }

            $groupHeader[$group][] = $obj->fix.$prefix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
