<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld doishun
 *        
 */

require_once(ROOT_DIR."customDir/70/package/Mng_entry70.php");

class Usr_Entry70 extends Mng_Entry70
{

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj)
    {
        $form_id = 70;
        
        //--------------------------
        // パッケージ機能
        //--------------------------
        $this->includeDir = TEMPLATES_DIR."../customDir/{$form_id}/package/templates/Usr/";
        if (method_exists($obj, "assign")) {
            $obj->assign("includeDir", $this->includeDir);
        }
    }


    // 外部テンプレートを読み込み直す
    function setTemplate($obj){
        // パッケージ機能の一環
        Usr_initial::setTemplate($obj, $this->includeDir);
    }


    /* デバッグ用 */
    function developfunc($obj){
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj)
    {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // エラーチェック # ブロック1
    function _check1($obj)
    {
        $group_id = 1;
        Usr_Check::_check1($obj);
        
        //-----------------------------------------
        // No.3 # 同伴者の入力数と購入数の不一致でエラー
        //-----------------------------------------
        $cnt     = 0; // 同伴者数
        $item_id = 27;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $item_id) && $obj->objErr->isNull($obj->arrParam[$key])) $cnt++;
        
        // その他決済 # 同伴者
        $other = strlen($obj->arrParam['ather_price0']) > 0 ? intval($obj->arrParam['ather_price0']) : 0;
        
        if($cnt != $other){
            $obj->objErr->addErr("Number of accompanying person's name you enter and the number of accompanying person fee you select should be the same.", "ather_price0");
        }
    }


    // エラーチェック # ブロック3
    function _check3($obj){
//        $obj->admin_flg = "";
        $group_id = 3;
        
        // No.5,6 # 最初の選択肢を選択したらテキストボックスを必須
        // No5カスタマイズを解除
        $item_id = 56;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
            $_item_id = 67;
            $key2     = "edata".$_item_id;
            if (Usr_init::isset_ex($obj, $group_id, $_item_id) && ! $obj->objErr->isNull($obj->arrParam [$key2])) {
                $name = Usr_init::getItemInfo($obj, $_item_id);
                $method = Usr_init::getItemErrMsg($obj, $_item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key2);
            }
        }
        
        
        Usr_Check::_check3($obj);
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------
    
    // No.5
    function mailfunc54($obj, $item_id, $name, $i = null){
        $group = 3;
        
        $key = "edata" . $item_id . $i;
        if(! isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
        
        if($obj->arrForm[$key] == 1){
            $item_id = 55;
            $key     = "edata".$item_id;
            $edata   = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
            $value = $value ." ". $edata;
        }
        
        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


    // No.6
    function mailfunc56($obj, $item_id, $name, $i = null){
        $group = 3;
    
        $key = "edata" . $item_id . $i;
        if(! isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
    
        if($obj->arrForm[$key] == 1){
            $item_id = 67;
            $key     = "edata".$item_id;
            $edata   = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
            $value = $value ." ". $edata;
        }
    
        $str = $obj->point_mark . $name . ": " . $value . "\n";
        return $str;
    }


//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------
//    function csvfunc_i($obj, $group, $pa_param, $item_id) {
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
//                " ",
//                "," 
//        ), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }
}
