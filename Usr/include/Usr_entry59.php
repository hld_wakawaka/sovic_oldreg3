<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry59 {

    // ----------------------------------------
    // カスタマイズメモ
    // ----------------------------------------

    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
//        
//         // jQueryを読み込む
//         $obj->useJquery = true;
//        
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
//        
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
//        
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
    }

    /* デバッグ用 */
    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }

    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj) {
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // エラーチェック # ブロック1
    function _check1($obj) {
    	//$obj->admin_flg = "";
        Usr_Check::_check1($obj);
        $group_id = 1;
        
        // Fee
        $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
        
        // xxで1番目の項目を選択したらテキスト必須
        $item_id = 63;
        $key = "edata" . $item_id;
        if (Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam [$key]) > 0) {

           if(!($amount == 2 || $amount == 3)) {
//           		$obj->objErr->addErr("項目Aを入力したら項目Bに入力してください", $key);
           } else {
           		unset($obj->objErr->_err["method"]);
           }
        } else {
//         	if(!($amount == 0 || $amount == 1)) {
//         		$obj->objErr->addErr("項目Aを入力しない場合、B以外を入力してください", $key);
//         	}
        }
        //var_dump($obj->objErr->_err);
    }

//     // エラーチェック # ブロック3
//     function _check3($obj) {
//         Usr_Check::_check3($obj);
        
//         // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//         $group_id = 3;
//         $item_id = 68;
//         $target = 'Allergy';
//         if (Usr_Check::chkSelect($obj, $group_id, $item_id, $target)) {
//             $item_id = 99;
//             $key = "edata" . $item_id;
//             if (Usr_init::isset_ex($obj, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
//                 $name = Usr_init::getItemInfo($obj, $item_id);
//                 $method = Usr_init::getItemErrMsg($obj, $item_id);
//                 $obj->objErr->addErr(sprintf($method, $name), $key);
//             }
//         }
//     }



    //--------------------------------------
    // No.3 # ページスキップ
    //--------------------------------------

    /** 1ページ目 */
    function pageAction1($obj) {
    	Usr_pageAction::pageAction1($obj);

    	$amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
//    	$method = strlen($obj->arrParam ['method']) > 0 ? intval($obj->arrParam ['method']) : - 1;
    	
    	// [form59] 支払い方法で招待コードによる支払いを選択したときのみ、確認画面にそのまま遷移させる
//    	if(in_array($amount,array("2","3")) || $method == "2"){
    	if(in_array($amount,array("2","3"))){
    		$obj->block = "4";
    		$obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
    	}
    }


    /** 戻るボタン */
    function backAction($obj) {
    	Usr_pageAction::backAction($obj);
    	$arrForm1 = $GLOBALS["session"]->getVar("form_param1");
    	$amount = strlen($arrForm1 ['amount']) > 0 ? intval($arrForm1 ['amount']) : - 1;
//    	$method = strlen($arrForm1 ['method']) > 0 ? intval($arrForm1 ['method']) : - 1;
    	
    	// [form59] 支払い方法で招待コードによる支払いを選択したときのみ、確認画面から入力画面にそのまま遷移させる
//    	if(in_array($amount,array("2","3")) || $method == "2"){
    	if(in_array($amount,array("2","3"))){
    		$obj->block = 1;
    		$obj->_processTemplate = "Usr/form/Usr_entry.html";
    	}
    }



    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    function sortFormIni($obj) {
        $arrGroup1 = & $obj->arrItemData [1];
        
        // 入れ替え
        $array = array ();
        foreach ( $arrGroup1 as $key => $data ) {
            switch ($key) {
				
            	case 58 :
            	case 59 :
            		break;
            	case 17 :
            		$array [$key] = $data;
            		$array [114] = $arrGroup1 [114];
            		$array [58] = $arrGroup1 [58];
            		$array [59] = $arrGroup1 [59];
            		break;
            	case 114 :
            		break;
                
                default :
                    $array [$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
//         var_dump($arrGroup1);
//         exit;
    }


    // [form58[reg3]]_追加調整：メール本文の修正
    
    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------
    
    function makePaymentBody($obj, $exec_type){
    	// 決済なし
    	if($obj->formdata["kessai_flg"] != "1") return "";
    
    	//支払合計
    	// [form59] お支払い合計金額が表示されるように、合計値を配列ではなく値で受け取るように修正
    	if($exec_type == "1"){
    		//    		$total = Usr_function::_setTotal(array(), $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
    		$total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
    	}else{
    		$total = $GLOBALS["session"]->getVar("ss_total_payment");
    	}
    
    	$total = $total;
    	
    		
    	// [form59] 支払い方法で銀行振込を選択した時でもお支払情報は表示
//    		if($obj->arrForm["method"] != 2){
    			if($obj->formdata["lang"] == LANG_JPN){
    				$obj->point_mark = "■";
    				$body_pay = "\n\n【お支払情報】\n\n";
    			}else{
    				$obj->point_mark = "*";
    				$body_pay = "\n\n[Payment Information]\n\n";
    			}
//    		}
    
    	// [form59]お支払い情報のみは必ず表示。それ以外の銀行振込情報や金額などは銀行振込やB選択時には表示しないでおく
    		
    		//$body_pay .= "\n\n";
    		if($obj->formdata["lang"] == LANG_JPN){
    			// お支払確認ページの表示/非表示 : デフォルトは表示
    			if(!in_array($obj->form_id, $obj->payment_not)){
                	if($obj->arrForm["amount"] == "0" || $obj->arrForm["amount"] == "1"){
    		    		$body_pay .= $obj->point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
                	}
    			}
    			$body_pay .= $obj->point_mark."金額: \n";
    		}
    		else{
    			if($total > 0){
    				// お支払確認ページの表示/非表示 : デフォルトは表示
    				if(!in_array($obj->form_id, $obj->payment_not)){
    					$body_pay .= ($obj->arrForm["method"] != "3")
    					? $obj->point_mark."Payment: ".$obj->wa_method[$obj->arrForm["method"]]."\n"
    							: $obj->point_mark."Payment :Cash on-site\n";
    				}
    			}
    			$body_pay .= $obj->point_mark."Amount of Payment:\n";
    		}
    		 
    		// Fee
    		$body_pay .= $obj->makePaymentBody1($exec_type);
    
    		//その他決済がある場合
    		$body_pay .= $obj->makePaymentBody2($exec_type);

    
    	if($obj->formdata["lang"] == LANG_JPN){
    		$body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
    	}
    	else{
    		$body_pay .="      Amount of Total Payment:".$obj->yen_mark.number_format($total)."\n\n";
    	}
    
    
    	// お支払情報のメール生成
    	// お支払確認ページの表示/非表示 : デフォルトは表示
    	if(!in_array($obj->form_id, $obj->payment_not)){
    		$body_pay .= $obj->makePaymentMethodBody($obj,$total);
    	}
    
    	return $body_pay;
    }
    
    
    /** 金額のメール本文 */
    function makePaymentBody1($obj, $exec_type){
    	 
    	$body_pay = "";
    	// 決済なし
    	if($obj->formdata["kessai_flg"] != "1") return "";
    	 
    	//新規の場合
    	if($exec_type == "1"){
    		foreach($obj->wa_price as $p_key => $data){
    			if(!isset($obj->arrForm["amount"])) continue;
    			if($p_key == $obj->arrForm["amount"]){
    				$price = $data["p1"];
    				if(is_numeric($price)){
    					$price = number_format($data["p1"]);
    					$price = ($obj->formdata["lang"] == LANG_JPN)
    					? $price."円"
    							: $obj->yen_mark.$price."";
    				}
    				 
    				// タグを除去
    				//                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
    				$data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除
    				 
    				if($obj->formdata["pricetype"] == "2"){
    					if($obj->formdata["lang"] == LANG_JPN){
    						$body_pay .= "　　　".$data["name"]."：".$price."\n";
    					}
    					else{
    						$body_pay .= "      ".$data["name"].":".$price."\n";
    					}
    				}
    				else{
    					if($obj->formdata["lang"] == LANG_JPN){
    						$body_pay .=  "　　　".$obj->formdata["csv_price_head0"]."：".$price."\n";
    					}
    					else{
    						$body_pay .=  "      ".$obj->formdata["csv_price_head0"].":".$price."\n";
    					}
    				}
    				break;
    			}
    		}
    	}
    	 
    	//更新の場合
    	if($exec_type == "2"){
    		//全てのその他決済で項目を生成しているため、出力は不要
    	}
    	return $body_pay;
    }
    
    function makePaymentBody2($obj, $exec_type){
    	$body_pay = "";
    	// 決済なし
    	if($obj->formdata["kessai_flg"] != "1") return "";
    
    	//その他決済がある場合
    	if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){
    
    		//新規の場合
    		if($exec_type == "1"){
    			foreach($obj->wa_ather_price  as $p_key => $data ){
    				if(!isset($obj->arrForm["ather_price".$p_key])) continue;
    				if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
    					$price = $data["p1"];
    
    					// Free, - => 0
    					if(!is_numeric($data["p1"])){
    						$data["p1"] = 0;
    					}
    					$sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];
    
    					// タグを除去
    					//                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
    					$data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除
    
    					if($obj->formdata["lang"] == LANG_JPN){
    						if(is_numeric($price)){
    							$price = number_format($price)."円";
    						}
    						$body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
    					}
    					else{
    						if(is_numeric($price)){
    							$price = $obj->yen_mark.number_format($price);
    						}
    						$body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum)."  (".$price." ×".$obj->arrForm["ather_price".$p_key].")\n";
    					}
    				}
    			}
    
    		}
    
    		//更新の場合
    		if($exec_type == "2"){
    			//更新の場合は、登録済みのデータから引っ張ってくる
    			$wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);
    
    			foreach($wa_payment_detail as $data){
    				$price = $data["price"];
    				// Free, - => 0
    				if(!is_numeric($data["price"])){
    					$data["price"] = 0;
    				}
    				$sum = $data["price"]*$data["quantity"];
    
    				// タグを除去
    				//                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
    				$data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除
    
    				if($obj->formdata["lang"] == LANG_JPN){
    					if(is_numeric($price)){
    						$price = $price."円";
    					}
    					$body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
    				}
    				else{
    					if(is_numeric($price)){
    						$price = $obj->yen_mark.$price;
    					}
    					$body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum);
    				}
    
    				// その他決済の場合は内訳を後ろにつける
    				if($data["type"] == "1"){
    					if($obj->formdata["lang"] == LANG_JPN){
    						$body_pay .="　（".$price." ×".$data["quantity"]."）";
    					}
    					else{
    						$body_pay .="  (".$price." ×".$data["quantity"].")";
    					}
    				}
    				$body_pay .= "\n";
    			}
    		}
    	}
    	return $body_pay;
    }
    
    /** メール本文生成 お支払情報 */
    function makePaymentMethodBody($obj, $total){
    	$body_pay = "";
    	if(!$total > 0) return $body_pay;
    
    	$actionName = "makePaymentMethodBody". $obj->arrForm["method"];
    	if(method_exists($obj, $actionName) && strlen($obj->arrForm["method"]) > 0){
    		$body_pay = $obj->$actionName($total);
    	}
    
    	return $body_pay;
    }
    
    
    function makePaymentMethodBody1($obj, $total){
    	$body_pay = "";
    	if(!$total > 0) return $body_pay;
    	 
    	// [form59]クレジットカード選択時の支払い情報が表示されるように、支払い情報の本文生成について修正
    	// オンライン決済のみ本文を生成する
    	//    	if($obj->formdata["credit"] != "2") return $body_pay;
    	//    	if($obj->formdata["credit"] != "1") return $body_pay;
    
    	if($obj->formdata["lang"] == LANG_JPN){
    		$wk_pay_card= "カード会社";
    		$wk_pay_card_num = "クレジットカード番号";
    		$wk_pay_card_limit = "有効期限";
    		$wk_pay_card_meigi = "カード名義人";
    		$wk_pay_month = "月";
    		$wk_pay_year = "年";
    	}
    	else{
    		$wk_pay_card= "Card Type";
    		$wk_pay_card_num = "Card Number";
    		$wk_pay_card_limit = "Expiration Date";
    		$wk_pay_card_meigi = "Card Holder's Name";
    		$wk_pay_month = "Month";
    		$wk_pay_year = "Year";
    	}
    
    	$body_pay .=$obj->point_mark.$wk_pay_card.":".$obj->wa_card_type[$obj->arrForm["card_type"]]."\n";
    	$body_pay .=$obj->point_mark.$wk_pay_card_num.":";
    
    	// カード番号をマスクする桁数
    	$a = 2;
    	switch($obj->arrForm["card_type"]){
    		//VISA
    		//MasterCard
    		//JCB
    		case 1:
    		case 2:
    		case 5:
    			$a = 16 -2;
    			break;
    
    			//Diners Club
    		case 3:
    			$a = 14 -2;
    			break;
    
    			//AMEX
    		case 4:
    			$a = 15 -2;
    			break;
    	}
    	$card = "";
    	$body_pay .= sprintf("%'*".$a."s", $card).substr($obj->arrForm["cardNumber"],-2)."\n";
    	$body_pay .= $obj->point_mark.$wk_pay_card_limit.":".$obj->arrForm["cardExpire1"].$wk_pay_month."　".$obj->arrForm["cardExpire2"].$wk_pay_year."\n";
    	$body_pay .= $obj->point_mark.$wk_pay_card_meigi.":".$obj->arrForm["cardHolder"]."\n";
    
    	return $body_pay;
    }
    
    
    function makePaymentMethodBody2($obj, $total){
    	$body_pay = "";
    	if(!$total > 0) return $body_pay;
    	 
    	// [form59] 銀行振込を選択した時は、銀行振込情報はメールに表示しないようにするため、コメントアウト
    
    	//    	if($obj->formdata["lang"] == LANG_JPN){
    	//    		$wk_pay_furikomi= "お振込み人名義";
    	//    		$wk_pay_bank = "お支払銀行名";
    	//    		$wk_pay_furikomibi = "お振込み日";
    	//   	}
    	//    	else{
    	//    		$wk_pay_furikomi= "Name of remitter";
    	//    		$wk_pay_bank = "Name of bank which you will remit";
    	//    		$wk_pay_furikomibi = "Date of remittance";
    	//    	}
    
    	//    	$pay2key = array("lessee", "bank", "closure");
    	//    	foreach($pay2key as $_key => $_val){
    	//    		if(!isset($obj->arrForm[$_val])) $obj->arrForm[$_val] = "";
    	//    	}
    
    	//    	$body_pay .=$obj->point_mark.$wk_pay_furikomi.": ".$obj->arrForm["lessee"]."\n";
    	//    	$body_pay .=$obj->point_mark.$wk_pay_bank.": ".$obj->arrForm["bank"]."\n";
    	//    	$body_pay .=$obj->point_mark.$wk_pay_furikomibi.": ".$obj->arrForm["closure"]."\n";
    
    	return $body_pay;
    }
}
