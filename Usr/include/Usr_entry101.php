<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld kawauchi
 *        
 */
class Usr_Entry101 {


   /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
//        
        // jQueryを読み込む
        $obj->useJquery = true;
//        
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
//        
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
//        
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
   }

//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }

//    /**
//     * ProcessBase::mainの前処理
//     */
//    function premain($obj) {
//        // Assignを直前で変更する場合や特定のエラーチェックで利用
//    }
//    
//    // 項目チェックの際は項目のdispフラグを考慮するため
//    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する
//    

    
   // エラーチェック # ブロック1
   function _check1($obj) {
       Usr_Check::_check1($obj);
       
       $group_id = 1;
       
       // Fee
       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
       
      // [reg3-form101] グループ1>Paper ID：2に⼊力した場合は、3の選択を必須
      $item_id = 27;
      $key = "edata" . $item_id;
      if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] != "") {
           $item_id = 28;
           $key = "edata" . $item_id;
           
           if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
               $name = Usr_init::getItemInfo($obj, $item_id);
               $method = Usr_init::getItemErrMsg($obj, $item_id);
               $obj->objErr->addErr(sprintf($method, $name), $key);
           }
      }
       
      // [reg3-form101] グループ1>Presentation Type：3に⼊力した場合は、2の選択を必須
      $item_id = 28;
      $key = "edata" . $item_id;
      if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] != "") {
          $item_id = 27;
          $key = "edata" . $item_id;
           
          if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
              $name = Usr_init::getItemInfo($obj, $item_id);
              $method = Usr_init::getItemErrMsg($obj, $item_id);
              $obj->objErr->addErr(sprintf($method, $name), $key);
          }
      }
      
      
      // [reg3-form101] E02：Paper ID とPresentation Typeに⼊力/選択がある場合は、★1または★2しか選べない
      $piKey = "edata27";
      $ptKey = "edata28";
      
      if($obj->arrParam [$piKey] != "" && $obj->arrParam [$ptKey] != "") {
          if($amount == 2 || $amount == - 1) {
              $key = "amount";
              $method = 'Please select ACM/SIG Member or Non‒ACM/SIG Member for fee if this registration covers any presentation.';
              $obj->objErr->addErr($method, $key);
          }
      }
      
      
      // [reg3-form101] E02：ACM/SIG Memberを選んだ時は、Membership Numberを入力しないとエラー
      if($amount == 0) {
           $item_id = 26;
           $key = "edata" . $item_id;
           if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
               $method = 'Please fill in your membership number if you apply for ACM/SIG Member registration.';
               $obj->objErr->addErr($method, $key);
           }
      }
      
      // [reg3-form101] E02：Membership Numberに入力がある時は、ACM/SIG Memberを選ばないとエラー
      $item_id = 26;
      $key = "edata" . $item_id;
      if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] != "") {
          if($amount != 0) {
              $key = "amount";
              $method = 'Please select ACM/SIG Member for fee if you are a member and filled in the membership number.';
              $obj->objErr->addErr($method, $key);
          }
      }
      
      
   }

}
