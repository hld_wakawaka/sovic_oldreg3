<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *
 */
class Usr_Entry39 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

//        // jQueryを読み込む
//        $obj->useJquery = true;
//
//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
//        for($i=0; $i<2000; $i++){
//            print $obj->getEntryNumber($obj)."<br/>";
//        }
//        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


//    // エラーチェック # ブロック1
//    function _check1($obj){
//        Usr_Check::_check1($obj);
//
//        $group_id = 1;
//
//        // Fee
//        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;
//
//
//        // xxで1番目の項目を選択したらテキスト必須
//        $item_id = 26;
//        $key = "edata".$item_id;
//        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
//            $item_id = 27;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//
//
//        //-------------------------------
//        // AccompanyingPersonの数
//        //-------------------------------
//        $cnt = 0;
//
//        // AccompanyingPerson 1
//        $keys = array('edata64', 'edata69', 'edata70', 'edata71');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 1'), 'edata64');
//            }else{
//                $cnt++;
//            }
//        }
//
//        // AccompanyingPerson 2
//        $keys = array('edata72', 'edata73', 'edata74', 'edata75');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 2'), 'edata72');
//            }else{
//                $cnt++;
//            }
//        }
//    }
//
//
//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }
//
//
//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     **/
//    function sortFormIni($obj){
//        $arrGroup1 =& $obj->arrItemData[1];
//
//        // 入れ替え
//        $array = array();
//        foreach($arrGroup1 as $key => $data){
//            switch($key){
//                case 7:     // middle name
//                    $array[$key]= $data;
//                    $array[26]  = $arrGroup1[26];
//                    $array[27]  = $arrGroup1[27];
//                    $array[28]  = $arrGroup1[28];
//                    $array[63]  = $arrGroup1[63];
//                    $array[64]  = $arrGroup1[64];
//                    break;
//
//                case 26:    // etc1
//                case 27:    // etc2
//                case 28:    // etc3
//                case 63:    // etc4
//                case 64:    // etc5
//                    break;
//
//                default:
//                    $array[$key] = $data;
//                    break;
//            }
//        }
//        $arrGroup1 = $array;
//    }
//
//
//
//    // ------------------------------------------------------
//    // ▽メールカスタマイズ
//    // ------------------------------------------------------
//
//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }



    function __constructMngCSV($obj){
        // CSVに出力
        $obj->arrItemData[1][137]['item_view'] = "";    // 申込確認書兼振込依頼書送付日
        $obj->arrItemData[1][138]['item_view'] = "";    // 振込依頼書(再)発行日
        $obj->arrItemData[1][139]['item_view'] = "";    // 督促日
        $obj->arrItemData[1][140]['item_view'] = "";    // 参加証発送日
        $obj->arrItemData[1][141]['item_view'] = "";    // 入金日
        $obj->arrItemData[1][142]['item_view'] = "";    // 返金日
        $obj->arrItemData[1][143]['item_view'] = "";    // 返金額
        $obj->arrItemData[1][144]['item_view'] = "";    // 備考欄
    }



    /** CSVヘッダ生成 */
    function entry_csv_entryMakeHeader($obj, $all_flg=false){
        // グループ別に生成
        $groupHeader = array("1" => array(), "2" => array(), "3" => array());

        // グループ1ッダ
        $groupHeader[1] = $obj->entryMakeHeader1($obj, $all_flg);

        // グループ2ヘッダ
        $groupHeader[2] = $obj->entryMakeHeader2($obj, $all_flg);

        // グループ3ヘッダ
        $groupHeader[3] = $obj->entryMakeHeader3($obj, $all_flg);


        $header = $groupHeader[1];
        if(count($groupHeader[3]) > 0) $header = array_merge($header, $groupHeader[3]);
        if(count($groupHeader[2]) > 0 && !$all_flg) $header = array_merge($header, $groupHeader[2]);
        $header[] = $obj->fix.$GLOBALS["csv"]["status"].$obj->fix;
        $header[] = $obj->fix.$GLOBALS["csv"]["rdate"] .$obj->fix;
        $header[] = $obj->fix.$GLOBALS["csv"]["udate"] .$obj->fix;
        $header[] = $obj->fix."キャンセル受付日".$obj->fix;

        // 生成
        $ret_buff = implode($obj->delimiter, $header)."\n";
        return $ret_buff;
    }



    /** CSV出力データ生成 */
    function entry_csv_entryMakeData($obj, $pa_param, $all_flg=false){
        foreach($pa_param as $key=>$data) {
            // 特殊 # 配列の項目の対応
            $item_id = substr($key, 5);
            if(isset($this->formarray) && !empty($this->formarray)){
                if(in_array($item_id, $this->formarray)) continue;
            }
            $pa_param[$key] = str_replace('"', '""', $data);
        }


        // グループ別に生成
        $groupBody = array("1" => array(), "2" => array(), "3" => array());

        // グループ1 データ生成
        $groupBody[1] = $obj->entryMakeData1($obj, $pa_param, $all_flg);

        // グループ2 データ生成
        $groupBody[2] = $obj->entryMakeData2($obj, $pa_param, $all_flg);

        // グループ3 データ生成
        $groupBody[3] = $obj->entryMakeData3($obj, $pa_param, $all_flg);


        $body = $groupBody[1];
        if(count($groupBody[3]) > 0) $body = array_merge($body, $groupBody[3]);
        if(count($groupBody[2]) > 0 && !$all_flg) $body = array_merge($body, $groupBody[2]);
        //状態
        $body[] = ($pa_param["status"] == "0") ? "-" : $obj->fix.$GLOBALS["entryStatusList"][$pa_param["status"]].$obj->fix;
        $body[] = $obj->fix.$pa_param["insday"].$obj->fix;    // 登録日
        $body[] = $obj->fix.$pa_param["upday"] .$obj->fix;    // 更新日
        $body[] = $obj->fix.$pa_param["cancelday"] .$obj->fix;    // キャンセル受付日

        $ret_buff .= implode($obj->delimiter, $body);
        $ret_buff .= "\n";

        return $ret_buff;
    }



    /**
     * 登録番号の採番を登録経路によってわける
     *
     */
    function getEntryNumber($obj) {
        // 登録経路：ユーザ
        if($obj->admin_flg == ""){
            $objDb = new DbGeneral();
            $saiban = $obj->o_entry->getEntryNum($objDb, $obj->formdata["form_id"]);

            //採番後のエントリー番号
            $obj->entry_number = $saiban["entory_count"] + 1;

            //--------------------------------------
            //エントリー番号採番
            //--------------------------------------
            $rs = $obj->o_entry->updEntryNum($objDb, $obj->formdata["form_id"], $obj->entry_number);
            if(!$rs) {
                return array(false, "", "");
            }
            return;
        }

        // 登録経路：管理者
        $number = glob($obj->customDir."entry_number/[0-9]*");
        if(!isset($number[0])){
            $number = array($obj->customDir."entry_number/1000");
            $create = file_put_contents($number[0], "create_file", FILE_APPEND | LOCK_EX);
            if(!$create){
                log_class::printLog("Fatal Error:ファイル生成で競合が発生".print_r($obj->arrParam, true).print_r($obj->arrForm, true));
            }
        }

        // 採番
        $entry_number = intval(basename($number[0]))+1;
        // 採番用ファイルの更新
        rename($number[0], $obj->customDir."entry_number/".$entry_number);
        $obj->entry_number = $entry_number;
        return $entry_number;
    }



    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $body_header = Usr_mail::makeMailBody_header($obj, $user_id, $passwd, $exec_type);

        $tag = "";
        // クレジットカード
        if($obj->arrForm["method"] == 1){
            $tag = "bank";
        }else
        // 銀行振込
        if($obj->arrForm["method"] == 2){
            $tag = "credit";
        }

        // タグ除去
        if(strlen($tag) > 0){
            $pattern = sprintf("!<%s.*?>.*?</%s>!ims", $tag, $tag);
            $body_header = preg_replace($pattern, "", $body_header);
        }
        $body_header = strip_tags($body_header);

        return $body_header;
    }



    /** 1ページ目 項目情報初期化 */
    function _init1($obj){
        $key = Usr_init::_init1($obj);
        $key[] = array("chkemail_28", "E-mail確認用", array(),    array(),    "",    0);

        return $key;
    }




    function _check1($obj) {
        Usr_Check::_check1($obj);

        //メールアドレス一致チェック
        $item_id = 28;
        $key     = "edata".$item_id;
        if(strlen($obj->arrParam[$key]) > 0){
            if($obj->arrParam[$key] != $_REQUEST["chkemail_".$item_id]){
                $msg = $obj->itemData[$item_id]['strip_tags']."と".$obj->itemData[$item_id]['strip_tags']."（確認用）の入力内容が一致していません。";
                $obj->objErr->addErr($msg, $key);
            }
        }
    }



    function getEntry($obj) {
        Usr_entryDB::getEntry($obj);

        $arrForm = $GLOBALS["session"]->getVar("form_param1");
        $arrForm["chkemail_28"] = $arrForm["edata28"];
        $obj->arrForm["chkemail_28"] = $arrForm["chkemail_28"];

        $GLOBALS["session"]->setVar("form_param1", $arrForm);
    }



    /*
     * 完了メールの送信
     */
    function sendCompleteMail($obj, $user_id, $passwd, $exec_type=1) {

        if(in_array($obj->form_id, $obj->sendmail_not)) return;

        // 英語フォームはUTF-8送信
        $isUTF8 = ($obj->formdata["lang"] == LANG_JPN) ? false : true;

        //本文
        $ws_mailBody = $obj->makeMailBody($user_id, $passwd, $exec_type);

        // 応募者への送信先
        $user_mail = strlen($obj->arrForm["edata28"]) > 0 ? $obj->arrForm["edata28"] : $obj->arrForm["edata25"];

        // 登録者へのメール
        if($obj->admin_flg == "" && $user_mail != ""){
            $obj->o_mail->SendMail($user_mail, $obj->subject, $ws_mailBody, $obj->formdata["form_mail"], $obj->formWord["word13"], $isUTF8);
        }

        // 管理者へのメール
        $obj->o_mail->SendMail($obj->formdata["form_mail"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"],$obj->formWord["word13"], $isUTF8);
        return;
    }



    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function __constructMng($obj){
        // 所属を検索項目に含める
        $obj->searchkey[] = "edata10";

        $days = array("cancel", "edata137", "edata138", "edata139", "edata140", "edata141", "edata142");
        foreach($days as $key){
            $obj->searchkey[] = $key."_syear";
            $obj->searchkey[] = $key."_smonth";
            $obj->searchkey[] = $key."_sday";
            $obj->searchkey[] = $key."_eyear";
            $obj->searchkey[] = $key."_emonth";
            $obj->searchkey[] = $key."_eday";
        }


        $obj->searchkey[] = "search_cancel_sday";
        $obj->searchkey[] = "search_137_sday";
        $obj->searchkey[] = "search_138_sday";
        $obj->searchkey[] = "search_139_sday";
        $obj->searchkey[] = "search_140_sday";
        $obj->searchkey[] = "search_141_sday";
        $obj->searchkey[] = "search_142_sday";
        $obj->searchkey[] = "search_cancel_eday";
        $obj->searchkey[] = "search_137_eday";
        $obj->searchkey[] = "search_138_eday";
        $obj->searchkey[] = "search_139_eday";
        $obj->searchkey[] = "search_140_eday";
        $obj->searchkey[] = "search_141_eday";
        $obj->searchkey[] = "search_142_eday";

        // 一括処理：日付
        $operationDate = array();
        $operationDate["rdate"]       = "申込受付日";
        $operationDate["cancel_date"] = "キャンセル受付日";
        $operationDate["edata137"]    = "申込確認書兼振込依頼書送付日";
        $operationDate["edata138"]    = "振込依頼書(再)発行日";
        $operationDate["edata139"]    = "督促日";
        $operationDate["edata140"]    = "参加証発送日";
        $operationDate["edata141"]    = "入金日";
        $operationDate["edata142"]    = "返金日";
        $obj->operationDate = $operationDate;
        if(method_exists($obj, "assign")){
            $obj->assign("operationDate", $obj->operationDate);
        }

        // デフォルトの表示件数
        $obj->limit = 100;

        // ソート順の拡張
        $obj->sort_name[3] = "受付日";

    }


    //---------------------------------------
    // ▽Mng 詳細カスタマイズ
    //---------------------------------------
    function __constructMngDetail($obj){
        $obj->_processTemplate = "Mng/entry/include/{$obj->form_id}/{$obj->form_id}Mng_entry_detail.html";
        $obj->assign("eid", $obj->eid);
    }


    // Mng 詳細 # 保存ボタンクリック
    function Mng_detail_saveAction($obj){
        include_once(MODULE_DIR.'entry_ex/Usr_entry.class.php');

        // フォームパラメータ # 任意項目
        $keys = array(137,138,139,140,141,142,143,144);

        // 更新データ生成
        $param = GeneralFnc::convertParam($this->_initOption($obj, $keys), $_REQUEST);
        if($obj->arrData['entry_way'] == ENTRY_WAY_ADMIN){
            $param['rdate'] = $_REQUEST['rdate']; // FAXのみ受付日を更新対象
        }
        $param['cancel_date'] = $_REQUEST['cancel_date'];
        $param["udate"] = "NOW";

        // エラーチェック
        if($obj->arrData['entry_way'] == ENTRY_WAY_ADMIN){
            // 受付日のフォーマットチェック
            $parserDate = strptime($param['rdate'], "%Y-%m-%d");
            if(!$parserDate || strlen($parserDate['unparsed']) > 0){
                $obj->arrErr["rdate"] = "申し込み受付日の入力内容をご確認ください。";
                $obj->arrData = array_merge($obj->arrData, $param);
                return;
            }
        }
        // 空白文字をNULLにする
        foreach($param as $_key => $_val){
            if(strlen($_val) == 0) $param[$_key] = NULL;
        }

        // エントリー情報取得
        $objDb = new DbGeneral();
        $objDb->begin();
        $where = "form_id = 39 and eid = ".$obj->eid;
        $rs = $objDb->update("entory_r", $param, $where);
        if(!$rs) {
            $obj->onload = "alert('登録情報の更新に失敗しました。');";
            $obj->arrData = array_merge($obj->arrData, $param);
            $objDb->rollback();
            return;
        }

        $objDb->commit();
        $obj->getEntry();

        // indexへ戻りアラート表示
        $obj->onload = "f_back();";
        $GLOBALS["session"]->setVar("onload", "alert('登録情報の更新に成功しました。');");
    }


    /* Mng # エントリー一覧 # 戻るボタン */
    function Mng_index_backAction($obj){
        // eid#checkboxの保持
        $arrEid = Mng_function::gf_set_eid();
        $obj->assign("arrEid", $arrEid);

        // 詳細画面からのjsOnload
        $obj->onload = $GLOBALS["session"]->getVar("onload");
        $GLOBALS["session"]->unsetVar("onload");

        // おまじない
        if(isset($obj->arrItemData[1][135]['select'])){
            foreach($obj->arrItemData[1][135]['select'] as $_key => $item){
                $_REQUEST['edata135'.$_key] = NULL;
                $obj->arrForm['edata135'.$_key] = NULL;
            }
        }
        return;
    }


    function _initOption($obj, $keys){
        $key = array();

        foreach($keys as $_key => $item_id){
            Usr_init::_initNini($obj, $key, $item_id);
        }
        return $key;
    }



    //---------------------------------------
    // ▽Mng 一括メール送信カスタマイズ
    //---------------------------------------

	function Mng_mailsend_lfSendMail($obj) {
		mb_internal_encoding("utf-8");
		mb_language("japanese");
		$obj->count = 0;

		foreach($obj->arrData as $key=>$val) {
			$obj->arrData[$key]["send"] = "0";
			$to = strlen($val["edata28"]) > 0 ? $val["edata28"] :$val["edata25"];

			if($to == "") continue;
			$subject = $obj->arrForm["mail_ttl"];
			$body    = $obj->arrForm["mail_body"];
			$body    = $obj->replaceStr($body, $val);
            $body    =  str_replace(array("\r\n", "\r"), "\n", $body);

			//送信元
			if( $obj->formWord["word13"] != "" ){
				$ps_fromname = mb_convert_encoding($obj->formWord["word13"], "ISO-2022-JP", "utf-8");
				$ps_from = mb_encode_mimeheader($ps_fromname)."<".$GLOBALS["userData"]["form_mail"]."> ";
	        }
	        else{
	        	$ps_from = $GLOBALS["userData"]["form_mail"];
	        }

			$head  = "From: ".$ps_from;
			$head .= "\n";
			$head .= "Cc: ".$GLOBALS["userData"]["form_mail"];
			//$head .= "\n";
			//$head .= "Bcc: ".SYSTEM_MAIL;

			$rs = mb_send_mail($to, $subject, $body, $head);
			if($rs) {
				$obj->arrData[$key]["send"] = "1";
				$obj->count++;
			}
			$obj->arrData[$key]["send_date"] = date("Y-m-d H:i:s");
		}
    }


    // 送信履歴を残す
    function Mng_mailsend_sevehistory($obj){
        $objDb = new DbGeneral();
        $objDb->begin();
        $obj->form_id = 39;

        // 履歴を残す
        $param = array();
        $param["form_id"]  = $obj->form_id;
        $param["subject"]  = $obj->arrForm["mail_ttl"];
        $param["body"]     = $obj->arrForm["mail_body"];
        $param["send_num"] = count($obj->arrData);
        $param["success"]  = $obj->count;
        $rs = $objDb->insert("mail_history", $param);

        if(!$rs) {
            $objDb->rollback();
            $obj->complete("履歴の登録に失敗しました。");
        }

        $where = "form_id = ".$obj->form_id;
        $send_id = $objDb->get("max(send_id)", "mail_history", $where);

        if(!$send_id) {
            $objDb->rollback();
            $obj->complete("送信番号の取得に失敗しました。");
        }

        unset($param);
        unset($where);

        foreach($obj->arrData as $val) {
            $param = array();
            $param["send_id"]     = $send_id;
            $param["eid"]         = $val["eid"];
            $param["mailaddress"] = strlen($val["edata28"]) > 0 ? $val["edata28"] :$val["edata25"];
            $param["result"]      = $val["send"];
            $param["send_date"]   = $val["send_date"];

            $rs = $objDb->insert("mail_sendentry", $param);
            if(!$rs) {
                $objDb->rollback();
                $obj->complete("履歴の登録に失敗しました。");
            }
            unset($param);
        }
        $objDb->commit();
    }



    //---------------------------------------
    // ▽Mng 検索カスタマイズ
    //---------------------------------------

    /**
     * 入力チェック
     *
     * @access public
     * @return object
     */
    function mng_index_check($obj){
        $arrKeys = array(
                        array(array(  "cancel_syear",   "cancel_smonth",   "cancel_sday")  , array(  "cancel_eyear",   "cancel_emonth",   "cancel_eday"))
                      , array(array("edata137_syear", "edata137_smonth", "edata137_sday")  , array("edata137_eyear", "edata137_emonth", "edata137_eday"))
                      , array(array("edata138_syear", "edata138_smonth", "edata138_sday")  , array("edata138_eyear", "edata138_emonth", "edata138_eday"))
                      , array(array("edata139_syear", "edata139_smonth", "edata139_sday")  , array("edata139_eyear", "edata139_emonth", "edata139_eday"))
                      , array(array("edata140_syear", "edata140_smonth", "edata140_sday")  , array("edata140_eyear", "edata140_emonth", "edata140_eday"))
                      , array(array("edata141_syear", "edata141_smonth", "edata141_sday")  , array("edata141_eyear", "edata141_emonth", "edata141_eday"))
                      , array(array("edata142_syear", "edata142_smonth", "edata142_sday")  , array("edata142_eyear", "edata142_emonth", "edata142_eday"))
                   );
        $arrNames= array(
                        array("キャンセル受付日（開始）"            , "キャンセル受付日（終了）")
                      , array("申込確認書兼振込依頼書送付日（開始）", "申込確認書兼振込依頼書送付日（終了）")
                      , array("振込依頼書(再)発行日（開始）"        , "振込依頼書(再)発行日（終了）")
                      , array("督促日（開始）"                      , "督促日（終了）")
                      , array("参加証発送日（開始）"                , "参加証発送日（終了）")
                      , array("入金日（開始）"                      , "入金日（終了）")
                      , array("返金日（開始）"                      , "返金日（終了）")
                   );
        $arrKey  = array(
                        array("search_cancel_sday", "search_cancel_eday")
                      , array("search_137_sday"   , "search_137_eday")
                      , array("search_138_sday"   , "search_138_eday")
                      , array("search_139_sday"   , "search_139_eday")
                      , array("search_140_sday"   , "search_140_eday")
                      , array("search_141_sday"   , "search_141_eday")
                      , array("search_142_sday"   , "search_142_eday")
                   );

        foreach($arrKeys as $_key => $_arrKeys){
            $errors = array();
            foreach($_arrKeys as $mode => $keys){
                // 入力日付のチェック
                $name = $arrNames[$_key][$mode];
                $key  = $arrKey[$_key][$mode];
                $errors[] = $obj->check_date($obj->objErr, $obj->arrForm, $keys, $name, $key, $mode);
            }

            // 開始日、終了日の妥当性チェック
            if($errors[0] && $errors[1]){
                // 開始、終了どちらも入力あり
                $error_key = "";
                $vals = array();
                foreach($arrKey[$_key] as $key){
                    $vals[] = $obj->arrForm[$key];
                    $error_key = $key;
                }
                $names = array();
                foreach($arrNames[$_key] as $name){
                    $names[] = $name;
                }
                if(strlen($vals[0])> 0 && strlen($vals[1]) > 0){
                    if(!$obj->objErr->isDateAgo($vals[0], $vals[1])) {
                        $obj->objErr->addErr("{$names[1]}は、{$names[0]}よりも未来に設定して下さい。", $error_key);
                    }
                }
            }
        }
    }


    function Mng_buildWhere_after($obj, $condition, $where){
        // キャンセル受付日：開始
        if(strlen($condition["search_cancel_sday"]) > 0){
            $where[] = "v_entry.cancel_date >= '{$condition['search_cancel_sday']} 00:00:00'";
        }
        // キャンセル受付日：終了
        if(strlen($condition["search_cancel_eday"]) > 0){
            $where[] = "v_entry.cancel_date <= '{$condition['search_cancel_eday']} 23:59:59'";
        }


        // 申込確認書兼振込依頼書送付日：開始
        if(strlen($condition["search_137_sday"]) > 0){
            $where[] = "v_entry.edata137::timestamp >= '{$condition['search_137_sday']} 00:00:00'";
        }
        // 申込確認書兼振込依頼書送付日：終了
        if(strlen($condition["search_137_eday"]) > 0){
            $where[] = "v_entry.edata137::timestamp <= '{$condition['search_137_eday']} 23:59:59'";
        }


        // 振込依頼書(再)発行日：開始
        if(strlen($condition["search_138_sday"]) > 0){
            $where[] = "v_entry.edata138::timestamp >= '{$condition['search_138_sday']} 00:00:00'";
        }
        // 振込依頼書(再)発行日：終了
        if(strlen($condition["search_138_eday"]) > 0){
            $where[] = "v_entry.edata138::timestamp <= '{$condition['search_138_eday']} 23:59:59'";
        }


        // 督促日：開始
        if(strlen($condition["search_139_sday"]) > 0){
            $where[] = "v_entry.edata139::timestamp >= '{$condition['search_139_sday']} 00:00:00'";
        }
        // 督促日：終了
        if(strlen($condition["search_139_eday"]) > 0){
            $where[] = "v_entry.edata139::timestamp <= '{$condition['search_139_eday']} 23:59:59'";
        }


        // 参加証発送日：開始
        if(strlen($condition["search_140_sday"]) > 0){
            $where[] = "v_entry.edata140::timestamp >= '{$condition['search_140_sday']} 00:00:00'";
        }
        // 参加証発送日：終了
        if(strlen($condition["search_140_eday"]) > 0){
            $where[] = "v_entry.edata140::timestamp <= '{$condition['search_140_eday']} 23:59:59'";
        }


        // 入金日：開始
        if(strlen($condition["search_141_sday"]) > 0){
            $where[] = "v_entry.edata141::timestamp >= '{$condition['search_141_sday']} 00:00:00'";
        }
        // 入金日：終了
        if(strlen($condition["search_141_eday"]) > 0){
            $where[] = "v_entry.edata141::timestamp <= '{$condition['search_141_eday']} 23:59:59'";
        }


        // 返金日：開始
        if(strlen($condition["search_142_sday"]) > 0){
            $where[] = "v_entry.edata142::timestamp >= '{$condition['search_142_sday']} 00:00:00'";
        }
        // 返金日：終了
        if(strlen($condition["search_142_eday"]) > 0){
            $where[] = "v_entry.edata142::timestamp <= '{$condition['search_142_eday']} 23:59:59'";
        }

        return $where;
    }



    function Mng_buildOrder_after($obj, $condition, $wk_order_by){
        if($condition["sort_name"] != "" && $condition["sort"] != ""){
            switch($condition["sort_name"]){
                // ソート順に登録日を追加
                case "3":
                    $wk_order_by = "rdate ";
                    if($condition["sort"] == "1"){
                        $wk_order_by .= "desc";
                    }else{
                        $wk_order_by .= "asc";
                    }
                    break;

                default: break;
            }
        }
        return $wk_order_by;
    }



    //---------------------------------------
    // ▽Mng 一括処理カスタマイズ
    //---------------------------------------

    function __constructMngPlural($obj){
//        $GLOBALS["entryStatusList"] = $GLOBALS["entryStatusList"] + $obj->operationDate;
        $GLOBALS["entryStatusList"][6] = "日付一括更新";
    }

    function mng_plural_premain($obj){
        $obj->_processTemplate = "Mng/entry/include/39/39Mng_edit_plural.html";
    }

    /* 一括処理種別の設定 */
    function mng_plural_setAction($obj){
        $status = intval($obj->arrForm["set_stat"]);

        // 削除
        if($status == 4){
            $obj->exec_name  = "削除";
            $obj->actionName = "delEntry";
            return;
        }

        // 支払いステータスの変更
        if($status == 5){
            $obj->exec_name  = "ステータス変更";
            $obj->actionName = "chgPaymentStatus";

            if(!isset($obj->arrForm["chg_status"]) || $obj->arrForm["chg_status"] == "") {
                $obj->complete("変更するステータスを選択して下さい。");
            }

            if(!is_numeric($obj->arrForm["chg_status"])) {
                Error::showErrorPage("処理に必要なデータの取得に失敗しました。");
                exit;
            }

            $msg = "変更するステータス：".$GLOBALS["paymentstatusList"][$obj->arrForm["chg_status"]]."<br /><br />";
            $obj->assign("freemsg", $msg);
            return;
        }

        // 日付一括更新
        if($status == 6){
            $obj->exec_name  = $GLOBALS["entryStatusList"][$status];
            $obj->actionName = "chgDateStatus";

            if(!isset($obj->arrForm["ope_date_status"]) || $obj->arrForm["ope_date_status"] == "") {
                $obj->complete("変更するステータスを選択して下さい。");
            }

            // 一括処理する日付
            $ope_date_status = $obj->arrForm["ope_date_status"];
            if(!array_key_exists($ope_date_status, $obj->operationDate)) {
                Errorarray_key_existsshowErrorPage("処理に必要なデータの取得に失敗しました。");
                exit;
            }

            // 更新する日付
            $ope_date = $obj->arrForm["ope_date"];
            // 日付の妥当性

            $msg = "一括処理する日付：".$obj->operationDate[$ope_date_status]."({$ope_date})<br /><br />";
            $obj->assign("freemsg", $msg);
            return;
        }


        // 採択
        // 初期状態
        // 不採択
        // キャンセル
        $obj->exec_name  = $GLOBALS["entryStatusList"][$status];
        $obj->actionName = "chgStatus";
    }


    // 日付の一括処理
    function chgDateStatus($obj, $set_stat){
        $objDb = new DbGeneral();
        $operation = $obj->arrForm["ope_date_status"];

        $param = array();
        $param[$operation] = $obj->arrForm["ope_date"];
        $param["udate"] = "NOW";

        $from  = "entory_r";
        $where = array();
        $where[] = "form_id = 39 and eid in (".implode(",", $obj->arrForm["eid"]).")";
        // 申込受付日は「FAX」のみ更新対象
        if($operation == "rdate"){
            $where[] = "entry_way = ".ENTRY_WAY_ADMIN;
        }
        $rs = $objDb->update($from, $param, $where);

        // チェック情報を破棄
        $GLOBALS["session"]->unsetVar("eid");

        $_REQUEST["ope_date_status"] = "";
        $_REQUEST["ope_date"]        = "";

        if(!$rs) return false;
        return true;
    }

}
