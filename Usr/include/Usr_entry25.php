<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry25 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        // 支払方法 # 銀行振込で固定
        unset($GLOBALS["method_J"][1]);
        // 支払方法 # 銀行振込の選択済みで固定
        $_REQUEST['method'] = 2;

        // お支払情報入力ページをスキップ
        $obj->payment_not = array($form_id);

        // jQueryを読み込む
        $obj->useJquery = true;

//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        // 金額
        $obj->wa_price2 = array();
        $c = count($obj->wa_price);
        foreach($obj->wa_price as $_key => $item){
            // 医師
            if($_key < $c/3){
                $obj->wa_price2[1][] = $_key;
                continue;
            }
            // コメディカル
            if($_key >= $c*2/3){
                $obj->wa_price2[2][] = $_key;
                continue;
            }
        }

        $keys = array_flip($obj->wa_price2[1]);
        $vals = $obj->wa_price2[2];
        $obj->wa_price2 = array_combine($keys, $vals);
        $obj->assign("va_price2", $obj->wa_price2);

        // 確認画面の場合は合計金額を表示
        if($obj->block == "4"){
            $obj->arrForm["total_price"] = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        }
    }

    function setFormIni($obj){
        Usr_initial::setFormIni($obj);

        // 医師とコメディカルに分ける
        $obj->item63 = array(1=>"医師", 2=>"医師3年目", 3=>"コメディカル");

        // 質問4
        $obj->arrItemData[1][63]['select2'] = array();
        $c = count($obj->arrItemData[1][63]['select']);
        foreach($obj->arrItemData[1][63]['select'] as $_key => $item){
            // 医師
            if($_key <= $c/3){
                $obj->arrItemData[1][63]['select2'][1][$_key] = $item;
                continue;
            }
            // コメディカル
            if($_key > $c*2/3){
                $obj->arrItemData[1][63]['select2'][3][$_key] = $item;
                continue;
            }
            // 医師3年目
            $obj->arrItemData[1][63]['select2'][2][$_key] = $item;
        }

        if(isset($obj->_smarty)){
            $obj->assign("item63", $obj->item63);
            $obj->assign("arrItemData", $obj->arrItemData);
        }
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


    // エラーチェック # ブロック1
    function _check1($obj){
        Usr_Check::_check1($obj);

        $group_id = 1;

        // 質問1 # 医師を選択したらテキスト必須
        $item_id = 26;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && in_array($obj->arrParam[$key], array(1,2))){
            $item_id = 27;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr(sprintf($method, $name), $key);
            }
        }

        $item_id = 26;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
            $edata26 = $obj->arrParam[$key];

            $item_id = 63;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && strlen($obj->arrParam[$key]) > 0){
                // 質問4で質問1関連を選択したかチェック
                $edata63 = $obj->arrParam[$key];
                if(!isset($obj->arrItemData[1][63]['select2'][$edata26][$edata63])){
                    $obj->objErr->addErr('質問1または質問4を確認してください。', $key);
                }

                // 質問4で宿泊費を含場合は質問5の懇親会には参加する[強制]
                if(in_array($obj->arrParam[$key], array(1,2,5,6,9,10))){
                    $item_id = 64;
                    $key = "edata".$item_id;
                    if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] != 1){
                        $obj->objErr->addErr('懇親会に参加してください。', $key);
                    }
                }
            }
        }

//        // 質問4 # 宿泊不要の場合は質問5を必須
//        $item_id = 63;
//        $key = "edata".$item_id;
//        if(Usr_init::isset_ex($obj, $group_id, $item_id) && in_array($obj->arrParam[$key], array(2,4))){
//            $item_id = 64;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }

    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makePaymentMethodBody2($obj, $total){
        $body_pay = "";
        return $body_pay;
    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 4:     // 名フリガナ
                    $array[$key]= $data;
                    $array[71]  = $arrGroup1[71];
                    break;

                case 71:    // 性別
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



   // ------------------------------------------------------
   // ▽CSVカスタマイズ
   // ------------------------------------------------------

    /**
     * CSVヘッダ生成
     */
    function payment_payMakeHeader($obj, $all_flg=false){
        $header   = array();
        // payment_csvのみ出力
        if(!$all_flg){
            $header[] = "応募者氏名";
            $header[] = "応募者氏名（カナ）";
        }
        $header[] = "取引ID";
        $header[] = "お支払い方法";
        $header[] = "ステータス";

        $header[] = "支払期限";
//        $header[] = "クレジット番号";
//        $header[] = "カード会社";
//        $header[] = "カード名義人";
//        $header[] = "有効期限";
//        $header[] = "クレジット支払い区分";
//        $header[] = "クレジット支払い回数";
////        $header[] = "セキュリティコード";
//
//        $header[] = "お振込み人名義";
//        $header[] = "お支払銀行名";
//        $header[] = "お振込み日";

        $header[] = "コンビニ　ショップコード";
        $header[] = "姓";
        $header[] = "名";
        $header[] = "セイ";
        $header[] = "メイ";
        $header[] = "電話番号";
        $header[] = "メールアドレス";
        $header[] = "郵便番号";
        $header[] = "住所";
        $header[] = "成約日";
        $header[] = "決済機関コード";
        //$header[] = "支払詳細";


        //------------------------------
        //決済項目
        //------------------------------
        foreach($obj->wa_ather_price as $data){
            $head = GeneralFnc::smarty_modifier_del_tags($data);
            $header[] = strip_tags(str_replace(array("_"), " ",$head));
        }

        $header[] = "合計金額";
        $header[] = "登録日";
        $header[] = "更新日";

        //生成
        $ret_buff  = implode($obj->delimiter, $header);
        $ret_buff .="\n";

        return $ret_buff;
    }


    /**
     * CSV出力データ生成
     */
    function payment_payMakeData($obj, $pa_param, $pa_detail, $all_flg=false){
        $ret_buff = "";
        $wk_buff = "";

        // payment_csvのみ出力
        if(!$all_flg){
            //応募者氏名
            $wk_buff[] =  "\"".$pa_param["edata1"]."　".$pa_param["edata2"]. "\"";

            //応募者氏名（カナ）
            $wk_buff[] =  "\"".$pa_param["edata3"]."　".$pa_param["edata4"]. "\"";
        }

        //取引ID
        $wk_buff[] = "\"".$pa_param["order_id"]."\"";

        //お支払い方法
        $wk_buff[] = ($pa_param["payment_method"] != "") ? "\"".$obj->wa_method[$pa_param["payment_method"]]."\"" : "";

        //ステータス
        $wk_buff[] = ($pa_param["payment_status"] != "") ? "\"".$obj->wa_pay_status[$pa_param["payment_status"]]."\"" : "";



        //支払期限
        $wk_buff[] = "\"".$pa_param["limit_date"]."\"";

//        //クレジット番号
//        $wk_buff[] = ($pa_param["c_number"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_number"])."\"" : "";
//
//        //カード会社
//        $wk_buff[] = ($pa_param["c_company"] != "") ? "\"".$obj->wa_card_type[$pa_param["c_company"]]."\"" : "";
//
//        //カード名義人
//        $wk_buff[] = "\"".$pa_param["c_holder"]."\"";
//
//        //有効期限
//        $wk_buff[] = ($pa_param["c_date"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_date"])."\"" : "";
//
//
//        //クレジット支払い方法
//        $wk_buff[] = ($pa_param["c_method"] != "") ? "\"".$obj->wa_jpo1[$pa_param["c_method"]]."\"" : "";
//
//        //クレジット支払い回数
//        $wk_buff[] = "\"".$pa_param["c_split"]."\"";
//
//        //セキュリティコード
////        $wk_buff[] = ($pa_param["c_scode"] != "") ? "\"".$obj->o_crypt->crypt_decode($pa_param["c_scode"])."\"" : "";
//
//
//        //お振込み人名義
//        $wk_buff[] = "\"".$pa_param["lessee"]."\"";
//
//        //お支払銀行名
//        $wk_buff[] = "\"".$pa_param["bank"]."\"";
//
//
//        //お振込み日
//        $wk_buff[] = "\"".$pa_param["closure"]."\"";


        //コンビニ　ショップコード
        $wk_buff[] = "";

        //姓
        $wk_buff[] = "\"".$pa_param["name1"]."\"";

        //名
        $wk_buff[] = "\"".$pa_param["name2"]."\"";

        //セイ
        $wk_buff[] =  "\"".$pa_param["kana1"]."\"";

        //メイ
        $wk_buff[] = "\"".$pa_param["kana2"]."\"";

        //電話番号
        $wk_buff[] = "\"".$pa_param["tel"]."\"";

        //メールアドレス
        $wk_buff[] = "\"".$pa_param["mail"]."\"";


        //郵便番号
        $wk_buff[] = "\"".$pa_param["zip"]."\"";

        //住所
        $wk_buff[] = "\"".$pa_param["addr"]."\"";

        //成約日
        $wk_buff[] = "\"".$pa_param["sold_date"]."\"";

        //決済機関コード
        $wk_buff[] = "\"".$pa_param["bank_code"]."\"";


        //支払詳細
        $wk_detail = "";

        if($pa_detail){
            $wk_buff = array_merge($wk_buff, $obj->pay_c($obj, $pa_detail));
        }else{
            foreach($obj->wa_ather_price as $ather_name){
                $wk_buff[] = "";
            }
        }



        //金額
        $wk_buff[] = "\"".$pa_param["price"]."\"";



        //登録日
        $wk_buff[] = ($pa_param["insday"] != "") ? "\"".$pa_param["insday"]."\"" : "";

        //更新日
        $wk_buff[] = ($pa_param["upday"] != "") ? "\"".$pa_param["upday"]."\"" : "";


        $ret_buff = implode($obj->delimiter, $wk_buff);
        $ret_buff .= "\n";
        return $ret_buff;
    }




    function csvfunc63($obj, $group, $pa_param, $item_id){
        $str = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), false);
        $str = trim($str, ",");

        // 選択肢の先頭に「医師」「コメディカル」をつける
        $item_id = 26;
        $head = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), false);
        return $head."：".$str;
    }


}
