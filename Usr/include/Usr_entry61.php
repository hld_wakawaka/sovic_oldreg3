<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hunglead doishun
 *        
 */

require_once(ROOT_DIR."customDir/61/package/Mng_entry61.php");

class Usr_Entry61 extends Mng_Entry61 {

    // 変数定義
    protected $group1 = array(27, 28, 63, 64, 69, 70, 71, 72, 73);            // May 15
    protected $group2 = array(74, 75, 76, 77, 78, 79, 80, 81, 82);            // May 16
    protected $group3 = array(83, 115, 116, 117, 118, 119, 120, 121, 122);    // May 17
    private $arrGroup = array();
    
    /**
     * Mayの日付単位で選択したitem_idを返却する
     *   メール、CSV、確認画面、Mng詳細での利用を想定
     * 
     * @see    self::$group1, self::$group2, self::$group3
     * @param  array   $group   グループ配列
     * @param  array   $arrForm 入力配列
     * @return integer $item_id グループに対応した選択値のitem_id # 未選択の場合はNULLを返却
     **/
    protected static function getMaySelectItemId($group, $arrForm){
        $item_id = NULL;
        foreach($group as $_key => $_item_id){
            $key  = "edata" . $_item_id;
            if(!isset($arrForm[$key])) $arrForm [$key] = "";
            if(strlen($arrForm [$key]) == 0) continue;
            
            $item_id = $_item_id;
            break;
        }
        return $item_id;
    }


    /**
     * Mayの日付単位で選択したitem_idをAssignする
     *   確認画面、Mng詳細での利用を想定
     * 
     * @see    self::getMaySelectItemId
     * @param  array   $params  引数の配列
     * @param  object  $smarty  Smartyオブジェクト
     * @return NULL
     **/
    function getMaySelectItemIdSmartyfunc($params, $smarty=NULL){
        extract($params);
        if(!isset($group))  return;
        if(!isset($arrForm)) return;
        
        $item_id = self::getMaySelectItemId($group, $arrForm);
        $smarty->assign("item_id", $item_id);
        return;
    }


    /**
     * May15,16,17の入力確認画面用の項目設定を行う
     *   確認画面とMng詳細での利用を想定
     * 
     * @param  object $obj
     * @return void
     **/
    function setMayUnDisplay($obj){
        // May15
        $i = 0;
        foreach($this->group1 as $_key => $item_id){
            if($i == 0){ $i++; continue; } // 最初のitem_idは表示
            $obj->arrItemData[1][$item_id]["disp"] = 1;
        }
        // May16
        $i = 0;
        foreach($this->group2 as $_key => $item_id){
            if($i == 0){ $i++; continue; } // 最初のitem_idは表示
            $obj->arrItemData[1][$item_id]["disp"] = 1;
        }
        // May17
        $i = 0;
        foreach($this->group3 as $_key => $item_id){
            if($i == 0){ $i++; continue; } // 最初のitem_idは表示
            $obj->arrItemData[1][$item_id]["disp"] = 1;
        }
        $obj->assign("may15", $this->group1);
        $obj->assign("may16", $this->group2);
        $obj->assign("may17", $this->group3);
        $obj->assign("arrItemData", $obj->arrItemData);
    }


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj) {
        // Mayごとのitem_id定義
        $this->arrGroup = array("May15 (Fri.) "=>$this->group1, "May16 (Sat.) "=>$this->group2, "May17 (Sun.)"=>$this->group3);
        
        // 定員に達すると選択できないようにする
        $this->arrQuotaDisabled = array();  // 金額キーと残り応募可能な人数 #マイナスだとエラー
        
        // 定員数を取得
        $arrQuota = $this->getQuota($obj);
        if(count($arrQuota) > 0){
            $total = $this->getTotal($obj);
            
            foreach($total as $_key => $_val){
                if(strlen($_val) == 0) $_val = 0;
                if(!isset($arrQuota[$_key])) continue;
                
                // edata[item_id]_[選択値]の書式なので分解する
                $tmp = explode("_", str_replace("edata", "", $_key));
                $item_id = $tmp[0];
                $value   = $tmp[1];
                // 定員 - 現在の応募者数 = 残り応募可能な数
                $this->arrQuotaDisabled[$item_id][$value] = $arrQuota[$_key] - $_val;
            }
        }

        // Smartyの修飾子を追加
        if (isset($obj->_smarty)) {
            $obj->_smarty->register_function('getMaySelectItemId', array ($this, 'getMaySelectItemIdSmartyfunc'));
        }
        
        return "";
    }


    /* edata毎の各定員数を取得 */
    function getQuota($obj){
        $arrQuota = array();
        
        // ファイル名の書式：[item_id]_[選択値]_[定員数]
        $quota1 = '*';
        $quota2 = '(edata[0-9]+_[0-9])_([0-9]+)';
        
        $form_id   = $obj->o_form->formData['form_id'];
        $customDir = ROOT_DIR."customDir/".$form_id."/quota/edata/";
        $ex = glob($customDir.$quota1);
        if(is_array($ex) && count($ex) > 0){
            foreach($ex as $_key => $script){
                if(preg_match("#/".$quota2."$#u", $script, $match) !== 1) continue;
                
                $arrQuota[$match[1]] = $match[2];
            }
        }
        return $arrQuota;
    }


    /* その他決済の集計データ */
    function getTotal($obj){
        // No.14 # 編集時の集計は当事者を除外する
        //新規　or 更新判別
        if(isset($GLOBALS["entryData"]["eid"])){
            $eid = $GLOBALS["entryData"]["eid"];
        }else{
            $eid = "";
        }
        
        $addwhere = "";
        if(strlen($eid) > 0){
            $addwhere = "and eid <> ".$eid;
        }
        // 集計用SQL
        $sql =<<< _SQL_
select 
      sum(edata27_1) as edata27_1 ,sum(edata27_2) as edata27_2
     ,sum(edata28_1) as edata28_1 ,sum(edata28_2) as edata28_2
     ,sum(edata63_1) as edata63_1 ,sum(edata63_2) as edata63_2
     ,sum(edata64_1) as edata64_1 ,sum(edata64_2) as edata64_2
     ,sum(edata69_1) as edata69_1 ,sum(edata69_2) as edata69_2
     ,sum(edata70_1) as edata70_1 ,sum(edata70_2) as edata70_2
     ,sum(edata71_1) as edata71_1 ,sum(edata71_2) as edata71_2
     ,sum(edata72_1) as edata72_1 ,sum(edata72_2) as edata72_2
     ,sum(edata73_1) as edata73_1 ,sum(edata73_2) as edata73_2
     ,sum(edata74_1) as edata74_1 ,sum(edata74_2) as edata74_2
     ,sum(edata75_1) as edata75_1 ,sum(edata75_2) as edata75_2
     ,sum(edata76_1) as edata76_1 ,sum(edata76_2) as edata76_2
     ,sum(edata77_1) as edata77_1 ,sum(edata77_2) as edata77_2
     ,sum(edata78_1) as edata78_1 ,sum(edata78_2) as edata78_2
     ,sum(edata79_1) as edata79_1 ,sum(edata79_2) as edata79_2
     ,sum(edata80_1) as edata80_1 ,sum(edata80_2) as edata80_2
     ,sum(edata81_1) as edata81_1 ,sum(edata81_2) as edata81_2
     ,sum(edata82_1) as edata82_1 ,sum(edata82_2) as edata82_2
     ,sum(edata83_1) as edata83_1 ,sum(edata83_2) as edata83_2
     ,sum(edata115_1) as edata115_1 ,sum(edata115_2) as edata115_2
     ,sum(edata116_1) as edata116_1 ,sum(edata116_2) as edata116_2
     ,sum(edata117_1) as edata117_1 ,sum(edata117_2) as edata117_2
     ,sum(edata118_1) as edata118_1 ,sum(edata118_2) as edata118_2
     ,sum(edata119_1) as edata119_1 ,sum(edata119_2) as edata119_2
     ,sum(edata120_1) as edata120_1 ,sum(edata120_2) as edata120_2
     ,sum(edata121_1) as edata121_1 ,sum(edata121_2) as edata121_2
     ,sum(edata122_1) as edata122_1 ,sum(edata122_2) as edata122_2
from
    (
        select
             case when edata27 = '1' then 1 else 0 end as edata27_1
            ,case when edata27 = '2' then 1 else 0 end as edata27_2
            ,case when edata28 = '1' then 1 else 0 end as edata28_1
            ,case when edata28 = '2' then 1 else 0 end as edata28_2
            ,case when edata63 = '1' then 1 else 0 end as edata63_1
            ,case when edata63 = '2' then 1 else 0 end as edata63_2
            ,case when edata64 = '1' then 1 else 0 end as edata64_1
            ,case when edata64 = '2' then 1 else 0 end as edata64_2
            ,case when edata69 = '1' then 1 else 0 end as edata69_1
            ,case when edata69 = '2' then 1 else 0 end as edata69_2
            ,case when edata70 = '1' then 1 else 0 end as edata70_1
            ,case when edata70 = '2' then 1 else 0 end as edata70_2
            ,case when edata71 = '1' then 1 else 0 end as edata71_1
            ,case when edata71 = '2' then 1 else 0 end as edata71_2
            ,case when edata72 = '1' then 1 else 0 end as edata72_1
            ,case when edata72 = '2' then 1 else 0 end as edata72_2
            ,case when edata73 = '1' then 1 else 0 end as edata73_1
            ,case when edata73 = '2' then 1 else 0 end as edata73_2
            ,case when edata74 = '1' then 1 else 0 end as edata74_1
            ,case when edata74 = '2' then 1 else 0 end as edata74_2
            ,case when edata75 = '1' then 1 else 0 end as edata75_1
            ,case when edata75 = '2' then 1 else 0 end as edata75_2
            ,case when edata76 = '1' then 1 else 0 end as edata76_1
            ,case when edata76 = '2' then 1 else 0 end as edata76_2
            ,case when edata77 = '1' then 1 else 0 end as edata77_1
            ,case when edata77 = '2' then 1 else 0 end as edata77_2
            ,case when edata78 = '1' then 1 else 0 end as edata78_1
            ,case when edata78 = '2' then 1 else 0 end as edata78_2
            ,case when edata79 = '1' then 1 else 0 end as edata79_1
            ,case when edata79 = '2' then 1 else 0 end as edata79_2
            ,case when edata80 = '1' then 1 else 0 end as edata80_1
            ,case when edata80 = '2' then 1 else 0 end as edata80_2
            ,case when edata81 = '1' then 1 else 0 end as edata81_1
            ,case when edata81 = '2' then 1 else 0 end as edata81_2
            ,case when edata82 = '1' then 1 else 0 end as edata82_1
            ,case when edata82 = '2' then 1 else 0 end as edata82_2
            ,case when edata83 = '1' then 1 else 0 end as edata83_1
            ,case when edata83 = '2' then 1 else 0 end as edata83_2
            ,case when edata115 = '1' then 1 else 0 end as edata115_1
            ,case when edata115 = '2' then 1 else 0 end as edata115_2
            ,case when edata116 = '1' then 1 else 0 end as edata116_1
            ,case when edata116 = '2' then 1 else 0 end as edata116_2
            ,case when edata117 = '1' then 1 else 0 end as edata117_1
            ,case when edata117 = '2' then 1 else 0 end as edata117_2
            ,case when edata118 = '1' then 1 else 0 end as edata118_1
            ,case when edata118 = '2' then 1 else 0 end as edata118_2
            ,case when edata119 = '1' then 1 else 0 end as edata119_1
            ,case when edata119 = '2' then 1 else 0 end as edata119_2
            ,case when edata120 = '1' then 1 else 0 end as edata120_1
            ,case when edata120 = '2' then 1 else 0 end as edata120_2
            ,case when edata121 = '1' then 1 else 0 end as edata121_1
            ,case when edata121 = '2' then 1 else 0 end as edata121_2
            ,case when edata122 = '1' then 1 else 0 end as edata122_1
            ,case when edata122 = '2' then 1 else 0 end as edata122_2
        from
            entory_r

        where
            form_id = 61
            and del_flg = 0
            and invalid_flg = 0
            {$addwhere}
    ) as entory_r_sum
;
_SQL_;
    
        //集計データ
        try{
            $db = new DbGeneral(true);
            $total = $db->query($sql);
            $total = $total[0];
            
        }catch(Exception $e){
            $total = array();
        }
        
        return $total;
    }


    function checkQuota($obj){
        // 管理者モードはチェックを行わない
        if(strlen($obj->admin_flg) > 0) return;
        // 定員数を常にチェックする
        if(count($this->arrQuotaDisabled) == 0) return;
        
        // 1ページ目の入力情報
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        
        $error = false;
        // $this->arrQuotaDisabled[$item_id][$value] = $arrQuota[$_key] - $_val;
        foreach($this->arrQuotaDisabled as $item_id => $_quota){
            foreach($_quota as $_value => $quota){
                $buy = $arrForm1["edata".$item_id];
                if(empty($buy)) continue;       // 選択なし
                if($buy != $_value) continue;   // 選択した値のみチェック対象にする

                $buy = 1;           // 応募単価は常に1
                $over= $quota-$buy; // 応募可能件数 - 購入枚数 = 0以上であればok
                if($over >= 0) continue;
                
                // overがマイナスで定員オーバー
                // 購入枚数を考慮した残り応募可能枚数
                $over= abs($over);
                $save= $buy - $over;    // 残り枚数
                
                $msg = ""; // エラーメッセージ
                switch($save){
                    default:
                        $name   = Usr_init::getItemInfo($obj, $item_id);
                        $select = $obj->arrItemData[1][$item_id]["select"];
                        $may    = "";
                        foreach($this->arrGroup as $_may => $_group){
                            if(!in_array($item_id, $_group)) continue;
                            $may = $_may.": "; break;
                        }
                        $format = "%s%s %s　This ticket has been sold out.";
                        $msg    = sprintf($format, $may, $name, $select[$_value]);
                        break;
                }
                $obj->arrErr[$item_id] = $msg;
                $error = true;
            }
        }
        if($error){
            $obj->block = "1";
            $obj->_processTemplate =  "Usr/form/Usr_entry.html";
            
            $obj->assign("arrErr",  $obj->arrErr);
            $obj->assign("block",   $obj->block);
        }
        return $error;
    }


    /* デバッグ用 */
    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 2));
//        print "</pre><br/><br/>";
    }

    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj) {
        // Assignを直前で変更する場合や特定のエラーチェックで利用

        // 定員数チェック
        $this->checkQuota($obj);
        
        if(method_exists($obj, "assign")){
            // Usr # 確認画面はMay15,16,17の行のみを表示
            if(isset($obj->block) && $obj->block == 4){
                $this->setMayUnDisplay($obj);
            }

            if(count($this->arrQuotaDisabled) > 0){
                foreach($this->arrQuotaDisabled as $item_id => $_val){
                    foreach($_val as $_key => $_value){
                        if($_value > 0) continue; // 1以上が応募枠あり
                        $obj->arrItemData[1][$item_id]['disable'][$_key] = 1;
                    }
                }
                $obj->assign("admin_flg", "");
                $obj->assign("arrItemData", $obj->arrItemData);
                $obj->assign("arrQuotaDisabled", $this->arrQuotaDisabled);
            }
            
            // No.11 # 確認画面 > 文言変更
            $GLOBALS['msg']['cofirm_title']  = 'Confirmation screen for your booking';
            $GLOBALS["msg"]["cofirm_desc_i"] = '<span style="font-weight: bold;font-size: 17px;">'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';
            $GLOBALS["msg"]["cofirm_desc_e"] = '<span style="font-weight: bold;font-size: 17px;">'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';
        }
    }


    /** 完了ページ */
    function completeAction($obj) {
        if($this->checkQuota($obj)) return;
    
        return Usr_pageAction::completeAction($obj);
    }


    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     */
    
    // [form61] etc1をTitleの上部へ項目移動を行う。Title1のidの時にその上にetc1を挿入。
    function sortFormIni($obj) {
        $arrGroup1 = & $obj->arrItemData [1];
        
        // 入れ替え
        $array = array ();
        foreach ( $arrGroup1 as $key => $data ) {
            switch ($key) {
                case 57 :
                    $array [26] = $arrGroup1 [26];
                    $array [$key] = $data;
                    break;
                
                case 26 : // etc1
                    break;
                
                default :
                    $array [$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    // No.2 # 完了メール
    function mailfunc28($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc63($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc64($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc69($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc70($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc71($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc72($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc73($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc74($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc75($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc76($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc77($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc78($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc79($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc80($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc81($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc82($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc83($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc115($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc116($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc117($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc118($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc119($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc120($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc121($obj, $item_id, $name, $i = null) { return ""; }
    function mailfunc122($obj, $item_id, $name, $i = null) { return ""; }

    /**
     * No.2 # 完了メール
     * May15,16,17の本文をこのメソッドで生成する
     */
    function mailfunc27($obj, $item_id, $name, $i = null) {
        $body = $this->makeMailfuncMayLS($obj);
        return $body;
    }


    /**
     * May15,16,17のメールの本文を生成する
     *   No.8の置換文字列を考慮
     * 
     * @param  $obj
     * @return $body May15,16,17のメールの本文
     **/
    function makeMailfuncMayLS($obj){
        $group = 1;
        
        $prefix = "";
        $space  = "";
        $body = "";
        foreach($this->arrGroup as $_name => $_group){
            $body .= "\n";
            $body .= "------------------\n";
            $body .= "\n";
            $body .= $prefix."{$_name}:\n";
            
            $item_id = self::getMaySelectItemId($_group, $obj->arrForm);
            if(is_null($item_id)) continue;

            $key  = "edata" . $item_id;
            if(!isset($obj->arrForm[$key])) $obj->arrForm [$key] = "";
            if(strlen($obj->arrForm [$key]) == 0) continue;
    
            $name =  strip_tags($obj->arrItemData[$group][$item_id]["item_name"]);
            $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
            $body .= $space.$name."\n";
            $body .= $space.$value."\n";
        }
        $body .= "\n";
        $body .= "------------------\n";
        
        return $body;
    }

}
