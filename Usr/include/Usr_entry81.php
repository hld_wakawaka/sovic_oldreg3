<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry81
{

    /* デバッグ用 */
    function developfunc($obj)
    {
//         // メールデバッグ
//         $obj->ins_eid = 2025;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
    }



    /**
     * 合計金額を計算する
     * 
     * @param  object
     * @return integer 合計金額
     */
    function calculateTotalPrice($obj)
    {
        return ($obj->eid == "")
               ? Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price)
               : $GLOBALS["session"]->getVar("ss_total_payment");
    }


    /** 1ページ目 */
    function pageAction1($obj) {
        $fix_flg = "";

        //3ページ目を使用
        if($obj->formdata["group3_use"] != "1"){
            $obj->block = "3";
            $fix_flg = "1";
            $obj->_processTemplate = "Usr/form/Usr_entry.html";
        }

        // 決済フォームを利用する場合
        $total_price = $this->calculateTotalPrice($obj);
        if($obj->formdata["credit"] != "0" && $total_price > 0){
            if(!in_array($obj->form_id, $obj->payment_not)){
                $obj->block = "pay";

                $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                $fix_flg = "1";
            }

        //決済を使用しない場合
        }else{
            //2ページ目使用
            if($obj->formdata["group2_use"] != "1"){
                $obj->block = "2";

                //共著者なしの場合は3ページ目を表示
                if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                    if($obj->formdata["group3_use"] != "1"){
                        $obj->block = "3";
                        $obj->_processTemplate =  "Usr/form/Usr_entry.html";

                    }else{
                        $obj->block = "4";
                        $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
                    }
                }
                $fix_flg = "1";
            }
        }

        //上記のいずれも該当しなかった場合
        if($fix_flg == ""){
            $obj->block = "4";
            $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
        }
    }





    /** 戻るボタン */
    function backAction($obj)
    {
        // 確認画面以外から戻るとき
        if($obj->wk_block != "4"){
            $getFormData= GeneralFnc::convertParam($obj->_init($obj->wk_block), $_REQUEST);

            // 共著者の所属機関
            $ret_param = $obj->_make35param($obj->wk_block);
            if(count($ret_param) > 0){
                $getFormData = array_merge($getFormData, $ret_param);
            }

            // ファイル引き継ぎ
            foreach($obj->arrfile as $item_id => $n){
                $key = "n_data".$item_id;
                $getFormData[$key] = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
            }

            // セッションにページパラメータをセットする
            $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $getFormData);
        }

        // セッションパラメータ取得
        $obj->getDispSession();


        // 合計金額#参加登録で決済利用
        if($obj->formdata["kessai_flg"] == "1"){
            $obj->total_price = ($obj->eid == "")
                              ? Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price)
                              : $GLOBALS["session"]->getVar("ss_total_payment");
            $obj->assign("total_price", $obj->total_price);
        }

        switch($obj->wk_block) {
            case "1":
            case "2":
                $obj->block = "1";
                break;

            // 3ページ目からの戻り
            case "3";
                $obj->block = "1";

                // ファイルアップロードチェック
                foreach($obj->arrfile as $item_id => $n){
                    $obj->_checkfile($getFormData, 3, $item_id);
                }

                if(count($obj->objErr->_err) > 0){
                    //エラーを自ページに表示
                    $obj->block = $obj->wk_block;

                }else{
                    // 添付資料アップロード
                    foreach($obj->arrfile as $item_id => $n){
                        if($obj->itemData[$item_id]["disp"] != "1"){
                            list($wb_ret, $msg) = $obj->fileTmp($_FILES["edata".$item_id], $item_id);
                            if(!$wb_ret){
                                $obj->arrErr["file_error".$item_id] = $msg;
                            }
                            $getFormData["hd_file".$item_id] = isset($obj->arrForm["hd_file".$item_id]) ? $obj->arrForm["hd_file".$item_id] : "";
                        }
                    }

                    // 2ページ目を使用
                    if($obj->formdata["group2_use"] != "1"){
                        $obj->block = "2";

                        // 共著者なしの場合は3ページ目を表示
                        if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                        || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                            $obj->block = "1";
                        }
                    }

                    // 決済を使用する場合は決済画面に遷移する
                    if($obj->formdata["credit"] != "0"){
                        if(!in_array($obj->form_id, $obj->payment_not)){
                            $obj->block = "pay";
                            $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                        }
                    }
                }
                break;


            // 決済画面から戻る場合
            case "pay":
                $obj->block = "1";

                // 2ページ目を使用
                if($obj->formdata["group2_use"] != "1"){
                    $obj->block = "2";

                    // 共著者なしの場合は3ページ目を表示
                    if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                    && $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                        $obj->block = "1";
                    }
                }
                break;


            // 確認画面から戻ってきた場合
            case "4":
                $obj->block = "1";

                // 2ページ目を使用
                if($obj->formdata["group2_use"] != "1"){
                    $obj->block = "2";

                    // 2ページ目を使用するが、共著者無しの場合
                    if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                    || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                        $obj->block = "1";
                    }
                }

                // 決済フォームを利用する場合
                if(!in_array($obj->form_id, $obj->payment_not)){
                    if($obj->formdata["credit"] != "0" && $obj->total_price > 0){
                        //決済画面に遷移する
                        $obj->block = "pay";
                        $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                    }
                }

                // 3ページ目を使用
                if($obj->formdata["group3_use"] != "1"){
                    $obj->block = "3";
                    $obj->_processTemplate = "Usr/form/Usr_entry.html";
                }
                break;
        }

        if(isset($obj->arrForm["edata31"])) {
            $wa_kikanlist = $obj->_makeListBox($obj->arrForm["edata31"]);
            $obj->arrForm["kikanlist"] = $wa_kikanlist;
        }
    }





    /**
     * 編集用URLのお知らせ
     * 
     */
    function makeEditBody($obj, $user_id="", $passwd="", $exec_type)
    {
        return "";
    }

}
