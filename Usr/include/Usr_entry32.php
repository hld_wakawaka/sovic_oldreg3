<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * 
 */
class Usr_Entry32 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        $obj->payment_not = array($form_id);

        // jQueryを読み込む
        $obj->useJquery = true;

        // 支払方法 # 銀行振込で固定
        unset($GLOBALS["method_J"][1]);
        // 支払方法 # 銀行振込の選択済みで固定
        $_REQUEST['method'] = 2;

//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        $obj->arrForm['method'] = 2;

        $obj->arrForm['total_price'] = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する


//    // エラーチェック # ブロック1
//    function _check1($obj){
//        Usr_Check::_check1($obj);
//
//        $group_id = 1;
//
//        // Fee
//        $amount = strlen($obj->arrParam['amount']) > 0 ? intval($obj->arrParam['amount']) : -1;
//
//
//        // xxで1番目の項目を選択したらテキスト必須
//        $item_id = 26;
//        $key = "edata".$item_id;
//        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
//            $item_id = 27;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//
//
//        //-------------------------------
//        // AccompanyingPersonの数
//        //-------------------------------
//        $cnt = 0;
//
//        // AccompanyingPerson 1
//        $keys = array('edata64', 'edata69', 'edata70', 'edata71');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 1'), 'edata64');
//            }else{
//                $cnt++;
//            }
//        }
//
//        // AccompanyingPerson 2
//        $keys = array('edata72', 'edata73', 'edata74', 'edata75');
//        if($obj->objErr->isInputwhich($obj->arrParam, $keys)){
//            if(!$obj->objErr->isInputAll($obj->arrParam, $keys)){
//                $method = $GLOBALS["msg"]["err_require_input"];
//                $obj->objErr->addErr(sprintf($method, 'Accompanying Person 2'), 'edata72');
//            }else{
//                $cnt++;
//            }
//        }
//    }
//
//
//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }
//
//
//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     **/
//    function sortFormIni($obj){
//        $arrGroup1 =& $obj->arrItemData[1];
//
//        // 入れ替え
//        $array = array();
//        foreach($arrGroup1 as $key => $data){
//            switch($key){
//                case 7:     // middle name
//                    $array[$key]= $data;
//                    $array[26]  = $arrGroup1[26];
//                    $array[27]  = $arrGroup1[27];
//                    $array[28]  = $arrGroup1[28];
//                    $array[63]  = $arrGroup1[63];
//                    $array[64]  = $arrGroup1[64];
//                    break;
//
//                case 26:    // etc1
//                case 27:    // etc2
//                case 28:    // etc3
//                case 63:    // etc4
//                case 64:    // etc5
//                    break;
//
//                default:
//                    $array[$key] = $data;
//                    break;
//            }
//        }
//        $arrGroup1 = $array;
//    }



    // ------------------------------------------------------
    // ▽Mngスタマイズ
    // ------------------------------------------------------

    function __constructMng($obj){
        // 決済CSV # 明細表示フラグ
        $obj->payment_csv_price_disp = true;
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    function makePaymentBody($obj, $exec_type){
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //支払合計
        if($exec_type == "1"){
            $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        }else{
            $total = $GLOBALS["session"]->getVar("ss_total_payment");
        }


        if($obj->formdata["lang"] == LANG_JPN){
            $obj->point_mark = "■";
            $body_pay = "\n\n【お支払情報】\n\n";
        }else{
            $obj->point_mark = "*";
            $body_pay = "\n\n[Payment Information]\n\n";
        }

        //$body_pay .= "\n\n";
        if($obj->formdata["lang"] == LANG_JPN){
            // お支払確認ページの表示/非表示 : デフォルトは表示
            if(!in_array($obj->form_id, $obj->payment_not)){
                $body_pay .= $obj->point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
            }
            $body_pay .= $obj->point_mark."金額（税込）: \n";
        }
        else{
            if($total > 0){
                // お支払確認ページの表示/非表示 : デフォルトは表示
                if(!in_array($obj->form_id, $obj->payment_not)){
                    $body_pay .= ($obj->arrForm["method"] != "3") 
                                ? $obj->point_mark."Payment:".$obj->wa_method[$obj->arrForm["method"]]."\n"
                                : $obj->point_mark."Payment:Cash on-site\n";
                }
            }
            $body_pay .= $obj->point_mark."Amount of Payment:\n";
        }


        // Fee
        $body_pay .= $obj->makePaymentBody1($exec_type);

        //その他決済がある場合
        $body_pay .= $obj->makePaymentBody2($exec_type);


        if($obj->formdata["lang"] == LANG_JPN){
            $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
        }
        else{
            $body_pay .="      Amount of Total Payment:".$obj->yen_mark.number_format($total)."\n\n";
        }


        // お支払情報のメール生成
        // お支払確認ページの表示/非表示 : デフォルトは表示
        if(!in_array($obj->form_id, $obj->payment_not)){
            $body_pay .= $obj->makePaymentMethodBody($total);
        }

        return $body_pay;
    }

    /** その他決済のメール本文 */
    function makePaymentBody2($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //その他決済がある場合
        if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

            //新規の場合
            if($exec_type == "1"){
                foreach($obj->wa_ather_price  as $p_key => $data ){
                    if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                    if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
                        $price = $data["p1"];

                        // Free, - => 0
                        if(!is_numeric($data["p1"])){
                            $data["p1"] = 0;
                        }
                        $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                        // タグを除去
                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = number_format($price)."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円\n";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = $obj->yen_mark.number_format($price);
                            }
                            $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum)."\n";
                        }
                    }
                }
                
            }

            //更新の場合
            if($exec_type == "2"){
                //更新の場合は、登録済みのデータから引っ張ってくる
                $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);

                foreach($wa_payment_detail as $data){
                    $price = $data["price"];
                    // Free, - => 0
                    if(!is_numeric($data["price"])){
                        $data["price"] = 0;
                    }
                    $sum = $data["price"]*$data["quantity"];

                    // タグを除去
                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);

                    if($obj->formdata["lang"] == LANG_JPN){
                        if(is_numeric($price)){
                            $price = $price."円";
                        }
                        $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                    }
                    else{
                        if(is_numeric($price)){
                            $price = $obj->yen_mark.$price;
                        }
                        $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum);
                    }
                    $body_pay .= "\n";
                }
            }
        }
        return $body_pay;
    }

//    /**
//     * 任意
//     */
//    function mailfunc[group_id]($obj, $item_id, $name, $i=null) {
//        $group = [group_id];
//
//        $key = "edata".$item_id.$i;
//        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
//
//        $str = $obj->point_mark.$name.": ".$value."\n";
//        return $str;
//    }



    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /**
     * CSVヘッダ生成
     */
    function payment_payMakeHeader($obj, $all_flg=false){
        $header   = array();
        // payment_csvのみ出力
        if(!$all_flg){
            $header[] = "応募者氏名";
            $header[] = "応募者氏名（カナ）";
        }
        $header[] = "ステータス";


        //------------------------------
        //決済項目
        //------------------------------
        foreach($obj->wa_ather_price as $data){
            $head = GeneralFnc::smarty_modifier_del_tags($data);
            $header[] = strip_tags(str_replace(array("_"), " ",$head));
            // 明細表示フラグ
            if($obj->payment_csv_price_disp){
                $header[] = $obj->payment_csv_price_head;
            }
        }

        $header[] = "合計金額";
        $header[] = "登録日";
        $header[] = "更新日";

        //生成
        $ret_buff  = implode($obj->delimiter, $header);
        $ret_buff .="\n";

        return $ret_buff;
    }

    /**
     * CSV出力データ生成
     */
    function payment_payMakeData($obj, $pa_param, $pa_detail, $all_flg=false){

        $ret_buff = "";
        $wk_buff = "";

        // payment_csvのみ出力
        if(!$all_flg){
            //応募者氏名
            $wk_buff[] =  "\"".$pa_param["edata1"]."　".$pa_param["edata2"]. "\"";

            //応募者氏名（カナ）
            $wk_buff[] =  "\"".$pa_param["edata3"]."　".$pa_param["edata4"]. "\"";
        }

        //ステータス
        $wk_buff[] = ($pa_param["payment_status"] != "") ? "\"".$obj->wa_pay_status[$pa_param["payment_status"]]."\"" : "";


        //支払詳細
        $wk_detail = "";

        if($pa_detail){
            $wk_buff = array_merge($wk_buff, $obj->pay_c($obj, $pa_detail));
        }else{
            foreach($obj->wa_ather_price as $ather_name){
                $wk_buff[] = "";
            }
        }



        //金額
        $wk_buff[] = "\"".$pa_param["price"]."\"";



        //登録日
        $wk_buff[] = ($pa_param["insday"] != "") ? "\"".$pa_param["insday"]."\"" : "";

        //更新日
        $wk_buff[] = ($pa_param["upday"] != "") ? "\"".$pa_param["upday"]."\"" : "";


        $ret_buff = implode($obj->delimiter, $wk_buff);
        $ret_buff .= "\n";
        return $ret_buff;
    }

//    function csvfunc[i]($obj, $group, $pa_param, $item_id){
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }


}
