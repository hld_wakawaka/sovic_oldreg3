<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author hld kawauchi
 *        
 */
class Usr_Entry98 {


   /* 共通設定のoverride !Mngでも呼ばれる! */
   function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
//        
        // jQueryを読み込む
        $obj->useJquery = true;
//        
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
//        
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
//        
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
   }

//    /* デバッグ用 */
//    function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
//    }

//    /**
//     * ProcessBase::mainの前処理
//     */
//    function premain($obj) {
//        // Assignを直前で変更する場合や特定のエラーチェックで利用
//    }
//    
//    // 項目チェックの際は項目のdispフラグを考慮するため
//    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する
//    

    
   // エラーチェック # ブロック1
   function _check1($obj) {
       Usr_Check::_check1($obj);
       
       $checkFeeNum1 = array(0, 1, 3, 5);
       $checkFeeNum2 = array(3, 4, 5);
       
       $group_id = 1;
       
       // Fee
       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
       
       // [cfp-form98] Feeで★1を選んだ場合、1の入力を必須とする（$amountが0か1か3か5の時、edata26の項目入力必須）：2016/03/07 12:25着手
       foreach($checkFeeNum1 as $num) {
           if($amount == $num) {
               $item_id = 26;
               $key = "edata" . $item_id;
               
               if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                   $name = Usr_init::getItemInfo($obj, $item_id);
                   $method = Usr_init::getItemErrMsg($obj, $item_id);
                   $obj->objErr->addErr(sprintf($method, $name), $key);
               }
           }
       }
       
      
      // [cfp-form98] 2に入力があった場合、★2を選択したらエラー（$amountが2か4か5の時、edata27の項目入力でエラー）：2016/03/07 13:35着手
      $item_id = 27;
      $key = "edata" . $item_id;
      if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$key] != "") {
          foreach($checkFeeNum2 as $num) {
               if($amount == $num) {
                   $key = "amount";
                   $method = 'If you are a author who registers a paper, please select "IEEE ComSoc Member or IEICE Member", "IEEE/Sister Society Member" or "Non Member" for 
Registration Category.';
                   $obj->objErr->addErr($method, $key);
               }
           }
      }
       
       
   }
//    
//    // エラーチェック # ブロック3
//    function _check3($obj) {
//        Usr_Check::_check3($obj);
//        
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id = 68;
//        $target = 'Allergy';
//        if (Usr_Check::chkSelect($obj, $group_id, $item_id, $target)) {
//            $item_id = 99;
//            $key = "edata" . $item_id;
//            if (Usr_init::isset_ex($obj, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
//                $name = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }
//    
//    /*
//     * 項目並び替え
//     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
//     */
//    function sortFormIni($obj) {
//        $arrGroup1 = & $obj->arrItemData [1];
//        
//        // 入れ替え
//        $array = array ();
//        foreach ( $arrGroup1 as $key => $data ) {
//            switch ($key) {
//                case 7 : // middle name
//                    $array [$key] = $data;
//                    $array [26] = $arrGroup1 [26];
//                    $array [27] = $arrGroup1 [27];
//                    $array [28] = $arrGroup1 [28];
//                    $array [63] = $arrGroup1 [63];
//                    $array [64] = $arrGroup1 [64];
//                    break;
//                
//                case 26 : // etc1
//                case 27 : // etc2
//                case 28 : // etc3
//                case 63 : // etc4
//                case 64 : // etc5
//                    break;
//                
//                default :
//                    $array [$key] = $data;
//                    break;
//            }
//        }
//        $arrGroup1 = $array;
//    }
//    
//    // ------------------------------------------------------
//    // ▽メールカスタマイズ
//    // ------------------------------------------------------
//    
//    /**
//     * 任意
//     */
//    function mailfunc_group_id($obj, $item_id, $name, $i = null) {
//        $group = 1;
//        
//        $key = "edata" . $item_id . $i;
//        if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
//        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
//        
//        $str = $obj->point_mark . $name . ": " . $value . "\n";
//        return $str;
//    }
//    
//    // ------------------------------------------------------
//    // ▽CSVカスタマイズ
//    // ------------------------------------------------------
//    function csvfunc_i($obj, $group, $pa_param, $item_id) {
//        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
//                " ",
//                "," 
//        ), true);
//        $wk_body = trim($wk_body, ",");
//        return $wk_body;
//    }
}
