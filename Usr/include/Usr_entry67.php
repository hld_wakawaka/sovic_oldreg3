<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry67 {

    /***************************
     * カスタマイズメモ
     * 必要な部分コピーして利用する
     ***************************/

//    /* 共通設定のoverride !Mngでも呼ばれる! */
	function __construct($obj) {
//         // form_idの取得
//         $form_id = $obj->o_form->formData ['form_id'];
       
//         // jQueryを読み込む
//         $obj->useJquery = true;
       
//         // 支払方法 # 銀行振込で固定
//         unset($GLOBALS ["method_J"] [1]);
//         // 支払方法 # 銀行振込の選択済みで固定
//         $_REQUEST ['method'] = 2;
       
//         // Smartyの修飾子を追加
//         if (isset($obj->_smarty)) {
//             $obj->_smarty->register_modifier('form[form_id]_example', array (
//                     $this,
//                     'form[form_id]_example' 
//             ));
//         }
       
//         // assignする場合
//         if (method_exists($obj, "assign")) {
//             $obj->assign('variable', $obj->variable);
//         }
	}
   
   /* デバッグ用 */
	function developfunc($obj) {
//         // メールデバッグ
//         $obj->ins_eid = 1615;
//         print "--------------------<pre style='text-align:left;'>";
//         print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//         print "</pre><br/><br/>";
	}

   /**
    * ProcessBase::mainの前処理
    */
	function premain($obj) {
		// Assignを直前で変更する場合や特定のエラーチェックで利用
	}
   
	// 項目チェックの際は項目のdispフラグを考慮するため
	// Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する
   
   	// エラーチェック # ブロック3
	function _check3($obj) {
		Usr_Check::_check3($obj);

		// [form67] 項目54で'Yes - Already submitted. 'を選択したら、項目55(Submission No)を必須入力。未入力の場合はエラー
		$group_id = 3;
		$item_id = 54;
		$key = "edata" . $item_id;
		
		if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1) {
			$item_id = 55;
            $key = "edata" . $item_id;
            if (Usr_init::isset_ex($obj, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
				$name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $obj->objErr->addErr('Enter Submission No..', $key);
            }
		}
	}
   
	// [form67] 任意1の最初の選択肢の末尾に「任意2項目名：任意2テキストボックス」とする。メールでも同様に表示
	function mailfunc54($obj, $item_id, $name, $i=null) {
      	$group = 3;
      	
      	$key = "edata".$item_id.$i;
      	if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
      	$value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
    
      	$str  = "";
      	$str .= Config::assign_obi(array("mode"=>"mail", "item_id"=>$item_id), $obj->_smarty);
   
      	$str .= $obj->point_mark.$name.": ".$value."\n";
      	if($obj->arrForm[$key] == "1"){
			$str .= " Submission No.: ".$obj->arrForm["edata55"]."\n";
      	}
      	
		return $str;
	}
}