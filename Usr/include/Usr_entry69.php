<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *        
 */
class Usr_Entry69 {

   /* デバッグ用 */
   function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
       
   }

   /**
    * モード別アクション処理を読み込む仕組みを標準対応予定
    *
    */
   function defaultAction($obj) {
       $ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
       if($ws_action == "") $ws_action = "default";
   
       $actionName = $ws_action."Action";
       if(method_exists($obj->exClass, $actionName) && $actionName != __FUNCTION__){
           return $obj->exClass->$actionName($obj);
       }else{
           return Usr_pageAction::defaultAction($obj);
       }
   }
   
   function ajaxAction($obj) {
       $authData = array (
           "coupon_code" => $_POST["edata26"],
           "result" => ""
       );
      
       $authResult = $this->authInviteCode($obj,$authData);
       echo json_encode($authResult);
       exit;
   }
   
   // [reg3-form69] No.4：グループ１>招待コードの認証・招待コードについて認証を行う。招待コードに対して予めある認証用の招待コードを使って認証を行う
   function authInviteCode($obj,$authData) {
       // 認証モード # エントリーCSV
       $form_id = 69;

       $customDir = ROOT_DIR."customDir/".$form_id."/";
       $filename  = $customDir.'auth/authInviteCodeList.csv';

       // 招待コードが物理的に存在していない場合、認証は非とする
       if(!file_exists($filename)) {
           $authData['result'] = false;
           return $authData;
       }
      
       $arrData = array();
       $line_count = 0;  // 行数
       $fp = fopen($filename, 'r');
       if($fp !== false) {
           while (!feof($fp)) {
               $line_count++;
               $arrCSV = fgetcsv($fp, 10000);
               if(empty($arrCSV)) continue;   // 空行はスキップ
               if($line_count == 1) continue;   // ヘッダ行はスキップ
               array_push($arrData, $arrCSV[0]);
           }
           fclose($fp);
       }

       // [reg3-form69] 招待コードマスタが全て未入力なら認証エラーを返す
       if(empty($arrData)) {
           $authData['result'] = false;
           return $authData;
       }
      
       // 未入力チェック。入力値が有れば、承認コードマスタとの認証コードチェック
       if(strlen($authData['coupon_code']) == 0) {
           $authData['result'] = false;
           return $authData;
       }
       $authData['result'] = (bool)array_search($authData['coupon_code'], $arrData);
       return $authData;
   }
   
   // エラーチェック # ブロック1
   function _check1($obj) {
       Usr_Check::_check1($obj);
       
       $group_id = 1;
       
       // Fee
       $amount = strlen($obj->arrParam ['amount']) > 0 ? intval($obj->arrParam ['amount']) : - 1;
       
       // [reg3-form69] No.2：グループ1 > エラー設定 :金額(Fee)選択の内、項目「Patron/Exhibitor (additional)」を選択したら、招待コード「Invite Code」を入力していないとエラー
       foreach(array(26=>"amount", "amount"=>26) as $_item_id => $item_id) {
           if ($_item_id == "amount") {
               if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$_item_id] == 3) {
                   $key = "edata" . $item_id;
                   if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                       $name = Usr_init::getItemInfo($obj, $item_id);
                       $method = Usr_init::getItemErrMsg($obj, $item_id);
                       $obj->objErr->addErr("Please fill in an invite code if you would like to purchase additional tickets for patrons or exhibitors.", $key);
                   }
               // [reg3-form69] No.2：グループ1 > エラー設定 :金額(Fee)選択の内、項目「Exhibit Area Only (additional)」を選択したら、招待コード「Invite Code」を入力していないとエラー
               } else if (Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam [$_item_id] == 4) {
                   $key = "edata" . $item_id;
                   if (Usr_init::isset_ex($obj, $group_id, $item_id) && ! $obj->objErr->isNull($obj->arrParam [$key])) {
                       $name = Usr_init::getItemInfo($obj, $item_id);
                       $method = Usr_init::getItemErrMsg($obj, $item_id);
                       $obj->objErr->addErr("Please fill in an invite code if you would like to purchase additional tickets for exhibit area only.", $key);
                   }
               }
           } else if ($_item_id == 26) {
               // [reg3-form69] No.3：グループ1 > 逆のエラー設定「Invite Code」を入力したら、金額項目「Patron/Exhibitor (additional)」と「Exhibit Area Only (additional)」の必須チェック
               $key = "edata".$_item_id;
               if(Usr_init::isset_ex($obj, $group_id, $_item_id) && $obj->arrParam [$key] != "") {
                   if((Usr_init::isset_ex($obj, $group_id, $_item_id) && $obj->arrParam [$item_id] != 3) && (Usr_init::isset_ex($obj, $group_id, $_item_id) && $obj->arrParam [$item_id] != 4)) {
                       $obj->objErr->addErr("You do not have to fill in an invite code unless you buy additional tickets for patrons or exhibitors or exhibit area only.", $item_id);
                   }
               }
           }
           
       }

       // [reg3-form69(追加見積):グループ1 > 追加エラー設定]
       // ・その他決済「Aditional Paper(Member)」を選択した場合はFee「Member」を選択していないとエラー
       // ・その他決済「Aditional Paper(Non-Member)」を選択した場合はFee「Non-Member」を選択していないとエラー
       foreach(array("ather_price4","ather_price5") as $item_id) {
           $key2 = "amount";
           if ($item_id == "ather_price4") {
               if ($obj->arrParam [$item_id] > 0) {
                   if(strval($obj->arrParam [$key2]) != "0") {
                       $obj->objErr->addErr("Please select the same category as Registration Fee for additional papers.");
                   }
               }

           } else if ($item_id == "ather_price5") {
               if ($obj->arrParam [$item_id] > 0) {
                   if($obj->arrParam [$key2] != 1) {
                       $obj->objErr->addErr("Please select the same category as Registration Fee for additional papers.");
                   }
               }
           }
       }
   }
   
   // エラーチェック # ブロック3
   function _check3($obj) {
        $obj->admin_flg = "";
        $group_id = 3;
        
        // No.14 # 最初の選択肢を選択したらテキストボックスを必須
        $item_id = 54;
        $key     = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key] == 1){
            Usr_init::addCheck($obj, array(55), 0);
        }

        Usr_Check::_check3($obj);
   }

   function mailfunc54($obj, $item_id, $name, $i = null) {
          $group = 3;
   
          $key = "edata" . $item_id . $i;
          if (! isset($obj->arrForm [$key])) $obj->arrForm [$key] = "";
          $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm [$key]);
          
          $str = $obj->point_mark . $name . ": " . $value;
          if($obj->arrForm[$key] == "1") {
             $key = "edata55" . $i;
             $name = $obj->arrItemData[$group][55]['item_name'];
             $str .= " {$name}: ".$obj->arrForm[$key];
          }
          $str .= "\n";
          return $str;
   }

   function mailfunc55($obj, $item_id, $name, $i = null) {
   }

}
