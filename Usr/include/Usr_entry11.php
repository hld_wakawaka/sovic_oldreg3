<?php

/**
 * reg3form11番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.01.21
 * 
 */
class Usr_Entry11 {

    function __construct($obj){}


    /** 開発用のデバッグ関数 */
    function developfunc($obj) {
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody(1396, "11", 1));
//        print "</pre><br/><br/>";
    }



    /* 項目並び替え */
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 1:
                    $array[26]  = $arrGroup1[26];
                    $array[$key] = $data;
                    break;

                case 7:
                    $array[$key] = $data;
                    $array[27]  = $arrGroup1[27];
                    break;

                case 7:
                case 26:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }


}
