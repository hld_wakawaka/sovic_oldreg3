<?php

/**
 * reg3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 *
 */
class Usr_Entry49 {

    //----------------------------------------
    // カスタマイズメモ
    //----------------------------------------


    /* 共通設定のoverride !Mngでも呼ばれる! */
    function __construct($obj){
        // form_idの取得
        $form_id = $obj->o_form->formData['form_id'];

        // jQueryを読み込む
        $obj->useJquery = true;

        // フォーム項目を配列にする特殊項目
        $obj->formarray = array(116, 117, 118, 119, 120);

        // フォームタイトルの非表示
        $obj->not_title_view = array(49);


        // 非公開の設定ファイルの有無をチェック
        $customDir = ROOT_DIR."customDir/".$form_id."/";

        $isStopGroup3   = false;    // グループ3の停止フラグ
        $stop = "stop";
        $ex = glob($customDir.$stop.'*');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                if($script == $customDir."stop_group3"){
                    $isStopGroup3 = true;
                    continue;
                }
            }
        }

        // グループ3を非表示
        if($isStopGroup3){
            $obj->o_form->formData['group3_use'] = 1;
        }


//        // 支払方法 # 銀行振込で固定
//        unset($GLOBALS["method_J"][1]);
//        // 支払方法 # 銀行振込の選択済みで固定
//        $_REQUEST['method'] = 2;
//
//        // Smartyの修飾子を追加
//        if(isset($obj->_smarty)){
//            $obj->_smarty->register_modifier('form[form_id]_example', array($this, 'form[form_id]_example'));
//        }
//
//        // assignする場合
//        if(method_exists($obj, "assign")){
//            $obj->assign('variable', $obj->variable);
//        }
    }

    //Mng詳細画面
    function __constructMngDetail($obj){
        //27の備考文を除外
        $obj->itemData[27]['item_select'] = "";
        $firstFlg = true;
        foreach($obj->itemData[27]['select'] as $key => $value){
            if($firstFlg == false){
                $obj->itemData[27]['item_select'] .= "\n";
            }
            if(strstr($value, "<br>", TRUE) !== false){
                $obj->itemData[27]['item_select'] .= strstr($value, "<br>", TRUE);
            }else{
                $obj->itemData[27]['item_select'] .= $value;
            }
            $firstFlg = false;
        }
    }


    /* デバッグ用 */
    function developfunc($obj) {
//         // メールデバッグ
//        $obj->ins_eid = 1615;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 1));
//        print "</pre><br/><br/>";
    }


    /**
     * ProcessBase::mainの前処理
     */
    function premain($obj){
        // Assignを直前で変更する場合や特定のエラーチェックで利用
        $obj->assign("formarray", $obj->formarray);
    }


    function getEntry($obj){
        Usr_entryDB::getEntry($obj);

        // 特殊項目の対応 TODO 応急処置
        $arrForm = $GLOBALS["session"]->getVar("form_param1");
        foreach($obj->formarray as $item_id){
            $key = 'edata'.$item_id;
            $arrForm[$key] = unserialize($arrForm[$key]);
            foreach($arrForm[$key] as $_key => $str){
                $str = str_replace("[:n:]", "\n", $str);   // 改行コード統一
                $str = stripslashes($str);                 // クォート
                $arrForm[$key][$_key] = $str;
            }
        }
        $GLOBALS["session"]->setVar("form_param1", $arrForm);
        $obj->getDispSession();
    }


    /**
     * モード別アクション処理を読み込む仕組みを標準対応予定
     *
     */
    function defaultAction($obj){
        $ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
        if($ws_action == "") $ws_action = "default";

        $actionName = $ws_action."Action";
        if(method_exists($obj->exClass, $actionName) && $actionName != __FUNCTION__){
            return $obj->exClass->$actionName($obj);
        }else{
            return Usr_pageAction::defaultAction($obj);
        }
    }


    function ajaxAction($obj){
    	// カスタマイズテンプレート設定の読み込み
//        Config::load($obj);
        $obj->arrItemData[1][116]['sep'] = " ";
        // 同伴者の人数
        $num = $_POST['num'];
        // 共著者なし
        if(empty($num)){
            echo ''; exit;
        }

        // セッションパラメータ取得
        $obj->getDispSession();
        $obj->arrForm['edata115'] = $num;

        $obj->assign("onload",     $obj->onload);
        $obj->assign("workDir",    $obj->workDir);
        $obj->assign("block",      $obj->block);
        $obj->assign("arrErr",     $obj->arrErr);	//エラー配列
        $obj->assign("admin_flg",  $obj->admin_flg);	//エラー配列
        $obj->assign("arrItemData",$obj->arrItemData);
        $obj->assign("formItem",   $obj->itemData);        //画面表示項目
        $obj->assign("group3use",  $obj->group3use);  // グループ3見出し表示フラグ
        $obj->assign("group3item", $obj->group3item); // グループ3のグループ分け
        $obj->assign("need_mark",  $obj->need_mark);	//必須マーク
        $obj->assign("yen_mark",   $obj->yen_mark);
        $obj->assign("arrForm" ,   $obj->arrForm);
        $obj->assign("formarray" , $obj->formarray);
        $obj->assign("group_id" ,  $obj->wk_block);

        echo $obj->_smarty->fetch("Usr/include/49/parts/AccompanyingPersion.html");
        exit;
    }


    // 項目チェックの際は項目のdispフラグを考慮するため
    // Usr_init::isset_ex($obj, $group_id, $item_id)が「true」か判定する

   // エラーチェック # ブロック1
   function _check1($obj){
       // エラーメッセージの調整
       foreach($obj->formarray as $item_id){
           $prefix = "";
           // Enter       # 117, 118,
           if(in_array($item_id, array(117, 118))){
               $prefix = "Accompanying Person's ";
           }
           // Select ones # 116, 119, 120
           if(in_array($item_id, array(116, 119, 120))){
               $prefix = "Accompanying Person, ";
           }
           $obj->itemData[$item_id]['strip_tags'] = $prefix.'"'.$obj->itemData[$item_id]['strip_tags'].'"';
       }

       Usr_Check::_check1($obj);

       // Participant Categoryに選択した対応項目を必須
       $arr27[1] = array(28,63,64);
       $arr27[2] = array(69,70,71,72);
       $arr27[3] = array(74,75,76);
       $i = $obj->arrParam['edata27'];

       foreach($arr27[$i] as $item_id){
           $key = "edata".$item_id;
           if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
               $name = Usr_init::getItemInfo($obj, $item_id);
               $method = Usr_init::getItemErrMsg($obj, $item_id);
               $obj->objErr->addErr(sprintf($method, $name), $key);
           }
       }

       // Select one(s) > Select one
       $radio = array(83,27,57);
       foreach($radio as $item_id){
           $key = "edata".$item_id;
           if(isset($obj->objErr->_err[$key])){
               $obj->objErr->_err[$key] = str_replace("Select one(s)", "Select one", $obj->objErr->_err[$key]);
           }
       }


        // 同伴者ありで人数が0人の場合
        if($obj->arrParam['edata83'] == 1 && empty($obj->arrParam['edata115'])){
            $obj->objErr->_err['edata115'] = "Select one(s) for Number of accompanying persons.";
        }

        /**
         * 同伴者に関するメッセージを表示しない
         *  ・同伴者なし(No)
         *  ・同伴者あり(Yes)で人数0
         */
        if($obj->arrParam['edata83'] == 2
        || ($obj->arrParam['edata83'] == 1 && empty($obj->arrParam['edata115']))){
            foreach(array(116,117,118,119,120) as $item_id){
                $key = "edata".$item_id;
                unset($obj->objErr->_err[$key]);
            }
        }
   }


    /** 新規エントリー処理 */
    function makeDbParam($obj){
        $param = Usr_entryDB::makeDbParam($obj);

        // etc50にedata28の選択肢を保存
        if($obj->arrForm["edata27"] == 1){
            $param["edata144"] = $obj->itemData[28]["select"][$obj->arrForm["edata28"]];
        }
        if($obj->arrForm["edata27"] == 3){
            $param["edata144"] = $obj->itemData[74]["select"][$obj->arrForm["edata74"]];
        }

        return $param;
    }


//    // エラーチェック # ブロック3
//    function _check3($obj){
//        Usr_Check::_check3($obj);
//
//        // 68(checkbox)で'Allergy'を選択したら、99(テキストエリア)を必須
//        $group_id = 3;
//        $item_id  = 68;
//        $target   = 'Allergy';
//        if(Usr_Check::chkSelect($obj, $group_id, $item_id, $target)){
//            $item_id = 99;
//            $key = "edata".$item_id;
//            if(Usr_init::isset_ex($obj, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
//                $name   = Usr_init::getItemInfo($obj, $item_id);
//                $method = Usr_init::getItemErrMsg($obj, $item_id);
//                $obj->objErr->addErr(sprintf($method, $name), $key);
//            }
//        }
//    }
//
//
    /*
     * 項目並び替え
     * 影響範囲：全て（入力、確認、メール、CSV、詳細）
     **/
    function sortFormIni($obj){
        $arrGroup1 =& $obj->arrItemData[1];

        // 入れ替え
        $array = array();
        foreach($arrGroup1 as $key => $data){
            switch($key){
                case 120:
                    $array[$key]= $data;
                    $array[77]  = $arrGroup1[77];
                    break;

                case 77:
                    break;

                default:
                    $array[$key] = $data;
                    break;
            }
        }
        $arrGroup1 = $array;
    }



   // ------------------------------------------------------
   // ▽メールカスタマイズ
   // ------------------------------------------------------

   function makeMailBody($obj, $user_id="", $passwd="", $exec_type=""){
       // カスタマイズテンプレート設定の読み込み
       Config::load($obj);

       // メール上部コメントの作成
       $head_comment = $obj->makeMailBody_header($user_id, $passwd, $exec_type);

       // 入力フォームのメール本文
       $arrbody = $obj->makeFormBody($exec_type);


       //----------------------------------------
       //本文生成
       //----------------------------------------
       $body  = "";
       $body .= $head_comment."\n\n";
       $body .= "\n";
       if(isset($obj->contact_before)) $body .= $obj->contact_before;
       $body .= "\n------------------------------------------------------------------------------\n";
       $body .= $obj->formdata["contact"];
       $body .= "\n------------------------------------------------------------------------------\n";
       if(isset($obj->contact_after)) $body .= $obj->contact_after;

       $formbody = $arrbody[1];
       if($obj->formdata["group2_use"] != "1") $formbody .= $arrbody[2];
       if($obj->formdata["group3_use"] != "1") $formbody .= $arrbody[3];
       if($obj->formdata["kessai_flg"] == "1") $formbody .= $obj->makePaymentBody($exec_type);
       $formbody .= $obj->makeEditBody($user_id, $passwd, $exec_type);

       $body = str_replace("#Registration Information#", "<Registration Information>", $body);
       $body = str_replace("_FORM_DATA_", $formbody, $body);
       return str_replace(array("\r\n", "\r"), "\n", $body);
   }


   function makeBodyGroup1($obj, &$arrbody, $exec_type){
       // Participant Categoryの選択した項目のみメールに表示する
       // 一旦すべて非表示とし、対応した項目を表示にする
       $group = 1;
       $arr27[1] = array(28,63,64);
       $arr27[2] = array(69,70,71,72);
       $arr27[3] = array(74,75,76);
       foreach($arr27 as $_arr27){
           foreach($_arr27 as $item_id){
               $obj->arrItemData[$group][$item_id]['item_mail'] = 1;
           }
       }
       $key = $obj->arrForm['edata27'];
       foreach($arr27[$key] as $item_id){
           $obj->arrItemData[$group][$item_id]['item_mail'] = "";
       }

       Usr_mail::makeBodyGroup1($obj, $arrbody);
   }


   /**
    * 同伴者に関数メール:複数項目
    */
   function mailfunc116($obj, $item_id, $name, $i=null) {
       $group = 1;
       if($obj->arrForm['edata115'] == 0) return;
       if($obj->arrForm['edata83']  != 1) return;

       $str = "\n";
       for($i=0; $i<$obj->arrForm['edata115']; $i++){
           foreach($obj->formarray as $item_id){
               $key   = "edata".$item_id;
               $name  = strip_tags($obj->arrItemData[$group][$item_id]["item_name"]);
               if(!isset($obj->arrForm[$key][$i])) $obj->arrForm[$key][$i] = "";

               $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key][$i]);
               $str .= $obj->point_mark.$name.": ".$value."\n";
           }
           $str .= "\n";
       }

       return $str;
   }


   function mailfunc77($obj, $item_id, $name, $i=null) {
       $group = 1;

       $key = "edata".$item_id.$i;
       if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
       $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

       $str = $obj->point_mark.$name.": ".$value."\n";
       return "\n".$str;
   }


   function mailfunc27($obj, $item_id, $name, $i=null) {
       $group = 1;

       $key = "edata".$item_id.$i;
       if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
       $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

       if(strstr($value, "<br>", TRUE) !== false){
           $value = strstr($value, "<br>", TRUE);
       }

       $str = $obj->point_mark.$name.": ".strip_tags($value)."\n";
       return "\n".$str;
   }


   // ------------------------------------------------------
   // ▽CSVカスタマイズ
   // ------------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            switch ($_data['item_id']){
                case 28:
                    $groupHeader[$group][] = $obj->fix.$name." No."     .$obj->fix;
                    $groupHeader[$group][] = $obj->fix.$name." Name"    .$obj->fix;
                    $groupHeader[$group][] = $obj->fix.$name." Factory" .$obj->fix;
                    break;

                // AccompanyinigPerson
                case 116:
                    $max = max($obj->arrItemData[$group][115]['select']);
                    for($i=1; $i<$max+1; $i++){
                        $prefix = "Accompanyinig Person {$i} ";
                        foreach(array(116,117,118,119,120) as $item_id){
                            $groupHeader[$group][] = $obj->fix.$prefix.$obj->arrItemData[$group][$item_id]['item_name'].$obj->fix;
                        }
                    }
                    break;

                default :
                    $groupHeader[$group][] = $obj->fix.$name.$obj->fix;
                    break;
            }
        }
        return $groupHeader[$group];
    }



    /** CSV出力データ グループ1生成 */
    function entry_csv_entryMakeData1($obj, $pa_param, $all_flg=false){
        $group = 1;
        $groupBody[$group] = array();
        $groupBody[$group][] = $pa_param["entry_no"];
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            $item_id = $_data["item_id"];

            $methodname = "csvfunc".$item_id;
            // カスタマイズあり
            if(method_exists($obj->exClass, $methodname)) {
                if($item_id == 28){
                    $obj->exClass->$methodname($obj, $group, $pa_param, $item_id, $groupBody);
                    continue;
                }else{
                    if($item_id == 116){
                        $obj->exClass->$methodname($obj, $group, $pa_param, $item_id, $groupBody);
                        continue;
                    }else{
                        $wk_body = $obj->exClass->$methodname($obj, $group, $pa_param, $item_id);
                    }
                }

            // カスタマイズなし
            } else {
                // 任意項目
                if($_data["controltype"] == "1"){
                    $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array("", ","), false);
                    $wk_body = trim($wk_body, ",");

                // 標準項目
                }else{
                    switch($item_id){
                        //会員・非会員
                        case 8:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_kaiin[$pa_param["edata".$item_id]] : "";
                            break;
                        //連絡先
                        case 16:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $obj->wa_contact[$pa_param["edata".$item_id]] : "";
                            break;
                        //都道府県
                        case 18:
                            //英語フォームの場合
                            if($GLOBALS["userData"]["lang"] == "2"){
                                $wk_body =  ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            }
                            else{
                                $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["prefectureList"][$pa_param["edata".$item_id]] : "";
                            }
                            break;
                        //共著者の有無
                        case 29:
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $obj->wa_coAuthor[$pa_param["edata".$item_id]] : "";
                            break;

                        //共著者の所属機関
                        case 31:
                            $wk_kikan_names = array();
                            $wk_kikanlist   = explode("\n", $pa_param["edata".$item_id]);

                            $i =  "";
                            $set_select = array();
                            foreach($wk_kikanlist as $num => $val){
                                $i = $num+1;
                                $set_select[$i] = str_replace(array("\r\n","\n","\r"), '', trim($val));
                                $wk_kikan_names[] = $i.".".str_replace(array("\r\n","\n","\r"), '', trim($val));
                            }
                            $wk_body = str_replace(array("\r\n","\n","\r"), ' ',implode(", ", $wk_kikan_names));
                            break;

                        //Mr. Mis Dr
                        case 57:
                            if($pa_param["edata".$item_id] != ""){
                                $wk_body = $GLOBALS["titleList"][$pa_param["edata".$item_id]];
                            }
                            else{
                                $wk_body = "";
                            }
                            break;
                        //国名リストボックス
                        case 114:
                            $wk_body =  ($pa_param["edata".$item_id] != "") ? $GLOBALS["country"][$pa_param["edata".$item_id]] : "";
                            break;
                        default:
                            $wk_body = ($pa_param["edata".$item_id] != "") ? $pa_param["edata".$item_id] : "";
                            break;
                    }
                }
            }
            $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
        }
        return $groupBody[$group];
    }



    /** Commendation Company */
    function csvfunc28($obj, $group, $pa_param, $item_id, &$groupBody){
        $wk_val = $obj->arrItemData[1][$item_id]["select"][$pa_param["edata".$item_id]];
        if(strpos($wk_val, "=== ◆") !== false) {
            $wk_array = array("", "");
        } else {
            $wk_array = explode(";", $wk_val);
        }

        if(isset($wk_array[1])) {
            $wk_array2 = explode("=", $wk_array[1]);
        }

        $wk_buff   = array();
        $wk_buff[0] = isset($wk_array[0])  ? trim($wk_array[0]) : "";
        $wk_buff[1] = isset($wk_array2[0]) ? trim($wk_array2[0]) : "";
        $wk_buff[2] = isset($wk_array2[1]) ? trim($wk_array2[1]) : "";

        $groupBody[$group][] = $obj->fix.$wk_buff[0].$obj->fix;
        $groupBody[$group][] = $obj->fix.$wk_buff[1].$obj->fix;
        $groupBody[$group][] = $obj->fix.$wk_buff[2].$obj->fix;

        return $wk_body;
    }


    function csvfunc27($obj, $group, $pa_param, $item_id){
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id], array(" ", ","), true);
        $wk_body = trim($wk_body, ",");
        //備考文除去
        if(strstr($wk_body, "<br>", TRUE) !== false){
            $wk_body = strstr($wk_body, "<br>", TRUE);
        }
        return strip_tags($wk_body);
    }

    // form49[reg3] テキストエリアの改行を半角スペースとして扱い、CSVに出力する
    function csvfunc77($obj, $group, $pa_param, $item_id) {
        $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param ["edata" . $item_id], array (
               " ",
               ","
        ), true);
        $wk_body = trim($wk_body, ",");
        return $wk_body;
    }

    /** AccompanyingPerson */
    function csvfunc116($obj, $group, $pa_param, $item_id, &$groupBody){
        foreach($obj->formarray as $item_id){
            $key = 'edata'.$item_id;
            $pa_param[$key] = unserialize($pa_param[$key]);
            foreach($pa_param[$key] as $_key => $str){
                $str = str_replace("[:n:]", "\n", $str);   // 改行コード統一
                $str = stripslashes($str);                 // クォート
                $pa_param[$key][$_key] = $str;
            }
        }

        $wk_body = "";
        $max = max($obj->arrItemData[$group][115]['select']);
        for($i=0; $i<$max; $i++){
            foreach(array(116,117,118,119,120) as $item_id){
                $wk_body = "";
                if(isset($pa_param[ "edata".$item_id][$i])){
                    $wk_body = Usr_Assign::nini($obj, $group, $item_id, $pa_param["edata".$item_id][$i], array(" ", ","), true);
                    $wk_body = strip_tags(trim($wk_body, ","));
                }
                $groupBody[$group][] = $obj->fix.$wk_body.$obj->fix;
            }
        }
    }


   // ------------------------------------------------------
   // ▽Mngカスタマイズ
   // ------------------------------------------------------

    function __constructMng($obj){
        // ログイン時の初期値：query=1
        if(strpos($_SERVER['HTTP_REFERER'], "Mng/login") !== false){
            $_GET['query'] = 1;
        }

        if(method_exists($obj, "assign")){
            $obj->assign("formarray" , $obj->formarray);
            // 管理者メニューの拡張
            $menu = Mng_function::makeMenu();
            unset($menu[0]);
            $_menu[0] = array("menu_name" => "管理トップ", "prg" => APP_ROOT."Mng/index.php?query=1");
            $_menu[1] = array("menu_name" => "エントリー管理", "prg" => APP_ROOT."Mng/index.php");
            $_menu[2] = array("menu_name" => "会社別参加人数確認", "prg" => APP_ROOT."Mng/index.php?query=2");
            array_unshift($menu, $_menu[0], $_menu[1], $_menu[2]);
            $obj->assign("va_menu", $menu);
        }

        // 検索キーの拡張
        if(isset($obj->searchkey)){
            $obj->searchkey[] = "category";
            $obj->searchkey[] = "commendation_company";
            $obj->searchkey[] = "participant_category";
            $obj->searchkey[] = "company";
        }

        // 集計 # index.php?query=x
        if(strpos($_SERVER['SCRIPT_FILENAME'], "Mng/index.php") !== false && isset($_GET['query'])){
            $query = $_GET['query'];

            // 集計用メソッド
            $queryClass = "Mng_query".$query;
            if(method_exists($this, $queryClass)){
                $this->$queryClass($obj);
                $obj->_processTemplate = "Mng/entry/include/49/49query{$query}.html";
                $obj->basemain();
            }
        }
    }


    /** 集計 # 日別の会社登録数  */
    function Mng_query1($obj){
        $db = new DbGeneral();

        //登録数
        $column = "count(eid) as cnt";
        $from = "entory_r";
        $where[] = "form_id = 49";
        $where[] = "del_flg = 0";
        $where[] = "status::integer <> 99";
        $countData = $db->getData($column, $from, $where, __FILE__, __LINE__);
        unset($where);

        //集計データ
        $column = "day";
        $column .= ",COUNT(CASE WHEN edata27 = '1' THEN 1 END) AS cnt1";
        $column .= ",COUNT(CASE WHEN edata27 = '2' THEN 2 END) AS cnt2";
        $column .= ",COUNT(CASE WHEN edata27 = '3' THEN 3 END) AS cnt3";
        $column .= ",SUM(CASE WHEN edata83 = '1' THEN edata115::integer else 0 END) AS num";

        $from = "(";
        $from .= "SELECT eid, e_user_id, edata29, edata83, edata115";
        $from .=  ",CASE WHEN edata30 <> '' THEN edata30::integer";
        $from .=        " WHEN edata30 = '' then 0";
        $from .=   "END AS edata30";
        $from .=  ",edata27";
        $from .=  ",substr(to_char(rdate, 'yyyy/mm/dd'), 1 ,10) as day";
        $from .= " FROM entory_r";
        $from .= " WHERE form_id = 49";
        $from .= " AND status::integer <> 99";
        $from .= " AND del_flg = 0";
        $from .= ") AS data1";
        $from .=" GROUP BY day";

        $where = "";
        $orderby = "day";
        $rs = $db->getListData($column, $from, $where, $orderby);

        $dataList = array("list" => $rs, "count"=> $countData["cnt"]);
        $obj->assign("dataList", $dataList);
    }


    /** 集計 # 会社別参加人数  */
    function Mng_query2($obj){
        $dataList = $this->_Analyze2();

        $_REQUEST["form_id"] = 49;
        $objForm = new Form;
        $formItem = $objForm->getListItem(49);

        // 「Commendation Company」の選択肢
        $wk_select  = explode("\n", $formItem['item_select'][28]);
        $i =  "";
        $set_select1 = array();
        foreach($wk_select as $num => $val){
            $i = $num+1;

            //非活性の対象か否か
            $pos = strpos($val, "#");
            if($pos !== false){
                $set_disable[$i] = "1";
            }
            else{
                $set_disable[$i] = "";
            }

            $set_select1[$i] = trim(str_replace(array("#", "\r\n", "\r", "\n"), "", $val));
        }
        $obj->assign("arrSelect28", $set_select1);


        // 「TPM Awards Associate Agency」の選択肢
        $wk_select  = explode("\n", $formItem['item_select'][74]);
        $i =  "";
        $set_select3 = array();
        foreach($wk_select as $num => $val){
            $i = $num+1;

            //非活性の対象か否か
            $pos = strpos($val, "#");
            if($pos !== false){
                $set_disable[$i] = "1";
            }
            else{
                $set_disable[$i] = "";
            }

            //$set_select3[$i] = $val;
            $set_select3[$i] = str_replace(array("#", "\r\n", "\r", "\n"), "", $val);
        }
        $obj->assign("arrSelect74", $set_select3);


        $arrCompany1 = array(); // 応募情報のうち「Commendation Company」
        $arrCompany2 = array(); // 応募情報のうち「Others such as Consultant Company」
        $arrCompany3 = array(); // 応募情報のうち「TPM Awards Associate Agency」
        if($dataList['count']  > 0){
            foreach($dataList['list'] as $_key => $_value){
                // Commendation Company
                if($_value['edata27'] == 1){
                    $arrCompany1[$_key] = array_search(trim($_value['company']), $set_select1);
                    continue;
                }

                // Others such as Consultant Company
                if($_value['edata27'] == 2){
                    $arrCompany2[$_key] = $_value;
                    continue;
                }

                // TPM Awards Associate Agency
                if($_value['edata27'] == 3){
                    $arrCompany3[$_key] = array_search($_value['company'], $set_select3);
                    continue;
                }

            }
        }

        // 最終的な応募情報
        $arrCompanyR1 = array();
        // 応募情報に全Commendation Companyを反映する
        foreach($set_select1 as $_edata27 => $_value){
            if(preg_match("/^===/", $_value) === 1) continue;

            $key = array_search($_edata27, $arrCompany1);
            // 応募あり
            if($key !== false){
                $arrCompanyR1[] = $dataList['list'][$key];

                // 応募なし
            }else{
                $arrCompanyR1[] = array(
                        'edata27' => 1
                        ,'company' => $_value
                        ,'cnt'     => '0'
                );
            }
        }
        $arrCompanyR3 = array();
        // 応募情報に全TPM Awards Associate Agencyを反映する
        foreach($set_select3 as $_edata77 => $_value){
            $key = array_search($_edata77, $arrCompany3);
            // 応募あり
            if($key !== false){
                $arrCompanyR3[] = $dataList['list'][$key];

                // 応募なし
            }else{
                $arrCompanyR3[] = array(
                        'edata27' => 3
                        ,'company' => $_value
                        ,'cnt'     => '0'
                );
            }
        }

        $arrCompany = array_merge($arrCompanyR1, $arrCompany2, $arrCompanyR3);
        $dataList['list'] = $arrCompany;
        $obj->assign("dataList", $dataList);
    }



    /**
     * データ集計
     */
    private function _Analyze2(){
        $db = new DbGeneral();

        //登録数
        $column = "count(eid) as cnt";
        $from = "entory_r";
        $where[] = "form_id = 49";
        $where[] = "del_flg = 0";
        $where[] = "status::integer <> 99";

        $countData = $db->getData($column, $from, $where, __FILE__, __LINE__);

        unset($where);

        //集計データ
        $column  = " edata27";
        $column .= ",company";
        $column .= ",count(edata27) as cnt";

        $from  =  "(";
        $from .=  " SELECT eid, e_user_id, edata27";
        $from .=  "    ,CASE WHEN edata27 = '1' THEN edata144";
        $from .=  "          WHEN edata27 = '2' THEN edata69";
        $from .=  "          WHEN edata27 = '3' THEN edata144 END as company";
        $from .=  "  FROM entory_r";
        $from .=  "  WHERE form_id = 49";
        $from .=  "  AND status::integer <> 99";
        $from .=  "  AND del_flg = 0";
        $from .=  ") as data1";
        $from .=  " group by edata27, company";

        $where = "";
        $orderby = "edata27";

        $rs = $db->getListData($column, $from, $where, $orderby);

        return array("list" => $rs, "count"=> $countData["cnt"]);
    }


    /** CSVのwhere文を拡張 */
    function entry_csv_getData_filter($obj, $column, $from, $where, $orderby){
        $condition = $this->Mng_buildCondition_after($obj, $obj->arrForm);
        $where     = $this->Mng_buildWhere_after($obj, $condition, $where);

        return array($column, $from, $where, $orderby);
    }


    /** 検索条件の拡張 */
    function Mng_buildCondition_after($obj, $condition){
        // Participant_Category
        $condition["participant_category"] = isset($condition["participant_category"]) ? $condition["participant_category"] : "";
        // Company
        $condition["company"] = isset($condition["company"]) ? $condition["company"] : "";

        // 一括メール送信のモード：ALL用
        if(get_class($obj) == "mailsend" && $obj->mode == "all"){
            // キャンセル以外を対象
            $condition["status2"] = "2";
        }

        return $condition;
    }


    /** 検索条件の拡張 */
    function Mng_buildWhere_after($obj, $condition, $where){
        // participant_category
        if($condition["participant_category"] != "") {
            $where[] = "edata27 = ".$obj->db->quote($condition["participant_category"]);
        }
        // Company
        if($condition["company"] != "") {
            $where[] = "coalesce(edata69, '') || '<:>' || coalesce(edata74, '') || '<:>' || coalesce(edata144, '') like ".$obj->db->quote("%".$condition["company"]."%");
        }

        return $where;
    }


    /** CSV出力の拡張 */
    function Mng_index_quick_csvAction($obj){
        // 検索条件 日付の妥当性チェック
        $obj->arrErr = $obj->check();
        $obj->setSearchkey();

        if(count($obj->arrErr) != 0) return;

        $wo_csv = new entry_csv;
        $wo_csv->arrForm = array();
        $wo_csv->arrForm["status2"] = "2";

        list($wb_ret, $csv_data) = $wo_csv->main();
        if(!$wb_ret){
            $obj->objErr->addErr($csv_data, "csv_data");
            $obj->arrErr = $obj->objErr->_err;
            return;
        }

        $obj->download->csv($csv_data, "entry2_".date('Ymdhis').".csv");
        exit;
    }


    /** 全員を対象に一括メール送信をする */
    function Mng_mailsend_allAction($obj){
        $obj->mode = "all";
        // 全員を検索条件にする
        $page = 1;
        $limit = -1;
        list($count, $obj->arrData) = Mng_function::getListEntry($obj, $page, $limit);
        if(!$count > 0) {
            $obj->complete("送信対象データが存在しません。");
        }

        $obj->assign("count", $count);
    }


    function Mng_mailsend_lfSendMail($obj) {
        mb_internal_encoding("utf-8");
        mb_language("uni");

        $obj->count = 0;

        foreach($obj->arrData as $key=>$val) {
            $obj->arrData[$key]["send"] = "0";

            $to = $val["edata25"];
            if($to == "") continue;

            $subject = $obj->arrForm["mail_ttl"];
            $body    = $obj->arrForm["mail_body"];
            $body    = $obj->replaceStr($body, $val);
            $body    =  str_replace(array("\r\n", "\r"), "\n", $body);


            //送信元
            if( $obj->formWord["word13"] != "" ){
                $ps_fromname = mb_convert_encoding($obj->formWord["word13"], "ISO-2022-JP", "utf-8");
                $ps_from = mb_encode_mimeheader($ps_fromname)."<".$GLOBALS["userData"]["form_mail"]."> ";
            }
            else{
                $ps_from = $GLOBALS["userData"]["form_mail"];
            }

            $head  = "From: ".$ps_from;
            $head .= "\n";
            $head .= "Cc: ".$GLOBALS["userData"]["form_mail"];
            //$head .= "\n";
            //$head .= "Bcc: ".SYSTEM_MAIL;
            $head .= "\n";
            $head .= "Content-Type:text/plain;charset=UTF-8\n";
            $head .= "Content-Transfer-Encoding:BASE64";
            $rs = mb_send_mail($to, $subject, $body, $head);

            if($rs) {
                $obj->arrData[$key]["send"] = "1";
                $obj->count++;
            }
            $obj->arrData[$key]["send_date"] = date("Y-m-d H:i:s");
        }
    }


    /**
      * 一括メール送信の置換文字列を拡張する
      */
    function Mng_mailsend_replaceStr_after($obj, &$body, &$arrForm, &$wa_replaceStr){
       $commendationcompanyList = Usr_Entry49::getCommendationCompanyList();

       $com_name = "";
       $com_cate = "";
       switch($arrForm['edata27']){
           // Commendation Company
           case 1:
               // 会社名
               list($com_id, $com_name) = explode(";", $commendationcompanyList[$arrForm["edata28"]-1]);
               $com_name = trim($com_name);  // \rを削除
               // カテゴリ
               $com_cate = $this->getCategory($arrForm["edata28"]);
               break;

               // Others such as Consultant Company
           case 2:
               // 会社名
               $com_name = $arrForm['edata69'];
               // カテゴリ
               $com_cate = 'Others such as Consultant Company';
               break;

               // TPM Awards Associate Agency
           case 3:
               // 会社名
               $com_name = $arrForm['edata144'];
               // カテゴリ
               $com_cate = 'TPM Awards Associate Agency';
               break;
       }

       $body = str_replace("_COMNO_",       trim($com_id),  $body);
       $body = str_replace("_COMNAME_",     trim($com_name),$body);
       $body = str_replace("_CATEGORY_",    trim($com_cate),$body);
    }


    /**
     * Commendation Campanyのリスト取得
     *
     * セレクトボックスの値の内、===から始まらないデータのみ抽出して配列で返す。
     * キーは、(0から始まる)配列番号
     */
    private static function getCommendationCompanyList() {

       $db = new DbGeneral();

       $where[] = "form_id = 49";
       $where[] = "item_id = 28";

       $res = $db->getData("item_select", "form_item", $where, __FILE__, __LINE__);

       if(!$res) return array();

       $arrList = explode("\n", $res["item_select"]);

       $buffer = array();
       $pattern = "/^===/";
       foreach($arrList as $key=>$val) {
           if(!preg_match($pattern, $val)) {
               $buffer[$key] = $val;
           }
       }

       return $buffer;

    }


    /**
     * カテゴリーのリスト取得
     *
     * セレクトボックスの値の内、===から始まるデータのみ抽出して配列で返す。
     * キーは、(0から始まる)配列番号
     */
    private static function getCategory($id) {

        $db = new DbGeneral();

        $where[] = "form_id = 49";
        $where[] = "item_id = 28";

    		$res = $db->getData("item_select", "form_item", $where, __FILE__, __LINE__);

		if(!$res) return array();

		$arrList = explode("\n", $res["item_select"]);

		$category = "";
		$pattern = "/^===/";
		foreach($arrList as $key=>$val) {
			if(preg_match($pattern, $val)) {
				$wk_array = explode("◆", $val);
				$category = trim(str_replace("Category : ", "", $wk_array[1]));
			}
			if($key == $id-1) {
				return $category;
			}
		}
    }



    /*
     * 確認画面備考文除外
     */
    function pageAction1($obj){
        $fix_flg = "";

        //3ページ目を使用
        if($obj->formdata["group3_use"] != "1"){
            $obj->block = "3";
            $fix_flg = "1";
            $obj->_processTemplate = "Usr/form/Usr_entry.html";
        }

        // 決済フォームを利用 かつ 決済金額が0円より多い場合
        if($obj->formdata["credit"] != "0" && $obj->total_price > 0){
            if(!in_array($obj->form_id, $obj->payment_not)){
                $obj->block = "pay";

                $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                $fix_flg = "1";
            }

            //決済を使用しない場合
        }else{
            //2ページ目使用
            if($obj->formdata["group2_use"] != "1"){
                $obj->block = "2";

                //共著者なしの場合は3ページ目を表示
                if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                    || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                    if($obj->formdata["group3_use"] != "1"){
                        $obj->block = "3";
                        $obj->_processTemplate =  "Usr/form/Usr_entry.html";

                    }else{
                        $obj->block = "4";
                        $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
                    }
                }
                $fix_flg = "1";
            }
        }

        //上記のいずれも該当しなかった場合
        if($fix_flg == ""){
            //27の備考文を除外
            foreach($obj->arrItemData[1][27]['select'] as $key => $value){
                if(strstr($value, "<br>", TRUE) !== false){
                    $obj->arrItemData[1][27]['select'][$key] = strstr($value, "<br>", TRUE);
                }
            }
            $obj->block = "4";
            $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
        }
    }

    /*
     * 確認画面備考文除外
     */
    function _confirm($obj){
        //フォームパラメータ
        $obj->arrParam = GeneralFnc::convertParam($obj->_init($obj->wk_block), $_REQUEST);

        //入力チェック
        $obj->arrErr = $obj->_check();
        if(!(count($obj->arrErr) > 0)){
            foreach($obj->arrfile as $item_id => $n){
                if($obj->itemData[$item_id]["disp"] != "1"){
                    //添付資料アップロード
                    list($wb_ret, $msg) = $obj->fileTmp($item_id);
                    if(!$wb_ret){
                        $obj->arrErr["file_error".$item_id] = $msg;
                    }
                }
            }
        }

        //セッションにページパラメータをセットする
        $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $obj->arrParam);

        //セッションパラメータ取得
        $obj->getDispSession();

        //エラーを自ページに表示
        if(count($obj->arrErr) > 0){
            $obj->block = $obj->wk_block;
        }else{
            //27の備考文を除外
            foreach($obj->arrItemData[1][27]['select'] as $key => $value){
                if(strstr($value, "<br>", TRUE) !== false){
                    $obj->arrItemData[1][27]['select'][$key] = strstr($value, "<br>", TRUE);
                }
            }
            $obj->block = "4";
            $obj->_processTemplate = "Usr/form/Usr_entry_confirm.html";

            //お支払合計金額の計算
            if($obj->formdata["kessai_flg"] == "1" ){
                $obj->arrForm["total_price"] = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
            }
        }
    }

}
