<?php

/**
 * reg3form3番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.12.09
 * 
 */
class Usr_Entry3 {

    function __construct($obj){
        // クーポン停止ファイルの有無をチェック
        $form_id   = $obj->o_form->formData['form_id'];
        $customDir = ROOT_DIR."customDir/".$form_id."/";

        $obj->isStopCoupon1 = false;    // クーポン1の停止フラグ
        $obj->isStopCoupon2 = false;    // クーポン2の停止フラグ
        $stopCoupon = "StopCoupon";
        $ex = glob($customDir.$stopCoupon.'*');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                $obj->isStopCoupon1 = true;
                $obj->isStopCoupon2 = true;
            }
        }


        $obj->arrOtherPrice01 = array();
//        $obj->arrOtherPrice01[1] = 'I will attend.';
        $obj->arrOtherPrice01[0] = 'I will NOT attend.';

        $obj->arrOtherPrice02 = array();
        $obj->arrOtherPrice02[1] = 1;
        $obj->arrOtherPrice02[2] = 2;

        if(method_exists($obj, "assign")){
            // 「Banquet for participants」を「I will NOT attend.」で固定
            $_REQUEST['ather_price0'] = 0;

            $obj->assign('arrOtherPrice01', $obj->arrOtherPrice01);
            $obj->assign('arrOtherPrice02', $obj->arrOtherPrice02);
        }

        // 支払方法をクレジット固定
        unset($GLOBALS["method_E"][2]);
    }

    function premain($obj){
        // 「Banquet for participants」を「I will NOT attend.」で固定
        $obj->arrForm['ather_price0'] = 0;
    }

    // 項目マスタ
    function setFormIni($obj) {
        Usr_initial::setFormIni($obj);

        // クーポンの発行の有無によって表示、非表示を切り替える

        // クーポン1の発行が無効の場合
        if($obj->isStopCoupon1){
            $obj->arrItemData[1][28]['disp'] = 1;
        }
        // クーポン2の発行が無効の場合
        if($obj->isStopCoupon2){
            $obj->arrItemData[1][63]['disp'] = 1;
        }

        // クーポン保存用edataは非表示
        $obj->arrItemData[1][143]['disp'] = 1;
        $obj->arrItemData[1][144]['disp'] = 1;
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        Usr_Check::_check1($obj);

        // 半角4桁チェック
        $checkUnique = 0;
        // クーポン1の発行が有効の場合のみ
        if(!$obj->isStopCoupon1){
            if(isset($obj->arrParam['edata28']) && strlen($obj->arrParam['edata28']) > 0){
                $len = strlen($obj->arrParam['edata28']);
                if($len < 4){
                    $obj->objErr->addErr("Enter 4 character for ".Usr_init::getItemInfo($obj, 28).".", "edata28");
                }else if(!isset($obj->objErr->_err['edata28'])){
                    $checkUnique++;
                }
            }
        }
        // クーポン2の発行が有効の場合のみ
        if(!$obj->isStopCoupon2){
            if(isset($obj->arrParam['edata63']) && strlen($obj->arrParam['edata63']) > 0){
                $len = strlen($obj->arrParam['edata63']);
                if($len < 4){
                    $obj->objErr->addErr("Enter 4 character for ".Usr_init::getItemInfo($obj, 63).".", "edata63");
                }else if(!isset($obj->objErr->_err['edata63'])){
                    $checkUnique++;
                }
            }
        }
        // 1,2の両方を入力してエラーが発生していない場合
        // クーポン1,2の発行が有効の場合のみ
        if(!$obj->isStopCoupon1 && !$obj->isStopCoupon2){
            if($checkUnique == 2){
                // 一致していたらエラー
                if($obj->arrParam['edata28'] == $obj->arrParam['edata63']){
                    $e28 = Usr_init::getItemInfo($obj, 28);
                    $e63 = Usr_init::getItemInfo($obj, 63);
                    $obj->objErr->addErr(sprintf("%s must not match with %s.", $e28, $e63), "edata28");
                }
            }
        }


        //------------------------------
        //支払金額
        //------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            // edata26のエラー発生フラグ
            $isError26 = false;

            // Feeで特定の項目を選択していたら、必須
            // → Name of association / organization ...
            // → Membership No. 
            $chkFee = array(0,1,2,3,5,7);
            if(in_array($obj->arrParam['amount'], $chkFee)){
                // 26 : Name of association / organization of which you are a member
                // 27 : Membership No.
                $rEdata = array(26, 27);

                foreach($rEdata as $_key){
                    $key  = "edata".$_key;
                    $name = Usr_init::getItemInfo($obj, $_key);

                    if(!$obj->objErr->isNull($obj->arrParam[$key])) {
                        $method = Usr_init::getItemErrMsg($obj, $_key);
                        $obj->objErr->addErr(sprintf($method, $name), $key);

                        if($_key == 26){
                            $isError26 = true;
                        }
                    }
                }
            }
            // 同じ団体を選択しているかチェック
            if(!$isError26){
                // Feeに対する26の対応配列
                $arrFeeto26 = array();
                $arrFeeto26[0] = array(1,2,3);
                $arrFeeto26[1] = array(6,4,5);
                $arrFeeto26[2] = array(7);
                $arrFeeto26[3] = array(8,9,10,11,12,13,14,15,16,17);
                $arrFeeto26[4] = array();
                $arrFeeto26[5] = array(1,2,3,6,7,8,9,10,11,12,13,14,15,16,4,5,17);
                $arrFeeto26[6] = array();
                $arrFeeto26[7] = array(1,2,3,6,7,8,9,10,11,12,13,14,15,16,4,5,17);

                // NomMember以外を選択
                $amount = $obj->arrParam['amount'];
                if(isset($arrFeeto26[$amount])){
                    if(!in_array($obj->arrParam['edata26'], $arrFeeto26[$amount])
                    && strlen($obj->arrParam['edata26']) > 0){
                        $obj->objErr->addErr("Registration category you selected cannot be applied to the association/organization of which you are a member.", "edata26");
                    }
                }
            }


            // その他決済のエラーチェック
            if($obj->formdata["kessai_flg"] == "1" && $obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

                // Banquetを必須
                $key = "ather_price0";
                if(!$obj->objErr->isNull($obj->arrParam[$key])) {
                    $obj->objErr->addErr("Select one for Banquet for participants .", $key);
                }


                // ▽Categoryチェック
                $categoryCount = self::getCategoryCount($obj);

                // Category：複数選択した場合（イレギュラー）
                if($categoryCount > 1){
                    $obj->objErr->addErr("Select one for Category for additional paper fee.", "ather_price2");
                    return;
                }


                // Category：1つ選択した場合
                // クーポン2の発行が有効の場合のみ
                if(!$obj->isStopCoupon2){
                    if($categoryCount == 1){
                        // 「Paper reference number 2」を必須
                        $_key = "63";
                        $key  = "edata63";
                        $name = Usr_init::getItemInfo($obj, $_key);

                        if(!$obj->objErr->isNull($obj->arrParam[$key])) {
                            $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $name), $key);
                        }
                    }

                    // Category：未選択
                    if($categoryCount == 0){
                        // 「Paper reference number 2」が入力されている
                        // -> Categoryを必須
                        $key  = "edata63";

                        if($obj->objErr->isNull($obj->arrParam[$key])) {
                            $obj->objErr->addErr("Select one for Category for additional paper fee.", "ather_price2");
                        }
                    }
                }


                // FeeとCategoryが同じ団体をチェックしているかチェック
                if($categoryCount == 1){
                    // Feeに対するCategoryの対応配列
                    $arrFeetoCt = array();
                    $arrFeetoCt[0] = "ather_price2";
                    $arrFeetoCt[1] = "ather_price3";
                    $arrFeetoCt[2] = "ather_price4";
                    $arrFeetoCt[3] = "ather_price5";
                    $arrFeetoCt[4] = "ather_price6";
                    //         #5 nom member
                    //         #6 nom member
                    //         #7 nom member

                    // NomMember以外を選択
                    $amount = $obj->arrParam['amount'];
                    if(isset($arrFeetoCt[$amount])){
                        $otherKey = $arrFeetoCt[$amount];
                        if(!isset($obj->arrParam[$otherKey]) || $obj->arrParam[$otherKey] != 1){
                            $obj->objErr->addErr("Category for registration and that for additional paper fee should be the same.", $otherKey);
                            unset($obj->objErr->_err["edata63"]);
                        }
                    }else{
                        $obj->objErr->addErr("Category for registration and that for additional paper fee should be the same.", "ather_price");
                        unset($obj->objErr->_err["edata63"]);
                    }
                }
            }
        }
        return;
    }

    /* Categoryの選択数を返す */
    function getCategoryCount($obj){
        $otherGroup3 = array(2,3,4,5,6);
        $arrOther03  = array();
        foreach($otherGroup3 as $key => $_key){
            $otherprice = 'ather_price'.$_key;
            if(!isset($obj->arrParam[$otherprice])) continue;
            if(strlen($obj->arrParam[$otherprice]) == 0) continue;

            $arrOther03[] = $obj->arrParam[$otherprice];
        }
        return count($arrOther03);
    }


    /**
     * フォーム項目の並び順変更
     * 継承して利用する
     * 対象変数 : $this->arrItemData
     */
    function developfunc($obj) {
//        $obj->eid = 1520;
//        $obj->couponNum = 2;
//        print "--------------------<pre style='text-align:left;'>";
//		print_r($obj->replaceMsg($obj->formWord["word1"]));
//		print "</pre><br/><br/>";
    }


    /**
     * 1ページ目 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init1($obj){
        $key = Usr_init::_init1($obj);
        Usr_init::overrideKey($key, "edata28", Usr_init::INIT_OPTION, "nk");
        Usr_init::overrideKey($key, "edata63", Usr_init::INIT_OPTION, "nk");
        return $key;
    }


    /* クレジットカードの新規登録処理 */
    function payAction1($obj){
        // 登録番号の取得を3Dセキュアの有無で分岐
        $obj->user_id = "";

        // エントリーID採番
        $obj->ins_eid  = $GLOBALS["db"]->getOne("select nextval('entory_r_eid_seq')");

        //---------------------------------------------------------
        // クレジット決済処理 (3Dセキュアは取引完了まで行う)
        //---------------------------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            $isPay = $obj->doCardPayAction();
            // 取引または決済に失敗した場合
            // 無効フラグを立てて決済ログに残す
            if(!$isPay){
                list($wb_ret, $obj->user_id, $obj->passwd) = $obj->insertEntry();
                $obj->eid = $obj->ins_eid;
                $obj->entryInvalid();
                $obj->db->commit();
                $obj->lockEnd();

                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? $GLOBALS["payerror_J"]
                     : $GLOBALS["payerror_E"];

                $arrErr = $obj->auth_param["arrErr"];
                $obj->complete($msg, implode("<br/>", $arrErr));
            }
        }

        // ファイルアップロード
        if(!$obj->TempUploadAction()){
            $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
            $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
            $obj->db->rollback();
            $obj->lockEnd();
            $obj->complete($msg);
            return;
        }

        // 3Dセキュア未使用
        if($obj->formdata["pgcard_use3ds"] != "1" || !$obj->o_authorize->isTdSecure()){
            // クーポンの発行
            $this->doCoupon($obj);

            // エントリーIDの取得
            $obj->getEntryNumber();
            $obj->user_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->entry_number);
        }

        //新規登録処理
        list($wb_ret, $obj->user_id, $obj->passwd) = $obj->insertEntry();
        $obj->eid = $obj->ins_eid;

        //---------------------------------------------------------
        // 3Dセキュアのクレジット決済処理
        //---------------------------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            $obj->doCardPay3dAction();
        }

        // ファイルの移動
        if(!$obj->MoveFileAction()) {
            $obj->db->rollback();
            $obj->lockEnd();
            $obj->complete("ファイルの移動に失敗しました。");
            return;
        }
    }


    /** 3Dセキュア決済ページ */
    function pageActionpay3d($obj) {
        // 二重登録チェック
        $rl = new Reload();
        if($rl->isReload()) {
            $msg = $GLOBALS["msg"]["reload"];
            $obj->complete($msg);
            return;
        }

        // エントリー番号
        $obj->eid = $_POST["c_field1"];
        $obj->ins_eid = $obj->eid;

        // 更新データ
        $param = array();

        // エラー配列
        $arrErr = $_POST['arrErr'];

        // 結果=成功
        if(count($arrErr) == 0) {
            $obj->lockStart();
            $obj->db->begin();

            // クーポンの発行
            $this->doCoupon($obj);
            $param["edata143"] = $obj->arrForm["edata143"];
            $param["edata144"] = $obj->arrForm["edata144"];

            // エントリーIDの取得
            $obj->getEntryNumber();
            $obj->user_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->entry_number);

            $obj->u_date = date('YmdHis');
            Usr_initial::createUploadDir($obj);
            // アップロード情報を取得
            $obj->arrForm = $GLOBALS["session"]->getVar("form_param3");
            $obj->arrForm['hd_file51'] = "";
            $obj->arrForm['hd_file52'] = "";
            $obj->arrForm['hd_file53'] = "";

            // 3Dセキュアのキャンセルを考慮し、元ファイル名を復元
            list($wa_authdata, $wa_payment_detail1, $wa_payment_detail2) = $obj->getPayData();
            if(strlen($wa_authdata['c_field_3'])){
                $arrFiles = explode(",", $wa_authdata['c_field_3']);
                $GLOBALS["session"]->setVar("workDir", $arrFiles[0]);
                $obj->arrForm['hd_file51'] = $arrFiles[1];
                $obj->arrForm['hd_file52'] = $arrFiles[2];
                $obj->arrForm['hd_file53'] = $arrFiles[3];
            }

            // ファイルアップロード
            if(!$obj->TempUploadAction()){
                $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
                $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
                $obj->db->rollback();
                $obj->lockEnd();
                $obj->complete($msg);
                return;
            }

            // 有効なファイル名
            $obj->wk_saki[51] = $obj->user_id.$obj->arrForm["edata51"];
            $obj->wk_saki[52] = $obj->user_id.$obj->arrForm["edata52"];
            $obj->wk_saki[53] = $obj->user_id.$obj->arrForm["edata53"];

            // 更新データ
            $param['e_user_id']   = $obj->user_id;
            $param["invalid_flg"] = "0"; // 無効フラグを戻す
            $param["udate"] = "Now()";
            if(strlen($obj->arrForm['hd_file51']) > 0) $param["edata51"] = $obj->user_id.$obj->arrForm['edata51'];
            if(strlen($obj->arrForm['hd_file52']) > 0) $param["edata52"] = $obj->user_id.$obj->arrForm['edata52'];
            if(strlen($obj->arrForm['hd_file53']) > 0) $param["edata53"] = $obj->user_id.$obj->arrForm['edata53'];

            $where   = array();
            $where[] = "eid = ".$obj->db->quote($obj->eid);
            $obj->db->update("entory_r", $param, $where, __FILE__, __LINE__);

            $param = array();
            $param["payment_status"] = "3";        //支払いステータスを「売上完了」にする
            $param["udate"] = "Now()";
            $obj->db->update("payment", $param, $where, __FILE__, __LINE__);

            // ファイルの移動
            if(!$obj->MoveFileAction()) {
                $obj->db->rollback();
                $obj->lockEnd();
                $obj->complete("ファイルの移動に失敗しました。");
                return;
            }

            $obj->db->commit();

            //メール送信
            $obj->sendRegistMail($obj->user_id, "");

            //完了メッセージ
            $obj->msg = $obj->replaceMsg($obj->formWord["word1"]);

            $obj->lockEnd();

            $obj->showComplete();
            exit;
        }


        // 結果=失敗
        $obj->msg = ($obj->formdata["lang"] == LANG_JPN)
                  ? $GLOBALS["3dpayerror_J"]
                  : $GLOBALS["3dpayerror_E"];
        $obj->arrErr = $arrErr;

        //支払いステータスを「与信失敗」にする
        $obj->lockStart();
        $obj->db->begin();
        $param["payment_status"] = "8";
        $param["udate"] = "Now()";
        $where[] = "eid = ".$obj->db->quote($obj->eid);
        $obj->db->update("payment", $param, $where, __FILE__, __LINE__);
        $obj->db->commit();
        $obj->lockEnd();

        // エントリーIDをセッションに保存
        $GLOBALS["session"]->setVar("pre_eid", $obj->eid);

        $obj->paymentErrAction();
    }


    function doCoupon($obj){
        // 発行したクーポンの数
        $obj->couponNum = 0;

        if($obj->isStopCoupon1) return false;
        // isStopCoupon2は考慮しない
 
        // 特定のFeeはクーポンを発行しない
        if(in_array($obj->arrForm['amount'], array(5,6,7))) return false;
 
        $arrCoupon = $this->getCouponList($obj);
        if(count($arrCoupon) == 0) return false;

        // 非表示フラグを下げる
        $obj->arrItemData[1][143]['disp'] = 0;
        $obj->arrItemData[1][144]['disp'] = 0;

        $num = 0;
        $triger    = array( 28, 63);
        $couponKey = array(143, 144); // 保存先のedata

        foreach($triger as $i => $_key){
            $key = "edata".$_key;
            if(isset($obj->arrForm[$key]) && strlen($obj->arrForm[$key]) > 0){
                $key = "edata".$couponKey[$i];
                $obj->arrForm[$key] = $this->getDoCoupon($obj, $arrCoupon);
                $num++;
            }
        }

        $obj->couponNum = $num;
    }


    /* クーポンの発行 */
    function getDoCoupon($obj, &$arrCoupon){
        if(count($arrCoupon) == 0) return "";

        $key    = count($arrCoupon)-1;
        $coupon = $arrCoupon[$key];
        unset($arrCoupon[$key]);

        return $coupon;
    }


    /* 未発行のクーポンを取得 */
    function getCouponList($obj){
        $arrCoupon = array();

        $couponDir = ROOT_DIR."templates/Usr/include/3/3coupon.txt";
        if(!file_exists($couponDir)) return $arrCoupon;

        // 2000件のクーポンリスト（全件)
        $strCoupon = GeneralFnc::readFile($couponDir);
        $strCoupon = str_replace(array("\r\n", "\r", "\n"), ",", $strCoupon);
        $arrCoupon = explode(",", $strCoupon);

        // 発行済みのクーポン
        $column = "edata143, edata144";
        $from   = "entory_r";
        $where  = array();
        $where[]= "form_id = 3";
        $where[]= "del_flg = 0";
        $where[]= "invalid_flg = 0";

        $doCouponList = $obj->db->getListData($column, $from, $where);

        // 
        $doCoupon = array();
        if(is_array($doCouponList) && count($doCouponList) > 0){
            foreach($doCouponList as $_key => $_doCoupon){
                if(strlen($_doCoupon['edata143']) > 0){
                    $doCoupon[] = $_doCoupon['edata143'];
                }
                if(strlen($_doCoupon['edata144']) > 0){
                    $doCoupon[] = $_doCoupon['edata144'];
                }
            }
        }

        // 未発行のクーポン（発行済みクーポンとの差分）
        $arrCoupon = array_diff($arrCoupon, $doCoupon);
        $arrCoupon = array_values($arrCoupon);

//        print "未発行--------------------<pre style='text-align:left;'>";
//		print_r($arrCoupon);
//		print "</pre><br/><br/>";
//        print "発行済み--------------------<pre style='text-align:left;'>";
//		print_r($doCoupon);
//		print "</pre><br/><br/>";

        return $arrCoupon;
    }


    /** edata73 */
    function mailfunc73($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str  = "\n";
        $str .= "\n";
        $str .= "[Accompanying Person]\n";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }


    /** edata77 */
    function mailfunc77($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }



    function replaceMsg($obj, $msg) {
        $entryData = $obj->o_entry->getRntry_r($obj->db, $obj->eid, $obj->form_id);

        $wa_replaceStr                  = array();
        $wa_replaceStr["ID"]            = $entryData["e_user_id"];
        $wa_replaceStr["SEI"]           = $entryData["edata1"];
        $wa_replaceStr["MEI"]           = $entryData["edata2"];
        $wa_replaceStr["MIDDLE"]        = $entryData["edata7"];
        $wa_replaceStr["TITLE"]         = isset($GLOBALS["titleList"][$entryData["edata57"]]) ? $GLOBALS["titleList"][$entryData["edata57"]] : "";
        $wa_replaceStr["SECTION"]       = $entryData["edata10"];
        $wa_replaceStr["PASSWORD"]      = $entryData["e_user_passwd"];
        $wa_replaceStr["INSERT_DAY"]    = date("Y-m-d H:i:s", strtotime($entryData["rdate"]));
        $wa_replaceStr["UPDATE_DAY"]    = date("Y-m-d H:i:s", strtotime($entryData["udate"]));

        $msg = Usr_function::wordReplace($msg, $wa_replaceStr);


        // クーポンの発行数に応じて表示を切り替える
        $msgSub = "";
        // isStopCoupon2は考慮しない
        if(!$obj->isStopCoupon1){
            // クーポンの発行数に応じて表示を切り替える
            $msgSub = "";
            switch($obj->couponNum){
                case 1:
                case 2:
                    $msgSub .= "<br/>";
//                    $msgSub .= "[PaperSubmissionKey]<br/>";
                    if(strlen($entryData["edata143"])){
                        $msgSub .= "<b>Paper Submission Key: ".$entryData["edata143"]."</b><br/>";
                    }
                    if(strlen($entryData["edata144"])){
                        $msgSub .= "<b>Paper Submission Key 2: ".$entryData["edata144"]."</b><br/>";
                    }
                    $msgSub .= "<br/>";
                    $msgSub .= "*In the process of manuscript submission, you are required to enter your registration number and Paper Submission Key.<br/>";
                    $msgSub .= "<br/>";
//                    $msgSub .= '<input type="submit" name="papersubmit" value="Manuscript Submission Click here" onclick="document.frm.action=\'http://www.ipec2014.org/manuscript.html\'" /><br/>';
                    $msgSub .= '<div id="papersubmit"><a href="http://www.ipec2014.org/manuscript.html" target="_blank">Manuscript Submission<br/>Click here</a></div><br/>';
                    $msgSub .= '<style>';
                    $msgSub .= 'div#papersubmit{ background: none repeat scroll 0 0 #EEEEEE; border: 1px solid #CCCCCC; padding: 5px 15px; width: 150px; cursor: pointer; text-align:center; }';
                    $msgSub .= 'div#papersubmit a { text-decoration:none; color:#000000;}';
                    $msgSub .= '</style>';
                    break;

                default: $msgSub = ""; break;
            }
        }
        $msg = str_replace("[PaperSubmissionKey]", $msgSub, $msg);

        return $msg;
    }



    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $msg = Usr_mail::makeMailBody_header($obj, $user_id, $passwd, $exec_type);

        // クーポンの発行数に応じて表示を切り替える
        $msgSub = "";
        // isStopCoupon2は考慮しない
        if(!$obj->isStopCoupon1){
            // クーポンの発行数に応じて表示を切り替える
            $msgSub = "";
            switch($obj->couponNum){
                case 1:
                case 2:
                    $msgSub .= "\n";
//                    $msgSub .= "[PaperSubmissionKey]\n";
                    if(strlen($obj->arrForm["edata143"])){
                        $msgSub .= "Paper Submission Key: ".$obj->arrForm["edata143"]."\n";
                    }
                    if(strlen($obj->arrForm["edata144"])){
                        $msgSub .= "Paper Submission Key 2: ".$obj->arrForm["edata144"]."\n";
                    }
                    $msgSub .= "\n";
                    $msgSub .= "*In the process of manuscript submission, you are required to enter your registration number and Paper Submission Key.\n";
                    $msgSub .= "http://www.ipec2014.org/manuscript.html\n";
                    break;

                default: $msgSub = ""; break;
            }
        }
        $msg = str_replace("[PaperSubmissionKey]", $msgSub, $msg);


        return $msg;
    }



    function makePaymentBody($obj, $exec_type){

        if($obj->formdata["lang"] == LANG_JPN){
            $point_mark = "■";
            $body_pay = "\n\n【お支払情報】\n\n";
        }else{
            $point_mark = "*";
            $body_pay = "\n\n[Payment Information]\n\n";
        }

        //更新の場合は、登録済みのデータから引っ張ってくる
        if($exec_type == "2"){
            $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid);
        }

        //支払合計
        if($exec_type == "1"){
            $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);

        }else{
            $total = $GLOBALS["session"]->getVar("ss_total_payment");
        }

        //-----------------------------------------
        // 決済情報
        //-----------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){

            //$body_pay .= "\n\n";
            if($obj->formdata["lang"] == LANG_JPN){
                $body_pay .= $point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
                $body_pay .= $point_mark."金額: \n";
            }
            else{
                if($total > 0){
                    $body_pay .= ($obj->arrForm["method"] != "3") 
                                ? $point_mark."Payment:".$obj->wa_method[$obj->arrForm["method"]]."\n"
                                : $point_mark."Payment:Cash on-site\n";
                }
                $body_pay .= $point_mark."Amount of Payment:\n";
            }


            //新規の場合
            if($exec_type == "1"){
                foreach($obj->wa_price as $p_key => $data){
                    if(!isset($obj->arrForm["amount"])) continue;
                    if($p_key == $obj->arrForm["amount"]){
                        $price = $data["p1"];
                        if(is_numeric($price)){
                            $price = number_format($data["p1"]);
                            $price = ($obj->formdata["lang"] == LANG_JPN)
                                   ? $price."円"
                                   : "\\".$price."";
                        }

                        // タグを除去
                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);

                        if($obj->formdata["pricetype"] == "2"){
                            if($obj->formdata["lang"] == LANG_JPN){
                                $body_pay .= "　　　".$data["name"]."：".$price."\n";
                            }
                            else{
                                $body_pay .= "      ".$data["name"].":".$price."\n";
                            }
                        }
                        else{
                            if($obj->formdata["lang"] == LANG_JPN){
                                $body_pay .=  "　　　".$obj->formdata["csv_price_head0"]."：".$price."\n";
                            }
                            else{
                                $body_pay .=  "      ".$obj->formdata["csv_price_head0"].":".$price."\n";
                            }
                        }
                        break;
                    }
                }
            }

            //更新の場合
            if($exec_type == "2"){
                //全てのその他決済で項目を生成しているため、出力は不要
            }


            //その他決済がある場合
            if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

                //新規の場合
                if($exec_type == "1"){
                    foreach($obj->wa_ather_price  as $p_key => $data ){
                        if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                        if($obj->arrForm["ather_price".$p_key] != ""){
                            $price = $data["p1"];

                            // Free, - => 0
                            if(!is_numeric($data["p1"])){
                                $data["p1"] = 0;
                            }
                            $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                            // タグを除去
                            $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);

                            if($obj->formdata["lang"] == LANG_JPN){
                                if(is_numeric($price)){
                                    $price = $price."円";
                                }
                                $body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
                            }
                            else{
                                if(is_numeric($price)){
                                    $price = "\\".$price;
                                }
                                $body_pay .="      ".$data["name"].":\\".number_format($sum)."  （".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
                            }
                        }
                    }
                    
                }

                //更新の場合
                if($exec_type == "2"){
                    foreach($wa_payment_detail as $data){
                        $price = $data["price"];
                        // Free, - => 0
                        if(!is_numeric($data["price"])){
                            $data["price"] = 0;
                        }
                        $sum = $data["price"]*$data["quantity"];

                        // タグを除去
                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = $price."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = "\\".$price;
                            }
                            $body_pay .="      ".$data["name"].":\\".number_format($sum);
                        }

                        // その他決済の場合は内訳を後ろにつける
                        if($data["type"] == "1"){
                            if($obj->formdata["lang"] == LANG_JPN){
                                $body_pay .="　（".$price." ×".$data["quantity"]."）";
                            }
                            else{
                                $body_pay .="  （".$price." ×".$data["quantity"]."）";
                            }
                        }
                        $body_pay .= "\n";
                    }
                }
            }


            if($obj->formdata["lang"] == LANG_JPN){
                $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
            }
            else{
                $body_pay .="      Amount of Total Payment:\\".number_format($total)."\n\n";
            }



            if($total > 0){
                switch($obj->arrForm["method"]){
                    case "1":
                        if($obj->formdata["lang"] == LANG_JPN){
                            $wk_pay_card= "カード会社";
                            $wk_pay_card_num = "クレジットカード番号";
                            $wk_pay_card_limit = "有効期限";
                            $wk_pay_card_meigi = "カード名義人";
                            $wk_pay_month = "月";
                            $wk_pay_year = "年";
                        }
                        else{
                            $wk_pay_card= "Card Type";
                            $wk_pay_card_num = "Card Number";
                            $wk_pay_card_limit = "Expiration Date";
                            $wk_pay_card_meigi = "Card Holder's Name";
                            $wk_pay_month = "Month";
                            $wk_pay_year = "Year";
                        }

                        $body_pay .=$point_mark.$wk_pay_card.":".$obj->wa_card_type[$obj->arrForm["card_type"]]."\n";
                        $body_pay .=$point_mark.$wk_pay_card_num.":";

                        $a = strlen($obj->arrForm["cardNumber"])-2;
                        if($a < 0){ $a=2; }
                        $card = "";
                        $body_pay .= sprintf("%'*".$a."s", $card).substr($obj->arrForm["cardNumber"],-2)."\n";

                        $body_pay .=$point_mark.$wk_pay_card_limit.":".$obj->arrForm["cardExpire1"].$wk_pay_month."　".$obj->arrForm["cardExpire2"].$wk_pay_year."\n";
                        $body_pay .=$point_mark.$wk_pay_card_meigi.":".$obj->arrForm["cardHolder"]."\n";
                        break;

                    case "2":
                        if($obj->formdata["lang"] == LANG_JPN){
                            $wk_pay_furikomi= "お振込み人名義";
                            $wk_pay_bank = "お支払銀行名";
                            $wk_pay_furikomibi = "お振込み日";
                        }
                        else{
                            $wk_pay_furikomi= "Name of remitter";
                            $wk_pay_bank = "Name of bank which you will remit";
                            $wk_pay_furikomibi = "Date of remittance";
                        }

                        $pay2key = array("lessee", "bank", "closure");
                        foreach($pay2key as $_key => $_val){
                            if(!isset($obj->arrForm[$_val])) $obj->arrForm[$_val] = "";
                        }

                        $body_pay .=$point_mark.$wk_pay_furikomi.":".$obj->arrForm["lessee"]."\n";
                        $body_pay .=$point_mark.$wk_pay_bank.":".$obj->arrForm["bank"]."\n";
                        $body_pay .=$point_mark.$wk_pay_furikomibi.":".$obj->arrForm["closure"]."\n";
                        break;

                    default:
                }
            }
        }
        return $body_pay;
    }




    /**
     * 支払詳細項目登録
     *
     */
    function _insPaymentDetail($obj, $pn_eid){

        //------------------------------------------
        //支払詳細
        //------------------------------------------
        //通常項目
        $param = "";
        $param["eid"] = $pn_eid;
        $param["name"] = "事前登録料金";

        if($obj->formdata["pricetype"] == "2"){
            $param["name"] .= "（".$obj->wa_price[$obj->arrForm["amount"]]["name"]."）";
            $param["key"] = $obj->arrForm["amount"];
        }

        $param["price"] = $obj->wa_price[$obj->arrForm["amount"]]["p1"];
        $param["quantity"] = "1";


        //登録処理実行
        $rs = $obj->db->insert("payment_detail", $param, __FILE__, __LINE__);
        if(!$rs) {
            //return array(false, "", "");
            return false;
        }

        //その他決済項目
        if($obj->formdata["ather_price_flg"] != "1"){
            foreach($obj->wa_ather_price as $pkey => $data){
                if($obj->arrForm["ather_price".$pkey] !=""){
                    $param = "";
                    $param["eid"] = $pn_eid;
                    $param["name"] = $data["name"];
                    $param["price"] = $data["p1"];
                    $param["quantity"] = $obj->arrForm["ather_price".$pkey];
                    $param["type"] = "1";
                    // 何番目の項目を選択したか保存する
                    $param["key"] = $pkey;

                    //登録処理実行
                    $rs = $obj->db->insert("payment_detail", $param, __FILE__, __LINE__);
                    if(!$rs) {
                        //return array(false, "", "");
                        return false;
                    }
                }
            }
        }

        return true;

    }


}
