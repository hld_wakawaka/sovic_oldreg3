<?php

class Usr_Entry27 {

//    /* デバッグ用 */
//    function developfunc($obj) {
//        // メールデバッグ
//        $obj->ins_eid = 1700;
//        print "--------------------<pre style='text-align:left;'>";
//        print_r($obj->makeMailBody('VISA-00002', "rhfN2L3T", 2));
//        print "</pre><br/><br/>";
//    }


    // エラーチェック # ブロック1
    function _check3($obj){
        Usr_Check::_check3($obj);

        $group_id = 3;

        // xxで2番目の項目を選択したらテキスト必須
        $item_id = 54;
        $key = "edata".$item_id;
        if(Usr_init::isset_ex($obj, $group_id, $item_id) && $obj->arrParam[$key."2"] == 2){
            $item_id = 55;
            $key = "edata".$item_id;
            if(Usr_init::isset_ex($obj, $group_id, $item_id) && !$obj->objErr->isNull($obj->arrParam[$key])){
                $name = Usr_init::getItemInfo($obj, $item_id);
                $method = Usr_init::getItemErrMsg($obj, $item_id);
                $msg = rtrim(sprintf($method, $name), '.:').'.';
                $obj->objErr->addErr($msg, $key);
            }
        }

        // Feeが「APAPU or ESPU」は「Non-member」を選択してはいけない
        $arrForm = $GLOBALS["session"]->getVar("form_param1");
        $amount = $arrForm['amount'];
        if(strlen($amount) > 0 && intval($amount) == 0){
            // Non-member select
            $item_id = 54;
            $key = "edata".$item_id."3";
            if(isset($obj->arrParam[$key]) && $obj->arrParam[$key] == 3){
                $obj->objErr->addErr("Don't select Non-Member of 1）Please check if you are a Member of APAPU or ESPU.", $key);
            }
        }
    }



    //------------------------------------------------
    // ▽ メールカスタマイズ
    //------------------------------------------------

    /** AccompanyingPerson1 # mailfunc[AccompanyingPerson1の最初のitem_id] */
    function mailfunc73($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str  = "\n";
        $str .= "\n";
        $str .= "[Accompanying Person(s)]\n";
        $str .= "\n";
        $str .= "Accompanying person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }


    /** AccompanyingPerson2 # mailfunc[AccompanyingPerson2の最初のitem_id] */
    function mailfunc78($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        $group   = 1;
        $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key], array(" ", ","), false);
        $str .= trim($wk_body, ",");

        $str.= "\n";
        return $str;
    }



    /** edata54 */
    function mailfunc54($obj, $item_id, $name) {
        $key = "edata".$item_id;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

        $group   = 3;
        $str  = $obj->point_mark.$name.": ";

        // 選択肢
        $select = $obj->arrItemData[$group][$item_id]["select"];

        switch($obj->arrItemData[$group][$item_id]["item_type"]) {
            case Usr_init::INPUT_CHECK:
                if(!isset($select)) break;

                if(strlen($param) > 0){
                    $arrForm = $obj->arrForm;
                    $obj->arrForm = array();
                    $chkeck = explode("|", $param);
                    foreach($chkeck as $n){
                        if($n != "" && array_key_exists($n, $select)){
                            $obj->arrForm[$key.$n] = $n;
                        }
                    }
                }

                // TODO 特殊項目を考慮していない
                foreach($select as $n=>$val) {
                    if(!isset($obj->arrForm[$key.$n])) continue;
                    if($obj->arrForm[$key.$n] == $n) {
                        $str .= "\n ".trim($select[$n]);
                        if($n == 2){
                            $str .= " ".$obj->arrForm['edata55'];
                        }
                    }
                }

                if(strlen($param) > 0){
                    $obj->arrForm = $arrForm;
                }
                break;

            default:
                $str .= Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
                $str .= trim($wk_body, ",");
                break;
        }


        $str.= "\n";
        return $str;
    }

    /** edata55 */
    function mailfunc55($obj, $item_id, $name) { return ''; }



    //------------------------------------------------
    // ▽ CSVカスタマイズ
    //------------------------------------------------

    /** CSVヘッダ-グループ1生成 */
    function entry_csv_entryMakeHeader1($obj, $all_flg=false){
        $group = 1;
        $groupHeader[$group][] = $obj->fix.$GLOBALS["csv"]["entryno"].$obj->fix;
        foreach($obj->arrItemData[$group] as $_key => $_data){
            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $prefix = "";
            $item_id = $_data['item_id'];
            // AccompanyingPerson 1
            if(in_array($item_id, array(73,74,75,76))){     // AccompanyingPerson1の関連項目
                $prefix = "Accompanying person 1 ";
            }
            // AccompanyingPerson 2
            if(in_array($item_id, array(78,79,80,81))){     // AccompanyingPerson2の関連項目
                $prefix = "Accompanying person 2 ";
            }

            $groupHeader[$group][] = $obj->fix.$prefix.$name.$obj->fix;
        }
        return $groupHeader[$group];
    }


}
?>