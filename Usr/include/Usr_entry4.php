<?php

/**
 * reg3form4番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2014.02.17
 * 
 */
class Usr_Entry4 {

    function __construct($obj){}

    function developfunc($obj) {}

    /** グループ1 入力チェック */
    function _checkpay($obj){
        Usr_Check::_checkpay($obj);

        unset($obj->objErr->_err['securityCode']);
    }

}
