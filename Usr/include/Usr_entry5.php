<?php

/**
 * reg3form5番専用カスタマイズフォーム
 *
 * @subpackage Usr
 * @author salon doishun
 * @since 2013.12.24
 * 
 */
class Usr_Entry5 {

    function __construct($obj){}

    function developfunc($obj) {}

    /** グループ1 入力チェック */
    function _check1($obj){
        Usr_Check::_check1($obj);

        // Feeに対するCategoryの対応配列
        $arrFeetoCt = array();
        $arrFeetoCt[0] = "ather_price4";
        $arrFeetoCt[1] = "ather_price5";
        $arrFeetoCt[2] = "ather_price6";
        $arrFeetoCt[3] = "ather_price7";
        $arrFeetoCt[4] = "ather_price8";
        $arrFeetoCt[5] = "ather_price9";

        // 特定のその他決済の選択数
        $countCt = 0;
        foreach($arrFeetoCt as $_key => $otherprice){
            if(!isset($obj->arrParam[$otherprice])) continue;
            if( empty($obj->arrParam[$otherprice])) continue;

            $countCt++;
        }

        if($countCt > 0){
            if($countCt > 1){
                $obj->objErr->addErr("Select one Additional Paper fee.", "ather_price4");

            // 同じ団体エラー
            }else{
                // NomMember以外を選択
                $amount = $obj->arrParam['amount'];
                if(isset($arrFeetoCt[$amount])){
                    $otherKey = $arrFeetoCt[$amount];
                    if(!isset($obj->arrParam[$otherKey]) || !$obj->arrParam[$otherKey] > 0){
                        $obj->objErr->addErr("Selection of fee category is incorrect.", $otherKey);
                    }
                }
            }
        }
    }


    /** グループ3 入力チェック */
    function _check3($obj){
        Usr_Check::_check3($obj);

        // yesを選択したらテキスト必須
        if(isset($obj->arrParam['edata54']) && $obj->arrParam['edata54'] == 1){
            if(isset($obj->arrParam['edata55']) && strlen($obj->arrParam['edata55']) == 0){
                $obj->objErr->addErr("Enter ".Usr_init::getItemInfo($obj, 55).".", "edata55");
            }
        }
        if(isset($obj->arrParam['edata56']) && $obj->arrParam['edata56'] == 1){
            if(isset($obj->arrParam['edata67']) && strlen($obj->arrParam['edata67']) == 0){
                $obj->objErr->addErr("Enter ".Usr_init::getItemInfo($obj, 67).".", "edata67");
            }
        }

        // 1ページ目の入力情報
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
        if(in_array($arrForm1["amount"], array(0,1,2,4))){
            if(isset($obj->arrParam['edata54']) && $obj->arrParam['edata54'] != 1){
                $obj->objErr->addErr("Select yes : ".Usr_init::getItemInfo($obj, 54).".", "edata54");
            }
        }
    }


    // ------------------------------------------------------
    // ▽メールカスタマイズ
    // ------------------------------------------------------

    /** edata73 */
    function mailfunc73($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str  = "\n";
        $str .= "\n";
        $str .= "[Accompanying Person]\n";
        $str .= "\n";
        $str .= "Accompanying Person 1\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }


    /** edata77 */
    function mailfunc77($obj, $key, $name, $i=null) {
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";

        $str  = "";
        $str .= "\n";
        $str .= "Accompanying Person 2\n";
        $str .= "\n";
        $str .= $obj->point_mark.$name.": ";

        switch($obj->itemData[$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->itemData[$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->itemData[$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->itemData[$key]["select"])) break;
                
                foreach($obj->itemData[$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n".$obj->itemData[$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }
        $str.= "\n";
        return $str;
    }


    function mailfunc55($obj, $key, $name, $i=null) { return ""; }
    function mailfunc67($obj, $key, $name, $i=null) { return ""; }


    function mailfunc54($obj, $key, $name, $i=null) {
        $group = 3;
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";

        // ラジオ、セレクト
        if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
            $str .= trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]);
            if($obj->arrForm[$wk_key] == 1){
                $str .= $obj->arrForm["edata55"];
            }
        }

        $str.= "\n";
        return $str;
    }


    function mailfunc56($obj, $key, $name, $i=null) {
        $group = 3;
        $wk_key = "edata".$key.$i;
        
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        $str = $obj->point_mark.$name.": ";

        // ラジオ、セレクト
        if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
            $str .= trim($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]]);
            if($obj->arrForm[$wk_key] == 1){
                $str .= $obj->arrForm["edata67"];
            }
        }

        $str.= "\n";
        return $str;
    }
    // ------------------------------------------------------
    // △メールカスタマイズ
    // ------------------------------------------------------


    // ------------------------------------------------------
    // ▽CSVカスタマイズ
    // ------------------------------------------------------

    /** CSVヘッダ-グループ3生成 */
    function entry_csv_entryMakeHeader3($obj, $all_flg=false){
        $group = 3;
        $groupHeader = array();
        foreach($obj->arrItemData[$group] as $_key => $_data){
            if(in_array($_data['item_id'], array(55, 67))) continue;

            // 表示する設定の場合出力
            if($_data["item_view"] == "1") continue;

            // 画面に表示する項目の名称
            $name = strip_tags($_data["item_name"]);

            $groupHeader[$group][] = "\"".$name."\"";
        }
        return $groupHeader[$group];
    }

    function csvfunc55($obj, $group, $pa_param, $pn_num){ return ""; }
    function csvfunc67($obj, $group, $pa_param, $pn_num){ return ""; }

    function csvfunc54($obj, $group, $pa_param, $pn_num){
        if($pa_param["edata54"] == "") return "";

        $str = str_replace(array("\n\r","\r","\n"), "", $obj->arrItemData[$group][$pn_num]["select"][$pa_param["edata54"]]);
        if($pa_param["edata54"] == 1){
            $str .= $pa_param["edata55"];
        }
        return $str;
    }

    function csvfunc56($obj, $group, $pa_param, $pn_num){
        if($pa_param["edata56"] == "") return "";

        $str = str_replace(array("\n\r","\r","\n"), "", $obj->arrItemData[$group][$pn_num]["select"][$pa_param["edata56"]]);
        if($pa_param["edata56"] == 1){
            $str .= $pa_param["edata67"];
        }
        return $str;
    }


}
