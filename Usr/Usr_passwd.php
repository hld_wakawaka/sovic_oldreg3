<?php

include('../application/cnf/include.php');
include('./form/function.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once(MODULE_DIR.'custom/Entry.class.php');
include_once(MODULE_DIR.'mail_class.php');

/**
 * パスワード再発行
 * 
 * @author hld.doi
 *
 */
class Usr_PasswdReset extends ProcessBase {

    var $expandpreffix = "Usr_PasswdReset_";

    /**
     * コンストラクタ
     */
    function Usr_PasswdReset(){
        parent::ProcessBase();

        //初期化
        $this->arrErr    = array();
        $this->admin_flg = "";   // 管理者モードフラグ
        $this->onload    = "";

        //インスタンス生成
        $this->o_form    = new Form();
        $this->o_entry   = new Entry();
        $this->objErr    = New Validate;
        $this->db        = new DbGeneral;
        $this->o_itemini = new item_ini();
        $this->arrForm   = GeneralFnc::convertParam($this->init(), $_POST);

        // フォームIDが不正な場合
        if(!$this->o_form->formData){
            Error::showErrorPage("フォーム基本情報の取得に失敗しました。");
        }

        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }

        // パスワード再発行を許可していない場合
        $this->form_id   = $this->o_form->formData["form_id"];
        Config::loadSection($this, Config::SECTION_MODE);
        if(is_null($this->use_password_reset) || !$this->use_password_reset){
            Error::showErrorPage("フォーム基本情報の取得に失敗しました。");
        }

        // 表示テンプレート
        $this->_processTemplate = "Usr/passwd/Usr_PasswdReset.html";

        // jQuery読み込みフラグ
        $this->useJquery = false;

        // 初期表示のメッセージ
        $this->msg = "登録Noとメールアドレスを入力してください。";

        // 外部クラスを読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($this->o_form->formData["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
        }

        // 共通で実行するコンストラクタ
        $method = $this->expandpreffix.'construct';
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            $this->exClass->$method($this);
        }
    }


    /**
     * メイン処理
     */
    function main(){
        if(!isset($this->arrForm["mode"])) $this->arrForm["mode"] = "default";

        // 処理アクション
        $actionName = $this->expandpreffix.$this->arrForm["mode"]."Action";
        if(is_object($this->exClass) && method_exists($this->exClass, $actionName)){
            $this->exClass->$actionName($this);
        }else if(method_exists($this, $this->arrForm["mode"]."Action")){
            $actionName = $this->arrForm["mode"]."Action";
            $this->$actionName();
        }else{
            $this->defaultAction();
        }


        $this->assign("arrErr",  $this->arrErr);
        $this->assign("form_id", $GLOBALS["form"]->formData["form_id"]);

        // 親クラスをする前の前処理
        $method = $this->expandpreffix.'premain';
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            $this->exClass->$method($this);
        }

        $this->assign("msg", $this->msg);

        // 親クラスに処理を任せる
        parent::main();
    }


    /**
     * モード指定なしのページ初期表示
     * @return void
     * */
    function defaultAction(){}


    /**
     * パスワード再発行処理
     * @return void
     * */
    function doAction(){
        //---------------------
        // 入力エラーチェック
        //---------------------
        $objErr    = New Validate();
        $objErr->check($this->init(), $this->arrForm);
        $this->arrErr = $objErr->_err;

        if(count($this->arrErr) > 0) return;

        // 登録Noとメールアドレスの照合
        if(!$this->auth($GLOBALS["form"]->formData["form_id"], $this->arrForm)){
            // 照合NG
            $this->arrErr['auth'] = $GLOBALS["msg"]["passwd_err_not_auth"];
            return;
        }

        // 照合ok # メールアドレスへパスワードを送付する
        $this->sendMail($GLOBALS["form"]->formData["form_id"], $this->arrForm);
    }


    /**
     * 入力値の設定
     * 
     * @param  void
     * @return array $key 入力値の設定配列
     */
    function init(){
        $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this);
        }

        // 0フォームの名前,  1項目名, 2長さ配列, 3チェック配列, 4変換, 5DB登録
        $key   = array();
        $key[] = array("mode"       , '処理モード'                           ,  array(), array("TOKUSYU"),                    "", 0);
        $key[] = array("user_id"    , $GLOBALS["msg"]["passwd_user_id"]    ,  array(), array("TOKUSYU", "NULL"),            "", 0);
        $key[] = array("mailaddress", $GLOBALS["msg"]["passwd_mailaddress"],  array(), array("TOKUSYU", "NULL", 'MAIL'), "nKV", 0);
        return $key;
    }


    /**
     * 登録Noとメールアドレスの妥当性チェックをして結果を返す
     * 
     * @param  integer $form_id チェック対象のフォーム
     * @param  array   $arrForm チェックパラメータ
     * @return boolean true:存在する組
     */
    function auth($form_id, $arrForm){
            $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this, $form_id, $arrForm);
        }

        $db = new DbGeneral(true);
        $arrVal  = array();
        $arrVal[] = $form_id;
        $arrVal[] = $arrForm['user_id'];
        $arrVal[] = $arrForm['mailaddress'];

        return $db->exists("entory_r", "form_id = ? and e_user_id = ? and edata25 = ? and del_flg = 0 and invalid_flg = 0", $arrVal);
    }


    /**
     * 登録Noに対応するメールアドレスへパスワードを通知する
     *   登録Noとメールアドレスの妥当性が確認されていることが前提
     *
     * @param  integer $form_id チェック対象のフォーム
     * @param  array   $arrForm チェックパラメータ
     * @return boolean 
     */
    function sendMail($form_id, $arrForm){
        $method = $this->expandpreffix.__FUNCTION__;
        if(is_object($this->exClass) && method_exists($this->exClass, $method)){
            return $this->exClass->$method($this, $form_id, $arrForm);
        }

        $db = new DbGeneral(true);
        $arrVal  = array();
        $arrVal[] = $form_id;
        $arrVal[] = $arrForm['user_id'];
        $arrVal[] = $arrForm['mailaddress'];

        // パスワード取得
        $arrData = $db->getRow("eid, form_id, e_user_passwd", "entory_r", "form_id = ? and e_user_id = ? and edata25 = ? and del_flg = 0 and invalid_flg = 0", $arrVal);
        $eid     = $arrData["eid"];
        $passwd  = $arrData["e_user_passwd"];

        //-------------------------------
        // パスワード再発行通知メールの送信
        //-------------------------------
        $objMail = new Mail();
        // メールタイトル
        $subject = ($this->o_form->formData["lang"] == LANG_JPN) 
                 ? "パスワード再発行通知メール"   // jp
                 : "パスワード再発行通知メール";  // en

        // メール本文
        $url = URL_ROOT."Usr/Usr_login.php?form_id=".$form_id;
        $body  = "";
        $body .= "パスワード再発行を受け付けました。\n";
        $body .= "\n";
        $body .= "パスワード：{$passwd}\n";
        $body .= "\n";
        $body .= "下記URLよりログイン後、応募情報の更新または任意のパスワードに変更することができます。\n";
        $body .= "{$url}\n";
        $body .= "\n";
        $body .= "--------------------------------------------------------------------\n";
        $body .= $this->o_form->formData["contact"]."\n";
        $body .= "--------------------------------------------------------------------\n";

        // 再発行があったことをログに残す
        log_class::printLog("パスワード再発行受付# form_id:{$form_id} eid:{$eid}");

        // メール送信処理
        $objMail->sendMail($arrForm['mailaddress'], $subject, $body, $this->o_form->formData["form_mail"]);

        $this->msg = "パスワード再発行を受け付けました。<br/>メールアドレスへパスワード通知メールを送信しました。";
        $this->_processTemplate = "Usr/passwd/Usr_PasswdReset_complate.html";
    }

}

/**
 * メイン処理開始
 **/

$c = new Usr_PasswdReset();
$c->main();






?>