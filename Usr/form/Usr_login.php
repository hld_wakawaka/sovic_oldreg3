<?php

include('../application/cnf/include.php');

/**
 * 利用者ログイン
 * 
 * @author salon
 *
 */
class login extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function login(){
		
		$form_id	= isset($_REQUEST["form_id"]) ? $_REQUEST["form_id"] : "";
		$eid 		= isset($_REQUEST["eid"]) ? $_REQUEST["eid"] : "";
		$admin_flg	= isset($_REQUEST["admin_flg"]) ? $_REQUEST["admin_flg"] : "";
		
		if($form_id != "" && !is_Numeric($form_id)) {
			Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
		}
		
		if($eid != "" && !is_Numeric($eid)) {
			Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
		}
		
		if($admin_flg != "" && !is_Numeric($admin_flg)) {
			Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
		}

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){
		
		// 表示HTMLの設定
		$this->_processTemplate = "Usr/Usr_login.html";
		
//		$this->assign("isTop", true);

		$this->arrForm = $_REQUEST;
		
		if(!$GLOBALS["form"]->isForm) Error::showErrorPage("ページが存在しません。");
		
		$this->_title = $GLOBALS["form"]->formData["lang"] == "2" ? "Login" : "ログイン";
		
		// 受付期間チェック
		$GLOBALS["form"]->checkValidDate();
		
		if(!isset($this->arrForm["mode"])) $this->arrForm["mode"] = "";
		$arrErr = array();
		
		switch($this->arrForm["mode"]) {
			
			case "login":
			
				$arrErr = $this->_check();
				
				if(count($arrErr) > 0) {
					break;
				}
				
				//ログイン処理
				$err = LoginEntry::doLogin($GLOBALS["form"]->formData["lang"], $_REQUEST["login_id"], $_REQUEST["password"]);
				
				if(!empty($err)) {
					//エラーの場合
					$arrErr[] = $err;
					$GLOBALS["log"]->write_log("euser_login failed", $_REQUEST["login_id"]);
					
				} else {
					
					/*
					if($this->arrForm["save"] == "1") {
						$this->_insert();
					} else {
						$this->_del();
					}*/
					
					$GLOBALS["log"]->write_log("euser_login Success", $GLOBALS["entryData"]["eid"]);
					
					$url = "./form/Usr_entry.php?form_id=".$GLOBALS["form"]->formData["form_id"];
					header("location: ".$url);
					
					exit;
				
				}
				
				break;
			case "logout":
			
				$eid = $GLOBALS["entryData"]["eid"];
			
				LoginEntry::doLogout();
				
				unset($this->arrForm);
				$this->arrForm = array();
				
				/*
				if (isset($_COOKIE[session_name()])) {
					setcookie(session_name(), '', time()-42000, '/');
				}
				*/
				
				//$this->_get();

				$this->_title = $GLOBALS["form"]->formData["lang"] == "2" ? "Logout" : "ログアウト";
				
				$GLOBALS["log"]->write_log("euser_logout", $eid);
				
				$this->assign("msg", $GLOBALS["msg"]["msg_logout"]);

				
				break;
				
			default:
				
				//$this->_get();

				
				$this->assign("msg", $GLOBALS["msg"]["msg_login"]);
				
				break;
		}
		
		$this->assign("arrErr", $arrErr);


		// 親クラスに処理を任せる
		parent::main();
	
	}
	
	function _check() {
		
		$objErr = New Validate;
	
		if(!$objErr->isNull($this->arrForm["login_id"])) {
			$objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $GLOBALS["msg"]["login_id"]));
		}
		if(!$objErr->isNull($this->arrForm["password"])) {
			$objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $GLOBALS["msg"]["login_passwd"]));
		}
		
		return $objErr->_err;
		
	}
	 

	

}

/**
 * メイン処理開始
 **/

$c = new login();
$c->main();







?>