<?php
/*
 * CreateDate: 2012/03/11
 *
 */
include('../../application/cnf/include.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once(MODULE_DIR.'custom/Entry.class.php');
include_once(TGMDK_ORG_DIR.'/crypt.class.php');
include_once(ROOT_DIR.'payment/card/Authorize.Class.php');
include_once(ROOT_DIR.'payment/card/AuthorizeCheck.Class.php');

class Usr_Verify extends ProcessBase {


    function Usr_Verify(){
        parent::ProcessBase();

        $this->form_id = $_REQUEST["form_id"];
        require_once(TGMDK_COPY_DIR."/".$this->form_id."/config.php");

        // パッケージ格納ディレクトリ
        ini_set("include_path", ini_get("include_path").":".TGMDK_ORG_DIR);
        require_once('com/gmo_pg/client/input/TdVerifyInput.php');
        require_once('com/gmo_pg/client/tran/TdVerify.php');

        // エラーハンドラ
        include_once(ROOT_DIR.'payment/card/ErrorMessageHandler.php');

        //インスタンス生成
        $this->o_form    = new Form();
        $this->o_entry   = new Entry();
        $this->objErr    = New Validate;
        $this->db        = new DbGeneral;
        $this->o_itemini = new item_ini();
        $this->o_crypt   = new CryptClass;

        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }

        Usr_initial::setFormData($this);
        Usr_initial::commonAssign($this);

        // フォーム名
        $this->_title = $this->o_form->formData['form_name'];
    }


    function main() {
        // おまじない
        $GLOBALS["session"]->setVar("start", "1");

        $arrErr = array();
        // 言語
        $form_lang = $this->o_form->formData['lang'];

        // 入力パラメータクラスをインスタンス化します
        $input = new TdVerifyInput();/* @var $input TdVerifyInput */

        //各種パラメータを設定します。
        //カード番号入力型・会員ID決済型に共通する値です。
        $input->setMd( $_POST['MD'] );
        $input->setPaRes( $_POST['PaRes'] );

        //API通信クラスをインスタンス化します
        $exe = new TdVerify();/* @var $exec TdVerify */

        //パラメータオブジェクトを引数に、実行メソッドを呼びます。
        //正常に終了した場合、結果オブジェクトが返るはずです。
        $output = $exe->exec( $input );/* @var $output TdVerifyOutput */

        //実行後、その結果を確認します。

        $isCancel = false;

        if( $exe->isExceptionOccured() ){//取引の処理そのものがうまくいかない（通信エラー等）場合、例外が発生します。
            $arrErr = array();
            $arrErr[] = "予期しないエラー";

            $auth_log  = print_r($exe, true);
            $auth_log .= print_r($input, true);
            $auth_log .= print_r($output, true);
            error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");

        }else{
            //例外が発生していない場合、出力パラメータオブジェクトが戻ります。
            if( $output->isErrorOccurred() ){//出力パラメータにエラーコードが含まれていないか、チェックしています。
                $errorHandle = new ErrorHandler($form_lang);
                $errorList = $output->getErrList();

                foreach($errorList as $_key => $errorInfo){
                    // $errorInfo->getErrCode()
                    $arrErr[] = $errorInfo->getErrInfo().':'.$errorHandle->getMessage($errorInfo->getErrInfo());
                    if($errorInfo->getErrInfo() == "E21020002"){
                        $isCancel = true;
                    }
                }
                $auth_log  = print_r($exe, true);
                $auth_log .= print_r($input, true);
                $auth_log .= print_r($output, true);
                $auth_log .= print_r($arrErr, true);
                error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");
            }

            //例外発生せず、エラーの戻りもないので、正常とみなします。
            //このif文を抜けて、結果を表示します。
        }

        //---------------------
        // パラメータ設定
        //---------------------
        $order_id  = $output->getOrderId();      // 決済取引番号
        $c_field_1 = $output->getClientField1(); // 追加フィールド1
        $c_field_2 = $output->getClientField2(); // 追加フィールド2
        $c_field_3 = $output->getClientField3(); // 追加フィールド3

        // access_idよりeidを取得する
        // キャンセルすると何もフィールドが返ってこないためユニークなMDを利用する
        $access_id = $_POST['MD'];
        $eid = Usr_entryDB::restoreEid($this, $access_id);

        //-------------------------------------
        // 与信が通っていればエラーなしとする
        //-------------------------------------
        $objAuth = new Authorize();
        while(true){
            // 正常終了
            if(count($arrErr) == 0) braek;

            // 受信パラメータから決済取引番号の与信状態をチェック
            if($objAuth->isCapture($order_id)){
                $arrErr = array();
//                break;
            }

            // セッションの決済取引番号から与信状態をチェック
            if($objAuth->isCapture($GLOBALS["session"]->getVar("order_id"))){
                $arrErr   = array();
                $order_id = $GLOBALS["session"]->getVar("order_id");
                $eid      = $this->db->get("eid", "payment", "form_id=? and order_id=?", array($this->form_id, $order_id));
                break;
            }

            break;
        }
        // 検査用の決済取引番号を破棄
        $GLOBALS["session"]->unsetVar("order_id");


        // eidよりエントリー情報を復元
        if(strlen($eid) > 0 && $eid > 0){
            $this->eid = $eid;
            Usr_initial::createUploadDir($this);
            Usr_entryDB::getEntry($this);
            unset($this->eid);

            $arrData  = $this->db->getRow("order_id, c_field_1, c_field_2, c_field_3", "payment", "form_id=? and eid=?", array($this->form_id, $eid));
            $order_id  = $arrData["order_id"];  // 決済取引番号
            $c_field_1 = $eid; //$arrData['c_field_1']; // 追加フィールド1
            $c_field_2 = $arrData['c_field_2']; // 追加フィールド2
            $c_field_3 = $arrData['c_field_3']; // 追加フィールド3
        }


        $this->assign('form_id'    , $this->form_id);
        $this->assign('arrErr'     , $arrErr);
        $this->assign('order_id'   , $order_id);     // order_id
        $this->assign('forward'    , $output->getForward());     // 仕向先カード会社
        $this->assign('method'     , $output->getMethod());      // 支払方法
        $this->assign('paytimes'   , $output->getPayTimes());    // 支払回数
        $this->assign('approval_no', $output->getApprovalNo());  // 承認番号
        $this->assign('tran_id'    , $output->getTranId());      // トランザクションID
        $this->assign('tran_date'  , $output->getTranDate());    // 決済日付
        $this->assign('chk_string' , $output->getCheckString()); // チェック文字列
        $this->assign('c_field1'   , $this->cnvClienField($c_field_1)); // 加盟店自由項目1
        $this->assign('c_field2'   , $this->cnvClienField($c_field_2)); // 加盟店自由項目2
        $this->assign('c_field3'   , $this->cnvClienField($c_field_3)); // 加盟店自由項目3

        // エラー及びキャンセル時に対応できるようworkDirをセッションに復元
        $arrFiles = explode(",", $c_field_3);
        $GLOBALS["session"]->setVar("workDir", $arrFiles[0]);

        if(count($arrErr) == 0){
            $msg = "3Dセキュア認証が完了しました。応募登録処理を開始します。";
            $this->assign("onload", "document.CUP_FORM.submit();");
        }else{
            $msg = ($form_lang == LANG_JPN) ? $GLOBALS["3dpayerror_J"] : $GLOBALS["3dpayerror_E"];
        }

        // 3Dセキュア認証ページでキャンセルを行った場合
        if($isCancel){
            $GLOBALS["msg"]["btn_change_payment"] = ($form_lang == LANG_JPN)
                                                  ? "決済情報の再入力を行う"
                                                  : "Re-enter the credit card information";

            // クレジットカード番号(下4桁)は破棄
            $arrFormPay = $GLOBALS["session"]->getVar("form_parampay");
            $arrFormPay["cardNumber"] = NULL;
            $GLOBALS["session"]->setVar("form_parampay", array());
//            $GLOBALS["session"]->setVar("form_parampay", $arrFormPay);

            // アップロードファイル情報を破棄
            $arrForm3 = $GLOBALS["session"]->getVar("form_param3");
            unset($arrForm3['edata51']  );
            unset($arrForm3['hd_file51']);
            unset($arrForm3['n_data51'] );

            unset($arrForm3['edata52']  );
            unset($arrForm3['hd_file52']);
            unset($arrForm3['n_data51'] );

            unset($arrForm3['edata53']  );
            unset($arrForm3['hd_file53']);
            unset($arrForm3['n_data51'] );
            $GLOBALS["session"]->setVar("form_param3", $arrForm3);
            $this->createWorkDir();
        }

        //---------------------------
        // ログメッセージ
        //---------------------------
        $this->logrotate($this->form_id);
        $inputLog  = $input->toString();
        $outputLog = $output->toString();
        $GLOBALS["log"]->write_log('PAY3DBK', $inputLog .'&'. $outputLog);


        $this->assign("msg", $msg);
        $this->_processTemplate = "Usr/form/Usr_pg3dresult.html";
        parent::main();
        exit;
    }

    function logrotate($form_id) {
        // form_id別にログを出力
        // daily用
        $form_id = $form_id;
        $usrLogDir = LOG_DIR."daily/".$form_id."/";
        if(!is_dir($usrLogDir)){
            mkdir($usrLogDir, 0777, true);
            chmod($usrLogDir, 0777);
        }
        $GLOBALS["log"]->filename = $usrLogDir.date('Ymd').".log";
        // DB用
        $usrLogDirDb = LOG_DIR."DBLog/".$form_id."/";
        if(!is_dir($usrLogDirDb)){
            mkdir($usrLogDirDb, 0777, true);
            chmod($usrLogDirDb, 0777);
        }
        $GLOBALS["log"]->filenameDb = $usrLogDirDb."DB_".date('Ymd').".log";
    }

    function cnvClienField($clientField){
        return htmlspecialchars(mb_convert_encoding($clientField, SCRIPT_ENC , 'SJIS') );
    }

    function getPayData(){
        return Usr_entryDB::getPayData($this);
    }

    /**初期表示時、任意項目の値を生成 */
    function _default_arrFormNini($start, $end, $wa_entrydata){
        return Usr_init::_default_arrFormNini($this, $start, $end, $wa_entrydata);
    }

    /**
     * セッションパラメータ取得
     * 画面入力データをセッションから取得する
     */
    function getDispSession(){
        $this->arrForm = array_merge($GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
        $this->arrForm = array_merge($this->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));
        $this->arrForm = array_merge($this->arrForm, (array)$GLOBALS["session"]->getVar("form_parampay"));

        if(isset($this->arrForm["edata31"])) {
            $wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
            $this->arrForm["kikanlist"] = $wa_kikanlist;
        }
    }

    /**所属期間名リストボックス生成 */
    function _makeListBox($ps_val){
        return Usr_init::_makeListBox($this, $ps_val);
    }

    function createWorkDir() {
        // 参加フォームもファイルのアップロードを許可する
        $workDir = $this->uploadDir."NEW/".date("YmdHis");
        //tmpディレクトリ作成（新規投稿用）
        if(!is_dir($workDir)){
            mkdir($workDir, 0777, true);
            chmod($workDir, 0777);
        }
        //セッションにワークディレクトリ名をセット
        $GLOBALS["session"]->setVar("workDir", $workDir);
    }
}


$c = new Usr_Verify();
$c->main();
?>
