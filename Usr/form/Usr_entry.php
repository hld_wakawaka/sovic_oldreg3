<?php

include('../../application/cnf/include.php');
include('./function.php');
include_once(MODULE_DIR.'mail_class.php');
include_once(MODULE_DIR.'Download.class.php');
include_once(MODULE_DIR.'custom/Form.class.php');
include_once(MODULE_DIR.'custom/Entry.class.php');
include_once(MODULE_DIR.'custom/crypt.class.php');

/**
 * 論文エントリーフォーム
 *
 *
 * @subpackage Usr
 * @author salon
 *
 */
class Usr_Entry extends ProcessBase {


    /**
     * コンストラクタ
     */
    function Usr_Entry(){

        parent::ProcessBase();

        //初期化
        $this->block     = "1";
        $this->arrErr    = array();
        $this->admin_flg = "";                 // 管理者モードフラグ
        $this->inc_dir   = "../include/";        // インクルードパス
        $this->onload    = "";
        $this->need_mark = $GLOBALS["msg"]["need_mark"];  // 必須マーク
        $this->yen_mark  = "JPY";  // 円マーク

        //インスタンス生成
        $this->o_form    = new Form();
        $this->logrotate(); // formID別のログ出力
        $this->o_entry   = new Entry();
        $this->objErr    = New Validate;
        $this->db        = new DbGeneral;
        $this->o_itemini = new item_ini();
        $this->o_mail    = new Mail();
        $this->o_crypt   = new CryptClass;

        // 強制的にメンテナンスとするフォーム
        $this->mente_form = array();

        // メールを送信しないフォーム
        $this->sendmail_not = array();

        // 参加フォームで決済項目は利用しないフォーム
        $this->payment_not = array();

        // 参加フォームで決済項目は利用しないフォーム
        $this->sendmail_not = array();

        // タイトルを表示しないフォーム
        $this->not_title_view = array();

        // アップロードを必須とする金額項目
        $this->filecheck = array();

        // フォーム項目を配列とする特殊項目
        $this->formarray = array();

        // jQuery読み込みフラグ
        $this->useJquery = false;

        // 編集完了時に利用 # 編集前の情報を保持するフラグ
        $this->save_editbefore = false;

        // 編集完了メールに利用 # 更新の前後のみ記載する
        $this->editmail_diff = false;

        // 必要クラスをinclude
        $ex = glob(MODULE_DIR.'entry_ex/*.php');
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                include_once($script);
            }
        }

        // アップロードファイルの添付IDと添え字
        Usr_init::_initFile($this);

        // 拡張クラスを読み込み
        $this->exClass = null;
        $isOverride = ProcessBase::isOverrideClass($this->o_form->formData["form_id"], $c);
        if($isOverride && is_object($c)) {
            $this->exClass = $c;
        }
        // 外部クラスを読み込み
        $this->svClass = null;
        $isOverride = ProcessBase::isExternalClass($this->o_form->formData["form_id"], $s);
        if($isOverride && is_object($s)) {
            $this->svClass = $s;
        }
    }


    /**
     * 簡易認証
     *
     * authディレクトリ内に[form]+form_idのファイルが存在すれば簡易認証を行う
     *
     * @access private
     *
     */
    function pf_auth() {
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pf_auth($this);
        }


        $filename = ROOT_DIR."auth/form".$this->o_form->formData["form_id"];

        if(!file_exists($filename)) return;

        $phrase = file_get_contents(trim($filename));

        $auth_flg = $GLOBALS["session"]->getVar("auth");

        $_REQUEST["authpw"] = isset($_REQUEST["authpw"]) ? $_REQUEST["authpw"] : "";

        if($auth_flg == "1") {
            if($phrase == $GLOBALS["session"]->getVar("auth_pas")) return;
        }

        if($_REQUEST["authpw"] != "") {
            $phrase = file_get_contents(trim($filename));

            if($phrase == $_REQUEST["authpw"]) {
                $GLOBALS["session"]->setVar("auth", "1");
                $GLOBALS["session"]->setVar("auth_pas", $_REQUEST["authpw"]);
                $_REQUEST["mode"] = "";
                return;
            } else {
                $this->assign("msg", "認証に失敗しました。（Password Error)");
            }
        }

        $this->assign("form_id", $this->o_form->formData["form_id"]);
        $this->_processTemplate = "Usr/Usr_Basic.html";

        parent::main();
        exit;

    }


    /**
     * セッション切れしていないかチェック
     *
     * ファーストアクセスでセッションstartに、1をたて、
     * ページ遷移毎に、セッションstartに1があるかチェックし、1がない場合は、セッション切れ
     */
    private function pf_checkSession() {
        $mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

        if($mode == "") {
            $GLOBALS["session"]->setVar("start", "1");
            return;

        } else {
            // 3D認証からの戻り
            if(strpos($_SERVER['HTTP_REFERER'], "Usr_verify.php") !== false){
                $GLOBALS["session"]->setVar("start", "1");
                return;
            }

            $start = $GLOBALS["session"]->getVar("start");
            if($start == "1")    return;
        }

        $url  = SSL_URL_ROOT."Usr/form/Usr_entry.php?form_id=".$this->o_form->formData["form_id"];

        Error::showErrorPage("*セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。", $url);
    }


    /**
     * メイン処理
     */
    function mainAction(){
        // アクセスログ
        Usr_function::accessLog($_REQUEST);

        // フォームID
        $this->form_id = $this->o_form->formData["form_id"];
        if($this->form_id == ""){
            Error::showErrorPage("ページが存在しません。<br />Page not found.");
        }

    	// カスタマイズテンプレート設定の読み込み @ autoload
        Config::loadSection($this);

        //----------------------
        //アクション取得
        //----------------------
        $ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";            // モード
        if($ws_action == "") $ws_action = "default";
        $this->wk_block = isset($_REQUEST["block"]) ? $_REQUEST["block"] : "1";        // ブロック

        $this->setMngAdmin();      // Mng管理者フラグの判定
        $this->createUploadDir();  // ファイルアップロードディレクトリ
        $this->setFormData();      // フォームデータの取得・表示テンプレートの設定
        $this->setFormIni();       // フォーム項目の取得
        $this->sortFormIni($this); // フォーム項目の表示順序変更
        $this->commonAssign();     // 共通アサイン
        $this->setTemplate();
        $this->setMoneyPaydata();
        $this->pf_auth();          // 簡易認証
        $this->showMenteform();    // メンテナンス中
        $this->pf_checkSession();  // セッション切れチェック
        $this->AuthAction();       // 受付期間チェック


        //---------------------------------
        //アクション別処理
        //---------------------------------
        $actionName = $ws_action."Action";
        if(method_exists($this, $actionName)){
            $this->$actionName();

            // 編集時はエントリーの決済情報を取得
            if($this->eid != "") {
                $this->getPayData();
            }

        }else{
            $this->defaultAction();
        }

        $this->workDir = $GLOBALS["session"]->getVar("workDir");

        $this->assign("onload",     $this->onload);
        $this->assign("workDir",    $this->workDir);
        $this->assign("block",      $this->block);
        $this->assign("arrErr",     $this->arrErr);
        $this->assign("admin_flg",  $this->admin_flg);
        $this->assign("arrItemData",$this->arrItemData);
        $this->assign("need_mark",  $this->need_mark);
        $this->assign("yen_mark",   $this->yen_mark);
        // titleを表示しないform
        if(in_array($this->formdata["form_id"], $this->not_title_view)) $this->assign("not_title", "1");

        // エラーメッセージに含まれるHTMLタグを除去
        $this->objErr->array_stripTags($this->arrErr);

        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'premain')){
            $this->exClass->premain($this);
        }

        // 親クラスに処理を任せる
        parent::main();

        // 開発用デバッグ関数
        $this->developfunc();
    }

    // ExtendからProcessBaseのmainを呼ぶため設置
    function basemain() {
        parent::main();

        // 開発用デバッグ関数
        $this->developfunc();
        exit;
    }

    /** 初期表示 */
    function defaultAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->defaultAction($this);
        }else{
            return Usr_pageAction::defaultAction($this);
        }
    }


    /** 登録内容の表示 */
    function displayAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->displayAction($this);
        }else{
            return Usr_pageAction::displayAction($this);
        }
    }


    /** ページ遷移ベース */
    function pageAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pageAction($this);
        }else{
            return Usr_pageAction::pageAction($this);
        }
    }


    /** 1ページ目 */
    function pageAction1() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pageAction1($this);
        }else{
            return Usr_pageAction::pageAction1($this);
        }
    }


    /** 2ページ目 */
    function pageAction2() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pageAction2($this);
        }else{
            return Usr_pageAction::pageAction2($this);
        }
    }


    /** 決済ページ */
    function pageActionpay() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pageActionpay($this);
        }else{
            return Usr_pageAction::pageActionpay($this);
        }
    }


    /** プライバシーポリシー */
    function agreeAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->agreeAction($this);
        }else{
            return Usr_pageAction::agreeAction($this);
        }
    }


    /** 3ページ目 */
    function pageAction3() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pageAction3($this);
        }else{
            return Usr_pageAction::pageAction3($this);
        }
    }


    /** 確認ページ */
    function confirmAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->confirmAction($this);
        }else{
            return Usr_pageAction::confirmAction($this);
        }
    }


    /** 戻るボタン */
    function backAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->backAction($this);
        }else{
            return Usr_pageAction::backAction($this);
        }
    }


    /** ダウンロードボタン */
    function downloadAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->downloadAction($this);
        }else{
            return Usr_pageAction::downloadAction($this);
        }
    }


    /**決済エラーからの戻り */
    function authErrAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->authErrAction($this);
        }else{
            return Usr_pageAction::authErrAction($this);
        }
    }


    /* 決済エラーの場合、確認画面に戻る */
    function paymentErrAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->paymentErrAction($this);
        }else{
            return Usr_pageAction::paymentErrAction($this);
        }
    }


    /** 完了ページ */
    function completeAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->completeAction($this);
        }else{
            return Usr_pageAction::completeAction($this);
        }
    }


    /** 本番ディレクトリにファイル移動を行う */
    function TempUploadAction($mode, $ps_eid){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->TempUploadAction($this, $mode, $ps_eid);
        }else{
            return Usr_pageAction::TempUploadAction($this, $mode, $ps_eid);
        }
    }


    // 本番ディレクトリへ移動
    function MoveFileAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->MoveFileAction($this);
        }else{
            return Usr_pageAction::MoveFileAction($this);
        }
    }


    /* 支払方法指定なし */
    function payAction(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->payAction($this);
        }else{
            return Usr_pageAction::payAction($this);
        }
    }


    /* 銀行振込の新規登録処理 */
    function payAction1(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->payAction1($this);
        }else{
            return Usr_pageAction::payAction1($this);
        }
    }


    /* クレジットカードの新規登録処理 */
    function payAction2(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->payAction2($this);
        }else{
            return Usr_pageAction::payAction2($this);
        }
    }


    // カード決済を実行する
    function doCardPayAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->doCardPayAction($this);
        }else{
            return Usr_pageAction::doCardPayAction($this);
        }
    }


    // カード決済を実行する(3Dセキュア)
    function doCardPay3dAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->doCardPay3dAction($this);
        }else{
            return Usr_pageAction::doCardPay3dAction($this);
        }
    }


    // 3Dセキュア決済ページ
    function pageActionpay3d() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->pageActionpay3d($this);
        }else{
            return Usr_pageAction::pageActionpay3d($this);
        }
    }


    /** メンテナンスを表示するフォームの場合は、メンテナンス画面を表示する */
    function showMenteform() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->showMenteform($this);
        }else{
            return Usr_initial::showMenteform($this);
        }
    }

    /** アップロードディレクトリがない場合は作成 */
    function createUploadDir() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->createUploadDir($this);
        }else{
            return Usr_initial::createUploadDir($this);
        }
    }

    /** Mng管理者フラグの判定 */
    function setMngAdmin()
    {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->setMngAdmin($this);
        }else{
            return Usr_initial::setMngAdmin($this);
        }
    }

    /** 管理者モードか一般モードか判断し、各処理を行う */
    function AuthAction() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->AuthAction($this);
        }else{
            return Usr_initial::AuthAction($this);
        }
    }

    /** フォーム基本情報の取得・アサイン */
    function setFormData() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->setFormData($this);
        }else{
            return Usr_initial::setFormData($this);
        }
    }

    /** フォーム項目の取得及び、整形 */
    function setFormIni() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->setFormIni($this);
        }else{
            return Usr_initial::setFormIni($this);
        }
    }

    /**
     * フォーム項目の並び順変更
     * 対象変数 : $this->arrItemData
     */
    function sortFormIni($obj) {
        // 外部クラスを優先
        if(is_object($this->svClass) && method_exists($this->svClass, __FUNCTION__)){
            return $this->svClass->sortFormIni($this);
        }
        // 外部クラスの呼び出し
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->sortFormIni($this);
        }
    }

    /** 共通アサインクラス */
    function commonAssign() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->commonAssign($this);
        }else{
            return Usr_initial::commonAssign($this);
        }
    }

    /** 内部、外部テンプレートの読み込み */
    function setTemplate() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->setTemplate($this);
        }else{
            return Usr_initial::setTemplate($this);
        }
    }

    /** 参加フォームのみ 金額、支払情報を生成 */
    function setMoneyPaydata() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->setMoneyPaydata($this);
        }else{
            return Usr_initial::setMoneyPaydata($this);
        }
    }

    /** ロック開始 */
    function lockStart() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->lockStart($this);
        }else{
            return Usr_lock::lockStart($this);
        }
    }

    /** ロック解除 */
    function lockEnd() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->lockEnd($this);
        }else{
            return Usr_lock::lockEnd($this);
        }
    }


    /** エントリー番号採番 */
    function getEntryNumber() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->getEntryNumber($this);
        }else{
            return Usr_entryDB::getEntryNumber($this);
        }
    }

    /** 取引番号採番 #クレジットカード用 */
    function getOrderNumber() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->getOrderNumber($this);
        }else{
            return Usr_entryDB::getOrderNumber($this);
        }
    }

    /** エントリーデータ取得 */
    function getEntry() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->getEntry($this);
        }else{
            return Usr_entryDB::getEntry($this);
        }
    }


    /** エントリーの決済データ取得 */
    function getPayData() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->getPayData($this);
        }else{
            return Usr_entryDB::getPayData($this);
        }
    }


    /** 新規エントリー処理 */
    function makeDbParam(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeDbParam($this);
        }else{
            return Usr_entryDB::makeDbParam($this);
        }
    }

    /** 新規エントリー処理 */
    function insertEntry(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->insertEntry($this);
        }else{
            return Usr_entryDB::insertEntry($this);
        }
    }

    /** エントリー情報更新処理 */
    function updateEntry(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->updateEntry($this);
        }else{
            return Usr_entryDB::updateEntry($this);
        }
    }

    /** 支払詳細項目登録 */
    function _insPaymentDetail($pn_eid){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_insPaymentDetail($this, $pn_eid);
        }else{
            return Usr_entryDB::_insPaymentDetail($this, $pn_eid);
        }
    }

    /** 決済メッセージ取得 */
    function _getPaymentMsg($ps_code){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_getPaymentMsg($this, $ps_code);
        }else{
            return Usr_entryDB::_getPaymentMsg($this, $ps_code);
        }
    }

    /** エントリーの無効処理 */
    function entryInvalid(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->entryInvalid($this);
        }else{
            return Usr_entryDB::entryInvalid($this);
        }
    }


    /** 入力チェック ブロック別に振り分ける */
    function _check(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_check($this);
        }else{
            return Usr_Check::_check($this);
        }
    }

    /** 応募期間チェック */
    function _chkPeriod() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_chkPeriod($this);
        }else{
            return Usr_Check::_chkPeriod($this);
        }
    }

    /** 一次応募期間中チェック */
    function _chkTerm(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_chkTerm($this);
        }else{
            return Usr_Check::_chkTerm($this);
        }
    }

    /** 1ページ目チェック */
    function _check1(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_check1($this);
        }else{
            return Usr_Check::_check1($this);
        }
    }

    /** 2ページ目チェック */
    function _check2(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_check2($this);
        }else{
            return Usr_Check::_check2($this);
        }
    }

    /** 3ページ目チェック */
    function _check3(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_check3($this);
        }else{
            return Usr_Check::_check3($this);
        }
    }

    /** 決済ページチェック */
    function _checkpay(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_checkpay($this);
        }else{
            return Usr_Check::_checkpay($this);
        }
    }

    /** 任意項目のチェック */
    function _checkNini($group){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_checkNini($this, $group);
        }else{
            return Usr_Check::_checkNini($this, $group);
        }
    }

    /** 共著者チェック */
    function _checkAuthor() {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_checkAuthor($this);
        }else{
            return Usr_Check::_checkAuthor($this);
        }
    }

    /** 共著者 任意項目のチェック*/
    function _checkChosyaNini($i){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_checkChosyaNini($this, $i);
        }else{
            return Usr_Check::_checkChosyaNini($this, $i);
        }
    }

    /** メールアドレス、氏名重複チェック */
    function checkRepeat($pa_formparam){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->checkRepeat($this, $pa_formparam);
        }else{
            return Usr_Check::checkRepeat($this, $pa_formparam);
        }
    }

    /** アップロードファイルチェック */
    function _checkfile($pa_formparam, $group_id, $item_id, $mode=""){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_checkfile($this, $pa_formparam, $group_id, $item_id, $mode);
        }else{
            return Usr_Check::_checkfile($this, $pa_formparam, $group_id, $item_id, $mode);
        }
    }

    /** 外部ファイルをinclude_onceする */
    function _chkIncFiles($ps_dir, $pn_form_id, $ps_file_name, $ps_extension){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_chkIncFiles($this, $ps_dir, $pn_form_id, $ps_file_name, $ps_extension);
        }else{
            return Usr_Check::_chkIncFiles($this, $ps_dir, $pn_form_id, $ps_file_name, $ps_extension);
        }
    }


    /** 項目情報初期化 */
    function _init($ps_block){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_init($this, $ps_block);
        }else{
            return Usr_init::_init($this, $ps_block);
        }
    }



    /** 1ページ目 項目情報初期化 */
    function _init1(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_init1($this);
        }else{
            return Usr_init::_init1($this);
        }
    }



    /** 2ページ目 項目情報初期化 */
    function _init2(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_init2($this);
        }else{
            return Usr_init::_init2($this);
        }
    }


    /** 3ページ目 項目情報初期化 */
    function _init3(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_init3($this);
        }else{
            return Usr_init::_init3($this);
        }
    }


    /** 決済ページ目 項目情報初期化 */
    function _initpay(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_initpay($this);
        }else{
            return Usr_init::_initpay($this);
        }
    }


    /** パラメータ初期設定 任意項目 */
    function _initNini(&$key, $item_id){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_initNini($this, $key, $item_id);
        }else{
            return Usr_init::_initNini($this, $key, $item_id);
        }
    }

    /** パラメータ初期設定 共著者情報の任意項目 */
    function _initChosyaNini(&$key, $index, $i, $k_num){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_initChosyaNini($this, $key, $index, $i, $k_num);
        }else{
            return Usr_init::_initChosyaNini($this, $key, $index, $i, $k_num);
        }
    }


    /**所属期間名リストボックス生成 */
    function _makeListBox($ps_val){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_makeListBox($this, $ps_val);
        }else{
            return Usr_init::_makeListBox($this, $ps_val);
        }
    }

    /** 入力チェックの項目設定 */
    function _setCheckMethod($ps_chk, $pn_len, $pn_type=""){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_setCheckMethod($this, $ps_chk, $pn_len, $pn_type);
        }else{
            return Usr_init::_setCheckMethod($this, $ps_chk, $pn_len, $pn_type);
        }
    }

    /** フォーム番号35　所属機関パラメータ生成 */
    function _make35param($pn_block){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_make35param($this, $pn_block);
        }else{
            return Usr_init::_make35param($this, $pn_block);
        }
    }

    /**初期表示時、任意項目の値を生成 */
    function _default_arrFormNini($start, $end, $wa_entrydata){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_default_arrFormNini($this, $start, $end, $wa_entrydata);
        }else{
            return Usr_init::_default_arrFormNini($this, $start, $end, $wa_entrydata);
        }
    }

    /** 初期表示時、任意項目の値を生成（共著者情報部分） */
    function _default_arrFormChosyaNini($start, $end, $data, $a_key){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_default_arrFormChosyaNini($this, $start, $end, $data, $a_key);
        }else{
            return Usr_init::_default_arrFormChosyaNini($this, $start, $end, $data, $a_key);
        }
    }


    /** 登録完了メールの送信 */
    function sendRegistMail($user_id, $passwd) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->sendRegistMail($this, $user_id, $passwd);
        }else{
            return Usr_mail::sendRegistMail($this, $user_id, $passwd);
        }
    }

    /** 編集完了メールの送信 */
    function sendEditMail($user_id, $passwd) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->sendEditMail($this, $user_id, $passwd);
        }else{
            return Usr_mail::sendEditMail($this, $user_id, $passwd);
        }
    }

    /* 完了メールの送信 */
    function sendCompleteMail($user_id, $passwd, $exec_type=1) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->sendCompleteMail($this, $user_id, $passwd, $exec_type);
        }else{
            return Usr_mail::sendCompleteMail($this, $user_id, $passwd, $exec_type);
        }
    }

    /** 完了メール作成 */
    function makeMailBody($user_id="", $passwd ="", $exec_type = ""){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeMailBody($this, $user_id, $passwd, $exec_type);
        }else{
            return Usr_mail::makeMailBody($this, $user_id, $passwd, $exec_type);
        }
    }

    /** メールの上部コメントを作成する */
    function makeMailBody_header($user_id="", $passwd ="", $exec_type = "") {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeMailBody_header($this, $user_id, $passwd, $exec_type);
        }else{
            return Usr_mail::makeMailBody_header($this, $user_id, $passwd, $exec_type);
        }
    }

    /** メールの上部コメントを作成する # 銀行振り込みまたはうレジットカードで表示を切り替える */
    function makeMailBody_header_replace_tag($body_header) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeMailBody_header_replace_tag($this, $body_header);
        }else{
            return Usr_mail::makeMailBody_header_replace_tag($this, $body_header);
        }
    }

    /** メール本文生成 入力情報 */
    function makeFormBody($exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeFormBody($this, $exec_type);
        }else{
            return Usr_mail::makeFormBody($this, $exec_type);
        }
    }

    /** メール本文生成 グループ1 */
    function makeBodyGroup1(&$arrbody, $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeBodyGroup1($this, $arrbody, $exec_type);
        }else{
            return Usr_mail::makeBodyGroup1($this, $arrbody, $exec_type);
        }
    }

    /** メール本文生成 グループ2 */
    function makeBodyGroup2(&$arrbody, $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeBodyGroup2($this, $arrbody, $exec_type);
        }else{
            return Usr_mail::makeBodyGroup2($this, $arrbody, $exec_type);
        }
    }

    /** メール本文生成 グループ3 */
    function makeBodyGroup3(&$arrbody, $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeBodyGroup3($this, $arrbody, $exec_type);
        }else{
            return Usr_mail::makeBodyGroup3($this, $arrbody, $exec_type);
        }
    }

    /** メール本文生成 決済情報 */
    function makePaymentBody($exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makePaymentBody($this, $exec_type);
        }else{
            return Usr_mail::makePaymentBody($this, $exec_type);
        }
    }


    /** メール本文生成 お支払情報 */
    function makePaymentMethodBody($total){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makePaymentMethodBody($this, $total);
        }else{
            return Usr_mail::makePaymentMethodBody($this, $total);
        }
    }


    /** 金額のメール本文 */
    function makePaymentBody1($exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makePaymentBody1($this, $exec_type);
        }else{
            return Usr_mail::makePaymentBody1($this, $exec_type);
        }
    }


    /** その他決済のメール本文 */
    function makePaymentBody2($exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makePaymentBody2($this, $exec_type);
        }else{
            return Usr_mail::makePaymentBody2($this, $exec_type);
        }
    }


    /** メール本文生成 お支払情報 カード */
    function makePaymentMethodBody1($total){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makePaymentMethodBody1($this, $total);
        }else{
            return Usr_mail::makePaymentMethodBody1($this, $total);
        }
    }


    /** メール本文生成 お支払情報 銀行振込 */
    function makePaymentMethodBody2($total){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makePaymentMethodBody2($this, $total);
        }else{
            return Usr_mail::makePaymentMethodBody2($this, $total);
        }
    }


    /** メール本文生成 編集パス */
    function makeEditBody($user_id="", $passwd="", $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeEditBody($this, $user_id, $passwd, $exec_type);
        }else{
            return Usr_mail::makeEditBody($this, $user_id, $passwd, $exec_type);
        }
    }

    function mailfunc8($name)  {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc8($this,  $name);
        }else{
            return Usr_mail::mailfunc8($this,  $name);
        }
    }

    function mailfunc16($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc16($this, $name);
        }else{
            return Usr_mail::mailfunc16($this, $name);
        }
    }

    function mailfunc18($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc18($this, $name);
        }else{
            return Usr_mail::mailfunc18($this, $name);
        }
    }

    function mailfunc29($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc29($this, $name);
        }else{
            return Usr_mail::mailfunc29($this, $name);
        }
    }

    function mailfunc31($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc31($this, $name);
        }else{
            return Usr_mail::mailfunc31($this, $name);
        }
    }

    function mailfunc32($name, $i) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc32($this, $name, $i);
        }else{
            return Usr_mail::mailfunc32($this, $name, $i);
        }
    }

    function mailfunc41($name, $i) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc41($this, $name, $i);
        }else{
            return Usr_mail::mailfunc41($this, $name, $i);
        }
    }

    function mailfunc48($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc48($this, $name);
        }else{
            return Usr_mail::mailfunc48($this, $name);
        }
    }

    function mailfunc49($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc49($this, $name);
        }else{
            return Usr_mail::mailfunc49($this, $name);
        }
    }

    function mailfunc50($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc50($this, $name);
        }else{
            return Usr_mail::mailfunc50($this, $name);
        }
    }

    function mailfunc51($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc51($this, $name);
        }else{
            return Usr_mail::mailfunc51($this, $name);
        }
    }

    function mailfunc52($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc52($this, $name);
        }else{
            return Usr_mail::mailfunc52($this, $name);
        }
    }

    function mailfunc53($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc53($this, $name);
        }else{
            return Usr_mail::mailfunc53($this, $name);
        }
    }

    function mailfunc57($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc57($this, $name);
        }else{
            return Usr_mail::mailfunc57($this, $name);
        }
    }

    function mailfunc61($name, $i) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc61($this, $name, $i);
        }else{
            return Usr_mail::mailfunc61($this, $name, $i);
        }
    }

    function mailfunc114($name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfunc114($this, $name);
        }else{
            return Usr_mail::mailfunc114($this, $name);
        }
    }

    function mailfuncFile($key, $name) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfuncFile($this, $key, $name);
        }else{
            return Usr_mail::mailfuncFile($this, $key, $name);
        }
    }

    function mailfuncNini($group, $key, $name, $i=null) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfuncNini($this, $group, $key, $name, $i);
        }else{
            return Usr_mail::mailfuncNini($this, $group, $key, $name, $i);
        }
    }

    function mailfuncNiniBody($group, $key, $i=null) {
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfuncNiniBody($this, $group, $key, $i);
        }else{
            return Usr_mail::mailfuncNiniBody($this, $group, $key, $i);
        }
    }

    /** メール本文生成 グループ1 # 差分モード */
    function makeDiffBodyGroup1(&$arrbody, $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeDiffBodyGroup1($this, $arrbody, $exec_type);
        }else{
            return Usr_mail::makeDiffBodyGroup1($this, $arrbody, $exec_type);
        }
    }

    /** メール本文生成 グループ2 # 差分モード */
    function makeDiffBodyGroup2(&$arrbody, $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeDiffBodyGroup2($this, $arrbody, $exec_type);
        }else{
            return Usr_mail::makeDiffBodyGroup2($this, $arrbody, $exec_type);
        }
    }

    /** メール本文生成 グループ3 # 差分モード */
    function makeDiffBodyGroup3(&$arrbody, $exec_type){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->makeDiffBodyGroup3($this, $arrbody, $exec_type);
        }else{
            return Usr_mail::makeDiffBodyGroup3($this, $arrbody, $exec_type);
        }
    }

    /** メール本文生成 グループ3 # 差分モード */
    function mailfuncDiffNiniBody($group, $item_id, $diff){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfuncDiffNiniBody($this, $group, $item_id, $diff);
        }else{
            return Usr_mail::mailfuncDiffNiniBody($this, $group, $item_id, $diff);
        }
    }

    function mailfuncDiff57($name, $diff){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfuncDiff57($this, $name, $diff);
        }else{
            return Usr_mail::mailfuncDiff57($this, $name, $diff);
        }
    }

    function mailfuncDiff52($name, $diff){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->mailfuncDiff52($this, $name, $diff);
        }else{
            return Usr_mail::mailfuncDiff52($this, $name, $diff);
        }
    }


    function replaceMsg($msg) {
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->replaceMsg($this, $msg);
        }

        $form_id   = $this->form_id;
        $eid       = $this->eid;
        $exec_type = 2;
        return Usr_function::wordReplace($msg, $this, compact("form_id", "eid", "exec_type"));
    }


    /**
     * セッションパラメータ取得
     * 画面入力データをセッションから取得する
     */
    function getDispSession(){
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->getDispSession($this);
        }

        $this->arrForm = array_merge($GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
        $this->arrForm = array_merge($this->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));
        $this->arrForm = array_merge($this->arrForm, (array)$GLOBALS["session"]->getVar("form_parampay"));

        if(isset($this->arrForm["edata31"])) {
            $wa_kikanlist = $this->_makeListBox($this->arrForm["edata31"]);
            $this->arrForm["kikanlist"] = $wa_kikanlist;
        }
    }


    /** 確認画面表示 */
    function _confirm(){
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->_confirm($this);
        }

        //フォームパラメータ
        $this->arrParam = GeneralFnc::convertParam($this->_init($this->wk_block), $_REQUEST);

        //入力チェック
        $this->arrErr = $this->_check();
        if(!(count($this->arrErr) > 0)){
            foreach($this->arrfile as $item_id => $n){
                if($this->itemData[$item_id]["disp"] != "1"){
                    //添付資料アップロード
                    list($wb_ret, $msg) = $this->fileTmp($item_id);
                    if(!$wb_ret){
                        $this->arrErr["file_error".$item_id] = $msg;
                    }
                }
            }
        }

        //セッションにページパラメータをセットする
        $GLOBALS["session"]->setVar("form_param".$this->wk_block, $this->arrParam);

        //セッションパラメータ取得
        $this->getDispSession();

        //エラーを自ページに表示
        if(count($this->arrErr) > 0){
            $this->block = $this->wk_block;
        }else{
            $this->block = "4";
            $this->_processTemplate = "Usr/form/Usr_entry_confirm.html";

            //お支払合計金額の計算
            if($this->formdata["kessai_flg"] == "1" ){
                $this->arrForm["total_price"] = Usr_function::_setTotal($this->wa_price, $this->arrForm, $this->formdata, $this->o_form->formData, $this->wa_ather_price);
            }
        }
    }


    /** 完了画面表示 */
    function complete($ps_msg, $pa_auth_err=""){
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->complete($this, $ps_msg, $pa_auth_err);
        }

        $this->_processTemplate = "Usr/form/Usr_entry_complete.html";
        $this->assign("msg", $ps_msg);

        //----------------------------------------
        //決済処理でエラーが発生した場合
        //----------------------------------------
        if(!empty($pa_auth_err)){
            $this->assign("errAuth", $pa_auth_err);
        }

        // 親クラスに処理を任せる
        parent::main();
        exit;
    }


    /** 完了ページの表示 */
    function showComplete() {
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->showComplete($this);
        }

        //----------------------------------
        //セッション情報クリア
        //----------------------------------
        $GLOBALS["session"]->unsetVar("form_param1");
        $GLOBALS["session"]->unsetVar("form_param2");
        $GLOBALS["session"]->unsetVar("form_param3");
        $GLOBALS["session"]->unsetVar("kyouID");           //共著者ID
        $GLOBALS["session"]->unsetVar("SS_USER");          //応募者ログイン情報
        $GLOBALS["session"]->unsetVar("workDir");
        $GLOBALS["session"]->unsetVar("ss_order_no");      //登録No.
        $GLOBALS["session"]->unsetVar("ss_total_payment"); //支払合計金額
        $GLOBALS["session"]->unsetVar("isLoginEntry");
        $GLOBALS["session"]->unsetVar("entryData");
        $GLOBALS["session"]->unsetVar("auth");
        $GLOBALS["session"]->unsetVar("start");            //セッション切れチェック項目
        $GLOBALS["session"]->unsetVar("form_editbefore");  //編集前の登録情報
        $GLOBALS["session"]->unsetVar("order_id");

        $this->assign("msg", $this->msg);
        $this->_processTemplate = "Usr/form/Usr_entry_complete.html";

        // 親クラスをする前の前処理
        if(is_object($this->exClass) && method_exists($this->exClass, 'premain')){
            $this->exClass->premain($this);
        }

        parent::main();

        // 開発用デバッグ関数
        $this->developfunc();
        exit;
    }


    /**
     * 添付ファイルアップロード
     * 添付ディレクトリにアップロードする
     */
    function fileTmp($item_id){
        // includeを優先する
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            return $this->exClass->fileTmp($this, $item_id);
        }

        // 一次格納ディレクトリ
        $workDir = $GLOBALS["session"]->getVar("workDir")."/";
        // 引き継ぐキー
        $key = "hd_file".$item_id;
        $key2= "file_upload".$item_id;


        // 既に一度ワークディレクトリに添付済みの場合
        $arrForm3 = $GLOBALS["session"]->getVar("form_param3");
        if($arrForm3[$key]  != "") $this->arrParam[$key]  = $arrForm3[$key];
        if($arrForm3[$key2] != "") $this->arrParam[$key2] = $arrForm3[$key2];

        // ファイルなし
        if(!isset($_FILES["edata".$item_id])) return array(true, "");

        // ファイルがアップロードされた場合
        $pa_filedata = $_FILES["edata".$item_id];
        if($pa_filedata["size"] > 0){
            // アップロードログ出力
            Usr_function::uploadLog($pa_filedata);

            if($pa_filedata["error"] > 0){
                return array(false,"ファイルのアップロードに失敗しました。");
            }

            // 直前までにアップロードしたファイルが存在する場合
            if($this->arrParam[$key] != "") {
                if(file_exists($workDir.$this->arrParam[$key])) {
                    unlink($workDir.$this->arrParam[$key]);
                }
            }

            // ファイルコピー
            $this->arrParam[$key] = $pa_filedata["name"];

            $rs = @move_uploaded_file($pa_filedata["tmp_name"], $workDir.$pa_filedata["name"]);
            if(!$rs){
                $this->arrParam[$key] = "";
                return array(false,"ファイルの一次保存に失敗しました。");
            }
            $this->arrParam["file_del".$this->arrParam[$key]] = "0";

            // ファイルアップロードフラグを立てる
            $this->arrParam[$key2] = 1;
        }

        return array(true, "");
    }


    function createWorkDir() {
        // 参加フォームもファイルのアップロードを許可する
        $workDir = $this->uploadDir."NEW/".date("YmdHis");
        //tmpディレクトリ作成（新規投稿用）
        if(!is_dir($workDir)){
            mkdir($workDir, 0777, true);
            chmod($workDir, 0777);
        }
        //セッションにワークディレクトリ名をセット
        $GLOBALS["session"]->setVar("workDir", $workDir);
    }


    function logrotate() {
//         // form_id別にログを出力
//         // daily用
//         $form_id = $this->o_form->formData["form_id"];
//         $usrLogDir = LOG_DIR."daily/".$form_id."/";
//         if(!is_dir($usrLogDir)){
//             mkdir($usrLogDir, 0777, true);
//             chmod($usrLogDir, 0777);
//         }
//         $GLOBALS["log"]->filename = $usrLogDir.date('Ymd').".log";
//         // DB用
//         $usrLogDirDb = LOG_DIR."DBLog/".$form_id."/";
//         if(!is_dir($usrLogDirDb)){
//             mkdir($usrLogDirDb, 0777, true);
//             chmod($usrLogDirDb, 0777);
//         }
//         $GLOBALS["log"]->filenameDb = $usrLogDirDb."DB_".date('Ymd').".log";
    }


    /** 開発用のデバッグ関数 */
    function developfunc(){
        if(is_object($this->exClass) && method_exists($this->exClass, __FUNCTION__)){
            $this->exClass->developfunc($this);
        }
    }

}


/**
 * メイン処理開始
 **/
$c = new Usr_Entry();
$c->mainAction();


?>
