<?php
/**
 * function
 * 	
 * @package packagename
 * @subpackage Usr
 * @author salon
 *
 */
class Usr_function{
	

	/**
	 * フォーム　アクセスログ
	 */	
	function accessLog($prame){

		$wa_log = "";
		
		$wa_log .="PARAM:";

		//リクエストパラメータ
		foreach($prame as $key  => $data){
			
			$wk_data = "";
			
			//キーがセキュリティーにかかわる項目の場合は、******に置き換える
			switch($key){
				//カード番号
				case "cardNumber":
				case "cardExpire1":
				case "securityCode":
//					if($data != ""){
//						$a = strlen($data);
//						$wk_data = sprintf("%'*".$a."s", $data);
//					}
					
				break;
				//セッションID
				case "PHPSESSID":
				case "PPA_ID":
				case "webfx-tree-cookie-persistence":
				case "reload":
					continue;
				break;				
				default:
					$wk_data = str_replace(array("\r\n","\r","\n"), "", $data);	
			}
			
			$wa_log .=	$key."=>".$wk_data." | ";
		}
		
		$mode = (isset($_REQUEST["mode"])) ? $_REQUEST["mode"] : "";

        // ブロックをログに含める
        $block = isset($_REQUEST["block"]) ? $_REQUEST["block"] : "1";

		$GLOBALS["log"]->write_log($mode.$block, $wa_log);
		
	}	 

	/**
	 * ファイルアップロード関連ログ出力
	 */
	function uploadLog($pa_filedata){
		
		$wa_log = "";
		$wa_log .="FILES:";
		
		//成功した場合
		if($pa_filedata["error"] == 0){
			$wa_log .="fileupload=>success | ";
		}
		//エラーの場合
		else{
			$wa_log .="fileupload=>error | error_num=>".$pa_filedata["error"];
			$wa_log .= " | error_reason=>";
			
			switch($pa_filedata["error"]){
				case 1:
					$wa_log .= "upload_max_filesize　で指定されたサイズを超えています";
				break;
				case 2:
					$wa_log .= "HTML フォームで指定されたサイズを超えています";
				break;
				case 3:
					$wa_log .= "アップロードされたファイルは一部のみしかアップロードされていません";
				break;
				case 4:
					$wa_log .= "ファイルはアップロードされませんでした。 ";
				break;
				case 6:
				
				break;
				default:
																			
			}
			
		}
		
		$wa_log .="=>c_name=>".$pa_filedata["name"]." | t_name=>".$pa_filedata["tmp_name"]." | size=>".$pa_filedata["size"];
		$GLOBALS["log"]->write_log($_REQUEST["mode"],$wa_log);
		
	}	 


	
	/**
	 * ファイル取得処理
	 * 
	 * @param string 取得先ディレクトリ
	 * @return array 取得ファイル名
	 */
	function getFiles($ps_dir){
		
		//取得ファイル名格納変数
		$wa_files = array();
	
		if( is_readable($ps_dir) ){
			@$wo_handle = opendir($ps_dir);
		} else {
			$wo_handle = "";
		}
			
		while ( $wo_handle != '' && $ws_wk_file = readdir($wo_handle) ) {
			if( $ws_wk_file != '.' && $ws_wk_file != '..' ) {
				if(is_file($ps_dir."/".$ws_wk_file)){
					//画面表示用にファイル名を配列に格納する
					array_push($wa_files, $ws_wk_file);					
				}
			}
		}
		
		if($wo_handle !=""){
			closedir($wo_handle);
		}

		return $wa_files;		
	}	 
	
	
	/**
	 * ディレクトリ削除
	 * 
	 * @param string 対象ディレクトリ
	 * @return bool
	 */
	function deleteDir($ps_dir){
		
		if (is_dir($ps_dir)) {
			$wo_dir_h = opendir($ps_dir);
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {
					if (is_dir($ps_dir . "/" . $ws_file)) {
					    $this->deleteDir($ps_dir . "/" . $ws_file);
					}
					else if (is_file($ps_dir . "/" . $ws_file)) {
					    unlink($ps_dir . "/" . $ws_file);
					}
				}
			}
			closedir($wo_dir_h);
			return rmdir($ps_dir);
		}
		else {
		    return true;
		}
		
	}	 	
	
	/**
	 * ファイル削除
	 * 	指定したディレクトリ内のファイルを削除する
	 * 
	 * @param string 対象ディレクトリ
	 * @return bool
	 */
	function deleteFile($ps_dir){

		if (is_dir($ps_dir)) {
			$wo_dir_h = opendir($ps_dir);
			while (false != ($ws_file = readdir($wo_dir_h)))
			{
				if($ws_file!="." && $ws_file != "..") {
					if (is_file($ps_dir . "/" . $ws_file)) {
					    unlink($ps_dir . "/" . $ws_file);
					}
				}
			}
			closedir($wo_dir_h);
			return true;
		}
		else {
		    return true;
		}
		
	}	 

    /**
     * 登録日、更新日の取得
     */
    function getEntryDate($po_db, $pn_id){

        $column = "to_char(rdate, 'yyyy/mm/dd hh24:mi:ss') as insday, to_char(udate, 'yyyy/mm/dd hh24:mi:ss') as upday";
        $from = "entory_r";
        $where[] = "del_flg = 0";
        $where[] = "eid = ".$po_db->quote($pn_id);

        $rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

        return $rs;
    }

    /**
     * 文字列置換
     * 
     * @param string  $subject   置換の対象となる文字列
     * @param array   $info      compact("form_id", "eid", "exec_type")
     */
    function wordReplace($subject, $obj, $info){
        // 対象文字列が空の場合は空を返す
        if(strlen($subject) == 0) return "";
        
        $objEntry   = new Entry();
        $objDb      = new DbGeneral();
        $entryData = $objEntry->getRntry_r($objDb, $info["eid"], $info["form_id"]);
        // title
        $entryData["edata57"] = strlen($entryData["edata57"]) > 0 ? $GLOBALS["titleList"][$entryData["edata57"]]: "";
        // rdate, udate
        $entryData["rdate"]   = date("Y-m-d H:i:s", strtotime($entryData["rdate"]));
        $entryData["udate"]   = date("Y-m-d H:i:s", strtotime($entryData["udate"]));
        // 新規登録固有の処理
        if($info["exec_type"] == "1"){
            $entryData["udate"] = "";
        }
        
        // 置換開始
        $ret_val = $subject;
        $ret_val = str_replace("_ID_"        , $entryData["e_user_id"]      , $ret_val);
        $ret_val = str_replace("_PASSWORD_"  , $entryData["e_user_passwd"]  , $ret_val);
        $ret_val = str_replace("_INSERT_DAY_", $entryData["rdate"]          , $ret_val);
        $ret_val = str_replace("_UPDATE_DAY_", $entryData["udate"]          , $ret_val);
        $ret_val = str_replace("_SEI_"       , $entryData["edata1"]         , $ret_val);
        $ret_val = str_replace("_MEI_"       , $entryData["edata2"]         , $ret_val);
        $ret_val = str_replace("_MIDDLE_"    , $entryData["edata7"]         , $ret_val);
        $ret_val = str_replace("_TITLE_"     , $entryData["edata57"]        , $ret_val);
        $ret_val = str_replace("_SECTION_"   , $entryData["edata10"]        , $ret_val);
        // 任意項目
        $group   = 1;
        $item_id = 26;
        $key     = "edata".$item_id;
        $etc = Usr_Assign::nini($obj, $group, $item_id, $entryData[$key]);
        $ret_val = str_replace("_ETC1_", $etc, $ret_val);
        
        return $ret_val;
    }


    /**
     * 支払金額の合計を計算
     */
    function _setTotal($pa_wa_price, &$pa_arrForm, $pa_formdata, $pa_form, $pa_ather_price){

//        if(!isset($pa_arrForm["amount"])) return 0;

        $total= isset($pa_wa_price[$pa_arrForm["amount"]]["p1"]) ? $pa_wa_price[$pa_arrForm["amount"]]["p1"] : 0;
        // Free, - => 0
        if(!is_numeric($total)){
            $total = 0;
        }

        if($pa_formdata["ather_price_flg"] != "1" && $pa_formdata["ather_price"] != ""){
            // その他決済項目
            foreach($pa_ather_price as $pkey => $data){
                if(!isset($pa_arrForm["ather_price".$pkey])) continue;
                if($pa_arrForm["ather_price".$pkey] != "" && $pa_arrForm["ather_price".$pkey] != "0"){
                    // Free, - => 0
                    if(!is_numeric($data["p1"])){
                        $data["p1"] = 0;
                    }
                    $total += $pa_arrForm["ather_price".$pkey] * $data["p1"];
                    $pa_arrForm["sum".$pkey] = $pa_arrForm["ather_price".$pkey] * $data["p1"];
                }
            }
        }

        return $total;
    }


    /**
     * テキストエリア等の入力項目を配列に変換する
     * 空白行、重複行、1行当たりの空白を削除
     * 
     * @param  string $str
     * @return array  $toArray
     */
    function gf_to_array_unique_trim($str){
        $toArray = array_merge(array_unique(array_filter(explode("\n", str_replace(array("\r\n", "\r"), "\n", $str)))));
        $toArray = array_map(array("self", 'gf_array_trim'), $toArray);
        return $toArray;
    }

    /**
     * 要素の空白を除去する
     * array_map(array("Usr_function", 'gf_array_trim'), $str)
     * 
     * @param  string $str
     * @return string
     */
    function gf_array_trim($str){
        return preg_replace('/[\s　]+/u', '', $str);
    }
} 
?>
