/**
 * reg3環境専用
 * 金額とその他決済項目の合計金額表示スクリプト
 * 
 * ex) $("#money_fee_area").feeCalculator({subtarget:["#money_option_area"], showElement:"#money_option_area"});
 *
 */
(function( $, undefined ) {



function Calculator() {
	this.version = "0.5.0",
	this._defaults = {
		 'target'       : "fee_calculate"	// 合計金額の出力先
		,'subtarget'    : ""				// レイアウトを複数に分けた時に各々計算する要素名     # 金額とその他決済も対象
		,'is_total'     : false				// レイアウトを複数に分けた時に各々出力するかのフラグ # デフォルト：合算した結果を表示	#未実装
		,'option_type'  : "select"			// その他決済項目のフォーム種別													# 未実装
		,'showElement'  : ""				// 合計金額を表示する要素 # afterで追記する
		,'showHTML'		: '<p class="r middle"><b><u>Amount of Total Payment: JPY<span id="total_price">0</span></u></b></p>'	// afterで追記するHTML
	}
}


$.extend(Calculator.prototype, {
	/* デフォルト値を返す */
	_initsetting: function() {
		return this._defaults;
	},


	/**
	 * 第1引数をターゲットに選択した金額の自動計算を行い出力する
	 * @param  target	合計金額の対象の親要素
	 * @param  options	拡張引数 #Calculator._defaults参照
	 * @return 
	 */
	_showComputation: function(target, options){
		var setting = $.extend(this._initsetting(), options);
		$(setting.showElement).after(setting.showHTML);

		var arrTarget = [target];
		if(setting.subtarget.length > 0){
			for(key in setting.subtarget){
				arrTarget.push(setting.subtarget[key]);
			}
		}

		// メインターゲットの金額の状態を監視
		$(target).bind("click", function(){ $.feeCalculator._attachCalculator(arrTarget, setting); });
		// サブターゲットの金額の状態を監視
		if(setting.subtarget.length > 0){
			for(key in setting.subtarget){
				$(setting.subtarget[key]).bind("change", function(){ $.feeCalculator._attachCalculator(arrTarget, setting); });
			}
		}

		$.feeCalculator._attachCalculator(arrTarget, setting);
	},


	/**
	 * 複数の要素の合計金額を個別に算出し合算を画面表示
	 * @param  arrTarget	対象の親要素配列
	 * @param  options		拡張引数 #Calculator._defaults参照
	 * @return void
	 */
	_attachCalculator: function(arrTarget, setting){
		var subtotal = 0;
		for(key in arrTarget){
			subtotal += this._selectCalculator(arrTarget[key], setting);
		}
		
		if(!$.isNumeric(subtotal)) subtotal = 0;
		$("#total_price").text(this._number_format(subtotal));
	},



	/**
	 * ターゲット要素で選択した金額の自動計算を行う
	 * @param  target	合計金額の対象の親要素
	 * @param  options	拡張引数 #Calculator._defaults参照
	 * @return total	合計金額
	 */
	_selectCalculator: function(target, setting){
		var total = 0;
		var target_area = $(target);

		//-----------------
		// Fee金額エリア
		//-----------------
		if(target_area.size() > 0){
			// 単一選択
			if(target_area.hasClass("fee1")){
				total += parseInt($("#money_fee_area #p1_0").text());
			// 複数選択
			}else if(target_area.hasClass("fee2")){
				// 選択した金額
				var fee = $("input:radio", target_area).filter(":checked").val();
				if(typeof fee !== "undefined") {
					total += parseInt($("#p1_"+fee).text());
				}
			}
			// 分割表示モード # 要望があれば実装する
		}


		//-----------------
		// その他決済エリア
		//-----------------
        // select
		var options = $("select", target_area);
		if(options.size() > 0){
            options.each(function(obj){
                var ticket = parseInt($("option:selected", this).val());
                if(ticket >= 1){
                    var id	= $(this).attr("id");
                    var price = parseInt($("#option_"+id).text());
                    if(!$.isNumeric(price)) price = 0;
                    total += price*ticket;
                }
            });
        }
        // checkbox
		var options = $("input:checkbox", target_area);
		if(options.size() > 0){
            options.each(function(obj){
                var id	= $(this).attr("id");
                var ticket = parseInt($("#"+id+":checked").val());
                if(ticket >= 1){
                    var price = parseInt($("#option_"+id).text());
                    if(!$.isNumeric(price)) price = 0;
                    total += price*ticket;
                }
            });
        }
        // radio
		var options = $("input:radio", target_area);
		if(options.size() > 0){
            options.each(function(obj){
                var id	= $(this).attr("id");
                var ticket = parseInt($("#"+id+":checked").val());
                if(ticket >= 1){
                    var price = parseInt($("#option_"+id).text());
                    if(!$.isNumeric(price)) price = 0;
                    total += price*ticket;
                }
            });
        }


		return total;
	},


	/**
	 * php#number_format
	 * @param  num	3桁区切りの対象の数値
	 * @return 3桁区切りの数値
	 */
	_number_format :function(num) {
		return num.toString().replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,')
	}

});


$.fn.feeCalculator = function(options){
	if(!this.selector) return this;
	$.feeCalculator._showComputation(this.selector, options);
};

$.feeCalculator = new Calculator();


})(jQuery);

