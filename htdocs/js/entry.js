/*
 * Usr_entry共通関数群
 * 
 */

(function( window, undefined ){

    if (window.entry === undefined) {
        window.entry = {};
    }

    var entry = window.entry;


    // エラー項目の強調
    entry.errorPointer = function(edata) {
        var element = "";
        var obj     = $("."+edata);
        var tagName = obj.get(0).tagName;
        var type    = obj.attr("type");


        if(tagName == "SELECT" || tagName == "TEXTAREA"
        ||(tagName == "INPUT" && type == "text")
        ||(tagName == "INPUT" && type == "file")
        ){
            element = obj;
        }else if(tagName == "INPUT" && (type == "radio" || type == "checkbox")){
            element = $("."+edata+"_label");
        }

        if(element.length > 0){
            element.css("background", "#FED1D1");
            return true;
        }

        return false;
    };


    // エラー項目の強調-一括設定
    entry.errorPointerArray = function(edataArray) {
        for(i in edataArray){
            entry.errorPointer(edataArray[i]);
        }
    };

})(window);
