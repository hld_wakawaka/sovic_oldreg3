
$.extend($, {
    /**
     * 異なる名前のラジオボタンをグループ化
     * ex. $.createRadioGroup("#feearea");
     * @param  target ラジオを包容する要素
     * @return void
     */
    createRadioGroup:function(target){
        var element = $(target+" [type='radio']");
        if(element.size() == 0) return;

        element.bind("click",(function(){
            $.each($(this).closest(target).find("[type='radio']"),function(){
                $(this).prop('checked', false);
            })
            $(this).prop('checked', true);
        }))
    }
});
