<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');
include_once(MODULE_DIR.'custom/Form.class.php');

/**
 * システム管理者　各種文言設定
 *
 *
 * @subpackage Sys
 * @author salon
 *
 */
class form_admin extends ProcessBase {

	/**
	 * コンストラクタ
	 */
	function form_admin(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){


		LoginAdmin::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		$this->_title = "スーパー管理者ページ";

		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//インスタンス
		//----------------------
		$this->wo_form = new Form();
		$this->db = new DbGeneral;
		$this->objErr = New Validate;

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//-----------------------
		//フォームパラメータ取得
		//-----------------------
		$this->arrForm["form_id"] = isset($_REQUEST["form_id"]) ? $_REQUEST["form_id"] : "";
		$this->arrForm["formadmin_id"] = isset($_REQUEST["formadmin_id"]) ? $_REQUEST["formadmin_id"] : "";

		//-------------------------
		//フォーム情報
		//-------------------------
		$this->formdata =  $this->_getFormTitle();
		if(!$this->formdata){
			$this->complete("フォーム情報の取得に失敗しました。");
		}
		$this->assign("formdata", $this->formdata);

		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_formadmin.html";

		//---------------------------------
		//アクション別処理
		//---------------------------------

		$arrErr = array();

		switch($ws_action){

			case "delete":

				$this->_delete();

				break;

			default:


		}

		//------------------------
		//　管理者のリスト取得
		//------------------------
		$this->arrFormAdmin = $this->getFormAdminList();
		$this->assign("arrData", $this->arrFormAdmin);

		$this->arrForm["form_id"] = $_REQUEST["form_id"];
		$this->arrForm["user_id"] = $_REQUEST["user_id"];

		$this->assign("arrErr", $arrErr);	//エラー配列
		$this->assign("msg", $this->msg);

		// 親クラスに処理を任せる
		parent::main();

		//print_r($this->arrForm);

	}

	/**
	 * フォーム情報
	 */
	function _getFormTitle(){

    	$column = "form_id, form_name";
    	$from = "form";
    	$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
    	$where[] = "del_flg = 0";

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	if(!$rs){
    		false;
    	}

		return $rs;



	}
	/**
	 * フォーム管理者の一覧を取得
	 */
	function getFormAdminList() {

		$column = "*";
    	$from = "form_admin";
    	$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
    	$where[] = "del_flg = 0";
    	$orderby = "formadmin_id";

    	$rs = $this->db->getListData($column, $from, $where, $orderby, -1, -1, __FILE__, __LINE__);

    	if(!$rs){
    		false;
    	}

		return $rs;

	}

	function _delete() {

		if($this->_reload->isReload()) {
			$this->msg = "既に実行した可能性があります。";
			return;
		}

		if($this->arrForm["formadmin_id"] == "") {
			$this->msg = "管理者IDの取得に失敗しました。";
			return;
		} elseif(!is_numeric($this->arrForm["formadmin_id"])) {
			$this->msg = "削除に必要なデータの取得に失敗しました。";
			return;
		}

		$param["del_flg"] = "1";
		$param["udate"] = "NOW";

		$where[] = "form_id = ".$this->db->quote($this->arrForm["form_id"]);
		$where[] = "formadmin_id = ".$this->db->quote($this->arrForm["formadmin_id"]);

		$this->db->update("form_admin", $param, $where, __FILE__, __LINE__);

		$this->msg = "管理者を削除しました。";

		return;

	}

}

/**
 * メイン処理開始
 **/

$c = new form_admin();
$c->main();







?>