<?php
/*
 * TGKパッケージの複製処理を行う
 *
 */

include_once('../../application/cnf/include.php');
include_once(MODULE_DIR.'mail_class.php');

class Sys_tgk{

	/**
	 * コンストラクタ
	 */
	function __construct($argc, $pn_form_id){

		if($argc != 2){

			//--------------------------------
			//終了処理
			//--------------------------------
			$msg = "クレジット決済 設定ファイルの作成を実施しようとしましたが、コマンド起動不正のため中止しました。\n\n";
			$this->_complete($msg);
			exit;
		}

		$this->form_id =$pn_form_id;
	}

	/**
	 * メイン処理
	 */
	function main(){
	    $cnf = "config.php";
	    $tgMDKcopyDir = TGMDK_COPY_DIR."/".$this->form_id;

		//-------------------------------
		//パッケージコピー
		//-------------------------------
		//ディレクトリ存在チェック
		if(!file_exists($tgMDKcopyDir."/".$cnf)){
    		if(!is_dir($tgMDKcopyDir)){
    			if(!mkdir($tgMDKcopyDir, 0777 ,true)){
    				//--------------------------------
    				//終了処理
    				//--------------------------------
    				$msg = "tgMdkパッケージコピーを実施しようとしましたが、ディレクトリ作成失敗したため中止しました。\n\n";
    				$msg .="フォームID:".$this->form_id."\n";
    				$msg .="ディレクトリ:".$tgMDKcopyDir;
    				$this->_complete($msg);
    			    exit;
    			}
            }
			//オリジナルパッケージをコピー
			$copyFrom = TGMDK_ORG_DIR."/".$cnf;
			$copyTo = $tgMDKcopyDir."/".$cnf;

			$cmd = "cp -rp ".$copyFrom." ".$copyTo;
			system($cmd);
		}


		//-------------------------------
		//定義ファイルの書き換え
		//-------------------------------
		$ws_conf = "";

		//対象の文字列を取得する
		$this->db = new DbGeneral;

		$getData = $this->db->getData("*", "form", "form_id = ".$this->db->quote($this->form_id), __FILE__, __LINE__);
		if(!$getData){
			$msg = "定義ファイル作成のための置き換え文字列のが取得できませんでした。\nDBの内容を確認してください。\n";
			$msg .="フォームID:".$this->form_id;
			$this->_complete("");
		}



		//文字列置換対象ファイル
		$conf_file = $tgMDKcopyDir."/".$cnf;

		if(file_exists($conf_file)){


		    $file = fopen($conf_file, "r");
		    flock($file, LOCK_EX);
		    while($string = fgets($file)) {
				$pos1 = strpos($string, "PGCARD_SHOP_ID");
				$pos2 = strpos($string, "PGCARD_SHOP_PASS");
				$pos3 = strpos($string, "PGCARD_SITE_ID");
				$pos4 = strpos($string, "PGCARD_SITE_PASS");
				$pos5 = strpos($string, "PGCARD_URL");
				$pos6 = strpos($string, "PGCARD_SECURE_RIDIRECT_HTML");
				$pos7 = strpos($string, "TdFlag");
				$pos8 = strpos($string, "TdTenantName");

				if($pos1 !== false){
					$buff = "define('PGCARD_SHOP_ID'   , '".$getData["pgcard_shop_id"]."'  );\r\n";

				}else if($pos2 !== false){
					$buff = "define('PGCARD_SHOP_PASS' , '".$getData["pgcard_shop_pass"]."'  );\r\n";

				}else if($pos3 !== false){
					$buff = "define('PGCARD_SITE_ID'   , '".$getData["pgcard_site_id"]."'  );\r\n";

				}else if($pos4 !== false){
					$buff = "define('PGCARD_SITE_PASS' , '".$getData["pgcard_site_pass"]."'  );\r\n";

				}else if($pos5 !== false){
					$buff = "define('PGCARD_URL' , '".DOMAINS."Usr/form/Usr_verify.php?form_id=".$this->form_id."'  );\r\n";

				}else if($pos6 !== false){
					$buff = "define('PGCARD_SECURE_RIDIRECT_HTML' , '".ROOT_DIR."Usr/form/RedirectPage.html'  );\r\n";

				}else if($pos7 !== false){
					$buff = "define('TdFlag' , '".$getData["pgcard_use3ds"]."'  );\r\n";

				}else if($pos8 !== false){
					$buff = "define('TdTenantName' , '".$getData["pgcard_3ds_shopname"]."'  );\r\n";

				}else{
					$buff = $string;
				}
				$ws_conf .= $buff;
		    }

		    flock($file, LOCK_UN);
		    fclose($file);


			//ファイルを作る
			$file = fopen($conf_file, "w");
		    flock($file, LOCK_EX);

			fwrite($file, $ws_conf);

		    flock($file, LOCK_UN);
		    fclose($file);

		}else{
			//--------------------------------
			//終了処理
			//--------------------------------
			$msg = "文字列置換失敗（対象ファイルが存在しないか、オープンに失敗しました。）\n\n";
			$msg .="フォームID:".$this->form_id."\n";
			$msg .="対象定義ファイル:".$conf_file;
			$this->_complete($msg);
			exit;
		}


		//--------------------------------
		//終了処理
		//--------------------------------
		$msg = "クレジット決済 設定ファイルの作成を実施しました。\n\n";
		$msg .="フォームID:".$this->form_id;
		$this->_complete($msg);

		return true;

	}

	/**
	 * 終了処理
	 */
	private function _complete($msg){

		$body = $msg;
		$subject = "【論文投稿システム　決済フォーム】クレジット決済 設定ファイル作成";


		//管理者宛て送信
		$this->o_mail = new Mail();
		$this->o_mail->SendMail(SYSTEM_MAIL, $subject, $body, "");


	}

}

$c = new Sys_tgk($argc, $argv[1]);
$c->main();

