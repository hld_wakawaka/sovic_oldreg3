<?php

$base_dir = realpath(dirname(__FILE__)."/")."/";

include($base_dir.'../../application/cnf/batch_include.php');
include_once(MODULE_DIR.'mail_class.php');
//include_once('../../application/module/custom/Form.class.php');
include($base_dir.'../../Usr/form/function.php');


/**
 * システム管理者　リセット
 * 	エントリー情報をリセットする
 *
 * @subpackage Sys
 * @author salon
 *
 */
class form_reset{

    /**
     * コンストラクタ
     */
    function form_reset($argc, $pn_formid){
        $this->form_id = $pn_formid;
    }

    /**
     * メイン処理
     */
    function main(){
        $this->db     = new DbGeneral(true);
        $this->o_mail = new Mail();
        $objUsrFunc   = new Usr_function();

        //フォーム情報取得
        $formData = $this->getFormData($this->db, $this->form_id);
        $subject = "エントリー情報リセット（".$formData["form_name"]."）";

        //---------------------------
        // フォームリセット開始
        //---------------------------
        try{
            log_class::printLog("フォームリセット：{$this->form_id}番フォームのリセット処理--Start", BATCH_LOG_FILE);
            $this->db->begin();
            
            // 1.エントリー対象のeidリストを取得
            $arrData = $this->getEntryList($this->db, $this->form_id);
            
            // 2.エントリー情報を削除
            $wb_ret1 = $this->resetEntry($this->db, $this->form_id);
            
            // 3.エントリー採番のリセット
            $wb_ret2 = $this->resetEntryNumber($this->db, $this->form_id);
            
            // 4.メールの送信履歴とその詳細を削除
            $wb_ret3 = $this->resetSendMail($this->db, $this->form_id, $arrData);
            
            // 5.金額の選択情報とその詳細を削除
            $wb_ret4 = $this->resetPayment($this->db, $this->form_id, $arrData);
            
            // いずれかがこけたら処理を停止
            if(!$wb_ret1 || !$wb_ret2 || !$wb_ret3 || !$wb_ret4) {
                $error  = "エントリー情報のリセットを行いましたが、正常に処理を終了することができませんでした。\n";
                if(!$wb_ret1) $error .= "　エントリー情報の削除に失敗しました。\n";
                if(!$wb_ret2) $error .= "　エントリー採番のリセットに失敗しました。\n";
                if(!$wb_ret3) $error .= "　メール送信履歴の削除に失敗しました。\n";
                if(!$wb_ret4) $error .= "　金額情報の削除に失敗しました。\n";
                throw new Exception($error);
            }
            
            //削除するディレクトリのパス
            $dir = UPLOAD_PATH."Usr/form".$this->form_id;
            $ret = $objUsrFunc->deleteDir($dir);
            if(!$ret){
                $error  = "エントリー情報のリセットを行いましたが、正常に処理を終了することができませんでした。\n";
                $error .= "ファイルアップロードディレクトリをご確認ください。[{$dir}]\n";
                throw new Exception($error);
            }
            
//            $ws_mailBody = "エントリー情報のリセットが完了しました。\n";
            $this->db->commit();
            
        }catch(Exception $e){
            $this->db->rollback();
            log_class::printLog("フォームリセット：".$e->__toString(), BATCH_LOG_FILE);
        }

        log_class::printLog("フォームリセット：{$this->form_id}番フォームのリセット処理--End", BATCH_LOG_FILE);

		//メール送信
		if($formData["form_mail"] != ""){
			//$this->o_mail->SendMail($formData["form_mail"], $subject, $ws_mailBody,"");
		}

	}


    /**
     * フォーム情報を取得する
     * 
     * @param  object  $db
     * @param  integer $form_id
     * @return array   $formData
     **/
    function getFormData($db, $form_id){
        $column = "form_id, form_name, type, lang, temp, edit_flg, form_desc, form_mail, contact, head, head_image, reception_date1, reception_date2, primary_reception_date, secondary_reception_date, agree, agree_text, group1, group2, group3, group4, group5, credit, pricetype, price";
        $where   = array();
        $where[] = "form_id = ?";
        $where[] = "del_flg = 0";
        $formData = $db->getRow($column, 'form', $where, array($form_id));
        return $formData;
    }


    /**
     * フォームに関連付くエントリー情報を物理削除を実施
     * 
     * @param  object  $db
     * @param  integer $form_id
     * @return boolean $result
     * @throws Exception
     **/
    function resetEntry($db, $form_id){
        $result = false;
        try{
            $result = $db->delete("entory_r", "form_id = ?", array($form_id));
            
        }catch(Exception $e){
            throw $e;
        }
        return $result;
    }


    /**
     * フォームに関連付くエントリー採番テーブルのリセットを実施
     * 
     * @param  object  $db
     * @param  integer $form_id
     * @return boolean $result
     * @throws Exception
     **/
    function resetEntryNumber($db, $form_id){
        $result = false;
        try{
            $param = array();
            $param["entory_count"] = 0;
            $param["order_count"] = 0;
            $param["udate"] = "NOW";
            $result = $db->update("entory_number", $param, "form_id = ?", array($form_id));
        }catch(Exception $e){
            throw $e;
        }
        return $result;
    }


    /**
     * フォームに関連付くエントリー採番テーブルのリセットを実施
     * 
     * @param  object  $db
     * @param  integer $form_id
     * @return array   $arrData
     * @throws Exception
     **/
    function getEntryList($db, $form_id){
        $arrData = array();
        try{
            $arrData = $db->getAll('eid', 'entory_r', "form_id = ?", array($form_id));
        }catch(Exception $e){
            throw $e;
        }
        return $arrData;
    }


    /**
     * フォームに関連付くメールの送信履歴とその詳細の削除を実施
     * 
     * @param  object  $db
     * @param  integer $form_id
     * @param  array   $arrData
     * @return boolean $result
     * @throws Exception
     **/
    function resetSendMail($db, $form_id, $arrForm){
        $result = false;
        try{
            // 0件の場合は削除を実施しない
            $data = $db->getRow("count(1) as count", 'mail_history', "form_id = ?", array($form_id));
            $count= $data['count'];
            if($count == 0) return true;
            
            $result = $db->delete("mail_history", "form_id = ?", array($form_id));
            foreach($arrForm as $_key => $_value){
                $eid = $_value['eid'];
                $db->delete("mail_sendentry", "eid = ?", array($eid));
            }
            
        }catch(Exception $e){
            throw $e;
        }
        return $result;
    }


    /**
     * フォームに関連付く金額情報とその詳細の削除を実施
     * 
     * @param  object  $db
     * @param  integer $form_id
     * @param  array   $arrData
     * @return boolean $result
     * @throws Exception
     **/
    function resetPayment($db, $form_id, $arrForm){
        $result = false;
        try{
            // 0件の場合は削除を実施しない
            $data = $db->getRow("count(1) as count", 'payment', "form_id = ?", array($form_id));
            $count= $data['count'];
            if($count == 0) return true;
            
            $result = $db->delete("payment", "form_id = ?", array($form_id));
            foreach($arrForm as $_key => $_value){
                $eid = $_value['eid'];
                $db->delete("payment_detail", "eid = ?", array($eid));
            }
        
        }catch(Exception $e){
            throw $e;
        }
        return $result;
    }

}

/**
 * メイン処理開始
 **/
$c = new form_reset($argc, $argv[1]);
$c->main();







?>