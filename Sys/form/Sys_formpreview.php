<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');

/**
 * システム管理者　フォーム登録・編集
 * 	フォームプレビュー画面表示
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class formpreview extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function formpreview(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

		LoginAdmin::checkLoginRidirect();
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["act"]) ? $_REQUEST["act"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;


		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/Sys_formpreview.html";
		$this->_title = "スーパー管理者ページ";

		//------------------------
		// 処理対象者
		//------------------------
		$user_data = array("managar_id" => "1"
						,"login_id" => "test001"
						,"password" => ""
						,"user_name" => "テスト1事務局"
						,"email" => "test1@xxxxx.co.jp"
						,"address"=>"広島市中区猫屋町"
						,"tel"=>"082-111-2222"
						,"fax"=>"082-222-3333");
						
		$this->assign("user_data", $user_data);	



		//---------------------------------
		//フォームの種類
		//---------------------------------


		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){
			
			//----------------------------
			//登録処理
			//----------------------------
			case "complete":

				//完了画面表示
				$this->_processTemplate = "Sys/Sys_complete.html";
				
//				//完了メッセージ
//				if($this->arrForm["mode"] == "1"){
//					$wk_mode="登録しました。";
//					$this->_title = "";
//				}
//				else{
//					$wk_mode = "編集しました。";
//					$this->_title = "";
//				}
				$this->_title = "フォーム登録　完了";
				$this->arrForm["url"] = "Sys_formlist.php";
				$form_name = isset($this->arrForm["form_name"]) ? $this->arrForm["form_name"] : "テスト　応募フォーム";
				$this->assign("msg", $form_name."を登録しました。");

			break;
			/**
			 * 初期表示時
			 */
			default:
			
				if($_REQUEST["form_id"] == ""){
					$wk_base = array();
				}
				else{

					//ダミーデータ
					$wk_base = array("form_name"=>"第1回 XXXXXXXXX学会論文受付フォーム"
									,"comment" => "・本文は900文字以内で作成してください。\n・必須の記載がある欄は必須事項です。\n・受付期間内は投稿した内容の編集が可能です。"
									,"syear" => "2010"
									,"smonth" => "08"
									,"sday" => "01"
									,"eyear" => "2010"
									,"emonth" => "09"
									,"eday" => "20"
									,"form_kbn" => isset($_REQUEST["form_kbn"]) ? $_REQUEST["form_kbn"] : "1"
									,"fst_syear" => "2010"
									,"fst_smonth" => "08"
									,"fst_sday" => "01"
									,"fst_eyear" => "2010"
									,"fst_emonth" => "08"
									,"fst_eday" => "20"
									,"reference" => "東京都港区XXXXXXXX　3-1-1\n　XXXXXX学会事務局\n\nTEL:03-123-4567"
									,"materials" => "1"
									,"item1" => "筆頭者の氏名（日本語表記）"
									,"item2" => "筆頭者の氏名（ひらがな）"
									,"item3" => "筆頭者の英語表記"
									,"article_kbn" => "1"
									,"item4" => "共著者のチェック"
									,"item5" => "筆頭者の会員・非会員のチェック"
									,"withnum" => "5"
									,"item6" => "電子メールアドレス"
									,"item7" => "筆頭者の所属機関住所の郵便番号（現在のもの）（例）065-0042"
									,"item8" => "筆頭者の所属機関住所（例）北海道札幌市東区本町"
									,"item9" => "筆頭者の電話番号（例）001-782-6160"
									,"item10" => "上記の内線番号"
									,"item11" => "筆頭者のFAX番号（例）001-782-4850"
									,"item12" => "筆頭者の所属機関名（例）クラーク病院整形外科"
									,"item13" => "筆頭者の所属機関名（英語表記）（例）Dept. of Orthop. Surg., CLARK Hosptal"
									,"item14" => "筆頭者の雑誌掲載用所属機関（例）クラーク病院整形"
									,"item15" => "筆頭者の雑誌掲載用所属機関（英語表記）（例）CLARK Hosptal"
									,"item16" => "筆頭著者の連絡先住所の郵便番号"
									,"item17" => "筆頭著者の連絡先住所"
									,"item18" => "筆頭著者の連絡先の電話番号"
									,"item19" => "内線"
									,"item20" => "筆頭著者の連絡先のFAX番号"
									,"item21" => "氏名"
									,"item22" => "氏名（かな）"
									,"item23" => "氏名（英語表記）"
									,"item24" => "会員・非会員のチェック"
									,"item25" => "雑誌掲載用所属機関"
									,"item26" => "発表形式"
									,"item27" => "演題名（例）候補遺伝子による後縦靭帯骨化症の広範囲原因遺伝子検索"
									,"item28" => "英語演題名（例）Extermal fixation for fractures in children."
									,"item29" => "抄録本文"
									,"attention" => "下の枠が抄録本文（タイトル、所属機関名、著者名は除く）を記入する欄です。\n先頭行も１マス開けずに左詰めで記入してください。"
									
									);
									
					if($_REQUEST["form_id"] == "9"){
						$wk_base["form_kbn"] = "3";
						$wk_base["form_name"] = "第20回　○○○○○○学会　参加受けフォーム";
						$wk_base["comment"] = "必要事項を記入して、お申込みください。";
					}										
									
					
				}

				$this->arrForm = $wk_base;				
				$this->arrForm["form_id"] = $_REQUEST["form_id"];
									
				
					
			 break;						
			
			
			
		}		

	

		
		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 



}

/**
 * メイン処理開始
 **/

$c = new formpreview();
$c->main();







?>