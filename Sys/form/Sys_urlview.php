<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');

/**
 * システム管理者　フォーム一覧
 * 	指定ユーザのフォーム一覧を表示する
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class urlview extends ProcessBase {

	/**
	 * コンストラクタ
	 */
	function urlview(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

		LoginAdmin::checkLoginRidirect();
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_urlview.html";
		$this->_title = "スーパー管理者ページ";
		
		$this->arrForm["form_id"] = $_REQUEST["form_id"];
		$this->arrForm["user_id"] = $_REQUEST["user_id"];
		
		$objUser = new User;
		$user_data = $objUser->get($this->arrForm["user_id"]);
		$this->assign("userData", $user_data);	
		
		// 親クラスに処理を任せる
		parent::main();
		
	}
	
}

/**
 * メイン処理開始
 **/

$c = new urlview();
$c->main();







?>