<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');



/**
 * システム管理者　フォーム登録・編集
 *     フォームの基本情報部分入力画面表示
 *
 * @subpackage Sys
 * @author salon
 *
 */
class formbase extends ProcessBase {

    /** CSV出力及び取り込みのデリミタ */
    private $delimiter = ",";
    private $workDir   = "";


    /**
     * コンストラクタ
     */
    function formbase(){
        LoginAdmin::checkLoginRidirect();

        parent::ProcessBase();

        //ログイン者情報
        $this->assign("user_name", "システム管理者");

        //スーパー管理者メニュー
        $this->assign("va_menu", sys_common::menu());

        // 表示HTMLの設定
        $this->_processTemplate = "Sys/form/Sys_form_import.html";
        $this->_title = "スーパー管理者ページ";

        // オブジェクト生成
        $this->db = new DbGeneral(true);

        // 編集初期化
        $this->arrErr  = array();
        $this->workDir = ROOT_DIR."Sys/copy/output/";
    }


    /**
     * メイン処理
     */
    function main(){
        // 処理モード
        $action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

        //  入力パラメータ
        $this->arrForm = GeneralFnc::convertParam($this->init($action), $_POST);


        //---------------------------------
        //アクション別処理
        //---------------------------------
        switch($action){
            // CSV出力
            case "output":
                try{
                    $this->checkOutput();
                    if(count($this->arrErr['output']) > 0) break;

                    // CSV出力を行う
                    $form_id = $this->arrForm['form_id'];
                    if($this->execOutput($form_id)){
                        // CSV出力結果のZIPアーカイブを作成
                        $zip_filename = "importCSV_".$form_id.".zip";
                        $cmd = "zip -j ".$this->outputDir.$zip_filename." ".$this->outputDir."/*.csv";
                        exec("$cmd > /dev/null &");
                        $this->assign("onload_jquery", "chkZipArchive('".$this->outputDir.$zip_filename."');");
                        $this->complete("ファイルのアーカイブを作成しています。<br/>完了しましたらダウンロードが表示されます。");
                    }

                }catch(Exception $e){
                    $this->arrErr['output'][] = $e->__toString();
                }
                break;

            case "ajax":
                $script = isset($_GET['action']) ? $_GET['action'] : "";
                $file   = isset($_GET['file'])   ? $_GET['file']   : "";
                if(strlen($script) == 0 || strlen($file) == 0){
                    echo "non";
                }
                echo $this->$script($file);
                exit;
                break;

            case "download":
                $file   = isset($_POST['file'])   ? $_POST['file']   : "";
                $this->dlZipArchive($file);
                break;

                // CSV取込
                case "input":
                try{
                    $this->checkInput();
                    if(count($this->arrErr['input']) > 0) break;

                    // Zipファイルの展開
                    $this->execInput();
                    $this->complete("Zipファイルの取込に成功しました。");

                }catch(Exception $e){
                    $this->arrErr['input'][] = $e->__toString();
                }
                break;

            default:
                 break;


        }


        $this->assign("arrErr", $this->arrErr);

        // 親クラスに処理を任せる
        parent::main();
    }


    /**
     * 処理モード別の入力パラメータ配列を生成
     * 
     * @param  text  $mode 処理モード
     * @return array $key  入力情報配列
     **/
    function init($mode) {
        $key = array();

        //(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
        if($mode == "input"){
            $key[] = array("archive",  "Zipファイル"       ,  array(),  array("NULL")           , "",  1,  0);
        }

        if($mode == "output"){
            $key[] = array("form_id",  "出力対象のフォームID",  array(),  array("NULL", "NUMERIC"), "n",  1,  0);
            $key[] = array("option" ,  "フォーム名"        ,  array(),  array("NULL")           , "n",  1,  0);
        }

        return $key;
    }


    // 完了画面の表示
    function complete($msg) {
        $this->assign("msg", $msg);
        $this->_processTemplate = "Sys/form/Sys_form_import_complete.html";
        $this->arrForm["url"] = "Sys_form_import.php";
    
        // 親クラスに処理を任せる
        parent::main();
        exit;
    }


    //-----------------------------------------------
    // CSV出力 function
    //-----------------------------------------------

    /**
     * CSV出力用のエラーチェックを行う
     * 
     * @param  void
     * @return エラー配列
     **/
    function checkOutput() {
        $objErr = new Validate;
        $objErr->check($this->init(), $this->arrForm);

        // エラーが発生していない場合はform_idの存在チェックを行う
        if(!(count($objErr->_err) > 0)){
            $form_id = $this->arrForm['form_id'];
            if(!$this->db->exists("form", "form_id=?", array($form_id))){
                $objErr->addErr("{$form_id}番フォームは存在しません。", "form_id");
            }
        }

        $objErr->sortErr($this->init());
        $this->arrErr['output'] = $objErr->_err;
    }


    /**
     * CSV出力を行う
     *
     * @param  void
     * @return エラー配列
     * @throws outputCsv
     **/
    function execOutput($form_id){
        $this->outputDir = $this->workDir.$form_id."/";
        // 出力先ディレクトリの作成
        if(!is_dir($this->outputDir)){
            mkdir($this->outputDir, 0777, true);
            chmod($this->outputDir, 0777);
        }
        // 出力先ディレクトリにファイルがあれば削除する # 1世代前のファイル
        $csvs = glob($this->outputDir."*.csv");
        if(is_array($csvs)){
            foreach($csvs as $_key => $csvfile){
                unlink($csvfile);
            }
        }

        // No.1 # エントリー情報
        $query = $this->db->query("select * from entory_number where form_id = ?"                    , array($form_id));
        $csvdata1 = $this->outputCsv($query, "entory_number.csv");

        // No.2 # フォーム情報
        $query = $this->db->query("select * from form          where form_id = ?"                    , array($form_id));
        $csvdata2 = $this->outputCsv($query, "form.csv");

        // No.3 # フォーム管理者情報
        $query = $this->db->query("select * from form_admin    where form_id = ? and super_flg = 1"  , array($form_id));
        $csvdata3 = $this->outputCsv($query, "form_admin.csv");

        // No.4 # フォーム項目情報
        $query = $this->db->query("select * from form_item     where form_id = ?"                    , array($form_id));
        $csvdata4 = $this->outputCsv($query, "form_item.csv");

        // No.5 # フォーム文言情報
        $query = $this->db->query("select * from form_word     where form_id = ?"                    , array($form_id));
        $csvdata5 = $this->outputCsv($query, "form_word.csv");

        // No.6 # ユーザ情報
        $query = $this->db->query("select users.* from users left join form on form.user_id = users.user_id where form_id =?", array($form_id));
        $csvdata6 = $this->outputCsv($query, "users.csv");

        // No.7 # エントリー情報を取得
        if($this->arrForm['option'] == 1){
            $query = $this->db->query("select * from entory_r where form_id = ?"                    , array($form_id));
            $csvdata7 = $this->outputCsv($query, "entory_r.csv");
        }
        return true;
    }


    /**
     * CSVコンテンツを生成する
     *
     * @param  array $query     1行あたりの連想配列
     * @return array $csvdata   CSVコンテンツ
     * @throws
     **/
    function outputCsv($query, $output){
        try{
            $fp = fopen($this->outputDir.$output, 'w');
            if(!$fp) throw new Exception("ファイルの展開に失敗しました。[".$this->outputDir.$output."]");
            $csvdata = "";
            foreach($query as $data){
                if(!fputcsv($fp, $data)) throw new Exception("ファイルの書き込みに失敗しました。[".$this->outputDir.$output."]");
            }

        }catch(Exception $e){
            $this->arrErr['output'] = $e->__toString();
        }
        fclose($fp);
    }


    /**
     * Zipファイル処理が完了したかチェック
     *
     * @param  string  $file  ZIPアーカイブまでのフルパス
     * @return integer 1or0   1:Zipファイル処理完了
     **/
    function chkZipArchive($file){
        echo (integer)is_file($file);
    }


    /**
     * Zipファイルのダウンロード
     *
     * @param  string  $file  ZIPアーカイブまでのフルパス
     * @return void
     **/
    function dlZipArchive($file){
        // ZIPアーカイブのダウンロード
        include(MODULE_DIR.'Download.class.php');
        $objDL = new Download();
        $objDL->file($file, pathinfo($file, PATHINFO_FILENAME).".".pathinfo($file, PATHINFO_EXTENSION), 1);
        exit;
    }


    //-----------------------------------------------
    // CSV取込 function
    //-----------------------------------------------
    
    /**
     * CSV出力用のエラーチェックを行う
     *
     * @param  void
     * @return エラー配列
     **/
    function checkInput() {
        $objErr = new Validate;
        $objErr->check($this->init(), $this->arrForm);

        // アップロードチェック
        if($_FILES["archive"]["error"] == UPLOAD_ERR_NO_FILE
        || $_FILES["archive"]["size"]  == 0){
            $objErr->addErr("CSVファイルをアップロードしてください。", "archive");
        }

        // 拡張子チェック
        if(!Validate::checkExt($_FILES["archive"], array("csv"))){
            $objErr->addErr("CSVファイルをアップロードしてください。", "archive");
        }

        $objErr->sortErr($this->init());
        $this->arrErr['input'] = $objErr->_err;
    }


    /**
     * CSV取込を行う
     *
     * @param  void
     * @return エラー配列
     * @throws outputCsv
     **/
    function execInput(){
        try{
            $to = realpath(ROOT_DIR."../../tmp/reg3/copy/")."/";
            if(ENV == ENV_TYPE1){   // ローカル環境
                $to = ROOT_DIR."Sys/copy/input/";
            }
            $rs = @rename($_FILES["archive"]["tmp_name"], $to.$_FILES["archive"]["name"]);
            if(!$rs) throw new Exception("CSVファイルの展開に失敗しました。[".$to.$_FILES["archive"]["name"]."]");
            chmod($to.$_FILES["archive"]["name"], 0777);

            $csv   = $to.$_FILES["archive"]["name"];
            $table = pathinfo($_FILES["archive"]["name"], PATHINFO_FILENAME);
            $this->db->query("COPY {$table} FROM '{$csv}' WITH CSV");
            unlink($csv);

        }catch(Excption $e){
            $this->arrErr["input"][] = $e->__toString();
        }
    }

    
}

/**
 * メイン処理開始
 **/

$c = new formbase();
$c->main();







?>