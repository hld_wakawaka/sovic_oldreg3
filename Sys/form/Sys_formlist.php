<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');

/**
 * システム管理者　フォーム一覧
 * 	指定ユーザのフォーム一覧を表示する
 *
 * @subpackage Sys
 * @author salon
 *
 */
class formlist extends ProcessBase {

	var $sess_key = "sk_form";
	var $search_key = array(
				"user_id"
				,"form_name"
				,"form_type"
				,"syear"
				,"smonth"
				,"sday"
				,"eyear"
				,"emonth"
				,"eday"
				);


	/**
	 * コンストラクタ
	 */
	function formlist(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){
		LoginAdmin::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");

		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_formlist.html";
		$this->_title = "スーパー管理者ページ";

		//----------------------
		// オブジェクト生成
		//----------------------
		$objForm = new Form;
		$objUser = new User;


		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 =date("Y",strtotime("-1 year"));
		$wk_year1 =date("Y");
		$wk_year2 =date("Y",strtotime("+1 year"));


		$syear[$wk_year0] = $wk_year0;
		$syear[$wk_year1] = $wk_year1;
		$syear[$wk_year2] = $wk_year2;

		$eyear[$wk_year0] = $wk_year0;
		$eyear[$wk_year1] = $wk_year1;
		$eyear[$wk_year2] = $wk_year2;


		//月
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;

		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）

		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//------------------
			//一覧表示
			//------------------
			case "search":

				$GLOBALS["session"]->unsetVar($this->sess_key);

				foreach($this->search_key as $val) {
					if(isset($_REQUEST[$val])) {
						if(is_array($_REQUEST[$val])) {
							$searchkey[$val] = $_REQUEST[$val];
						} else {
							$searchkey[$val] = trim($_REQUEST[$val]);
						}
					} else {
						$searchkey[$val] = "";
					}
				}
				$GLOBALS["session"]->SetVar($this->sess_key, $searchkey, 30);

				break;

			case "clear":

				$GLOBALS["session"]->unsetVar($this->sess_key);

				break;

			//---------------------------
			//リセット
			//---------------------------
			case "reset":

//				$cmd = "/usr/bin/php ".ROOT_DIR."Sys/form/Sys_reset.php ".$_REQUEST["form_id"]." ".$_REQUEST["user_id"]." > /dev/null &";
				$cmd = "/usr/bin/php ".ROOT_DIR."Sys/form/Sys_reset.php ".$_REQUEST["form_id"]." > /dev/null &";


				system($cmd);

				$this->assign("msg", "エントリー情報をリセットバッチを実行しました。");
				$this->_processTemplate = "Sys/Sys_complete.html";

				$this->arrForm["url"] = "Sys_formlist.php?user_id=".$_REQUEST["user_id"];

				// 親クラスに処理を任せる
				parent::main();
				exit;

				break;


			case "mnglogin":

				if($_REQUEST["form_id"] > 0) {

					$db = new DbGeneral();
					$where[] = "form_id = ".$db->quote($_REQUEST["form_id"]);
					$where[] = "	super_flg = 1";
					$memberData = $db->getData("*", "v_user", $where, __FILE__, __LINE__);

					if($memberData) {

						//先に別端末でログインしていないか？
						if($memberData["access_session"] != "" && $memberData["access_session"] != session_id()){

							//一定時間内の場合は他のユーザが操作中とみなす
							$access_date = date("YmdHis",strtotime("+".SESSION_LIFE_TIME." sec" ,strtotime(substr($memberData["access_time"], 0,19))));

							if($access_date >= date("YmdHis")){
								$this->onload = "二重ログイン！";
								break;
							}
						}

						//パスワード間違い件数クリア
						//最終ログイン日時セット
						$wb_ret = $db->update("form_admin", array("login_err_count" => 0, "login_try_date" => NULL, "access_time"=>"now()" ,"access_session" => session_id()), "login_id =".$db->quote($memberData["login_id"]));

						//最新の情報を取得
						$where[] = "formadmin_id = ".$db->quote($memberData["formadmin_id"]);
						$memberData = $db->getData("*", MEMBER_TABLE, $where, __FILE__, __LINE__);
						if(!$memberData){
							$this->onload = ERR_NOT_MEMBER_ID;
							break;
						}

						$GLOBALS["session"]->setVar("isLogin", true);
						$GLOBALS["session"]->setVar("userData", $memberData);

						$GLOBALS["userData"] = $memberData;

						header("location: ".MNG_URL."index.php");
						exit;
					}

				}

				break;
			/**
			 * 初期表示時
			 */
			default:

			 	break;


		}

		$this->arrForm = $GLOBALS["session"]->getVar($this->sess_key);
		$this->arrForm["user_id"] = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : $this->arrForm["user_id"];

		$formtype = isset($this->arrForm["form_type"]) ? $this->arrForm["form_type"] : array();
		$form["form_type"] = SmartyForm::createRadioChecked("form_type", $GLOBALS["formtypeList"], $formtype, "checkbox", "form");
		$this->assign("form", $form);

		if(!$this->arrForm["user_id"] == "") {

			$userData = $objUser->get($this->arrForm["user_id"]);
			$this->assign("userData", $userData);

			$page = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";
			$arrData = $objForm->getList($this->arrForm, $page, 100);

			$this->assign("arrData", $arrData);
		}


		// 親クラスに処理を任せる
		parent::main();

	}

}

/**
 * メイン処理開始
 **/

$c = new formlist();
$c->main();







?>
