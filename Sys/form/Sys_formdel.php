<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');

/**
 * システム管理者
 * 	フォーム削除
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class formdel extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function formdel(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

		LoginAdmin::checkLoginRidirect();
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());
		
		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_formdel.html";
		$this->_title = "フォーム情報削除";

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = $_REQUEST;
		
		if($this->arrForm["form_id"] == "") $this->complete("削除するフォームが指定されていません。");
		
		$this->arrForm = $GLOBALS["form"]->get($this->arrForm["form_id"]);
		if($this->arrForm["del_flg"] == "1") $this->complete($this->arrForm["form_name"]."は、既に削除されています。");


		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//-----------------
			//削除
			//-----------------
			case "complete":
			
				$db = new DbGeneral;
				
				$param["del_flg"] = "1";
				$param["udate"] = "NOW";
				$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);
				
				$rs = $db->update("form", $param, $where, __FILE__, __LINE__);
				
				if(!$rs) {
					$this->complete($this->arrForm["form_name"]."の削除に失敗しました。");
				}
				$this->complete($this->arrForm["form_name"]."の削除しました。");
				
			　break;		
			
			/**
			 * 初期表示時
			 */
			default:

			 break;
			
		}		

		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}
	
	// 完了画面の表示
	function complete($msg) {
		
		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";
		
		if(isset($this->arrForm["user_id"])) {
			$this->arrForm["url"] = "Sys_formlist.php?user_id=".$this->arrForm["user_id"];
		} else {
			$this->arrForm["url"] = "../index.php";
		}
		
		// 親クラスに処理を任せる
		parent::main();
		exit;
		
	}
	 



}

/**
 * メイン処理開始
 **/

$c = new formdel();
$c->main();







?>