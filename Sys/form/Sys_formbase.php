<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');
include(MODULE_DIR.'custom/Entry.class.php');


/**
 * システム管理者　フォーム登録・編集
 * 	フォームの基本情報部分入力画面表示
 *
 * @subpackage Sys
 * @author salon
 *
 */
class formbase extends ProcessBase {

	var $objForm;


	/**
	 * コンストラクタ
	 */
	function formbase(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){

		LoginAdmin::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");

		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//-----------------------
		//フォームパラメータ
		//-----------------------

		$formparam1 = GeneralFnc::convertParam($this->init(), $_REQUEST);
		$formparam2 = GeneralFnc::convertParam($this->init2(), $_REQUEST);
		$this->arrForm = array_merge($formparam1, $formparam2);





		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_formbase.html";
		$this->_title = "スーパー管理者ページ";

		//----------------------
		// オブジェクト生成
		//----------------------
		$this->objForm    = new Form;
		$this->objEntry   = new Entry();
		$this->objItemIni = new item_ini;
		$objUser = new User;

		//------------------------
		// 処理対象者
		//------------------------
		if($this->arrForm["user_id"] == "") {
			$this->complete("ユーザーが選択されていません。");
		}

		$user_data = $objUser->get($this->arrForm["user_id"]);
		$this->assign("user_data", $user_data);

		//--------------------------
		// 日付用リストボックス生成
		//--------------------------
		$wk_year0 ="2010";
		$wk_year2 =date("Y",strtotime("+1 year"));

		for($y = $wk_year0; $y <= $wk_year2; $y++) {
			$syear[$y] = $y;
			$eyear[$y] = $y;
		}

		//月
		for($i=1; $i<13; $i++){
			$wk_month = sprintf('%02d', $i);
			$smonth[$wk_month] = $wk_month;
			$emonth[$wk_month] = $wk_month;
		}
		//日付
		for($i=1; $i<32; $i++){
			$wk_day = sprintf('%02d', $i);
			$sday[$wk_day] = $wk_day;
			$eday[$wk_day] = $wk_day;
		}

		//時間
		for($i = 0; $i < 24; $i++) {
			$wk_hour = sprintf('%02d', $i);
			$shour[$wk_hour] = $wk_hour;
			$ehour[$wk_hour] = $wk_hour;
		}

		$this->assign("syear", $syear);				//開始（年）
		$this->assign("smonth", $smonth);			//開始（月）
		$this->assign("sday", $sday);				//開始（日）
		$this->assign("shour", $shour);				//開始（時）

		$this->assign("eyear", $eyear);				//終了（年）
		$this->assign("emonth", $emonth);			//終了（月）
		$this->assign("eday", $eday);				//終了（日）
		$this->assign("ehour", $ehour);				//開始（時）


		//---------------------------------
		//アクション別処理
		//---------------------------------

		$this->arrErr = array();
		switch($ws_action){

			case "save":
			case "save2":

				$this->check();
				$imgerr = $this->upload_image();
				if($imgerr != "") $this->arrErr["heade_image"] = $imgerr;
				if(count($this->arrErr) > 0) break;

				//MDK設定情報 をセッションにセット
				$this->_set_ss_merchant();


				$item = "0";
				if($ws_action == "save2") $item = "1";

				if($this->arrForm["form_id"] == "") {
					$this->insert('1', $item);
				} else {
					$this->update('1', $item);
				}

				break;

			case "next":

				//---------------------------------
				//MDK設定情報 をセッションにセット
				//---------------------------------
				$this->_set_ss_merchant();

				//---------------------------------
				//MDK設定情報 をセッションから復元
				//---------------------------------
				$this->_restore_merchant();


				$this->check();
				$imgerr = $this->upload_image();
				if($imgerr != "") $this->arrErr["heade_image"] = $imgerr;
				if(count($this->arrErr) > 0) break;


				$this->_processTemplate = "Sys/form/Sys_formitem.html";

				 break;

			case "confirm":

				//---------------------------------
				//MDK設定情報 をセッションから復元
				//---------------------------------
				$this->_restore_merchant();

				$this->check();
				$this->check_item();
				if(count($this->arrErr) > 0) {
					$this->_processTemplate = "Sys/form/Sys_formitem.html";
					break;
				}

				if($this->arrForm["price"] != "") {
					$list = explode("\n", $this->arrForm["price"]);
					foreach($list as $val) {
						$arrPrice[] = explode(" ", $val);
					}
					$this->assign("arrPrice", $arrPrice);
				}
				if($this->arrForm["ather_price"] != "") {
					$list = explode("\n", $this->arrForm["ather_price"]);
					foreach($list as $val) {
						$arrAtherPrice[] = explode(" ", $val);
					}
					$this->assign("arrAtherPrice", $arrAtherPrice);
				}


				//----------------------------------------------
				// 自動金額変更@追加->4/11
				//----------------------------------------------
				if($this->arrForm["auto_change_price"] != "") {
					$list = explode("\n", $this->arrForm["auto_change_price"]);
					foreach($list as $val) {
						$arrPrice_auto[] = explode(" ", $val);
					}
					$this->assign("arrPrice_auto", $arrPrice_auto);
				}
				if($this->arrForm["auto_change_ather_price"] != "") {
					$list = explode("\n", $this->arrForm["auto_change_ather_price"]);
					foreach($list as $val) {
						$arrAtherPrice_auto[] = explode(" ", $val);
					}
					$this->assign("arrAtherPrice_auto", $arrAtherPrice_auto);
				}

				$this->_processTemplate = "Sys/form/Sys_form_confirm.html";



				break;

			case "complete":

				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");

				//---------------------------------
				//MDK設定情報 をセッションから復元
				//---------------------------------
				$this->_restore_merchant();

				$this->check();
				$this->check_item();
				if(count($this->arrErr) > 0) {
					$this->_processTemplate = "Sys/form/Sys_formitem.html";
					break;
				}



				if($this->arrForm["form_id"] == "") {
					$this->insert(0, 1);
				} else {
					$this->update(0, 1);
				}

				break;

			case "back":
				//---------------------------------
				//MDK設定情報 をセッションから復元
				//---------------------------------
				$this->_restore_merchant();

				break;

			/**
			 * 初期表示時
			 */
			default:

				if($this->arrForm["form_id"] != "") {
					$this->arrForm = $this->objForm->get($this->arrForm["form_id"]);
					if($this->arrForm["auto_change_day"] != "") {
						$str = str_split($this->arrForm["auto_change_day"], 2);
						$this->arrForm["myear"] = $str[0].$str[1];
						$this->arrForm["mmonth"] = $str[2];
						$this->arrForm["mday"] = $str[3];
						$this->arrForm["mhour"] = $str[4];
					}

					unset($this->arrForm["password"]);

					// 受付期間
					if($this->arrForm["reception_date1"] != "") {
						$this->arrForm["syear"] = date('Y', strtotime($this->arrForm["reception_date1"]));
						$this->arrForm["smonth"] = date('m', strtotime($this->arrForm["reception_date1"]));
						$this->arrForm["sday"] = date('d', strtotime($this->arrForm["reception_date1"]));
						$this->arrForm["shour"] = date('H', strtotime($this->arrForm["reception_date1"]));
					}
					if($this->arrForm["reception_date2"] != "") {
						$this->arrForm["eyear"] = date('Y', strtotime($this->arrForm["reception_date2"]));
						$this->arrForm["emonth"] = date('m', strtotime($this->arrForm["reception_date2"]));
						$this->arrForm["eday"] = date('d', strtotime($this->arrForm["reception_date2"]));
						$this->arrForm["ehour"] = date('H', strtotime($this->arrForm["reception_date2"]));
					}

                    // システムメンテナンス
					if($this->arrForm["maintenance_date1"] != "") {
						$this->arrForm["msyear"]  = date('Y', strtotime($this->arrForm["maintenance_date1"]));
						$this->arrForm["msmonth"] = date('m', strtotime($this->arrForm["maintenance_date1"]));
						$this->arrForm["msday"]   = date('d', strtotime($this->arrForm["maintenance_date1"]));
						$this->arrForm["mshour"]  = date('H', strtotime($this->arrForm["maintenance_date1"]));
					}
					if($this->arrForm["maintenance_date2"] != "") {
						$this->arrForm["meyear"]  = date('Y', strtotime($this->arrForm["maintenance_date2"]));
						$this->arrForm["memonth"] = date('m', strtotime($this->arrForm["maintenance_date2"]));
						$this->arrForm["meday"]   = date('d', strtotime($this->arrForm["maintenance_date2"]));
						$this->arrForm["mehour"]  = date('H', strtotime($this->arrForm["maintenance_date2"]));
					}

                    // 一次受付期間終了日
					if($this->arrForm["primary_reception_date"] != "") {
						$this->arrForm["fst_syear"] = date('Y', strtotime($this->arrForm["primary_reception_date"]));
						$this->arrForm["fst_smonth"] = date('m', strtotime($this->arrForm["primary_reception_date"]));
						$this->arrForm["fst_sday"] = date('d', strtotime($this->arrForm["primary_reception_date"]));
						$this->arrForm["fst_shour"] = date('H', strtotime($this->arrForm["primary_reception_date"]));
					}

					// 二次受付期間開始日
					if($this->arrForm["secondary_reception_date"] != "") {
						$this->arrForm["fst_eyear"] = date('Y', strtotime($this->arrForm["secondary_reception_date"]));
						$this->arrForm["fst_emonth"] = date('m', strtotime($this->arrForm["secondary_reception_date"]));
						$this->arrForm["fst_eday"] = date('d', strtotime($this->arrForm["secondary_reception_date"]));
						$this->arrForm["fst_ehour"] = date('H', strtotime($this->arrForm["secondary_reception_date"]));
					}
					$this->arrForm["dst_head_image"] = $this->arrForm["head_image"];

					if($this->arrForm["group3"] != "") {
						list($this->arrForm["group3_1"], $this->arrForm["group3_2"], $this->arrForm["group3_3"]) = explode("|", $this->arrForm["group3"]);
					}

					$arrFormItem = $this->objForm->getListItem($this->arrForm["form_id"]);

					if($arrFormItem) {
						$this->arrForm = array_merge($this->arrForm, $arrFormItem);
					}



				}else{
					// 新規登録の場合は見出しをデフォルト@4/18
					$this->arrForm["ather_price_head0"] = $GLOBALS["regist"]["0"];
					$this->arrForm["ather_price_head1"] = $GLOBALS["regist"]["1"];
					$this->arrForm["price_head0"] 		= $GLOBALS["regist"]["0"];
					$this->arrForm["price_head1"] 		= $GLOBALS["regist"]["1"];
					$this->arrForm["auto_change_flg"]	= 0;
					$this->arrForm["disp"]	= 1;
				}

				if(!isset($this->arrForm["agree"]) || $this->arrForm["agree"] == "") $this->arrForm["agree"] = "0";
				if(!isset($this->arrForm["edit_flg"]) || $this->arrForm["edit_flg"] == "") $this->arrForm["edit_flg"] = "1";


				//MDK設定情報セッションクリア
				$GLOBALS["session"]->unsetVar("ss_merchant");
			 	break;


		}

		$this->init_field();
		$this->assign("arrErr", $this->arrErr);

		// 親クラスに処理を任せる
		parent::main();
	}

	function init() {

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("form_id", 		"フォームID", 				array(),		array(),				"n",	0,	0);
		$key[] = array("login_id",		"ログインID", 				array(0, 0),	array("NULL"),			"an",	1,	0);
		$key[] = array("password",		"パスワード", 				array(5, 12),	array("LEN"),			"an",	1,	0);
		$key[] = array("pass_chg_flg",	"パスワード変更機能の利用", 	array(),		array(),				"n",	1,	1);
		$key[] = array("form_name",		"フォーム名", 				array(0, 0),	array("NULL"),			"KV",	1,	0);
		$key[] = array("lang", 			"言語", 					array(),		array("SELECT"),		"n",	1,	1);
		$key[] = array("head", 			"フォーム頭文字", 			array(1,21),	array("NULL", "LEN"),	"a",	1,	0);
		$key[] = array("form_desc", 	"フォーム説明",			array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("form_mail", 	"メールアドレス",			array(0, 0),	array("NULL", "MAIL"),	"a",	1,	0);
		$key[] = array("syear", 		"受付期間開始日(年)",	array(0, 4),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("smonth", 		"受付期間開始日(月)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("sday", 			"受付期間開始日(日)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("shour", 		"受付期間開始日(時)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("eyear", 		"受付期間終了日(年)",	array(0, 4),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("emonth", 		"受付期間終了日(月)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("eday", 			"受付期間終了日(日)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("ehour", 		"受付期間終了日(時)",	array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("msyear", 		"システムメンテナンス開始日(年)",	array(0, 4),	array("LEN"),	"n",	0,	0);
		$key[] = array("msmonth", 		"システムメンテナンス開始日(月)",	array(0, 2),	array("LEN"),	"n",	0,	0);
		$key[] = array("msday", 		"システムメンテナンス開始日(日)",	array(0, 2),	array("LEN"),	"n",	0,	0);
		$key[] = array("mshour", 		"システムメンテナンス開始日(時)",	array(0, 2),	array("LEN"),	"n",	0,	0);
		$key[] = array("meyear", 		"システムメンテナンス終了日(年)",	array(0, 4),	array("LEN"),	"n",	0,	0);
		$key[] = array("memonth", 		"システムメンテナンス終了日(月)",	array(0, 2),	array("LEN"),	"n",	0,	0);
		$key[] = array("meday", 		"システムメンテナンス終了日(日)",	array(0, 2),	array("LEN"),	"n",	0,	0);
		$key[] = array("mehour", 		"システムメンテナンス終了日(時)",	array(0, 2),	array("LEN"),	"n",	0,	0);
		$key[] = array("type", 			"フォーム種別",			array(0, 1),	array("SELECT", "LEN"),	"n",	1,	1);
		$key[] = array("fst_syear", 	"一次受付期間終了日(年)",	array(0, 4),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_smonth", 	"一次受付期間終了日(月)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_sday", 		"一次受付期間終了日(日)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_shour", 	"一次受付期間終了日(時)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_eyear", 	"二次受付期間開始日(年)",	array(0, 4),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_emonth", 	"二次受付期間開始日(月)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_eday", 		"二次受付期間開始日(日)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("fst_ehour", 	"二次受付期間開始日(時)",	array(0, 2),	array("LEN"),			"n",	0,	0);
		$key[] = array("credit", 		"決済システム", 			array(),		array("SELECT"),		"n",	1,	0);
		$key[] = array("pricetype", 	"金額設定タイプ", 			array(),		array(),				"n",	1,	1);
		$key[] = array("price", 		"金額", 					array(),		array(),				"nKV",	1,	0);


		// 自動金額変更@追加
		$key[] = array("auto_change_flg",	"自動金額変更", 					array(),		array(),	"n",	1,	1);
		$key[] = array("price_header",			"金額：項目見出し",			array(),		array(),	"KV",	1,	0);
		$key[] = array("price_head0",	"金額：見出し(early)",					array(),		array(),	"KV",	1,	0);
		$key[] = array("price_head1",	"金額：見出し(late)",					array(),		array(),	"KV",	1,	0);
		$key[] = array("ather_price_header",	"その他決済：項目見出し",		array(),		array(),	"KV",	1,	0);
		$key[] = array("ather_price_head0",	"その他決済項目：見出し(early)",	array(),		array(),	"KV",	1,	0);
		$key[] = array("ather_price_head1",	"その他決済項目：見出し(late)",		array(),		array(),	"KV",	1,	0);
		$key[] = array("myear", 		"自動金額変更(年)",					array(0, 4),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("mmonth", 		"自動金額変更(月)",					array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("mday", 			"自動金額変更(日)",					array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("mhour", 		"自動金額変更(時)",					array(0, 2),	array("SELECT", "LEN"),	"n",	0,	0);
		$key[] = array("disp",	"表示形式",			 						array(),		array(),	"n",	1,	1);
		$key[] = array("auto_change_disp",	"自動金額表示形式", 				array(),		array(),	"n",	1,	1);
		$key[] = array("auto_change_price",	"自動金額変更:金額",				array(),		array(),	"nKV",	1,	0);
		$key[] = array("auto_change_ather_price",		"自動金額変更:その他決済項目",array(),		array(),	"nKV",	1,	0);
		$key[] = array("auto_change_price_head0",		"自動金額変更:金額：見出し(early)",			array(),		array(),	"KV",	1,	0);
		$key[] = array("auto_change_price_head1",		"自動金額変更:金額：見出し(late)",				array(),		array(),	"KV",	1,	0);
		$key[] = array("auto_change_ather_price_head0",	"自動金額変更:その他決済項目：見出し(early)",	array(),		array(),	"KV",	1,	0);
		$key[] = array("auto_change_ather_price_head1",	"自動金額変更:その他決済項目：見出し(late)",		array(),		array(),	"KV",	1,	0);

		$key[] = array("csv_price_head0",	"CSV用:金額：見出し(early)",					array(),	array(),	"KV",	1,	0);
//		$key[] = array("csv_price_head1",	"CSV用:金額：見出し(late)",					array(),	array(),	"KV",	1,	0);
		$key[] = array("csv_ather_price_head0",	"CSV用:その他決済項目：見出し(early)",		array(),	array(),	"KV",	1,	0);
//		$key[] = array("csv_ather_price_head1",	"CSV用:その他決済項目：見出し(late)",		array(),	array(),	"KV",	1,	0);
		$key[] = array("csv_auto_change_price_head0",		"CSV用:自動金額変更:金額：見出し(early)",			array(),	array(),	"KV",	1,	0);
//		$key[] = array("csv_auto_change_price_head1",		"CSV用:自動金額変更:金額：見出し(late)",			array(),	array(),	"KV",	1,	0);
		$key[] = array("csv_auto_change_ather_price_head0",	"CSV用:自動金額変更:その他決済項目：見出し(early)",	array(),	array(),	"KV",	1,	0);
//		$key[] = array("csv_auto_change_ather_price_head1",	"CSV用:自動金額変更:その他決済項目：見出し(late)",	array(),	array(),	"KV",	1,	0);


		$key[] = array("edit_flg", 		"編集許可", 				array(),		array(),				"n",	1,	1);
		$key[] = array("contact", 		"お問い合せ先", 			array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("dst_head_image","ヘッダ画像", 				array(0, 0),	array(),				"a",	0,	0);
		$key[] = array("temp_image", 	"アップロード画像", 			array(0, 0),	array(),				"a",	0,	0);
		$key[] = array("image_del", 	"画像削除", 				array(0, 0),	array(),				"n",	0,	0);
		$key[] = array("agree", 		"個人情報の確認", 		array(0, 0),	array(),				"n",	1,	1);
		$key[] = array("agree_text", 	"個人情報の本文", 		array(0, 0),	array(),				"KV",	1,	0);

		$key[] = array("agree_type", 	"Privacy Policy認証", 			array(0, 0),	array(),	"n",	1,	1);
		$key[] = array("agree_chktext", "Privacy Policy認証：テキスト", array(0, 0),	array(),	"KV",	1,	0);

		$key[] = array("group1", 		"グループ項目名1", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group2", 		"グループ項目名2", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group3", 		"グループ項目名3", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group3_1", 		"グループ項目名3_1", 		array(0, 0),	array(),				"KV",	0,	0);
		$key[] = array("group3_2", 		"グループ項目名3_2", 		array(0, 0),	array(),				"KV",	0,	0);
		$key[] = array("group3_3", 		"グループ項目名3_3", 		array(0, 0),	array(),				"KV",	0,	0);
		$key[] = array("group4", 		"グループ項目名4", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group5", 		"グループ項目名5", 		array(0, 0),	array(),				"KV",	1,	0);
		$key[] = array("group2_use", 	"グループ項目2利用フラグ", 	array(0, 0),	array(),				"n",	1,	1);
		$key[] = array("group3_use", 	"グループ項目3利用フラグ", 	array(0, 0),	array(),				"n",	1,	1);
		$key[] = array("user_id", 		"ユーザID", 				array(0, 0),	array("NULL"),			"n",	1,	0);
		$key[] = array("ather_price_flg", "その他決済項目利用フラグ", array(),		array(),				"n",	1,	1);

		$key[] = array("ather_price", 	"その他決済項目", 		array(),		array(),				"nKV",	1,	0);
		$key[] = array("group3_layout", "グループ項目3　任意項目レイアウト", 	array(),	array(),				"n",	1,	1);

        // 追加項目#Mng CSV-Basic
		$key[] = array("use_csvbasic", 	"フォーム管理者　CSVDL-Basic認証設定",	array(),	array("SELECT"),	"n",	1,	0);



		$key[] = array("temp", 			"一時保存", 				array(0, 0),	array(),				"n",	0,	1);

		return $key;

	}

	function init2() {

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("item_name", 		"項目名", 			array(),		array(),	"KV",	1,	0);
		$key[] = array("item_view", 		"非表示フラグ", 		array(),		array(),	"KV",	1,	0);
		$key[] = array("item_check", 		"チェックタイプ", 		array(),		array(),	"KV",	1,	0);
		$key[] = array("item_len", 			"制限サイズ", 		array(),		array(),	"KV",	1,	0);
		$key[] = array("item_pview", 		"一次非表示フラグ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_mail", 		"メール非表示フラグ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_type", 		"コントロールタイプ", 	array(),		array(),	"KV",	1,	0);
		$key[] = array("item_select",		"選択肢項目", 		array(),		array(),	"KV",	1,	0);
		$key[] = array("item_memo",			"備考欄", 			array(),		array(),	"KV",	1,	0);

		return $key;

	}

	function init_field() {

		if(!isset($this->arrForm["type"]) || !isset($this->arrForm["lang"])) return;
		if(!$this->arrForm["type"] > 0) return;
		if(!$this->arrForm["lang"] > 0) return;

		$arrIni = $this->objItemIni->getList($this->arrForm["type"], $this->arrForm["lang"]);

		if(!$arrIni) return;

		switch($this->arrForm["type"]) {
			case "1":
			case "2":
			case "3":
				if($this->arrForm["lang"] == "1") {
					$group[1]["title"] = $this->arrForm["group1"] != "" ? $this->arrForm["group1"] : "筆頭著者";
					$group[2]["title"] = $this->arrForm["group2"] != "" ? $this->arrForm["group2"] : "共著者";
					$group[3]["title"] = $this->arrForm["group3"] != "" ? $this->arrForm["group3"] : "抄録";
				} else {
					$group[1]["title"] = $this->arrForm["group1"] != "" ? $this->arrForm["group1"] : "筆頭著者";
					$group[2]["title"] = $this->arrForm["group2"] != "" ? $this->arrForm["group2"] : "共著者";
					$group[3]["title"] = $this->arrForm["group3"] != "" ? $this->arrForm["group3"] : "抄録";
				}
				break;

			default:
				break;
		}

		for($i = 1; $i <= $this->objItemIni->cnt; $i++) {
			if(!isset($this->arrForm["item_check"][$i])) $this->arrForm["item_check"][$i] = array();
			$forms["item_check"][$i] = SmartyForm::createRadioChecked("item_check[$i]", $GLOBALS["checktypeList"], $this->arrForm["item_check"][$i], "checkbox", "form", 4);
		}

		$this->assign("forms", $forms);
		$this->assign("group", $group);
		$this->assign("field", $arrIni);


	}

	function check() {

		$objErr = new Validate;

		$objErr->check($this->init(), $this->arrForm);


		//-----------------------------------------------------------------------------------
		// 自動金額を必須項目から外す@追加->4/11
		if($this->arrForm["myear"] == "" && $this->arrForm["mmonth"] == "" && $this->arrForm["mday"] == "") {
			unset($objErr->_err['myear']);
			unset($objErr->_err['mmonth']);
			unset($objErr->_err['mday']);
			unset($objErr->_err['mhour']);
			unset($objErr->_err['auto_change_price']);
			unset($objErr->_err['auto_change_ather_price']);
		}
		// 日付チェック
		$this->arrForm["auto_change_day"] = "";
		if($this->arrForm["myear"] > 0 && $this->arrForm["mmonth"] > 0 && $this->arrForm["mday"] > 0) {
			$this->arrForm["mhour"] = $this->arrForm["mhour"] != "" ? $this->arrForm["mhour"] : "00";
			// 自動金額変更日の整形
			$this->arrForm["auto_change_day"] = $this->arrForm["myear"]."-".$this->arrForm["mmonth"]."-".$this->arrForm["mday"]." ".$this->arrForm["mhour"].":00:00";
			if(!$objErr->isDate($this->arrForm["auto_change_day"])) {
				$objErr->addErr("自動金額変更日を確認して下さい。", "auto_change_day");
			}else{
				// 正しい日付ならDB登録用に整形
				$this->arrForm["auto_change_day"] = $this->arrForm["myear"].$this->arrForm["mmonth"].$this->arrForm["mday"].$this->arrForm["mhour"];
			}
		}

		// 表示形式チェック -> 利用する場合
		if($this->arrForm["auto_change_flg"] != 0){
			//　日付、金額が選択されていない

			if($this->arrForm["auto_change_disp"] == ""){
				$objErr->addErr("自動金額:表示形式を選択して下さい。", "auto_change_disp");
			}

			if($this->arrForm["auto_change_day"] == ""){
				$objErr->addErr("自動金額変更日を確認して下さい。", "auto_change_day");
			}
			if($this->arrForm["auto_change_price"] == "" && $this->arrForm["auto_change_ather_price"] == "" ){
				$objErr->addErr("自動金額変更:金額またはその他決済項目を入力して下さい。", "auto_change");
			}
		}
		//--------------------------------------------------------------------------------ここまで




		//パスワードチェック
		if($this->arrForm["form_id"] == "" && $this->arrForm["password"] == "") {
			$objErr->addErr("パスワードが入力されていません。", "password");
		}

		//ログインIDの重複チェック
		if($this->arrForm["login_id"] != "" && !$this->objForm->checkLoginId($this->arrForm["form_id"], $this->arrForm["login_id"])) {
			$objErr->addErr("ログインIDは他のフォームで登録されています。", "login_id");
		}


		//フォーム頭文字の重複チェック
		/*
		if($this->arrForm["head"] != ""){
			if(!$this->objForm->checkFormHead($this->arrForm["form_id"], $this->arrForm["head"])){
				$objErr->addErr("フォーム頭文字は他のフォームで登録されています。", "head");
			}
		}
		*/
		// フォーム頭文字の文字制限チェック（半角英数と「-」のみ許可）
		if(!$objErr->isAlphaNumericMarkCustom($this->arrForm["head"], array("-"))){
			$objErr->addErr("フォーム頭文字は「半角英数とハイフン(-)」で入力して下さい。", "head");
		}


        //--------------------------------
        // 受付期間の日付チェック
        //--------------------------------
        // 受付期間 開始
        $key  = "reception_date1";
        $keys = array("syear", "smonth", "sday", "shour");
        if(!$this->checkDate($objErr, $keys, $this->arrForm, $key)){
            $objErr->addErr("受付期間開始日を確認して下さい。", $key);
        }
        // 受付期間 終了
        $key  = "reception_date2";
        $keys = array("eyear", "emonth", "eday", "ehour");
        if(!$this->checkDate($objErr, $keys, $this->arrForm, $key)){
            $objErr->addErr("受付期間終了日を確認して下さい。", $key);
        }
        // 未来の日付かチェック
        $key1 = "reception_date1";
        $key2 = "reception_date2";
        if($this->arrForm[$key1] != "" && $this->arrForm[$key2] != "") {
            if(!$objErr->isDateAgo($this->arrForm[$key1], $this->arrForm[$key2])) {
                $objErr->addErr("受付期間終了日は、開始日よりも未来に設定して下さい。", $key1);
            }
        }


        //--------------------------------
        // システムメンテナンスの日付チェック
        //--------------------------------
        // システムメンテナンス 開始
        $key  = "maintenance_date1";
        $keys = array("msyear", "msmonth", "msday", "mshour");
        $this->arrForm[$key] = NULL;
        if(!$this->checkDate($objErr, $keys, $this->arrForm, $key)){
            $objErr->addErr("システムメンテナンス開始日を確認して下さい。", $key);
        }
        // システムメンテナンス 終了
        $key  = "maintenance_date2";
        $keys = array("meyear", "memonth", "meday", "mehour");
        $this->arrForm[$key] = NULL;
        if(!$this->checkDate($objErr, $keys, $this->arrForm, $key)){
            $objErr->addErr("システムメンテナンス終了日を確認して下さい。", $key);
        }
        // 未来の日付かチェック
        $key1 = "maintenance_date1";
        $key2 = "maintenance_date2";
        if($this->arrForm[$key1] != "" && $this->arrForm[$key2] != "") {
            if(!$objErr->isDateAgo($this->arrForm[$key1], $this->arrForm[$key2])) {
                $objErr->addErr("システムメンテナンス終了日は、開始日よりも未来に設定して下さい。", $key1);
            }
        }


		//　一次受付期間
		switch($this->arrForm["type"]) {
			case "3":
				if(!isset($this->arrForm["credit"])) {
					$objErr->addErr("決済システムを選択して下さい。", "credit");
				} else {
					switch($this->arrForm["credit"]) {
						// 決済システムを利用する
						case "1":
						case "2":
						    // オンライン決済のみ
						    if($this->arrForm["credit"] == 2){
    							if($_REQUEST["pgcard_shop_id"] == ""){
    								$objErr->addErr("ショップIDを入力してください。", "pgcard_shop_id");
    							}
    							else{
    								if(!$objErr->isAlphaNumeric($_REQUEST["pgcard_shop_id"])){
    									$objErr->addErr("ショップIDが正しい値か、確認してください。", "pgcard_shop_id");
    								}
    							}

    							if($_REQUEST["pgcard_shop_pass"] == ""){
    								$objErr->addErr("ショップパスワードを入力してください。", "pgcard_shop_pass");
    							}
    							else{
    								if(!$objErr->isAlphaNumeric($_REQUEST["pgcard_shop_pass"])){
    									$objErr->addErr("ショップパスワードが正しい値か、確認してください。", "pgcard_shop_pass");
    								}
    							}

    							if($_REQUEST["pgcard_site_id"] == ""){
    								$objErr->addErr("サイトIDを入力してください。", "pgcard_site_id");
    							}
    							else{
    								if(!$objErr->isAlphaNumeric($_REQUEST["pgcard_site_id"])){
    									$objErr->addErr("サイトIDが正しい値か、確認してください。", "pgcard_site_id");
    								}
    							}

    							if($_REQUEST["pgcard_site_pass"] == ""){
    								$objErr->addErr("サイトパスワードを入力してください。", "pgcard_site_pass");
    							}
    							else{
    								if(!$objErr->isAlphaNumeric($_REQUEST["pgcard_site_pass"])){
    									$objErr->addErr("サイトパスワードが正しい値か、確認してください。", "pgcard_site_pass");
    								}
    							}

    							if(!isset($_REQUEST["pgcard_use3ds"])|| $_REQUEST["pgcard_use3ds"] == ""){
    								$objErr->addErr("3Dセキュアの利用を選択してください。", "pgcard_use3ds");
    							}
    							else{
    								if(!$objErr->isAlphaNumeric($_REQUEST["pgcard_use3ds"])){
    									$objErr->addErr("3Dセキュアが正しい値か、確認してください。", "pgcard_use3ds");

                                    // 店舗名チェック
    								}elseif($_REQUEST["pgcard_use3ds"] == 1 && strlen($this->arrForm["pgcard_3ds_shopname"]) == 0){
    									$objErr->addErr("3Dセキュア利用時のショップ名を入力してください。", "pgcard_use3ds");
    								}

                                    // 店舗名の長さチェック
    								if($_REQUEST["pgcard_use3ds"] == 1 && strlen($this->arrForm["pgcard_3ds_shopname"]) > 0){
                                        $base64 = base64_encode($this->arrForm["pgcard_3ds_shopname"]);
                                        if(strlen($base64) > 25){
        									$objErr->addErr("3Dセキュア利用時のショップ名が長いため許可できません。25以下になるようにしてください。[".strlen($base64)."]", "pgcard_use3ds");
                                        }
    								}
    							}
						    }

							if(!isset($this->arrForm["pricetype"]) || $this->arrForm["pricetype"] == "") {
								$objErr->addErr("金額設定タイプを選択して下さい。", "pricetype");
								break;
                            }

                            if(!isset($this->arrForm["disp"]) || strlen($this->arrForm["disp"]) == 0){
								$objErr->addErr("表示形式を選択して下さい。", "pricetype");
                                break;
                            }

							if($this->arrForm["price"] == "") {
								$objErr->addErr("金額を設定して下さい。", "pricetype");
								break;
							}

							$this->arrForm["price"] = trim($this->arrForm["price"]);
							$this->arrForm["price"] = str_replace("　", " ", $this->arrForm["price"]);
							$this->arrForm["price"] = str_replace("  ", " ", $this->arrForm["price"]);

							$disp = $this->arrForm['disp'];
							// 一律タイプ
							if($this->arrForm["pricetype"] == "1") {

								//入力チェック
								$this->evenlyType("price", $objErr, "", $disp);
								if($this->arrForm["price_head0"] == "") {
									$objErr->addErr("金額:見出し(early)を入力して下さい。", "price_head0");
								}
								if($this->arrForm["csv_price_head0"] == "") {
									$objErr->addErr("CSV用：金額:見出し(early)を入力して下さい。", "csv_price_head0");
								}

								if($this->arrForm["price_head1"] == "") {
									$objErr->addErr("金額:見出し(late)を入力して下さい。", "price_head1");
								}
//									if($this->arrForm["csv_price_head1"] == "") {
//										$objErr->addErr("CSV用：金額:見出し(late)を入力して下さい。", "csv_price_head1");
//									}

							// 選択タイプ
							} else {

								$this->changeType("price", $objErr, "", $disp);
								if($this->arrForm["price_head0"] == "") {
									$objErr->addErr("金額:見出し(early)を入力して下さい。", "price_head0");
								}
								if($this->arrForm["csv_price_head0"] == "") {
									$objErr->addErr("CSV用：金額:見出し(early)を入力して下さい。", "csv_price_head0");
								}

								if($this->arrForm["price_head1"] == "") {
									$objErr->addErr("金額:見出し(late)を入力して下さい。", "price_head1");
								}
//									if($this->arrForm["csv_price_head1"] == "") {
//										$objErr->addErr("CSV用：金額:見出し(late)を入力して下さい。", "csv_price_head1");
//									}
							}

							//-------------------------------------------------------
							// 金額自動更新@追加->4/11 : 金額が入力されている
							//-------------------------------------------------------
							if($this->arrForm["auto_change_price"] != ""){
								if($this->arrForm["auto_change_price_head0"] == "") {
									$objErr->addErr("自動金額変更:金額:見出し(early)を入力して下さい。", "auto_change_price_head0");
								}
								if($this->arrForm["csv_auto_change_price_head0"] == "") {
									$objErr->addErr("CSV用：自動金額変更:金額:見出し(early)を入力して下さい。", "csv_auto_change_price_head0");
								}

								if($this->arrForm["auto_change_price_head1"] == "") {
									$objErr->addErr("自動金額変更:金額:見出し(late)を入力して下さい。", "auto_change_price_head1");
								}
//								if($this->arrForm["csv_auto_change_price_head1"] == "") {
//									$objErr->addErr("CSV用：自動金額変更:金額:見出し(late)を入力して下さい。", "csv_auto_change_price_head1");
//								}
								// 入力チェック
								$disp = $this->arrForm['auto_change_disp'];
								$changeType = $this->changeType("auto_change_price", $objErr, "自動金額変更:", $disp);
								$evenlyType = $this->evenlyType("auto_change_price", $objErr, "自動金額変更:", $disp);

								// 一律、選択タイプのどちらもフォーマットエラー
								if($evenlyType === 0 && $changeType === 0){
									unset($objErr->_err['auto_change_price']);
									// 改行の有無でタイプを判別
									if(strstr($this->arrForm["auto_change_price"], "\n")){
										$changeType = $this->changeType("auto_change_price", $objErr, "自動金額変更:", $disp);
									}else{
										$evenlyType = $this->evenlyType("auto_change_price", $objErr, "自動金額変更:", $disp);
									}

								// 一律、選択タイプのどちらかがフォーマットok
								}elseif($evenlyType === 0 || $changeType === 0){
									unset($objErr->_err['auto_change_price']);
								}
							}

							//-------------------------------------------------------
							// 金額自動更新@追加->4/11
//							if($this->arrForm["ather_price_flg"] != "0") {
//								//その他決済を利用しないのに金額自動変更で入力している
//								if($this->arrForm["auto_change_ather_price"] != ""){
//									$objErr->addErr("自動金額変更：その他決済項目を利用する場合は、内容を入力して下さい。", "auto_change_ather_price");
//									break;
//								}
//							}
							//　その他決済を利用しなくても自動変更で追加する場合もあるので判定文の外
							if($this->arrForm['auto_change_ather_price'] != ""){
								if($this->arrForm["auto_change_ather_price_head0"] == "") {
									$objErr->addErr("自動金額変更:その他決済項目:見出し(early)を入力して下さい。", "auto_change_ather_price_head0");
								}

								if($this->arrForm["auto_change_ather_price_head1"] == "") {
									$objErr->addErr("自動金額変更:その他決済項目:見出し(late)を入力して下さい。", "auto_change_ather_price_head1");
								}
//								if($this->arrForm["csv_auto_change_ather_price_head0"] == "") {
//									$objErr->addErr("CSV用：自動金額変更:その他決済項目:見出し(early)を入力して下さい。", "csv_auto_change_ather_price_head0");
//								}
//								if($this->arrForm["csv_auto_change_ather_price_head1"] == "") {
//									$objErr->addErr("CSV用：自動金額変更:その他決済項目:見出し(late)を入力して下さい。", "csv_auto_change_ather_price_head1");
//								}
								//その他決済項目のエラーチェック
								$this->otherCheck("auto_change_ather_price", $objErr, "自動金額変更:", $this->arrForm['auto_change_disp']);
							}
							//------------------------------------------------------------


							// その他決済項目を利用
							if($this->arrForm["ather_price_flg"] == "0") {

								//その他決済項目のエラーチェック
								$this->otherCheck("ather_price", $objErr, "", $this->arrForm['disp']);
								if($this->arrForm["ather_price_head0"] == "") {
									$objErr->addErr("その他決済項目:見出し(early)を入力して下さい。", "ather_price_head0");
								}
//								if($this->arrForm["csv_ather_price_head0"] == "") {
//									$objErr->addErr("CSV用：その他決済項目:見出し(early)を入力して下さい。", "csv_ather_price_head0");
//								}

								if($this->arrForm["ather_price_head1"] == "") {
									$objErr->addErr("その他決済項目:見出し(late)を入力して下さい。", "ather_price_head1");
								}
//								if($this->arrForm["csv_ather_price_head1"] == "") {
//									$objErr->addErr("CSV用：その他決済項目:見出し(late)を入力して下さい。", "csv_ather_price_head1");
//								}
							}

							break;
						default:
							//決済システムを利用しない
							break;
					}



				}
				break;
		}


		$this->arrErr = $objErr->_err;
	}

	/**
	 * 一律タイプのエラーチェック*/
	function evenlyType($key, &$objErr, $auto, $disp){

		$price = explode(" ", $this->arrForm[$key]);

		// 表示形式がearlyのみ
		if($disp == 1){
			if($price[1] != "non"){
				$objErr->addErr($auto."金額のlateの部分はnonを入力して下さい。", $key);
				return 0;
			}
		}

        // 金額にFreeを許可
		if(!$objErr->isNumeric($price[0])) {
            if($price[0] != "Free"){
    			$objErr->addErr($auto."金額を半角数字で設定して下さい。", $key);
    			return 0;
            }
		}
		// 0円を許可
//		if(!$price[0] > 0) {
//			$objErr->addErr($auto."金額は1以上を設定して下さい。", $key);
//			return 0;
//		}
		if(!isset($price[1]) || $price[1] == "") {
            if($price[1] != "-" && $price[1] != "Free"){
    			$objErr->addErr($auto."当日登録料金を設定しない場合は、0を入力して下さい。", $key);
    			return 0;
            }
		}
		// 自動金額変更:条件追加
		if(!$objErr->isNumeric($price[1]) && $disp == 2) {
            if($price[1] != "-" && $price[1] != "Free"){
    			$objErr->addErr($auto."当日登録料金を半角数字で設定して下さい。", $key);
    			return 0;
            }
		}
		$this->arrForm[$key] = $price[0]." ".$price[1];
	}
	/**
	 * 選択タイプのエラーチェック*/
	function changeType($key, &$objErr, $auto, $disp){

		$pricelist = explode("\n", $this->arrForm[$key]);

		if(count($pricelist) < 2) {
			$objErr->addErr("選択タイプの金額は改行して複数登録して下さい。", $key);
			return 0;
		}

		foreach($pricelist as $val) {
			$list = explode(" ", trim($val));

			foreach($list as $k=>$v) {
				$list[$k] = str_replace("\t", "", trim($v));
			}

			// 表示形式がearlyのみ
			if($disp == 1){
				if($list[2] != "non"){
					$objErr->addErr($auto."金額のlateの部分はnonを入力して下さい。", $key);
					return 0;
				}
			}

			if(!count($list) == 3) {
				$objErr->addErr($auto."金額の選択タイプは「項目」スペース「事前登録料金」スペース「当日登録料金」の形式で入力して下さい。", $key);
				return 0;
			}
			if(!$objErr->isNumeric(trim($list[1]))) {
                if(trim($list[1]) != "Free"){
    				$objErr->addErr($auto."選択タイプの金額は半角数字で入力して下さい。", $key);
    				return 0;
                }
			}
			// 自動金額変更:条件追加@表示項目early+lateの場合
			if((!isset($list[2]) || trim($list[2]) == "") && $disp == 2) {
                if(trim($list[2]) != "Free"){
    				$objErr->addErr($auto."選択タイプの当日登録料金を設定しない場合は、0を入力して下さい。", $key);
    				return 0;
                }
			}
			// 自動金額変更:条件追加@表示項目early+lateの場合
			if(!$objErr->isNumeric(trim($list[2])) && $disp == 2) {
                if(trim($list[2]) != "-" && trim($list[2]) != "Free"){
    				$objErr->addErr($auto."選択タイプの当日登録料金は半角数字で入力して下さい。", $key);
    				return 0;
                }
			}

			if(!$objErr->isNumeric(trim($list[3]))) {
				$objErr->addErr($auto."アップロードファイル連動フラグは半角数字で入力して下さい。", $key);
				return 0;
			}
			$retprice[] = trim($list[0])." ".trim($list[1])." ".trim($list[2])." ".trim($list[3]);
		}
		$this->arrForm[$key] = implode("\n", $retprice);
		unset($retprice);
	}
	/**
	 * その他決済エラーチェック*/
	function otherCheck($key, &$objErr, $auto, $disp){

		if($this->arrForm[$key] == "") {
			$objErr->addErr("その他決済項目を利用する場合は、内容を入力して下さい。", $key);
			return;
		}

		$this->arrForm[$key] = str_replace("　"," ", trim($this->arrForm[$key]));
		$this->arrForm[$key] = str_replace("  "," ", trim($this->arrForm[$key]));

		$pricelist = explode("\n", $this->arrForm[$key]);
		foreach($pricelist as $_key => $val) {

			if($val == "") continue;

			$list = explode(" ", $val);
			foreach($list as $k=>$v) {
				$list[$k] = str_replace("\t", "", trim($v));
			}

			// 表示形式がearlyのみ
			if($disp == 1){
				if($list[2] != "non"){
					$objErr->addErr($auto."金額のlateの部分はnonを入力して下さい。", $key);
					return;
				}
			}

			if(count($list) != "6") {
				$objErr->addErr($auto."その他決済項目のフォーマットを確認して下さい。当日登録料金、制限数、コメントを利用しない場合は0を入力して下さい。", $key);
				return;
			}

            //--------------------------------
            // 前期
            //--------------------------------
			// 0円とFreeを許可
			if(!isset($list[1]) || !$objErr->isNumeric($list[1])) {
                if($list[1] != "Free"){
                	$objErr->addErr($auto."その他決済項目の事前登録料金を入力して下さい。", $key);
		    		return;
	            }
			}

            //--------------------------------
            // 後期
            //--------------------------------
			// 自動金額変更:条件追加@表示項目early+lateの場合
			if((!isset($list[2]) || !$objErr->isNumeric($list[2])) && $disp == 2) {
				if($list[2] != "-" && $list[2] != "Free"){
					$objErr->addErr($auto."その他決済項目の当日登録料金を入力して下さい。利用しない場合は0を入力して下さい。", $key);
				    return;
				}
			}

			if(!isset($list[4]) || !$objErr->isNumeric($list[4]) || ($objErr->isNumeric($list[4]) && !$list[4] > 0)) {
				$objErr->addErr($auto."その他決済項目の制限数は1以上の数値で入力して下さい。", $key);
				return;
			}

			$retprice[] = implode(" ", $list);
			unset($list);

		}

		$this->arrForm[$key] = implode("\n", $retprice);
		unset($retprice);
	}


	function registFormAdmin(&$db) {

		$param["form_id"] = $this->arrForm["form_id"];
		$param["super_flg"] = "1";
		$param["login_id"] = $this->arrForm["login_id"];
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		} else {

			$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);

			$rs = $db->_getOne("password", "form", $where, __FILE__, __LINE__);

			if(!$rs) {
				$db->rollback();
				$this->complete("フォーム管理者の登録に失敗しました。");
			}

			$param["password"] = $rs;


		}

		$rs = $db->insert("form_admin", $param, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			$this->complete("フォーム管理者の登録に失敗しました。");
		}

		return;

	}


	function check_item() {

		foreach($this->arrForm["item_name"] as $key=>$val) {

			$len = 0;
			$textformat = 0;

			if(isset($this->arrForm["item_check"][$key])) {
				foreach($this->arrForm["item_check"][$key] as $ck) {
					switch($ck) {
						case "1":
						case "2":
						case "3":
							$len++;
							break;
						case "4":
						case "5":
							$textformat++;
							break;
						default:
							break;
					}
				}

				if($len > 1) {
					$this->arrErr["item_check"][$key] = "長さのチェックはバイト・文字数・単語のうち、いずれか1つ選択して下さい。";
				}
				if($textformat > 1) {
					$this->arrErr["item_check"][$key] = "半角・全角のチェックはいずれか１つ選択して下さい。";
				}
				if($len == 1) {
					if($this->arrForm["item_len"][$key] == "" || $this->arrForm["item_len"][$key] == "0") {
						$this->arrErr["item_len"][$key] = "長さ(ファイル容量)のチェックをする場合は、サイズを指定して下さい。";
					}
				}

			}

			if(isset($this->arrForm["item_type"][$key])) {

				if($this->arrForm["item_type"][$key] == 2 || $this->arrForm["item_type"][$key] == 3 || $this->arrForm["item_type"][$key] == 4) {

					if($this->arrForm["item_select"][$key] == "") {

						$this->arrErr["item_select"][$key] = "選択肢を入力して下さい。";
					}
				}
			}

            if(in_array($key, array(51,52,53))){
                $ext = preg_replace('/[\s　]+/u', '', $this->arrForm["item_select"][$key]);
                if(strlen($ext) == 0) continue;

                if(!Validate::isAlphaNumeric($ext)){
						$this->arrErr["item_select"][$key] = "拡張子は半角英数で入力して下さい。[". $ext."]";
                }
            }
		}
	}


    function checkDate(&$objErr, $keys, &$arrForm, $key, $mode="start"){
        $isOk = true;
        if($objErr->isInputwhich($arrForm, $keys)){
            // 年月日入力
            if($isOk = $objErr->isInputAll($arrForm, $keys)){
                $arrForm[$key] = $this->createDate($arrForm, $keys, $mode);
                $isOk = $objErr->isDate($arrForm[$key]);
            }
        }
        return $isOk;
    }

    // 入力値[ymdh]をハイフンで連結
    function createDate($arrForm, $argkeys, $mode="start"){
        $keys = new stdClass();
        $keys->y = $argkeys[0];
        $keys->m = $argkeys[1];
        $keys->d = $argkeys[2];
        $keys->h = $argkeys[3];

        $is = ($mode == "start") ? ":00:00" : ":59:59";
        return $arrForm[$keys->y]."-".$arrForm[$keys->m]."-".$arrForm[$keys->d]." ".$arrForm[$keys->h].$is;
    }



	// DB新規登録　$temp（一時保存の場合1）　$item(フォーム項目の更新も行う場合1)
	function insert($temp=0, $item=0) {

		$db = new DbGeneral;

		$db->begin();

		foreach($this->init() as $val) {

			if(!isset($this->arrForm[$val[0]])) $this->arrForm[$val[0]] = "";

			if($val[5] == "1") {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
			if($val[6] == "1") {
				if(empty($param[$val[0]]) || $param[$val[0]] == "") $param[$val[0]] = "0";
			}
		}

		$param["group3"] = $this->arrForm["group3_1"]."|".$this->arrForm["group3_2"]."|".$this->arrForm["group3_3"];

		if($temp == "0") {
			$param["temp"] = "0";
		} else {
			$param["temp"] = "1";
		}

        // 受付期間
		if($this->arrForm["reception_date1"] != "" && $this->arrForm["reception_date2"] != "") {
			$param["reception_date1"] = $this->arrForm["reception_date1"];
			$param["reception_date2"] = $this->arrForm["reception_date2"];
		}
        // システムメンテナンス
		$param["maintenance_date1"] = NULL;
		$param["maintenance_date2"] = NULL;
		if($this->arrForm["maintenance_date1"] != ""){
			$param["maintenance_date1"] = $this->arrForm["maintenance_date1"];
		}
		if($this->arrForm["maintenance_date2"] != ""){
			$param["maintenance_date2"] = $this->arrForm["maintenance_date2"];
		}

		if($this->arrForm["type"] == "1") {
			if($this->arrForm["primary_reception_date"] != "" && $this->arrForm["secondary_reception_date"] != "") {
				$param["primary_reception_date"] = $this->arrForm["primary_reception_date"];
				$param["secondary_reception_date"] = $this->arrForm["secondary_reception_date"];
			}
		}

		$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);

		if($this->arrForm["temp_image"] != "") {
			if($this->arrForm["image_del"] == "1") {
				if(file_exists(IMG_PATH."header/".$this->arrForm["temp_image"])) {
					unlink(IMG_PATH."header/".$this->arrForm["temp_image"]);
				}
			} else {
				$param["head_image"] = $this->arrForm["temp_image"];
			}
		}

		//-------------------------------------
		//決済　マーチャント情報
		//-------------------------------------
		if($this->arrForm["type"] == "3" && $this->arrForm["credit"] != "0"){

			//---------------------------------
			//MDK設定情報 をセッションから復元
			//---------------------------------
			$this->_restore_merchant();
			$param["pgcard_shop_id"]   = $this->arrForm["pgcard_shop_id"];
			$param["pgcard_shop_pass"] = $this->arrForm["pgcard_shop_pass"];
			$param["pgcard_site_id"]   = $this->arrForm["pgcard_site_id"];
			$param["pgcard_site_pass"] = $this->arrForm["pgcard_site_pass"];
			$param["pgcard_use3ds"]    = $this->arrForm["pgcard_use3ds"];
			$param["pgcard_3ds_shopname"] = $this->arrForm["pgcard_3ds_shopname"];
		}

		//-------------------------------------
		//自動金額変更
		//-------------------------------------
		if($this->arrForm['auto_change_flg'] == 1){
			$param["auto_change_day"] = $this->arrForm["auto_change_day"];
		}


		$rs = $db->insert("form", $param, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			if($temp == "0") {
				$this->complete("フォーム情報の登録に失敗しました。");
			} else {
				$this->complete("フォーム情報の一次保存に失敗しました。");
			}
		}

		$this->arrForm["form_id"] = $db->_getOne("max(form_id)", "form", "del_flg = 0", __FILE__, __LINE__);
		if(!$this->arrForm["form_id"]) {
			$db->rollback();
			$this->complete("フォームIDの取得に失敗しました。");
		}

		if($item == "1") {

			$rs = $this->setitem($db, $temp);

			if(!$rs) {
				$db->rollback();
				if($temp == "0") {
					$this->complete("フォーム情報の登録に失敗しました。");
				} else {
					$this->complete("フォーム情報の一次保存に失敗しました。");
				}
			}
		}

        // 採番テーブル作成
		$this->objEntry->registEntryNumber($db, $this->arrForm["form_id"]);
		// フォーム管理者テーブルへの追加
		$this->registFormAdmin($db);

		//-------------------------------------------------
		//決済モジュールの複製
		//--------------------------------------------------
		if($this->arrForm["type"] == "3" && $this->arrForm["credit"] != "0"){

			//オリジナルパッケージをコピー
			$cmd = "/usr/bin/php ".ROOT_DIR."Sys/form/Sys_tgk.php ".$this->arrForm["form_id"]." > /dev/null &";
			system($cmd);

		}



		$db->commit();
		if($temp == "0") {
			$this->complete("フォーム情報を登録しました。");
		} else {
			$this->complete("フォーム情報を一次保存しました。");
		}

	}



	// フォーム項目の更新を行う。
	function setitem(&$db, $temp) {

		foreach($this->arrForm["item_name"] as $key=>$val) {

			$check = $this->checkItem($db, $this->arrForm["form_id"], $key);

			$param["item_name"] = $val;
			$param["item_view"] = isset($this->arrForm["item_view"][$key]) ? "1" : "0";
			$param["item_check"] = isset($this->arrForm["item_check"][$key]) ? $this->cnvCheckData($this->arrForm["item_check"][$key]) : "";
			$param["item_len"] = $this->arrForm["item_len"][$key];
			$param["item_pview"] = isset($this->arrForm["item_pview"][$key]) ? "1" : "0";
			$param["item_mail"] = isset($this->arrForm["item_mail"][$key]) ? "1" : "0";
			$param["item_type"] = isset($this->arrForm["item_type"][$key]) ? $this->arrForm["item_type"][$key] : "";
			$param["item_select"] = isset($this->arrForm["item_select"][$key]) ? $this->arrForm["item_select"][$key] : "";
			$param["item_memo"] = $this->arrForm["item_memo"][$key];

			if(!$check) {
				// 登録
				$param["form_id"] = $this->arrForm["form_id"];
				$param["item_id"] = $key;

				$rs = $db->insert("form_item", $param, __FILE__, __LINE__);

			} else {
				// 更新
				$param["udate"] = "NOW";
				$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);
				$where[] = "item_id = ".$db->quote($key);

				$rs = $db->update("form_item", $param, $where, __FILE__, __LINE__);

				unset($where);
			}

			if(!$rs) {
				return false;
			}

			unset($param);
		}

		return true;

	}

	// チェック項目を配列から文字列へ
	function cnvCheckData($arrcheck=array()) {

		if(!count($arrcheck) > 0) return "";

		return implode("|", $arrcheck);
	}

	// フォーム項目に登録があるか確認　ある場合true　ない場合false
	function checkItem(&$db, $form_id, $item_id) {

		$where[] = "form_id = ".$db->quote($form_id);
		$where[] = "item_id = ".$db->quote($item_id);

		$rs = $db->_getOne("form_id", "form_item", $where, __FILE__, __LINE__);

		if(!$rs) return false;

		return true;

	}

	// DB更新　$temp（一時保存の場合1）　$item(フォーム項目の更新も行う場合1)
	function update($temp=0, $item=0) {

		$db = new DbGeneral;

		$db->begin();

		foreach($this->init() as $val) {

			if(!isset($this->arrForm[$val[0]])) $this->arrForm[$val[0]] = "";

			if($val[5] == "1") {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
			if($val[6] == "1") {
				if(empty($param[$val[0]]) || $param[$val[0]] == "") $param[$val[0]] = "0";
			}
		}

		$param["group3"] = $this->arrForm["group3_1"]."|".$this->arrForm["group3_2"]."|".$this->arrForm["group3_3"];

		unset($param["password"]);
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		}

        // 受付期間
		if($this->arrForm["reception_date1"] != "" && $this->arrForm["reception_date2"] != "") {
			$param["reception_date1"] = $this->arrForm["reception_date1"];
			$param["reception_date2"] = $this->arrForm["reception_date2"];
		}
        // システムメンテナンス
		$param["maintenance_date1"] = NULL;
		$param["maintenance_date2"] = NULL;
		if($this->arrForm["maintenance_date1"] != ""){
			$param["maintenance_date1"] = $this->arrForm["maintenance_date1"];
		}
		if($this->arrForm["maintenance_date2"] != ""){
			$param["maintenance_date2"] = $this->arrForm["maintenance_date2"];
		}

		if($this->arrForm["type"] == "1") {
			if($this->arrForm["primary_reception_date"] != "" && $this->arrForm["secondary_reception_date"] != "") {
				$param["primary_reception_date"] = $this->arrForm["primary_reception_date"];
				$param["secondary_reception_date"] = $this->arrForm["secondary_reception_date"];
			}
		} else {
			$param["primary_reception_date"] = NULL;
			$param["secondary_reception_date"] = NULL;
		}


		// 登録済み画像があり、削除フラグがある場合　又は、登録済みと新たにアップロードした画像がある場合は、登録済み画像を削除する。
		if(($this->arrForm["dst_head_image"] != "" && $this->arrForm["image_del"] == "1") || ($this->arrForm["dst_head_image"] != "" && $this->arrForm["temp_image"] != "")) {
			$param["head_image"] = "";
			if(file_exists(IMG_PATH."header/".$this->arrForm["dst_head_image"])) {
				unlink(IMG_PATH."header/".$this->arrForm["dst_head_image"]);
			}
		}
		// 登録済み画像がなく、新規アップロードファイルがあり、削除フラグがある場合は、新規アップロードファイルを削除する。
		if($this->arrForm["dst_head_image"] == "" && $this->arrForm["temp_image"] != "" && $this->arrForm["image_del"] == "1") {
			if(file_exists(IMG_PATH."header/".$this->arrForm["temp_image"])) {
				unlink(IMG_PATH."header/".$this->arrForm["temp_image"]);
			}
			$this->arrForm["temp_image"] = "";
		}

		if($this->arrForm["temp_image"] != "") $param["head_image"] = $this->arrForm["temp_image"];

		if($temp == "0") {
			$param["temp"] = "0";
		} else {
			$param["temp"] = "1";
		}

		$param["udate"] = "NOW";


		//-------------------------------------
		//決済　マーチャント情報
		//-------------------------------------
		if($this->arrForm["type"] == "3" && $this->arrForm["credit"] != "0"){

			//---------------------------------
			//MDK設定情報 をセッションから復元
			//---------------------------------
			$this->_restore_merchant();
			$param["pgcard_shop_id"]   = $this->arrForm["pgcard_shop_id"];
			$param["pgcard_shop_pass"] = $this->arrForm["pgcard_shop_pass"];
			$param["pgcard_site_id"]   = $this->arrForm["pgcard_site_id"];
			$param["pgcard_site_pass"] = $this->arrForm["pgcard_site_pass"];
			$param["pgcard_use3ds"]    = $this->arrForm["pgcard_use3ds"];
			$param["pgcard_3ds_shopname"] = $this->arrForm["pgcard_3ds_shopname"];
		}

		//-------------------------------------
		//自動金額変更
		//-------------------------------------
		if($this->arrForm['auto_change_flg'] == 1){
			$param["auto_change_day"] = $this->arrForm["auto_change_day"];
		}

		$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);

		$rs = $db->update("form", $param, $where, __FILE__, __LINE__);

		if(!$rs) {
			$db->rollback();
			if($temp == "0") {
				$this->complete("フォーム情報の更新に失敗しました。");
			} else {
				$this->complete("フォーム情報の一次保存に失敗しました。");
			}
		}

		// $itemが1の場合はフォーム項目の更新も行う
		if($item == "1") {

			$rs = $this->setitem($db, $temp);

			if(!$rs) {
				$db->rollback();
				if($temp == "0") {
					$this->complete("フォーム情報の更新に失敗しました。");
				} else {
					$this->complete("フォーム情報の一次保存に失敗しました。");
				}
			}
		}

		$this->updateFormAdmin($db);

		//-------------------------------------------------
		//決済モジュールの複製
		//--------------------------------------------------
		if($this->arrForm["type"] == "3" && $this->arrForm["credit"] != "0"){

			//オリジナルパッケージをコピー
			$cmd = "/usr/bin/php ".ROOT_DIR."Sys/form/Sys_tgk.php ".$this->arrForm["form_id"]." > /dev/null &";

			system($cmd);

		}




		$db->commit();
		if($temp == "0") {
			$this->complete("フォーム情報を更新しました。");
		} else {
			$this->complete("フォーム情報を一次保存しました。");
		}

	}


	function updateFormAdmin(&$db) {

		$where[] = "form_id = ".$db->quote($this->arrForm["form_id"]);
		$where[] = "super_flg = 1";

		$rs = $db->_getOne("formadmin_id", "form_admin", $where, __FILE__, __LINE__);

		if(!$rs) {
			return $this->registFormAdmin($db);
		}

		$param["login_id"] = $this->arrForm["login_id"];
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		}
		$param["udate"] = "NOW";

		$rs = $db->update("form_admin", $param, $where);


		if(!$rs) {
			$db->rollback();
			$this->complete("フォーム管理者の登録に失敗しました。");
		}

		return;

	}


	// 完了画面の表示
	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";

		$this->arrForm["url"] = "Sys_formlist.php?user_id=".$this->arrForm["user_id"];

		// 親クラスに処理を任せる
		parent::main();
		exit;

	}

	function upload_image() {

		if(empty($_FILES)) return;
		if(!$_FILES["upfile"]["size"] > 0) return;

		// ファイルがアップロードされた時
		if($_FILES["upfile"]["size"] > 0) {
			if($_FILES["upfile"]["error"] > 0) return "ファイルのアップロードに失敗しました。";

			if($this->arrForm["temp_image"] != "") {
				if(file_exists(IMG_PATH."header/".$this->arrForm["temp_image"])) {
					unlink(IMG_PATH."header/".$this->arrForm["temp_image"]);
				}
			}

			$filename = date("YmdHis");
			$file_info = pathinfo($_FILES["upfile"]["name"]);

			$this->arrForm["temp_image"] = date("YmdHis").".".$file_info["extension"];
			$rs = @move_uploaded_file($_FILES["upfile"]["tmp_name"], IMG_PATH."header/".$this->arrForm["temp_image"]);

			if(!$rs) return "ファイルの一時保存に失敗しました。";

			$this->arrForm["image_del"] = "0";

		}

		return;

	}

	/**
	 * セッションからマーチャント情報を復元する
	 */
	function _restore_merchant(){
		$wk_merchant = $GLOBALS["session"]->getVar("ss_merchant");
		$this->arrForm["pgcard_shop_id"]   = $wk_merchant["pgcard_shop_id"];
		$this->arrForm["pgcard_shop_pass"] = $wk_merchant["pgcard_shop_pass"];
		$this->arrForm["pgcard_site_id"]   = $wk_merchant["pgcard_site_id"];
		$this->arrForm["pgcard_site_pass"] = $wk_merchant["pgcard_site_pass"];
		$this->arrForm["pgcard_use3ds"]    = $wk_merchant["pgcard_use3ds"];
		$this->arrForm["pgcard_3ds_shopname"] = $wk_merchant["pgcard_3ds_shopname"];
	}

	/**
	 * セッションにマーチャント情報を登録する
	 */
	function _set_ss_merchant(){
        $merchant = array();
		$merchant["pgcard_shop_id"]   = trim($_REQUEST["pgcard_shop_id"]);
		$merchant["pgcard_shop_pass"] = trim($_REQUEST["pgcard_shop_pass"]);
		$merchant["pgcard_site_id"]   = trim($_REQUEST["pgcard_site_id"]);
		$merchant["pgcard_site_pass"] = trim($_REQUEST["pgcard_site_pass"]);
		$merchant["pgcard_use3ds"]    = trim($_REQUEST["pgcard_use3ds"]);
		$merchant["pgcard_3ds_shopname"] = trim($_REQUEST["pgcard_3ds_shopname"]);
		// 3Dセキュア利用で店舗名が空白→フォーム名を利用する
		if($merchant["pgcard_use3ds"] == 1 && strlen($merchant["pgcard_3ds_shopname"]) == 0){
		    $merchant['pgcard_3ds_shopname'] = $this->arrForm['form_name'];
		}

		$GLOBALS["session"]->setVar("ss_merchant", $merchant);
	}

//	/**
//	 * マーチャント情報変更履歴登録
//	 */
//	function _set_merchant_history($po_db){
//
//		$param="";
//		$where="";
//
//		$param["merchant_cc_id"] = $this->arrForm["merchant_cc_id"];
//		$param["merchant_sec_key"] = $this->arrForm["merchant_sec_key"];
//		$param["udate"] = "NOW";
//		$where[] = "form_id = ".$this->arrForm["form_id"];
//
//		$rs = $po_db->update("entory_number", $param, $where, __FILE__, __LINE__);
//		if(!$rs) {
//			$po_db->rollback();
//			$this->complete("マーチャント情報の更新に失敗しました。");
//		}
//	}
}

/**
 * メイン処理開始
 **/

$c = new formbase();
$c->main();







?>