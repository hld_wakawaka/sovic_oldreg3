<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');
include_once(MODULE_DIR.'custom/Form.class.php');

/**
 * システム管理者　各種文言設定
 *
 *
 * @subpackage Sys
 * @author salon
 *
 */
class formadmin_edit extends ProcessBase {

	/**
	 * コンストラクタ
	 */
	function formadmin_edit(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){


		LoginAdmin::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		$this->_title = "スーパー管理者ページ";

		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//インスタンス
		//----------------------
		$this->wo_form = new Form();
		$this->db = new DbGeneral;
		$this->objErr = New Validate;

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";


		//-----------------------
		//フォームパラメータ取得
		//-----------------------
		$this->arrForm = GeneralFnc::convertParam($this->_init(), $_REQUEST);

		//-------------------------
		//フォーム情報
		//-------------------------
		$this->formdata =  $this->_getFormTitle();
		if(!$this->formdata){
			$this->complete("フォーム情報の取得に失敗しました。");
		}
		$this->assign("formdata", $this->formdata);

		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_formadmin_edit.html";

		//---------------------------------
		//アクション別処理
		//---------------------------------

		$arrErr = array();

		switch($ws_action){

			case "confirm":
			
				$arrErr = $this->_check();
				if(count($arrErr) > 0) break;
				
				$this->_processTemplate = "Sys/form/Sys_formadmin_edit_confirm.html";

				break;

			case "complete":

				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");
				
				$arrErr = $this->_check();
				if(count($arrErr) > 0) break;
		
				if($this->arrForm["formadmin_id"] != "") {
					$this->_update();
				} else {
					$this->_insert();
				}

				$this->complete($this->msg);

				break;
			//-------------------
			//戻る
			//-------------------
			case "back":
				break;
				
			case "edit":
            case "lock_reset":   //ロック解除
			
				$this->arrForm = $this->getData();
                
            
                //ロック解除の場合
                $this->arrForm["lock_reset"] = ($ws_action == "lock_reset") ? 1 : "";


                
				break;
			//-------------------
			//初期表示時
			//-------------------
			default:

		}


		$this->arrForm["form_id"] = $_REQUEST["form_id"];
		$this->arrForm["user_id"] = $_REQUEST["user_id"];

		$this->assign("arrErr", $arrErr);	//エラー配列
        

		// 親クラスに処理を任せる
		parent::main();
		

		
		//print_r($this->arrForm);

	}


	/**
	 *
	 */
	function _check(){
		
		//　ログインIDのチェック
		if($this->arrForm["login_id"] == "") {
			$this->objErr->addErr("ログインIDが未入力です。", "login_id");
		} elseif(!$this->objErr->isAlphaNumeric($this->arrForm["login_id"])) {
			//　半角英数字のみ
			$this->objErr->addErr(sprintf($GLOBALS["msg"]["err_han_alphanumeric"], "ログインID"), "login_id");
		}
		
		// パスワードのチェック　更新時は必須ではない
        //　ロック解除の場合は必須
		if($this->arrForm["formadmin_id"] == "" || $this->arrForm["lock_reset"] == 1) {
			if($this->arrForm["password"] == "") {
				$this->objErr->addErr(sprintf($GLOBALS["msg"]["err_require_input"], "ログインパスワード"), "password");
			}
		}
		
		if($this->arrForm["password"] != "") {
			//　半角英数字のみ
			if(!$this->objErr->isAlphaNumeric($this->arrForm["password"])) {
				$this->objErr->addErr(sprintf($GLOBALS["msg"]["err_han_alphanumeric"], "ログインパスワード"), "password");
			} else {
				if($this->arrForm["password2"] == "") {
					$this->objErr->addErr(sprintf($GLOBALS["msg"]["err_han_alphanumeric"], "確認用ログインパスワード"), "password2");
				} else {
					//　確認用と一致しているか
					if($this->arrForm["password"] != $this->arrForm["password2"]) {
						$this->objErr->addErr("ログインパスワードと確認用ログインパスワードが一致しません。", "password2");
					}
				}
			}
		}
		
		// 重複チェック
		////$where[] = "form_id = ".$this->db->quote($this->arrForm["form_id"]);
		$where[] = "login_id = ".$this->db->quote($this->arrForm["login_id"]);
		$where[] = "del_flg = 0";
		
		if($this->arrForm["formadmin_id"] != "") {
			$where[] = "formadmin_id <> ".$this->db->quote($this->arrForm["formadmin_id"]);
		}
		
		$rs = $this->db->getData("*", "form_admin", $where, __FILE__, __LINE__);
		
		if($rs) {
			$this->objErr->addErr("同じログインIDが存在します。", "password");
		}
		
		return $this->objErr->_err;
	}
	/**
	 * フォーム項目
	 */
	function _init(){

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)）
		$key[] = array("formadmin_id",	"管理者ID",		array(),	array(),	"n",	1);
		$key[] = array("user_id",		"ユーザーID",		array(),	array(),	"n",	0);
		$key[] = array("form_id",		"フォームID",		array(),	array(),	"n",	1);
		$key[] = array("login_id",		"ログインID",		array(),	array(),	"an",	1);
		$key[] = array("password",		"ログインパスワード",	array(),	array(),	"an",	1);
		$key[] = array("password2",		"ログインパスワード2",	array(),	array(),	"an",	0);
        $key[] = array("lock_reset",       "ロック解除",       array(),    array(),    "n",    0); 
		return $key;
	}

	/**
	 * フォーム情報
	 */
	function _getFormTitle(){

    	$column = "form_id, form_name";
    	$from = "form";
    	$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
    	$where[] = "del_flg = 0";

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	if(!$rs){
    		false;
    	}

		return $rs;

	}

	/**
	 * 新規登録処理
	 */
	function _insert(){

		$key = $this->_init();

		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}
		
		$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		unset($param["formadmin_id"]);
		
		$this->db->begin();

		$rs = $this->db->insert("form_admin", $param, __FILE__, __LINE__);

		if(!$rs) {
			$this->msg = "管理者の登録に失敗しました。";
			$this->db->rollback();
			return false;
		}

		$this->msg = "管理者の登録が完了しました。";
		$this->db->commit();
		return true;
	}
	
	

	/**
	 * 更新処理
	 */
	function _update(){
		$key = $this->_init();

		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}
		
		unset($param["formadmin_id"]);
		unset($param["form_id"]);
		
		if($this->arrForm["password"] != "") {
			$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		}
        else{
        	unset($param["password"]);
        }

        //ロック解除の場合
        if($this->arrForm["lock_reset"] == 1){
        	$param["lock_flg"] = 0;
            $param["login_err_count"] = 0;
            $param["login_try_date"] = NULL;
        }

		$param["udate"] = "NOW";
		$where = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
		$where = "formadmin_id = ".$this->db->quote($_REQUEST["formadmin_id"]);
		
		$this->db->begin();

		$rs = $this->db->update("form_admin", $param, $where, __FILE__, __LINE__);

		if(!$rs) {
			$this->msg = "管理者の更新に失敗しました。";
			$this->db->rollback();
			return false;
		}

		$this->msg = "管理者の更新が完了しました。";
		$this->db->commit();
		return true;
	}
	
	function getData() {
		
		if($this->arrForm["formadmin_id"] == "") return;
		
		$column = "*";
    	$from = "form_admin";
    	$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
    	$where[] = "formadmin_id = ".$this->db->quote($_REQUEST["formadmin_id"]);
    	$where[] = "del_flg = 0";

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	if(!$rs){
    		false;
    	}
    	
    	unset($rs["password"]);

		return $rs;
		
		
	}

	/**
	 * 終了処理
	 *
	 * @access public
	 * @param  string
	 */
	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";
		//$this->_title = "各種文言設定（".$this->formdata["form_name"]."）";
		$this->arrForm["url"] = "Sys_formadmin.php?user_id=".$this->arrForm["user_id"]."&form_id=".$this->arrForm["form_id"];

		// 親クラスに処理を任せる
		parent::main();
		exit;

	}

}

/**
 * メイン処理開始
 **/

$c = new formadmin_edit();
$c->main();







?>