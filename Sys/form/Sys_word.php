<?php

include('../../application/cnf/include.php');
include('../Sys_common.php');
include_once(MODULE_DIR.'custom/Form.class.php');

/**
 * システム管理者　各種文言設定
 *
 *
 * @subpackage Sys
 * @author salon
 *
 */
class form_word extends ProcessBase {

	/**
	 * コンストラクタ
	 */
	function form_word(){

		parent::ProcessBase();
	}

	/**
	 * メイン処理
	 */
	function main(){


		LoginAdmin::checkLoginRidirect();

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		$this->_title = "スーパー管理者ページ";

		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());



		//----------------------
		//インスタンス
		//----------------------
		$this->wo_form = new Form();
		$this->db = new DbGeneral;
		$this->objErr = New Validate;


		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";


		//-----------------------
		//フォームパラメータ取得
		//-----------------------
		$this->arrForm = GeneralFnc::convertParam($this->_init(), $_REQUEST);


		//------------------------
		//文言情報取得
		//------------------------
		$this->arrWord = $this->wo_form->getFormWord($_REQUEST["form_id"]);

		//-------------------------
		//フォーム情報
		//-------------------------
		$this->formdata =  $this->_getFormTitle();
		if(!$this->formdata){
			$this->complete("フォーム情報の取得に失敗しました。");
		}
		$this->assign("formdata", $this->formdata);




		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/form/Sys_word.html";





		//---------------------------------
		//アクション別処理
		//---------------------------------

		$arrErr = array();

		switch($ws_action){

			case "confirm":

//
//				//差出人の長さチェック
//				if($this->arrForm["word13"] != ""){
//					$arrErr = $this->_check();
//					if(count($arrErr) > 0){
//
//					}
//			    	else{
//			    		$this->_processTemplate = "Sys/form/Sys_word_confirm.html";
//			    	}
//				}
				$this->_processTemplate = "Sys/form/Sys_word_confirm.html";


				break;

			case "complete":

				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");


				//---------------------
				//トランザクション
				//---------------------
				$this->db->begin();


				if(!$this->arrWord) {
					$wb_ret = $this->_insFormWord();
					$msg = "各種文言設定を行いました。登録";
				} else {
					$wb_ret = $this->_updFormWord();
					$msg = "各種文言設定を行いました。更新";
				}

				if(!$wb_ret){
					$msg = "文言設定処理に失敗しました。";
					$this->complete($msg);
				}

				//-----------------------
				//コミット
				//-----------------------
				$this->db->commit();

				$this->complete($msg);

				break;
			//-------------------
			//戻る
			//-------------------
			case "back":

				break;
			//-------------------
			//初期表示時
			//-------------------
			default:

				if(!$this->arrWord){

				}
				else{

					//DBから取得したデータ
					$this->arrForm = $this->arrWord;
					
				}


		}


		$this->arrForm["form_id"] = $_REQUEST["form_id"];
		$this->arrForm["user_id"] = $_REQUEST["user_id"];

		$this->assign("arrErr", $arrErr);	//エラー配列



		// 親クラスに処理を任せる
		parent::main();
		
		//print_r($this->arrForm);

	}


	/**
	 *
	 */
	function _check(){

		//フォーム情報取得
		$column = "form_id, form_name, type, lang, temp, edit_flg, form_desc, form_mail, contact, head, head_image, reception_date1, reception_date2, primary_reception_date, secondary_reception_date, agree, agree_text, group1, group2, group3, group4, group5, credit, pricetype, price";
		$from = "form";
		$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
		$where[] = "del_flg = 0";

		$formData = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

		$chkdata  = $this->arrForm["word13"]."<".$formData["form_mail"].">";
		if(!$this->objErr->isMLen($chkdata,65)){
			$this->objErr->addErr("差出人名が長すぎます。<".$formData["form_mail"].">を含めて65byte内になるように設定してください。", "agree");
		}
		return $this->objErr->_err;
	}
	/**
	 * フォーム項目
	 */
	function _init(){

		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)）
		$key[] = array("word1", "登録完了画面",                     array(),	array(),	"KV",	1);
		$key[] = array("word2", "編集完了画面",                     array(),	array(),	"KV",	1);
		$key[] = array("word13", "メール差出人名",                  array(),	array(),	"KV",	1);
		$key[] = array("word3", "登録完了メール",                   array(),	array(),	"KV",	1);
		$key[] = array("word11", "登録完了メールタイトル",          array(),	array(),	"KV",	1);
		$key[] = array("word4", "編集完了メール",                   array(),	array(),	"KV",	1);
		$key[] = array("word12", "編集完了メールタイトル",          array(),	array(),	"KV",	1);
		$key[] = array("word5", "グループ１　説明",                 array(),	array(),	"KV",	1);
		$key[] = array("word6", "グループ2　説明",                  array(),	array(),	"KV",	1);
		$key[] = array("word7", "グループ3　説明",                  array(),	array(),	"KV",	1);
		$key[] = array("word8", "支払金額　説明",                   array(),	array(),	"KV",	1);
		$key[] = array("word9", "支払方法　説明",                   array(),	array(),	"KV",	1);
		$key[] = array("word10", "支払情報　説明",                  array(),	array(),	"KV",	1);
		$key[] = array("word14", "フォーム表示タイトル",            array(),	array(),	"KV",	1);
		$key[] = array("word15", "銀行振込説明文",                  array(),	array(),	"KV",	1);
		$key[] = array("word16", "クレジット説明文",                array(),	array(),	"KV",	1);

		$key[] = array("word17", "グループ１　ページ下部のコメント", array(),	array(),	"KV",	1);
		$key[] = array("word18", "グループ２　ページ下部のコメント", array(),	array(),	"KV",	1);
		$key[] = array("word19", "グループ３　ページ下部のコメント", array(),	array(),	"KV",	1);


		$key[] = array("word20", "各ブロックの見出し　背景色",              array(),	array(),	"a",	1);
		$key[] = array("word21", "各ブロックの見出し　フォントカラー",      array(),	array(),	"a",	1);
		$key[] = array("word22", "各ブロックtableヘッダ　背景色",           array(),	array(),	"a",	1);
		$key[] = array("word23", "各ブロックtableヘッダ　フォントカラー",   array(),	array(),	"a",	1);
		$key[] = array("word24", "各ブロックの見出し　ボーダーカラー",      array(),	array(),	"a",	1);
		$key[] = array("word25", "各ブロックtableヘッダ　ボーダーカラー",   array(),	array(),	"a",	1);

		$key[] = array("word26", "エントリー開始前メッセージ",      array(),	array(),	"KV",	1);
		$key[] = array("word27", "エントリー終了メッセージ",        array(),	array(),	"KV",	1);

        $key[] = array("word28", "お振り込み人名義　コメント",      array(),   array(),    "KV",    1);
        $key[] = array("word29", "お支払銀行名　コメント",          array(),   array(),    "KV",    1);
        $key[] = array("word30", "お振り込み日　コメント",          array(),   array(),    "KV",    1);
        
        $key[] = array("word31", "カード会社　コメント",            array(),   array(),    "KV",    1);
        $key[] = array("word32", "クレジットカード番号　コメント",  array(),   array(),    "KV",    1);
        $key[] = array("word33", "有効期限　コメント",              array(),   array(),    "KV",    1);
        $key[] = array("word34", "カード名義人　コメント",          array(),   array(),    "KV",    1);
        $key[] = array("word35", "セキュリティコード　コメント",    array(),   array(),    "KV",    1);
        $key[] = array("word36", "システムメンテナンス　メッセージ",array(),   array(),    "KV",    1);
        $key[] = array("word38", "確認画面メッセージ：新規登録用", array(),	array(),	"KV",	1);
        $key[] = array("word39", "確認画面メッセージ：編集用", array(),	array(),	"KV",	1);

		return $key;
	}

	/**
	 * フォーム情報
	 */
	function _getFormTitle(){

    	$column = "form_id, form_name";
    	$from = "form";
    	$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
    	$where[] = "del_flg = 0";

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	if(!$rs){
    		false;
    	}

		return $rs;



	}


	/**
	 * 新規登録処理
	 */
	function _insFormWord(){

		$key = $this->_init();

		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}

		$param["form_id"] = $_REQUEST["form_id"];
		$rs = $this->db->insert("form_word", $param, __FILE__, __LINE__);

		if(!$rs) {
			return false;
		}

		return true;
	}

	/**
	 * 更新処理
	 */
	function _updFormWord(){
		$key = $this->_init();

		foreach($key as $val) {
			if($val[5] == 1) {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
		}

		$param["udate"] = "NOW";
		$where = "form_id = ".$this->db->quote($_REQUEST["form_id"]);


		$rs = $this->db->update("form_word", $param, $where, __FILE__, __LINE__);

		if(!$rs) {
			return false;
		}

		return true;
	}

	/**
	 * 終了処理
	 *
	 * @access public
	 * @param  string
	 */
	function complete($msg) {

		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";
		//$this->_title = "各種文言設定（".$this->formdata["form_name"]."）";
		$this->arrForm["url"] = "Sys_formlist.php?user_id=".$_REQUEST["user_id"];

		// 親クラスに処理を任せる
		parent::main();
		exit;

	}

}

/**
 * メイン処理開始
 **/

$c = new form_word();
$c->main();







?>