<?php

include('../application/cnf/include.php');
include('./Sys_common.php');

/**
 * システム管理者
 * 	ユーザ登録・編集
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class passwrodedit extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function passwrodedit(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

		LoginAdmin::checkLoginRidirect();
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = GeneralFnc::convertParam($this->init(), $_REQUEST);


		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/Sys_passwordedit.html";
		$this->_title = "スーパー管理者ページ";
		
		$arrErr = array();

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){
			
			//-----------------
			//確認画面
			//-----------------		
			case "confirm":
			
				$arrErr = $this->check();
				if(count($arrErr) > 0) break;

				$this->_processTemplate = "Sys/Sys_passwordedit_confirm.html";
			break;

			//-----------------
			//登録・編集処理
			//-----------------
			case "complete":
			
				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");
			
				$arrErr = $this->check();
				if(count($arrErr) > 0) break;
				
				$this->update();
				
			 break;
			 
			 case "back":
			 
			 break;
			
			/**
			 * 初期表示時
			 */
			default:
				
					
			 break;						
			
		}		

		$this->assign("arrErr", $arrErr);

		
		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}
	
	function init() {
		
		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("password",	"パスワード", 		array(5, 12),	array("NULL", "LEN"),	"a",	1,	0);
		$key[] = array("password2", "確認用パスワード",	array(5, 12),	array("NULL"),			"a",	0,	0);

		return $key;
		
	}
	function check() {
		
		$objErr = New Validate;
		
		$objErr->check($this->init(), $this->arrForm);
		
		if($this->arrForm["password"] != $this->arrForm["password2"]) {
			$objErr->addErr("パスワードと確認用パスワードが一致しません。");
		}
		
		return $objErr->_err;
		
	}
	
	function update() {
		
		$db = new DbGeneral;

		$param["password"] = md5($this->arrForm["password"].PASS_PHRASE);
		$param["udate"] = "NOW";
		
		$where[] = "admin_id = ".$db->quote($GLOBALS["adminData"]["admin_id"]);
		
		$rs = $db->update("admin", $param, $where, __FILE__, __LINE__);
		
		if(!$rs) {
			$this->complete("パスワードの変更に失敗しました。");
		}
		$this->complete("パスワードを変更しました。");
		
	}

	function complete($msg) {
		
		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";
		
		// 親クラスに処理を任せる
		parent::main();
		exit;
		
	}
}

/**
 * メイン処理開始
 **/

$c = new passwrodedit();
$c->main();







?>