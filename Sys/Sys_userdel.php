<?php

include('../application/cnf/include.php');
include('./Sys_common.php');

/**
 * システム管理者
 * 	ユーザ削除
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class userdel extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function userdel(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

		LoginAdmin::checkLoginRidirect();
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm["user_id"] = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : "";
		
		if(!$this->arrForm["user_id"] > 0) $this->complete("ユーザーIDが指定されていません。");

		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/Sys_userdel.html";
		$this->_title = "ユーザ情報削除";
		
		//----------------------
		// オブジェクト生成
		//----------------------
		$objUser = new User;


		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			//-----------------
			//削除
			//-----------------
			case "complete":
			
				$this->del();
				
			　break;		
			
			/**
			 * 初期表示時
			 */
			default:
			
				$this->arrForm = $objUser->get($this->arrForm["user_id"]);
			
			 break;
			
		}
		
		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}
	
	function del() {
		
		$db = new DbGeneral;
		
		$param["del_flg"] = "1";
		$param["udate"] = "NOW";
		
		$where[] = "user_id = ".$db->quote($this->arrForm["user_id"]);
		
		$rs = $db->update("users", $param, $where, __FILE__, __LINE__);
		
		if(!$rs) {
			$this->complete("削除に失敗しました。");
		}
		
		$this->complete("削除が完了しました。");
		
	}
	
	function complete($msg) {
		
		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";
		
		// 親クラスに処理を任せる
		parent::main();
		exit;
		
	}
	 



}

/**
 * メイン処理開始
 **/

$c = new userdel();
$c->main();







?>