<?php

include('../application/cnf/include.php');
include('./Sys_common.php');

/**
 * システム管理者TOP
 * 
 * @author salon
 *
 */
class index extends ProcessBase {
	
	var $sess_key = "sk_user";
	var $search_key = array("sk_name", "page");


	/**
	 * コンストラクタ
	 */
	function index(){
		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

	
		LoginAdmin::checkLoginRidirect();
		
		// 表示HTMLの設定
		$this->_processTemplate = "Sys/index.html";
		$this->_title = "スーパー管理者ページ";
		
//		$this->assign("isTop", true);

		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
		
		//----------------------
		// オブジェクト生成
		//----------------------
		$objUser = new User;


		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){

			case "search":
			
				$GLOBALS["session"]->unsetVar($this->sess_key);
				
				foreach($this->search_key as $val) {
					$searchkey[$val] = isset($_REQUEST[$val]) ? $_REQUEST[$val] : "";
				}
				$GLOBALS["session"]->SetVar($this->sess_key, $searchkey, 30);
				
				break;
				
			case "clear":
			
				$GLOBALS["session"]->unsetVar($this->sess_key);
				
				break;
			
		}
		
		$this->arrForm = $GLOBALS["session"]->getVar($this->sess_key);
		
		$page = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";
		$arrData = $objUser->getList($this->arrForm, $page, ROW_LIMIT);
		
		$this->assign("arrData", $arrData);

		// 親クラスに処理を任せる
		parent::main();
	
		
	}	 
	 



}

/**
 * メイン処理開始
 **/

$c = new index();
$c->main();







?>