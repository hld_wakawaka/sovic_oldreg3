<?php

include('../application/cnf/include.php');
include('./Sys_common.php');

/**
 * システム管理者
 * 	ユーザ登録・編集
 * 
 * @subpackage Sys
 * @author salon
 *
 */
class useredit extends ProcessBase {


	/**
	 * コンストラクタ
	 */
	function useredit(){

		parent::ProcessBase();
	}
	
	/**
	 * メイン処理
	 */	 
	function main(){

		LoginAdmin::checkLoginRidirect();
		
		//-------------------------------
		//ログイン者情報
		//-------------------------------
		$this->assign("user_name", "システム管理者");
		
		//-------------------------------
		//スーパー管理者メニュー
		//-------------------------------
		$this->assign("va_menu", sys_common::menu());

		//----------------------
		//アクション取得
		//----------------------
		$ws_action = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";

		//フォームパラメータ
		$this->arrForm = GeneralFnc::convertParam($this->init(), $_REQUEST);
		
		//----------------------
		// オブジェクト生成
		//----------------------
		$objUser = new User;


		//------------------------
		// 表示HTMLの設定
		//------------------------
		$this->_processTemplate = "Sys/Sys_useredit.html";
		$this->_title = "スーパー管理者ページ";
		
		$arrErr = array();

		//---------------------------------
		//アクション別処理
		//---------------------------------
		switch($ws_action){
			
			//-----------------
			//確認画面
			//-----------------		
			case "confirm":
			
				$arrErr = $this->check();
				if(count($arrErr) > 0) break;

				$this->_processTemplate = "Sys/Sys_useredit_confirm.html";
			break;

			//-----------------
			//登録・編集処理
			//-----------------
			case "complete":
			
				if($this->_reload->isReload()) $this->complete("既に実行した可能性があります。");
			
				$arrErr = $this->check();
				if(count($arrErr) > 0) break;
				
				if($this->arrForm["user_id"] == "") {
					$this->insert();
				} else {
					$this->update();
				}
				
			 break;
			 
			 case "back":
			 
			 break;
			
			/**
			 * 初期表示時
			 */
			default:
			
				if($this->arrForm["user_id"] != "") $this->arrForm = $objUser->get($this->arrForm["user_id"]);
				
					
			 break;						
			
		}		

		$this->assign("arrErr", $arrErr);

		
		
		// 親クラスに処理を任せる
		parent::main();
	
		
	}
	
	function init() {
		
		//(0変数名、1項目名、2長さ(最小、最大）、3チェックすること、4変換、5データベースに登録する(1:する、0:しない)、 6 空の時0で埋める(1:する、0:しない)）
		$key[] = array("user_id", 	"顧客ID", 	array(),		array(),				"n",	0,	0);
		$key[] = array("user_name", "お名前", 	array(0, 40),	array("NULL", "MLEN"),	"KV",	1,	0);
		$key[] = array("email", 	"メール", 	array(0, 200),	array("LEN", "MAIL"),	"a",	1,	0);
		$key[] = array("addr", 		"住所", 		array(0, 300),	array("MLEN"),			"KV",	1,	0);
		$key[] = array("tel", 		"電話番号",	array(0, 13),	array("LEN"),			"n",	1,	0);
		$key[] = array("fax", 		"ファックス",	array(0, 13),	array("LEN"),			"n",	1,	0);
		
		return $key;
		
	}
	function check() {
		
		$objErr = New Validate;
		
		$objErr->check($this->init(), $this->arrForm);
		
		return $objErr->_err;
		
	}
	
	function insert() {
		
		$db = new DbGeneral;
		
		foreach($this->init() as $val) {
			if($val[5] == "1") {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
			if($val[6] == "1") {
				if($param[$val[0]] == "") $param[$val[0]] = "0";
			}
		}
		
		$rs = $db->insert("users", $param, __FILE__, __LINE__);
		
		if(!$rs) {
			$this->complete("ユーザー情報の登録に失敗しました。");
		}
		$this->complete("ユーザー情報を登録しました。");
		
	}
	
	function update() {
		
		$db = new DbGeneral;
		
		foreach($this->init() as $val) {
			if($val[5] == "1") {
				$param[$val[0]] = $this->arrForm[$val[0]];
			}
			if($val[6] == "1") {
				if($param[$val[0]] == "") $param[$val[0]] = "0";
			}
		}
		
		$param["udate"] = "NOW";
		
		$where[] = "user_id = ".$db->quote($this->arrForm["user_id"]);
		
		$rs = $db->update("users", $param, $where, __FILE__, __LINE__);
		
		if(!$rs) {
			$this->complete("ユーザー情報の更新に失敗しました。");
		}
		$this->complete("ユーザー情報を更新しました。");
		
	}

	function complete($msg) {
		
		$this->assign("msg", $msg);
		$this->_processTemplate = "Sys/Sys_complete.html";
		
		// 親クラスに処理を任せる
		parent::main();
		exit;
		
	}
}

/**
 * メイン処理開始
 **/

$c = new useredit();
$c->main();







?>