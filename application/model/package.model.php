<?php
/**
 * パッケージ機能を有効化するクラス
 * 
 * @author hunglead doi
 * */
abstract class AbstracePackage {

    private $form_id;
    private $includeDir;

    // ------------------------------------------------------
    // ▽Usrカスタマイズ
    // ------------------------------------------------------

    /**
     * パッケージ機能をロードする
     * 小クラスよりフォームIDを判別する
     * 
     * @param  void
     * @return void
     * */
    function onloadPackage()
    {
        if(!($class = get_called_class())) return;

        $this->form_id = preg_replace('/[^0-9]/', '', $class);

        //--------------------------
        // パッケージ機能
        //--------------------------
        $this->includeDir = TEMPLATES_DIR."../customDir/{$this->form_id}/package/templates/Usr/";
        if (method_exists($obj, "assign")) {
            $obj->assign("includeDir", $this->includeDir);
        }
    }


    // 外部テンプレートを読み込み直す
    function setTemplate($obj)
    {
        // パッケージ機能の一環
        Usr_initial::setTemplate($obj, $this->includeDir);
    }



    // ------------------------------------------------------
    // ▽Mngカスタマイズ
    // ------------------------------------------------------

    function mng_index_premain($obj)
    {
        $includeDir = TEMPLATES_DIR."../customDir/{$this->form_id}/package/templates/Mng/";
        $obj->setTemplateMng($obj, $includeDir);
    }


    function mng_detail_premain($obj)
    {
        $template_dir = $obj->_smarty->template_dir."../customDir/{$this->form_id}/package/templates/";
        $obj->_processTemplate = $template_dir."Mng/entry/Mng_entry_detail.html";
        $obj->assign("package", $template_dir);
    }


}

?>
