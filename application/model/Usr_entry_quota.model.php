<?php
/**
 * 応募制限(金額設定または項目)設定のロードとチェックメソッドクラス
 *   呼び出し元で必要な作業
 *   ・exClassでonStartのコール
 *   ・premain及びcompleteActionで必ずcheckQuotaをコール
 *    ・premainでonDestroyのコール
 *   エラーメッセージのバインドはexClassにbindQuotaErrorを定義することで行えます。
 * 
 * 4/17時点ではその他決済項目のみを集計の対象とする # 工数不足orz
 * 
 * @author hunglead doi
 * */
abstract class AbstraceQuota {

    /** 金額キーと残り応募可能な人数 #マイナスだとエラー */
    protected $arrQuotaDisabled = array();
    /** 定員情報配列 */
    protected $arrQuota = array();

    private $form_id;

    /**
     * 応募制限設定のロード及び残り応募可能人数を数える
     * 
     * @param  integer $form_id チェック対象のフォームID
     * @return void
     * */
    function onStart($form_id="") {
        // フォームIDをセット
        if(strlen($form_id)) $this->setFormId($form_id);

        // 定員に達すると選択できないようにする
        $this->arrQuotaDisabled = array();

        // 定員数を取得
        $this->arrQuota = $this->getQuota();
        if(count($this->arrQuota) > 0){
            $total = $this->getTotal();
            
            foreach($total as $_key => $_val){
                if(strlen($_val) == 0) $_val = 0;
                if(!isset($this->arrQuota[$_key])) continue;
                
                if(preg_match("/^ather_price([0-9]+)/u", $_key, $match) === 1){
                    // 定員 - 現在の応募者数 = 残り応募可能な数
                    $this->arrQuotaDisabled[$_key] = $this->arrQuota[$_key] - $_val;
                    continue;
                }
                
                // Not Support
//                 // edata[item_id]_[選択値]の書式なので分解する
//                 $tmp = explode("_", str_replace("edata", "", $_key));
//                 $item_id = $tmp[0];
//                 $value   = $tmp[1];
//                 // 定員 - 現在の応募者数 = 残り応募可能な数
//                 $this->arrQuotaDisabled[$item_id][$value] = $this->arrQuota[$_key] - $_val;
             }
        }
    }


    /**
     * 終端メソッド
     * 
     * @param  object exClass
     * @return void
     * */
    final public function onDestroy($obj){
        if (isset($obj->_smarty)) {
            $obj->_smarty->assign("arrQuotaDisabled", $this->arrQuotaDisabled);
        }
    }


    /***
     * 処理対象のフォームIDをセットする
     * 
     * @param  integer $form_id
     * @return void
     */
    final public function setFormId($form_id){
        $this->form_id = $form_id;
    }


    /** 金額設定及び項目設定の各定員数を取得 */
    function getQuota(){
        $tmp1 = $this->getQuotaFee();
        $tmp2 = $this->getQuotaFeeOther();
        $tmp3 = $this->getQuotaItem();

        $arrQuota = $tmp1 + $tmp2 + $tmp3;
        return $arrQuota;
    }


    /** 金額設定の各定員数を取得 */
    function getQuotaFee(){
        $arrQuota = array();
    
        // ファイル名の書式：[item_id]_[選択値]_[定員数]
        $quota1 = 'fee*_*';
        $quota2 = '(fee[0-9]+)_([0-9]+)';
    
        $customDir = ROOT_DIR."customDir/".$this->form_id."/quota/fee/";
        $ex = glob($customDir.$quota1);
        if(is_array($ex) && count($ex) > 0){
            foreach($ex as $_key => $script){
                if(preg_match("#/".$quota2."$#u", $script, $match) !== 1) continue;
    
                $arrQuota[$match[1]] = $match[2];
            }
        }
        return $arrQuota;
    }


    /** その他決済の各定員数を取得 */
    function getQuotaFeeOther(){
        $arrQuota = array();

        // ファイル名の書式：[item_id]_[選択値]_[定員数]
        $quota1 = 'ather_price*_*';
        $quota2 = '(ather_price[0-9]+)_([0-9]+)';

        $customDir = ROOT_DIR."customDir/".$this->form_id."/quota/fee/";
        $ex = glob($customDir.$quota1);
        if(is_array($ex) && count($ex) > 0){
            foreach($ex as $_key => $script){
                if(preg_match("#/".$quota2."$#u", $script, $match) !== 1) continue;

                $arrQuota[$match[1]] = $match[2];
            }
        }
        return $arrQuota;
    }


    /** edata毎の各定員数を取得 */
    function getQuotaItem(){
        $arrQuota = array();
    
        // ファイル名の書式：[item_id]_[選択値]_[定員数]
        $quota1 = '*';
        $quota2 = '(edata[0-9]+_[0-9])_([0-9]+)';
    
        $customDir = ROOT_DIR."customDir/".$this->form_id."/quota/edata/";
        $ex = glob($customDir.$quota1);
        if(is_array($ex) && count($ex) > 0){
            foreach($ex as $_key => $script){
                if(preg_match("#/".$quota2."$#u", $script, $match) !== 1) continue;
    
                $arrQuota[$match[1]] = $match[2];
            }
        }
        return $arrQuota;
    }


    function getTotal(){
        //新規　or 更新判別し当人を集計から除外
        // Mng編集→新規登録とすると編集扱いになる TODO
        $eid = "";
        if(isset($GLOBALS["entryData"]["eid"])){
            $eid = $GLOBALS["entryData"]["eid"];
        }

        $tmp1 = array(); // not support
        $tmp2 = $this->getTotalOther($eid);
        $tmp3 = array(); // not support
    
        $total = $tmp1 + $tmp2 + $tmp3;
        return $total;
    }


    /** その他決済の集計データ */
    function getTotalOther($eid){
        $addwhere = strlen($eid) == 0 ? '' : "and entory_r.eid <> ".$eid;
        
        //集計データ
        $total = array();

        $db = new DbGeneral(true);
        while(true){
            try{
                $column = array();
                $exfrom = array();
                foreach($this->arrQuota as $_key => $_quota){
                    if(preg_match("/^ather_price([0-9]+)/u", $_key, $match) === 0) continue;
                    $key = $match[1];
                    $column[]  = " sum(cnt{$key}) as ather_price{$key}";
                    $exfrom[] = "SUM(CASE WHEN payment_detail.key = '{$key}' THEN quantity END) AS cnt{$key}";
                }
                if(count($column) == 0) break;
                $column = implode(",", $column);
                $exfrom = implode(",", $exfrom);
                
                
                $from = "(";
                $from .= " select";
                $from .= "  ".$exfrom;
                $from .= " ";
                $from .= " ";
                $from .= " from payment_detail";
                $from .= " LEFT OUTER JOIN entory_r";
                $from .= " on payment_detail.eid = entory_r.eid";
                $from .= " ";
                $from .= " where";
                $from .= "      payment_detail.type = 1";
                $from .= "  and payment_detail.del_flg = 0";
                $from .= "  and entory_r.form_id = {$this->form_id}";
                $from .= "  and entory_r.del_flg = 0";
                $from .= "  and entory_r.invalid_flg = 0";
                $from .= " {$addwhere}";
                $from .= " ";
                $from .= " group by payment_detail.key";
                $from .= ") AS cnt";
            
                $rs = $db->getAll($column, $from);
                if(isset($rs[0])){
                    $total = $rs[0];
                }
                break;
            
            }catch(Exception $e){
                $total = array();
            }
        }
        return $total;
    }



    function checkQuota($obj, $exClass){
        // 定員数を常にチェックする
        if(count($this->arrQuotaDisabled) == 0) return;
    
        // 1ページ目の入力情報
        $arrForm1 = $GLOBALS["session"]->getVar("form_param1");
    
        $error = false;
        foreach($this->arrQuotaDisabled as $_key => $_quota){
            if(preg_match("/^ather_price([0-9]+)/u", $_key, $match) === 1){
                $buy = $arrForm1[$_key];
                if($buy == 0) continue;

                $over= $_quota-$buy; // 応募可能件数 - 購入枚数 = 0以上であればok
                if($over >= 0) continue;

                // overがマイナスで定員オーバー
                // 購入枚数を考慮した残り応募可能枚数
                $over= abs($over);
                $save= $buy - $over;

                $msg = "";
                // エラーメッセージをフォーム毎にバインド
                if(method_exists($exClass, "bindQuotaError")){
                    $msg = $exClass->bindQuotaError($_key, $buy, $save, $over);
                // デフォルトメッセージ
                }else{
                    switch($save){
                        default:
                            $msg = "応募制限エラー";
                            break;
                    }
                }
                $obj->arrErr[$_key] = $msg;
//                print "購入枚数：".$buy." / 残り：".$save." / オーバー：".$over;
                $error = true;
                
                continue;
            }
        }
        if($error){
            $obj->block = "1";
            $obj->_processTemplate =  "Usr/form/Usr_entry.html";
    
            $obj->assign("arrErr",  $obj->arrErr);
            $obj->assign("block",   $obj->block);
        }
        return $error;
    }


//     /**
//      * 応募制限エラーに引っかかった場合のエラーメッセージのバインド
//      *
//      * @param  string  $_key 対象のキー(fee,ather_price,edata)
//      * @param  integer $buy  応募枚数
//      * @param  integer $save 残り応募可能枚数
//      * @param  integer $over 超過枚数
//      * @return string  エラーメッセージ
//      * */
//     abstract function bindQuotaError($_key, $buy, $save, $over);


}

?>
