<?php

interface externalResource
{

    /**
     * 項目の並び替えを行う
     * 
     * @param  $obj ページオブジェクト
     * @return void
     * */
    public function sortFormIni($obj);
}
