<?php
require_once("PEAR.php");


/**
 * ログインの基底クラス
 * 
 * 重複ログイン、アカウントロック
 * 
 */
class LoginBase extends PEAR {

    // ログインユーザ情報
    private static $objMember = array();

    function __construct(){
        parent::PEAR();
    }


    public static function getLoginObjectUsr($form_id, $login_id="", $mail="", $eid=""){
        $dbGeneral = new DbGeneral();

        // 重複ログイン / ロックを考慮
        $column   = array();
        $column[] = 'entory_r.eid';
        $column[] = 'entory_r.form_id';
        $column[] = 'entory_r.e_user_id';
        $column[] = 'entory_r.e_user_passwd';
        $column[] = 'entory_r.status';
        $column[] = '(select formuser_id from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as formuser_id';
        $column[] = '(select access_time     from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as access_time';
        $column[] = '(select access_session  from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as access_session';
        $column[] = '(select lock_flg        from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as lock_flg';
        $column[] = '(select lock_date       from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as lock_date';
        $column[] = '(select login_err_count from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as login_err_count';
        $column[] = '(select login_try_date  from form_user where entory_r.eid = form_user.eid and form_user.del_flg = 0) as login_try_date';

        if(!IF_ENTRY_ID_EMAIL) {
            if(strlen($login_id) > 0){
                $where[] = ENTRY_ID_FIELD_NAME." = ".$dbGeneral->quote($login_id);
            }
        } else {
            if(strlen($mail) > 0){
                $mail = urlencode($mail);
                $where[] = ENTRY_ID_FIELD_NAME." = ".$dbGeneral->quote(addslashes($mail));
            }
        }
        if(ENTRY_NOT_DEL_WHERE != "") {
            $where[] = ENTRY_NOT_DEL_WHERE;
        }
        if(strlen($eid) > 0){
            $where[] = "eid = ".$dbGeneral->quote($eid);
        }

        $where[] = "form_id = ".$dbGeneral->quote($form_id);
        $where[] = "del_flg = 0";

        self::$objMember = $dbGeneral->getData(implode(',', $column), 'entory_r', $where, __FILE__, __LINE__);
        return self::$objMember;
    }


    public static function getLoginObjectMng($login_id, $mail){
        $dbGeneral = new DbGeneral();

        $column = "*";
        $from = MEMBER_TABLE;

        if(!IF_MEMBER_ID_EMAIL) {
            $where[] = MEMBER_ID_FIELD_NAME." = ".$dbGeneral->quote($login_id);
        } else {
            $mail = urlencode($mail);
            $where[] = MEMBER_ID_FIELD_NAME." = ".$dbGeneral->quote(addslashes($mail));
        }
        if(MEMBER_NOT_DEL_WHERE != "") {
            $where[] = MEMBER_NOT_DEL_WHERE;
        }

        self::$objMember = $dbGeneral->getData($column, $from, $where, __FILE__, __LINE__);
        return self::$objMember;
    }


    // 最終のロググインチャレンジより一定時間経過した時刻を取得
    public static function getDelayDate($key='login_try_date', $delay=ACOUNT_LOCK_TIME){
        return date("YmdHis",strtotime("+".$delay." minute" ,strtotime(substr(self::$objMember[$key], 0,19))));
    }


    // 一定時間経過しているかチェック
    public static function isInterval($key){
        $login_try_date = self::getDelayDate($key);
        return strtotime($login_try_date) < strtotime(date("YmdHis"));
    }


    // 規定回数オーバーかチェック
    public static function isCountOver($count=PASS_ERROR_COUNT){
        $login_err_count = self::$objMember['login_err_count']+1;
        return $count <= $login_err_count;
    }


    // 更新パラメータ：ロック情報リセット
    public static function getParamReset(){
        $param = array();
        $param["udate"]           = "now()";
        $param["login_try_date"]  = "now()";
        $param["login_err_count"] = 1;
        return $param;
    }

    // 更新パラメータ：正常処理
    public static function getParamUpdate(){
        $param  = array();
        $param["login_err_count"] = 0;
        $param["login_try_date"]  = NULL;
        $param["access_time"]     = "now()";
        $param["access_session"]  = session_id();
        $param["udate"]     = "now()";
        return $param;
    }


    // 更新パラメータ：ロック解除
    public static function getParamUnlock(){
        $param = array();
        $param["lock_flg"]  = 0;
        $param["lock_date"] = NULL;
        $param["udate"]     = "now()";
        return $param;
    }


    // 更新パラメータ：ログアウト
    public static function getParamClear(){
        $param = array();
        $param["lock_flg"] = 0;
        $param["udate"]    = "now()";
        $param["login_err_count"] = 0;
        $param["login_try_date"]  = NULL;
        $param["access_session"]  = NULL;
        return $param;
    }


    // 更新パラメータ：パスワード間違い
    public static function getParamCountup(){
        $param = array();
        $param["udate"] = "now()";

        // ログイン実行日時セット
        if(self::$objMember["login_err_count"] == 0){
            $param["login_try_date"] = "now()";
        }

        //パスワード間違いカウント カウントアップ
        $param["login_err_count"] = self::$objMember["login_err_count"]+1;

        // 規定回数オーバーの場合 ロック
        if(self::isCountOver()){
            $param["lock_flg"] = 1;
            $param["lock_date"] = "now()";
        }
        return $param;
    }


    //----------------------------------------------------------------
    // Mng専用処理
    //----------------------------------------------------------------

    // [Mng]パスワード間違い時のカウントアップ
    public static function countupMng(){
        $dbGeneral = new DbGeneral();
        $where = "formadmin_id =".$dbGeneral->quote(self::$objMember['formadmin_id']);

        // 一定時間内にN回目失敗した場合は、アカウントロック
        $param = self::isInterval('login_try_date')
               ? self::getParamReset()     // 一定期間を過ぎている場合
               : self::getParamCountup();  // 一定期間内

        $wb_ret = $dbGeneral->update("form_admin", $param, $where, __FILE__, __LINE__);
        return $wp_ret;
    }


    // [Mng]ロック解除
    public static function unlockMng(){
        $dbGeneral = new DbGeneral();
        $where = "formadmin_id =".$dbGeneral->quote(self::$objMember['formadmin_id']);

        $param = self::getParamUnlock();
        return $dbGeneral->update("form_admin", $param, $where, __FILE__, __LINE__);
    }


    // [Mng]ログイン成功時にログインユーザ情報を更新
    public static function updateMng(){
        $dbGeneral = new DbGeneral();
        $where = "formadmin_id =".$dbGeneral->quote($GLOBALS["userData"]["formadmin_id"]);
        $param = self::getParamUpdate();
        return $dbGeneral->update("form_admin", $param, $where, __FILE__, __LINE__);
    }


    // [Mng]ログアウト時のクリア
    public static function clearMng(){
        $dbGeneral = new DbGeneral();
        $where = "formadmin_id =".$dbGeneral->quote($GLOBALS["userData"]["formadmin_id"]);

        $param = self::getParamClear();
        return $dbGeneral->update("form_admin", $param, $where, __FILE__, __LINE__);
    }


    //----------------------------------------------------------------
    // Usr専用処理
    //----------------------------------------------------------------

    // [Usr]パスワード間違い時のカウントアップ
    public static function countupUsr(){
        $dbGeneral = new DbGeneral();
        $where = "eid =".$dbGeneral->quote(self::$objMember['eid']);

        // 一定時間内にN回目失敗した場合は、アカウントロック
        $param = self::isInterval('login_try_date')
               ? self::getParamReset()     // 一定期間を過ぎている場合
               : self::getParamCountup();  // 一定期間内
        $param['eid']             = self::$objMember['eid'];
        $param['form_id']         = self::$objMember['form_id'];

        $exists = $dbGeneral->exists('form_user', $where, __FILE__, __LINE__);
        $wb_ret = (!$exists)
                ? $dbGeneral->insert("form_user", $param,         __FILE__, __LINE__)
                : $dbGeneral->update("form_user", $param, $where, __FILE__, __LINE__);
        return $wp_ret;
    }


    // [Usr]ロック解除
    public static function unlockUsr(){
        $dbGeneral = new DbGeneral();
        $where = "eid =".$dbGeneral->quote(self::$objMember['eid']);

        $param = self::getParamUnlock();
        $param['eid']       = self::$objMember['eid'];
        $param['form_id']   = self::$objMember['form_id'];

        return $dbGeneral->update("form_user", $param, $where, __FILE__, __LINE__);
    }


    // [Usr]ログイン成功時にログインユーザ情報を更新
    public static function updateUsr(){
        $dbGeneral = new DbGeneral();
        $eid     = self::$objMember['eid'];
        $form_id = self::$objMember['form_id'];
        $where   = "eid =".$dbGeneral->quote($eid);

        $param  = self::getParamUpdate();
        $param2 = array('form_id'=>$form_id, 'eid'=>$eid, 'rdate'=>'now()');

        $exists = $dbGeneral->exists('form_user', $where, __FILE__, __LINE__);
        $wb_ret = (!$exists)
                ? $dbGeneral->insert("form_user", $param+$param2, __FILE__, __LINE__)
                : $dbGeneral->update("form_user", $param, $where, __FILE__, __LINE__);
        return $wb_ret;
    }


    // [Usr]ログアウト時のクリア
    public static function clearUsr(){
        $dbGeneral = new DbGeneral();
        $where   = "eid =".$dbGeneral->quote($GLOBALS["entryData"]['eid']);

        $param = self::getParamClear();
        return $dbGeneral->update("form_user", $param, $where, __FILE__, __LINE__);
    }

}


?>