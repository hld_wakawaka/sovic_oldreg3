<?php

class Form {

	var $db;
	var $isForm = false;
	var $formData;
	var $formWord;

    function Form($admin=0) {

    	$this->db = new DbGeneral;


    	if(!isset($_REQUEST["form_id"])) return;
    	if(!$_REQUEST["form_id"] > 0) return;
    	if(is_array($_REQUEST["form_id"])) return;
    	if(!is_Numeric($_REQUEST["form_id"])) return;

    	if(!$_REQUEST["form_id"] > 0) return;
    	if(is_array($_REQUEST["form_id"])) return;

    	$column = "form_id, form_name, type, lang, temp, edit_flg, form_desc, form_mail, contact, head, head_image, reception_date1, reception_date2, primary_reception_date, secondary_reception_date, agree, agree_text, group1, group2, group3, group4, group5, credit, pricetype, price, group2_use ,group3_use, group3_layout";
       	$column .= ",ather_price_flg, ather_price, price_header, price_head0, price_head1, ather_price_header, ather_price_head0, ather_price_head1, disp, csv_price_head0, csv_ather_price_head0";
       	$column .= ",pgcard_use3ds, agree_type, agree_chktext";
       	$column .= ",maintenance_date1, maintenance_date2";     // システムメンテナンス

    	$from = "form";
    	$where[] = "form_id = ".$this->db->quote($_REQUEST["form_id"]);
    	$where[] = "del_flg = 0";

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	if(!$rs) return;

    	if($admin == "0" && $rs["temp"] == "1") {
    		return;
    	}

    	$this->isForm = true;
    	$this->formData = $rs;
    	$this->formWord = $this->getFormWord($_REQUEST["form_id"]);

    }

    function checkValidDate() {

    	$entry_status = $this->checkValidDateSub();

	   	switch($entry_status) {
			// 受付前
			case "1":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_start"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date1"])));
				if($this->formWord["word26"] != ""){
					$ws_str = $this->formWord["word26"];
				}
				Error::showErrorPage($ws_str);
				break;
			// 受付後
			case "2":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_end"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date2"])));

				if($this->formWord["word27"] != ""){
					$ws_str = $this->formWord["word27"];
				}
				Error::showErrorPage($ws_str);
				break;
			// 1次2次
			case "3":
				$message = sprintf($GLOBALS["msg"]["validDate_middle"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["secondary_reception_date"])));
				Error::showErrorPage($message);
				break;
			default:
				break;
		}

		if(!$this->checkPropriety()) {
			Error::showErrorPage($GLOBALS["msg"]["err_login_status"]);
		}

		return;

    }

    function checkValidData_Ex() {

    	$entry_status = $this->checkValidDateSub();

	   	switch($entry_status) {
			case "1":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_start"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date1"])));
				if($this->formWord["word26"] != ""){
					$ws_str = $this->formWord["word26"];
				}
				Error::showErrorPage($ws_str);
				break;
			case "2":
				$ws_str = sprintf($GLOBALS["msg"]["validDate_end"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["reception_date2"])));

				if($this->formWord["word27"] != ""){
					$ws_str = $this->formWord["word27"];
				}
				Error::showErrorPage($ws_str);
				break;
			case "3":
				Error::showErrorPage(sprintf($GLOBALS["msg"]["validDate_middle"], date($GLOBALS["msg"]["dateformat"], strtotime($GLOBALS["form"]->formData["secondary_reception_date"]))));
				break;
			default:
				break;
		}

		return;
    }


    function checkValidDateSub() {

    	$now = strtotime(date("Y-m-d H:i:s"));
		$formData = $GLOBALS["form"]->formData;

		$date1 = strtotime($formData["reception_date1"]);
		$date2 = strtotime($formData["reception_date2"]);

		if($now < $date1) return "1";

		if($formData["type"] == "1") {
			$pdate = strtotime($formData["primary_reception_date"]);
			$sdate = strtotime($formData["secondary_reception_date"]);

			if($now > $pdate && $now < $sdate) {
				return "3";
			}

		}

		if($now > $date2) return "2";

		return "0";

    }

    //　編集可かどうかチェック
    function checkPropriety() {

    	if($GLOBALS["form"]->formData["edit_flg"] == "2") return false;

    	return true;
    }

    function get($form_id) {

    	$column = "*";
    	$from = "form";
    	$where[] = "form_id = ".$this->db->quote($form_id);
    	$where[] = "del_flg = 0";

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	return $rs;

    }

    function getList($condition, $page=1, $limit=ROW_LIMIT) {

    	$condition["form_name"]	= isset($condition["form_name"])	? $condition["form_name"]	: "";
    	$condition["form_type"]	= isset($condition["form_type"])	? $condition["form_type"]	: "";
    	$condition["syear"] 	= isset($condition["syear"])		? $condition["syear"]		: "";
    	$condition["smonth"]	= isset($condition["smonth"])		? $condition["smonth"]		: "";
    	$condition["sday"]		= isset($condition["sday"])			? $condition["sday"]		: "";
    	$condition["eyear"]		= isset($condition["eyear"])		? $condition["eyear"]		: "";
    	$condition["emonth"]	= isset($condition["emonth"])		? $condition["emonth"]		: "";
    	$condition["eday"]		= isset($condition["eday"])			? $condition["eday"]		: "";

    	$column = "*";
    	$from = "form";

    	$where[] = "del_flg = 0";
    	$where[] = "user_id = ".$this->db->quote($condition["user_id"]);

    	// フォーム名
    	if($condition["form_name"] != "") {
    		$condition["form_name"] = str_replace("　", " ", trim($condition["form_name"]));
    		$arrkey = explode(" ", $condition["form_name"]);
    		foreach($arrkey as $val) {
    			$where[] = "form_name like ".$this->db->quote("%".$val."%");
    		}
    	}

    	// フォームタイプ
    	if(is_array($condition["form_type"])) {
    		$subwhere = array();
    		foreach($condition["form_type"] as $val) {
    			if($val > 0) $subwhere[] = "type = ".$this->db->quote($val);
    		}
    		if(count($subwhere) > 0) {
    			$where[] = "(".implode(" OR ", $subwhere).")";
    		}
    		unset($subwhere);
    	}

    	// 受付期間　開始
    	if($condition["syear"] > 0) {
    		if($condition["smonth"] > 0 && $condition["sday"] > 0) {
    			$whereday = date("Y-m-d H:i:s", mktime(0, 0, 0, $condition["smonth"], $condition["sday"], $condition["syear"]));
    		} elseif($condition["smonth"] > 0) {
    			$whereday = date("Y-m-d H:i:s", mktime(0, 0, 0, $condition["smonth"], 1, $condition["syear"]));
    		} else {
    			$whereday = date("Y-m-d H:i:s", mktime(0, 0, 0, 1, 1, $condition["syear"]));
    		}
    		$where[] = "reception_date1 >= ".$this->db->quote($whereday);
    	}
    	// 受付期間　終了
    	if($condition["eyear"] > 0) {
    		if($condition["emonth"] > 0 && $condition["eday"] > 0) {
    			$whereday = date("Y-m-d H:i:s", mktime(23, 59, 59, $condition["emonth"], $condition["eday"], $condition["eyear"]));
    		} elseif($condition["smonth"] > 0) {
    			$whereday = date("Y-m-d H:i:s", mktime(23, 59, 59, $condition["emonth"], 1, $condition["eyear"]));
    		} else {
    			$whereday = date("Y-m-d H:i:s", mktime(23, 59, 59, 1, 1, $condition["eyear"]));
    		}
    		$where[] = "reception_date2 <= ".$this->db->quote($whereday);
    	}

    	$offset = $limit * ($page - 1);

    	$orderby = "udate desc";

    	$rs = $this->db->getListData("*", $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);

    	if(!$rs) {
    		$buffer["allrow"] = 0;
    	} else {
    		$buffer["list"] = $rs;
    		$buffer["allrow"] = $this->db->_rows;
    		$buffer["row"] = $this->db->_rowCount;
    	}

    	return $buffer;

    }

    // 同じログインIDが存在しないかチェックする　ログインIDが空、又は、重複するログインIDがある場合は、false
    function checkLoginId($form_id=0, $login_id="") {

    	if($login_id == "") return false;

    	$where[] = "login_id = ".$this->db->quote(trim($login_id));

    	if($form_id > 0) {
    		$where[] = "(form_id <> ".$this->db->quote($form_id) ." OR (super_flg <> 1 and form_id = ".$this->db->quote($form_id)."))";
    	}

    	///// 2013.04 全フォームを対象にチェックを行う
//     	if($form_id > 0) {
//     		$where[] = "form_id <> ".$this->db->quote($form_id);
//     	}

    	$rs = $this->db->_getOne("form_id", "form_admin", $where, __FILE__, __LINE__);
    	//$rs = $this->db->_getOne("form_id", "form", $where, __FILE__, __LINE__);

    	if(!$rs) return true;

    	return false;

    }

    // 同じサブ管理者ログインIDが存在しないかチェックする　ログインIDが空、又は、重複するログインIDがある場合は、false
    function checkSubLoginId($form_id=0, $login_id="") {

    	if($login_id == "") return false;

    	$where[] = "sub_login_id = ".$this->db->quote(trim($login_id));
    	if($form_id > 0) {
    		$where[] = "form_id <> ".$this->db->quote($form_id);
    	}

    	$rs = $this->db->_getOne("form_id", "form", $where, __FILE__, __LINE__);

    	if(!$rs) return true;

    	return false;

    }

    function getListItem($form_id="") {

    	if($form_id == "") return;

    	$where[] = "form_id = ".$this->db->quote($form_id);
    	$orderby = "item_id";

    	$rs = $this->db->getListData("*", "form_item", $where, $orderby, -1, -1, __FILE__, __LINE__);

    	if(!$rs) return;

    	foreach($rs as $val) {
    		foreach($val as $key=>$val2) {
    			if($key == "item_check") {
    				$buffer["item_check"][$val["item_id"]] = ($val2 == "") ? array() : explode('|', $val2);
    			} elseif($key == "form_id" || $key == "item_id" || $key == "rdate" || $key == "udate") {
    			} else {
    				$buffer[$key][$val["item_id"]] = $val2;
    			}

    		}
    	}

    	return $buffer;

    }

	/**
	 * フォーム文言情報取得
	 */
	function getFormWord($form_id = ""){

    	$column = "*";
    	$from = "form_word";
    	$where[] = "form_id = ".$this->db->quote($form_id);

    	$rs = $this->db->getData($column, $from, $where, __FILE__, __LINE__);

    	if(!$this->db->_rows > 0) return false;

    	//if(!$rs) return false;

    	// 2012/11/08
    	// 改行コードを統一
    	$rs['word3'] = str_replace(array("\r\n", "\r"), "\n", $rs['word3']);
    	$rs['word4'] = str_replace(array("\r\n", "\r"), "\n", $rs['word4']);

    	return $rs;

	}

	/**
	 * フォーム頭文字の重複チェック
	 */
	function checkFormHead($form_id=0, $ps_head){

    	$where[] = "head = ".$this->db->quote(trim($ps_head))."";
    	$where[] = "del_flg = 0";

    	//自分以外
    	if($form_id > 0) {
    		$where[] = "form_id <> ".$this->db->quote($form_id);
    	}

    	$rs = $this->db->_getOne("form_id", "form", $where, __FILE__, __LINE__);


    	if(!$rs) return true;

    	return false;

	}
}
?>