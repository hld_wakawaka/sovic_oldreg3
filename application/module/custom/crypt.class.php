<?php
require_once 'Crypt/Blowfish.php';

define("CRYPT_KEY", "LP1wGDPq7R4eYdKL");
define("CRYPT_KEY_S", "4bBPNNjY");

class CryptClass {
	
	function CryptClass() {}
	
	function crypt_encode($value) {
    	
    	if($value == "") return;
    	
    	$blowfish = Crypt_Blowfish::factory('cbc', CRYPT_KEY, CRYPT_KEY_S);
		$encrypted_data = $blowfish->encrypt(base64_encode($value));
    	
    	return $encrypted_data;
	}
    
	function crypt_decode($value) {
    	
    	if($value == "") return;
    	
    	$blowfish = Crypt_Blowfish::factory('cbc', CRYPT_KEY, CRYPT_KEY_S);
    	$decrypted = $blowfish->decrypt($value);
		if (PEAR::isError($decrypted)) {
			die($decrypted->getMessage() . "\n");
		}
		$decrypted_data = base64_decode($decrypted);
		
		return $decrypted_data;
    	
	}
	
}


?>