<?php

/**
 * 論文エントリーメソッド
 *
 * @package 論文フォーム
 * @subpackage module
 * @author salon
 *
 */
class Entry {


	//for update でレコードロックを実行するためDBオブジェクトはパラメータで渡す

	/**
	 * コンストラクタ
	 */
    function Entry() {

    }

	/**
	 * 論文フォーム　項目情報取得
	 * 　　
	 *
	 * @access public
	 * @param int form_id
	 */
    function getFormItem($po_db, $form_id) {

    	$column = "*";
    	$from = "form_item";
    	$where[] = "form_id = ".$po_db->quote($form_id);
    	$orderby = "item_id";

		$rs = $po_db->getListData($column, $from, $where, $orderby);

    	if(!$rs) {
    		return false;
    	}

    	return $rs;

    }

	/**
	 * 論文フォーム　エントリー情報取得
	 * 　　
	 *
	 * @access public
	 * @param int eid
	 */
	function getRntry_r($po_db, $pn_eid, $form_id){

    	$column = "*";
    	$from = "entory_r";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "form_id = ".$po_db->quote($form_id);
    	$where[] = "del_flg = 0";

    	$rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

    	return $rs;
	}



	/**
	 * 論文フォーム　エントリー情報(共著者)取得
	 * 　　
	 *
	 * @access public
	 * @param int eid
	 */
	function getRntry_aff($po_db, $pn_eid){

    	$column = "*";
    	$from = "entory_aff";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";
    	$orderby = "id";

		$rs = $po_db->getListData($column, $from, $where, $orderby);

    	return $rs;
	}

	/**
	 * 論文フォーム　エントリー情報(共著者)取得 共著者の数考慮
	 * 　　
	 *
	 * @access public
	 * @param int eid
	 */
	function getRntry_aff_ex($po_db, $pn_eid, $num=0){

		if($num == "" || $num == "0") return;

    	$column = "*";
    	$from = "entory_aff";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";
    	$orderby = "id";
    	$limit = $num;
    	$offset = "0";

		$rs = $po_db->getListData($column, $from, $where, $orderby, $offset, $limit, __FILE__, __LINE__);

    	return $rs;
	}



    function registEntryNumber($po_db, $form_id) {

        $rs = $po_db->_getOne("id", "entory_number", "form_id = ".$po_db->quote($form_id), __FILE__, __LINE__);
        if($rs) return;

        $param = array();
        $param["form_id"] = $form_id;
        $param["entory_count"] = "0";
        $param["order_count"]  = "0";

        $rs = $po_db->insert("entory_number", $param, __FILE__, __LINE__);
        if(!$rs) {
            $po_db->rollback();
            $this->complete("フォームの初期設定に失敗しました。");
            exit;
        }
        return;
    }


    /**
     * エントリー番号　最終番号取得
     *
     * @access public
     * @param object DB
     * @param int フォームID
     */
    function getEntryNum($po_db, $form_id){

        $column = "*";
        $from = "entory_number";
        $where[] = "form_id = ".$po_db->quote($form_id)." FOR UPDATE";

        $arrCount = $po_db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!$arrCount){
            // 採点テーブル作成の失敗
            $this->registEntryNumber($po_db, $form_id);
            $arrCount = $this->getEntryNum($po_db, $form_id);
        }

        return $arrCount;

    }

     /**
     * エントリー番号採番
     *
     * @access public
     * @param object DB
     * @param int フォームID
     */
    function updEntryNum($po_db, $form_id, $num){

        $param = array();
        $param["entory_count"] = $num;
        $param["udate"]        = "NOW";
        $where = "form_id = ".$po_db->quote($form_id);

        $rs = $po_db->update("entory_number", $param, $where, __FILE__, __LINE__);
        if(!$rs) {
            return false;
        }

        return true;
    }


     /**
     * 取引番号採番
     *
     * @access public
     * @param object DB
     * @param int フォームID
     */
    function updOrderNum($po_db, $form_id, $num){

        $param = array();
        $param["order_count"] = $num;
        $param["udate"]       = "NOW";
        $where = "form_id = ".$po_db->quote($form_id);

        $rs = $po_db->update("entory_number", $param, $where, __FILE__, __LINE__);
        if(!$rs) {
            return false;
        }

        return true;
    }


	/**
	 *　パスワード自動生成
	 *
	 * @access public
	 * @param int 生成するパスワードの長さ
	 * @param string パスワードで使用する文字列の組み合わせタイプ
	 *
	 */
	function makePasswd($length , $mode = 'alnum'){

		if ($length < 1 || $length > 256) {
			return false;
		}

		$password = "";

		$smallAlphabet = 'abcdefghijklmnopqrstuvwxyz';
		$largeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$numeric       = '0123456789';

		switch ($mode){
			//----------------
			// 小文字英字
			//----------------
			case 'small':
			    $chars = $smallAlphabet;
			    break;

			//-----------------
			// 大文字英字
			//-----------------
			case 'large':
			    $chars = $largeAlphabet;
			    break;

			//-----------------
			// 小文字英数字
			//-----------------
			case 'smallalnum':
			    $chars = $smallAlphabet . $numeric;
			    break;

			//-----------------
			// 大文字英数字
			//-----------------
			case 'largealnum':
			    $chars = $largeAlphabet . $numeric;
			    break;

			//-----------------
			// 数字
			//-----------------
			case 'num':
			    $chars = $numeric;
			    break;

			//-------------------
			// 大小文字英字
			//-------------------
			case 'alphabet':
			    $chars = $smallAlphabet . $largeAlphabet;
			    break;

			//--------------------
			// 大小文字英数字
			//--------------------
			case 'alnum':
			default:
			    $chars = $smallAlphabet . $largeAlphabet . $numeric;
			    break;
		}

		$charsLength = strlen($chars);


		for ($i = 0; $i < $length; $i++) {
		    $num = mt_rand(0, $charsLength - 1);
		    $password .= $chars{$num};
		}

		return $password;

	}


	/**
	 * 決済情報取得
	 * 	指定したエントリーの決済情報を取得する
	 */
	function getAuthorize($po_db, $pn_eid){

    	$column = "*";
    	$from = "payment";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";

    	$rs = $po_db->getData($column, $from, $where, __FILE__, __LINE__);

    	return $rs;


	}

	/**
	 * 支払詳細項目取得
     * 
     * @param GeneralDb $po_db              データベース接続オブジェクト
     * @param integer   $pn_eid             取得対象のeid
     * @param integer   $ps_type            取得対象（0:Fee,1:その他決済項目）
     * @param boolean   $ignore_get_flg     false:金額情報を取得、true:金額情報を無条件で取得しない
	 */
	function getPaymentDetail($po_db, $pn_eid, $ps_type="", $ignore_get_flg=false){
    	$from = "payment_detail";
    	$where[] = "eid = ".$po_db->quote($pn_eid);
    	$where[] = "del_flg = 0";

    	if($ps_type != ""){
    		$where[] = "type = ".$po_db->quote($ps_type);
    	}


		$orderby = "detail_id, type";

		$rs = $po_db->getListData("*", $from, $where, $orderby, -1, -1, __FILE__, __LINE__);
        // Fee非表示設定 @ Config.fee_hidden
        if($ps_type != 1 && $ignore_get_flg){
            foreach($rs as $_key => $_payment_detail){
                if($_payment_detail['type'] == 0){
                    unset($rs[$_key]);
                }
            }
        }

		return $rs;



	}

	function getListFromId($arrId=array()) {

		if(!count($arrId) > 0) return;

		$from = $this->getTable($GLOBALS["userData"]["type"]);

		$db = new DbGeneral;

		$where[] = "eid in (".implode(",", $arrId).")";
		$where[] = "del_flg = 0";
		$where[] = "form_id = ".$db->quote($GLOBALS["userData"]["form_id"]);
		$orderby = "udate desc";

		$rs = $db->getListData("*", $from, $where, $orderby, -1, -1, __FILE__, __LINE__);

		return $rs;

	}

	function getTable($type) {

		switch($type) {
			case "1":
			case "2":
			case "3":
				return "v_entry";
				break;
			default:
				return "";
				break;

		}
		return "v_entry";
	}
}
?>