<?php

//
// HSession.php
// $Id: HSession.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
// セッションクラス

/*
 * session level
 */
define('HSLV_FOREVER', 0);
define('HSLV_LOGIN', 10);
define('HSLV_LOGIN_ADMIN', 15);
define('HSLV_CART', 20);
define('HSLV_LOCAL', 30);
/* セッションの基本的な名前空間 */
define('SESSION_NAME_SPACE', 'sovic_reg3_'.ENV);

class HSession{

    // constructor
    function HSession(){
        // セキュア属性（SSLの場合のみ）
        if(ENV == ENV_TYPE3){
            ini_set('session.cookie_secure', 1);
        }
        
        session_start();
        $this->setNameSpace();
        if($this->getRg()){
            session_register(HSESSION_VARIABLE_NAME);
        }
    }


    /**
     * アプリのセッション名前空間を定義する
     * Mngログイン状況によって名前空間を判定するため必ずsession_startを呼び出したあとに実行する
     *
     * @param  void
     * @return void
     */
    function setNameSpace(){
        if (PAGE_RANK == "usr"){
            /**
             * フォーム間のセッション競合を考慮
             * Mng経由はMngとして扱う
             * Mngを別namespaceを定義すると一時的にログアウトとして扱われるため
             */
            $form_id = isset($_GET ['form_id']) ? $_GET ['form_id'] : "";
            // Mngにログインしているかチェック
            list($val, $level) = $this->getVarLevel('isLogin', SESSION_NAME_SPACE);
            // Mngにログインしている場合はMngとして扱う
            if($val){
                define('HSESSION_VARIABLE_NAME', SESSION_NAME_SPACE);
            // Usrとして扱う
            }else{
                // form_idの指定あり # Usr
                if(strlen($form_id) > 0){
                    define('HSESSION_VARIABLE_NAME', SESSION_NAME_SPACE . $form_id);
                // form_idの指定なし # Usrでない
                }else{
                    define('HSESSION_VARIABLE_NAME', SESSION_NAME_SPACE);
                }
            }
        }else{
            define('HSESSION_VARIABLE_NAME', SESSION_NAME_SPACE);
        }
    }


    function clear(){
        session_unregister(HSESSION_VARIABLE_NAME);
    }


    function destroy(){
        session_destroy();
    }


    function isAvail(){
        return session_is_registered(HSESSION_VARIABLE_NAME);
    }


    // セッション変数を設定する
    function setVar($name, $val, $level = HSLV_FOREVER){
        $vars = HSESSION_VARIABLE_NAME;
        if($this->getRg()){
            global $$vars;
            if(!isset($$vars)) $$vars = array ();
            ${$vars}[$name] = array ($val,$level);
        }else{
            $_SESSION[$vars][$name] = array ($val, $level);
        }
    }


    function getVarLevel($name, $vars = HSESSION_VARIABLE_NAME){
        if(!$this->issetVar($name, $vars)) return;
        
        if($this->getRg()){
            global $$vars;
            list($val, $level) = ${$vars}[$name];
        }else{
            list($val, $level) = $_SESSION[$vars][$name];
        }
        return array($val, $level);
    }


    // セッション変数から値を取り出す
    function getVar($name){
        list($val, $level) = $this->getVarLevel($name);
        return $val;
    }


    function getLevel($name){
        list($val, $level) = $this->getVarLevel($name);
        return $level;
    }


    function issetVar($name, $vars = HSESSION_VARIABLE_NAME){
        if($this->getRg()){
            global $$vars;
            $result = isset(${$vars}[$name]);
        }else{
            $result = isset($_SESSION[$vars][$name]);
        }
        return $result;
    }


    function unsetVar($name){
        $vars = HSESSION_VARIABLE_NAME;
        if($this->getRg()){
            global $$vars;
            unset(${$vars}[$name]);
        }else{
            unset($_SESSION[$vars][$name]);
        }
    }


    function getAllInfo(){
        $vars = HSESSION_VARIABLE_NAME;
        $result = array ();
        if($this->getRg()){
            global $$vars;
            if(isset($$vars)){
                foreach($$vars as $name => $pair){
                    list ($val, $level) = $pair;
                    $result[$name] = $level;
                }
            }
        }else{
            if(isset($_SESSION[$vars]) && is_array($_SESSION[$vars])){
                foreach($_SESSION[$vars] as $name => $pair){
                    list($val, $level) = $pair;
                    $result[$name] = $level;
                }
            }
        }
        return $result;
    }


    // あるセッションレベルのセッション変数を全て削除
    function unsetAllAt($level){
        $infos = $this->getAllInfo();
        foreach($infos as $name => $lv){
            if($lv == $level){
                $this->unsetVar($name);
            }
        }
    }


    function unsetAllOver($level){
        $infos = $this->getAllInfo();
        foreach($infos as $name => $lv){
            if($lv > $level){
                $this->unsetVar($name);
            }
        }
    }


    function unsetAllOverAt($level){
        $infos = $this->getAllInfo();
        foreach($infos as $name => $lv){
            if ($lv >= $level){
                $this->unsetVar($name);
            }
        }
    }


    // utilities
    function getRg(){
        return get_cfg_var('register_globals');
    }


    // for debugging
    function dump(){
        $infos = $this->getAllInfo();
        foreach($infos as $name => $lv){
            list($val, $level) = $this->getVarLevel($name);
            echo "name: $name, val: $val, level: $level<br>\n";
        }
    }

}

?>
