<?php
require_once(MODULE_DIR."GeneralFnc.class.php");

class log_class {

    var $message;
    var $filename;
    var $filenameDb;

    function log_class() {
        $this->filename   = LOG_DIR."daily/".date('Ymd').".log";
        $this->filenameDb = LOG_DIR."DBLog/DB_".date('Ymd').".log";
        $this->filenameEr = LOG_DIR."errorLog/Error_".date('Ymd').".log";
    }


    /**
     * 操作ログ
     * @deprecated
     */
    public function write_log($str, $option="") {
        $str = str_replace(array("\r\n", "\r", "\n", "\t"), '', $str);
        $logstr = $str."\t".$option;
        log_class::printLog($logstr);
    }


    /**
     * DBログ
     * @deprecated
     */
    public function write_log_DB($str) {

//        $str = str_replace(array("\r\n", "\r", "\n", "\t"), '', $str);
        $logstr = date('Y-m-d H:i:s')."\t".$_SERVER["REMOTE_ADDR"]."\t".$str."\t".$_SERVER["REQUEST_URI"]."\n";

        error_log($logstr, 3, $this->filenameDb);
    }



    public static function printLog($msg, $path=""){
        // ログイン情報をログに残す
        if(isset($GLOBALS["session"])){
            if ($GLOBALS["session"]->getVar("isLogin")) {
                $form_id = isset($GLOBALS["userData"]["form_id"]) ? $GLOBALS["userData"]["form_id"] : "";
                if(strlen($form_id) == 0) $form_id = isset($_GET['form_id']) ? $_GET['form_id'] : "";
                $msg = "User_".$form_id."\t".$msg;
            }else if($GLOBALS["session"]->getVar("isLoginAdmin")) {
                $msg = "Admin_".$GLOBALS["adminData"]["admin_id"]."\t".$msg;
            }
        }

        // 日付の取得
        $now = date('Y-m-d H:i:s');

        // アクセス元ユーザーエージェント
        $ua = Utils::getUserAgent();

        // 出力パスの作成
        if(strlen($path) === 0){
            // Sys,Mng,Sysに振り分ける
            $path = USR_LOG_FILE;
            $dofunction = Utils::getFunction();
            if($dofunction == FUNCTION_SYS){
                $path = SYS_LOG_FILE;
            }else if($dofunction == FUNCTION_MNG){
                $path = MNG_LOG_FILE;
            }
        }


        $script_name = isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : "";
        $remote_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";
        $msg = "$now [{$script_name}] $msg from {$remote_addr}. UserAgent:{$ua}\n";
//        if ($verbose) {
//            if (GC_Utils_Ex::isFrontFunction()) {
//                $msg .= 'customer_id = ' . $_SESSION['customer']['customer_id'] . "\n";
//            }
//            if (GC_Utils_Ex::isAdminFunction()) {
//                $msg .= 'login_id = ' . $_SESSION['login_id'] . '(' . $_SESSION['authority'] . ')' . '[' . session_id() . ']' . "\n";
//            }
//            $msg .= GC_Utils_Ex::toStringBacktrace(GC_Utils_Ex::getDebugBacktrace());
//        }

        error_log($msg, 3, $path);
    }


}
?>