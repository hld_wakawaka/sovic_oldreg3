<?php
/**
 * Downloadクラス
 *
 * @package 論文フォーム
 * @subpackage module
 * @author salon
 *
 */

class Download{


	/**
	 * CSVダウンロード
	 * 	CSV形式のデータを任意のファイル名でCSVファイルとしてダウンロードする
	 *
	 * @access public
	 * @param string 出力データ
	 * @param string ダウンロードするときのファイル名
	 *
	 */
	function csv($ps_data, $ps_filename){

//        header("Content-disposition: inline; filename=" . urlencode($ps_filename));
        header("Content-disposition: attachment; filename=" . urlencode($ps_filename));
        header("Content-type: application/octet-stream-extorder; name=" . urlencode($ps_filename));
        header('Pragma: public');
        header("Cache-Control: public");

		echo $ps_data;
		exit;
	}

	/**
	 * ファイルダウンロード実行
	 *
	 * @access public
	 * @param string ダウンロードするファイル（フルパス）
	 * @param stirng ダウンロードするときのファイル名
	 * @param int ダウンロード後にファイルを削除　1:削除する
	 *
	 */
	function file($ps_readfile, $ps_filename="", $pn_del = ""){
        ob_start();

        $ws_ua=$_SERVER['HTTP_USER_AGENT'];

        //IEの場合、SJISに変換
        if (strstr($ws_ua, 'MSIE')) {
            $ps_filename = mb_convert_encoding($ps_filename, "SJIS", "UTF-8");


        //Safariの場合、Content-dispositionのfilenameは""とする

        //""とした場合、$_SERVER['PATH_INFO']のファイル名が使用される
        //（Safari　文字化け対応）
        } elseif (strstr($ws_ua, "Safari")) {
            //$ps_filename = "";
            $ps_filename = mb_convert_encoding($ps_filename, "SJIS", "UTF-8");


        //FireFoxの場合、そのまま（またはUTF-8に変換）
        } else {
           $ps_filename = mb_convert_encoding($ps_filename, "SJIS", "UTF-8");
        }
        header("Content-type: application/octet-stream-sovic;");
        header("Content-Disposition: attachment; filename=" . $ps_filename);
        header('Pragma: public');
        header("Cache-Control: public");
        ob_end_clean();
        flush();
        readfile($ps_readfile);

		//ダウンロード後に削除
		if($pn_del == "1"){
			unlink($ps_readfile);
		}

        exit;
	}










}

?>
