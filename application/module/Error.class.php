<?php
/**
 * 共通エラーメッセージ ****************************
 */

// 未入力エラー
define("ERR_REQUIRE_INPUT_PARAM", "%sを入力してください。");
// 未選択エラー
define("ERR_REQUIRE_SELECT_PARAM", "%sを選択してください");
// 未選択エラー
define("ERR_REQUIRE_CHECK_PARAM", "%sをチェックしてください。");

// 数値エラー
define("ERR_NUMERIC", "%sは数字で入力してください。");
// 半角数値エラー
define("ERR_HAN_NUMERIC", "%sは、半角数字で入力してください。");
// 全角数値エラー
define("ERR_ZEN_NUMERIC", "%sは、全角数字で入力してください。");
// 半角英数字エラー
define("ERR_HAN_ALPHANUMERIC", "%sは、半角英数字で入力してください。");
// 全角カタカナエラー
define("ERR_ZENKANA", "%sは全角カタカナで入力してください。");
// 半角英数記号エラー
define("ERR_HAN_ALPHANUMERICMARK", "%sは、半角英数記号で入力してください。");
// 全角英数字エラー
define("ERR_ZEN_ALPHANUMERIC", "%sは、全角英数字で入力してください。");

//全角エラー
define("ERR_ZEN", "%sは、全角で入力してください。");

//バイト数エラー
define("ERR_BYTE", "%sは、%sbyteで入力してください。");

// メールアドレスエラー
define("ERR_MAIL", "%sは、正しいメールアドレスではありません。");

// 入力文字数エラー
define("ERR_STRLEN", "%sは、%s文字で入力してください。");
// 入力文字数エラー
define("ERR_STRLEN_MAX", "%sは、%s文字以内で入力してください。");
// 入力文字数エラー
define("ERR_STRLEN_BETWEEN", "%sは、%s文字から%s文字で入力してください。");

// ファイルサイズエラー
define("ERR_OVER_MAX_FILE_SIZE", "%sのファイルサイズが大きすぎます。　%sバイト以下のファイルを選択してください。");

// ファイル拡張子エラー
define("ERR_SUFFIX_FILE", "%sのファイル拡張子はアップロードできません。　アップロード可能な拡張子は%sです。");

// 入力形式エラー
define("ERR_FORMAT", "%sを確認してください。");

// 入力形式エラー
define("ERR_FORMAT_HAN_NUMERIC_HAIFUN", "%sを確認してください。(半角数字、ハイフン付きで入力ください)");

// 入力形式エラー
define("ERR_FORMAT_HAN_ALPHANUMERIC", "%sを確認してください。(半角英数字で入力してください)");

// 重複エラー
define("ERR_OVER_LAP", "入力した%sは、既に登録済みです。");

// 存在なしエラー
define("ERR_NOT_EXIST", "%sが見つかりません。");

// 処理手順不正エラー
define("ERR_REQUIRE_PARAMETER", "処理に必要な情報が不足しています。お手数ですが最初からやり直してください。");

// DBシステムメンテナンスエラー
define("ERR_DB_SYSTEM", "只今、システムメンテナンス中です。しばらく経ってから再度アクセスしてください。");

/**
 * 独自エラーメッセージ ****************************
 */

// メンバーIDの未登録エラー
define("ERR_NOT_MEMBER_ID", "ログインID又は、パスワードが間違っています。");
// パスワードの間違いエラー
define("ERR_MISTAKE_PASSWORD", "ログインID又は、パスワードが間違っています。");
// アカウントロックのエラー
define("ERR_ACCOUNT_LOCK", "規程回数ログインに失敗したため、アカウントがロックされました。");

/**
 * 管理画面ログイン用エラーメッセージ　*********************
 */
// ID間違い
define("ERR_NOT_ADMIN_MAIL", "ログインID又は、パスワードが間違っています。");
// パスワード間違い
define("ERR_MISTAKE_ADMIN_PASSWORD", "ログインID又は、パスワードが間違っています。");


class Error {

    static function showErrorPage($errMsg, $returnPage = "", $returnPageName = "戻る", $tmplDir = TEMPLATES_DIR, $tmplFile = TMPL_FILE_ERROR) {

        $errorParam["errMsg"]            = $errMsg;
        $errorParam["returnPage"]        = $returnPage;
        $errorParam["returnPageName"]    = $returnPageName;
        $errorParam["tmplDir"]            = $tmplDir;
        $errorParam["tmplFile"]            = $tmplFile;
        $errorParam["_REQUEST"]            = $_REQUEST;

        if(isset($GLOBALS["session"])) {
            $GLOBALS["session"]->setVar("errorParam", $errorParam, HSLV_LOCAL);
        }

        $query = PAGE_RANK == "usr" ? "?form_id=".$GLOBALS["form"]->formData['form_id'] : "";
        header("location: ".URL_MESSAGE_PAGE.$query);
        exit;
    }

    static function showSystemError($errMsg, $returnPage = "", $tmplDir = TEMPLATES_DIR, $tmplFile = TMPL_FILE_ERROR){

        Error::showErrorPage($errMsg, $returnPage, "戻る", $tmplDir, $tmplFile);
        exit;
    }
}
?>
