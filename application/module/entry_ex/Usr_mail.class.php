<?php

class Usr_mail {

    /** 
     * 登録完了メールの送信
     */
    function sendRegistMail($obj, $user_id, $passwd) {
        // 3Dセキュアの場合は稀にセッションがクリアされるので取り直す
        // おまじないで全パターンも取り直すように変更
        $obj->getEntry();
        // passwordを最新の状態から取り出す
        $ss_user = $GLOBALS["session"]->getVar("SS_USER");
        $passwd  = $ss_user["e_user_passwd"];

        //タイトル
        $obj->subject = ($obj->formdata["lang"] == LANG_JPN) ? "エントリー完了通知メール" : "Notice: Your registration has been completed";
        if($obj->formWord["word11"] != ""){
            $obj->subject = $obj->formWord["word11"];
        }
        $obj->subject = @ereg_replace("_ID_", $user_id, $obj->subject);

        // メール送信
        $obj->sendCompleteMail($user_id, $passwd, 1);
        
    }


    /**
     * 編集完了メールの送信
     */
    function sendEditMail($obj, $user_id, $passwd) {
        // 3Dセキュアの場合は稀にセッションがクリアされるので取り直す
        // おまじないで全パターンも取り直すように変更
        $obj->getEntry();
        // passwordを最新の状態から取り出す
        $ss_user = $GLOBALS["session"]->getVar("SS_USER");
        $passwd  = $ss_user["e_user_passwd"];

        //タイトル
        $obj->subject = ($obj->formdata["lang"] == LANG_JPN) ? "エントリー情報変更通知メール" : "Notice: Information has been edited";
        if($obj->formWord["word12"] != ""){
            $obj->subject = $obj->formWord["word12"];
        }
        $obj->subject = @ereg_replace("_ID_", $user_id, $obj->subject);
        
        // メール送信
        $obj->sendCompleteMail($user_id, $passwd, 2);
        
    }


    /*
     * 完了メールの送信
     */
    function sendCompleteMail($obj, $user_id, $passwd, $exec_type=1) {
        
        if(in_array($obj->form_id, $obj->sendmail_not)) return;

        // 英語フォームはUTF-8送信
        $isUTF8 = ($obj->formdata["lang"] == LANG_JPN) ? false : true;

        //本文
        $ws_mailBody = $obj->makeMailBody($user_id, $passwd, $exec_type);

        // 登録者へのメール
        if($obj->admin_flg == "" && $obj->arrForm["edata25"] != ""){
            $obj->o_mail->SendMail($obj->arrForm["edata25"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"], $obj->formWord["word13"], $isUTF8);
        }

        // 管理者へのメール
        $obj->o_mail->SendMail($obj->formdata["form_mail"], $obj->subject, $ws_mailBody, $obj->formdata["form_mail"],$obj->formWord["word13"], $isUTF8);
        return;
    }


    function makeMailBody($obj, $user_id="", $passwd="", $exec_type=""){
        // カスタマイズテンプレート設定の読み込み
        Config::load($obj);

        // メール上部コメントの作成
        $head_comment = $obj->makeMailBody_header($user_id, $passwd, $exec_type);

        // 入力フォームのメール本文
        $arrbody = $obj->makeFormBody($exec_type);


        //----------------------------------------
        //本文生成
        //----------------------------------------
        $body  = "";
        $body .= $head_comment."\n\n";
        $body .= $arrbody[1];
        if($obj->formdata["group2_use"] != "1") $body .= $arrbody[2];
        if($obj->formdata["group3_use"] != "1") $body .= $arrbody[3];
        if($obj->formdata["kessai_flg"] == "1") $body .= $obj->makePaymentBody($exec_type);
        $body .= $obj->makeEditBody($user_id, $passwd, $exec_type);

        $body .= "\n";
        if(isset($obj->contact_before)) $body .= $obj->contact_before;
        $body .= "\n------------------------------------------------------------------------------\n";
        $body .= $obj->formdata["contact"];
        $body .= "\n------------------------------------------------------------------------------\n";
        if(isset($obj->contact_after)) $body .= $obj->contact_after;

        return str_replace(array("\r\n", "\r"), "\n", $body);
    }


    /**
     * メールの上部コメントを作成する
     */
    function makeMailBody_header($obj, $user_id="", $passwd ="", $exec_type = "") {
        $form_id = $obj->formdata['form_id'];
        
        // 置換文字列適用
        switch($exec_type){
            // 新規の場合
            case "1":
                $eid     = $obj->ins_eid;
                $ret_str = Usr_function::wordReplace($obj->formWord["word3"], $obj, compact("form_id", "eid", "exec_type"));
                break;
            
            // 更新の場合
            case "2":
                $eid     = $obj->eid;
                $ret_str = Usr_function::wordReplace($obj->formWord["word4"], $obj, compact("form_id", "eid", "exec_type"));
                break;
            
            default:
                $ret_str = "";
                break;
        }
        
        // タグを適用する
        $ret_str = $obj->makeMailBody_header_replace_tag($ret_str);
        
        return $ret_str;
    }


    /**
     * メールの上部コメントを作成する # 銀行振り込みまたはうレジットカードで表示を切り替える
     */
    function makeMailBody_header_replace_tag($obj, $body_header){
        $tag = "";
        $taglist = array("bank", "credit", "other");
        
        switch($obj->arrForm["method"]){
            // クレジットカード
            case "1":
                $tag = "credit";
                break;
            
            // 銀行振込
            case "2":
                $tag = "bank";
                break;
            
            // その他
            case "99":
                $tag = "other";
                break;
            
            default:
                break;
        }
        
        // タグ除去
        if(strlen($tag) > 0){
            // 他のタグを削除
            foreach($taglist as $_del){
                if($_del == $tag) continue;
                $pattern = sprintf("!<%s.*?>.*?</%s>!ims", $_del, $_del);
                $body_header = preg_replace($pattern, "", $body_header);
            }
            $body_header = strip_tags($body_header);
        }

        return $body_header;
    }


    function makeFormBody($obj, $exec_type){
        $arrbody = array();

        // 言語別設定
        $obj->point_mark = ($obj->formdata["lang"] == LANG_JPN) ? "■" : "*";

        // 編集時は更新の前後を表示する
        if($exec_type == 2 && $obj->editmail_diff){
            // 編集の前後の差分
            $obj->arrDiff = Usr_entryDB::getParamEditDiff($obj);

            // グループ1
            $obj->makeDiffBodyGroup1($arrbody, $exec_type);

            // グループ2
            $obj->makeDiffBodyGroup2($arrbody, $exec_type);

            // グループ3
            $obj->makeDiffBodyGroup3($arrbody, $exec_type);


            return $arrbody;
        }


        // グループ1
        $obj->makeBodyGroup1($arrbody, $exec_type);

        // グループ2
        $obj->makeBodyGroup2($arrbody, $exec_type);

        // グループ3
        $obj->makeBodyGroup3($arrbody, $exec_type);


        return $arrbody;
    }



    function makeBodyGroup1($obj, &$arrbody, $exec_type){
        // グループ1
        $group = 1;

        $body_obi  = ""; // 帯
        $body_main = ""; // 本文

        // 言語別設定
        if($obj->formdata["lang"] == LANG_JPN){
            $body_obi = ($obj->formdata["group1"] != "") ? "【".$obj->formdata["group1"]."】\n\n" : "";
        }else{
            $body_obi = ($obj->formdata["group1"] != "") ? "[".$obj->formdata["group1"]."]\n\n" : "";
        }

        // メールに出力する項目数
        $count = 0;
        foreach($obj->arrItemData[$group] as $_key => $data){
            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;
            $count++;

            $key = $data["item_id"];

            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            if(!isset($obj->arrForm["edata".$key])){
                $obj->arrForm["edata".$key] = "";
            }

            $methodname = "mailfunc".$key;
            if(method_exists($obj, $methodname)) {
                $body_main .= $obj->$methodname($name);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $body_main .= $obj->exClass->$methodname($obj, $key, $name);

                }else{
                    if($data["controltype"] == "1") {
                        $body_main .= $obj->mailfuncNini($group, $key, $name);    //任意
                    } else {
                        $body_main .= Config::assign_obi(array("mode"=>"mail", "item_id"=>$key), $obj->_smarty);
                        $body_main .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key]."\n";
                    }
                }
            }
        }

        /**
         * 帯と本文を結合
         *    出力項目数が0の場合は帯も非表示とする
         * */
        $arrbody[$group] = "";
        if($count){
            $arrbody[$group] = $body_obi.$body_main;
        }
    }


    function makeBodyGroup2($obj, &$arrbody, $exec_type){
        // グループ2が存在する時
        $group = 2;
        if($obj->formdata["group2_use"] != "1") {

            // 言語別設定
            $arrbody[$group] = "";

            // 言語別設定
            if($obj->formdata["lang"] == LANG_JPN){
                $body2_title = ($obj->formdata["group2"] != "") ? "\n\n【".$obj->formdata["group2"]."】\n\n" : "\n\n";
            }else{
                $body2_title = ($obj->formdata["group2"] != "") ? "\n\n[".$obj->formdata["group2"]."]\n\n" : "\n\n";
            }


            if(isset($obj->arrForm["edata31"]) && $obj->arrForm["edata31"] != "") {
                if(!isset($obj->arrForm["edata30"])) $obj->arrForm["edata30"] = 0;
                for($i = 0; $i < $obj->arrForm["edata30"]; $i++) {
                    if(isset($obj->arrForm["group2_del"])) {
                        if(in_array($i, $obj->arrForm["group2_del"])) {
                            continue;
                        }
                    }

                    $arrbody[$group] .= $body2_title;

                    foreach($obj->arrItemData[$group] as $_key => $data){
                        if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

                        $key = $data["item_id"];
                        $name = $data["strip_tags"];

                        if(!isset($obj->arrForm["edata".$key])){
                            $obj->arrForm["edata".$key] = "";
                        }

                        $methodname = "mailfunc".$key;
                        if(method_exists($obj, $methodname)) {
                            $arrbody[$group] .= $obj->$methodname($name, $i);
                        } else {
                            if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                                $arrbody[$group] .= $obj->exClass->$methodname($obj, $key, $name, $i);

                            }else{
                                if($data["controltype"] == "1") {
                                    $arrbody[$group] .= $obj->mailfuncNini($group, $key, $name, $i);    //任意
                                } else {
                                    if(!isset($obj->arrForm["edata".$key.$i])) $obj->arrForm["edata".$key.$i] = "";
                                    $arrbody[$group] .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key.$i]."\n";
                                }
                            }
                        }
                    }
                    $arrbody[$group] .= "\n";
                }
            }
        }
    }


    function makeBodyGroup3($obj, &$arrbody, $exec_type){
        // グループ3
        $group = 3;

        // グループ3のセクション毎に分ける
        $arrbody_obi  = array(1=>'', 2=>'', 3=>''); // 帯
        $arrbody_main = array(1=>'', 2=>'', 3=>''); // 本文
        $arrcount     = array(1=>0 , 2=>0 , 3=>0); // 出力項目数

        $preGroup3No = 0;
        foreach($obj->arrItemData[$group] as $_key => $data){
            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;


            // ファイルアップロード1は必須のみ表示判定をする
            if($data["item_id"] == 51){
                $filecheck= in_array($obj->arrForm["amount"], $obj->filecheck);
                if(!$filecheck) continue;
            }

            $group3no = $data["group3"];
            if($preGroup3No != $group3no){
                $preGroup3No = $group3no;

                // 言語別設定
                if(isset($obj->formdata["arrgroup3"][$preGroup3No-1])){
                    $format = "";
                    if($obj->formdata["lang"] == LANG_JPN){
                        $format = ($obj->formdata["arrgroup3"][$preGroup3No-1] != "") ? "\n\n【%s】\n\n" : "\n";
                    }
                    else{
                        $format = ($obj->formdata["arrgroup3"][$preGroup3No-1] != "") ? "\n\n[%s]\n\n" : "\n";
                    }
                    $arrbody_obi[$preGroup3No] = sprintf($format, $obj->formdata["arrgroup3"][$preGroup3No-1]);
                }
            }
            // 表示項目数を加算
            $arrcount[$preGroup3No]++;

            $key = $data["item_id"];

            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            if(!isset($obj->arrForm["edata".$key])){
                $obj->arrForm["edata".$key] = "";
            }

            $methodname = "mailfunc".$key;
            if(method_exists($obj, $methodname)) {
                $arrbody_main[$preGroup3No] .= $obj->$methodname($name);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $arrbody_main[$preGroup3No] .= $obj->exClass->$methodname($obj, $key, $name);

                }else{
                    if($data["controltype"] == "1") {
                        $arrbody_main[$preGroup3No] .= $obj->mailfuncNini($group, $key, $name);    //任意
                    } else {
                        $arrbody_main[$preGroup3No] .= $obj->point_mark.$name.": ".$obj->arrForm["edata".$key]."\n";
                    }
                }
            }
        }

        // 帯と本文を結合
        /**
         * 帯と本文を結合
         *    出力項目数が0の場合は帯も非表示とする
         * */
        $arrbody[$group] = "";
        if($arrcount[1] > 0) $arrbody[$group] .= $arrbody_obi[1].$arrbody_main[1];
        if($arrcount[2] > 0) $arrbody[$group] .= $arrbody_obi[2].$arrbody_main[2];
        if($arrcount[3] > 0) $arrbody[$group] .= $arrbody_obi[3].$arrbody_main[3];
    }


    function makePaymentBody($obj, $exec_type){
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //支払合計
        if($exec_type == "1"){
            $total = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        }else{
            $total = $GLOBALS["session"]->getVar("ss_total_payment");
        }


        if($obj->formdata["lang"] == LANG_JPN){
            $obj->point_mark = "■";
            $body_pay = "\n\n【お支払情報】\n\n";
        }else{
            $obj->point_mark = "*";
            $body_pay = "\n\n[Payment Information]\n\n";
        }

        //$body_pay .= "\n\n";
        if($obj->formdata["lang"] == LANG_JPN){
            // お支払確認ページの表示/非表示 : デフォルトは表示
            if(!in_array($obj->form_id, $obj->payment_not)){
                $body_pay .= $obj->point_mark."お支払方法: ".$obj->wa_method[$obj->arrForm["method"]]."\n";
            }
            $body_pay .= $obj->point_mark."金額: \n";
        }
        else{
            if($total > 0){
                // お支払確認ページの表示/非表示 : デフォルトは表示
                if(!in_array($obj->form_id, $obj->payment_not)){
                    $body_pay .= ($obj->arrForm["method"] != "3") 
                                ? $obj->point_mark."Payment:".$obj->wa_method[$obj->arrForm["method"]]."\n"
                                : $obj->point_mark."Payment:Cash on-site\n";
                }
            }
            $body_pay .= $obj->point_mark."Amount of Payment:\n";
        }


        // Fee
        $body_pay .= $obj->makePaymentBody1($exec_type);

        //その他決済がある場合
        $body_pay .= $obj->makePaymentBody2($exec_type);


        if($obj->formdata["lang"] == LANG_JPN){
            $body_pay .="　　　お支払合計金額：".number_format($total)."円\n\n";
        }
        else{
            $body_pay .="      Amount of Total Payment:".$obj->yen_mark.number_format($total)."\n\n";
        }


        // お支払情報のメール生成
        // お支払確認ページの表示/非表示 : デフォルトは表示
        if(!in_array($obj->form_id, $obj->payment_not)){
            $body_pay .= $obj->makePaymentMethodBody($total);
        }

        return $body_pay;
    }


    /** 金額のメール本文 */
    function makePaymentBody1($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //新規の場合
        if($exec_type == "1"){
            foreach($obj->wa_price as $p_key => $data){
                if(!isset($obj->arrForm["amount"])) continue;
                if($p_key == $obj->arrForm["amount"]){
                    $price = $data["p1"];
                    if(is_numeric($price)){
                        $price = number_format($data["p1"]);
                        $price = ($obj->formdata["lang"] == LANG_JPN)
                               ? $price."円"
                               : $obj->yen_mark.$price."";
                    }

                    // タグを除去
//                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                    $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["pricetype"] == "2"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .= "　　　".$data["name"]."：".$price."\n";
                        }
                        else{
                            $body_pay .= "      ".$data["name"].":".$price."\n";
                        }
                    }
                    else{
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .=  "　　　".$obj->formdata["csv_price_head0"]."：".$price."\n";
                        }
                        else{
                            $body_pay .=  "      ".$obj->formdata["csv_price_head0"].":".$price."\n";
                        }
                    }
                    break;
                }
            }
        }

        //更新の場合
        if($exec_type == "2"){
            //全てのその他決済で項目を生成しているため、出力は不要
        }
        return $body_pay;
    }


    /** その他決済のメール本文 */
    function makePaymentBody2($obj, $exec_type){
        $body_pay = "";
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return "";

        //その他決済がある場合
        if($obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){

            //新規の場合
            if($exec_type == "1"){
                foreach($obj->wa_ather_price  as $p_key => $data ){
                    if(!isset($obj->arrForm["ather_price".$p_key])) continue;
                    if($obj->arrForm["ather_price".$p_key] != "" && $obj->arrForm["ather_price".$p_key] != "0"){
                        $price = $data["p1"];

                        // Free, - => 0
                        if(!is_numeric($data["p1"])){
                            $data["p1"] = 0;
                        }
                        $sum = $data["p1"] * $obj->arrForm["ather_price".$p_key];

                        // タグを除去
//                        $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                        $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                        if($obj->formdata["lang"] == LANG_JPN){
                            if(is_numeric($price)){
                                $price = number_format($price)."円";
                            }
                            $body_pay .="　　　".$data["name"]."：".number_format($sum)."円　（".$price." ×".$obj->arrForm["ather_price".$p_key]."）\n";
                        }
                        else{
                            if(is_numeric($price)){
                                $price = $obj->yen_mark.number_format($price);
                            }
                            $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum)."  (".$price." ×".$obj->arrForm["ather_price".$p_key].")\n";
                        }
                    }
                }
                
            }

            //更新の場合
            if($exec_type == "2"){
                //更新の場合は、登録済みのデータから引っ張ってくる
                $wa_payment_detail = Entry::getPaymentDetail($obj->db, $obj->eid, "", $obj->hide_fee_area);

                foreach($wa_payment_detail as $data){
                    $price = $data["price"];
                    // Free, - => 0
                    if(!is_numeric($data["price"])){
                        $data["price"] = 0;
                    }
                    $sum = $data["price"]*$data["quantity"];

                    // タグを除去
//                    $data["name"] = GeneralFnc::smarty_modifier_del_tags($data["name"]);
                    $data["name"] = strip_tags($data["name"]);  // 終了タグがないタグを削除

                    if($obj->formdata["lang"] == LANG_JPN){
                        if(is_numeric($price)){
                            $price = $price."円";
                        }
                        $body_pay .="　　　".$data["name"]."：".number_format($sum)."円";
                    }
                    else{
                        if(is_numeric($price)){
                            $price = $obj->yen_mark.$price;
                        }
                        $body_pay .="      ".$data["name"].":".$obj->yen_mark.number_format($sum);
                    }

                    // その他決済の場合は内訳を後ろにつける
                    if($data["type"] == "1"){
                        if($obj->formdata["lang"] == LANG_JPN){
                            $body_pay .="　（".$price." ×".$data["quantity"]."）";
                        }
                        else{
                            $body_pay .="  (".$price." ×".$data["quantity"].")";
                        }
                    }
                    $body_pay .= "\n";
                }
            }
        }
        return $body_pay;
    }


    /** メール本文生成 お支払情報 */
    function makePaymentMethodBody($obj, $total){
        $body_pay = "";
        if(!$total > 0) return $body_pay;

        $actionName = "makePaymentMethodBody". $obj->arrForm["method"];
        if(method_exists($obj, $actionName) && strlen($obj->arrForm["method"]) > 0){
            $body_pay = $obj->$actionName($total);
        }

        return $body_pay;
    }


    function makePaymentMethodBody1($obj, $total){
        $body_pay = "";
        if(!$total > 0) return $body_pay;
        // オンライン決済のみ本文を生成する
        if($obj->formdata["credit"] != "2") return $body_pay;

        if($obj->formdata["lang"] == LANG_JPN){
            $wk_pay_card= "カード会社";
            $wk_pay_card_num = "クレジットカード番号";
            $wk_pay_card_limit = "有効期限";
            $wk_pay_card_meigi = "カード名義人";
            $wk_pay_month = "月";
            $wk_pay_year = "年";
        }
        else{
            $wk_pay_card= "Card Type";
            $wk_pay_card_num = "Card Number";
            $wk_pay_card_limit = "Expiration Date";
            $wk_pay_card_meigi = "Card Holder's Name";
            $wk_pay_month = "Month";
            $wk_pay_year = "Year";
        }

        $body_pay .=$obj->point_mark.$wk_pay_card.":".$obj->wa_card_type[$obj->arrForm["card_type"]]."\n";
        $body_pay .=$obj->point_mark.$wk_pay_card_num.":";

        // カード番号をマスクする桁数
        $a = 2;
        switch($obj->arrForm["card_type"]){
            //VISA
            //MasterCard
            //JCB
            case 1:
            case 2:
            case 5:
                $a = 16 -2;
                break;

            //Diners Club
            case 3:
                $a = 14 -2;
                break;

            //AMEX
            case 4:
                $a = 15 -2;
                break;
        }
        $card = "";
        $body_pay .= sprintf("%'*".$a."s", $card).substr($obj->arrForm["cardNumber"],-2)."\n";
        $body_pay .= $obj->point_mark.$wk_pay_card_limit.":".$obj->arrForm["cardExpire1"].$wk_pay_month."　".$obj->arrForm["cardExpire2"].$wk_pay_year."\n";
        $body_pay .= $obj->point_mark.$wk_pay_card_meigi.":".$obj->arrForm["cardHolder"]."\n";

        return $body_pay;
    }


    function makePaymentMethodBody2($obj, $total){
        $body_pay = "";
        if(!$total > 0) return $body_pay;

        if($obj->formdata["lang"] == LANG_JPN){
            $wk_pay_furikomi= "お振込み人名義";
            $wk_pay_bank = "お支払銀行名";
            $wk_pay_furikomibi = "お振込み日";
        }
        else{
            $wk_pay_furikomi= "Name of remitter";
            $wk_pay_bank = "Name of bank which you will remit";
            $wk_pay_furikomibi = "Date of remittance";
        }

        $pay2key = array("lessee", "bank", "closure");
        foreach($pay2key as $_key => $_val){
            if(!isset($obj->arrForm[$_val])) $obj->arrForm[$_val] = "";
        }

        $body_pay .=$obj->point_mark.$wk_pay_furikomi.":".$obj->arrForm["lessee"]."\n";
        $body_pay .=$obj->point_mark.$wk_pay_bank.":".$obj->arrForm["bank"]."\n";
        $body_pay .=$obj->point_mark.$wk_pay_furikomibi.":".$obj->arrForm["closure"]."\n";

        return $body_pay;
    }


    /**
     * 編集用URLのお知らせ
     * 
     */
    function makeEditBody($obj, $user_id="", $passwd="", $exec_type){
//        if($obj->formdata["type"] == "3")     return;
        if($obj->formdata["edit_flg"] != "1") return;

        // この欄の表示フラグが立ってない場合は非表示 @ Config.show_mail_edit_body
        if($obj->show_mail_edit_body != true) return;

        if(strlen($user_id) == 0) return;
        if(strlen($passwd) == 0) return;

        $form_id = $obj->formdata['form_id'];

        $body = "";
        if($obj->formdata["lang"] == LANG_JPN){
            $body .= "\n\n受付期間内は、下記のID,パスワードでログインし登録内容の編集が可能です。\n";
            $body .= SSL_URL_ROOT."Usr/Usr_login.php?form_id={$form_id}\n";
            $body .= "ID:".$user_id."\n";
            $body .= "パスワード:".$passwd."\n";
        }
        else{
            //新規の場合
            $ret_str = "";
            if($exec_type == "1"){
                $body .= "\n\nYou can log in to edit the booking information with the following ID and password during the application period.\n";
                $body .= SSL_URL_ROOT."Usr/Usr_login.php?form_id={$form_id}\n";
                $body .= "ID:".$user_id."\n";
                $body .= "PASSWORD:".$passwd."\n";
            }
            //更新の場合
            if($exec_type == "2"){
                $body .= "\n\nYou can log in to edit the booking information with the following ID and password during the application period.\n";
                $body .= SSL_URL_ROOT."Usr/Usr_login.php?form_id={$form_id}\n";
                $body .= "ID:".$user_id."\n";
                $body .= "PASSWORD:".$passwd."\n";
            }
        }
        
        return $body;
    }


    /**
     * 会員・非会員
     */
    function mailfunc8($obj, $name) {
        $str = $obj->point_mark.$name.": ";

        if(isset($obj->arrForm["edata8"]) && isset($obj->wa_kaiin[$obj->arrForm["edata8"]])){
            $str .=$obj->wa_kaiin[$obj->arrForm["edata8"]]."\n";
        }
        else{
            $str .= "\n";
        }

        return $str;
    }

    /**
     * 連絡先(勤務先・自宅)
     */
    function mailfunc16($obj, $name) {
        $str = $obj->point_mark.$name.": ";

        if(isset($obj->arrForm["edata16"]) && strlen($obj->arrForm["edata16"]) > 0){
            $str .= $obj->wa_contact[$obj->arrForm["edata16"]]."\n";
        }
        else{
            $str .= "\n";
        }

        return $str;
    }

    /**
     * 都道府県
     */
    function mailfunc18($obj, $name) {
        $str = $obj->point_mark.$name.": ";

        if($obj->formdata["lang"] != LANG_JPN){

            if(isset($obj->arrForm["edata18"])){
                $str .= $obj->arrForm["edata18"]."\n";
            }
            else{
                $str .= "\n";
            }
        }
        else{
            if(isset($obj->arrForm["edata18"]) && isset($GLOBALS["prefectureList"][$obj->arrForm["edata18"]])){
                $str .= $GLOBALS["prefectureList"][$obj->arrForm["edata18"]]."\n";
            }
            else{
                $str .= "\n";
            }
        }

        return $str;
    }

    /**
     * 共著者の有無
     */
    function mailfunc29($obj, $name) {
        if(isset($obj->arrForm["edata29"]) && strlen($obj->arrForm["edata29"]) > 0){
            return $obj->point_mark.$name.": ".$obj->wa_coAuthor[$obj->arrForm["edata29"]]."\n";
        }
        return $obj->point_mark.$name.": \n";
    }

    /*
     * 共著者の所属期間
     */
    function mailfunc31($obj, $name) {
        return $obj->point_mark.$name.":\n".$obj->arrForm["edata31"]."\n\n";
    }

    /**
     * 共著者　所属期間リスト
     */
    function mailfunc32($obj, $name, $i) {
        $str = $obj->point_mark.$name.": ";

        if(!isset($obj->arrForm["edata31"]) || $obj->arrForm["edata31"] == "") return $str."\n";

        $arrList = explode("\n", $obj->arrForm["edata31"]);

        foreach($arrList as $key=>$val) {
            $p_key = $key + 1;

            if(isset($obj->arrForm["edata32".$i.$p_key])) {
                if($obj->arrForm["edata32".$i.$p_key] == $p_key) {
                    $str .= trim($val)." ";
                }
            }
        }

        return $str."\n";
    }

    /**
     * 共著者・会員
     */
    function mailfunc41($obj, $name, $i) {
        if(!isset($obj->arrForm["edata41".$i]) || !isset($obj->wa_kaiin[$obj->arrForm["edata41".$i]])) {
            return $obj->point_mark.$name.": \n";
        }

        return $obj->point_mark.$name.": ".$obj->wa_kaiin[$obj->arrForm["edata41".$i]]."\n";
    }

    /**
     * 発表形式
     */
    function mailfunc48($obj, $name) {
        $str = $obj->point_mark.$name.": ";

        if($obj->arrForm["edata48"] != "" || isset($obj->itemData[48]["select"][$obj->arrForm["edata48"]])){
            $str .= $obj->itemData[48]["select"][$obj->arrForm["edata48"]]."\n";
        }
        else{
            $str .= "\n";
        }

        return $str;
    }

    /**
     * 抄録
     */
    function mailfunc49($obj, $name) {
        return $obj->point_mark.$name.": \n".$obj->arrForm["edata49"]."\n\n";
    }

    /**
     * 抄録英語
     */
    function mailfunc50($obj, $name) {
        return $obj->point_mark.$name.": \n".$obj->arrForm["edata50"]."\n\n";
    }

    /**
     * ファイルアップロード
     */
    function mailfunc51($obj, $name) {
        return $obj->mailfuncFile(51, $name);
    }
    function mailfunc52($obj, $name) {
        return $obj->mailfuncFile(52, $name);
    }
    function mailfunc53($obj, $name) {
        return $obj->mailfuncFile(53, $name);
    }

    /**
     * Mr. Mrs.
     */
    function mailfunc57($obj, $name) {
        
        $str  = "";
        $str .= Config::assign_obi(array("mode"=>"mail", "item_id"=>57), $obj->_smarty);
        $str .= $obj->point_mark.$name.": ";
        
        if($obj->arrForm["edata57"] != ""){
            $str .=$GLOBALS["titleList"][$obj->arrForm["edata57"]]."\n";
        }
        else{
            $str .="\n";
        }
        
        return $str;
    }

    /**
     * 共著者 Mr. Mrs.
     */
    function mailfunc61($obj, $name, $i) {
        
        $str = $obj->point_mark.$name.": ";
        
        if(isset($obj->arrForm["edata61".$i]) && $obj->arrForm["edata61".$i] != ""){
            $str .=$GLOBALS["titleList"][$obj->arrForm["edata61".$i]]."\n";
        }
        else{
            $str .="\n";
        }
        
        return $str;
        
    }

    /**
     * 国名　リストボックス
     */
    function mailfunc114($obj, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
            if(isset($obj->arrForm["edata114"]) && isset($GLOBALS["country"][$obj->arrForm["edata114"]])) {
                $str .= $GLOBALS["country"][$obj->arrForm["edata114"]]."\n";
            }
            else{
                $str .= "\n";
            }
        
        return $str;
        
    }

    function mailfuncFile($obj, $key, $name) {
        
        $str = $obj->point_mark.$name.": ";
        
        $file_name = isset($obj->arrForm["hd_file".$key]) ? $obj->arrForm["hd_file".$key] : "";

        if(isset($obj->arrForm["file_del".$key]) && $obj->arrForm["file_del".$key] == "1"){
            $file_name = "";
        }
        
        $str .= $file_name."\n";
        
        return $str;
        
    }

    /**
     * 任意
     */
    function mailfuncNini($obj, $group, $item_id, $name, $i=null) {
        $key = "edata".$item_id.$i;
        if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";
        $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);

        $str  = "";
        $str .= Config::assign_obi(array("mode"=>"mail", "item_id"=>$item_id), $obj->_smarty);
        $str .= $obj->point_mark.$name.": ".$value."\n";

        return $str;
    }

    /**
     * 任意の入力
     * @deprecated
     */
    function mailfuncNiniBody($obj, $group, $key, $i=null) {
        
        $str = '';
        $wk_key = "edata".$key.$i;
        if(!isset($obj->arrForm[$wk_key])) $obj->arrForm[$wk_key] = "";
        
        switch($obj->arrItemData[$group][$key]["item_type"]) {
            case "1":
                // テキストエリア
                $str .= "\n ".$obj->arrForm[$wk_key];
                break;
            case "2":
            case "4":
                // ラジオ、セレクト
                if(isset($obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]])) {
                    $str .= $obj->arrItemData[$group][$key]["select"][$obj->arrForm[$wk_key]];
                }
                break;
            case "3":
                // チェックボックス
                if(!isset($obj->arrItemData[$group][$key]["select"])) break;
                
                foreach($obj->arrItemData[$group][$key]["select"] as $n=>$val) {
                    if(!isset($obj->arrForm[$wk_key.$n])) continue;
                    if($obj->arrForm[$wk_key.$n] == $n) {
                        $str .= "\n ".$obj->arrItemData[$group][$key]["select"][$n];
                    }
                }
                
                break;
            default:
                // テキストボックス
                $str .= $obj->arrForm[$wk_key];
        }

        return $str;
    }


    //----------------------------------------------------------
    // ▽編集メールの差分表示
    //----------------------------------------------------------

    /* 編集時 # 更新前の差分の本文 # グループ1 */
    function makeDiffBodyGroup1($obj, &$arrbody, $exec_type){
        // グループ1
        $group = 1;

        // 言語別設定
        $key = "group".$group;
        if($obj->formdata["lang"] == LANG_JPN){
            $arrbody[$group] = ($obj->formdata[$key] != "") ? "【".$obj->formdata[$key]."】\n\n" : "";
        }else{
            $arrbody[$group] = ($obj->formdata[$key] != "") ? "[".$obj->formdata[$key]."]\n\n" : "";
        }

        // 差分なし
        if(!isset($obj->arrDiff[$group]) || empty($obj->arrDiff[$group])){
            $arrbody[$group] .= $GLOBALS["msg"]["edit_mail_non"];
            return;
        }


        $arrForm = $obj->arrForm;
        foreach($obj->arrDiff[$group] as $_key => $diff){
            if(strlen($diff->before) == 0) $diff->before = $GLOBALS["msg"]["edit_non"];
            if(strlen($diff->after)  == 0) $diff->after  = $GLOBALS["msg"]["edit_non"];

            // edataXX
            $item_id  = substr($_key, 5);
            $data = $obj->arrItemData[$group][$item_id];

            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            $methodname = "mailfuncDiff".$item_id;
            if(method_exists($obj, $methodname)) {
                $arrbody[$group] .= $obj->$methodname($name, $diff);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $arrbody[$group] .= $obj->exClass->$methodname($obj, $item_id, $name, $diff);

                }else{
                    // 任意項目
                    if($data["controltype"] == "1") {
                        $arrbody[$group] .= $obj->point_mark.$name.": ".$obj->mailfuncDiffNiniBody($group, $item_id, $diff);

                    // 標準項目 # テキストタイプ
                    } else {
                        $arrbody[$group] .= $obj->point_mark.$name.": ".$diff->before.'→'.$diff->after."\n";
                    }
                }
            }
            $arrbody[$group] .= "\n";
        }
        $obj->arrForm = $arrForm;
    }


    /* 編集時 # 更新前の差分の本文 # グループ1 */
    function makeDiffBodyGroup2($obj, &$arrbody, $exec_type){
        // グループ2が存在する時
        $group = 2;
        if($obj->formdata["group2_use"] != "1") {}
    }


    /* 編集時 # 更新前の差分の本文 # グループ1 */
    function makeDiffBodyGroup3($obj, &$arrbody, $exec_type){
        // グループ1
        $group = 3;

        $arrbody[$group] = "";

        // 差分なし
        if(!isset($obj->arrDiff[$group]) || empty($obj->arrDiff[$group])){
            return;
        }

        $preGroup3No = 0;
        $arrForm = $obj->arrForm;
        foreach($obj->arrDiff[$group] as $_key => $diff){
            if(strlen($diff->before) == 0) $diff->before = $GLOBALS["msg"]["edit_non"];
            if(strlen($diff->after)  == 0) $diff->after  = $GLOBALS["msg"]["edit_non"];

            // edataXX
            $item_id  = substr($_key, 5);
            $data = $obj->arrItemData[$group][$item_id];

            if($data["disp"] == "1" || $data["item_mail"] == "1") continue;

            // title
            $group3no = $data["group3"];
            if($preGroup3No != $group3no){
                $preGroup3No = $group3no;

                // 言語別設定
                if(isset($obj->formdata["arrgroup3"][$preGroup3No-1])){
                    $format = "";
                    if($obj->formdata["lang"] == LANG_JPN){
                        $format = ($obj->formdata["arrgroup3"][$preGroup3No-1] != "") ? "\n【%s】\n\n" : "\n";
                    }
                    else{
                        $format = ($obj->formdata["arrgroup3"][$preGroup3No-1] != "") ? "\n[%s]\n\n" : "\n";
                    }
                    $arrbody[$group] .= sprintf($format, trim($obj->formdata["arrgroup3"][$preGroup3No-1]));
                }
            }


            // 項目名
            $data["item_name"] = strip_tags($data["item_name"]);
            $name = $data["item_name"];

            $methodname = "mailfuncDiff".$item_id;
            if(method_exists($obj, $methodname)) {
                $arrbody[$group] .= $obj->$methodname($name, $diff);
            } else {
                if(is_object($obj->exClass) && method_exists($obj->exClass, $methodname)){
                    $arrbody[$group] .= $obj->exClass->$methodname($obj, $item_id, $name, $diff);

                }else{
                    // 任意項目
                    if($data["controltype"] == "1") {
                        $arrbody[$group] .= $obj->point_mark.$name.": ".$obj->mailfuncDiffNiniBody($group, $item_id, $diff);

                    // 標準項目 # テキストタイプ
                    } else {
                        $arrbody[$group] .= $obj->point_mark.$name.": ".$diff->before.'→'.$diff->after."\n";
                    }
                }
            }
            $arrbody[$group] .= "\n";
        }
        $obj->arrForm = $arrForm;
    }

    /* 任意の入力 */
    function mailfuncDiffNiniBody($obj, $group, $item_id, $diff){
        $str  = '';
        $_key = 'edata'.$item_id;

        $obj->arrForm = array();
        switch($obj->arrItemData[$group][$item_id]["item_type"]){
            case Usr_init::INPUT_CHECK:
                // DB用パラメータからPOSTパラメータへ変換
                $wk_checked  = explode("|", $diff->before);
                foreach($wk_checked as $chk_val){
                    $obj->arrForm[$_key.$chk_val] = $chk_val;
                }
                $before = trim($obj->mailfuncNiniBody($group, $item_id));
                $obj->arrForm = array();

                $wk_checked  = explode("|", $diff->after);
                foreach($wk_checked as $chk_val){
                    $obj->arrForm[$_key.$chk_val] = $chk_val;
                }
                $after = trim($obj->mailfuncNiniBody($group, $item_id));
                $obj->arrForm = array();
                $str .= "\n ".$before."\n ↓\n ".$after."\n";
                break;

            case Usr_init::INPUT_AREA:
                $obj->arrForm[$_key] = $diff->before; $before = trim($obj->mailfuncNiniBody($group, $item_id));
                $obj->arrForm[$_key] = $diff->after;  $after  = trim($obj->mailfuncNiniBody($group, $item_id));
                $str .= "\n".$before."\n↓\n".$after."\n";
                break;

            default:
                $obj->arrForm[$_key] = $diff->before; $before = trim($obj->mailfuncNiniBody($group, $item_id));
                $obj->arrForm[$_key] = $diff->after;  $after  = trim($obj->mailfuncNiniBody($group, $item_id));
                $str .= $before.'→'.$after."\n";
                break;
        }
        return $str;
    }


    /* title */
    function mailfuncDiff57($obj, $name, $diff) {
        $before = strlen($diff->before) > 0 ? $GLOBALS["titleList"][$diff->before] : '';
        $after  = strlen($diff->after ) > 0 ? $GLOBALS["titleList"][$diff->after ] : '';

        $str  = $obj->point_mark.$name.": ";
        $str .= $before.'→'.$after."\n";

        return $str;
    }

    function mailfuncDiff52($obj, $name, $diff) {
        $str  = $obj->point_mark.$name.": ";
        $str .= $diff->before.'→'.$diff->after."\n";
        return $str;
    }



}


?>