<?php

require_once(MODULE_DIR.'entry_ex/Usr_init.class.php');

class Usr_initial {

    /** Mng管理者フラグの判定 */
    function setMngAdmin($obj)
    {
        //------------------------------------
        //一般利用者モード
        //------------------------------------
        if(!isset($_GET["admin_flg"])) $_GET["admin_flg"] = "";
        $obj->admin_flg = $_GET["admin_flg"] != "" ? "1" : "";
    }


    /**
     * メンテナンスを表示するフォームの場合は、メンテナンス画面を表示する
     */
    function showMenteform($obj) {
        // メンテナンス用メッセージ
        $mainteMsg = "";

        // メンテナンス判定
        $form_id = $obj->o_form->formData["form_id"];
        if(!in_array($form_id, $obj->mente_form)){
            $date1 = strtotime($obj->o_form->formData["maintenance_date1"]);
            $date2 = strtotime($obj->o_form->formData["maintenance_date2"]);
            $now   = strtotime(date("Y-m-d H:i:s"));
            // メンテナンス時刻設定あり
            if(0 < strlen($date1) || 0 < strlen($date2)){
                $isMainte = false;
                if(0 < strlen($date1) && 0 < strlen($date2)){
                    if($date1 <= $now && $now <= $date2){
                        $isMainte = true;
                    }
                }
                if(0 < strlen($date1) && strlen($date2) == 0){
                    if($date1 <= $now){
                        $isMainte = true;
                    }
                }
                if(strlen($date1) == 0 && 0 < strlen($date2)){
                    if($now <= $date2){
                        $isMainte = true;
                    }
                }

                if($isMainte){
                    array_push($obj->mente_form, $form_id);

                    // メンテナンス開始日時
                    $mdate1 = strlen($obj->o_form->formData["maintenance_date1"]) > 0
                            ? date("Y年m月d日 H時i分", strtotime($obj->o_form->formData["maintenance_date1"]))
                            : '';

                    // メンテナンス終了日時
                    $mdate2 = strlen($obj->o_form->formData["maintenance_date2"]) > 0
                            ? date("Y年m月d日 H時i分", strtotime($obj->o_form->formData["maintenance_date2"]))
                            : '未定';

                    $mainteMsg = sprintf($GLOBALS["msg"]["maintenance"], $mdate1, $mdate2);
                }
            }
        }

        // 一般利用者のみメンテナンス表示
        if(strlen($obj->admin_flg) > 0) return;

        if(count($obj->mente_form) > 0) {
            if(in_array($obj->form_id, $obj->mente_form)) {
                // 共通テンプレート
                $obj->_processTemplate = "Usr/form/Usr_maintenance.html";
                $obj->setFormData();      // フォームデータの取得・表示テンプレートの設定
                $obj->commonAssign();     // 共通アサイン

                // システムメンテナンスメッセージ
                if(strlen($obj->o_form->formWord["word36"]) > 0){
                    $mainteMsg = $obj->o_form->formWord["word36"];
                }
                $obj->assign("mainteMsg", $mainteMsg);

                // 親クラスに処理を任せる
                $obj->main();

                // 開発用デバッグ関数
                $obj->developfunc();
                exit;
            }
        }
    }


    /**
     * アップロードディレクトリがない場合は作成
     */
    function createUploadDir($obj) {
        $obj->uploadDir = UPLOAD_PATH."Usr/form".$obj->form_id."/";    //ファイルアップロードディレクトリ

        if(!is_dir($obj->uploadDir)){
            mkdir($obj->uploadDir, 0777, true);
            chmod($obj->uploadDir, 0777);
        }
    }


    /**
     * 管理者モードか一般モードか判断し、各処理を行う
     */
    function AuthAction($obj) {
        if($obj->admin_flg == ""){

            //-------------------------------
            //エントリー可能期間チェック
            //-------------------------------
            $obj->_chkPeriod();


            //------------------------
            //新規　or 更新判別
            //------------------------
            //ユーザのセッション情報があるか？
            if(isset($GLOBALS["entryData"]["eid"])){
                $obj->eid = $GLOBALS["entryData"]["eid"];
                $obj->assign("va_userdata", $GLOBALS["entryData"]);    //ユーザのデータ
            }
            else{
                $obj->eid = "";
            }

            //----------------------------------------------
            //ログイン認証
            //----------------------------------------------
            if(isset($_REQUEST["eid"]) && (trim($_REQUEST["eid"]) != "")){

                //ログインチェック
                if (!$GLOBALS["session"]->getVar("isLoginEntry")) {
                    $msg = ($obj->o_form->formData["lang"] == LANG_JPN) ? "ログインしていません。" : "Not logged in";
                    Error::showErrorPage($msg);
                }
            }
        }
        //------------------------------------
        //管理者モード
        //------------------------------------
        else{
            // ログインチェック
            $url = SSL_URL_ROOT."Usr/Usr_login.php?form_id=".$obj->form_id;
            LoginMember::checkLoginRidirect($url, false);

            $obj->eid = isset($_GET["eid"]) ? $_GET["eid"] : "";
            if($obj->eid != "" && !is_Numeric($obj->eid)) {
                Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
            }

            if($obj->admin_flg != "" && !is_Numeric($obj->admin_flg)) {
                Error::showErrorPage("ページが存在しません。<br />Page not found."); exit;
            }

            //ユーザのセッション情報があるか？
            if(isset($GLOBALS["entryData"]["eid"])){
                $obj->assign("va_userdata", $GLOBALS["entryData"]);    //ユーザのデータ
            }
        }

        $obj->assign("eid", $obj->eid);        //エントリーID
    }


    /**
     * フォーム基本情報の取得・アサイン
     */
    function setFormData($obj) {
        //---------------------------------
        //フォーム基本情報取得
        //---------------------------------
        $obj->formdata = $obj->o_form->formData;
        
        if(!$obj->formdata){
            Error::showErrorPage("フォーム基本情報の取得に失敗しました。");
        }

        // 言語のイニシャル
        $obj->lang  = ($obj->formdata["lang"] == LANG_JPN) ? 'J' : 'E';
        $obj->langR = ($obj->formdata["lang"] == LANG_JPN) ? 'j' : 'e';

        //---------------------------------------------
        //決済ありなしの判別
        //---------------------------------------------
        $obj->formdata["kessai_flg"] = "";
        if($obj->formdata["type"] == "3" && $obj->formdata["credit"] != "0"){
            $obj->formdata["kessai_flg"] = "1";    //決済ありフラグ　ON
        }

        // グループ3の見出し
        if($obj->formdata["group3"] != "") {
            $obj->formdata["arrgroup3"] = explode("|", $obj->formdata["group3"]);
        }

        $obj->assign("lang",     $obj->lang);
        $obj->assign("langR",    $obj->langR);
        $obj->assign("formData", $obj->formdata);
    }


    /**
     * フォーム項目の取得及び、整形
     */
    function setFormIni($obj) {
        // フォーム別 処理系の変数定義
        // Processbaseに定義したいけど、form_idが取得できないため、
        // 項目取得は必ず通るためここで呼び出す
        // 項目取得をする = フォームに関する処理を行うが前提
        Config::loadVariable($this);
        if(!isset($obj->item_sort)) $obj->item_sort = "item_ini.sort";

        //----------------------------------
        // フォーム項目を取得
        //----------------------------------
        $col = array();
        $col[] = 'form_item.form_id';
        $col[] = 'form_item.item_id';
        $col[] = 'form_item.item_name';
        $col[] = 'form_item.item_view';
        $col[] = 'form_item.item_check';
        $col[] = 'form_item.item_len';
        $col[] = 'form_item.item_pview';
        $col[] = 'form_item.item_mail';
        $col[] = 'form_item.item_type';
        $col[] = 'form_item.item_select';
        $col[] = 'form_item.rdate';
        $col[] = 'form_item.udate';
        $col[] = 'form_item.item_memo';
        $col[] = 'form_item.item_sort';
        $col[] = 'item_ini.group_id';
        $col[] = 'item_ini.controltype';
        $col[] = 'item_ini.sort';
        $col[] = 'item_ini.defname';
        $from = "form_item Left Join item_ini ON form_item.item_id = item_ini.item_id";
        $where = "form_type = 0 and lang = ".$obj->db->quote($obj->formdata["lang"])." and form_id = ".$obj->db->quote($obj->form_id);
        $orderby = "group_id, ".$obj->item_sort;
        $objItem = $obj->db->getListData(implode(",", $col), $from, $where, $orderby);
        if(!$objItem){
            Error::showErrorPage("フォームの項目の取得に失敗しました。");
        }


        // 利用する任意項目のitem_idの配列
        $obj->option_item = array("1"=>array(), "2"=>array(), "3"=>array());

        // グループ(ブロック別)のitemData
        $obj->arrItemData = array("1"=>array(), "2"=>array(), "3"=>array());

        // グループ3のグループ分け
        if(!isset($obj->arrfile)) Usr_init::_initFile($obj);
        $group3 = array("0" => array(46, 47, 48, 49, 50)    // 論文情報
                      , "1" => array_keys($obj->arrfile)    // ファイルアップロード 
                      , "2" => array());                    // 任意項目

        // グループ3のグループ別の利用項目数
        $obj->isUseGroup3 = array("1"=> 0, "2"=> 0, "3"=> 0);

        // 1ページ目 共著者情報
        $chosha = array(29, 30, 31);

        // 項目定義
        foreach($objItem as $i_key => $data){
            $item_id = $data["item_id"];

            $keys = array('form_id', 'item_id', 'item_name', 'item_view', 'item_check', 'item_len'
                        , 'item_pview', 'item_mail', 'item_type', 'item_select', 'rdate', 'udate'
                        , 'item_memo', 'item_sort');
            foreach($keys as $itemKey){
                $obj->itemData[$item_id][$itemKey] = $data[$itemKey];
            }
            $obj->itemData[$item_id]["group_id"] = $data["group_id"];
            $obj->itemData[$item_id]["controltype"] = $data["controltype"];
            $obj->itemData[$item_id]["disp"] = ($data["item_view"] == "1") ? "1" : "";
            $obj->itemData[$item_id]["need"] = "";
            $obj->itemData[$item_id]["select"] = "";
            $obj->itemData[$item_id]["mail"] = "";

            //参加フォーム 共著者に関する項目は非表示
            if($obj->formdata["type"] == "3" && in_array($item_id, $chosha)){
                $obj->itemData[$item_id]["disp"] = "1";
            }

            // おまじない
            if(!isset($obj->itemData[$item_id]['item_id'])){
                $obj->itemData[$item_id]['item_id'] = $item_id;
            }

            //画面に表示する項目の名称
            $obj->itemData[$item_id]["item_name"] = ($data["item_name"] != "") ? $data["item_name"] : $data["defname"];
            $obj->itemData[$item_id]["strip_tags"] = strip_tags($obj->itemData[$item_id]["item_name"]);

            //アブストラクトの場合
            if($obj->formdata["type"] == "1"){

                //一次期間中
                if($obj->_chkTerm() === true){
                    if($data["item_pview"] == "1"){
                        $obj->itemData[$item_id]["disp"] = "1";
                    }
                }
            }

            //必須有無
            $pos = strstr($data["item_check"],"0");
            if($pos !== false){
                $obj->itemData[$item_id]["need"] = "1";
            }

            //メールアドレスに関するチェック
            $pos = strstr($data["item_check"],"6");
            if($pos !== false){
                $obj->itemData[$item_id]["mail"] = "1";
            }

            //チェック・ラジオボタン・セレクトボックス
            if($data["item_select"] != ""){
                $wk_select  = explode("\n", $data["item_select"]);
                $i =  "";
                $set_select = array();
                foreach($wk_select as $num => $val){
                    $i = $num+1;

                    //非活性の対象か否か
                    $pos = strpos($val, "#");
                    if($pos !== false){
                        $set_disable[$i] = "1";
                    }
                    else{
                        $set_disable[$i] = "";
                    }

                    //$set_select[$i] = $val;
                    $set_select[$i] = str_replace(array("#"), "", $val);;
                }
                $obj->itemData[$item_id]["select"] = $set_select;
                $obj->itemData[$item_id]["disable"] = $set_disable;
            }

            //発表形式
            if($item_id == "48"){
                //表示する場合
                if($data["item_view"] != "1"){

                    $wk_h_keisiki = explode("\n", $data["item_select"]);
                    $i =  "";
                    $set_select = array();
                    foreach($wk_h_keisiki as $num => $val){
                        $i = $num+1;
                        $set_select[$i] = $val;
                    }
                    $obj->itemData[$item_id]["select"] = $set_select;
                }
            }

            // 利用する任意項目を格納
            if($obj->itemData[$item_id]["disp"] != "1" && $data["controltype"] == "1") {
                $obj->option_item[$data["group_id"]][] = $item_id;
            }

            $obj->arrItemData[$data["group_id"]][$item_id] = $obj->itemData[$item_id];
            // グループ3は見出しを表示するため詳細に分ける
            if($data["group_id"] == 3){
                $item_id = $obj->itemData[$item_id]['item_id'];
                if(in_array($item_id, $group3[0])){
                    $obj->arrItemData[$data["group_id"]][$item_id]['group3'] = 1;
                    if($obj->itemData[$item_id]["disp"] != 1){
                        $obj->isUseGroup3[1]++;
                    }

                }elseif(in_array($item_id, $group3[1])){
                    $obj->arrItemData[$data["group_id"]][$item_id]['group3'] = 2;
                    if($obj->itemData[$item_id]["disp"] != 1){
                        $obj->isUseGroup3[2]++;
                    }

                }else{
                    $obj->arrItemData[$data["group_id"]][$item_id]['group3'] = 3;
                    if($obj->itemData[$item_id]["disp"] != 1){
                        $obj->isUseGroup3[3]++;
                    }
                }
            }
        }
    }


    /**
     * 共通アサインクラス
     */
    function commonAssign($obj) {
        //---------------------------------
        // 各種文言情報取得
        //---------------------------------
        $obj->formWord =  $obj->o_form->getFormWord($obj->form_id);
        $obj->assign("formWord",$obj->formWord);
        
        //---------------------------------
        // ヘッダー画像
        //---------------------------------
        $ws_header_img = ($obj->formdata["head_image"] != "") ? IMG_URL."header/".$obj->formdata["head_image"]: "";
        $obj->assign("va_header", $ws_header_img);        //ヘッダー
        
        //--------------------------------------------
        //定数情報取得
        //--------------------------------------------
        //都道府県
        if($obj->formdata["lang"] == LANG_JPN){
            $obj->assign("va_ken",$GLOBALS["prefectureList"]);
        }

        // 言語設定
        self::setLanguage($obj, $obj->formdata["lang"]);

        // フォーム別のカスタムcss,jsの読み込み
        $form_id = $obj->form_id;
        Usr_initial::readCustomfiles($obj, $form_id);

        // jQuery読み込みフラグ
        $obj->assign('useJquery',   $obj->useJquery);

        $obj->assign("va_contact",  $obj->wa_contact);
        $obj->assign("va_kaiin",    $obj->wa_kaiin);
        $obj->assign("va_coAuthor", $obj->wa_coAuthor);
        $obj->assign("va_jpo1",     $obj->wa_jpo1);
        $obj->assign("va_method",   $obj->wa_method);
        $obj->assign("va_card_type",$obj->wa_card_type);
        $obj->assign("formItem",    $obj->itemData);    // 項目マスタ
    }


    // 言語設定
    static function setLanguage($obj, $lang){
        //日本語
        if($lang == LANG_JPN){
            $obj->wa_contact   = $GLOBALS["contact_J"];    // 連絡先
            $obj->wa_kaiin     = $GLOBALS["member_J"];     // 会員・非会員
            $obj->wa_coAuthor  = $GLOBALS["coAuthor_J"];   // 共著者の有無
            $obj->wa_jpo1      = $GLOBALS["jpo1_J"];       // 支払回数
            $obj->wa_method    = $GLOBALS["method_J"];     // 支払方法
            $obj->wa_card_type = $GLOBALS["card_type_J"];  // カードの種類
        }
        else{
            $obj->wa_contact   = $GLOBALS["contact_E"];    // 連絡先
            $obj->wa_kaiin     = $GLOBALS["member_E"];
            $obj->wa_coAuthor  = $GLOBALS["coAuthor_E"];
            $obj->wa_jpo1      = $GLOBALS["jpo1_E"];       // 支払回数
            $obj->wa_method    = $GLOBALS["method_E"];     // 支払方法
            $obj->wa_card_type = $GLOBALS["card_type_E"];  // カードの種類
        }
    }


    // フォーム別のカスタムcss,jsの読み込み
    static function readCustomfiles($obj, $form_id){
        // フォーム別のカスタマイズ用CSS
        $isCustomCss = false;
        $css = 'css/'.$form_id.'.css';
        $baseDir   = 'customDir/'.$form_id."/";
        $customDir = ROOT_DIR.$baseDir;
        $ex = glob($customDir.$css);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                $isCustomCss = true;
                break;
            }
        }
        $obj->assign("is_custom_css", $isCustomCss);
        $obj->assign("customCss",     APP_ROOT.$baseDir.$css);

        // フォーム別のカスタマイズ用JS
        $isCustomJs = false;
        $js = 'js/'.$form_id.'.js';
        $baseDir   = 'customDir/'.$form_id."/";
        $customDir = ROOT_DIR.$baseDir;
        $ex = glob($customDir.$js);
        if(is_array($ex)){
            foreach($ex as $_key => $script){
                $isCustomJs = true;
                break;
            }
        }

        $obj->customDir = $customDir;
        $obj->assign("is_custom_js", $isCustomJs);
        $obj->assign("customJs",     APP_ROOT.$baseDir.$js);
    }


    function setTemplate($obj, $includeDir="") {
        if(strlen($includeDir) == 0) $includeDir = TEMPLATES_DIR."Usr/include/".$obj->form_id."/";

        //------------------------
        // 表示HTMLの設定
        //------------------------
        $obj->_processTemplate = "Usr/form/Usr_entry.html";
        $obj->_title = $obj->formdata["form_name"];


        // 同意文の外部テンプレート
        $tmpfile = $obj->form_id."agree.html";
        if(file_exists($includeDir.$tmpfile)) {
            $obj->assign("agree_inc_flg", 1);
            $obj->assign("tpl_agree", $tmpfile);
        }


        // 1-3ページの外部テンプレートの読み込み
        for($i = 1; $i < 4; $i++) {
            $tmpfile = $obj->form_id."block".$i.".html";
            if(file_exists($includeDir.$tmpfile)) {
                $obj->assign("block".$i."_inc_flg", 1);
                $obj->assign("tpl_block".$i."_name", $tmpfile);
            }
            $tmpfile = $obj->form_id."block".$i."_confirm.html";
            if(file_exists($includeDir.$tmpfile)) {
                $obj->assign("block".$i."_confirm_inc_flg", 1);
                $obj->assign("tpl_block".$i."_confirm_name", $tmpfile);
            }
            
        }


        //決済ありの場合、
        // 外部テンプレートの読み込み
        if($obj->formdata["kessai_flg"] == "1"){
            //その他決済の項目
            $money_inc_flg = "";
            $tpl_name = "";
            $wb_ret = $obj->_chkIncFiles($includeDir, $obj->form_id, "money", "html");
            if($wb_ret === true){
                $money_inc_flg = "1";
                $tpl_name = $obj->form_id."money.html";
            }
            $obj->assign("money_inc_flg", $money_inc_flg);
            $obj->assign("tpl_name", $tpl_name);

            // 金額：編集
            $money_edit_inc_flg = "";
            $tpl_name = "";
            $wb_ret = $obj->_chkIncFiles($includeDir, $obj->form_id, "money_edit", "html");
            if($wb_ret === true){
                $money_edit_inc_flg = "1";
                $tpl_name = $obj->form_id."money_edit.html";
            }
            $obj->assign("money_edit_inc_flg", $money_edit_inc_flg);
            $obj->assign("tpl_edit_name", $tpl_name);

            //決済情報入力部分
            $payment_inc_flg = "";
            $tpl_name = "";
            $wb_ret = $obj->_chkIncFiles($includeDir, $obj->form_id, "payment", "html");
            if($wb_ret === true){
                $payment_inc_flg = "1";
                $tpl_name = $obj->form_id."payment.html";
            }
            $obj->assign("payment_inc_flg", $payment_inc_flg);
            $obj->assign("tpl_payment_name", $tpl_name);

            //内容確認画面の　決済部分（支払項目と決済情報）
            $payment_confirm_inc_flg = "";
            $tpl_name = "";
            $wb_ret = $obj->_chkIncFiles($includeDir, $obj->form_id, "payment_confirm", "html");
            if($wb_ret === true){
                $payment_confirm_inc_flg = "1";
                $tpl_name = $obj->form_id."payment_confirm.html";
            }

            $obj->assign("payment_confirm_inc_flg", $payment_confirm_inc_flg);
            $obj->assign("tpl_payment_confirm_name", $tpl_name);


            // お支払確認ページの表示/非表示 : デフォルトは表示
            $payment_confirm_disp_flg = "1";
            if(in_array($obj->form_id, $obj->payment_not)){
                $payment_confirm_disp_flg = "";
            }
            $obj->assign("payment_confirm_disp_flg", $payment_confirm_disp_flg);
        }
        $obj->assign("includeDir", $includeDir);
    }



    function setMoneyPaydata($obj){
        $year                 = array();
        $month                = array();
        $obj->wa_price        = array();
        $obj->wa_ather_price  = array();
        $obj->w_head          = array();
        $obj->filecheck       = array();


        //------------------------------------
        //参加フォームの場合の設定
        //------------------------------------
        if($obj->formdata["type"] == "3"){
            // 有効期限　リストボックス生成
            //年
            $wk_year =date("Y");
            for($i=0; $i<20; $i++){
                $year[$wk_year] = $wk_year;
                $wk_year++;
            }

            //月
            for($i=1; $i<13; $i++){
                $wk_month = sprintf('%02d', $i);
                $month[$wk_month] = $wk_month;
            }

            /**
             * アップロードファイルの定義
             */

            //金額
            //選択タイプ
            if($obj->formdata["pricetype"] == "1" || $obj->formdata["pricetype"] == "2"){
                $wk_price = explode("\n", $obj->formdata["price"]);

                foreach($wk_price as $key => $data){
                    $data = str_replace(array("　"," "), "|",trim($data));
                    $wk_price_data = explode("|", $data);


                    if($obj->formdata["pricetype"] == "2"){
                        $obj->wa_price[$key]["key"] = $key;
                        $obj->wa_price[$key]["name"] = str_replace(array("_"), " ",$wk_price_data[0]);
                        $obj->wa_price[$key]["p1"] = $wk_price_data[1];
                        $obj->wa_price[$key]["p2"] = $wk_price_data[2];
                        $obj->wa_price[$key]["p3"] = $wk_price_data[3];
                        
                        if($wk_price_data[3] == "1") {
                            $obj->filecheck[] = $key;
                        }
                    }
                    else{
                        //$obj->wa_price[$key]["name"] = "";
                        $obj->wa_price[$key]["key"] = $key;
                        $obj->wa_price[$key]["p1"] = $wk_price_data[0];
                        $obj->wa_price[$key]["p2"] = $wk_price_data[1];
                    }
                }
            }

            //その他決済を利用する場合
            if($obj->formdata["kessai_flg"] == "1" && $obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){
                $wk_price = explode("\n", $obj->formdata["ather_price"]);
                foreach($wk_price as $key => $data){
                    $data = str_replace(array("　"," "), "|",trim($data));
                    $wk_price_data = explode("|", $data);
                    $obj->wa_ather_price[$key]["name"]  = str_replace(array("_"), " ",$wk_price_data[0]);
//                    $obj->wa_ather_price[$key]["p1"]    = ($wk_price_data[1] == "Free") ? 0 : $wk_price_data[1];
                    $obj->wa_ather_price[$key]["p1"]    = $wk_price_data[1];
                    $obj->wa_ather_price[$key]["p2"]    = $wk_price_data[2];
                    $obj->wa_ather_price[$key]["tani"]  = str_replace(array("_"), " ",$wk_price_data[3]);
                    $obj->wa_ather_price[$key]["limit"] = ((int)$wk_price_data[4] == 0) ? 10 : $wk_price_data[4];

                    $comment = ($wk_price_data[5] == "0") ? "" : str_replace(array("_"), " ",$wk_price_data[5]);
                    $obj->wa_ather_price[$key]["tani2"] = $obj->wa_ather_price[$key]["tani"]."<br />".$comment;
                }
            }

            // 見出し
            $obj->w_head['price_header']        = $obj->formdata['price_header'];
            $obj->w_head['price_head0']        = $obj->formdata['price_head0'];
            $obj->w_head['price_head1']         = $obj->formdata['price_head1'];
            $obj->w_head['ather_price_header'] = $obj->formdata['ather_price_header'];
            $obj->w_head['ather_price_head0']    = $obj->formdata['ather_price_head0'];
            $obj->w_head['ather_price_head1']    = $obj->formdata['ather_price_head1'];

            //お支払方法
            $ss_method = $GLOBALS["session"]->getVar("form_param1");
            $obj->ss_method = isset($ss_method["method"]) ? $ss_method["method"] : "";
        }

        // ファイルアップロードが必須の場合は項目数を-1する
        $arrForm = $GLOBALS["session"]->getVar("form_param1");
        $amount  = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : $arrForm['amount'];
        if(!in_array($amount, $obj->filecheck)){
            $obj->isUseGroup3[2] -= 1;
        }

        // グループ3の利用項目がない場合は自動的にグループ3を利用不可
        // TODO Usr_entryの本番実装の兼ね合いでひとまずここに記述
        if(array_sum($obj->isUseGroup3) === 0){
            $obj->formdata['group3_use'] = 1;
        }


        if(method_exists($obj, "assign")){
            $obj->assign("year", $year);
            $obj->assign("month", $month);
            $obj->assign("va_price", $obj->wa_price);
            $obj->assign("va_ather_price", $obj->wa_ather_price);
            $obj->assign("v_head", $obj->w_head);
            $obj->assign("disp", $obj->formdata['disp']);
            $obj->assign("filecheck", $obj->filecheck);
            $obj->assign("isUseGroup3",$obj->isUseGroup3);
        }
    }

}


?>