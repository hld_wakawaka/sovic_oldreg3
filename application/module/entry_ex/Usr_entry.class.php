<?php

class Usr_entryDB {


    function getEntryNumber($obj) {
        $saiban = $obj->o_entry->getEntryNum($obj->db, $obj->formdata["form_id"]);

        //採番後のエントリー番号
        $obj->entry_number = $saiban["entory_count"] + 1;

        //--------------------------------------
        //エントリー番号採番
        //--------------------------------------
        $rs = $obj->o_entry->updEntryNum($obj->db, $obj->formdata["form_id"], $obj->entry_number);
        if(!$rs) {
            return array(false, "", "");
        }
        return;
    }


    function getOrderNumber($obj) {
        $saiban = $obj->o_entry->getEntryNum($obj->db, $obj->formdata["form_id"]);

        //採番後の取引番号
        $obj->order_number = $saiban["order_count"] + 1;

        //--------------------------------------
        // 決済取引番号採番
        //--------------------------------------
        $rs = $obj->o_entry->updOrderNum($obj->db, $obj->formdata["form_id"], $obj->order_number);
        if(!$rs) {
            return array(false, "", "");
        }

        //--------------------------------------
        // 決済取引番号の重複チェック
        //--------------------------------------
        $order_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->order_number);

        $objChk = new AuthorizeCheck();
        $check = $objChk->checkOrderId($obj, $order_id);
        if(!$check){
            // 決済取引IDが重複しているので取り直し
            $obj->getOrderNumber();
        }
        return;
    }


    function getEntry($obj) {
        //ファイルアップロード用　ユニークディレクトリ名
        $date = date('YmdHis');

        //ファイルアップロード用　ワークディレクトリ生成
        $workDir = $obj->uploadDir.$obj->eid."/".$date;

        if(!is_dir($workDir)){
            mkdir($workDir, 0777, true);
            chmod($workDir, 0777);
        }
        //セッションにワークディレクトリ名をセット
        $GLOBALS["session"]->setVar("workDir", $workDir);

        //応募データ取得
        $wa_entrydata = $obj->o_entry->getRntry_r($obj->db, $obj->eid, $obj->form_id);
        if(!$wa_entrydata){
            Error::showErrorPage("応募情報の取得に失敗しました。");
        }


        //応募者のID・パスワード
        $ss_entry_user = array("e_user_id" => $wa_entrydata["e_user_id"], "e_user_passwd" => $wa_entrydata["e_user_passwd"]);
        $GLOBALS["session"]->setVar("SS_USER", $ss_entry_user);

        //共著者データ取得
        $wa_aff = $obj->o_entry->getRntry_aff($obj->db, $obj->eid);

        //決済データ取得
        if($obj->formdata["kessai_flg"] == "1"){
            list($wa_authdata, $wa_payment_detail1, $wa_payment_detail2) = $obj->getPayData();
        }


        //----------------------------
        //管理者モードの場合
        //----------------------------
        if($obj->admin_flg != ""){
            $wk_user_id["eid"]       =  $obj->eid;
            $wk_user_id["e_user_id"] =  $wa_entrydata["e_user_id"];
            $wk_user_id['form_id']   =  $wa_entrydata['form_id'];
            $GLOBALS["session"]->setVar("isLoginEntry", true);
            $GLOBALS["session"]->setVar("entryData", $wk_user_id);
        }

        //----------------------
        //1ページ目データ
        //----------------------
        $start = 1;
        $end = 32;

        for($index=$start; $index < $end; $index++){

            if($index != 26 && $index != 27 && $index != 28){
                $obj->arrForm["edata".$index] = $wa_entrydata["edata".$index];
            }
            else{
                //任意項目
                $obj->_default_arrFormNini(26, 29, $wa_entrydata);
            }
        }

        $obj->_default_arrFormNini(63, 65, $wa_entrydata);

        //任意項目6～20
        $obj->_default_arrFormNini(69, 84, $wa_entrydata);

        //任意項目21～50
        $obj->_default_arrFormNini(115, 145, $wa_entrydata);

        //英語フォーム固有の項目
        $start = 57;
        $end = 61;
        for($index=$start; $index < $end; $index++){
            $obj->arrForm["edata".$index] = $wa_entrydata["edata".$index];
        }
        //国名リストボックスの値
        $obj->arrForm["edata114"] = $wa_entrydata["edata114"];

        $obj->arrForm["chkemail"] = $wa_entrydata["edata25"];

        //決済ありの場合
        if($obj->formdata["kessai_flg"] == "1"){
            $obj->arrForm["amount"] = $wa_payment_detail1[0]["key"];

            //その他決済項目
            if($obj->formdata["ather_price_flg"] != "1" && $wa_payment_detail2){
                //メソッドチェック
                $flg = 0;
                if($flg != 1){
                    foreach($wa_payment_detail2 as $_key => $data){
                        $pkey = $data['key'];
                        $obj->arrForm["ather_price".$pkey] = $data["quantity"];
                    }
                }
            }
        }

        $GLOBALS["session"]->setVar("form_param1", $obj->arrForm);

        //----------------------
        //2ページ目データ
        //----------------------
        $obj->arrForm = "";

        //共著者の登録がある場合
        if($wa_aff){
            foreach($wa_aff as $a_key => $data){
                $start = 32;
                $end = 46;

                for($index=$start; $index < $end; $index++){

                    if($index != 43 && $index != 44 && $index != 45){

                        //共著者の所属機関
                        if($index == 32){
                            $wk_checked  = explode("|", $data["edata".$index]);
                            foreach($wk_checked as $chk_val){
                                $obj->arrForm["edata".$index.$a_key.$chk_val] = $chk_val;
                            }
                        }
                        else{
                            $obj->arrForm["edata".$index.$a_key] = $data["edata".$index];
                        }

                    }
                    else{
                        //任意項目生成
                        $obj->_default_arrFormChosyaNini(43, 46, $data, $a_key);
                    }
                }

                //英語フォーム固有の項目
                $start = 61;
                $end = 63;

                for($index=$start; $index < $end; $index++){
                    $obj->arrForm["edata".$index.$a_key] = $data["edata".$index];

                }

                //共著者ID
                $kyouid[$a_key] = $data["id"];

                //任意項目生成
                $obj->_default_arrFormChosyaNini(65, 68, $data, $a_key);
                $obj->_default_arrFormChosyaNini(84, 99, $data, $a_key);

            }
            $GLOBALS["session"]->setVar("form_param2", $obj->arrForm);
            $GLOBALS["session"]->setVar("kyouID", $kyouid);
        }


        //----------------------
        //3ページ目データ
        //----------------------
        $obj->arrForm = "";
        $start = 46;
        $end = 57;

        for($index=$start; $index < $end; $index++){

            if($index != 54 && $index != 55 && $index != 56){
                $obj->arrForm["edata".$index] = $wa_entrydata["edata".$index];
            }
            else{
                $obj->_default_arrFormNini(54, 57, $wa_entrydata);
            }

            //添付資料
            if(in_array($index, array_keys($obj->arrfile))){
                //--------------------------
                //ワークディレクトリにファイルを移動
                //--------------------------
                if(is_file($obj->uploadDir.$obj->eid."/".$wa_entrydata["edata".$index])){
                    if (!copy($obj->uploadDir.$obj->eid."/".$wa_entrydata["edata".$index], $obj->uploadDir.$obj->eid."/".$date."/".$wa_entrydata["edata".$index])) {
                        $obj->complete("ファイルのコピーに失敗しました。");
                    }
                }
                $obj->arrForm["hd_file".$index] = $wa_entrydata["edata".$index];
                $obj->arrForm["n_data".$index] = $wa_entrydata["edata".$index];
            }
        }

        $obj->_default_arrFormNini(67, 69, $wa_entrydata);
        $obj->_default_arrFormNini(99, 114, $wa_entrydata);

        $obj->_default_arrFormNini(145, 160, $wa_entrydata);

        $GLOBALS["session"]->setVar("form_param3", $obj->arrForm);


        //-------------------------------
        //決済ページ
        //-------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            $obj->arrForm["card_type"] = $wa_authdata["c_company"];
            $obj->arrForm["cardNumber"] = $obj->o_crypt->crypt_decode($wa_authdata["c_number"]);

            $wk_c_date = $obj->o_crypt->crypt_decode($wa_authdata["c_date"]);

            $wk_c_date =  explode("/", $wk_c_date);
            if(!isset($wk_c_date[1])) $wk_c_date[1] = "";

            $obj->arrForm["cardExpire1"] =$wk_c_date[0];
            $obj->arrForm["cardExpire2"] = "20".$wk_c_date[1];

            $obj->arrForm["cardHolder"] = $wa_authdata["c_holder"];

            $obj->arrForm["jpo1"] = $wa_authdata["c_method"];
            $obj->arrForm["jpo2"] = $wa_authdata["c_split"];
            $obj->arrForm["securityCode"] = NULL;
            // セキュリティコードは保存しない
//            $obj->arrForm["securityCode"] = $obj->o_crypt->crypt_decode($wa_authdata["c_scode"]);


            $obj->arrForm["lessee"] = $wa_authdata["lessee"];
            $obj->arrForm["bank"] = $wa_authdata["bank"];
            $obj->arrForm["closure"] = $wa_authdata["closure"];

            $GLOBALS["session"]->setVar("form_parampay", $obj->arrForm);
        }

        //-------------------------------
        //セッション取得
        //-------------------------------
        $obj->getDispSession();
        
        return;
        
    }


    function getPayData($obj){
        //決済データ取得
        if($obj->formdata["kessai_flg"] != "1") return;

        $wa_authdata = $obj->o_entry->getAuthorize($obj->db, $obj->eid);

        $obj->arrForm["method"] = $wa_authdata["payment_method"];

        //セッションに支払合計金額をセット
        $GLOBALS["session"]->setVar("ss_total_payment", $wa_authdata["price"]);


        //支払詳細項目
        $wa_payment_detail1 = $obj->o_entry->getPaymentDetail($obj->db, $obj->eid, "0", $obj->hide_fee_area);

        //その他決済項目
        if($obj->formdata["ather_price_flg"] != "1"){
            $wa_payment_detail2 = $obj->o_entry->getPaymentDetail($obj->db, $obj->eid, "1");
        }

        // 決済とその他決済をマージ
        $arrPayment = $wa_payment_detail1;
        if(is_array($wa_payment_detail2)){
            $arrPayment = array_merge((array)$arrPayment, (array)$wa_payment_detail2);
        }
        $obj->assign("arrPayment", $arrPayment);

        // 支払合計金額
        $payment = $obj->o_entry->getAuthorize($obj->db, $obj->eid);
        $payment_price = $payment["price"];
        $obj->assign("payment_price", $payment_price);

        return array($wa_authdata, $wa_payment_detail1, $wa_payment_detail2);
    }


    /* 登録/更新用のパラメータを生成 */
    function makeDbParam($obj){
        $param = array();
        $arrgroupId = array(1,3);
        $filekeys = array_keys($obj->arrfile);
        foreach($arrgroupId as $group_id){
            foreach($obj->arrItemData[$group_id] as $item_id => $arrItemData){
                $key = 'edata'.$item_id;

                // 非表示の項目はセットしない
                if($arrItemData["disp"] == "1") continue;
                // ファイルアップロードの項目はここではセットしない
                if(in_array($item_id, $filekeys)) continue;

                $param[$key] = isset($obj->arrForm[$key]) ? $obj->arrForm[$key] : "";
                // 特殊 # 配列
                if(in_array($item_id, $obj->formarray)){
                    foreach($param[$key] as $_key => $_value){
                        $param[$key][$_key] = str_replace(array("\r\n", "\r"), "[:n:]", $_value);   // 改行コード統一
                        $param[$key][$_key] = addslashes($param[$key][$_key]);                      // クォート
                    }
                    $param[$key] = serialize($param[$key]);
                }

                // 任意項目のチェックボックス
                if($obj->arrItemData[$group_id][$item_id]['controltype'] != 1) continue;
                if($obj->arrItemData[$group_id][$item_id]['item_type']   != 3) continue;

                $ins_val = array();
                foreach($obj->arrItemData[$group_id][$item_id]["select"] as $k => $data){
                    // 特殊 # 配列
                    if(in_array($item_id, $obj->formarray)){
                        $obj->arrForm[$key.$k] = serialize($obj->arrForm[$key.$k]);
                    }
                    array_push($ins_val, isset($obj->arrForm[$key.$k]) ? $obj->arrForm[$key.$k] : "");
                }
                $param[$key] = implode("|", $ins_val);
            }
        }
        return $param;
    }


    /**
     * 新規エントリー処理
     *
     */
    function insertEntry($obj){
        //リネイム後のファイル名
        $obj->wk_saki = array();

        /// 削除対象のファイル
        $obj->wk_del = array();

        //----------------------------------------------
        //論文エントリーテーブル
        //----------------------------------------------
        $param = $obj->makeDbParam();
        $param["udate"] = "NOW";
        $param["form_id"] = $obj->formdata["form_id"];            //フォームID
        $param["eid"] = $obj->ins_eid;    //エントリーID

        //ユーザID
        //$user_id = $obj->formdata["head"].sprintf("%05d", $GLOBALS["session"]->getVar("ss_order_no"));
        //$user_id = $obj->formdata["head"]."-".sprintf("%05d", $GLOBALS["session"]->getVar("ss_order_no"));
        //$user_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->entry_number);
        $user_id = $obj->user_id;


        $param["e_user_id"] = $user_id;

        //パスワード自動生成
        $passwd = $obj->o_entry->makePasswd(PASSWD_LEN, PASSWD_TYPE);
        $param["e_user_passwd"] =$passwd;

        //アップロードファイル
        $date = date("Ymd");
        foreach($obj->arrfile as $item_id => $n){
            if($obj->arrForm["n_data".$item_id] != "" && $obj->arrForm["file_del".$item_id] == "1") {
                $param["edata".$item_id] = "";
                $obj->wk_del[$item_id] = $obj->arrForm["n_data".$item_id];
            }

            // 画像をアップロードした場合のみファイル名を更新
            if($obj->arrForm['file_upload'.$item_id] != 1) continue;

            $wk_files = isset($obj->wk_moto[$item_id]) ? $obj->wk_moto[$item_id] : "";
            if($wk_files != "") {
                $extension = pathinfo($GLOBALS["session"]->getVar("workDir")."/".$wk_files, PATHINFO_EXTENSION);
                $obj->wk_saki[$item_id] = $param["e_user_id"].$date.$n.".".$extension;
                $param["edata".$item_id] = $obj->wk_saki[$item_id];
            }
        }

        // 登録経路
        $param['entry_way'] = $obj->admin_flg == "" ? ENTRY_WAY_USER : ENTRY_WAY_ADMIN;

        $rs = $obj->db->insert("entory_r", $param, __FILE__, __LINE__);
        if(!$rs) {
            return array(false, "", "");
        }



        //----------------------------------------------
        //共著者テーブル
        //----------------------------------------------
        //共著者ありの時
        if(isset($obj->arrForm["edata29"]) && $obj->arrForm["edata29"] == "1"){

            for($i=0; $i < $obj->arrForm["edata30"]; $i++){

                //初期化
                $param = "";


                //共著者の所属機関
                $wa_kikanlist = $obj->_makeListBox($obj->arrForm["edata31"]);

                $ins_val = array();
                foreach($wa_kikanlist as $k => $data){
                    if(isset($obj->arrForm["edata32".$i.$k])){
                        array_push($ins_val, $obj->arrForm["edata32".$i.$k]);
                    }
                }
                $param["edata32"] = count($ins_val) > 0 ? implode("|", $ins_val) : "";


                $param["edata33"] = $obj->arrForm["edata33".$i];
                $param["edata34"] = $obj->arrForm["edata34".$i];
                $param["edata35"] = $obj->arrForm["edata35".$i];
                $param["edata36"] = $obj->arrForm["edata36".$i];
                $param["edata37"] = $obj->arrForm["edata37".$i];
                $param["edata38"] = $obj->arrForm["edata38".$i];
                $param["edata39"] = $obj->arrForm["edata39".$i];
                $param["edata40"] = $obj->arrForm["edata40".$i];
                $param["edata41"] = $obj->arrForm["edata41".$i];
                $param["edata42"] = $obj->arrForm["edata42".$i];
                $param["edata61"] = $obj->arrForm["edata61".$i];
                $param["edata62"] = $obj->arrForm["edata62".$i];


                //任意項目のタイプがチェックの場合
                $wk_nini = array("43", "44", "45", "65", "66"
                                ,"84","85","86","87","88","89","90","91","92","93","94","95","96","97","98");

                foreach($wk_nini as $nini_id){

                    if($obj->itemData[$nini_id]["item_type"] == "3"){
                        $ins_val = array();
                        foreach($obj->itemData[$nini_id]["select"] as $k => $data){
                            if(isset($obj->arrForm["edata".$nini_id.$i.$k])){
                                array_push($ins_val, $obj->arrForm["edata".$nini_id.$i.$k]);
                            }
                        }
                        $param["edata".$nini_id] = implode("|", $ins_val);
                    }
                    else{
                        $param["edata".$nini_id] = $obj->arrForm["edata".$nini_id.$i];
                    }
                }

                $param["form_id"] = $obj->formdata["form_id"];            //フォームID
                $param["eid"] = $obj->ins_eid;    //エントリーID


                //登録処理実行
                $rs = $obj->db->insert("entory_aff", $param, __FILE__, __LINE__);
                if(!$rs) {
                    return array(false, "", "");
                }
            }
        }

        //----------------------------------------
        //決済情報テーブル
        //----------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            $status = "0";

            // 登録パラメータ
            $param = array();
            $param["form_id"] = $obj->formdata["form_id"];      //フォームID
            $param["eid"] = $obj->ins_eid;                      //エントリーID
            $param["payment_method"] = $obj->arrForm["method"]; //支払方法

            //支払方法
            switch($obj->arrForm["method"]){
                //クレジットカード
                case "1":
                    // オフライン決済
                    if($obj->formdata["credit"] == "1"){
                        $status = 1;
                        $param["order_id"]  = NULL; //オーダーID
                        $param["c_company"] = NULL;
                        $param["c_number"]  = NULL;
                        $param["c_holder"]  = NULL;
                        $param["c_date"]    = NULL;
                        $param["c_method"]  = NULL;
                        $param["c_split"]   = NULL;
                        $param["c_scode"]   = NULL;

                    // オンライン決済
                    }else{
                        $status = 3;
                        $param["order_id"]  = $obj->auth_param["order_id"];  //オーダーID
                        $param["c_company"] = $obj->arrForm["card_type"];    //カード会社
                        $param["c_number"]  = $obj->o_crypt->crypt_encode(substr($obj->arrForm["cardNumber"], -4));
                        $param["c_holder"]  = $obj->arrForm["cardHolder"];
                        $param["c_date"]    = $obj->o_crypt->crypt_encode($obj->arrForm["cardExpire1"]."/".substr($obj->arrForm["cardExpire2"],2,2));
                        $param["c_method"]  = "10";
                        $param["c_split"]   = "1";
                        // セキュリティコードは保存しない
                        $param["c_scode"]   = NULL;
//                        $param["c_scode"]   = $obj->o_crypt->crypt_encode($obj->arrForm["securityCode"]);
                        $param["c_field_1"] = $obj->auth_param["access_id"];
                        $param["c_field_2"] = $obj->auth_param["access_pass"];
                        $param["c_field_3"] = $obj->auth_param["client_field3"];
                    }

                break;
                //銀行振り込み
                case "2":
                    $status = 4;
                    $param["lessee"] = $obj->arrForm["lessee"];
                    $param["bank"] = $obj->arrForm["bank"];
                    $param["closure"] = $obj->arrForm["closure"];

                break;
                case "3":
                break;
                case "4":
                break;
                case "5":
                break;
                case "6":

                break;
            }

            //支払金額が0円の場合
            if($obj->total_price == 0){
                $status = 99;
            }

            $param["payment_status"] = $status;
            $param["price"] = $obj->total_price;                //金額


            // CSV用見出しの登録----------------------------------------------------------------
            $param['csv_ather_price_head0'] = $obj->formdata["csv_ather_price_head0"];
            $param['csv_price_head0']         = $obj->formdata["csv_price_head0"];


            //登録処理実行
            $rs = $obj->db->insert("payment", $param, __FILE__, __LINE__);
            if(!$rs) {
                return array(false, "", "");
            }



            //------------------------------------------
            //支払詳細
            //------------------------------------------
            $wb_ret = $obj->_insPaymentDetail($obj->ins_eid);
            if(!$wb_ret){
                return array(false, "", "");
            }

        }



//        //--------------------------------------
//        //エントリー番号採番
//        //--------------------------------------
//        $rs = $obj->o_entry->updEntryNum($obj->db, $obj->formdata["form_id"], $obj->entry_number);
//        if(!$rs) {
//            return array(false, "", "");
//        }



        return array(true, $user_id, $passwd);

    }

    /**
     * エントリー情報更新処理
     *
     */
    function updateEntry($obj){
        //----------------------------------------------
        // 論文エントリーテーブル
        //----------------------------------------------
        $param = $obj->makeDbParam();
        $param["udate"] = "NOW";

        // アップロードファイル
        $date = date('Ymd');

        /// 削除対象のファイル
        $obj->wk_del = array();

        // リネイム後のファイル名
        $obj->wk_saki = array();

        foreach($obj->arrfile as $item_id => $n){
            if($obj->arrForm["n_data".$item_id] != "" && $obj->arrForm["file_del".$item_id] == "1") {
                $param["edata".$item_id] = "";
                $obj->wk_del[$item_id] = $obj->arrForm["n_data".$item_id];
            }

            // 画像をアップロードした場合のみファイル名を更新
            if($obj->arrForm['file_upload'.$item_id] != 1) continue;

            $wk_files = isset($obj->wk_moto[$item_id]) ? $obj->wk_moto[$item_id] : "";
            if($wk_files != "") {
                $extension = pathinfo($GLOBALS["session"]->getVar("workDir")."/".$wk_files, PATHINFO_EXTENSION);
                $obj->wk_saki[$item_id] = $GLOBALS["entryData"]["e_user_id"].$date.$n.".".$extension;
                $param["edata".$item_id] = $obj->wk_saki[$item_id];
            }
        }

        // 更新
        $where = "eid = ".$obj->db->quote($obj->eid);
        $rs = $obj->db->update("entory_r", $param, $where, __FILE__, __LINE__);
        if(!$rs) {
            return false;
        }
        unset($param);
        unset($where);


        //----------------------------------------------
        //共著者テーブル
        //----------------------------------------------
        //共著者ありの時
        if(isset($obj->arrForm["edata29"]) && $obj->arrForm["edata29"] == "1"){

            $wa_kyouid = $GLOBALS["session"]->getVar("kyouID");

            // 既に登録されている共著者情報に削除フラグを立てる
            /*$param["del_flg"] = "1";
            $param["udate"] = "NOW";
            $where[] = "eid = ".$obj->db->quote($obj->eid);
            $where[] = "form_id = ".$obj->db->quote($obj->formdata["form_id"]);
            $where[] = "del_flg = 0";
            $rs = $obj->db->update("entory_aff", $param, $where, __FILE__, __LINE__);
            if(!$rs) {
                return false;
            }
            unset($param);
            unset($where);*/

            for($i=0; $i < $obj->arrForm["edata30"]; $i++){

                //初期化
                $param = "";


                //共著者の所属機関
                $wa_kikanlist = $obj->_makeListBox($obj->arrForm["edata31"]);
                $ins_val = array();

                foreach($wa_kikanlist as $k => $data){
                    if(isset($obj->arrForm["edata32".$i.$k])){
                        array_push($ins_val, $obj->arrForm["edata32".$i.$k]);
                    }
                }
                $param["edata32"] = implode("|", $ins_val);


                $param["edata33"] = isset($obj->arrForm["edata33".$i]) ? $obj->arrForm["edata33".$i] : "";
                $param["edata34"] = isset($obj->arrForm["edata34".$i]) ? $obj->arrForm["edata34".$i] : "";
                $param["edata35"] = isset($obj->arrForm["edata35".$i]) ? $obj->arrForm["edata35".$i] : "";
                $param["edata36"] = isset($obj->arrForm["edata36".$i]) ? $obj->arrForm["edata36".$i] : "";
                $param["edata37"] = isset($obj->arrForm["edata37".$i]) ? $obj->arrForm["edata37".$i] : "";
                $param["edata38"] = isset($obj->arrForm["edata38".$i]) ? $obj->arrForm["edata38".$i] : "";
                $param["edata39"] = isset($obj->arrForm["edata39".$i]) ? $obj->arrForm["edata39".$i] : "";
                $param["edata40"] = isset($obj->arrForm["edata40".$i]) ? $obj->arrForm["edata40".$i] : "";
                $param["edata41"] = isset($obj->arrForm["edata41".$i]) ? $obj->arrForm["edata41".$i] : "";
                $param["edata42"] = isset($obj->arrForm["edata42".$i]) ? $obj->arrForm["edata42".$i] : "";


                //任意項目のタイプがチェックの場合
                $wk_nini = array("43", "44", "45", "65", "66"
                                ,"84","85","86","87","88","89","90","91","92","93","94","95","96","97","98");




                foreach($wk_nini as $nini_id){

                    if($obj->itemData[$nini_id]["item_type"] == "3"){

                        $ins_val = array();
                        foreach($obj->itemData[$nini_id]["select"] as $k => $data){
                            if(!isset($obj->arrForm["edata".$nini_id.$i.$k])) $obj->arrForm["edata".$nini_id.$i.$k] = "";
                            array_push($ins_val, $obj->arrForm["edata".$nini_id.$i.$k]);
                        }
                        $param["edata".$nini_id] = implode("|", $ins_val);

                    }
                    else{
                        $param["edata".$nini_id] = isset($obj->arrForm["edata".$nini_id.$i]) ? $obj->arrForm["edata".$nini_id.$i] : "";
                    }
                }

                $param["udate"] = "NOW";

                $where = "eid = ".$obj->db->quote($obj->eid);


                //共著者情報が存在する場合
                if(isset($wa_kyouid[$i])){
                    $param["del_flg"] = "0";
                    $where .= " and id = ".$obj->db->quote($wa_kyouid[$i]);
                    $rs = $obj->db->update("entory_aff", $param, $where, __FILE__, __LINE__);
                    if(!$rs) {
                        return false;
                    }

                }
                //共著者情報が存在しない場合
                else{

                    $param["rdate"] = "NOW";
                    $param["form_id"] = $obj->formdata["form_id"];            //フォームID
                    $param["eid"] = $obj->eid;


                    //登録処理実行
                    $rs = $obj->db->insert("entory_aff", $param, __FILE__, __LINE__);
                    if(!$rs) {
                        return array(false, "", "");
                    }
                }
            }
        }


        //----------------------------------------
        //決済情報テーブル
        //----------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){

            $wo_crypt = new CryptClass;
            $param = "";


            $param["payment_method"] = $obj->arrForm["method"];    //支払方法

            $status = "0";

            //支払方法
            switch($obj->arrForm["method"]){
                //クレジットカード
                case "1":
                    //オンライン・オフライン
                    $status = ($obj->formdata["credit"] == "1") ? 1 : 3;
                break;
                //銀行振り込み
                case "2":
                    $status = 4;
                    $param["order_id"] = $obj->auth_param["order_id"];
                    unset($obj->arrForm["card_type"]);
                    unset($obj->arrForm["cardNumber"]);
                    unset($obj->arrForm["cardExpire1"]);
                    unset($obj->arrForm["cardExpire2"]);
                    unset($obj->arrForm["cardHolder"]);
                    unset($obj->arrForm["jpo1"]);
                    unset($obj->arrForm["jpo2"]);
                    unset($obj->arrForm["securityCode"]);

                break;
                case "3":
                break;
                case "4":
                break;
                case "5":
                break;
                case "6":

                break;
            }


            $param["payment_status"] = $status;

//            $param["price"] = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);                //金額

            $param["c_company"] = (isset($obj->arrForm["card_type"]))   ? $obj->arrForm["card_type"] : "";            //カード会社
            $param["c_number"]  = (isset($obj->arrForm["cardNumber"]))  ? $obj->o_crypt->crypt_encode(substr($obj->arrForm["cardNumber"], -4)) : "";
            $param["c_date"]    = (isset($obj->arrForm["cardExpire1"])) ? $obj->o_crypt->crypt_encode($obj->arrForm["cardExpire1"]."/".substr($obj->arrForm["cardExpire2"],2,2)) : "";
            $param["c_holder"]  = (isset($obj->arrForm["cardHolder"]))  ? $obj->arrForm["cardHolder"] : "";
            // お支払い方法が銀行振込なのにクレジットカード決済情報が付与されるためコメントアウト
//            $param["c_method"]  = "10";
//            $param["c_split"]   = "1";

            $param["c_scode"] = NULL;
            // セキュリティコードは保存しない
//            $param["c_scode"] = (isset($obj->arrForm["securityCode"])) ? $obj->o_crypt->crypt_encode($obj->arrForm["securityCode"]) : "";


            $param["lessee"] = (isset($obj->arrForm["lessee"])) ? $obj->arrForm["lessee"] : "";
            $param["bank"] = (isset($obj->arrForm["bank"])) ? $obj->arrForm["bank"] : "";
            $param["closure"] =(isset($obj->arrForm["closure"])) ? $obj->arrForm["closure"] : "" ;
            $param["udate"] = "NOW";


            // CSV用見出しの登録----------------------------------------------------------------
//            $param['csv_ather_price_head0'] = $obj->formdata["csv_ather_price_head0"];
//            $param['csv_price_head0']         = $obj->formdata["csv_price_head0"];

            $where = "eid = ".$obj->db->quote($obj->eid);


            //処理実行
            $rs = $obj->db->update("payment", $param, $where, __FILE__, __LINE__);
            if(!$rs) {
                return array(false, "", "");
            }


/***
            //------------------------------------------
            //支払詳細
            //------------------------------------------
            //無効処理
            $param ="";
            $param["del_flg"] = "1";
            $param["udate"] = "NOW";

            $where = "eid = ".$obj->eid;


            //処理実行
            $rs = $obj->db->update("payment_detail", $param, $where, __FILE__, __LINE__);
            if(!$rs) {
                return array(false, "", "");
            }


            //登録処理
            $wb_ret = $obj->_insPaymentDetail($obj->eid);
            if(!$wb_ret){
                return array(false, "", "");
            }
***/

        }
        return true;
    }


    /**
     * 支払詳細項目登録
     *
     */
    function _insPaymentDetail($obj, $pn_eid){

        //------------------------------------------
        //支払詳細
        //------------------------------------------
        //通常項目
        $param = "";
        $param["eid"] = $pn_eid;
        $param["name"] = "事前登録料金";

        if($obj->formdata["pricetype"] == "2"){
            $param["name"] .= "（".$obj->wa_price[$obj->arrForm["amount"]]["name"]."）";
            $param["key"] = $obj->arrForm["amount"];
        }

        $param["price"] = $obj->wa_price[$obj->arrForm["amount"]]["p1"];
        $param["quantity"] = "1";


        //登録処理実行
        $rs = $obj->db->insert("payment_detail", $param, __FILE__, __LINE__);
        if(!$rs) {
            //return array(false, "", "");
            return false;
        }

        //その他決済項目
        if($obj->formdata["ather_price_flg"] != "1"){
            foreach($obj->wa_ather_price as $pkey => $data){
                if($obj->arrForm["ather_price".$pkey] !=""  && $obj->arrForm["ather_price".$pkey] != "0"){
                    $param = "";
                    $param["eid"] = $pn_eid;
                    $param["name"] = $data["name"];
                    $param["price"] = $data["p1"];
                    $param["quantity"] = $obj->arrForm["ather_price".$pkey];
                    $param["type"] = "1";
                    // 何番目の項目を選択したか保存する
                    $param["key"] = $pkey;

                    //登録処理実行
                    $rs = $obj->db->insert("payment_detail", $param, __FILE__, __LINE__);
                    if(!$rs) {
                        //return array(false, "", "");
                        return false;
                    }
                }
            }
        }

        return true;

    }

    /**
     * 決済メッセージ取得
     *
     */
    function _getPaymentMsg($obj, $ps_code){

        if($ps_code == ""){
            return false;
        }

        $column = "*";
        $from = "payment_msg";
        $where[] = "msg_code = ".$obj->db->quote($ps_code);
        $where[] = "del_flg = 0";
        $where[] = "lang = ".$obj->db->quote($obj->formdata["lang"]);


        $rs = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);

        return $rs;

    }


    /** エントリーの無効処理 */
    function entryInvalid($obj){
        if(!isset($obj->eid)) return false;
        if(strlen($obj->eid) == 0) return false;

        $param = array();
        $param["invalid_flg"] = $obj->auth_param['invalid_flg'];

        $where = array();
        $where[] = "eid = ".$obj->db->quote($obj->eid);
        $obj->db->update("entory_r", $param, $where, __FILE__, __LINE__);
        unset($param);

        // 決済を与信待ちにする
        $param = array();
        $param["payment_status"] = "1";

        $obj->db->update("payment", $param, $where, __FILE__, __LINE__);
        unset($param);
        unset($where);
    }


    function restoreEid($obj, $access_id){
        if(strlen($access_id) == 0) return -1;

        $column = "eid";
        $from = "payment";
        $where   = array();
        $where[] = "c_field_1 = ".$obj->db->quote($access_id);

        $res = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!is_array($res)){
            return -1;
        }
        return $res['eid'];
    }


    /* 編集の前後の入力値の差分を求める */
    function getParamEditDiff($obj){
        // 編集の前後の差分
        $arrDiff = array();

        if(!$obj->save_editbefore) return $arrDiff;

        // 編集前の登録情報
        $arrData = $GLOBALS["session"]->getVar("form_editbefore");
        // 編集後の登録情報
        $arrForm = Usr_entryDB::makeDbParam($obj);

        // ファイルアップロード
        $filekeys = array_keys($obj->arrfile);

        // 差分を求める
        $diff = array_diff_assoc($arrData, $arrForm);
        if(!empty($diff)){
            foreach($diff as $_key => $_val){
                // edataXX
                $item_id  = substr($_key, 5);
                // ファイルアップロードは後で処理
                if(in_array($item_id, $filekeys)) continue;

                if(!isset($arrData[$_key])) $arrData[$_key] = "";
                if(!isset($arrForm[$_key])) $arrForm[$_key] = "";
                if($arrData[$_key] == $arrForm[$_key]) continue;

                $group_id = $obj->itemData[$item_id]['group_id'];
                $arrDiff[$group_id][$_key] = new stdClass();
                $arrDiff[$group_id][$_key]->before = $arrData[$_key];
                $arrDiff[$group_id][$_key]->after  = $arrForm[$_key];
            }
        }
        // ファイルアップロードの差分
        if(count($obj->wk_saki) > 0){
            foreach($filekeys as $_key => $item_id){
                if(!isset($obj->wk_saki[$item_id])) continue;

                $_key = 'edata'.$item_id;
                $group_id = $obj->itemData[$item_id]['group_id'];
                $arrDiff[$group_id][$_key] = new stdClass();
                $arrDiff[$group_id][$_key]->before = $arrData[$_key];
                $arrDiff[$group_id][$_key]->after  = $obj->wk_saki[$item_id];
            }
        }

        return $arrDiff;
    }

}

?>