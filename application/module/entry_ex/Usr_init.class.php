<?php

class Usr_init {

    /* 任意項目のitem_type */
    const INPUT_TEXT   = 0;
    const INPUT_AREA   = 1;
    const INPUT_RADIO  = 2;
    const INPUT_CHECK  = 3;
    const INPUT_SELECT = 4;

    /* Usr_init::_initの添え字 */
    const INIT_EDATA  = 0;
    const INIT_NAME   = 1;
    const INIT_LEN    = 2;
    const INIT_CHECK  = 3;
    const INIT_OPTION = 4;
    const INIT_DBFLG  = 5;


    /**
     * 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init($obj, $ps_block){
        /* 標準項目の中でtext,textarea以外の項目 */ 
        $obj->defaultItemSelect = array(
                                        1 => array(8, 16, 18, 57, 114, 29, 30)
                                       ,2 => array(32, 61, 41)
                                       ,3 => array(48)
                                   );

        // 項目キー
        $key = array();

        //---------------------------------
        // ブロック別処理
        //---------------------------------
        $initName = "_init".$ps_block;
        if(method_exists($this, $initName)){
            $key = $obj->$initName();
        }
        return $key;
    }



    /**
     * 1ページ目 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init1($obj){
        $key      = array();
        $ps_block = 1;

        foreach($obj->arrItemData[$ps_block] as $_key => $val){
            if($val["disp"] == "1") continue;

            $item_id = $val['item_id'];

            // 任意項目
            if($val['controltype'] == 1){
                $obj->_initNini($key, $item_id);

            // 標準項目
            }else{
                $pn_type = in_array($item_id, $obj->defaultItemSelect[$ps_block]) ? 1 : 0;
                // 日本語フォーム固有
                if($obj->formdata["lang"] == LANG_JPN){
                    // 郵便番号
                    if($item_id == 17) $val["item_check"] = rtrim($val["item_check"])."|17";
                    // 電話・FAX番号
                    if(in_array($item_id, array(21,24))) $val["item_check"] = rtrim($val["item_check"])."|21";
                }

                // エラーチェック項目の生成
                list($arrChk, $arrChklen) = Usr_init::_setCheckMethod($obj, $val["item_check"], $val["item_len"], $pn_type);

                $key[] = array("edata".$item_id, $val['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
                // 確認用メールアドレス
                if($item_id == 25){
                    $key[] = array("chkemail", $obj->itemData[25]['strip_tags']."確認用", array(),    array(),    "",    0);
                }
            }
        }

        //同意書
        if($obj->formdata["agree"] == "1"){
            $key[] = array("agree", "同意文", array(),    array(),    "",    0);
        }

        //------------------------------
        //決済ありの場合
        //------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            if($obj->formdata["lang"] == LANG_JPN){
                $method = "支払方法";
            }
            else{
                $method = "Payment";
            }
            $key[] = array("amount", "金額", array(),    array(),    "",    0);
            $key[] = array("method", $method, array(),    array("SELECT"),    "",    0);
        }


        //-------------------------------
        //その他決済を利用する場合
        //-------------------------------
        if($obj->formdata["kessai_flg"] == "1" && $obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){
            foreach($obj->wa_ather_price as $pkey => $data){
                $key[] = array("ather_price".$pkey, $data["name"], array(),    array("NUMERIC"),    "",    0);
            }
        }

        return $key;
    }



    /**
     * 2ページ目 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init2($obj){
        $key      = array();
        $ps_block = 2;

        $group2 = ($obj->o_form->formData['group2'] == "")
                ? "共著者"
                : $obj->o_form->formData['group2'];

        if(isset($_REQUEST["edata30"])){
            $loop = $_REQUEST["edata30"];
        }
        else{
            $session_data = $obj->arrForm = $GLOBALS["session"]->getVar("form_param".$obj->block);
            $loop = $session_data["edata30"];
        }


        //画面で入力した数だけ、パラメータを生成
        $k_num =0;
        for($i=0; $i < $loop; $i++){
            $k_num++;

            foreach($obj->arrItemData[$ps_block] as $_key => $val){
                if($val["disp"] == "1") continue;

                $item_id = $val['item_id'];

                // 任意項目
                if($val['controltype'] == 1){
                    $obj->_initChosyaNini($key, $item_id, $i, $k_num);

                // 標準項目
                }else{
                    list($arrChk, $arrChklen) = Usr_init::_setCheckMethod($obj, $val["item_check"], $val["item_len"]);
                    $key[] = array("edata".$item_id.$i, $group2.$k_num."　".$val['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
                }
            }
        }

        return $key;
    }



    /**
     * 3ページ目 項目情報初期化
     *
     * @param stirng block番号
     */
    function _init3($obj){
        $key      = array();
        $ps_block = 3;

        foreach($obj->arrItemData[$ps_block] as $_key => $val){
            if($val["disp"] == "1") continue;

            $item_id = $val['item_id'];

            // 任意項目
            if($val['controltype'] == 1){
                $obj->_initNini($key, $item_id);

            // 標準項目
            }else{
                // ファイルアップロード
                if(in_array($item_id, array_keys($obj->arrfile))){
                    $key[] = array("n_data".$item_id,   $obj->itemData[$item_id]['strip_tags'], array(),    array(),    "",    0);
                    $key[] = array("hd_file".$item_id,  $obj->itemData[$item_id]['strip_tags'], array(),    array(),    "",    0);
                    $key[] = array("file_del".$item_id, $obj->itemData[$item_id]['strip_tags'], array(),    array(),    "",    0);
                    $key[] = array("file_upload".$item_id, $obj->itemData[$item_id]['strip_tags'], array(),    array(),    "",    0);

                // その他の標準項目
                }else{
                    $pn_type = in_array($item_id, $obj->defaultItemSelect[$ps_block]) ? 1 : 0;
                    list($arrChk, $arrChklen) = Usr_init::_setCheckMethod($obj, $val["item_check"], $val["item_len"], $pn_type);
                    $key[] = array("edata".$item_id, $val['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
                }
            }
        }

        return $key;
    }


    // アップロードファイルの添付IDと添え字
    function _initFile($obj){
        $obj->arrfile = array(
                            "51" => "a",
                            "52" => "b",
                            "53" => "c"
                        );
    }


    /**
     * 決済ページ目 項目情報初期化
     *
     * @param stirng block番号
     */
    function _initpay($obj){
        $key      = array();
        $ps_block = "pay";

        $wk_sesssion = $GLOBALS["session"]->getVar("form_param1");
        if(!isset($wk_sesssion["method"])) $wk_sesssion["method"] = "";

        switch($wk_sesssion["method"]){
            //クレジットカードの場合
            case "1":

                if($obj->formdata["lang"] == LANG_JPN){
                    $card_agree   = "同意";
                    $card_type    = "カード会社";
                    $cardNumber   = "クレジットカード番号";
                    $cardExpire1  = "有効期限（月）";
                    $cardExpire2  = "有効期限（年）";
                    $cardHolder   = "カード名義人";
                    $securityCode = "セキュリティコード";
                }
                else{
                    $card_agree   = "同意";
                    $card_type    = "Card Type";
                    $cardNumber   = "Card Number";
                    $cardExpire1  = "Expiration Date(Month)";
                    $cardExpire2  = "Expiration Date(Year )";
                    $cardHolder   = "Card Holder's Name";
                    $securityCode = "SecurityCode";

                }

                //$key[] = array("amount", "金額", array(),    array("SELECT"),    "",    0);
                $key[] = array("credit_agree", $card_agree , array(),  array()                  ,   "",  0);
                $key[] = array("card_type"   , $card_type  , array(),  array("SELECT")          ,   "",  0);
                $key[] = array("cardNumber"  , $cardNumber , array(),  array("NULL","NUMERIC")  ,  "n",  0);
                $key[] = array("cardExpire1" , $cardExpire1, array(),  array("NULL")            ,   "",  0);
                $key[] = array("cardExpire2" , $cardExpire2, array(),  array("NULL")            ,   "",  0);

                $key[] = array("cardHolder", $cardHolder, array(),    array("NULL"),    "a",    0);

                //$key[] = array("jpo1", "支払区分", array(),    array("SELECT"),    "",    0);

                //支払回数
//                    $chk = array();
//                    if($_REQUEST["jpo1"] == "61"){
//                        $chk = array("NULL");
//                    }
//
//
//                    $key[] = array("jpo2", "支払回数", array(),    $chk,    "",    0);

                $key[] = array("securityCode", $securityCode, array(),    array("NULL"),    "n",    0);

             break;
             //銀行振り込みの場合
            case "2":
                $key[] = array("lessee", "お振込み人名義", array(),    array(),    "",    0);
                $key[] = array("bank", "お支払い銀行名", array(),    array(),    "",    0);
                $key[] = array("closure", "お振込み日", array(),    array(),    "",    0);
             break;
        }

        return $key;
    }



    /**
     * パラメータ初期設定　任意項目
     *     各ページの任意項目のパラメータ初期設定を行う
     *
     */
    function _initNini($obj, &$key, $index){
        $mode = "";

        //タイプによって異なる
        if($obj->itemData[$index]["item_type"] == "2" || $obj->itemData[$index]["item_type"] == "4"){
            $mode = "1";
        }
        //入力チェック項目
        list($arrChk, $arrChklen)  = Usr_init::_setCheckMethod($obj, $obj->itemData[$index]["item_check"], $obj->itemData[$index]["item_len"], $mode);

        if($obj->itemData[$index]["item_type"] == "3"){

            //チェック・ラジオボタン・セレクトボックス
            if($obj->itemData[$index]["select"] != ""){
                foreach($obj->itemData[$index]["select"] as $num => $val){
                    $key[] = array("edata".$index.$num, $obj->itemData[$index]['strip_tags'], array(),    array(),    "",    0);
                }
                // エラーメッセージの並び替え用に仕込んでおく
                // エラーチェック部はUsr_check._checkNiniによって実行される
                $key[] = array("edata".$index     , $obj->itemData[$index]['strip_tags'], array(),    array(),    "",    0);
            }

        }
        else{
            $key[] = array("edata".$index, $obj->itemData[$index]['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
        }
    }


    /**
     * パラメータ初期設定　共著者情報の任意項目
     */
    function _initChosyaNini($obj, &$key, $index, $k, $k_num){
        $group2 = ($obj->o_form->formData['group2'] == "")
                ? "共著者"
                : $obj->o_form->formData['group2'];


        $mode = "";

        //タイプによって異なる
        if($obj->itemData[$index]["item_type"] == "2" || $obj->itemData[$index]["item_type"] == "4"){
            $mode = "1";
        }

        //入力チェック項目
        list($arrChk, $arrChklen)  = Usr_init::_setCheckMethod($obj, $obj->itemData[$index]["item_check"], $obj->itemData[$index]["item_len"], $mode);

        if($obj->itemData[$index]["item_type"] == "3"){

            //チェック・ラジオボタン・セレクトボックス
            if($obj->itemData[$index]["select"] != ""){
                foreach($obj->itemData[$index]["select"] as $num => $val){
                    $key[] = array("edata".$index.$k.$num, $group2.$k_num."　".$obj->itemData[$index]['strip_tags'], array(),    array(),    "",    0);
                }
            }
        }
        else{
            $key[] = array("edata".$index.$k, $group2.$k_num."　".$obj->itemData[$index]['strip_tags'], $arrChklen,    $arrChk,    "K",    1);
        }
    }


    /**
     * 所属期間名リストボックス生成
     */
    function _makeListBox($obj, $ps_val){
        $retList = "";
        if(trim($ps_val) == ""){
            return $retList;
        }

        $arrList = explode("\n", $ps_val);

        $num = 0;
        foreach($arrList as $key => $data){
            $num = $key+1;
            $retList[$num] = $data;
        }

        return $retList;

    }

    /**
     * 入力チェックの項目設定
     *
     * @access public
     * @param string パイプ区切りで登録されているチェックする項目
     * @param string 長さ
     * @param stirng モード　１：選択形式の項目 NULLの場合は入力形式の項目
     */
    function _setCheckMethod($obj, $ps_chk, $pn_len, $pn_type=""){
        //返却配列
        $ret_chk = array("TOKUSYU");
        $len = array();

        if($ps_chk == ""){

            return array($ret_chk, $len);
        }

        $arr_chk = explode("|", $ps_chk);
        foreach($arr_chk as $data){

            switch($data){
                case "0":
                    if($pn_type == "1"){
                        array_push($ret_chk, "SELECT");
                    }
                    else{
                        array_push($ret_chk, "NULL");
                    }
                    break;

                case "1":
                    array_push($ret_chk, "BYTE");
                    break;

                case "2":
                    array_push($ret_chk, "MOJICNT");
                    break;

                case "3":
                    array_push($ret_chk, "WORDCHK");
                    break;

                case "4":
                    array_push($ret_chk, "ZEN");
                    break;

                case "5":
                    array_push($ret_chk, "HAN");
                    break;

                case "6":
                    array_push($ret_chk, "MAIL");
                    break;

                // 郵便番号はフォーマットチェックを追加
                case "17":
                    array_push($ret_chk, "ZIP");
                    break;

                // 電話番号はフォーマットチェックを追加
                case "21":
                    array_push($ret_chk, "TEL");
                    break;
            }

        }


        //$ret_chk = implode(",", $ret_chk);

        if($pn_len != ""){
            array_push($len, 0);
            array_push($len,$pn_len);

        }


        return array($ret_chk, $len);
    }



    /**
     * フォーム番号35　所属機関パラメータ生成
     */
    function _make35param($obj, $pn_block){
        //２ページ目の場合
        if($pn_block == 2){

            $ret_data = array();


            //共著者の数をセッションから復元
            $form1data = $GLOBALS["session"]->getVar("form_param1");
            $form1data["edata31"] = (isset($form1data["edata31"])) ? $form1data["edata31"] : "";
            $wa_kikanlist = $obj->_makeListBox($form1data["edata31"]);

            if($form1data["edata31"] == "") return $ret_data;


            $cyosya = $form1data["edata30"];

            for($i = 0; $i < $cyosya; $i++){

                foreach($wa_kikanlist as $key => $data ){

                    if(isset($_REQUEST["edata32".$i.$key])){
                        $ret_data["edata32".$i.$key] = $_REQUEST["edata32".$i.$key];
                    }
                }
            }
            return $ret_data;
        }
    }


    /**
     * 初期表示時、任意項目の値を生成
     *     項目の開始番号と終了番号を渡すと初期表示時を生成する
     */
    function _default_arrFormNini($obj, $start, $end, $wa_entrydata){
        for($index=$start; $index < $end; $index++){
            if(!isset($wa_entrydata["edata".$index])) $wa_entrydata["edata".$index] = "";

            if($obj->itemData[$index]["item_type"] == "3"){

                $wk_checked  = explode("|", $wa_entrydata["edata".$index]);

                foreach($wk_checked as $chk_val){
                    $obj->arrForm["edata".$index.$chk_val] = $chk_val;
                }
            }
            else{
                $obj->arrForm["edata".$index] = $wa_entrydata["edata".$index];
            }
        }
    }


    /**
     * 初期表示時、任意項目の値を生成（共著者情報部分）
     *     項目の開始番号と終了番号を渡すと初期表示時を生成する
     */
    function _default_arrFormChosyaNini($obj, $start, $end, $data, $a_key){
        for($index=$start; $index < $end; $index++){
            if($obj->itemData[$index]["item_type"] == "3"){
                if(!isset($data["edata".$index])) continue;

                $wk_checked  = explode("|", $data["edata".$index]);
                foreach($wk_checked as $chk_val){
                    $obj->arrForm["edata".$index.$a_key.$chk_val] = $chk_val;
                }
            }
            else{
                $obj->arrForm["edata".$index.$a_key] = $data["edata".$index];
            }
        }
    }

    /* 項目情報を取得する */
    function getItemInfo($obj, $item_id, $getString="item_name"){
        return strip_tags($obj->itemData[$item_id][$getString]);
    }


    /* 任意項目のフォーム別のエラーメッセージを返す */
    function getItemErrMsg($obj, $item_id){
        $item_type = Usr_init::getItemInfo($obj, $item_id, "item_type");
        switch($item_type){
            case Usr_init::INPUT_TEXT:
            case Usr_init::INPUT_AREA:
                $method = $GLOBALS["msg"]["err_require_input"];
                break;
            case Usr_init::INPUT_RADIO:
            case Usr_init::INPUT_CHECK:
            case Usr_init::INPUT_SELECT:
                $method = $GLOBALS["msg"]["err_require_select"];
                break;
        }
        return $method;
    }


    /*
     * Usr_init::_initで取得したarray::$keyの添え字を取得する
     * 
     * @param  array  $key   Usr_init::_init[1-3]
     * @param  string $edata 取得対象のedata名 ex.edata26
     * @return int    $i     $keyの一次元の添え字
     */
    function getKey($key, $edata=""){
        $i = -1;
        if(strlen($edata) == 0) return $i;
        if(!is_array($key))     return $i;
        if(count($key) == 0)    return $i;

        foreach($key as $_i => $_key){
            if($edata !== $_key[0]) continue;

            $i = $_i;
            break;
        }
        return $i;
    }


    /* 
     * Usr_init::_initで取得したarray::$keyの添え字を取得する
     * ex.Usr_init::overrideKey($key, "edata28", Usr_init::INIT_OPTION, "nk");
     * 
     * @param  array  $key    Usr_init::_init[1-3]
     * @param  string $edata  取得対象のedata名 ex.edata26
     * @param  string $option 上書き対象の添え字
     * @param  mixed  $val    上書きする値
     * @return boolean
     */
    function overrideKey(&$key, $edata, $option, $val){
        $bool = false;
        if(strlen($edata) == 0) return $i;
        if(!is_array($key))     return $i;
        if(count($key) == 0)    return $i;

        $i = Usr_init::getKey($key ,$edata);
        $key[$i][$option] = $val;
    }


    /* 
     * 項目定義を上書きする
     * 
     * @param  object  $obj      Usr_entry
     * @param  integer $item_id  上書き対象の項目ID
     * @param  string  $option   上書き対象の添え字
     * @param  mixed   $val      上書きする値
     * @return void
     */
    function overrideItem($obj, $item_id, $option, $val){
        $group_id = Usr_init::getItemInfo($obj, $item_id, "group_id");
        $obj->arrItemData[$group_id][$item_id][$option] = $val;
    }


    /*
     * Usr_init::_initで取得したarray::$keyが有効か判定する
     * 
     * @param  object  $obj ページオブジェクト
     * @param  integer $group_id
     * @param  integer $item_id
     * @return boolean true:項目を利用している
     * 
     **/
    function isset_ex($obj, $group_id="", $item_id=""){
        if(strlen($item_id) == 0) return true;

        $key = 'edata'.$item_id;
        if($obj->arrItemData[$group_id][$item_id]['disp'] != 1){
            $item_type = Usr_init::getItemInfo($obj, $item_id, "item_type");
            switch($item_type){
                case Usr_init::INPUT_CHECK:
                    $select  = $obj->arrItemData[$group_id][$item_id]['select'];
                    if(count($select) > 0){
                        foreach($select as $i => $_value){
                            if(isset($obj->arrParam[$key.$i]) && strlen($obj->arrParam[$key.$i]) > 0){
                                return true;
                            }
                        }
                        return false;
                    }
                    return false;
                    break;

                default:
                    return isset($obj->arrParam[$key]);
                    break;
            }
        }
        return false;
    }


    /**
     * エラーチェックを追加する
     * 
     * @param  object  $obj
     * @param  array   $target 追加対象の項目ID配列
     * @param  integer $check  チェック内容
     * @param  string  $sep    チェック内容の区切り字
     * @return void
     * */
    public static function addCheck($obj, $target=array(), $check="0", $sep="|"){
        foreach($target as $item_id){
            $item_type = Usr_init::getItemInfo($obj, $item_id, "item_type");
            switch($item_type){
                case Usr_init::INPUT_CHECK:
                   $obj->itemData[$item_id]["need"] = "1";
                   break;

                default:
                    $itemCheck = explode($sep, $obj->itemData[$item_id]["item_check"]);
                    $nullCheck = array_search($check, $itemCheck);
                    if($nullCheck === false){
                        $itemCheck[] = $check;
                        $obj->itemData[$item_id]["item_check"] = trim(implode($sep, $itemCheck), $sep);
                    }
                    break;
            }
        }
    }

}


?>