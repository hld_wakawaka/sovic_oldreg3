<?php

/* CSV,メール等で利用する共通のAssign関数群
 * edata別にメソッドを定義
 */
class Usr_Assign {

    public function edataXX($params, &$smarty){
        extract($params);
        if(!isset($item_id))  return;
        if(!isset($group_id)) return;

        $obj  = $smarty->get_template_vars();
        $arrForm     = $obj['arrForm'];
        $arrItemData = $obj['arrItemData'];
        if(isset($disp)) $arrItemData[$group_id][$item_id]['disp'] = $disp;

        $key = 'edata'.$item_id;

        $smarty->assign('item_id',   $item_id);
        $smarty->assign('_formItem', $arrItemData[$group_id][$item_id]);
        $smarty->assign('edata',     $key);
        $smarty->assign('data',      $arrForm[$key]);
    }


    // 標準 # Are you a member ～～～? (Yes・No)
    public static function edata8($obj){
        $value = "";
        if(isset($obj->arrForm["edata8"]) && isset($obj->wa_kaiin[$obj->arrForm["edata8"]])){
            $value = $obj->wa_kaiin[$obj->arrForm["edata8"]];
        }
        return $value;
    }

    // 標準 # Contact (Office・Home)
    public static function edata16($obj){
        $value = "";
        if(isset($obj->arrForm["edata16"]) && strlen($obj->arrForm["edata16"]) > 0){
            $str = $obj->wa_contact[$obj->arrForm["edata16"]];
        }
        return $value;
    }

    // 標準 # 都道府県
    public static function edata18($obj){
        $value = "";
        if($obj->formdata["lang"] != LANG_JPN){
            if(isset($obj->arrForm["edata18"])){
                $value = $obj->arrForm["edata18"];
            }
        }else{
            if(isset($obj->arrForm["edata18"]) && isset($GLOBALS["prefectureList"][$obj->arrForm["edata18"]])){
                $value = $GLOBALS["prefectureList"][$obj->arrForm["edata18"]];
            }
        }
        return $value;
    }

    // 標準 # Mr. Mrs.
    public static function edata57($obj){
        $value = "";
        if($obj->arrForm["edata57"] != ""){
            $value = $GLOBALS["titleList"][$obj->arrForm["edata57"]];
        }
        return $value;
    }

    // 標準 # Country
    public static function edata114($obj){
        $value = "";
        if(isset($obj->arrForm["edata114"]) && isset($GLOBALS["country"][$obj->arrForm["edata114"]])) {
            $value = $GLOBALS["country"][$obj->arrForm["edata114"]];
        }
        return $value;
    }

    // 任意
    public static function nini($obj, $group, $item_id, $param, $arrsep=array("\n ", "\n "), $trim=false){
        $value = '';
        $sep1 = $arrsep[0];
        $sep3 = $arrsep[1];

        $key = "edata".$item_id;

        // 選択肢
        $select = $obj->arrItemData[$group][$item_id]["select"];

        switch($obj->arrItemData[$group][$item_id]["item_type"]) {
            case Usr_init::INPUT_AREA:
                // 改行コード除去フラグ
                if($trim){ $param = str_replace(array("\n", "\r", "\n\r"), " ", $param); }
                $value = $sep1.$param;
                break;

            case Usr_init::INPUT_RADIO:
            case Usr_init::INPUT_SELECT:
                if(isset($select[$param])) {
                    $value = trim($select[$param]);
                }
                break;

            case Usr_init::INPUT_CHECK:
                if(!isset($select)) break;

                if(strlen($param) > 0){
                    $arrForm = $obj->arrForm;
                    $obj->arrForm = array();
                    $chkeck = explode("|", $param);
                    foreach($chkeck as $n){
                        if($n != "" && array_key_exists($n, $select)){
                            $obj->arrForm[$key.$n] = $n;
                        }
                    }
                }

                // TODO 特殊項目を考慮していない
                foreach($select as $n=>$val) {
                    if(!isset($obj->arrForm[$key.$n])) continue;
                    if($obj->arrForm[$key.$n] == $n) {
                        $value .= $sep3.trim($select[$n]);
                    }
                }

                if(strlen($param) > 0){
                    $obj->arrForm = $arrForm;
                }
                break;

            case Usr_init::INPUT_TEXT:
            default:
                $value = $param;
        }
        return $value;
    }

}

?>
