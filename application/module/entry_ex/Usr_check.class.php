<?php


/**
 * 論文エントリーフォーム
 *
 *
 * @subpackage Usr
 * @author salon
 *
 */
class Usr_Check {


    function _chkPeriod($obj) {
        return $obj->o_form->checkValidData_Ex();
    }


    /**
     * 一次応募期間中チェック
     * 
     * @return boolean
     */
    function _chkTerm($obj){
        $end = mktime(substr($obj->formdata["primary_reception_date"], 11,2),substr($obj->formdata["primary_reception_date"], 14,2),substr($obj->formdata["primary_reception_date"], 17,2),substr($obj->formdata["primary_reception_date"], 5,2), substr($obj->formdata["primary_reception_date"], 8,2), substr($obj->formdata["primary_reception_date"], 0,4));
        $today = time();

        //比較
        if($today < $end){
            return true;
        }
        return false;
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check($obj){
        $checkmethod = "_check".$obj->wk_block;

        if(method_exists($obj, $checkmethod)) {
            $obj->$checkmethod();
        }

        $obj->objErr->sortErr($obj->_init($obj->wk_block));
        return $obj->objErr->_err;
    }


    /**
     * 入力チェック
     *
     * @return array
     */
    function _check1($obj){
        //管理者モードの場合でも行う処理
        if($obj->itemData[29]["disp"] != "1"){
            if(isset($obj->arrParam["edata29"]) && $obj->arrParam["edata29"] == "2"){
                $obj->arrParam["edata30"] = "";
                $obj->arrParam["edata31"] = "";
            }
        }

        // 管理者はここまで
        if($obj->admin_flg == "1") return;


        //氏名、メールアドレス一致チェック
        if(!$obj->checkRepeat($obj->arrParam)){
            $msg = ($obj->formdata["lang"] == LANG_JPN)
                 ? "既にご登録済みです。"
                 :"You are already registered.";
            $obj->objErr->addErr($msg, "edata25");
        }

        // 基本チェック
        $obj->objErr->check($obj->_init(1), $obj->arrParam);

        //同意書
        if($obj->formdata["agree"] == "1"){
            if($obj->arrParam["agree"] == "" ){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? "同意文をご覧になり、同意の上、お進みください"
                     : "Please read the agreement and proceed further only if you agree.";
                $obj->objErr->addErr($msg, "agree");
            }
        }

        //メールアドレス一致チェック
        if($obj->arrParam["edata25"] != "" && $obj->itemData[25]["mail"] == "1"){
            if($obj->arrParam["edata25"] != $_REQUEST["chkemail"]){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? $obj->itemData[25]['strip_tags']."と".$obj->itemData[25]['strip_tags']."（確認用）の入力内容が一致していません。"
                     : $obj->itemData[25]['strip_tags']. " and ".$obj->itemData[25]['strip_tags']."（confirm）  do not match.";
                $obj->objErr->addErr($msg, "edata25");
            }
        }

        // 共著者チェック
//        $obj->_checkAuthor();

        //任意項目
        $obj->_checkNini(1);

        //英語フォームの場合、国のチェック
        if($obj->formdata["lang"] == LANG_ENG){
            //国名プルダウンが必須の場合
            if($obj->itemData[114]["disp"] != "1"){
                if($obj->itemData[114]["need"] == "1" && $obj->arrParam["edata114"] == "239"){
                    if($obj->arrParam["edata60"] == ""){
                        $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[60]['strip_tags']);
                        $obj->objErr->addErr($msg, "edata60");
                    }
                }

            }else{
                if($obj->itemData[60]["disp"] != "1" && $obj->itemData[60]["need"] == "1"){
                    if($obj->arrParam["edata60"] == ""){
                        $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[60]['strip_tags']);
                        $obj->objErr->addErr($msg, "edata60");
                    }
                }
            }
        }



        //------------------------------
        //支払金額
        //------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            // 編集時はチェックしない
            if($obj->eid != "") return;

            // 合計金額が0円の場合はお支払い方法を入力しなくてもOK
            $total_price = Usr_function::_setTotal($obj->wa_price, $obj->arrParam, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
            if($total_price == 0){
                unset($obj->objErr->_err["method"]);
            }

            if($obj->arrParam["amount"] == ""){
                if($obj->formdata["lang"] == LANG_JPN){
                    $obj->objErr->addErr("お支払い金額を選択してください。", "amount");
                }
                else{
                    $obj->objErr->addErr("Select one(s) for Fee.", "amount");
                }
            }

            //-------------------------------
            //その他決済を利用する場合
            //-------------------------------
            if($obj->formdata["kessai_flg"] == "1" && $obj->formdata["ather_price_flg"] != "1" && $obj->formdata["ather_price"] != ""){
                foreach($obj->wa_ather_price as $pkey => $data){
                    if($data["limit"] != "0"){
                        if($obj->arrParam["ather_price".$pkey] > $data["limit"] ){
                            if($obj->formdata["lang"] == LANG_JPN){
                                $obj->objErr->addErr($data["name"]."は".$data["limit"].$data["tani"]."以内で入力してください。", "ather_price".$pkey);
                            }
                            else{
                                $obj->objErr->addErr("The maximum allowed number of ".$data["name"]." is ".$data["limit"].".", "ather_price".$pkey);
                            }
                        }
                    }
                }
                // その他決済項目の必須設定をON @ Config.add_error_exist_otherprice
                if($obj->exist_otherprice){
                    $values = array();
                    foreach($obj->wa_ather_price as $pkey => $data){
                        $values[$pkey] = $obj->arrParam["ather_price".$pkey];
                    }
                    // 未選択
                    if(array_sum($values) == 0){
                        $otherPriceName = strlen($obj->exist_otherprice_title) > 0 ? $obj->exist_otherprice_title : "";
                        if(strlen($otherPriceName) == 0){
                            $otherPriceName = $obj->formdata["lang"] == LANG_JPN ? "お支払い金額" : "Fee";
                        }
                        $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_require_select"], $otherPriceName), "exist_otherprice");
                    }
                }
            }
        }
        return;
    }


    //----------------------------
    //共著者
    //-----------------------------
    function _checkAuthor($obj) {
        if($obj->itemData[29]["disp"] != "1"){
            if($obj->arrParam["edata29"] == ""){
                $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[29]['strip_tags']);
                $obj->objErr->addErr($msg, "edata29");
            }
            else{
                //共著ありの場合
                if($obj->arrParam["edata29"] == "1"){

                    //共著者の数
                    if($obj->itemData[30]["disp"] != "1"){
                        if($obj->arrParam["edata30"] == ""){
                            $msg = sprintf($GLOBALS["msg"]["err_require_input"], $obj->itemData[30]['strip_tags']);
                            $obj->objErr->addErr($msg, "edata30");
                        }
                        else{
                            //半角数字で入力されていない場合
                            if(!$obj->objErr->isNumeric($obj->arrParam["edata30"])){
                                if($obj->formdata["lang"] == LANG_JPN){
                                    $msg = $obj->itemData[30]['strip_tags']."は、半角数字で入力してください。";
                                }
                                else{
                                    $msg = sprintf($GLOBALS["msg"]["err_han_numeric"], $obj->itemData[30]['strip_tags']);
                                }

                                $obj->objErr->addErr($msg, "edata30");
                            }

                            if($obj->arrParam["edata30"] > 15){
                                if($obj->formdata["lang"] == LANG_JPN){
                                    $msg = $obj->itemData[30]['strip_tags']."は、15人までしか登録できません。";
                                }
                                else{
                                    $msg = "The submission". $obj->itemData[30]['strip_tags']." can involve up to 15 coauthors.";
                                }
                                $obj->objErr->addErr($msg, "edata30");
                            }
                        }

                    }

                    //共著者　所属機関
                    if($obj->itemData[32]["disp"] != "1"){
                        if($obj->arrParam["edata31"] == ""){
                            $msg = sprintf(ERR_REQUIRE_INPUT_PARAM, $obj->itemData[31]['strip_tags']);
                            $obj->objErr->addErr($msg, "edata31");
                        }
                    }
                }
            }
        }
    }


    //--------------------------------
    //2ページ目
    //--------------------------------
    function _check2($obj){
        // 管理者はしない
        if($obj->admin_flg == "1") return;

        // 基本チェック
        $obj->objErr->check($obj->_init(2), $obj->arrParam);

        $group2 = ($obj->o_form->formData['group2'] == "")
                ? "共著者"
                : $obj->o_form->formData['group2'];

        $form1data = $GLOBALS["session"]->getVar("form_param1");
        if(!isset($form1data["edata30"])) $form1data["edata30"] = "0";


        //共著者の数
        for($i=0; $i < $obj->arrForm["edata30"]; $i++){

            //----------------------------
            //共著者所属機関のチェック
            //----------------------------
            if($obj->itemData[32]["need"] == "1"){

                $ok =0;
                $wa_kikanlist = $obj->_makeListBox($form1data["edata31"]);

                foreach($wa_kikanlist as $key => $val){
                    $wk_val = isset($obj->arrParam["edata32".$i.$key]) ? $obj->arrParam["edata32".$i.$key] : "";
                    if($wk_val != "")$ok++;
                }
                if($ok == 0){
                    $msg = sprintf(ERR_REQUIRE_SELECT_PARAM, $group2.($i + 1)."　".$obj->itemData[32]['strip_tags']);
                    $obj->objErr->addErr($msg, "edata32".$i.$key);
                }
            }

            //任意項目
            $obj->_checkChosyaNini($i);
        }
    }


    //--------------------------------
    //3ページ目
    //--------------------------------
    function _check3($obj){
        $group_id = 3;

        //ファイルアップロードチェック
        foreach($obj->arrfile as $item_id => $n){
            $obj->_checkfile($obj->arrParam, $group_id, $item_id, "1");
        }

        // 管理者はしない
        if($obj->admin_flg == "1") return;

        $obj->objErr->check($obj->_init($group_id), $obj->arrParam);
        $obj->_checkNini($group_id);
    }


    //----------------------------------
    //決済ページ
    //----------------------------------
    function _checkpay($obj){
         // 管理者はしない
         if($obj->admin_flg == "1") return;

         // 編集時はチェックしない
         if($obj->eid != "") return;

        // 基本チェック
        $obj->objErr->check($obj->_init($obj->wk_block), $obj->arrParam);

        $page1 = $GLOBALS["session"]->getVar("form_param1");
        if($obj->eid == ""){
            $ttotal_price = Usr_function::_setTotal($obj->wa_price, $page1, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        }
        else{
            $ttotal_price = $GLOBALS["session"]->getVar("ss_total_payment");
        }


        // 合計金額が0円
        if($ttotal_price == "0"){
            unset($obj->objErr->_err['card_type']);
            unset($obj->objErr->_err['cardNumber']);
            unset($obj->objErr->_err['cardExpire1']);
            unset($obj->objErr->_err['cardExpire2']);
            unset($obj->objErr->_err['cardHolder']);
            return;
        }

        // クレジット決済でない場合
        if($obj->ss_method != "1"){
            return;
        }

        // クレジットカードに関する同意をしていない場合
        if(isset($obj->arrParam['credit_agree']) && in_array($obj->arrParam['credit_agree'], array("0","1")) && $obj->arrParam['credit_agree'] != "1"){
            /**
             * 同意するチェックボックスはその他のエラーが発生してもチェック状態は保持しない
             * */
            $obj->objErr->addErr($GLOBALS["msg"]["credit"]["agree"], "credit_agree");
        }

        //カード番号チェック
        if($obj->arrParam["card_type"] != "" && $obj->arrParam["cardNumber"] != ""){

            if($obj->formdata["lang"] == LANG_JPN){
                $card_error = "ご入力いただいたカード情報に誤りがあります。入力内容をご確認ください。";
            }
            else{
                $card_error = "The credit card information you entered is invalid. Please check information entered (number, valid until date, etc.).";
            }

            switch($obj->arrParam["card_type"]){
                //VISA
                case 1:
                    if(strlen($obj->arrParam["cardNumber"]) != 16){
                        $obj->objErr->addErr($card_error, "cardNumber");
                    }
                    else{
                        if(substr($obj->arrParam["cardNumber"],0,1) != "4"){
                            $obj->objErr->addErr($card_error, "cardNumber");
                        }
                    }

                break;

                //MasterCard
                case 2:
                    if(strlen($obj->arrParam["cardNumber"]) != 16){
                        $obj->objErr->addErr($card_error, "cardNumber");
                    }
                    else{
                        if(substr($obj->arrParam["cardNumber"],0,1) != "5"){
                            $obj->objErr->addErr($card_error, "cardNumber");
                        }
                    }

                break;

                //Diners Club
                case 3:
                    if(strlen($obj->arrParam["cardNumber"]) != 14){
                        $obj->objErr->addErr($card_error, "cardNumber");
                    }
                    else{
                        if(substr($obj->arrParam["cardNumber"],0,2) != "36"){
                            $obj->objErr->addErr($card_error, "cardNumber");
                        }
                    }

                break;

                //AMEX
                case 4:
                    if(strlen($obj->arrParam["cardNumber"]) != 15){
                        $obj->objErr->addErr($card_error, "cardNumber");
                    }
                    else{
                        if(substr($obj->arrParam["cardNumber"],0,2) != "34" && substr($obj->arrParam["cardNumber"],0,2) != "37"){
                            $obj->objErr->addErr($card_error, "cardNumber");
                        }
                    }


                break;

                //JCB
                case 5:
                    if(strlen($obj->arrParam["cardNumber"]) != 16){
                        $obj->objErr->addErr($card_error, "cardNumber");
                    }
                    else{
                        if(substr($obj->arrParam["cardNumber"],0,2) != "35"){
                            $obj->objErr->addErr($card_error, "cardNumber");
                        }
                    }

                break;
            }
        }

        // セキュリティコード
        if(isset($obj->arrParam["securityCode"]) && !$obj->objErr->isNumeric($obj->arrParam["securityCode"])){
            $name = ($obj->formdata["lang"] == LANG_JPN) ? "セキュリティコード" : "Security Code";
            $obj->objErr->addErr(sprintf($GLOBALS["msg"]["err_han_numeric"], $name), "securityCode");
        }

    }


    /**
     * 任意項目のチェック
     *
     */
    function _checkNini($obj, $group){
        if(!count($obj->option_item[$group]) > 0) return;

        foreach($obj->option_item[$group] as $i) {
            
            if($obj->itemData[$i]["disp"] != "1" && $obj->itemData[$i]["need"] == "1"){
                //チェックボックスの時
                if($obj->itemData[$i]["item_type"] == "3"){
                    $ok =0;
                    foreach($obj->itemData[$i]["select"] as $key => $val){
                        $wk_val = isset($obj->arrParam["edata".$i.$key]) ? $obj->arrParam["edata".$i.$key]: "";
                        if($wk_val != "") $ok++;
                    }

                    if($ok == 0){
                        $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[$i]['strip_tags']);

                        if($obj->formdata["lang"] == "2"){
                            $msg = sprintf($GLOBALS["msg"]["err_require_check"], $obj->itemData[$i]['strip_tags']);
                        }
                        $obj->objErr->addErr($msg, "edata".$i);
                    }
                }
            }
            
        }
        
        return;

    }

    /**
     * 共著者　任意項目のチェック
     */
    function _checkChosyaNini($obj, $i){
        $group2 = ($obj->o_form->formData['group2'] == "")
                ? "共著者"
                : $obj->o_form->formData['group2'];

        $num = $i+1;
                
        foreach($obj->option_item[2] as $item_id) {
            
            if($obj->itemData[$item_id]["disp"] != "1" && $obj->itemData[$item_id]["need"] == "1") {
                
                // チェックボックスじゃなければ次へ
                if($obj->itemData[$item_id]["item_type"] != "3") continue;
                
                $ok =0;
                
                foreach($obj->itemData[$item_id]["select"] as $key => $val){
                    $wk_val = isset($obj->arrParam["edata".$item_id.$i.$key]) ? $obj->arrParam["edata".$item_id.$i.$key] : "";
                    if($wk_val != "") $ok++;
                }

                if($ok == 0){
                    $msg = sprintf($GLOBALS["msg"]["err_require_select"], $obj->itemData[$item_id]['strip_tags']."(".$num.")");
                    $obj->objErr->addErr($msg, "edata".$item_id.$i);
                }
            }
            
        }
        
        return;

    }

    /**
     * メールアドレス、氏名重複チェック
     *
     */
    function checkRepeat($obj, $pa_formparam){
        if($pa_formparam["edata25"] == "" || $pa_formparam["edata1"] == "" || $pa_formparam["edata2"] == ""){
            return true;
        }

        $column = "*";
        $from = "entory_r";
        $where[] = "form_id = ".$obj->db->quote($obj->o_form->formData["form_id"]);
        $where[] = "del_flg = 0";
        // 無効なエントリーは考慮しない
        $where[] = "invalid_flg = 0";

        //メールアドレス
        $where[] = "edata25 ilike ".$obj->db->quote($pa_formparam["edata25"]);


        $where[] = "edata1 ilike ".$obj->db->quote($pa_formparam["edata1"]);
        $where[] = "edata2 ilike ".$obj->db->quote($pa_formparam["edata2"]);



        //更新の場合
        if($obj->eid != ""){
            $where[] = "eid <> ".$obj->db->quote($obj->eid);
        }


        $rs = $obj->db->getData($column, $from, $where, __FILE__, __LINE__);
        if(!$rs){
            return true;

        }

        return false;




    }

    /**
     * アップロードファイルチェック
     *
     * @param int 項目開始番号
     * @param int 項目終了番号
     * @param string mode 「戻る」ボタン以外は「1」
     */
    function _checkfile($obj, $pa_formparam, $group_id, $item_id, $mode=""){
        //一般ユーザモードの場合にチェック
        if($obj->admin_flg != "") return;

        // 1ページ目の入力情報
        $arrForm = $GLOBALS["session"]->getVar("form_param1");

        // 非表示はチェックしない
        if($obj->arrItemData[$group_id][$item_id]["disp"] == "1") return;

        $key = "edata".$item_id;

        // 必須チェック
        if($mode == "1" && ($obj->arrItemData[$group_id][$item_id]["need"] == "1" || ($item_id == "51" && in_array($arrForm["amount"], $obj->filecheck)))){
            // 未指定
            if($pa_formparam["hd_file".$item_id] == "" && $_FILES[$key]["size"] == 0){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? $obj->arrItemData[$group_id][$item_id]['strip_tags']."を指定してください。"
                     : "Specify ".$obj->arrItemData[$group_id][$item_id]['strip_tags'].".";
                $obj->objErr->addErr($msg, $key);
            }

            // 必須項目を削除し新しいファイルを未指定
            if($_FILES[$key]["size"] == 0 && isset($pa_formparam["file_del".$item_id])
            && $pa_formparam["file_del".$item_id] == 1
            && (
                   $pa_formparam["hd_file".$item_id] == $pa_formparam["n_data".$item_id]
                || $item_id == "51" && in_array($arrForm["amount"], $obj->filecheck)
               )
            ){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? $obj->arrItemData[$group_id][$item_id]['strip_tags']."は必須です。削除する場合は、別のファイルを指定してください。"
                     : $obj->arrItemData[$group_id][$item_id]['strip_tags']." must be included. Select another file for deletion.";
                $obj->objErr->addErr($msg, $key);
            }
        }

        //サイズチェックを行う場合
        $pos = strstr($obj->arrItemData[$group_id][$item_id]["item_check"],"1");
        if($mode == "" && $pos !== false){
            if($obj->arrItemData[$group_id][$item_id]["item_len"] < $_FILES[$key]["size"]){
                if($obj->formdata["lang"] == LANG_JPN){
                    $msg = $obj->arrItemData[$group_id][$item_id]['strip_tags']."が".$obj->arrItemData[$group_id][$item_id]["item_len"]."byteを超えています。";
                }
                else{
                    $msg = $obj->arrItemData[$group_id][$item_id]['strip_tags']." cannot exceed ".$obj->arrItemData[$group_id][$item_id]["item_len"]." byte.";
                }


                $obj->objErr->addErr($msg, $key);
            }
        }

        // 拡張子チェック
        if($mode == "1" && strlen($obj->arrItemData[$group_id][$item_id]['item_select']) > 0){
            $arrExt = Usr_function::gf_to_array_unique_trim($obj->arrItemData[$group_id][$item_id]['item_select']);
            if(!Validate::checkExt($_FILES[$key], $arrExt)){
                $strExt = implode(", ", $arrExt);

                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? $obj->arrItemData[$group_id][$item_id]['strip_tags']."の拡張子は「".$strExt."」でアップロードしてください。"
                     : "Please upload ". $obj->arrItemData[$group_id][$item_id]['strip_tags']." in ". preg_replace("/,(.*?)$/u", " or $1", $strExt) ." file."
                     ;

                $obj->objErr->addErr($msg, $key);
            }
        }

        // 以下アップロード1のみのチェック
        if($item_id != 51) return;

        // アップロード必須でない(=非表示) かつ 拡張性制限あり
        // エラーを破棄する
        if(!in_array($arrForm["amount"], $obj->filecheck) && isset($obj->objErr->_err["edata51"])){
            unset($obj->objErr->_err["edata51"]);
        }

    }


    /**
     * 外部ファイルが存在するかチェック
     *     拡張子がPHPの場合はファイルをinclude_onceする
     *
     * @param stirng ディレクトリ
     * @param int form_id
     * @param string ファイル名
     * @param string 拡張子
     */
    function _chkIncFiles($obj, $ps_dir, $pn_form_id, $ps_file_name, $ps_extension){

        //探したいファイルの名前
        $find_file = $pn_form_id.$ps_file_name.".".$ps_extension;

        if (is_dir($ps_dir)) {
            if ($dh = opendir($ps_dir)) {
                while (($file = readdir($dh)) !== false) {
                    //外部読み込みファイルが見つかった場合
                    if($file == $find_file){
                        //PHPファイルの場合
                        if($ps_extension == "php"){
                            include_once($ps_dir.$find_file);
                        }

                        return true;
                    }
                }
                closedir($dh);
            }
        }

        return false;
    }


    /**
     * 対象の項目を選択しているか判定
     *  arrItemData[group_id][item_id][select]
     * 
     * @param  integer $group_id
     * @param  integer $item_id
     * @param  string  $target
     * @return boolean true:選択している
     * 
     */
    function chkSelect($obj, $group_id, $item_id, $target=""){
        if($obj->arrItemData[$group_id][$item_id]['disp'] != '') return false;

        $val = array_search($target, $obj->arrItemData[$group_id][$item_id]['select']);
        if($val === false) return false;

        $isChk = false;
        switch($obj->arrItemData[$group_id][$item_id]['item_type']){
            case Usr_init::INPUT_TEXT:
            case Usr_init::INPUT_AREA:
            case Usr_init::INPUT_RADIO:
                break;

            case Usr_init::INPUT_SELECT:
                $key = 'edata'.$item_id;
                $isChk = isset($obj->arrParam[$key]) && $obj->arrParam[$key] == $val;
                break;

            case Usr_init::INPUT_CHECK:
                $key = 'edata'.$item_id.$val;
                $isChk = isset($obj->arrParam[$key]) && $obj->arrParam[$key] == $val;
                break;

            default:
                break;
        }

        return $isChk;
    }

}

?>