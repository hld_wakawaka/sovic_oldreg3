<?php

class Usr_lock {

    function lockStart($obj) {
        //ロックファイル存在チェック
        $lock_file = ROOT_DIR."LockDir/lock".$obj->form_id.".txt";
        While(file_exists($lock_file)){
            //存在する場合はスリープ
            $GLOBALS["log"]->write_log("SLEEP", "sleep");
            if(file_exists(ROOT_DIR."LockDir/stop".$obj->form_id.".txt")){
                break;
            }
            sleep(5);
        }

        //ロックファイル生成
        error_log("lock start------".date("Y-m-d H:i:s")."|form_id => ".$obj->form_id."| lock_file =>".$lock_file, 3 ,$lock_file);
        if(chmod($lock_file, 0777)){
//            $GLOBALS["log"]->write_log("LOCK_START", "form_id => ".$obj->form_id."| date =>".date("Y-m-d H:i:s")."| lock_file =>".$lock_file);
        }
    }


    function lockEnd($obj) {
        $lock_file = ROOT_DIR."LockDir/lock".$obj->form_id.".txt";
        if(file_exists($lock_file)){
            if(unlink($lock_file)){
                $GLOBALS["log"]->write_log("LOCK_END", "form_id => ".$obj->form_id."| date =>".date("Y-m-d H:i:s")."| lock_file =>".$lock_file);
            }else{
                $GLOBALS["log"]->write_log("LOCK_END_FAIL", "form_id => ".$obj->form_id."| date =>".date("Y-m-d H:i:s")."| lock_file =>".$lock_file);
            }
        }else{
            $GLOBALS["log"]->write_log("LOCK_END_UNKNOWN", "form_id => ".$obj->form_id."| date =>".date("Y-m-d H:i:s")."| lock_file =>".$lock_file);
        }
    }

}


?>