<?php

/* MicrosoftWord生成の基底クラス
 * 
 */
class Mng_Word {

    private $template;  // テンプレート名
    private $tempDir;   // テンプレート保存先

    private $savefile;  // 生成ファイル名
    private $saveDir;   // 生成ファイル保存先

    private $arrForm;   // 差し込むデータ

    // テーブル
    public  $thead;     // ヘッダ
    private $splitrow;  // テーブル分割行
    private $insertbody;// 分割したテーブルの間に差し込む文書

    function __construct($temp=array(), $save=array()){
        // phpWordオブジェクトを読み込み
        require_once LIB_DIR.'PHPWord.php';

        if(is_array($temp) && count($temp) == 2) $this->setTemplate($temp[0], $temp[1]);

        if(is_array($save) && count($save) == 2) $this->setSavefile($save[0], $save[1]);
    }


    /**
     * テンプレート設定
     * @param  string $template テンプレートファイル名
     * @param  string $tempDir  テンプレート保存先
     * @return object Mng_Word
     *
     */
    public function setTemplate($template, $tempDir){
        $this->template = $template;
        $this->tempDir  = $tempDir;
        return $this;
    }


    /**
     * 生成ファイルの保存先の設定
     * @param  string $savefile 生成ファイル名
     * @param  string $saveDir  生成ファイル保存先
     * @return object Mng_Word
     *
     */
    public function setSavefile($savefile, $saveDir){
        $this->savefile = $savefile;
        $this->saveDir  = $saveDir;
        return $this;
    }


    /**
     * 差し込むデータの設定
     * @param  array $arrData
     * @return object Mng_Word
     */
    public function setData($arrData){
        $this->arrForm = $arrData;
        return $this;
    }


    /**
     * 差し込むデータの設定
     * @param  array $arrData
     * @return object Mng_Word
     */
    private function getData(){
        return $this->arrForm;
    }


    public function createWord($obj){
        if(!isset($obj->exClass)) $obj->exClass = NULL;

        $objWord = new PHPWord();
        $document = $objWord->loadTemplate($this->tempDir.$this->template);
        $obj->arrForm = $this->getData();

        // グループ1
        $this->makeDocumentGroup1($obj, $document);

        // グループ2
        $this->makeDocumentGroup2($obj, $document);

        // グループ3
        $this->makeDocumentGroup3($obj, $document);

        // 生成直前の前処理
        $methodname = 'presaveword';
        if(method_exists($obj->exClass, $methodname)){
            $obj->exClass->$methodname($obj, $document);
        }

        // 保存先ディレクトリ
        if(!is_dir($this->saveDir)){
            mkdir($this->saveDir, 0777, true);
            chmod($this->saveDir, 0777);
        }

        return $document->save($this->saveDir.$this->savefile);
    }

    public function makeDocumentGroupBase($obj, &$document, $group){
        $methodname = 'makeDocumentGroupBase';
        if(method_exists($obj->exClass, $methodname)){
           return $obj->exClass->$methodname($obj, $document);
        }

        foreach($obj->arrItemData[$group] as $_key => $_data){
            if($_data["disp"] == "1") continue;

            $item_id = $_data["item_id"];
            $key     = 'edata'.$item_id;
            if(!isset($obj->arrForm[$key])) $obj->arrForm[$key] = "";

            $methodname = "wordfunc".$item_id;
            // カスタマイズあり
            if(method_exists($obj->exClass, $methodname)) {
                $value = $obj->exClass->$methodname($obj, $group, $item_id);

            // カスタマイズなし
            }else{
                // 任意
                if($_data["controltype"] == "1") {
                    $value = Usr_Assign::nini($obj, $group, $item_id, $obj->arrForm[$key]);
                // 標準
                }else{
                    $value = method_exists("Usr_Assign", $key)
                           ? Usr_Assign::$key($obj)
                           : $obj->arrData[$key];
                }
            }
            $value = htmlspecialchars($value);
            $document->setValue($key, $value);
        }
    }

    public function makeDocumentGroup1($obj, &$document){
        if(method_exists($obj->exClass, __FUNCTION__)){
           return $obj->exClass->makeDocumentGroup1($this, $obj, $document);
        }
        $this->makeDocumentGroupBase($obj, $document, 1);
    }


    public function makeDocumentGroup2($obj, &$document){
    }


    public function makeDocumentGroup3($obj, &$document){
        if(method_exists($obj->exClass, __FUNCTION__)){
           return $obj->exClass->makeDocumentGroup3($this, $obj, $document);
        }
        $this->makeDocumentGroupBase($obj, $document, 3);
    }


    /**
     * @param  integer $row 行数
     * @param  integer $col 列数
     * 
     */
    function createTable($matrix, $arrwidth, $arralign, $m=0, $splitrow=-1, $insertbody=""){
        $table = "<w:tbl>_THEAD__TBODY_</w:tbl>";
        $thead = $this->createThead($matrix, $arrwidth);
        $tbody = $this->createTbody($matrix, $arralign, $m, $splitrow, $insertbody);
        $table = str_replace('_THEAD_', $thead, $table);
        $table = str_replace('_TBODY_', $tbody, $table);
        return $table;
    }

    function createThead($matrix, $arrwidth){
        extract($matrix);
        if(count($arrwidth) != $col) return NULL;

        $thead = array();
        $thead[] = '
            <w:tblPr>
                <w:jc w:val="center"/>
                <w:tblW w:w="'.array_sum($arrwidth).'" />
                <w:tblLayout w:type="fixed"/>
                <w:tblCellMar>
                <w:left w:w="0" w:type="dxa"/>
                <w:right w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPr>
            <w:tblGrid>';

        for($i=0; $i<$col; $i++){
            $thead[] = '<w:gridCol w:w="'.$arrwidth[$i].'"/>';
        }

        $thead[] = '</w:tblGrid>';


        $border = '<w:tcBorders><w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="8" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/></w:tcBorders>';
        $margin = '<w:tcMar><w:top w:w="0" w:type="dxa"/><w:left w:w="0" w:type="dxa"/><w:bottom w:w="0" w:type="dxa"/><w:right w:w="0" w:type="dxa"/></w:tcMar>';
        $font   = '<w:rPr><w:rFonts w:ascii="Century" w:hAnsi="Century" w:cs="Century" w:hint="eastAsia"/><w:lang w:eastAsia="ja-JP"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr>';
        $attr   = '<w:pPr><w:spacing w:after="0" w:line="200" w:lineRule="exact"/><w:jc w:val="center"/></w:pPr>';
        $tr = '<w:tc><w:tcPr>'.$border.$margin.'<w:vAlign w:val="center"/></w:tcPr><w:p>'.$attr.'<w:r>'.$font.'<w:t>%s</w:t></w:r></w:p></w:tc>';

        // ヘッダ # th
        $thead[] = '<w:tr w:rsidR="007B1CDB" w:rsidTr="00E50D13">';
        $thead[] = '<w:trPr><w:jc w:val="center"/><w:trHeight w:hRule="exact" w:val="500"/></w:trPr>';
        for($j=0; $j<$col; $j++){
            $thead[] = sprintf($tr, "_HEAD_{$j}_");
        }
        $thead[] = '</w:tr>';

        $this->thead = $thead;

        return implode('', $thead);
    }

    function createTbody($matrix, $arralign, $m, $splitrow, $insertbody){
        extract($matrix);

        $tbody = '';

        $border = '<w:tcBorders><w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="8" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/></w:tcBorders>';
        $margin = '<w:tcMar><w:top w:w="'.$m.'" w:type="dxa"/><w:left w:w="'.$m.'" w:type="dxa"/><w:bottom w:w="'.$m.'" w:type="dxa"/><w:right w:w="'.$m.'" w:type="dxa"/></w:tcMar>';
        $font   = '<w:rPr><w:rFonts w:ascii="Century" w:hAnsi="Century" w:cs="Century" w:hint="eastAsia"/><w:lang w:eastAsia="ja-JP"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr>';
        $align  = '<w:pPr><w:jc w:val="%s"/></w:pPr>';
        $tr = '<w:tc><w:tcPr>'.$border.$margin.'</w:tcPr><w:p>'.$align.'<w:r>'.$font.'<w:t>%s</w:t></w:r></w:p></w:tc>';

        // データ
        for($i=0; $i<$row; $i++){
            if($splitrow > 0 && $i == $splitrow){
                $insertbody = str_replace('[THEAD]', implode('', $this->thead), $insertbody);
                $tbody .= $insertbody;
            }
            $tbody .= '<w:tr w:rsidR="007B1CDB" w:rsidTr="00E50D13">';
            $tbody .= '<w:trPr><w:trHeight w:hRule="exact" w:val="auto"/></w:trPr>';
            for($j=0; $j<$col; $j++){
                $tbody .= sprintf($tr, $arralign[$j], "_CELL_{$i}_{$j}_");
            }
            $tbody .= '</w:tr>';
        }
        return $tbody;
    }

}

?>
