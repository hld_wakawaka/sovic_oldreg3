<?php

class Usr_pageAction {

    /** 初期表示 */
    function defaultAction($obj) {
        $obj->block = "1";

        //セッション情報クリア
        $GLOBALS["session"]->unsetVar("form_param1");
        $GLOBALS["session"]->unsetVar("form_param2");
        $GLOBALS["session"]->unsetVar("form_param3");
        $GLOBALS["session"]->unsetVar("form_parampay");
        $GLOBALS["session"]->unsetVar("kyouID");           //共著者ID
        $GLOBALS["session"]->unsetVar("SS_USER");          //応募者ログイン情報
        $GLOBALS["session"]->unsetVar("workDir");
        $GLOBALS["session"]->unsetVar("ss_order_no");      //登録No.
        $GLOBALS["session"]->unsetVar("ss_total_payment"); //支払合計金額
        $GLOBALS["session"]->unsetVar("order_id");

        // 編集
        if($obj->eid != "") {
            // エントリーデータ取得
            $obj->getEntry();
            // 編集前の情報を保持
            if($obj->save_editbefore){
                // DBに保存する用のパラメータを生成
                $arrForm = Usr_entryDB::makeDbParam($obj);
                $filekeys = array_keys($obj->arrfile);   // ファイルアップロード
                foreach($filekeys as $_key => $item_id){
                    $key = 'edata'.$item_id;
                    $arrForm[$key] = $obj->arrForm[$key];
                }

                $arrData = array();
                $group   = array(1,2,3);
                foreach($group as $_key => $group_id){
                    foreach($obj->arrItemData[$group_id] as $_key2 => $arrItemData){
                        if($arrItemData['disp'] != '') continue;

                        $key = 'edata'.$arrItemData['item_id'];
                        $arrData[$key] = $arrForm[$key];
                    }
                }
                $GLOBALS["session"]->setVar("form_editbefore", $arrData);
            }

        // 新規登録
        }else{
            if($obj->formdata["kessai_flg"] == "1"){
                $GLOBALS["session"]->setVar("form_parampay", $obj->arrForm);
            }

            // NEWにディレクトリを作成し、セッションに格納する
            $obj->createWorkDir();

            //---------------------------------
            //プライバシーポリシー同意画面
            //---------------------------------
            if($obj->formdata["agree"] == "3" || $obj->formdata["agree"] == "4"){
                //別画面で同意画面を表示する
                if($obj->formdata["lang"] == LANG_JPN){
                    $obj->_title = $obj->formdata["form_name"]." 同意書";
                }else{
                    $obj->_title = $obj->formdata["form_name"]." Privacy Policy";
                }
                // 共通テンプレート
                $obj->_processTemplate = "Usr/form/Usr_entry_agree.html";
            }

            //初期値
            if($obj->formdata["agree"] != "0"){
                $obj->arrForm["agree"] = "";
            }

            if($GLOBALS["session"]->getVar("startmsg")) {
                $obj->arrErr = $GLOBALS["session"]->getVar("startmsg");
                $GLOBALS["session"]->unsetVar("startmsg");
            }
        }
    }

    /** 初期表示 */
    function displayAction($obj) {
        Usr_pageAction::defaultAction($obj);

        $obj->_processTemplate = "Usr/form/Usr_entry_display.html";
    }


    /** ページ遷移ベース */
    function pageAction($obj) {
        // フォームパラメータ
        $getFormData= GeneralFnc::convertParam($obj->_init($obj->wk_block), $_REQUEST);

        // 共著者の所属機関
        $ret_param = $obj->_make35param($obj->wk_block);
        if(count($ret_param) > 0){
            $getFormData = array_merge($getFormData, $ret_param);
        }

        // チェック用の変数に入れる
        $obj->arrParam = $getFormData;

        // エラーチェック
        $obj->arrErr = $obj->_check();

        //セッションにページパラメータをセットする
        $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $obj->arrParam);

        // セッションパラメータ取得
        $obj->getDispSession();

        // 合計金額#参加登録で決済利用
        if($obj->formdata['type'] == 3 && $obj->formdata['credit'] != 0){
            $obj->total_price = ($obj->eid == "")
                              ? Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price)
                              : $GLOBALS["session"]->getVar("ss_total_payment");
            $obj->assign("total_price", $obj->total_price);
        }

        // エラーがある場合
        if(count($obj->arrErr) > 0){
            //エラーを自ページに表示
            $obj->block = $obj->wk_block;

            //決済ページの場合は決済画面にメッセージを表示する
            if($obj->wk_block == "pay"){
                $obj->_processTemplate =  "Usr/form/Usr_payment.html";
            }
            return;
        }

        $blockaction = "pageAction".$obj->wk_block;
        return $obj->$blockaction();
    }



    /** 1ページ目 */
    function pageAction1($obj) {
        $fix_flg = "";

        //3ページ目を使用
        if($obj->formdata["group3_use"] != "1"){
            $obj->block = "3";
            $fix_flg = "1";
            $obj->_processTemplate = "Usr/form/Usr_entry.html";
        }

        // 決済フォームを利用 かつ 決済金額が0円より多い場合
        if($obj->formdata["credit"] != "0" && $obj->total_price > 0){
            if(!in_array($obj->form_id, $obj->payment_not)){
                $obj->block = "pay";

                $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                $fix_flg = "1";
            }

        //決済を使用しない場合
        }else{
            //2ページ目使用
            if($obj->formdata["group2_use"] != "1"){
                $obj->block = "2";

                //共著者なしの場合は3ページ目を表示
                if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                    if($obj->formdata["group3_use"] != "1"){
                        $obj->block = "3";
                        $obj->_processTemplate =  "Usr/form/Usr_entry.html";

                    }else{
                        $obj->block = "4";
                        $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
                    }
                }
                $fix_flg = "1";
            }
        }

        //上記のいずれも該当しなかった場合
        if($fix_flg == ""){
            $obj->block = "4";
            $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";
        }
    }



    /** 2ページ目 */
    function pageAction2($obj) {
        $fix_flg = "";

        //3ページ目を使用
        if($obj->formdata["group3_use"] != "1"){
            $obj->block = "3";
            $obj->_processTemplate =  "Usr/form/Usr_entry.html";

            $fix_flg = "1";
        }

        //決済フォームを利用する場合
        if($obj->formdata["credit"] != "0"){
            $obj->block = "pay";
            $obj->_processTemplate =  "Usr/form/Usr_payment.html";
            $fix_flg = "1";

        }


        //上記のいずれも該当しなかった場合
        if($fix_flg == ""){
            $obj->block = "4";
            $obj->_processTemplate =  "Usr/form/Usr_entry_confirm.html";

        }
    }



    /** 決済ページ */
    function pageActionpay($obj) {
        //3ページ目を使用
        if($obj->formdata["group3_use"] != "1"){
            $obj->block = "3";
            $obj->_processTemplate =  "Usr/form/Usr_entry.html";

        }else{
            //確認画面へ
            $obj->_confirm($obj->wk_block);
        }
    }



    /** プライバシーポリシー */
    function agreeAction($obj) {
        //同意が必要な場合
        if(in_array($obj->formdata["agree"], array(3, 4))){
            if($_REQUEST["agree"] == ""){
                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? "以下をご覧になり、同意の上、お進みください。"
                     : "Please read the agreement and proceed further only if you agree.";

                $obj->objErr->addErr($msg, "agree");
                $obj->arrErr = $obj->objErr->_err;

                $obj->_processTemplate = "Usr/form/Usr_entry_agree.html";
                $obj->_title = ($obj->formdata["lang"] == LANG_JPN)
                             ? $obj->formdata["form_name"]." 同意書"
                             : $obj->formdata["form_name"]." Privacy Policy";
            }
        }
        $obj->block = "1";
    }



    /** 3ページ目 */
    function pageAction3($obj) {
        $obj->_confirm();
    }



    /** 確認ページ */
    function confirmAction($obj) {
        $obj->_confirm();
    }



    /** 戻るボタン */
    function backAction($obj) {
        // 確認画面以外から戻るとき
        if($obj->wk_block != "4"){
            $getFormData= GeneralFnc::convertParam($obj->_init($obj->wk_block), $_REQUEST);

            // 共著者の所属機関
            $ret_param = $obj->_make35param($obj->wk_block);
            if(count($ret_param) > 0){
                $getFormData = array_merge($getFormData, $ret_param);
            }

            // ファイル引き継ぎ
            foreach($obj->arrfile as $item_id => $n){
                $key = "n_data".$item_id;
                $getFormData[$key] = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
            }

            // セッションにページパラメータをセットする
            $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $getFormData);
        }

        // セッションパラメータ取得
        $obj->getDispSession();


        // 合計金額#参加登録で決済利用
        if($obj->formdata["kessai_flg"] == "1"){
            $obj->total_price = ($obj->eid == "")
                              ? Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price)
                              : $GLOBALS["session"]->getVar("ss_total_payment");
            $obj->assign("total_price", $obj->total_price);
        }

        switch($obj->wk_block) {
            case "1":
            case "2":
                $obj->block = "1";
                break;

            // 3ページ目からの戻り
            case "3";
                $obj->block = "1";

                // ファイルアップロードチェック
                foreach($obj->arrfile as $item_id => $n){
                    $obj->_checkfile($getFormData, 3, $item_id);
                }

                if(count($obj->objErr->_err) > 0){
                    //エラーを自ページに表示
                    $obj->block = $obj->wk_block;

                }else{
                    // 添付資料アップロード
                    foreach($obj->arrfile as $item_id => $n){
                        if($obj->itemData[$item_id]["disp"] != "1"){
                            list($wb_ret, $msg) = $obj->fileTmp($_FILES["edata".$item_id], $item_id);
                            if(!$wb_ret){
                                $obj->arrErr["file_error".$item_id] = $msg;
                            }
                            $getFormData["hd_file".$item_id] = isset($obj->arrForm["hd_file".$item_id]) ? $obj->arrForm["hd_file".$item_id] : "";
                        }
                    }

                    // 2ページ目を使用
                    if($obj->formdata["group2_use"] != "1"){
                        $obj->block = "2";

                        // 共著者なしの場合は3ページ目を表示
                        if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                        || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                            $obj->block = "1";
                        }
                    }

                    // 決済を使用する かつ 決済金額が0円より多い場合は決済画面に遷移する
                    if($obj->formdata["credit"] != "0" && $obj->total_price > 0){
                        if(!in_array($obj->form_id, $obj->payment_not)){
                            $obj->block = "pay";
                            $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                        }
                    }
                }
                break;


            // 決済画面から戻る場合
            case "pay":
                $obj->block = "1";

                // 2ページ目を使用
                if($obj->formdata["group2_use"] != "1"){
                    $obj->block = "2";

                    // 共著者なしの場合は3ページ目を表示
                    if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                    && $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                        $obj->block = "1";
                    }
                }
                break;


            // 確認画面から戻ってきた場合
            case "4":
                $obj->block = "1";

                // 2ページ目を使用
                if($obj->formdata["group2_use"] != "1"){
                    $obj->block = "2";

                    // 2ページ目を使用するが、共著者無しの場合
                    if(!isset($obj->arrForm["edata29"])|| !isset($obj->arrForm["edata30"])
                    || $obj->arrForm["edata29"] == "2" || $obj->arrForm["edata30"] == ""){
                        $obj->block = "1";
                    }
                }

                // 決済フォームを利用する場合
                if(!in_array($obj->form_id, $obj->payment_not)){
                    if($obj->formdata["credit"] != "0" && $obj->total_price > 0){
                        //決済画面に遷移する
                        $obj->block = "pay";
                        $obj->_processTemplate =  "Usr/form/Usr_payment.html";
                    }
                }

                // 3ページ目を使用
                if($obj->formdata["group3_use"] != "1"){
                    $obj->block = "3";
                    $obj->_processTemplate = "Usr/form/Usr_entry.html";
                }
                break;
        }

        if(isset($obj->arrForm["edata31"])) {
            $wa_kikanlist = $obj->_makeListBox($obj->arrForm["edata31"]);
            $obj->arrForm["kikanlist"] = $wa_kikanlist;
        }
    }



    /** ダウンロードボタン */
    function downloadAction($obj) {
        // フォームパラメータ
        $getFormData= GeneralFnc::convertParam($obj->_init($obj->wk_block), $_REQUEST);

        // セッションにページパラメータをセットする
        $GLOBALS["session"]->setVar("form_param".$obj->wk_block, $getFormData);

        // セッションパラメータ取得
        $obj->getDispSession();

        $obj->block = $obj->wk_block;

        if($_REQUEST["down_num"] != ""){
            $down_num = $_REQUEST["down_num"];

            // ダウンロードするファイル
            $down_file = $obj->uploadDir.$obj->eid."/".$_REQUEST["n_data".$down_num];

            if($_REQUEST["down_type"] == "2"){
                $tmp_dir   = $GLOBALS["session"]->getVar("workDir");
                $down_file = $tmp_dir."/".$_REQUEST["hd_file".$down_num];
            }

            if(is_file($down_file)){
                // ダウンロード実行
                $wo_download = new Download();
                $wo_download->file($down_file, $_REQUEST["hd_file".$down_num] , "");
                exit;
            }
            $obj->objErr->addErr("対象ファイルが存在しません。", "edata".$down_num);
            $obj->arrErr = $obj->objErr->_err;
        }
    }



    /**決済エラーからの戻り */
    function authErrAction($obj) {
        // セッションパラメータ取得
        $obj->getDispSession();

        $obj->block = $obj->wk_block;

        // 金額の合計
        $ttotal_price = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
        $obj->assign("total_price", $ttotal_price);

        if($obj->wk_block == "pay"){
            $obj->_processTemplate =  "Usr/form/Usr_payment.html";
        }
    }



    /*
     * 決済エラーの場合、確認画面に戻る
     */
    function paymentErrAction($obj) {
        // セッションパラメータ取得
        $obj->getDispSession();

        $obj->arrErr   = array();
        $obj->arrErr[] = "決済エラーが発生しました。決済情報を確認の上修正して下さい。";
        $obj->arrErr[] = $obj->msg;

        // 確認画面
        $obj->block = "4";

        // テンプレート
        $obj->_processTemplate = "Usr/form/Usr_entry_confirm.html";

        // お支払合計金額の計算
        $obj->arrForm["total_price"] = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
    }



    /** 完了ページ */
    function completeAction($obj) {
        // 二重登録チェック
        $rl = new Reload();
        if($rl->isReload()) {
            $msg = $GLOBALS["msg"]["reload"];
            $obj->complete($msg);
            return;
        }

        // セッションパラメータ取得
        $obj->arrForm = array_merge((array)$GLOBALS["session"]->getVar("form_param1"), (array)$GLOBALS["session"]->getVar("form_param2"));
        if(empty($obj->arrForm)){
            Error::showErrorPage("*セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
        }

        $obj->arrForm = array_merge($obj->arrForm, (array)$GLOBALS["session"]->getVar("form_param3"));
        if(empty($obj->arrForm)){
            Error::showErrorPage("※セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
        }

        $obj->arrForm = array_merge($obj->arrForm, (array)$GLOBALS["session"]->getVar("form_parampay"));
        if(empty($obj->arrForm)){
            Error::showErrorPage("セッションタイムアウトになりました。お手数ですが、初めから登録を行ってください。");
        }

        $obj->u_date = date('YmdHis');


        //トランザクション
        $obj->db->begin();

        // 二重登録チェック
        if(!Usr_Check::checkRepeat($obj, $obj->arrForm)){
            $msg = ($obj->formdata["lang"] == LANG_JPN)
                 ? "既にご登録済みです。"
                 :"You are already registered.";
            $obj->arrErr["edata25"] = $msg;
            $obj->_processTemplate = "Usr/form/Usr_entry.html";
            return;
        }

        //------------------------------------
        //新規登録の場合
        //------------------------------------
        if($obj->eid == ""){

            $exec_type = "1";

            // ロック
            $obj->lockStart();

            // 合計金額#参加登録で決済利用
            if($obj->formdata["kessai_flg"] == "1"){
                $obj->total_price = Usr_function::_setTotal($obj->wa_price, $obj->arrForm, $obj->formdata, $obj->o_form->formData, $obj->wa_ather_price);
            }

            //---------------------------------
            // 支払方法別処理
            //---------------------------------
            $actionName = "payAction".$obj->arrForm["method"];
            $obj->$actionName();

            //メール送信
            $obj->sendRegistMail($obj->user_id, $obj->passwd);

            //完了メッセージ
            $obj->msg = $obj->replaceMsg($obj->formWord["word1"]);

            $obj->lockEnd();


        //------------------------------------
        // 更新の場合
        //------------------------------------
        }else{
            $exec_type = "2";

            //ファイルアップロード
            if(!$obj->TempUploadAction()){
                $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
                $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
                $obj->db->rollback();
                $obj->lockEnd();
                $obj->complete($msg);
                return;
            }

            //応募者のログイン情報をセッションから復元
            $ss_user = $GLOBALS["session"]->getVar("SS_USER");
            $user_id = $ss_user["e_user_id"];
            $passwd  = $ss_user["e_user_passwd"];

            // 更新処理
            $obj->updateEntry();

            // ファイルの移動
            if(!$obj->MoveFileAction()) {
                $obj->db->rollback();
                $obj->complete("ファイルの移動に失敗しました。");
                return;
            }

            //メール送信
            $obj->sendEditMail($user_id, $passwd);

            //完了メッセージ
            $obj->msg = $obj->replaceMsg($obj->formWord["word2"]);

            // ログインセッションを破棄
            LoginEntry::doLogout();
        }

        // コミット
        $obj->db->commit();

        $obj->showComplete();
        exit;
    }



    /** 本番ディレクトリにファイル移動を行う準備 */
    function TempUploadAction($obj){
        $obj->wk_moto = array();
        $workDir = $GLOBALS["session"]->getVar("workDir");
        if($workDir == "") return false;

        foreach($obj->arrfile as $item_id => $n) {
            //アップロードするファイルが存在する場合、移動
            if(!isset($obj->arrForm["n_data".$item_id]  ) || empty($obj->arrForm["n_data".$item_id]  )) $obj->arrForm["n_data".$item_id]   = "";
            if(!isset($obj->arrForm["hd_file".$item_id] ) || empty($obj->arrForm["hd_file".$item_id] )) $obj->arrForm["hd_file".$item_id]  = "";
            if(!isset($obj->arrForm["file_del".$item_id]) || empty($obj->arrForm["file_del".$item_id])) $obj->arrForm["file_del".$item_id] = "0";

            $obj->wk_moto[$item_id] = "";
            if($obj->arrForm["hd_file".$item_id] != "" && $obj->arrForm["file_del".$item_id] == "0"){
                $obj->wk_moto[$item_id] = $obj->arrForm["hd_file".$item_id];
            }
        }
        return true;
    }



    // 本番ディレクトリへ移動
    function MoveFileAction($obj) {
        // リネイム後のファイル名.[extension]
        $obj->wk_file=array();

        // 作成したディレクトリへ移動する
        $workDir = $workDir = $GLOBALS["session"]->getVar("workDir");
        $afterDir = $obj->uploadDir.$obj->eid;

        if(!is_dir($afterDir)){
            mkdir($afterDir, 0777, true);
            chmod($afterDir, 0777);
        }

        $afterDir .= "/";
        $workDir .= "/";

        // 削除が有る場合
        if(isset($obj->wk_del)) {
            foreach($obj->wk_del as $key=>$file) {
                if(file_exists($afterDir.$file)) {
                    unlink($afterDir.$file);
                }
            }
        }

        // ファイルをアップロードしていない場合
        if(count($obj->wk_saki) == 0) return true;

        // 本アップロード処理
        foreach($obj->wk_moto as $item_id => $mv_file){
            if($mv_file == "") continue;
            if(!isset($obj->wk_saki[$item_id])) continue;

            if(is_file($workDir.$mv_file)){
                $from = $workDir.$mv_file;
                $to   = $afterDir.$obj->wk_saki[$item_id];
                if(!copy($from, $to)){
                    $GLOBALS["log"]->write_log($_REQUEST["mode"], "ファイルの移動に失敗（".$from."→".$to.")");
                    return false;
                }
            }
        }

        // アップロード履歴を残す
        $list = explode('/', trim($workDir, '/'));
        $histryDir = $afterDir.$list[count($list)-1]."/";
        if(!is_dir($histryDir)){
            mkdir($histryDir, 0777, true);
            chmod($histryDir, 0777);
        }

        $files = glob($workDir."*");
        if(is_array($files)){
            foreach($files as $_key => $from){
                $to = $histryDir.pathinfo($from, PATHINFO_BASENAME);
                if(!copy($from, $to)){
                    $GLOBALS["log"]->write_log($_REQUEST["mode"], "アップロード履歴の作成に失敗 （".$from."→".$to.")");
                }
            }
        }
        return true;
    }


    /* 支払方法指定なし */
    function payAction($obj){
        $obj->payAction2();
    }


    /* 銀行振込の新規登録処理 */
    function payAction2($obj){
        // エントリーIDの取得
        $obj->getEntryNumber();

        // エントリーID採番
        $obj->ins_eid  = $GLOBALS["db"]->getOne("select nextval('entory_r_eid_seq')");

        // ファイルアップロード
        if(!$obj->TempUploadAction()){
            $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
            $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
            $obj->db->rollback();
            $obj->lockEnd();
            $obj->complete($msg);
            return;
        }

        //新規登録処理
        $obj->user_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->entry_number);
        list($wb_ret, $obj->user_id, $obj->passwd) = $obj->insertEntry();
        $obj->eid = $obj->ins_eid;

        // ファイルの移動
        if(!$obj->MoveFileAction()){
            $obj->db->rollback();
            $obj->lockEnd();
            $obj->complete("ファイルの移動に失敗しました。");
            return;
        }
    }


    /* クレジットカードの新規登録処理 */
    function payAction1($obj){
        // 登録番号の取得を3Dセキュアの有無で分岐
        $obj->user_id = "";

        // エントリーID採番
        $obj->ins_eid  = $GLOBALS["db"]->getOne("select nextval('entory_r_eid_seq')");

        //---------------------------------------------------------
        // クレジット決済処理 (3Dセキュアは取引完了まで行う)
        //---------------------------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            $isPay = $obj->doCardPayAction();
            // 取引または決済に失敗した場合
            // 無効フラグを立てて決済ログに残す
            if(!$isPay){
                list($wb_ret, $obj->user_id, $obj->passwd) = $obj->insertEntry();
                $obj->eid = $obj->ins_eid;
                $obj->entryInvalid();
                $obj->db->commit();
                $obj->lockEnd();

                $msg = ($obj->formdata["lang"] == LANG_JPN)
                     ? $GLOBALS["payerror_J"]
                     : $GLOBALS["payerror_E"];

                $arrErr = $obj->auth_param["arrErr"];
                $obj->complete($msg, implode("<br/>", $arrErr));
            }
        }

        // ファイルアップロード
        if(!$obj->TempUploadAction()){
            $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
            $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
            $obj->db->rollback();
            $obj->lockEnd();
            $obj->complete($msg);
            return;
        }

        // 3Dセキュア未使用, 決済を行わない場合
        if($obj->formdata["pgcard_use3ds"] != "1" || (isset($obj->o_authorize) && !$obj->o_authorize->isTdSecure()) 
        || $obj->formdata["credit"] != "2"){
            // エントリーIDの取得
            $obj->getEntryNumber();
            $obj->user_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->entry_number);
        }

        //新規登録処理
        list($wb_ret, $obj->user_id, $obj->passwd) = $obj->insertEntry();
        $obj->eid = $obj->ins_eid;

        //---------------------------------------------------------
        // 3Dセキュアのクレジット決済処理
        //---------------------------------------------------------
        if($obj->formdata["kessai_flg"] == "1"){
            $obj->doCardPay3dAction();
            // 3DセキュアONの場合はここでreturn
//            return;
        }

        // ファイルの移動
        if(!$obj->MoveFileAction()) {
            $obj->db->rollback();
            $obj->lockEnd();
            $obj->complete("ファイルの移動に失敗しました。");
            return;
        }
    }


    /* クレジットカード決済 */
    function doCardPayAction($obj){
        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return true;

        // オンライン決済でない
        if($obj->formdata["credit"] != "2") return true;

        // 銀行振込
        if($obj->arrForm["method"] == "2") return true;

        // 合計金額が0円
        if($obj->total_price == 0) return true;

        // 必要な情報チェック
        if(!isset($_REQUEST["form_id"]) || $_REQUEST["form_id"] == ""){
            Error::showErrorPage("処理に必要なパラメータが不足しているため、処理が行えませんでした。");
        }

        // 各フォーム用設定ファイル
        $cnf = "config.php";
        $tgMDKcopyDir = TGMDK_COPY_DIR."/".$obj->form_id;
        if(!file_exists($tgMDKcopyDir."/".$cnf)){
            $err_msg = "処理に必要なパラメータが不足しているため、処理が行えませんでした。";
            $err_mail  = "設定ファイルが作成されているか、サーバを確認してください。\n";
            $err_mail .= "ファイル:/MDK/".$tgMDKcopyDir."/".$cnf;

            //管理者宛てエラー送信
            $obj->o_mail->SendMail(SYSTEM_MAIL, "【論文投稿システム　決済フォーム】設定ファイル不明エラー", $err_mail , "");

            Error::showErrorPage($err_msg);
        }


        // 決済処理実行
        include_once(ROOT_DIR.'payment/card/Authorize.Class.php');
        include_once(ROOT_DIR.'payment/card/AuthorizeCheck.Class.php');

        $obj->o_authorize = new Authorize();

        switch($obj->arrForm["method"]){
            //カード決済
            case "1":
                // 取引番号の採番
                $obj->getOrderNumber();
                list($wb_ret, $obj->auth_param) = $obj->o_authorize->card_exec($obj);
                break;
        }


        //ログ出力
        $auth_log = "";
        foreach($obj->auth_param as $auth_key => $data){
            if(is_array($data)){
                foreach($data as $_auth_key => $_data){
                    $auth_log .= $auth_key.":".$_data."|";
                }
                continue;
            }
            $auth_log .= $auth_key.":".$data."|";
        }
        error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");

        // 決済の成功フラグだけ返す
        return $wb_ret;
    }


    function doCardPay3dAction($obj) {
        // クレジット決済かつ3Dセキュア利用の場合
        if(!isset($obj->arrForm["method"])) return;

        // クレジット決済でない
        if($obj->arrForm["method"] != "1") return;

        // 決済なし
        if($obj->formdata["kessai_flg"] != "1") return;

        // 3Dセキュア未使用
        if($obj->formdata["pgcard_use3ds"] != "1") return;

        // オンライン決済でない
        if($obj->formdata["credit"] != "2") return;

        // 3Dセキュア利用不可
        if(!$obj->o_authorize->isTdSecure()) return;


        // エントリーの無効処理（一時保存）
        $obj->entryInvalid();

        // 他の応募者を考慮しロック解除
        $obj->db->commit();
        $obj->lockEnd();

        // 3Dセキュア認証画面へ遷移（リダイレクトページ）
        $obj->assign('ACSUrl'  , $obj->o_authorize->getAcsUrl());
        $obj->assign('PaReq'   , $obj->o_authorize->getPaReq());
        $obj->assign('TermUrl' , $obj->o_authorize->getTermUrl());
        $obj->assign('MD'      , $obj->o_authorize->getMd());

        $GLOBALS["log"]->write_log(__FUNCTION__, "3Dセキュア認証開始");

        $obj->assign("onload",  "OnLoadEvent();");
        $obj->_processTemplate = "Usr/form/Usr_pg3dsend.html";
        $obj->main();
        exit;
    }


    /** 3Dセキュア決済ページ */
    function pageActionpay3d($obj) {
        // 二重登録チェック
        $rl = new Reload();
        if($rl->isReload()) {
            $msg = $GLOBALS["msg"]["reload"];
            $obj->complete($msg);
            return;
        }

        // エントリー番号
        $obj->eid = $_POST["c_field1"];
        $obj->ins_eid = $obj->eid;

        // 更新データ
        $param = array();

        // エラー配列
        $arrErr = $_POST['arrErr'];

        // 結果=成功
        if(count($arrErr) == 0) {
            $obj->lockStart();
            $obj->db->begin();

            // エントリーIDの取得
            $obj->getEntryNumber();
            $obj->user_id = $obj->formdata["head"]."-".sprintf("%05d", $obj->entry_number);

            $obj->u_date = date('YmdHis');
            Usr_initial::createUploadDir($obj);
            // アップロード情報を取得
            $obj->arrForm = $GLOBALS["session"]->getVar("form_param3");
            foreach($obj->arrfile as $item_id => $n){
                $obj->arrForm['hd_file'.$item_id] = "";
            }

            // 3Dセキュアのキャンセルを考慮し、元ファイル名を復元
            list($wa_authdata, $wa_payment_detail1, $wa_payment_detail2) = $obj->getPayData();
            if(strlen($wa_authdata['c_field_3'])){
                $arrFiles = explode(",", $wa_authdata['c_field_3']);
                $GLOBALS["session"]->setVar("workDir", $arrFiles[0]);
                $i = 1;
                foreach($obj->arrfile as $item_id => $n){
                    $obj->arrForm['hd_file'.$item_id] = $arrFiles[$i];
                    $i++;
                }
            }

            // ファイルアップロード
            if(!$obj->TempUploadAction()){
                $msg = "ERROR:作業ディレクトリの取得に失敗しました。";
                $GLOBALS["log"]->write_log($_REQUEST["mode"], $msg);
                $obj->db->rollback();
                $obj->lockEnd();
                $obj->complete($msg);
                return;
            }

            // 有効なファイル名
            foreach($obj->arrfile as $item_id => $n){
                $obj->wk_saki[$item_id] = $obj->user_id.$obj->arrForm["edata".$item_id];
            }

            // 更新データ
            $param['e_user_id']   = $obj->user_id;
            $param["invalid_flg"] = "0"; // 無効フラグを戻す
            $param["udate"] = "Now()";
            foreach($obj->arrfile as $item_id => $n){
                $key = 'edata'.$item_id;
                if(strlen($obj->arrForm['hd_file'.$item_id]) > 0) $param[$key] = $obj->user_id.$obj->arrForm[$key];
            }

            $where   = array();
            $where[] = "eid = ".$obj->db->quote($obj->eid);
            $obj->db->update("entory_r", $param, $where, __FILE__, __LINE__);

            $param = array();
            $param["payment_status"] = "3";        //支払いステータスを「売上完了」にする
            $param["udate"] = "Now()";
            $obj->db->update("payment", $param, $where, __FILE__, __LINE__);

            // ファイルの移動
            if(!$obj->MoveFileAction()) {
                $obj->db->rollback();
                $obj->lockEnd();
                $obj->complete("ファイルの移動に失敗しました。");
                return;
            }

            $obj->db->commit();

            //メール送信
            $obj->sendRegistMail($obj->user_id, "");

            //完了メッセージ
            $obj->msg = $obj->replaceMsg($obj->formWord["word1"]);

            $obj->lockEnd();

            $obj->showComplete();
            exit;
        }


        // 結果=失敗
        $obj->msg = ($obj->formdata["lang"] == LANG_JPN)
                  ? $GLOBALS["3dpayerror_J"]
                  : $GLOBALS["3dpayerror_E"];
        $obj->arrErr = $arrErr;

        //支払いステータスを「与信失敗」にする
        $obj->lockStart();
        $obj->db->begin();
        $param["payment_status"] = "8";
        $param["udate"] = "Now()";
        $where[] = "eid = ".$obj->db->quote($obj->eid);
        $obj->db->update("payment", $param, $where, __FILE__, __LINE__);
        $obj->db->commit();
        $obj->lockEnd();

        // エントリーIDをセッションに保存
        $GLOBALS["session"]->setVar("pre_eid", $obj->eid);

        $obj->paymentErrAction();
    }


}


?>