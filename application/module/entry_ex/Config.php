<?php

/**
 * 設定ファイルの設置でテンプレート化したカスタマイズを反映させるクラス
 * 
 * 
 */
class Config {

    const SECTION_AUTOLOAD   = 'autoload';
    const SECTION_MODE       = 'config';
    const SECTION_LAYOUT     = 'Layout';
    const SECTION_VARIABLE   = 'variable';
    const SECTION_DEFINE     = 'Define';


    public static $arrConfig;
    public  static $mode;
    private static $assigned = false;

    // ロードを許可するメソッド
    private static $arrow_config = array(
         'page1_payment_credit'
        ,'page1_payment_bank'
        ,'request_param_method_credit'
        ,'request_param_method_bank'
        ,'page1_method_hidden'
        ,'use_jquery'
        ,'use_jquery_fee_calculator'    // 合計金額表示
        ,'error_pointer'
        ,'error_message'
        ,'need_mark'
        ,'back_coution_class'
        ,'item_hidden'
        ,'filter'
        ,'form_word'
        ,'entry_confirm_btn_text_before'
        ,'mail_footer'
        ,'item_sort'                  // variable
        ,'use_password_reset'         // config
        ,'fee_hidden'                 // autoload
        ,'add_error_exist_otherprice' // config
        ,'show_mail_edit_body'        // config
    );

    // 呼び出し元クラスで処理を行わないクラス一覧
    private static $non_arrow = array('Config' , 'ProcessBase');


    /**
     * autoloadで利用する呼び出しモードを設定する
     * 
     * @throws Exception
     * @param  void
     * @return void
     */
    private static function init(){
        // 一度だけモードを設定する
        if(!is_null(self::$mode)) return;

        try{
            if(strpos($_SERVER['PHP_SELF'], APP_ROOT."Usr") === 0){
                Config::$mode = "Usr";
            }

            else if(strpos($_SERVER['PHP_SELF'], APP_ROOT."Mng") === 0){
                Config::$mode = "Mng";
            }

            else if(strpos($_SERVER['PHP_SELF'], APP_ROOT."Sys") === 0){
                Config::$mode = "Sys";
            }

            else{
                throw new Exception("不明なスクリプト");
            }

        }catch(Exception $e){
            Config::$mode = NULL;
            throw $e;
        }
    }


    /**
     * 改善点
     *  どのセクションにどの項目を定義すればいいのか曖昧になってきてしまっている
     *  このカスタムテンプレートを利用するフォームの数が少ないうちに調整するのが好ましい
     *  案としてセクションごとの配列を定義してそれを基準に読み込む
     * 
     *  曖昧になってきてしまった定義の例
     *   use_jqueryなど
     *   何がAutoloadなのか、何がLayoutなのかがはっきりしていない
     * 
     * そもそも定義はユーザは触れないから定義だけはっきりしてしまえばいい気がする
     */


    /* カスタマイズテンプレートの定義 */
    public static function load($obj){

        try{
            Config::init();
            if(Config::$mode == 'Sys') return;

            $arrConfig = Config::loadConfig($obj);

            // 常にロードする設定を読み込む
            Config::autoload($obj, $arrConfig);


            // モード用の設定を読み込む
            $section = Config::$mode."_".Config::SECTION_MODE;
            if(isset($arrConfig[$section])){
                Config::run($obj, $arrConfig[$section]);
            }


            // 通常の設定を読み込む
            $trace = debug_backtrace();
            $trace = debug_backtrace();
            foreach($trace as $_trace){
                if(!isset($_trace['class'])) $_trace['class'] = "";
                if(!isset($_trace['function'])) $_trace['function'] = "";

                $class    = $_trace['class'];
                $function = $_trace['function'];

                if(strlen($class)    == 0) continue;
                if(strlen($function) == 0) continue;
                if(in_array($class, Config::$non_arrow)) continue;

                $section = sprintf("%s_%s", $class, $function);
                if(isset($arrConfig[$section])){
                    Config::run($obj, $arrConfig[$section]);
                }
    	    }


            // レイアウト系の設定を読み込む
            $section = Config::SECTION_LAYOUT;
            if(isset($arrConfig[$section])){
                Config::run_layout($obj, $arrConfig[$section]);
            }


            // 定数定義の設定を読み込む
            $section = Config::SECTION_DEFINE;
            if(isset($arrConfig[$section])){
                Config::run_define($obj, $arrConfig[$section]);
            }

            // Assignはまとめて最後にやる
            if(Config::$assigned && method_exists($obj, "assign")){
                $obj->assign("arrItemData", $obj->arrItemData);
            }

        }catch(Exception $e){
            // 
        }
    }


    /* 変数の定義 */
    /* コンクリフトしないような変数を定義すること */
    /* 処理系に利用する変数のみを定義する */
    /* 最終的にAssiginして利用する変数はレイアウト系なのでそのセクションを利用する */
    public static function loadVariable($obj){

        try{
            Config::init();
            if(Config::$mode == 'Sys') return;
            $arrConfig = Config::loadConfig($obj);

            // 変数用の設定を読み込む
            // Mng,Usrで競合しないかな…
            $section = Config::SECTION_VARIABLE;
            if(isset($arrConfig[$section])){
                Config::run($obj, $arrConfig[$section]);
            }

        }catch(Exception $e){
            // 
        }
    }



    /**
     * 外部から一部の設定だけを読み込む
     * 
     * @param  object $obj
     * @return array  $arrConfig 全セクションの設定
     */
    public function loadSection($obj, $section=Config::SECTION_AUTOLOAD){
        Config::init();
        if(Config::$mode == 'Sys') return;

        $arrConfig = Config::loadConfig($obj);

        // モード用の設定を読み込む
        $section = ($section == Config::SECTION_AUTOLOAD) ? $section : Config::$mode."_".$section;
        if(isset($arrConfig[$section])){
            Config::run($obj, $arrConfig[$section]);
            return true;
        }
        return false;
    }


    /**
     * 設定ファイルを読み込む
     * 
     * @param  object $obj
     * @return array  $arrConfig 全セクションの設定
     */
    private static function loadConfig($obj){
        // 一度だけ設定を読み込む
        if(is_null(self::$arrConfig)){
        	if(!isset($obj->form_id)) return;

            // フォーム別設定ファイル
            $arrConfigWay = array();
            $arrConfigWay[0] = ROOT_DIR."application/bootstrap/";
            $arrConfigWay[1] = ROOT_DIR."customDir/".$obj->form_id."/package/config/";
            
            while(true){
                foreach($arrConfigWay as $_key => $configDir){
                    // ファイルパス
                    $config = sprintf($configDir."form%s.ini", $obj->form_id);
                    
                    // 設定ファイルあり
                    if(file_exists($config)) break 2;
                }
                // 設定ファイルなし
                return;
            }

            $arrConfig = parse_ini_file($config, true);
            if(!$arrConfig || count($arrConfig) == 0) return;

            self::$arrConfig = $arrConfig;
        }
        return self::$arrConfig;
    }

    /**
     * セクションに関わらず常に設定を反映させる
     * 
     * @param object $obj
     * @param array  $arrConfig 全セクションの設定
     * @return void
     */
    private static function autoload($obj, $arrConfig){
        $section = Config::SECTION_AUTOLOAD;
        if(!isset($arrConfig[$section])) return;
        Config::run($obj, $arrConfig[$section]);
    }


    /**
     * 設定ファイルの反映
     *      常に許可された有効な設定のみ反映させる
     *      fixed 依存関係のチェックは行わない
     *      fixed 設定を読み込む優先順位を考慮しない
     * 
     * @param object $obj
     * @param array  $arrConfig セクションごとの設定
     * @return void
     */
    private static function run($obj, $arrConfig){
        foreach($arrConfig as $_unit => $_value){
//            if($_value != 1) continue;
            if(!in_array($_unit, self::$arrow_config)) continue;
            if(!method_exists('Config', $_unit)) continue;
            Config::$_unit($obj, $_value);
        }
    }


    /**
     * レイアウト系の設定ファイルの反映
     *      fixed 依存関係のチェックは行わない
     *      fixed 設定を読み込む優先順位を考慮しない
     * 
     * @param object $obj
     * @param array  $arrConfig セクションごとの設定
     * @return void
     */
    private static function run_layout($obj, $arrConfig){
        foreach($arrConfig as $_unit => $_value){
            $unit = explode("@", $_unit);
            if(count($unit) != 2) continue;

            $item_id = $unit[0];
            $unit    = $unit[1];

            if(!method_exists('Config', $unit)) continue;

            Config::$unit($obj, $item_id, $_value);
        }
    }


    /**
     * 特殊設定:Defineセクション
     *   設定名と設定値をそのまま定数を定義する
     *   すでに定数名が定義されていたら一度破棄して生成し直す
     * 
     * @param object $obj
     * @param array  $arrConfig セクションごとの設定
     * @return void
     */
    private static function run_define($obj, $arrConfig){
        foreach($arrConfig as $_key => $_config){
            if(defined($_key)) unset($_key);
            define($_key, $_config);
        }
    }


    // 設定値からグローバル変数を置換する
    // 書式：#GLOBAL\[キー]\..#
    private static function global_variable_replace($string){
        if(preg_match("/[\#]{1}GLOBALS(".preg_quote("\\")."(([a-zA-Z0-9]+_*)+))+[\#]{1}/u", $string, $matches) !== 1) return $string;

        $pattern = $matches[0];
        $variable = trim($pattern, "#");
        $keys = explode("\\", $variable);
        $replace = $GLOBALS;
        foreach($keys as $i => $key){
            if(!isset($replace[$key])) break;
            $replace = $replace[$key];
        }
        // 対応する値なし
        if(is_array($replace)) $replace = "";

        // 複数指定を考慮し繰り返す
        return Config::global_variable_replace(str_replace($pattern, $replace, $string));
    }


    /**
     * AutoLoad,設定上書き系
    /*-------------------------------------*/

    // Feeを項目を非表示とする # 一律タイプのみ
    private static function fee_hidden($obj, $value){
        $obj->hide_fee_area = true;
        // Fee情報を空にする
        $obj->wa_price = array();
        
        // MngCSV
        if(Config::$mode == "Mng"){
            if(isset($obj->wa_ather_price[-1])){
                unset($obj->wa_ather_price[-1]);
            }
        }
        
        if(method_exists($obj, "assign")){
            $obj->assign("hide_fee_area", $obj->hide_fee_area);
        }
    }
    
    
    // クレジットカードで固定
    private static function page1_payment_credit($obj, $value){
        // 銀行振込を破棄
        unset($obj->wa_method[2]);

        if(method_exists($obj, "assign")){
            $obj->assign("va_method",   $obj->wa_method);
        }
    }


    // 銀行振込で固定
    private static function page1_payment_bank($obj, $value){
        // クレジットカードを破棄
        unset($obj->wa_method[1]);

        if(method_exists($obj, "assign")){
            $obj->assign("va_method",   $obj->wa_method);
        }
    }


    // 支払方法 # クレジットカードの選択済みで固定
    private static function request_param_method_credit($obj, $value){
        $obj->arrForm['method'] = 1;
    }


    // 支払方法 # 銀行振込の選択済みで固定
    private static function request_param_method_bank($obj, $value){
        $obj->arrForm['method'] = 2;
    }


    // 支払方法をhiddenに変更
    private static function page1_method_hidden($obj, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('method_layout_hidden', true);
        }
    }


    // jQueryを読み込む
    private static function use_jquery($obj, $value=""){
        if(method_exists($obj, "assign")){
            $obj->assign('useJquery',  true);
        }
    }


    // 合計金額表示を利用する
    private static function use_jquery_fee_calculator($obj, $value){
        Config::use_jquery($obj); // 依存関係

        if(method_exists($obj, "assign")){
            $obj->assign('use_jquery_fee_calculator',  true);
        }
    }

    // エラーが発生した箇所を強調する
    private static function error_pointer($obj, $value){
        Config::use_jquery($obj); // 依存関係

        $onload = "";
        if(is_array($obj->arrErr) && count($obj->arrErr) > 0){
            $arrErr = array_keys($obj->arrErr);
            $arrErr = array_filter($arrErr, create_function('$val', 'return substr($val, 0, 5) == "edata";'));
            if(function_exists("json_encode")){
                $onload = sprintf('entry.errorPointerArray(%s);', json_encode($arrErr));
            }

        }
        $obj->assign("onLoad", $onload);
    }


    // エラーメッセージを特定の文言に書き換える
    private static function error_message($obj, $value){
        if(is_array($obj->arrErr) && count($obj->arrErr) > 0){
            $obj->assign("arrErr", explode("|", $value));
        }
    }

    // 必須マークの設定
    private static function need_mark($obj, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('need_mark', $value);
        }
    }


    // 戻るボタン注意書きのクラス
    private static function back_coution_class($obj, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('back_coution_class', $value);
        }
    }


    // 項目を非表示にする
    private static function item_hidden($obj, $value){
        Config::$assigned = true;

        $items = array_filter(explode(',', $value));
        foreach($items as $_key => $item_id){
            $group_id = Usr_init::getItemInfo($obj, $item_id, "group_id");
            if(!in_array($group_id, array(1,3))) continue;

            $obj->arrItemData[$group_id][$item_id]['disp']      = 1;
            $obj->arrItemData[$group_id][$item_id]['item_view'] = 1;
        }
    }


    // 項目の並び順をフォーム専用に自動ソートする
    private static function item_sort($obj, $value){
        $obj->item_sort = "form_item.item_sort";
        /**
         * 通常はitem_ini.sortを基準に並び順を決定する
         * このメソッドを呼び出すことでform_item.item_sortを基準に並び順を決定する
         * 事前にform_item.item_sortの並びを整える必要があります
         * とりあえず呼び出し元の定義まで…。
         */
    }


    // 特定のクラスのメソッド実行前のフィルタ(特殊)
    // 要改善：任意のタイミングで呼び出したい
    // セクション名を引数にする
    private static function filter($obj, $value){
        // 実装は外部クラスに任せる
        $method = "filter_section";
        if(is_object($obj->exClass) && method_exists($obj->exClass, $method)){
            return $obj->exClass->$method($obj);
        }
    }


    // フォーム文言を上書きする
    private static function form_word($obj, $value){
        $value = explode("||", $value);
        foreach($value as $_key => $arrWord){
            if(preg_match("/^word\d{2}@/u", $arrWord, $match) !== 1) continue;
            list($word_key, $word) = explode("@", $arrWord);
            if(!isset($obj->formWord[$word_key])) continue;

            $obj->formWord[$word_key] = Config::global_variable_replace($word);
        }

        if(method_exists($obj, "assign")){
            $obj->assign('formWord', $obj->formWord);
        }
    }


    // エントリーページの確認画面のボタンの直上に表示するテキスト
    // 影響範囲：入力
    private static function entry_confirm_btn_text_before($obj, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('entry_confirm_btn_text_before', $value);
        }
    }


    /**
     * パスワード再設定の機能の有効切り替え
     *   configセクション
     * */
    private static function use_password_reset($obj, $value){
        $obj->use_password_reset = true;
        if(method_exists($obj, "assign")){
            $obj->assign('use_password_reset', $obj->use_password_reset);
        }
    }


    /**
     * その他決済項目の必須設定をONにする
     *   autolodセクション
     *   # Usr_configはpremainの直前で呼ばれるためautoloadでないとエラーチェック時に実行されない
     * */
    private static function add_error_exist_otherprice($obj, $value)
    {
        $obj->exist_otherprice = true;
        $obj->exist_otherprice_title = $value;
    }


    /**
     * 編集可フォーム - 受領メールID/PWエリアの表示フラグ
     *   configセクション
     * */
    private static function show_mail_edit_body($obj, $value)
    {
        if($value) $obj->show_mail_edit_body = true;
    }


    /**
     * レイアウト系
    /*-------------------------------------*/

    // 帯追加
    // 影響範囲：入力、確認、詳細、メール
    // 未対応  ：CSV
    private static function obi($obj, $item_id, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('obi_id' .$item_id, $item_id);
            $obj->assign('obi_val'.$item_id, $value);
        }
    }


    // 帯下のテキスト
    // 影響範囲：入力、確認、詳細、メール
    private static function obi_after_text($obj, $item_id, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('obi_after_text'.$item_id, $value);
        }
    }


    // 帯下のテキスト2
    // 影響範囲：入力、確認、詳細、メール
    private static function obi_after_text2($obj, $item_id, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('obi_after_text2'.$item_id, $value);
        }
    }


    // 帯下の注意 *帯と帯下テキストの間に表示
    // 影響範囲：入力
    private static function obi_text($obj, $item_id, $value){
        if(method_exists($obj, "assign")){
            $obj->assign('obi_text'.$item_id, $value);
        }
    }


    /**
     * 帯、帯下のテキスト、帯なしテキストassign
     * smarty_function
     * 
     * @param  array  $params
     * @return array  $smarty
     */
    function assign_obi($params, &$smarty){
        $mode = $params['mode'];
        $view = $smarty->get_template_vars();
        $item_id = isset($view['item_id']) ? $view['item_id'] : $params['item_id'];
        $lang    = $view['lang'];

        if(!isset($view['obi_id'.$item_id])) return;

        $obi = $view['obi_val'.$item_id]; // 追加帯テキスト


        // Usrモード
        if(Config::$mode == 'Usr'){
            // メール
            if($mode == "mail"){
                if(strlen($obi) > 0){ // 帯ありテキスト
                    $format  = ($lang == LANG_JPN) ? "\n【%s】\n\n%s%s" : "\n[%s]\n\n%s%s%s";
                }else{                  // 帯なしテキスト
                    $format  = ($lang == LANG_JPN) ? "\n%s%s%s" : "\n%s%s%s%s";
                }

                $obi  = strip_tags($obi);
                $text = "";
                $after_text = (isset($view['obi_after_text'.$item_id]))
                      ? strip_tags($view['obi_after_text'.$item_id])."\n\n"
                      : '';
                // < や > の対策
                $after_text  = htmlspecialchars_decode($after_text);
                $after_text2 = "";

            // 入力、確認
            }else{
                if(strlen($obi) > 0){ // 帯ありテキスト
                    $format  = '</table><div class="bname obi'.$item_id.'">%s</div>%s%s%s<table class="rtbl rtbl'.$item_id.'">';
                }else{                  // 帯なしテキスト
                    $format  = '</table>%s<br/>%s%s%s<table class="rtbl rtbl'.$item_id.'">';
                }
                $text = (isset($view['obi_text'.$item_id]) && $mode=="input") ? $view['obi_text'.$item_id] : '';
                $after_text = (isset($view['obi_after_text'.$item_id]))
                      ? $view['obi_after_text'.$item_id]
                      : '';
                $after_text2= (isset($view['obi_after_text2'.$item_id]) && $mode=="input")
                      ? $view['obi_after_text2'.$item_id]
                      : '';
            }
        }
        // Mngモード
        if(Config::$mode == 'Mng'){
            $format  = '</table><br/>%s<table class="listtbl"><caption>%s%s%s</caption>';
            $obi  = strlen($obi) > 0 ? "<p>".$obi."</p>" : '';
            $text = "";
            $after_text = (isset($view['obi_after_text'.$item_id]))
                  ? strip_tags($view['obi_after_text'.$item_id])
                  : '';
            $after_text2 = "";
        }

        return sprintf($format, $obi, $text, $after_text, $after_text2);
    }



    /**
     * 項目上書き系
    /*-------------------------------------*/


    // Size of text (default 40)
    // User only
    private static function item_text_width($obj, $item_id, $value){
        Usr_init::overrideItem($obj, $item_id, "size", $value);
        Config::$assigned = true;
    }


    // 項目の表示設定[2:編集不可、それ以外:非表示]
    private static function item_disp($obj, $item_id, $value){
        Usr_init::overrideItem($obj, $item_id, "disp", $value);
        Config::$assigned = true;
    }


    // Add attribute disabled (default false)
    // User only
    private static function item_disable($obj, $item_id, $value){
        if(strlen($obj->admin_flg) > 0) return;
        Usr_init::overrideItem($obj, $item_id, "disabled", $value);
        Config::$assigned = true;
    }


    // Separater of radio or checkbox (default br)
    // User only
    private static function item_separator($obj, $item_id, $value){
        Usr_init::overrideItem($obj, $item_id, "sep", $value);
        Config::$assigned = true;
    }


    // Size of textarea (default 5 and 85)
    // User only
    private static function item_textarea_row($obj, $item_id, $value){
        Usr_init::overrideItem($obj, $item_id, "rows", $value);
        Config::$assigned = true;
    }


    // Size of textarea (default 5 and 85)
    // User only
    private static function item_textarea_col($obj, $item_id, $value){
        Usr_init::overrideItem($obj, $item_id, "cols", $value);
        Config::$assigned = true;
    }



    /**
     * メールクラス系
    /*-------------------------------------*/
    private static function mail_footer($obj, $value){
        if(strlen($value) == 0) return;

        // CustomDirにテンプレートを配置
        $template = $obj->customDir.__FUNCTION__.".txt";

        if($value == 1){
            if(!file_exists($template)) return;
            $obj->formdata["contact"] = file_get_contents($template);
        }
    }


}


?>