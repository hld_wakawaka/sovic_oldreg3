<?php
require_once("PEAR.php");
require_once(MODULE_DIR."custom/LoginBase.class.php");

/**
 * LoginEntryクラス
 *
 * ログイン 応募者クラス
 *
 * @package	include.LoginEntry
 * @access	public
 **/

// テーブル名
//define("MEMBER_TABLE", "v_user");
 // ログインID項目名
define("ENTRY_ID_FIELD_NAME", "e_user_id");
 // ログインIDはメールアドレスの場合
define("IF_ENTRY_ID_EMAIL", false);
 // パスワード項目名
define("ENTRY_PW_FIELD_NAME", "e_user_passwd");
 // 削除ステータス
//define("MEMBER_NOT_DEL_WHERE", "temp = 0");
define("ENTRY_NOT_DEL_WHERE", "");

class LoginEntry extends LoginBase {

	var $_memberData = array();

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function LoginEntry() {
		parent::PEAR();
	}

	/**
	 * デストラクタ
	 * @access	public
	 */
	function _LoginEntry() {
	}

//	PHP5の場合のコンストラクタ
//	function __construct() {
//	}

//	PHP5の場合のデストラクタ
//	function __destruct() {
//	}

	/**
	 * ログインチェックを行います
	 * @access	public
	 */
	function checkLogin($url = LOGIN_ROOT) {

		if (!$GLOBALS["session"]->getVar("isLoginEntry")) {
			return false;
		}

		return true;
	}

	function checkValid() {

		if($GLOBALS["entryData"]["status"] == "2") {
			$err = $GLOBALS["msg"]["err_login_status"];
		}

	}

	/* 受付期間かどうかチェック
	 *
	 * 戻り値
	 *  0 : 正常
	 *  1 : 開始前
	 *  2 : 終了後
	 *  3 : 審査中
	 */

	/**
	 * ログインチェックを行います
	 * @access	public
	 */
	function checkLoginRidirect($url = LOGIN_ROOT, $isReturnCurrentPage = true) {

        // エラーフラグ
        $err_flg = 1;
		if ($GLOBALS["session"]->getVar("isLoginEntry")) {
		    $err_flg = 0;

		    $eid     = $GLOBALS["entryData"]['eid'];
		    $form_id = $GLOBALS["entryData"]['form_id'];
            // ログインユーザを取得
            $entryData = parent::getLoginObjectUsr($form_id, NULL, NULL, $eid);
            if(!$entryData){
                $err_flg = 1;
            }

            //-----------------------
            // 同一セッションか？
            //-----------------------
            // 重複ログインチェック
            if($entryData["access_session"] != "" && $entryData["access_session"] != session_id()){
                // 一定時間内の場合は他のユーザが操作中とみなす
                if(!parent::isInterval('access_time')){
                    $err_flg = 1;
                }
            }
        }

        return !(bool)$err_flg;
	}

	/**
	 * ログイン処理を行います
	 * @access	public
	 * @param  integer  $type 言語タイプ
	 * @param	string	$login_id
	 * @param	string	$password
	 * @return	string	エラーメッセージ
	 */
	function doLogin($type, $login_id, $password) {

        $form_id = $GLOBALS["form"]->formData["form_id"];

		$errMsg = "";

		// 入力チェック
		if (empty($login_id)) {
			if(!IF_ENTRY_ID_EMAIL) {
				$errMsg[] = sprintf(ERR_REQUIRE_INPUT_PARAM, $GLOBALS["msg"]["login_id"]);
			} else {
				$errMsg[] = sprintf(ERR_REQUIRE_INPUT_PARAM, "メールアドレス");
			}
		}
		if (empty($password)) {
			$errMsg[] = sprintf(ERR_REQUIRE_INPUT_PARAM, $GLOBALS["msg"]["login_passwd"]);
		}
		
		if(isset($_GLOBALS["form"])) {
			if(!$GLOBALS["form"]->formData["form_id"] > 0) {
				$errMsg[] = "システムエラーが発生しました。　None Form_id";
			}
		}
		

		if (empty($errMsg)) {
            // ログインユーザを取得
			$entryData = parent::getLoginObjectUsr($form_id, $login_id, $mail);

			if (!$entryData) {
				$errMsg = $GLOBALS["msg"]["err_not_loginid"];

			} else {
				// パスワード違いエラー
				if ($entryData[ENTRY_PW_FIELD_NAME] != $password) {
					$errMsg = $GLOBALS["msg"]["err_mistake_password"];

                    // 一定時間内にN回目失敗した場合は、アカウントロック
                    // パスワード間違いのカウントアップ
                    parent::countupUsr();

                    // 一定期間を過ぎている場合
                    if(parent::isInterval('login_try_date')){
                        $GLOBALS["log"]->write_log('login', "login error count reset----[Usr]login_id:".$login_id."|count:".$param["login_err_count"]);

                    // 一定期間内
                    }else{
                        $GLOBALS["log"]->write_log('login', "login error count up----[Usr]login_id:".$login_id."|count:".$param["login_err_count"]);
                        // 規定回数オーバーの場合 ロック
                        if(parent::isCountOver()){
                            $errMsg = $GLOBALS["msg"]["account_lock"];
                            $GLOBALS["log"]->write_log('login', "login lock----[Usr]login_id:".$login_id);
                        }
                    }
				}

				if($entryData["status"] == "2") {
					$errMsg = $GLOBALS["msg"]["err_login_status"];
				}
			}
		}


		// ログイン成功
		if (empty($errMsg)) {
            // アカウントロック
            if($entryData["lock_flg"] == 1){
                // 一定期間を過ぎている場合は、ロック解除
                if(parent::isInterval('lock_date')){
                    parent::unlockUsr();
                    $GLOBALS["log"]->write_log($mode,"login lock cancellation ----[Usr]login_id:".$login_id);

                // 一定期間内
                }else{
                    $GLOBALS["log"]->write_log($mode,"login locked----[Usr]login_id:".$login_id);

                    // ロックされてるからログイン不可
                    $errMsg = $GLOBALS["msg"]["account_lock"];
                    return $errMsg;
                }
            }


            // 重複ログインチェック
            if($entryData["access_session"] != "" && $entryData["access_session"] != session_id()){
                // 一定時間内の場合は他のユーザが操作中とみなす
                if(!parent::isInterval('access_time')){
                    $errMsg = $GLOBALS["msg"]["account_same"];
                    return $errMsg;
                }
            }


			// ログイン成功なのでログイン会員オブジェクトを生成
			$loginEntry = new LoginEntry();
			unset($entryData["e_user_passwd"]);

            // パスワード間違い件数クリア
            // 最終ログイン日時セット
            parent::updateUsr();

			$loginEntry->_entryData = $entryData;

			// セッション情報にログイン情報を保存する
			$GLOBALS["session"]->setVar("isLoginEntry", true);
			$GLOBALS["session"]->setVar("entryData", $entryData);
			$entryData = $GLOBALS["session"]->getVar("entryData");
			$GLOBALS["entryData"] = $entryData;

		}

		return $errMsg;
	}

	/**
	 * ログアウト処理を行います
	 * @access	public
	 * @return	string	エラーメッセージ
	 */
	function doLogout() {
        parent::clearUsr();
		$GLOBALS["session"]->unsetVar("isLoginEntry");
		$GLOBALS["session"]->unsetVar("entryData");
        $GLOBALS["session"]->unsetVar("auth");

//		session_destroy();
		return;
	}

}
?>
