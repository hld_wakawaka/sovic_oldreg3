<?php
require_once("PEAR.php");

// ログイン画面
//define("LOGIN_ROOT_ADMIN", SSL_URL_ROOT."Sys/login/index.php");
define("LOGIN_ROOT_ADMIN", URL_ROOT."Sys/login/index.php");
// 管理者用テーブル名
define("ADMIN_TABLE", "admin");
// ログインID項目名
define("ADMIN_ID_FIELD_NAME", "login_id");
// パスワード項目名
define("ADMIN_PW_FIELD_NAME", "password");
// 名前項目名
define("ADMIN_NAME_FIELD_NAME", "name");


/**
 * LoginAdminクラス
 *
 * ログイン管理クラス
 *
 * @package	include.LoginAdmin
 * @access	public
 * @author	Jun Fushimi <fushimi@act-creative.co.jp>
 * @create	2004/12/29
 * @version	$Id: LoginAdmin.class.php,v 1.1 2007/09/26 18:04:00 fushimi Exp $
 **/
class LoginAdmin extends PEAR {

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function LoginAdmin() {
		parent::PEAR();
	}

	/**
	 * デストラクタ
	 * @access	public
	 */
	function _LoginAdmin() {
	}

//	PHP5の場合のコンストラクタ
//	function __construct() {
//	}

//	PHP5の場合のデストラクタ
//	function __destruct() {
//	}

	/**
	 * ログインチェックを行います
	 * @access	public
	 */
	static function checkLogin() {

		if (!$GLOBALS["session"]->getVar("isLoginAdmin")) {
			return false;
		}

		return true;
	}

	/**
	 * ログインチェックを行います
	 * @access	public
	 */
	static function checkLoginRidirect($url = LOGIN_ROOT_ADMIN) {

		if (!LoginAdmin::checkLogin()) {
			header("location: ".$url);
		}

		return true;
	}

	/**
	 * ログイン処理を行います
	 * @access	public
	 * @param	string	$login_id
	 * @param	string	$password
	 * @return	string	エラーメッセージ
	 */
	function doLogin($login_id, $password) {

		$errMsg = "";

		// 入力チェック
		if (empty($login_id)) {
			$errMsg = sprintf(ERR_REQUIRE_INPUT_PARAM, "ログインID");
		}
		if (empty($password)) {
			$errMsg = sprintf(ERR_REQUIRE_INPUT_PARAM, "パスワード");
		}

		if (empty($errMsg)) {

			$dbGeneral = new DbGeneral();

			$column = "*";
			$from = ADMIN_TABLE;

			//$where[] = ADMIN_ID_FIELD_NAME."	= '".addslashes($login_id)."'";
			$where[] = ADMIN_ID_FIELD_NAME."	= ".$dbGeneral->quote($login_id);
			$where[] = "del_flg	= 0";
			$adminData = $dbGeneral->getData($column, $from, $where, __FILE__, __LINE__);

			if (!$adminData) {
				$errMsg = ERR_NOT_ADMIN_MAIL;
			} else {
				if ($adminData[ADMIN_PW_FIELD_NAME] != md5($password.PASS_PHRASE)) {
					// パスワード違いエラー
					$errMsg = ERR_MISTAKE_ADMIN_PASSWORD;
				}
			}
		}

		// ログイン成功
		if (empty($errMsg)) {
			// セッション情報にログイン情報を保存する
			$GLOBALS["session"]->setVar("isLoginAdmin", true, HSLV_LOGIN_ADMIN);
			$GLOBALS["session"]->setVar("adminData", $adminData, HSLV_LOGIN_ADMIN);
			$adminData = $GLOBALS["session"]->getVar("adminData");
			$GLOBALS["adminData"] = $adminData;
		}

		return $errMsg;
	}

	/**
	 * ログアウト処理を行います
	 * @access	public
	 * @return	string	エラーメッセージ
	 */
	function doLogout() {

		$GLOBALS["session"]->unsetVar("isLoginAdmin");
		$GLOBALS["session"]->unsetVar("adminData");
		//$GLOBALS["session"]->unsetAllAt(HSLV_LOGIN_ADMIN);

		//session_destroy();

		return;
	}

	/**
	 * 管理者一覧をadmin_idをキー、名前を値として返します
	 * @access	public
	 * @return	array
	 */
	function getAdminList() {

		$adminArray = array();

		$dbGeneral = new DbGeneral();

		$column		= "*";
		$from		= ADMIN_TABLE;

		$orderBy	= ADMIN_NAME_FIELD_NAME;
		$offset		= -1;
		$limit		= -1;
		$rowArray = $dbGeneral->getListData($column, $from, null, $orderBy, $offset, $limit, __FILE__, __LINE__);
		foreach ($rowArray as $key => $value) {
			$adminArray[$value[ADMIN_ID_FIELD_NAME]] = $value[ADMIN_NAME_FIELD_NAME];
		}

		return $adminArray;
	}
}
?>
