<?php

class Utils {

    /**
     * Sys,Mng,Usrであるか判別する
     * 
     * @param  void
     * @return void
     */
    public static function getFunction(){
        $mode = FUNCTION_USR;

        if(strpos($_SERVER['PHP_SELF'], APP_ROOT.FUNCTION_USR) === 0){
            $mode = FUNCTION_USR;
        }

        else if(strpos($_SERVER['PHP_SELF'], APP_ROOT.FUNCTION_MNG) === 0){
            $mode = FUNCTION_MNG;
        }

        else if(strpos($_SERVER['PHP_SELF'], APP_ROOT.FUNCTION_SYS) === 0){
            $mode = FUNCTION_SYS;
        }

        return $mode;
    }


    /**
     * バックトレースをテキスト形式で出力する
     *
     * 現状スタックトレースの形で出力している。
     * @param  array  $arrBacktrace バックトレース
     * @return string テキストで表現したバックトレース
     */
    public static function toStringBacktrace($arrBacktrace) {
        $string = '';

        $arrBacktrace = array_reverse($arrBacktrace);
        foreach ($arrBacktrace as $backtrace) {
            if (strlen($backtrace['class']) >= 1) {
                $func = $backtrace['class'] . $backtrace['type'] . $backtrace['function'];
            } else {
                $func = $backtrace['function'];
            }

            $string .= $backtrace['file'] . '(' . $backtrace['line'] . '): ' . $func . "\n";
        }

        return $string;
    }


    /**
     * ユーザーエージェントを取得する
     *
     * @param  NULL
     * @return string ユーザーエージェント
     */
    public static function getUserAgent(){
        if(!isset($_SERVER["HTTP_USER_AGENT"])) return NULL;
        $ua = strtolower($_SERVER["HTTP_USER_AGENT"]);
        $arrUA = array("chrome", "opera", "safari", "msie", "firefox", "rv");
        foreach($arrUA as $_key => $_ua){
            $pattern = '([ \t\n\r\f\v]|^)'.$_ua.'(\/|[ \t\n\r\f\v]|\:)([0-9]+($|\.|[ \t\n\r\f\v]|\;))+';
            // chrome, opera, safari, ie<=11, firefox
            if(preg_match('/'.$pattern.'/u', $ua, $matches) === 1){
                $ua = trim($matches[0], " \t\n\r\f\v;.");
                $ua = str_replace(array(":", " "), "/", $ua);
                $ua = str_replace("rv",   "msie", $ua);
                $ua = str_replace("msie", "ie",   $ua);
                break;

            // ie11, other
            }else{
                $ua = "unknown:".$ua;
            }
        }
        return $ua;
    }

}

