<?php
require_once("MDB2.php");

define("PEAR_LOG_DEBUG", "");

/**
 * MyDBクラス
 *
 * データベースクラス
 * TODO GLOBAL["db"]の形式をやめてシングルトンにする
 *
 **/
class MyDB{

	// データベース接続DSN
	var $_dsn;
	// データベースコネクション
	var $_con;
	// 直近に実行したSQL
	var $_sql;
	// 例外を捕捉するフラグ
	var $isThrowable = false;

	/**
	 * コンストラクタ
	 * @access	public
	 * @param	string	$dsn	データベース接続DSN
	 */
	function MyDB($dsn = DB_DSN) {
		$this->_dsn = $dsn;
	}
 
	 

	/**
	 * インスタンスを得る
	 * @access	public
	 * @param	string	$dsn            データベース接続DSN
	 * @param   boolean $isThrowable    例外を捕捉するフラグ
	 * @return reference of instance
	 */
	static function &getInstance($dsn = DB_DSN, $isThrowable=false) {
		//DB接続をstaticスコープで実装 使い回す
		static $db;

		if(!isset($db)){
			$db = new MyDB($dsn);
			$db->connect();
		}

        // 設定値の初期化
        $db->isThrowable = $isThrowable;

		return $db;

	}

	/**
	 * データベースへの接続を行う
	 * @access	public
	 * @param	boolean	$pConnect	持続接続時はtrueを設定
	 */
	function connect($pConnect = false){

		$pConnect = array(
			'debug' => 2,
			'portability' => MDB2_PORTABILITY_ALL,
			'result_buffering' => true,
		);


		$this->_con = MDB2::connect($this->_dsn, $pConnect);
		if(PEAR::isError($this->_con)){
			$this->error($this->_con->getMessage());
		}
		$this->_con->loadModule('Extended');
	}

	/**
	 * データベースの切断処理を行う
	 * @access	public
	 */
	function disconnect(){
		if (DB::isConnection($this->_con)) {
			$this->_con->disconnect();
		}
	}


    /**
     * 例外を捕捉するフラグのON/OFFを切り替える
     * SQLを発行する直前に実行する
     * 
     * @param  boolean $isThrowable
     * @return null
     */
    function setThrowable($isThrowable){
        $this->isThrowable = $isThrowable;
    }


	/**
	 * トランザクションを開始する
	 * @access	public
	 */
	function begin(){
		$this->_con->beginTransaction();
	}

	/**
	 * トランザクションを確定する
	 * @access	public
	 */
	function commit(){
		$this->_con->commit();
	}

	/**
	 * トランザクションを取り消す
	 * @access	public
	 */
	function rollback(){
		$this->_con->rollback();
	}



    /** SQLをログに出力する形式で生成  */
    public function toQueryString($sql='', $arrVal=array()){
        return "SQL: " .$sql. "\n PlaceHolder: " .print_r($arrVal, true);
    }



    /**
     * クエリーを実行する
     * 
     * 将来的にthrow出来るように仕組みだけ実装する
     * throwするフラグを設置してそのフラグがONの場合にthrowさせる
     * 
     * @access	public
     * @throws  Exception : isThrowable=trueの場合
     *              $objDb->setThrowable(true);やオプションを指定してクラスを生成すればよい
     * @param	string	$sql	SQL文字列
     * @param	array	$arrVal	パラメータ配列
     * @param	mixed	$types	プレースホルダの型指定 デフォルトnull = string
     * @param	string	$result_types	返値の型指定:MDB2_PREPARE_MANIP=select以外(insert, update, delete)
     * @return	mixed	DB_result
     */
    function query($sql, $arrVal=array(), $types=NULL, $result_types=MDB2_PREPARE_RESULT){
        $sth = $this->_con->prepare($sql, $types, $result_types);
        if(PEAR::isError($sth)){
            $errMsg = $this->getDbError($sth, $sql, $arrVal);
            log_class::printLog($errMsg, ERROR_LOG_FILE);
            if($this->isThrowable) throw new Exception($errMsg);
            $this->showErrorPage();
        }


        // 実測
        $queryLog = $this->toQueryString($sth->query, print_r($arrVal, true))."\n";
        $timeStart = microtime(true);

        // SQL実行
        $result = $sth->execute((array) $arrVal);
        if(PEAR::isError($result)){
            $_sql = isset($sth->query) ? $sth->query : '';
            $errMsg = $this->getDbError($result, $_sql, $arrVal);
            log_class::printLog($errMsg, ERROR_LOG_FILE);
            if($this->isThrowable) throw new Exception($errMsg);
            $this->showErrorPage();
        }

        $timeEnd   = microtime(true);
        $timeExecTime = $timeEnd - $timeStart;
        $queryLog .= " Execution time: ". sprintf('%.2f sec', $timeExecTime) . "\n";

        // 実行したQuery
        $this->last_query = stripslashes($sth->query);
        // prepare解放
        $sth->free();

        // 実行結果をログに残す
        // Sys, Mngはログに残さない
        if(Utils::getFunction() == FUNCTION_USR){
            log_class::printLog($queryLog, DB_LOG_FILE);
        }

        return $result;
	}


	/**
	 * クエリーを実行する（結果が一つの場合）
	 * @access	public
	 * @param	string	$sql	SQL文字列
	 * @param	mixed	$param	パラメータ配列
	 * @param	string	$file	呼び出し元ファイル名
	 * @param	string	$line	呼び出し元行数
	 * @return	string	結果文字列
	 */
	function getOne($sql, $param = null, $file = "", $line = ""){

        // 実行結果をログに残す
        log_class::printLog($sql, DB_LOG_FILE);

        $this->_sql = $sql;

		if (is_null($param)) {
			$res = $this->_con->extended->getOne($sql);
		} else {
			$res = $this->_con->extended->getOne($sql, $param);
		}
		

		// エラーチェック
		if(MDB2::isError($res)){
			$this->error($res->getMessage().":".$sql);
		}

		return $res;

	}

	/**
	 * リテラルとして安全に使用できるように入力内容を整形する
	 * @access	public
	 * @param	string	$param	文字列
	 * @return	string	整形文字列
	 */
	function quote($param){
		return $this->_con->quote($param);
	}

	/**
	 * 現在の DBMS の規則に基づいて文字列をエスケープする
	 * @access	public
	 * @param	string	$param	文字列
	 * @return	string	整形文字列
	 */
	function escapeSimple($param){

		return $this->_con->escapeSimple($param);

	}

	/**
	 * エラー処理を行う
	 * @access	public
	 * @param	string	$errMsg	エラーメッセージ
//	 * @throws  Exception
	 */
	function error($errMsg){
        // 実行結果をログに残す
        log_class::printLog($errMsg, DB_LOG_FILE);

        $this->showErrorPage();
	}


    /**
     * DBエラーログを生成
     */
    private function getDbError($sth, $sql='', $arrVal=array()){
        $sql = $this->toQueryString($sql, $arrVal);

        $errMsg = $sql."\n";
        $errMsg.= $sth->toString()."\n";
        $errMsg.= Utils::toStringBacktrace($sth->getBackTrace())."\n";
        return $errMsg;
    }


    /**
     * エラーページを表示する
     */
    function showErrorPage(){
        if(DEBUG){
            Error::showSystemError($GLOBALS["msg"]["err_db_system"]."<br>".$errMsg);
        }else{
            Error::showSystemError($GLOBALS["msg"]["err_db_system"]);
        }
        exit;
    }
}
?>
