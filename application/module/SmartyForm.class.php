<?php
// Smarty使用時のフォーム表示クラス
// 
// $Id: SmartyForm.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
//
class SmartyForm {

	// コンボ項目表示関数
	static function setComboValue(&$smarty, $name, $array, $value, $mode = "", $toptext = "", $attribute = "") {
		$smarty->assign($name, SmartyForm::createCombo($name, $array, $value, $mode, $toptext, $attribute));
	}

	// コンボ項目表示関数（配列用）
	static function setComboValueArray(&$smarty, $name, $array, $value, $mode = "", $toptext = "", $key = "", $size = "1") {
		$smarty->assign($name, SmartyForm::createComboArray($name, $array, $value, $mode, $toptext, $key, $size));
	}

	// ラジオボタン、チェックボックス項目表示関数
	static function setRadioChecked(&$smarty, $name, $array, $value, $type = "radio", $mode = "", $brNumber = 0, $attribute = "", $noteArray = "") {
		$smarty->assign($name, SmartyForm::createRadioChecked($name, $array, $value, $type, $mode, $brNumber, $attribute, $noteArray));
	}

	// コンボ項目作成関数
	static function createCombo($name, $array=array(), $value, $mode = "", $toptext = "", $attribute = "") {

		$ret= "";

		if ($mode == "form") {
			$ret .= "<select name=\"$name\"";

			if (!empty($attribute)) {
				foreach ($attribute as $key => $value) {
					if (!empty($value)) {
						$ret .= " ".$key."=\"".$value."\"";
					} else {
						$ret .= " ".$key;
					}
				}
			}

			$ret .= ">\n";

			if ($toptext != "") {
				$selected = "";
				if (!is_array($value)) {
					// 値が配列以外の場合

					if ($value == "")
						$selected = " selected";
				} else {
					// 値が配列の場合

					foreach ($value as $key2 => $value2) {
						if ($value2 == "") {
							$selected = " selected";
							break;
						}
					}
				}
				$ret .= "<option value=\"\"$selected>$toptext</option>\n";
			}

			if(is_array($array)) {
				foreach ($array as $key => $arrValue) {
					$selected = "";
					if (!is_array($value)) {
						// 値が配列以外の場合

						if ($value == $key and $value != "")
							$selected = " selected";
					} else {
						// 値が配列の場合

						foreach ($value as $key2 => $value2) {
							if ($value2 == $key and $value2 != "") {
								$selected = " selected";
								break;
							}
						}
					}

					$ret .= "<option value=\"$key\"$selected>$arrValue</option>\n";
				}
			}

			$ret .= "</select>\n";
		} else {
			if ($toptext != "") {
				if (!is_array($value)) {
					// 値が配列以外の場合

					if ($value == "")
						$ret = $toptext;
					else
						$ret = $array[$value];
				} else {
					// 値が配列の場合

					foreach ($value as $key2 => $value2) {
						if ($value2 == "")
							$ret .= $toptext."<br>";
						else
							$ret .= $array[$value2]."<br>";
					}
				}
			} else {
				if (!is_array($value)) {
					// 値が配列以外の場合

					$ret = $array[$value];
				} else {
					// 値が配列の場合

					foreach ($value as $key2 => $value2) {
						$ret .= $array[$value2]."<br>";
					}
				}
			}
		}
		
		return $ret;
	}

	// コンボ項目作成関数（配列用）
	static function createComboArray($name, $array, $value, $mode = "", $toptext = "", $key = "", $size = "1") {
		$ret= "";

		if ($size != "1") {
			$multiple = " multiple";
		}

		if ($mode == "form") {
			$ret .= "<select name=\"".$name."[".$key."]\" size=\"$size\"$multiple>\n";
			if ($toptext != "") {
				$selected = "";
				if (!is_array($value)) {
					// 値が配列以外の場合

					if ($value == "")
						$selected = " selected";
				} else {
					// 値が配列の場合

					foreach ($value as $key2 => $value2) {
						if ($value2 == "") {
							$selected = " selected";
							break;
						}
					}
				}
				$ret .= "<option value=\"\"$selected>$toptext</option>\n";
			}

			foreach ($array as $key => $arrValue) {
				$selected = "";
				if (!is_array($value)) {
					// 値が配列以外の場合

					if ($value == $key and $value != "")
						$selected = " selected";
				} else {
					// 値が配列の場合

					foreach ($value as $key2 => $value2) {
						if ($value2 == $key and $value2 != "") {
							$selected = " selected";
							break;
						}
					}
				}

				$ret .= "<option value=\"$key\"$selected>$arrValue</option>\n";
			}
			$ret .= "</select>\n";
		} else {
			if ($toptext != "") {
				if (!is_array($value)) {
					// 値が配列以外の場合

					if ($value == "")
						$ret = $toptext;
				} else {
					// 値が配列の場合

					foreach ($value as $key2 => $value2) {
						if ($value2 == "")
							$ret .= $toptext."<br>";
					}
				}
			}

			if (!is_array($value)) {
				// 値が配列以外の場合

				if ($value != "")
					$ret = $array[$value];
			} else {
				// 値が配列の場合

				foreach ($value as $key2 => $value2) {
					if ($value2 != "")
						$ret .= $array[$value2]."<br>";
				}
			}
		}

		return $ret;
	}

	// ラジオボタン、チェックボックス項目作成関数
	static function createRadioChecked($name, $array, $value, $type = "radio", $mode = "", $brNumber = 0, $attribute = "", $noteArray = "") {

		$ret= "";

		// 説明を表示
		if(isset($noteArray[0])) $ret = $noteArray[0];

		if ($mode == "form") {
			$index = 0;
			$number = 1;
			foreach ($array as $key => $arrValue) {

				$index++;

				if ($arrValue != "") {
					$checked = "";
					if (!is_array($value)) {
						// 値が配列以外の場合
						
						if($value !== "" and $value == $key) {
							if(!is_null($value)) {
								$checked = " checked";
							}
						}

						
					} else {
						// 値が配列の場合

						foreach ($value as $key2 => $value2) {
							if ($value2 !== "" and $value2 == $key) {
								if(!is_null($value2)) {
									$checked = " checked";
								}
								break;
							}
						}
					}

					// チェックボックスの場合には、nameの部分を配列にする

					if ($type == "checkbox")
						$ret .= "<input type=\"$type\" name=\"".$name."[]\" value=\"$key\"$checked";
					else
						$ret .= "<input type=\"$type\" name=\"$name\" value=\"$key\"$checked";

					if (!empty($attribute)) {
						foreach ($attribute as $key => $value) {
							if (!empty($value)) {
								$ret .= " ".$key."=\"".$value."\"";
							} else {
								$ret .= " ".$key;
							}
						}
					}

					$ret .= " id=\"".$name."_".$key."\" />  <label for=\"".$name."_".$key."\">$arrValue</label>  \n";

					// 説明を表示
					if(isset($noteArray[$index])) {
						$ret .= $noteArray[$index];
					}

					// BRを入れる位置に来たらBRタグを入れる
					if ($brNumber != 0) {
						if ($brNumber == $number) {
							$ret .= "<br />\n";
							$number = 1;
						} else {
							$number++;
						}
					}
				}
			}
		} else {
			if (!is_array($value)) {
				// 値が配列以外の場合

				if ($value != "")
					$ret = $array[$value];
			} else {
				// 値が配列の場合

				$number = 1;
				foreach ($value as $key2 => $value2) {
					if ($value2 != "") {
						$ret .= $array[$value2]." ";

						// BRを入れる位置に来たらBRタグを入れる

						if ($brNumber != 0) {
							if ($brNumber == $number) {
								$ret .= "<br />\n";
								$number = 1;
							} else {
								$number++;
							}
						}
					}
				}
			}
		}

		return $ret;
	}
}
?>
