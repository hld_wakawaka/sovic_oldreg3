<?php
require_once("DbBase.class.php");

/**
 * DbGeneralクラス
 *
 * DB汎用操作クラス
 *
 * @package	include.dbClass.DbGeneral
 * @access	public
 * @create	2005/04/27
 * @version	$Id: DbGeneral.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
 **/
class DbGeneral extends DbBase {

    protected $objQuery;


    function __construct($isThrowable=false) {
        parent::__construct();
        $this->objQuery = MyDB::getInstance(DB_DSN, $isThrowable);
    }


    function __destruct() {}



    /**
     * @see MyDB.setThrowable
     * @param  boolean $isThrowable
     * @return null
     */
    function setThrowable($isThrowable=false){
        $this->objQuery->setThrowable($isThrowable);
    }



    /**
     * SQLの構築
     * @access  public
     * @param   string  $column     取得カラム
     * @param   string  $table      テーブル名称
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   array   $whereVal   プレースホルダ
     * @param	string  $orderBy	データ取得時並び順文
     * @param	integer $offset     取得開始行数
     * @param	integer $limit      取得行数
     * @return  string  $sql        発行するSQL
     */
    function buildSQL($column, $table, $whereMix="", $whereVal=array(), $orderBy="", $offset=-1, $limit=-1){
        $sql = "SELECT ".$column." FROM ".$table;

        // where句
        $where = $this->buildWhere($whereMix);
        $sql .= $where;

        if(!empty($orderBy)){
            $sql .= " ORDER BY ".$orderBy;
        }

        if($offset > -1){
            $sql .= " OFFSET ".$offset;
        }

        if($limit > -1){
            $sql .= " LIMIT ".$limit;
        }

        return $sql;
    }



    /**
     * WHERE句の構築
     * @access  public
     * @param   mixed   $whereMix   検索条件(string or array)
     * @return  string  $where      where句
     */
    function buildWhere($whereMix=""){
        $where = "";
        if($whereMix){
            if(is_array($whereMix)){
                $where = " WHERE ".implode(" AND ", $whereMix);
            }else{
                $where = " WHERE ".$whereMix;
            }
        }
        return $where;
    }



    /**
     * データの取得処理を行います（一覧取得）
     * @access  public
     * @param   string  $column     データ取得時列名
     * @param   string  $table      データ取得時FROM
     * @param   mixed   $whereMix   検索条件（配列の場合と文字列で処理方法が変わる）
     * @param   string  $orderBy    データ取得時並び順文
     * @param   string  $offset     取得開始行数
     * @param   string  $limit      取得行数
     * @param   string  $file       呼び出し元ファイル名
     * @param   string  $line       呼び出し元行数
     * @return  mixed   取得成功時はデータ一覧配列、取得失敗時は、false
     */
    function getListData($column, $table, $whereMix="", $orderBy="", $offset=-1, $limit=-1) {
        $sql1   = $this->buildSQL($column, $table, $whereMix, array(), $orderBy, $offset, $limit);
        $sql2   = $this->buildSQL($column, $table, $whereMix, array(), $orderBy);

        // 実際のデータ取得処理は、親クラスに任せる
        return parent::getListData($sql1, $sql2, $offset, $limit);
    }



    /**
     * データの取得処理を行います
     * @access  public
     * @param   string  $column     データ取得時列名
     * @param   string  $table      データ取得時FROM
     * @param   mixed   $whereMix   検索条件（配列の場合と文字列で処理方法が変わる）
     * @param   array   $whereVal   プレースホルダ
     * @return  mixed   取得成功時はデータ格納連想配列、取得失敗時は、false
     */
    function getRow($column, $table, $whereMix="", $whereVal=array(), $fetchMode=MDB2_FETCHMODE_ASSOC){
        $sql = $this->buildSQL($column, $table, $whereMix, $whereVal);

        // 変数初期化#取得結果
        $arrData     = false;
        $result      = $this->objQuery->query($sql, $whereVal);
        $this->_rows = $result->numRows();

        if(!empty($this->_rows)){
            // 1行取得
            $arrData = $result->fetchRow($fetchMode);
        }

        return $arrData;
    }


    /**
     * クエリを実行し、全ての行を返す
     *
     * @param  string  $sql       SQL クエリ
     * @param  array   $arrVal    プリペアドステートメントの実行時に使用される配列。配列の要素数は、クエリ内のプレースホルダの数と同じでなければなりません。
     * @param  integer $fetchmode 使用するフェッチモード。デフォルトは DB_FETCHMODE_ASSOC。
     * @return array   データを含む2次元配列。失敗した場合に 0 または DB_Error オブジェクトを返します。
     */
    public function query($sql, $whereVal = array(), $fetchmode = MDB2_FETCHMODE_ASSOC){
        $result = $this->objQuery->query($sql, $whereVal);
        $data = $result->fetchAll($fetchmode);
        return $data;
    }


	function begin() {
        $this->objQuery->begin();
		// TODO ログに残す
	}

	function commit() {
		$this->objQuery->commit();
		// TODO ログに残す
	}

	function rollback() {
		$this->objQuery->rollback();
		// TODO ログに残す
	}



    /**
     * データの追加処理を行います
     * @access   public
     * @param    string   $table    テーブル名称
     * @param    array    $param    カラム名称と値の連想配列
     * @return
     */
    function insert($table, $param) {

        // SQL生成
        $sql            = "";
        $column            = array();
        $columnValue    = array();

        $sql .= "INSERT INTO ".$table;

        foreach ($param as $key => $value) {
            $value = $this->objQuery->quote($value);

            if ($value === "'NULL'") {
                $value = "NULL";
            }
            
            if($value === "'NOW'") {
                $value = "now()";
            }

            //NONの場合は、登録に入れない
            if($value === "'NON'") {
            } else {
                $column[]        = $key;
                $columnValue[]    = $value;
            }
        }
        $sql .= " (".implode(",", $column).")";
        $sql .= " VALUES(".implode(",", $columnValue).")";

        return $this->objQuery->query($sql, NULL, NULL, MDB2_PREPARE_MANIP);
    }



    /**
     * データの更新処理を行います
     * @access  public
     * @param   string  $table      テーブル名称
     * @param   array   $param      更新内容
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   array   $whereVal   プレースホルダ
     * @return
     */
    function update($table, $param, $whereMix="", $whereVal=array()){
        // SQL生成
        $sql    = "";
        $where  = "";
        $column = array();

        $sql .= "UPDATE ".$table;

        foreach ($param as $key => $value) {
            $value = $this->objQuery->quote($value);

            if($value === "'NULL'"){
                $value = "NULL";
            }
            if($value === "'NOW'"){
                $value = "now()";
            }

            if($value === "'NON'"){
            }else{
                $column[] = $key." = ".$value;
            }
        }

        $sql .= " SET ".implode(",", $column);

        // where句
        $where = $this->buildWhere($whereMix);
        $sql .= $where;

        return $this->objQuery->query($sql, $whereVal, NULL, MDB2_PREPARE_MANIP);
    }



    /**
     * データの更新処理を行います
     * @access  public
     * @param   string  $table      テーブル名称
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   array   $whereVal   プレースホルダ
     * @return  boolean $rows       実行結果
     */
    function delete($table, $whereMix="", $whereVal=array()) {
        $sql = 'DELETE FROM ' . $table;
        // where句
        $where = $this->buildWhere($whereMix);
        $sql .= $where;

        $rows = $this->objQuery->query($sql, $whereVal, NULL, MDB2_PREPARE_MANIP);
        return $rows;
    }



    /**
     * 指定したカラムを取得する
     * @access  public
     * @param   string  $table      テーブル名称
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   array   $whereVal   プレースホルダ
     * @return  mixed   SQL の実行結果
     */
    function get($column, $table, $whereMix="", $whereVal=array()) {
        $sql = $this->buildSQL($column, $table, $whereMix, $whereVal);
        $result = $this->objQuery->query($sql, $whereVal);
        return $result->fetchOne();
    }



    /**
     * クエリを実行し全行取得する
     * @access  public
     * @param   string  $table      テーブル名称
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   array   $whereVal   プレースホルダ
     * @return  mixed   SQL の実行結果
     */
    function getAll($column, $table, $whereMix="", $whereVal=array(), $fetchmode = MDB2_FETCHMODE_ASSOC) {
        $sql = $this->buildSQL($column, $table, $whereMix, $whereVal);
        $result = $this->objQuery->query($sql, $whereVal);
        return $result->fetchAll($fetchmode);
    }



    /**
     * 値をクォートする
     *
     * @see MDB2::quote()
     * @param  string $value クォートを行う文字列
     * @return string        クォートされた文字列
     */
    function quote($value) {
        return $this->objQuery->quote($value);
    }



    /**
     * レコードの存在チェック
     * 
     * @param   string  $table      対象テーブル
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   array   $whereVal   プレースホルダ
     * @return  boolean true:存在
     * 
     */
    function exists($table, $whereMix="", $whereVal=array()){
        $sql = $this->buildSQL("*", $table, $whereMix, $whereVal);
        $sql = sprintf('SELECT CASE WHEN EXISTS(%s) THEN 1 ELSE 0 END', $sql);
        $result = $this->objQuery->query($sql, $whereVal);
        return (bool)$result->fetchOne();
    }

}
?>
