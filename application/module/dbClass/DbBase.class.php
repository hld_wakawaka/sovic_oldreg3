<?php
require_once("PEAR.php");

/**
 * DbBaseクラス
 *
 * DbBaseクラス
 *
 * @package	include.dbClass.DbBase
 * @access	public
 * @create	2004/10/19
 * @version	$Id: DbBase.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
 **/
class DbBase extends PEAR {

	var $_maxRows			= 0;		// １ページの最大表示件数
	var $_rowCount			= 0;		// ページ内の取得行数
	var $_startDisplayRows	= 0;		// ページ内表示開始行数
	var $_endDisplayRows	= 0;		// ページ内表示終了行数

	var $_rows				= 0;		// 最大行数
	var $_isPrevious		= false;	// 前ページ有無
	var $_isNext			= false;	// 次ページ有無
	var $_isPreviousRows	= 0;		// 前ページ開始行数
	var $_isNextRows		= 0;		// 次ページ開始行数

	var $_errorText		= "";



    function __construct() {
        parent::PEAR();
    }


    function __destruct() {}



	/**
	 * データの取得処理を行います（一覧取得）
	 * @access	public
	 * @param	string	$sql1		データ取得時SQL
	 * @param	string	$sql2		データ総件数取得時SQL
	 * @param	string	$offset		取得開始行数
	 * @param	string	$limit		取得行数
	 * @return	mixed	取得成功時はデータ一覧配列、取得失敗時は、false
	 */
	function getListData($sql1, $sql2, $offset = -1, $limit = -1) {

		// 変数初期化
		$listData				= false;

		$this->_maxRows			= $limit;

		$this->_rows			= 0;
		$this->_isPrevious		= false;
		$this->_isNext			= false;
		$this->_isPreviousRows	= 0;
		$this->_isNextRows		= 0;

		$res	= $this->objQuery->query($sql1);
		$rows	= $res->numRows();

		if (!empty($rows)) {
			$listData = array();

			for ($curRow = 0; $curRow < $rows; $curRow++) {
				// 1行取得
				$data = $res->fetchRow(DB_FETCHMODE_ASSOC, $curRow);
				// リスト保存用クラス変数に追加
				array_push($listData, $data);
			}

			// 総件数を取得
			$res = $this->objQuery->query($sql2);
			$this->_rows = $res->numRows();

			// 画面遷移がある場合には、次、前リンクの有無を判定
			if ($limit > -1) {
				if ($offset > 0) {
					$this->_isPrevious		= true;
					$this->_isPreviousRows	= $offset - $limit;
				}

				if ($this->_rows > $offset + $limit) {
					$this->_isNext			= true;
					$this->_isNextRows		= $offset + $limit;
				}
			}

			$this->_rowCount			= count($listData);
			$this->_startDisplayRows	= $offset + 1;
			$this->_endDisplayRows		= $offset + $this->_rowCount;

		}

		return $listData;
	}



    /**
     * データの取得処理を行います
     * @deprecated
     * @access  public
     * @param   string  $column     データ取得時列名
     * @param   string  $table      データ取得時FROM
     * @param   mixed   $whereMix   検索条件（配列の場合と文字列で処理方法が変わる）
     * @param   string  $file       未使用の引数
     * @param   string  $line       未使用の引数
     * @return  mixed   取得成功時はデータ格納連想配列、取得失敗時は、false
     */
    function getData($column, $table, $whereMix="", $file="", $line=""){
        return $this->getRow($column, $table, $whereMix, array(), MDB2_FETCHMODE_ASSOC);
    }



    /**
     * 指定したカラムを取得する
     * @deprecation
     * @access  public
     * @param   string  $column     データ取得時列名
     * @param   string  $table      テーブル名称
     * @param   mixed   $whereMix   検索条件(string or array)
     * @param   string  $file       未使用の引数
     * @param   string  $line       未使用の引数
     * @return  mixed   SQL の実行結果
     */
    function _getOne($column, $table, $whereMix="", $file="", $line=""){
        return $this->get($column, $table, $whereMix, array());
    }

}
?>
