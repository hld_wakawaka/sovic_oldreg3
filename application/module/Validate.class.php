<?php
// 汎用入力チェック関数クラス
// 
// $Id: Validate.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
//

// 利用文字エンコード
define("USE_ENC", "UTF-8");

class Validate {

	var $_err = array();

	function check($init, $array) {
	
		if(!is_array($init)){ return; }

		foreach($init as $val) {
		
			$key		= $val[0];	//変数名
			$name		= $val[1];	//項目名(マルチバイト)
			$len		= $val[2];	//長さ制限
			$check		= $val[3];	//チェック内容
			
			if(!isset($array[$key])) $array[$key] = "";
		
			foreach($check as $func) {
				
				switch($func) {
					
					
				
					case "NULL":

						if(!$this->isNull($array[$key])) {
							$this->addErr(sprintf($GLOBALS["msg"]["err_require_input"], $name), $key);
						}
						break;					
					case "LEN":
						if($len[0] > 0) {
							if(!$this->isLenMinMax($array[$key], $len[0], $len[1])) {
					    		$this->addErr(sprintf($GLOBALS["msg"]["err_strlen_between"], $name, $len[0], $len[1]), $key);
							}
							
						} else {
							if(!$this->isLen($array[$key], $len[1])) {
					    		$this->addErr(sprintf($GLOBALS["msg"]["err_strlen_max"], $name, $len[1]), $key);
							}
						}
						break;
					case "MLEN":
						if($len[0] > 0) {
							if(!$this->isMLenMinMax($array[$key], $len[0], $len[1])) {
					    		$this->addErr(sprintf($GLOBALS["msg"]["err_strlen_between"], $name, floor($len[0]/2), floor($len[1]/2) ), $key);
							}
							
						} else {
							if(!$this->isMLen($array[$key], $len[1])) {
					    		$this->addErr(sprintf($GLOBALS["msg"]["err_strlen_max"], $name, floor($len[1]/2)), $key);
							}
						}
						break;
					case "SELECT":
						if(!$this->isNull($array[$key])) {
							$this->addErr(sprintf($GLOBALS["msg"]["err_require_select"], $name), $key);
						}
						break;
					case "MAIL":
						if(!$this->isMail($array[$key])) {
							$this->addErr(sprintf($GLOBALS["msg"]["err_mail"], $name), $key);
						}
						break;
						
					case "NUMERIC":
						if(!$this->isNumeric($array[$key])) {
							$this->addErr(sprintf($GLOBALS["msg"]["err_numeric"], $name), $key);
						}
						break;					
					case "BYTE":
						if(!$this->isByte($array[$key], $len[1])){
							$this->addErr(sprintf($GLOBALS["msg"]["err_byte"], $name, $len[1]), $key);
						}
						break;
					case "MOJICNT":
						if(!$this->isMOJI($array[$key], $len[1])){
							$this->addErr(sprintf($GLOBALS["msg"]["err_strlen_max"], $name, $len[1]), $key);
						}						
						break;
					case "WORDCHK":
						if(!$this->isWordCnt($array[$key], $len[1])){
							$this->addErr(sprintf($GLOBALS["msg"]["err_word_cnt"], $name, $len[1]), $key);
						}						
						break;					
					case "ZEN":
						if(!$this->isZen($array[$key])){
							$this->addErr(sprintf($GLOBALS["msg"]["err_zen"], $name), $key);
						}					
					
						break;
					case "HAN":
						if(!$this->isAlphaNumericMark($array[$key])){
							$this->addErr(sprintf($GLOBALS["msg"]["err_han_alphanumericmark"], $name), $key);
						}						
						break;

					// 郵便番号のフォーマットチェック
					case "ZIP":
						if(!$this->isZip($array[$key])){
							$this->addErr(sprintf("%sはハイフン区切りで入力してください。", $name), $key);
						}
						break;

					// 電話番号のフォーマットチェック
					case "TEL":
						if(!$this->isPhone($array[$key])){
							$this->addErr(sprintf("%sはハイフン区切りで入力してください。", $name), $key);
						}
						break;

					case "TOKUSYU":
						if(!$this->isTokusyu($array[$key])){
							$this->addErr(sprintf($GLOBALS["msg"]["err_tokusyu"], $name), $key);
						}					
						break;																							
					default:
						break;				
				
				}
			}	//foreach $check
		}	//foreach init
	}	//function
	
	function addErr($str, $key="") {
		$this->_err[$key] = $str;
	}
	

	// エラーの並べ替え
	function sortErr($init) {
		
		if(!count($this->_err) > 0) return;
		
		$wk_err = array();
		foreach($init as $val) {
			if(isset($this->_err[$val[0]])) {
				$wk_err[$val[0]] = $this->_err[$val[0]];
				unset($this->_err[$val[0]]);
			}
		}
		
		$this->_err = array_merge((array)$wk_err, (array)$this->_err);
		
		return;
		
	}

	function isNull($str) {
		
		if(is_array($str)) {
			if(count($str) <= 0) {
				return false;
			}
            // 配列を再帰的にチェック
            if(is_array($str)){
                foreach($str as $_str){
                    $func = __FUNCTION__;
                    if(!self::$func($_str)) return false;
                }
            }
		} else {
			if($str == "") {
				return false;
			}
		}
		return true;
		
	}

	// 全角カタカナ以外はエラー
	function isZenKana($str) {
	
		if($str == "") return true;

		$str = @mbereg_replace("　", "", $str);
		$str = @mbereg_replace(" ", "", $str);
		if(!preg_match("/^[ァ-ヾ]+$/u",$str)){
			return false;
		}
			
		else{
			return true;
		}
			
	}

	//	全角ひらがな、全角カタカナ、半角英数チェック関数
	//
	//	全角ひらがな、全角カタカナ、半角英数以外の文字が含まれていた場合は、「false」
	function isAlphaNumericZenHiraZenKana($str) {
	
		if($str == "") return true;

		if (!@mberegi("^[A-Za-z0-9ぁ-んァ-ヶー]+$", $str))
			return false;
		else
			return true;
	}

	//	半角英数チェック関数
	//
	//	半角英数以外の文字が含まれていた場合は、「false」
	function isAlphaNumeric($str) {
	
		if($str == "") return true;

		if (!@ereg("^[A-Za-z0-9]+$", $str))
			return false;
		else
			return true;
	}

	//	半角英数記号チェック関数
	//
	//	半角英数記号以外の文字が含まれていた場合は、「false」
	function isAlphaNumericMark($str) {
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str)) return false;
            }
            return true;
        }
	
		if($str == "") return true;

		$tmp = Validate::replaceMark($str);

		if (!@ereg("^[A-Za-z0-9]+$", $tmp) and $tmp != "")
			return false;
		else
			return true;
	}

	//	正規表現でうまくヒットできない文字を省く
	//
	function replaceMark($str) {
	
		if($str == "") return true;

		$tmp = $str;
		$tmp = str_replace("~", "", $tmp);
		$tmp = str_replace("`", "", $tmp);
		$tmp = str_replace("[", "", $tmp);
		$tmp = str_replace("]", "", $tmp);
		$tmp = str_replace(";", "", $tmp);
		$tmp = str_replace(",", "", $tmp);
		$tmp = str_replace("<", "", $tmp);
		$tmp = str_replace(">", "", $tmp);
		$tmp = str_replace("_", "", $tmp);
		$tmp = str_replace("\\", "", $tmp);
		$tmp = str_replace(".", "", $tmp);
		$tmp = str_replace("*", "", $tmp);
		$tmp = str_replace("+", "", $tmp);
		$tmp = str_replace("?", "", $tmp);
		$tmp = str_replace("^", "", $tmp);
		$tmp = str_replace("$", "", $tmp);
		$tmp = str_replace("|", "", $tmp);
		$tmp = str_replace("(", "", $tmp);
		$tmp = str_replace(")", "", $tmp);
		$tmp = str_replace("{", "", $tmp);
		$tmp = str_replace("}", "", $tmp);
		$tmp = str_replace("@", "", $tmp);
		$tmp = str_replace(":", "", $tmp);
		$tmp = str_replace("/", "", $tmp);
		$tmp = str_replace("!", "", $tmp);
		$tmp = str_replace("\"", "", $tmp);
		$tmp = str_replace("#", "", $tmp);
		$tmp = str_replace("%", "", $tmp);
		$tmp = str_replace("&", "", $tmp);
		$tmp = str_replace("'", "", $tmp);
		$tmp = str_replace("=", "", $tmp);
		$tmp = str_replace("-", "", $tmp);

		$tmp = str_replace(" ", "", $tmp);
		$tmp = str_replace("\r", "", $tmp);
		$tmp = str_replace("\n", "", $tmp);
		$tmp = str_replace("\t", "", $tmp);

		return $tmp;
	}

	//	半角数チェック関数
	//
	//	半角数以外の文字が含まれていた場合は、「false」
	function isNumeric($str) {
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str)) return false;
            }
            return true;
        }
	
		if($str == "") return true;

		if (!@ereg("^[0-9]+$", $str))
			return false;
		else
			return true;
	}

	function isInt($str) {
	
		if($str == "") return true;
		
		if(!is_numeric($str)) {
			return false;
		} elseif(@ereg("\.", $str)) {
			return false;
		}
		return true;
	}
	
	function isFloat($str) {
	
		if($str == "") return true;
		
		$pattern = "^([0-9]+).([0-9]+)$";
		$pattern2 = "^([0-9]+)$";
		
		if (!@ereg($pattern, $str, $date) and !@ereg($pattern2, $str, $date))
			return false;

		return true;
		
		
	}

	//	半角数記号チェック関数
	//
	//	半角数記号以外の文字が含まれていた場合は、「false」
	function isNumericMark($str) {
	
		if($str == "") return true;

		$tmp = Validate::replaceMark($str);

		if (!@ereg("^[0-9]+$", $tmp) and $tmp != "")
			return false;
		else
			return true;
	}

	//	日付形式チェック関数
	//
	//	日付形式の場合は、「false」
	function isDate($str) {
	
		if($str == "") return true;

		if (!@ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $str, $date) and !@ereg("([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})", $str, $date))
			return false;
		elseif (!checkdate($date[2], $date[3], $date[1]))
			return false;

		return true;
	}

	//	日付時刻チェック関数
	//
	//	日付形式の場合は、「false」
	function isDateTime($str, $format=array("Y/m/d H:i:s", "Y-m-d H:i:s")) {
	
		if($str == "") return true;
		if(!is_array($format))  return true;
		if(count($format) == 0) return true;

        foreach($format as $_key => $_format){
    		if (date($format, strtotime($str)) != $str) {
    			return false;
    		}
        }

		return true;
	}

	// $strがメールアドレスとして正当かチェックを行う
	function isMail($str) {
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str)) return false;
            }
            return true;
        }
	
		if($str == "") return true;
		
		if (!@ereg("^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$", $str)) {
			return false;
		}

		return true;
	}

	// 郵便番号形式チェック関数(000-0000)
	function isZip($zip = "") {
	
		if($zip == "") return true;

		if ( !@ereg( "^[0-9]{3}-[0-9]{4}$", $zip )) {
			return false;
		}
		return true;
	}

	// $strがリンク先として正当かチェックを行う
	function isLink($str) {
	
		if($str == "") return true;
		
		if (!@ereg("([[:alnum:]\+\$\;\?\.%,!#~*/:@&=_-]+)", $str))
			return false;
		else
			return true;
	}

	// $strがリンク先として正当かチェックを行う
	function isUrl($str) {
	
		if($str == "") return true;
		
		if (!@ereg("(https?|ftp|news)(://[[:alnum:]\+\$\;\?\.%,!#~*/:@&=_-]+)", $str))
			return false;
		else
			return true;
	}

	// 日付が正しいかを判断して結果を返します
    function isValidDate($aStr, $aSepList="-/ .") {
	
		if($aStr == "") return true;
		
      if( @ereg("^([0-9]+)[$aSepList]([0-9]+)[$aSepList]([0-9]+)$", $aStr, $m) ) {
        return checkdate($m[2], $m[3], $m[1]);
      }
      return false;
    }
	
	//日付（YYYY/MM形式）が正しいか判断して結果を返します
    function isValidYearMonth($aStr) {
	
		if($aStr == "") return true;
		
      if( @ereg("^([0-9]+)/([0-9]+)$", $aStr, $m) ) {
        return checkdate($m[2], 1, $m[1]);
      }
      return false;
    }

	// 電話番号形式チェック関数(0000000000)国内のみ
	function isPhone( $phone ) {
	
		if($phone == "") return true;

		if ( !@ereg( "^0[0-9]{1,5}-[0-9]{0,4}-[0-9]{3,4}$", $phone ) )
//		if ( !ereg( "^0[0-9]*$", $phone ) )
			return false;
		else
			return true;

	}
	
	//長さのチェック(～以内)
	function isLen($str, $max) {
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str, $max)) return false;
            }
            return true;
        }
	
		if($str == "") return true;
		
		if(strlen($str) > $max) {
			return false;
		}
		return true;
	}
	
	//長さのチェック(～以内)
	function isMLen($str, $max) {
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str, $max)) return false;
            }
            return true;
        }
	
		if($str == "") return true;
		
		$str = mb_convert_encoding($str, "SJIS", USE_ENC);
		
		if(strlen($str) > $max) {
			return false;
		}
		return true;
	}
	
	//長さのチェック（～以上～以内）
	function isLenMinMax($str, $min, $max) {
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str, $min, $max)) return false;
            }
            return true;
        }
	
		if($str == "") return true;
		
		$str = mb_convert_encoding($str, "SJIS", USE_ENC);
		
		if(strlen($str) > $max || strlen($str) < $min) {
			return false;
		}
		return true;
	}
	
	/**
	 * 英数と特定の記号チェック
	 * @param string $str チェック対象文字列
	 * @param array  $arr 許可する配列
     * 
     * @return boolean
	 */
	function isAlphaNumericMarkCustom($str, $arr=array("_","-")) {
	
		if($str == "") return true;
		
		foreach($arr as $val) {
			$str = str_replace($val, "", $str);
		}
		
		if (!@ereg("^[A-Za-z0-9]+$", $str) and $str != "")
			return false;
		else
			return true;
		
	}
	
	function isMatching($str, $str2) {
	
		if($str == "" || $str2 == "") {
			return true;
		}
		
		if($str == $str2) {
			return true;
		}
		
		return false;
	}

	/**
	 * バイト数チェック
	 */
	function isByte($str, $max){
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str, $max)) return false;
            }
            return true;
        }

		if($str == "") return true;
		
		$str = mb_convert_encoding($str, "SJIS", USE_ENC);
		
		if(strlen($str) > $max) {
			return false;
		}
		return true;		
	}
	
	/**
	 * 文字数チェック
	 * 	バイト数ではなく、文字の数をチェックする
	 */
	function isMOJI($str, $max){
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str, $max)) return false;
            }
            return true;
        }
		
		if($str == "") return true;
		
		$str = mb_convert_encoding($str, "SJIS", USE_ENC);
		$str = str_replace(array("\r", "\n", "\r\n"), "", $str);

		if(mb_strlen(trim($str), "SJIS") > $max) {
			return false;
		}
		return true;
		
	}
	
	/**
	 * 単語数チェック
	 */
	function isWordCnt($str, $max){
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str, $max)) return false;
            }
            return true;
        }
		
		if($str == "") return true;
		
		//全角スペースが含まれる場合、半角スペースに統一
		$str = str_replace(array("　", "\n", "\r", "\r\n"), " ", $str);
		
		$wa_word = explode(" ", $str);
		
		$wa_chk_word = array();
		
		foreach($wa_word as $data){
			
			if($data != ""){
				array_push($wa_chk_word, $data);
			}
		}
		
		if(count($wa_chk_word) > $max){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 全角チェック
	 * 	全角文字で構成されているか？
	 */	
	function isZen($str){
        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str)) return false;
            }
            return true;
        }

		if($str == "") return true;
		
		$str = mb_convert_encoding($str, "SJIS", USE_ENC);
		
		$strcnt = mb_strlen($str,"SJIS" ) * 2;
		$strlen = strlen($str);
		
		if($strlen <> $strcnt ){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 特殊文字チェック
	 * 	特殊文字が含まれる場合はエラー
	 */
	function isTokusyu($str){

        // 配列を再帰的にチェック
        if(is_array($str)){
            foreach($str as $_str){
                $func = __FUNCTION__;
                if(!self::$func($_str)) return false;
            }
            return true;
        }

		if($str == "") return true;
		//$str = mb_convert_encoding($str, "SJIS", USE_ENC);
/*		
		if(ereg("/[①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹ㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼≒≡∫∮∑√⊥∠∟⊿∵∩∪￢￤＇＂]/",$str)){
			return false;
		}
*/		
//		$ngArray = array('①','②','③','④','⑤','⑥','⑦','⑧','⑨','⑩','⑪','⑫','⑬','⑭','⑮','⑯','⑰','⑱','⑲','⑳','Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ','ⅰ','ⅱ','ⅲ','ⅳ','ⅴ','ⅵ','ⅶ','ⅷ','ⅸ','ⅹ','㍉','㌔','㌢','㍍','㌘','㌧','㌃','㌶','㍑','㍗','㌍','㌦','㌣','㌫','㍊','㌻','㎜','㎝','㎞','㎎','㎏','㏄','㎡','㍻','〝','〟','№','㏍','℡','㊤','㊥','㊦','㊧','㊨','㈱','㈲','㈹','㍾','㍽','㍼','≒','≡','∫','∮','∑','√','⊥','∠','∟','⊿','∵','∩','∪','￢','￤','＇','＂'); 
        // 2013.08.08.19:00
		$ngArray = array('①','②','③','④','⑤','⑥','⑦','⑧','⑨','⑩','⑪','⑫','⑬','⑭','⑮','⑯','⑰','⑱','⑲','⑳','Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ','ⅰ','ⅱ','ⅲ','ⅳ','ⅴ','ⅵ','ⅶ','ⅷ','ⅸ','ⅹ','㍉','㌔','㌢','㍍','㌘','㌧','㌃','㌶','㍑','㍗','㌍','㌦','㌣','㌫','㍊','㌻','㎜','㎝','㎞','㎎','㎏','㏄','㎡','㍻','〝','〟','№','㏍','℡','㊤','㊥','㊦','㊧','㊨','㈱','㈲','㈹','㍾','㍽','㍼','∮','∑','∟','⊿','￤','＇','＂'); 

		foreach ($ngArray as $v) {
			
			$pp = strpos($str, $v); 
			if ($pp === false){
				continue; 
			}
			else{
				return false;
			}
		}

		
		return true;
	} 


	/*
	 * ファイルの拡張子チェック
	 * @param array $fileInfo $_FILE[hoge]
	 * @param array $arrExt   array(jpg, jpeg)
	 */
	function checkExt($fileInfo, $arrExt=""){

		if($fileInfo['error'] == 4) return true;

		$ext = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));

		$res = false;
		foreach($arrExt as $checkExt){
			if($checkExt == $ext){
				$res = true;
			}
		}
		return $res;
	}


    /**
     * HTMLタグを除去する
     *
     * @param array  $data   除去対象配列
     * @param string $ignore 除去しないタグ
     *
     */
    function array_stripTags(&$data, $ignore=""){

        if(is_array($data) && count($data) > 0){
            foreach($data as &$value){
                $value = strip_tags($value, $ignore);
            }
        }
    }


    /**
     * 配列のどれかの要素に値がセットされているかチェック
     * 
     * @param  array   $arrForm 入力情報
     * @param  array   $keys    チェック対象のキー配列
     * @return boolean $which   $keysのうちどれか入力している：true
     * 
     * */ 
    public function isInputwhich($arrForm, $keys){

        // 初期値：すべてnull
        $which = false;
        foreach($keys as $key){
            $value = $arrForm[$key];
            if($this->isNull($value)){
                $which = true;
                break;
            }
        }

        return $which;
    }


    /**
     * 配列の全ての要素を入力しているチェック
     * 
     * @param  array   $arrForm 入力情報
     * @param  array   $keys    チェック対象のキー配列
     * @return boolean $all     $keysを全て入力している：true
     * 
     * */
    public function isInputAll($arrForm, $keys){

        // 初期値：すべて入力[前提]
        $all = true;
        foreach($keys as $key){
            $value = $arrForm[$key];
            if(!$this->isNull($value)){
                $all = false;
                break;
            }
        }

        return $all;
    }


    /**
     * 日付1と日付2の妥当性をチェックする
     * 
     * @param  string  $date1 日付1(yyyy-mm-dd h:i:s)
     * @param  string  $date2 日付2(yyyy-mm-dd h:i:s)
     * @param  integer $same 同じ場合も可とする場合1、可としない場合2
     * @return boolean
     */
    public function isDateAgo($date1, $date2, $same=true){
        if($same){
            if(strtotime($date1) <= strtotime($date2)){
                return true;
            }
        }else{
            if(strtotime($date1) < strtotime($date2)){
                return true;
            }
        }
        
        return false;
    }


}
?>
