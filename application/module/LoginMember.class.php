<?php
require_once("PEAR.php");
require_once(MODULE_DIR."custom/LoginBase.class.php");

/**
 * LoginMemberクラス
 *
 * ログイン管理クラス
 *
 * @package	include.LoginMember
 * @access	public
 **/

 // テーブル名
define("MEMBER_TABLE", "v_user");
 // ログインID項目名
define("MEMBER_ID_FIELD_NAME", "admin_login_id");
 // ログインIDはメールアドレスの場合
define("IF_MEMBER_ID_EMAIL", false);
 // パスワード項目名
define("MEMBER_PW_FIELD_NAME", "admin_password");
 // 削除ステータス
//define("MEMBER_NOT_DEL_WHERE", "temp = 0");
define("MEMBER_NOT_DEL_WHERE", "");

class LoginMember extends LoginBase {

	var $_memberData = array();

	/**
	 * コンストラクタ
	 * @access	public
	 */
	function LoginMember() {
		parent::PEAR();
	}

	/**
	 * デストラクタ
	 * @access	public
	 */
	function _LoginMember() {
	}

//	PHP5の場合のコンストラクタ
//	function __construct() {
//	}

//	PHP5の場合のデストラクタ
//	function __destruct() {
//	}

	/**
	 * ログインチェックを行います
	 * @access	public
	 */
	static function checkLogin($url = LOGIN_ROOT) {

		if (!$GLOBALS["session"]->getVar("isLogin")) {
			return false;
		}

		return true;
	}

	/**
	 * ログインチェックを行います
	 * @access	public
	 */
	static function checkLoginRidirect($url = MNG_LOGIN_PAGE, $isReturnCurrentPage = true) {

        // Basic認証のワンタイムログインは1度だけ許可する
        $isBasic = $GLOBALS["session"]->getVar("isBasic");
        if($isBasic === true){
            $GLOBALS["session"]->unsetVar("isBasic");
            return true;
        }

        // ログインチェック
        try{
            if(!$GLOBALS["session"]->getVar("isLogin")) throw new Exception("Not login.");

            // Usr考慮
            parse_str($_SERVER['QUERY_STRING'], $output);
            extract($output);
            if(isset($form_id) && strlen($form_id) > 0){
                $form_id       = $GLOBALS["form"]->formData['form_id']; // アクセスしたフォーム
                $login_form_id = $GLOBALS["userData"]['form_id'];       // ログインしたフォーム
                if($form_id != $login_form_id) throw new Exception("Access denied.");
            }


            // ログインユーザ取得
            $login_id = $GLOBALS["userData"]['admin_login_id'];
            $mail     = $GLOBALS["userData"]['form_mail'];
            $memberData = parent::getLoginObjectMng($login_id, $mail);
            if(!$memberData) throw new Exception("Not found object of Mng.");


            // 重複ログインチェック
            if($memberData["access_session"] != "" && $memberData["access_session"] != session_id()){
                // 一定時間内の場合は他のユーザが操作中とみなす
                if(!parent::isInterval('access_time')) throw new Exception($GLOBALS["msg"]["account_same"]);
            }

        }catch(Exception $e){
            if ($isReturnCurrentPage) {
                $url .= "?return=".rawurlencode($_SERVER["REQUEST_URI"]);
            }

            // 接続セッション情報、ログインエラー回数、ログイン実行日時をクリア
            parent::clearMng();

            header("location: ".$url);
            exit;
        }

        // 最終アクセス日時更新
        parent::updateMng();
        return true;
	}

	/**
	 * ログイン処理を行います
	 * @access	public
	 * @param	string	$login_id
	 * @param	string	$password
	 * @return	string	エラーメッセージ
	 */
	function doLogin($login_id, $password) {


		$errMsg = "";

        //ログ出力用
        $mode = "doLogin";
        $wa_log = "";

		// 入力チェック
		if (empty($login_id)) {
			if(!IF_MEMBER_ID_EMAIL) {
				$errMsg[] = sprintf(ERR_REQUIRE_INPUT_PARAM, "ユーザーID");
			} else {
				$errMsg[] = sprintf(ERR_REQUIRE_INPUT_PARAM, "メールアドレス");
			}
		}
		if (empty($password)) {
			$errMsg[] = sprintf(ERR_REQUIRE_INPUT_PARAM, "パスワード");
		}

		if (empty($errMsg)) {
			$dbGeneral = new DbGeneral();

            // ログインユーザを取得
			$memberData = parent::getLoginObjectMng($login_id, $mail);

			if (!$memberData) {
				$errMsg[] = ERR_NOT_MEMBER_ID;


			} else {

                //-----------------------------
                //パスワード誤り
                //-----------------------------
                if ($memberData[MEMBER_PW_FIELD_NAME] != md5($password.PASS_PHRASE)) {
                    $msg = ERR_MISTAKE_PASSWORD;

                    // 一定時間内にN回目失敗した場合は、アカウントロック
                    // パスワード間違いのカウントアップ
                    parent::countupMng();

                    // 一定期間を過ぎている場合
                    if(parent::isInterval('login_try_date')){
                        $GLOBALS["log"]->write_log($mode,"login error count reset----login_id:".$login_id."|count:".$param["login_err_count"]);

                    // 一定期間内
                    }else{
                        $GLOBALS["log"]->write_log($mode,"login error count up----login_id:".$login_id."|count:".$param["login_err_count"]);

                        // 規定回数オーバーの場合 ロック
                        if(parent::isCountOver()){
                            $msg = ERR_ACCOUNT_LOCK;
                            $GLOBALS["log"]->write_log($mode,"login lock----login_id:".$login_id);
                        }
                    }

                    // パスワード違いエラー
                    $errMsg[] = $msg;
                }
            }
        }


        unset($param);

		// ログイン成功
		if (empty($errMsg)) {
            //アカウントロックされている場合
            if($memberData["lock_flg"] == 1){
                // 一定期間を過ぎている場合は、ロック解除
                if(parent::isInterval('lock_date')){
                    parent::unlockMng();
                    $GLOBALS["log"]->write_log($mode,"login lock cancellation ----login_id:".$login_id);

                // 一定期間内
                }else{
                    $GLOBALS["log"]->write_log($mode,"login locked----login_id:".$login_id);

                    // ロックされてるからログイン不可
                    $errMsg[] = ERR_ACCOUNT_LOCK;
                    return $errMsg;
                }
            }


            // 重複ログインチェック
            if($memberData["access_session"] != "" && $memberData["access_session"] != session_id()){
                // 一定時間内の場合は他のユーザが操作中とみなす
                if(!parent::isInterval('access_time')){
                    $errMsg[] = "既に同じIDでログイン中です";
                    return $errMsg;
                }
            }


            // パスワード間違い件数クリア
            // 最終ログイン日時セット
            parent::updateMng();

            //最新の情報を取得
            $where[] = "formadmin_id = ".$dbGeneral->quote($memberData["formadmin_id"]);
            $memberData = $dbGeneral->getData("*", MEMBER_TABLE, $where, __FILE__, __LINE__);
            if(!$memberData){
                $errMsg[] = ERR_NOT_MEMBER_ID;
                return $errMsg;
            }


            // ログイン成功なのでログイン会員オブジェクトを生成
            $loginMember = new LoginMember();
            $loginMember->_memberData = $memberData;

			// セッション情報にログイン情報を保存する
			$GLOBALS["session"]->setVar("isLogin", true);
			$GLOBALS["session"]->setVar("userData", $memberData);
			$userData = $GLOBALS["session"]->getVar("userData");
			$GLOBALS["userData"] = $userData;
		}

		return $errMsg;
	}

	/**
	 * ログアウト処理を行います
	 * @access	public
	 * @return	string	エラーメッセージ
	 */
	function doLogout() {
        parent::clearMng();
		$GLOBALS["session"]->unsetVar("isLogin");
		$GLOBALS["session"]->unsetVar("userData");
        $GLOBALS["session"]->unsetVar("isBasic");
        $GLOBALS["session"]->destroy();
		return;
	}

	/**
	 * ログ
	 */
	function errLog($prame){

		$wa_log = "";

		$wa_log .="PARAM:";

		//リクエストパラメータ
		foreach($prame as $key  => $data){

			$wk_data = "";


			$wk_data = str_replace(array("\r\n","\r","\n"), "", $data);
			$wa_log .=  $key."=>".$wk_data." | ";
		}


	}
}
?>
