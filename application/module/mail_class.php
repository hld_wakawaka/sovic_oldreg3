<?php

/**
 * メールクラス
 *  メール送信
 *
 */

class Mail {

     /**
     * メールを送信する
	 *
     * @param  pa_data	フォルダデータ配列
     * @param  pn_preid	ルートフォルダの親ID（デフォルトは0とする）
     * @return  array
     */
    function SendMail($ps_to, $ps_subject, $ps_contents, $ps_from="", $ps_fromname="", $isUTF8=false)
    {

		/**
		 * 変数定義
		 */
		//$ws_to = "";
		$ws_Reply = "";
		$ws_header1 = "";
		$ws_header2 = "";





		/**
		 * 送信先・送信元セット
		 */
		$ws_to = $ps_to;

		//送信先が設定されていない場合
		if($ws_to == ""){
			return false;
		}


        /* UTF-8でメールを送信 */
        if($isUTF8){
    		/**
    		 * エンコード
    		 */
    		mb_internal_encoding("utf-8");
            mb_language("uni");


            $ps_from_string = $ps_from;

    		//送信元
    		if( $ps_fromname != "" ){
    			$ps_fromname2 = $ps_fromname;
    			$ps_from_string = mb_encode_mimeheader($ps_fromname2)."<".$ps_from."> ";
            }
            // 半角円マークがバックスラッシュになるのを防ぐ
            $ps_contents = preg_replace("/\\\\/u", "\xC2\xA5", $ps_contents);

        }else{
    		/**
    		 * エンコード
    		 */
    		mb_internal_encoding("utf-8");
    		mb_language("Japanese");


            $ps_from_string = $ps_from;

    		//送信元
    		if( $ps_fromname != "" ){
    			$ps_fromname2 = mb_convert_encoding($ps_fromname, "ISO-2022-JP", "utf-8");
    			$ps_from_string = mb_encode_mimeheader($ps_fromname2)."<".$ps_from."> ";
            }
        }



		/**
		 * メールヘッダーエンコード
		 * MIME エンコード
		 */
		$ws_header1 = "From: ".$ps_from_string;
//		$ws_header2 = "Bcc: ".MAIL_BCC;
//		$ws_header = $ws_header1."\n".$ws_header2;
		$ws_header = $ws_header1;


        //リターンパス
        $parameter = "-f ".$ps_from;
        ini_set("sendmail_from", $ps_from);


		// 3回までリトライ
		//for ($wn_cnt =1; $wn_cnt <= 3; $wn_cnt++) {
			$wb_ret = mb_send_mail($ws_to, $ps_subject, $ps_contents, $ws_header, $parameter);

			// 成功したらループを抜ける
//			if ($wb_ret) {
//			        break;
//			}
//			usleep(100000);
		//}

		if (!$wb_ret) {
			return false;
		}

		return true;

	}



}
?>