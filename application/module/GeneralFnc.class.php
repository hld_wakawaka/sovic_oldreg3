<?php
// 汎用関数クラス
// 
// $Id: GeneralFnc.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
//

class GeneralFnc {

	// $strの前に「0」をつめて、$len文字にする
	function formatZero($str, $len) {

//  return substr( str_repeat("0",$len).$str , -$len );  // この１行でできないでしょうか？

		$j = mb_strlen($str);

		if ($j > $len)
			return $str;

		for ($i = 1; $i <= $len - $j; $i++) {
			$str = "0".$str;
		}

		return $str;
	}
	
	//配列から空っぽの値を省く
	function array_trip($arr) {
		
		$newarr = array();
		foreach($arr as $val) {
			if($val != "") {
				$newarr[] = $val;
			}
		}
		return $newarr;
	}

	// ファイル読み込み関数
	function readFile($filePath) {

		// 読み込み先ファイルオープン
		$fp = fopen($filePath, "r+");
		// ファイルをロック
		flock($fp, LOCK_EX);

		$buf = fread( $fp, filesize($filePath) ); // 読み込み

		// ロック解除
		flock($fp, LOCK_UN);
		// ファイルをクローズ
		fclose($fp);
		return $buf;
	}

	// ファイル書き込み関数
	function writeFile($fileName, $message) {

		// 書き込み先ファイルオープン
		$fp = fopen($fileName, "a+");
		// ファイルをロック
		flock($fp, LOCK_EX);

		// ファイルへの書き込み
		fwrite($fp, $message."\n");

		// ロック解除
		flock($fp, LOCK_UN);
		// ファイルをクローズ
		fclose($fp);
	}
	
	// ファイル書き込み関数
	function clearwriteFile($fileName, $message) {

		// 書き込み先ファイルオープン
		$fp = fopen($fileName, "w");
		// ファイルをロック
		flock($fp, LOCK_EX);

		// ファイルへの書き込み
		fwrite($fp, $message."\n");

		// ロック解除
		flock($fp, LOCK_UN);
		// ファイルをクローズ
		fclose($fp);
	}
	
	// URLにリンク設定を行なう
	function linkURL($str) {
        $str = mbereg_replace(
        		'https?://'.
        		'(([a-zA-Z0-9]+\.){1,}[a-zA-Z]+|(\d+.){3}\d+)'. // host
        		'(:\d+)?'. // port
        		'(/[-.!~*\d\w;/?:@&=+$,%#]+)?',
        		'<a href="\0" target="_blank">\0</a>', $str);
        return $str;
	}

	/**
	 * ランダムな文字列を生成します。
	 * @access	public
	 * @param	int		$min		最小文字数
	 * @param	int		$max		最大文字数
	 * @param	string	$char		利用可能文字
	 * @return	boolean	更新結果
	 */
	function createRandamKey($min, $max, $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {

		$len = 0;
		$key = "";

		// 文字数の決定
		// 乱数ジェネレータを初期化
		srand();
		// 文字数を乱数で決定
		$len = rand($min, $max);

		// キー生成
		for ($i = 1; $i <= $len; $i++) {
			// 乱数ジェネレータを初期化
			srand();
			$key .= substr($char, rand(0, strlen($char) -1 ), 1);
		}

		return $key;
	}

	/**
	 * 和暦から西暦に変換します
	 * @access	public
	 * @param	string	$waEra		和暦年号
	 * @param	int		$waYear		和暦年
	 * @return	int		西暦年
	 */
	function wareki_to_seireki($waEra, $waYear){ 

	    $year = "";

	    switch($waEra){
	        case "明治":
	            $year=1867 + (integer)$waYear;
	            break;
	        case "大正":
	            $year=1911 + (integer)$waYear;
	            break;
	        case "昭和":
	            $year=1925 + (integer)$waYear;
	            break;
	        case "平成":
	            $year=1988 + (integer)$waYear;
	            break;
	    }

		return $year;
	}

	/**
	 * 和暦から西暦に変換します
	 * @access	public
	 * @param	string	$waEra		和暦年号
	 * @param	int		$waYear		和暦年
	 * @param	int		$waMonth	和暦月
	 * @param	int		$waDay		和暦日
	 * @return	string	西暦年月日
	 */
	function wareki_to_seireki_full($waEra, $waYear, $waMonth, $waDay){ 

	    $year = "";

	    switch($waEra){
	        case "明治":
	            $year=1867 + (integer)$waYear;
	            break;
	        case "大正":
	            $year=1911 + (integer)$waYear;
	            break;
	        case "昭和":
	            $year=1925 + (integer)$waYear;
	            break;
	        case "平成":
	            $year=1988 + (integer)$waYear;
	            break;
	    }

		if (checkdate($waMonth, $waDay, $year)) {
			return $year."-".GeneralFnc::formatZero($waMonth, 2)."-".GeneralFnc::formatZero($waDay, 2);
		} else {
			return false;
		}
	}

	/**
	 * 西暦から和暦に変換します
	 * @access	public
	 * @param	int　	$year		西暦
	 * @return	array	和暦の年号と年を配列で戻す
	 */
	function seireki_to_wareki($year){ 

		$wareki = array();

	    if ($year > 1988) {
	        $wareki[0] = "平成";
	        $wareki[1] = (integer)$year - 1988;
	    } elseif ($year > 1925) {
	        $wareki[0] = "昭和";
	        $wareki[1] = (integer)$year - 1925;
	    } elseif ($year > 1911) {
	        $wareki[0] = "大正";
	        $wareki[1] = (integer)$year - 1911;
	    } elseif ($year > 1867) {
	        $wareki[0] = "明治";
	        $wareki[1] = (integer)$year - 1867;
	    }

		return $wareki;
	}

	/**
	 * 西暦から和暦に変換します
	 * @access	public
	 * @param	int　	$year		西暦
	 * @return	array	和暦の年号と年と月と日を配列で戻す
	 */
	function seireki_to_wareki_full($date){ 

		$wareki = false;

		if (!empty($date)) {

			list($year, $month, $day) = split("-", $date);

			$wareki = array();

			$wareki = GeneralFnc::seireki_to_wareki($year);
			$wareki[2] = $month;
			$wareki[3] = $day;

	    }

		return $wareki;
	}

	function mb_convert_array(&$array) {
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				GeneralFnc::mb_convert_array($value);
			} else {
				$array[$key] = mb_convert_encoding($value, ini_get ("default_charset"), mb_detect_encoding($value));
			}
		}
	}
	
	//リクエストデータの取り込み(配列は３階層まで）	
	static function convertParam($init, $arrValue) {
		
		$output = array();
	
		if(is_array($init)) {
			foreach($init as $val) {
		
				$key = $val[0];
				$opt = $val[4];
		
				if(isset($arrValue[$key])) {
					$output[$key] = GeneralFnc::gf_convert($arrValue[$key], $opt);
				}else{
					$output[$key] = "";
				}
			}
		}
		
		return $output;
	
	}
	
	function gf_convert($value, $opt, $i=0) {
	
		if($i > 3) {
			return;
		}
	
		if(is_array($value)) {
			foreach($value as $key=>$val) {
				$value[$key] = GeneralFnc::gf_convert($val, $opt, $i+1);
			}
		} else {
			$value = trim($value);
			$value = mb_convert_kana($value, $opt, SCRIPT_ENC);
		}
		
		return $value;
	
	}
	
	/**
	 * 数字の配列を作成する
	 * @param int $start, $end, $step;
	 */
	 function create_num_array($start=0,$end=1,$step=1) {
	 	
		$i = $start;
		
		if($start <= $end) {
	 		while($i <= $end) {
				$array[$i] = $i;
				$i = $i + $step;
			}
		}
		
		if($start < $end) {
			while($i <= $end) {
				$array[$i] = $i;
				$i = $i - $step;
			}
		}
		
		return $array;
		
	 }
	 
	 function create_hidden($array) {
	 
	 	$hiddentxt = '<input type="hidden" name="%s" value="%s" />';
	 	
		foreach($array as $key=>$val) {
			if(is_array($val)) {
				foreach($val as $key2=>$val2) {
					$output .= sprintf($hiddentxt, $key."[".$key2."]", $val2);
				}
			} else {
				$output .= sprintf($hiddentxt, $key, $val);
			}
		}
		
		return $output;
	 }
	 
	 function pager($limit=30, $page, $total) {
	 
	 	$params = array(
			'mode' => 'sliding',
			'perPage' => $limit,
			'delta' => 5,
			'urlVar' => 'page',
			'currentPage' => $page,
			'spacesBeforeSeparator' => 1,
			'spacesAfterSeparator' => 1,
			'clearIfVoid' => false,
			'totalItems' => $total
		);

		$pager = & Pager::factory($params);
		
		return $pager;
	 
	 }
	 
	 // ディレクトリを作成する。
	 /* param $path ディレクトリパス
	  * param　$permission　パーミッション
	  */
	 function createDir($path) {
	 	
	 	if(!is_dir($path)) {
			mkdir($path);
			chmod($path, 0777);
		}
	 	
	 }
	 
	 //半角で区切って配列を返す
	 function customExplode($str="") {
	 	
	 	$str = trim($str);
	 	if($str == "") return array();
	 	
	 	$str = str_replace("　", " ", $str);
	 	
	 	return explode(" ", $str);
	 	
	 }



	/**
	 * 外部ファイルが存在するかチェック
	 * 	拡張子がPHPの場合はファイルをinclude_onceする
	 *
	 * @param stirng ディレクトリ
	 * @param int form_id
	 * @param string ファイル名
	 * @param string 拡張子
	 */
	function _chkIncFiles($ps_dir, $pn_form_id, $ps_file_name, $ps_extension){

		//探したいファイルの名前
		$find_file = $pn_form_id.$ps_file_name.".".$ps_extension;

		if (is_dir($ps_dir)) {
		    if ($dh = opendir($ps_dir)) {
		        while (($file = readdir($dh)) !== false) {
		        	//外部読み込みファイルが見つかった場合
		            if($file == $find_file){
		            	//PHPファイルの場合
		            	if($ps_extension == "php"){
							include_once($ps_dir.$find_file);
		            	}

						return true;
		            }
		        }
		        closedir($dh);
		    }
		}

		return false;
	}


    function smarty_modifier_del_tags($string) {

        if($string == "") return;

        $pattern = "<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>(.*?)<\/(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>";
        $string = preg_replace("/".$pattern."/u", "", $string);

        return $string;
    }


    // ex. GeneralFnc::stdClass('{"y":"syear", "m":"smonth", "d":"sday"}');
    function stdClass($json_str=NULL) {
        $obj = new stdClass();
        $json = json_decode($json_str);
        if($json !== null){
            foreach($json as $key => $item){
                $obj->{"{$key}"} = $item;
            }
        }
        return $obj;
    }


    function convTimestamp($year, $month, $day, $format="Y-m-d", $hour=0, $minute=0, $second=0){
        return date($format, mktime($hour, $minute, $second, $month, $day, $year));
    }

}
?>
