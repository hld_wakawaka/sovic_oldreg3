<?php
require_once("PEAR.php");
require_once(MODULE_DIR.'entry_ex/Config.php');

/**
 * ProcessBaseクラス
 *
 * ProcessBaseクラス
 *
 * @package	include.ProcessBase
 * @access	public
 * @create	2004/10/19
 * @version	$Id: ProcessBase.class.php,v 1.1 2007/09/13 16:39:01 fushimi Exp $
 **/
class ProcessBase extends PEAR {

	var $_reload;
	var $_processTemplate;
	var $_tplval;
	var $_smarty;
	var $arrForm;
	
	var $_title;
	var $pager;


	/**
	 * コンストラクタ
	 * @access	public
	 */
	function ProcessBase() {
	
		parent::PEAR();

		// リロード防止クラスのインスタンス生成
		$this->_reload = new Reload();
		
		// 画面表示内容を設定
		$this->_smarty = new MySmarty();
		
		if(isset($_REQUEST["mode"])) $GLOBALS["log"]->write_log($_REQUEST["mode"]);
	}

	/**
	 * デストラクタ
	 * @access	public
	 */
	function _ProcessBase() {
	}

	/**
	 * メイン処理
	 * @access	public
	 */
	function main() {
    	// カスタマイズテンプレート設定の読み込み
        Config::load($this);

		// リロード防止用隠し項目をアサイン
		$this->assign("embed", $this->_reload->embed());
		$this->assign("urlparam", $this->_reload->urlparam());

		if (!empty($errMsg)) {
			// エラーがある場合には、エラーメッセージ部を表示
			$this->assign("errMsg", $errMsg);
		}
		
		$this->assign("approot", APP_ROOT);
		$this->assign("cssdir", "/admin/");
		$this->assign("_title", $this->_title);
		$this->assign("pagetitle", $this->_title);
		$this->assign("arrForm", $this->arrForm);
		$this->assign("pager", $this->pager);
		
		if($GLOBALS["form"]->isForm) {
			$this->assign("fData", $GLOBALS["form"]->formData);
		}
		
		if($GLOBALS["session"]->getVar("isLoginEntry")) {
			$this->assign("entryData", $GLOBALS["session"]->getVar("entryData"));
		}
		
		if ($GLOBALS["session"]->getVar("isLoginAdmin")) {
			$this->assign("isAdmin", true);
			$this->assign("adminData", $GLOBALS["session"]->getVar("adminData"));
		} else {
			$this->assign("isAdmin", false);
		}
		
		if ($GLOBALS["session"]->getVar("isLogin")) {
			$this->assign("isLogin", true);
			$this->assign("userData", $GLOBALS["session"]->getVar("userData"));
		} else {
			$this->assign("isLogin", false);
		}

		// ブラウザのキャッシュを残さないようにする
		header("Content-Type: text/html; charset=utf-8");
		header("Expires: Thu, 01 Dec 1994 16:00:00 GMT");
		header("Last-Modified: ". gmdate("D, d M Y H:i:s"). " GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// 画面表示
		$this->_smarty->display($this->_processTemplate);
		

	}
	
	function assign($key, $val) {
		
		$this->_smarty->assign($key, $val);
		
	}


    /**
     * 拡張クラスを読み込む
     * 
     * @param  integer $form_id  読み込み対象のフォームID
     * @param  object  $objClass 拡張クラスのインスタンス(参照渡し)
     * @param  boolean $inc_flg  拡張クラスを読み込むかフラグ(true:読み込む / false:読み込まない)
     * @return boolean $isOverrideClass 拡張クラスの存在の有無
     **/
    function isOverrideClass($form_id, &$objClass=null, $inc_flg=true){
        $isOverrideClass = false;

        // フォームID指定なし（例外）
        if(!$form_id > 0) return $isOverrideClass;

        $inc = "Usr_entry".$form_id;
        $arrIncWay = array();
        $arrIncWay[0] = ROOT_DIR."Usr/include/".$inc.".php";                    // Usr/include/Usr_entry[].php
        $arrIncWay[1] = ROOT_DIR."customDir/{$form_id}/package/".$inc.".php";   // package/[]/package/Usr_entry[].php
        
        foreach($arrIncWay as $_key => $inc_file){
            $isOverrideClass = file_exists($inc_file);
            if($isOverrideClass && $inc_flg) {
                include_once($inc_file);
                if(class_exists($inc)) {
                    $objClass = new $inc($this);
                    break;
                }
            }
        }

        return $isOverrideClass;
    }

    /**
     * 外部クラスを読み込む
     * 
     * @param  integer $form_id  読み込み対象のフォームID
     * @param  object  $objClass 外部クラスのインスタンス(参照渡し)
     * @param  boolean $inc_flg  外部クラスを読み込むかフラグ(true:読み込む / false:読み込まない)
     * @return boolean $isOverrideClass 外部クラスの存在の有無
     **/
    function isExternalClass($form_id, &$objClass=null, $inc_flg=true){
        $isOverrideClass = false;

        // フォームID指定なし（例外）
        if(!$form_id > 0) return $isOverrideClass;

        $inc = "resource".$form_id;
        $arrIncWay = array();
        $arrIncWay[0] = ROOT_DIR."customDir/{$form_id}/external/".$inc.".php";   // package/[]/package/Usr_entry[].php
        
        foreach($arrIncWay as $_key => $inc_file){
            $isOverrideClass = file_exists($inc_file);
            if($isOverrideClass && $inc_flg) {
                include_once($inc_file);
                if(class_exists($inc)) {
                    $objClass = new $inc($this);
                    break;
                }
            }
        }

        return $isOverrideClass;
    }

}
?>
