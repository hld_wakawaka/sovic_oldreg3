<?php
/**
 * システム設定ファイル
 *
 * @author salon
 *
 */




//-----------------------------------------
// 実行環境
//-----------------------------------------
define("ENV_TYPE1", "local");
define("ENV_TYPE2", "dev");
define("ENV_TYPE3", "evt");

// 実行環境を設定
if(strpos(__FILE__, "testhp.jp") !== false) {
    define("ENV", ENV_TYPE2);
} elseif(strpos(__FILE__, "doishun") !== false) {
    define("ENV", ENV_TYPE1);
}else{
    define("ENV", ENV_TYPE3);
}



// 実行環境別の設定の読み込み
if(ENV == ENV_TYPE1){  // ローカル環境
    define("APP_ROOT", "/sovic_reg3/");
    // DB定義
    define("DB_ENV_ONE"  , "evt3_dev");
    define("DB_ENV_OTHRE", "link_cfp_dev");
    define("DB_DSN", "pgsql://evt_dev:tp1DviZK@localhost/".DB_ENV_ONE);
    // ルートディレクトリ（ファイルシステムでのルートディレクトリを絶対パスで指定）
    // アプリケーション基底パス
    $baseDir = realpath(dirname(__FILE__) . '/../../../');
    define("ROOT_DIR", $baseDir.APP_ROOT); unset($baseDir);
    // サイトURL、SSL対応URL
    define("URL_ROOT",       "http://".$_SERVER["HTTP_HOST"].APP_ROOT);
}
if(ENV == ENV_TYPE2){   // テスト環境
    define("APP_ROOT", "/reg3/");
    // DB接続
    define("DB_ENV_ONE"  , "evt3_dev");
    define("DB_ENV_OTHRE", "link_cfp_dev");
    define("DB_DSN", "pgsql://evt_dev:tp1DviZK@localhost/".DB_ENV_ONE);
    // ルートディレクトリ（ファイルシステムでのルートディレクトリを絶対パスで指定）
    define("ROOT_DIR", "/var/www/vhosts/testhp.jp/httpdocs".APP_ROOT);
    // サイトURL、SSL対応URL
    define("URL_ROOT", "http://testhp.jp".APP_ROOT);
}
if(ENV == ENV_TYPE3){   // 本番環境
    define("APP_ROOT", "/reg3/");
    // DB接続
    define("DB_ENV_ONE", "evt3");
    define("DB_DSN", "pgsql://evt:ZNsLupwY@localhost/".DB_ENV_ONE);
    // ルートディレクトリ（ファイルシステムでのルートディレクトリを絶対パスで指定）
    define("ROOT_DIR", "/var/www/vhosts/evt-reg.jp/httpdocs".APP_ROOT);
    // サイトURL、SSL対応URL
    define("URL_ROOT", "http://evt-reg.jp".APP_ROOT);
}


//-----------------------------------------
// サイト名
//-----------------------------------------
define("APP_TITLE", "");

//-------------------------------------------
// ライブラリディレクトリ（ファイルシステム上でのライブラリディレクトリ）
//-------------------------------------------
define("LIB_DIR", ROOT_DIR."application/library/");

//-------------------------------------------
// モジュールディレクトリ（モジュール）
//-------------------------------------------
define("MODULE_DIR", ROOT_DIR."application/module/");

//-------------------------------------------
// Smartyのテンプレートディレクトリ（メール本文）
//-------------------------------------------
define("TEMPLATES_DIR", ROOT_DIR."templates/");
// Smartyのコンパイルディレクトリ
define("COMPILE_DIR", ROOT_DIR."templates_c/");

//-----------------------------------------------------
// timezoneの設定
//-----------------------------------------------------
ini_set("date.timezone", "Asia/Tokyo");

//--------------------------------------------
// ログディレクトリ
//--------------------------------------------
define("LOG_DIR", ROOT_DIR."application/Log/");
define("DB_LOG_FILE"    , LOG_DIR."DBLog/DB_".date('Ymd').".log");
define("ERROR_LOG_FILE" , LOG_DIR."errorLog/Error_".date('Ymd').".log");
define("BATCH_LOG_FILE" , LOG_DIR."batchLog/Batch_".date('Ymd').".log");

//決済ログ
define("LOG_TGMDK_DIR", ROOT_DIR."application/Log/tgMdk/");

//--------------------------------------------
// エラーメールの送信先
//--------------------------------------------
define("ERROR_MAIL", "info@salon.ne.jp");




// アップロードファイルディレクトリ
define("UPLOAD_PATH", ROOT_DIR."upload/");
// アップロードファイルURL
define("UPLOAD_URL", APP_ROOT."upload/");

//　画像ファイルディレクトリ
define("IMG_PATH", ROOT_DIR."htdocs/images/");


//-----------------------------------------------------
// include_pathの再設定
//-----------------------------------------------------
ini_set("include_path", ini_get("include_path").":".LIB_DIR);
//ini_set("include_path", ini_get("include_path").":".LIB_DIR."pear/");

// デバックモードの設定
if (file_exists(LIB_DIR."DEBUG")) {
	define("DEBUG", true);
} else {
	define("DEBUG", false);
}

// 表示するエラーの設定

if (DEBUG) {
	ini_set("error_reporting", E_ALL | ~E_NOTICE | ~E_STRICT);
	ini_set("display_errors", true);
    error_reporting(error_reporting() ^ E_STRICT);
} else {
	ini_set("error_reporting", E_ALL | ~E_NOTICE | ~E_WARNING | ~E_DEPRECATED | ~E_STRICT);
	ini_set("display_errors", false);
    error_reporting(error_reporting() ^ E_STRICT);
}

/**
 * PEAR ****************************
 */

require_once("PEAR.php");
require_once("DB.php");
require_once("Pager/Pager.php");

// モジュール別
require_once("constants.ini.php");
require_once(MODULE_DIR."Error.class.php");
require_once(MODULE_DIR."GeneralFnc.class.php");
require_once(MODULE_DIR."Utils.php");
require_once(MODULE_DIR."HSession.class.php");
require_once(MODULE_DIR."LoginMember.class.php");
require_once(MODULE_DIR."LoginAdmin.class.php");
require_once(MODULE_DIR."LoginEntry.class.php");
require_once(MODULE_DIR."MyDB.class.php");
require_once(MODULE_DIR."MySmarty.class.php");
require_once(MODULE_DIR."ProcessBase.class.php");
require_once(MODULE_DIR."Reload.class.php");
require_once(MODULE_DIR."SmartyForm.class.php");
require_once(MODULE_DIR."Validate.class.php");
require_once(MODULE_DIR."log.class.php");
//require_once("Upload.class.php");
//require_once("Download.class.php");




// DBクラス
require_once(MODULE_DIR."dbClass/DbGeneral.class.php");

// カスタムクラス
require_once(MODULE_DIR."custom/User.class.php");
require_once(MODULE_DIR."custom/Form.class.php");
require_once(MODULE_DIR."custom/item_ini.class.php");

// DB接続
$notConnectDb = false;
if (!$notConnectDb) {


	// DB接続処理
	$GLOBALS["db"] = &MyDB::getInstance();
	//$GLOBALS["db"] = new Myadodb(DB_DSN);

}

// ログクラス
$GLOBALS["log"] = new log_class;

$GLOBALS["form"] = new Form;





?>
