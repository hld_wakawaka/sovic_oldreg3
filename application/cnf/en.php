<?php
/*
 * CreateDate: 2010/09/17
 *
 */

// 日本語バージョンを持っておく
$GLOBALS["msg"]['j'] = $GLOBALS["msg"];

$GLOBALS["msg"]["err_require_input"]	= "Enter %s.";
$GLOBALS["msg"]["err_require_select"]	= "Select one(s) for %s.";
$GLOBALS["msg"]["err_require_check"]	= "Check one(s) for %s.";
$GLOBALS["msg"]["err_numeric"]			= "%s must be numbers.";
$GLOBALS["msg"]["err_han_numeric"]		= "%s must be single-byte numbers.";
$GLOBALS["msg"]["err_zen_numeric"]		= "%s must be double-byte numbers.";
$GLOBALS["msg"]["err_han_alphanumeric"]	= "%s must be single-byte alphanumeric characters.";
$GLOBALS["msg"]["err_zenkana"]			= "%s must be full-width katakana.";
$GLOBALS["msg"]["err_han_alphanumericmark"]= "%s must be single-byte alphanumeric characters or symbols.";
$GLOBALS["msg"]["err_zen_alphanumeric"]	= "%s must be double-byte alphanumeric characters.";
$GLOBALS["msg"]["err_zen"]				= "%s must be double-byte characters.";
$GLOBALS["msg"]["err_byte"]				= "%s must be %s bytes or less.";
$GLOBALS["msg"]["err_mail"]				= "%s is an invalid email address.";
$GLOBALS["msg"]["err_strlen"]			= "%s must be %s characters long.";
$GLOBALS["msg"]["err_strlen_max"]		= "%s cannot exceed %s  characters.";
$GLOBALS["msg"]["err_strlen_between"]	= "%s must be %s characters or more but not more than %s characters.";
$GLOBALS["msg"]["err_over_max_filesize"]= "%sのファイルサイズが大きすぎます。　%sバイト以下のファイルを選択してください。";
$GLOBALS["msg"]["err_suffix_file"]		= "%sのファイル拡張子はアップロードできません。　アップロード可能な拡張子は%sです。";
$GLOBALS["msg"]["err_format"]			= "Please check %s again.";
$GLOBALS["msg"]["err_format_han_numeric_haifun"]= "Enter %s in single-byte numbers with hyphens.";
$GLOBALS["msg"]["err_format_han_alphanumeric"]	= "%sを確認してください。(半角英数字で入力してください)";
$GLOBALS["msg"]["err_over_lap"]			= "入力した%sは、既に登録済みです。";
$GLOBALS["msg"]["err_not_exist"]		= "%s not found.";
$GLOBALS["msg"]["err_require_parameter"]= "Sorry, some of the required information is missing. Please try again from the beginning.";
$GLOBALS["msg"]["err_db_system"]		= "We are currently performing system maintenance. Please check back later. ";
$GLOBALS["msg"]["err_word_cnt"]		= "%s cannot exceed %s words.";
$GLOBALS["msg"]["err_tokusyu"]		= "%s includes machine dependent characters.";

$GLOBALS["msg"]["msg_login"]			= "Enter your ID and password to log in.";
$GLOBALS["msg"]["msg_logout"]			= "You are now logged out. Thank you for visiting us. ";

$GLOBALS["msg"]["err_not_loginid"]		= "Incorrect login ID or password";
$GLOBALS["msg"]["err_mistake_password"]	= "Incorrect login ID or password";
$GLOBALS["msg"]["err_login_status"]		= "Unauthorized login";

$GLOBALS["msg"]["validDate_start"]		= "Login failed because applications are not being accepted yet. Please log in after %s.";
$GLOBALS["msg"]["validDate_end"]		= "The application deadline was %s.";
$GLOBALS["msg"]["validDate_middle"]		= "The first application deadline has passed. The second period starts on %s.";
$GLOBALS["msg"]["dateformat"]			= "m/d/Y";

$GLOBALS["msg"]["login_id"]				= "Login ID";
$GLOBALS["msg"]["login_passwd"]			= "Password";

$GLOBALS["msg"]["btn_login"]			= "login";
$GLOBALS["msg"]["btn_logout"]			= "ログアウト";
$GLOBALS["msg"]["btn_return"]			= "back";
$GLOBALS["msg"]["btn_next"]				= "next";
$GLOBALS["msg"]["btn_regist"]			= "register";
$GLOBALS["msg"]["btn_update"]			= "update";
$GLOBALS["msg"]["btn_send"]				= "register";

$GLOBALS["msg"]["btn_change_fee"]	= "Change payment method";
$GLOBALS["msg"]["btn_change_payment"]	= "Modify information entered";

$GLOBALS["msg"]["reload"] = "既に登録は完了しています。";

// システムメンテナンス用メッセージ
$GLOBALS["msg"]["maintenance"] = 'Sorry, our system is temporarily unavailable to the system maintenance.<br/>We appreciate your kind understanding.';

//必須マーク
$GLOBALS["msg"]["need_mark"]				= "*";

// 同意文言
$GLOBALS["msg"]["agree1"] = "Agree";
$GLOBALS["msg"]["agree2"] = "Disagree";
// チェックボックス用の同意分
$GLOBALS["msg"]["agreechk"] = "I agree with the above-mentioned matter and register for the Conference.";

// ブラウザバックの注意書き
$GLOBALS["msg"]["browser_back"]	= '*Please do not use the "Back" button on your browser.';

// 3Dセキュアに関する文言
$GLOBALS["msg"]["3d_send"]			= 'Credit card transaction is now processing, so please wait for some moment.<br/>If you are not automatically redirected, please click "authentication" button below.';
$GLOBALS["msg"]["btn_3d_send"]		= "authentication";

$GLOBALS["msg"]["3d_result"]		= 'Now processing ...  please wait until you move to the completion screen with your registration number.<br/>In case you are not automatically redirected, please click "complete registration" button below.';
$GLOBALS["msg"]["btn_3d_result"]	= "complete registration";

// 確認画面に関する文言
$GLOBALS["msg"]["cofirm_title"]			= "<b>Confirmation screen for your registration</b>";
$GLOBALS["msg"]["cofirm_desc1"]			= "Please check all information below and click the [register] button at the bottom.";
$GLOBALS["msg"]["cofirm_desc2"]			= "<b>*Your registration has not been completed yet. <br />Please be sure to click the [register] button to complete the procedure.</b>";
$GLOBALS["msg"]["cofirm_desc_i"]		= '<span style="font-weight: bold;font-size: 17px;">'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';
$GLOBALS["msg"]["cofirm_desc_e"]		= '<span style="font-weight: bold;font-size: 17px;">'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';

// Usr:アカウントロック,二重ログイン
$GLOBALS["msg"]["account_lock"] = "Account has been locked, because of a failure to log in the number of regulations.";
$GLOBALS["msg"]["account_same"] = "It is currently logged in with the same ID.";

// 編集メール 更新された項目がない場合
$GLOBALS["msg"]["edit_mail_non"] = "You have not updated any information this time.\n\n";
$GLOBALS["msg"]["edit_non"] = "";

// CSVのヘッダ
$GLOBALS["csv"]["entryno"] = "entry No.";
$GLOBALS["csv"]["status"]  = "status";
$GLOBALS["csv"]["rdate"]   = "regist date";
$GLOBALS["csv"]["udate"]   = "edit date";

// クレジットカードの同意に関するエラー
$GLOBALS["msg"]["credit"]["agree"] = "Please proceed further only if you accept our policy.";

?>
