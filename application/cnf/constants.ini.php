<?php
/**
 * 定数定義
 *
 * @author salon
 *
 */

//-----------------------------------
// 暗号化用KEY
//-----------------------------------
define("PASS_PHRASE", "8xSBFi8Paa7PndQZGh0e");

//-----------------------------------
// パスワードエラー上限
//-----------------------------------
define("PASS_ERROR_COUNT", 10);

//-----------------------------------
// アカウントロック制限時間
//-----------------------------------
define("ACOUNT_LOCK_TIME", 30);     //単位：分


//-----------------------------------
// セッション切れ時間
//-----------------------------------
define("SESSION_LIFE_TIME", ini_get('session.gc_maxlifetime')); //単位：秒
//define("SESSION_LIFE_TIME", 120); //単位：秒


//----------------------------------
//一覧の表示数
//----------------------------------
define("ROW_LIMIT", 100);


//-----------------------------------
//利用エンコード
//-----------------------------------
define("SCRIPT_ENC", "UTF-8");


//-----------------------------------
//言語設定
//-----------------------------------
define("LANG_JPN", 1);  // 日本語
define("LANG_ENG", 2);  // 英語


//サロン用メールアドレス
define("SYSTEM_MAIL", "hikae@salon.ne.jp");


//-----------------------------------
// 処理モード
//-----------------------------------
define("FUNCTION_SYS", "Sys");
define("FUNCTION_MNG", "Mng");
define("FUNCTION_USR", "Usr");


//-----------------------------------
// フォーム項目
//-----------------------------------
// フォームタイプ
$GLOBALS["formtypeList"]["1"] = "アブストラクト";
$GLOBALS["formtypeList"]["2"] = "フルペーパー";
$GLOBALS["formtypeList"]["3"] = "参加登録";

// 添付ファイルサイズ　最大数
$GLOBALS["uploadfile_max"] = "10";

// チェック項目
$GLOBALS["checktypeList"]["0"] = "必須";
$GLOBALS["checktypeList"]["1"] = "byte";
$GLOBALS["checktypeList"]["2"] = "文字数";
$GLOBALS["checktypeList"]["3"] = "単語数";
$GLOBALS["checktypeList"]["4"] = "全角";
$GLOBALS["checktypeList"]["5"] = "半角";
$GLOBALS["checktypeList"]["6"] = "メール";

// コントロールタイプ
$GLOBALS["controltypeList"]["0"] = "テキスト";
$GLOBALS["controltypeList"]["1"] = "テキストエリア";
$GLOBALS["controltypeList"]["2"] = "ラジオボタン";
$GLOBALS["controltypeList"]["3"] = "チェックボックス";
$GLOBALS["controltypeList"]["4"] = "セレクトボックス";

// 個人情報タイプ
$GLOBALS["agreetypeList"]["0"] = "利用しない";
$GLOBALS["agreetypeList"]["1"] = "埋め込み認証付";
$GLOBALS["agreetypeList"]["2"] = "埋め込み認証無";
$GLOBALS["agreetypeList"]["3"] = "別ページ認証付";
$GLOBALS["agreetypeList"]["4"] = "別ページ認証無";

// 決済システムタイプ
$GLOBALS["credittypeList"]["0"] = "利用しない";
$GLOBALS["credittypeList"]["1"] = "オフライン決済";
$GLOBALS["credittypeList"]["2"] = "オンライン決済";

// 金額設定タイプ
$GLOBALS["pricetypeList"]["1"] = "一律タイプ";
$GLOBALS["pricetypeList"]["2"] = "選択タイプ";

//都道府県
$GLOBALS["prefectureList"]["1"]		= "北海道";
$GLOBALS["prefectureList"]["2"]		= "青森県";
$GLOBALS["prefectureList"]["3"]		= "岩手県";
$GLOBALS["prefectureList"]["4"]		= "宮城県";
$GLOBALS["prefectureList"]["5"]		= "秋田県";
$GLOBALS["prefectureList"]["6"]		= "山形県";
$GLOBALS["prefectureList"]["7"]		= "福島県";
$GLOBALS["prefectureList"]["8"]		= "茨城県";
$GLOBALS["prefectureList"]["9"]		= "栃木県";
$GLOBALS["prefectureList"]["10"]	= "群馬県";
$GLOBALS["prefectureList"]["11"]	= "埼玉県";
$GLOBALS["prefectureList"]["12"]	= "千葉県";
$GLOBALS["prefectureList"]["13"]	= "東京都";
$GLOBALS["prefectureList"]["14"]	= "神奈川県";
$GLOBALS["prefectureList"]["15"]	= "山梨県";
$GLOBALS["prefectureList"]["16"]	= "長野県";
$GLOBALS["prefectureList"]["17"]	= "新潟県";
$GLOBALS["prefectureList"]["18"]	= "富山県";
$GLOBALS["prefectureList"]["19"]	= "石川県";
$GLOBALS["prefectureList"]["20"]	= "福井県";
$GLOBALS["prefectureList"]["21"]	= "岐阜県";
$GLOBALS["prefectureList"]["22"]	= "静岡県";
$GLOBALS["prefectureList"]["23"]	= "愛知県";
$GLOBALS["prefectureList"]["24"]	= "三重県";
$GLOBALS["prefectureList"]["25"]	= "滋賀県";
$GLOBALS["prefectureList"]["26"]	= "京都府";
$GLOBALS["prefectureList"]["27"]	= "大阪府";
$GLOBALS["prefectureList"]["28"]	= "兵庫県";
$GLOBALS["prefectureList"]["29"]	= "奈良県";
$GLOBALS["prefectureList"]["30"]	= "和歌山県";
$GLOBALS["prefectureList"]["31"]	= "鳥取県";
$GLOBALS["prefectureList"]["32"]	= "島根県";
$GLOBALS["prefectureList"]["33"]	= "岡山県";
$GLOBALS["prefectureList"]["34"]	= "広島県";
$GLOBALS["prefectureList"]["35"]	= "山口県";
$GLOBALS["prefectureList"]["36"]	= "徳島県";
$GLOBALS["prefectureList"]["37"]	= "香川県";
$GLOBALS["prefectureList"]["38"]	= "愛媛県";
$GLOBALS["prefectureList"]["39"]	= "高知県";
$GLOBALS["prefectureList"]["40"]	= "福岡県";
$GLOBALS["prefectureList"]["41"]	= "佐賀県";
$GLOBALS["prefectureList"]["42"]	= "長崎県";
$GLOBALS["prefectureList"]["43"]	= "熊本県";
$GLOBALS["prefectureList"]["44"]	= "大分県";
$GLOBALS["prefectureList"]["45"]	= "宮崎県";
$GLOBALS["prefectureList"]["46"]	= "鹿児島県";
$GLOBALS["prefectureList"]["47"]	= "沖縄県";

//-------------------------------------
//国名
//-------------------------------------
$GLOBALS["country"]["1"] 	= "Afghanistan";
$GLOBALS["country"]["2"] 	= "Aland Islands";
$GLOBALS["country"]["3"] 	= "Albania";
$GLOBALS["country"]["4"] 	= "Algeria";
$GLOBALS["country"]["5"] 	= "American Samoa";
$GLOBALS["country"]["6"] 	= "Andorra";
$GLOBALS["country"]["7"] 	= "Angola";
$GLOBALS["country"]["8"] 	= "Anguilla";
$GLOBALS["country"]["9"] 	= "Antigua and Barbuda";
$GLOBALS["country"]["10"] 	= "Argentina";
$GLOBALS["country"]["11"] 	= "Armenia";
$GLOBALS["country"]["12"]	= "Aruba";
$GLOBALS["country"]["13"] 	= "Australia";
$GLOBALS["country"]["14"] 	= "Austria";
$GLOBALS["country"]["15"] 	= "Azerbaijan";
$GLOBALS["country"]["16"] 	= "Bahamas";
$GLOBALS["country"]["17"] 	= "Bahrain";
$GLOBALS["country"]["18"] 	= "Bangladesh";
$GLOBALS["country"]["19"] 	= "Barbados";
$GLOBALS["country"]["20"] 	= "Belarus";
$GLOBALS["country"]["21"] 	= "Belgium";
$GLOBALS["country"]["22"] 	= "Belize";
$GLOBALS["country"]["23"] 	= "Benin";
$GLOBALS["country"]["24"] 	= "Bermuda";
$GLOBALS["country"]["25"] 	= "Bhutan";
$GLOBALS["country"]["26"] 	= "Bolivia";
$GLOBALS["country"]["27"] 	= "Bosnia and Herzegovina";
$GLOBALS["country"]["28"] 	= "Botswana";
$GLOBALS["country"]["29"] 	= "Brazil";
$GLOBALS["country"]["30"] 	= "British Virgin Islands";
$GLOBALS["country"]["31"] 	= "Brunei Darussalam";
$GLOBALS["country"]["32"] 	= "Bulgaria";
$GLOBALS["country"]["33"] 	= "Burkina Faso";
$GLOBALS["country"]["34"] 	= "Burundi";
$GLOBALS["country"]["35"] 	= "Cambodia";
$GLOBALS["country"]["36"] 	= "Cameroon";
$GLOBALS["country"]["37"] 	= "Canada";
$GLOBALS["country"]["38"] 	= "Cape Verde";
$GLOBALS["country"]["39"] 	= "Cayman Islands";
$GLOBALS["country"]["40"] 	= "Central African Republic";
$GLOBALS["country"]["41"] 	= "Chad";
$GLOBALS["country"]["42"] 	= "Channel Islands";
$GLOBALS["country"]["43"] 	= "Chile";
$GLOBALS["country"]["44"] 	= "China";
$GLOBALS["country"]["45"] 	= "Colombia";
$GLOBALS["country"]["46"] 	= "Comoros";
$GLOBALS["country"]["47"] 	= "Congo";
$GLOBALS["country"]["48"] 	= "Cook Islands";
$GLOBALS["country"]["49"] 	= "Costa Rica";
$GLOBALS["country"]["50"] 	= "Cote d'Ivoire";
$GLOBALS["country"]["51"] 	= "Croatia";
$GLOBALS["country"]["52"] 	= "Cuba";
$GLOBALS["country"]["53"] 	= "Cyprus";
$GLOBALS["country"]["54"] 	= "Czech Republic";
$GLOBALS["country"]["55"] 	= "Democratic Republic of the Congo";
$GLOBALS["country"]["56"] 	= "Denmark";
$GLOBALS["country"]["57"] 	= "Djibouti";
$GLOBALS["country"]["58"] 	= "Dominica";
$GLOBALS["country"]["59"] 	= "Dominican Republic";
$GLOBALS["country"]["60"] 	= "Ecuador";
$GLOBALS["country"]["61"] 	= "Egypt";
$GLOBALS["country"]["62"] 	= "El Salvador";
$GLOBALS["country"]["63"] 	= "Equatorial Guinea";
$GLOBALS["country"]["64"] 	= "Eritrea";
$GLOBALS["country"]["65"] 	= "Estonia";
$GLOBALS["country"]["66"] 	= "Ethiopia";
$GLOBALS["country"]["67"] 	= "Faeroe Islands";
$GLOBALS["country"]["68"] 	= "Falkland Islands (Malvinas)";
$GLOBALS["country"]["69"] 	= "Fiji";
$GLOBALS["country"]["70"] 	= "Finland";
$GLOBALS["country"]["71"] 	= "France";
$GLOBALS["country"]["72"] 	= "French Guiana";
$GLOBALS["country"]["73"] 	= "French Polynesia";
$GLOBALS["country"]["74"] 	= "Gabon";
$GLOBALS["country"]["75"] 	= "Gambia";
$GLOBALS["country"]["76"] 	= "Georgia";
$GLOBALS["country"]["77"] 	= "Germany";
$GLOBALS["country"]["78"] 	= "Ghana";
$GLOBALS["country"]["79"] 	= "Gibraltar";
$GLOBALS["country"]["80"] 	= "Greece";
$GLOBALS["country"]["81"] 	= "Greenland";
$GLOBALS["country"]["82"] 	= "Grenada";
$GLOBALS["country"]["83"] 	= "Guadeloupe";
$GLOBALS["country"]["84"] 	= "Guam";
$GLOBALS["country"]["85"] 	= "Guatemala";
$GLOBALS["country"]["86"] 	= "Guernsey";
$GLOBALS["country"]["87"] 	= "Guinea";
$GLOBALS["country"]["88"] 	= "Guinea-Bissau";
$GLOBALS["country"]["89"] 	= "Guyana";
$GLOBALS["country"]["90"] 	= "Haiti";
$GLOBALS["country"]["91"] 	= "Holy See";
$GLOBALS["country"]["92"] 	= "Honduras";
$GLOBALS["country"]["93"] 	= "Hong Kong";
$GLOBALS["country"]["94"] 	= "Hungary";
$GLOBALS["country"]["95"] 	= "Iceland";
$GLOBALS["country"]["96"] 	= "India";
$GLOBALS["country"]["97"] 	= "Indonesia";
$GLOBALS["country"]["98"] 	= "Iran";
$GLOBALS["country"]["99"] 	= "Iraq";
$GLOBALS["country"]["100"]  = "Ireland";
$GLOBALS["country"]["101"]  = "Isle of Man";
$GLOBALS["country"]["102"]  = "Israel";
$GLOBALS["country"]["103"]  = "Italy";
$GLOBALS["country"]["104"]  = "Jamaica";
$GLOBALS["country"]["105"]  = "Japan";
$GLOBALS["country"]["106"]  = "Jersey";
$GLOBALS["country"]["107"]  = "Jordan";
$GLOBALS["country"]["108"]  = "Kazakhstan";
$GLOBALS["country"]["109"]  = "Kenya";
$GLOBALS["country"]["110"]  = "Kiribati";
$GLOBALS["country"]["111"]  = "Korea";
$GLOBALS["country"]["112"]  = "Kuwait";
$GLOBALS["country"]["113"]  = "Kyrgyzstan";
$GLOBALS["country"]["114"]  = "Lao People's Democratic Republic";
$GLOBALS["country"]["115"]  = "Latvia";
$GLOBALS["country"]["116"]  = "Lebanon";
$GLOBALS["country"]["117"]  = "Lesotho";
$GLOBALS["country"]["118"]  = "Liberia";
$GLOBALS["country"]["119"]  = "Libyan Arab Jamahiriya";
$GLOBALS["country"]["120"]  = "Liechtenstein";
$GLOBALS["country"]["121"]  = "Lithuania";
$GLOBALS["country"]["122"]  = "Luxembourg";
$GLOBALS["country"]["123"]  = "Macao";
$GLOBALS["country"]["124"]  = "Madagascar";
$GLOBALS["country"]["125"]  = "Malawi";
$GLOBALS["country"]["126"]  = "Malaysia";
$GLOBALS["country"]["127"]  = "Maldives";
$GLOBALS["country"]["128"]  = "Mali";
$GLOBALS["country"]["129"]  = "Malta";
$GLOBALS["country"]["130"]  = "Marshall Islands";
$GLOBALS["country"]["131"]  = "Martinique";
$GLOBALS["country"]["132"]  = "Mauritania";
$GLOBALS["country"]["133"]  = "Mauritius";
$GLOBALS["country"]["134"]  = "Mayotte";
$GLOBALS["country"]["135"]  = "Mexico";
$GLOBALS["country"]["136"]  = "Micronesia";
$GLOBALS["country"]["137"]  = "Monaco";
$GLOBALS["country"]["138"]  = "Mongolia";
$GLOBALS["country"]["139"]  = "Montenegro";
$GLOBALS["country"]["140"]  = "Montserrat";
$GLOBALS["country"]["141"]  = "Morocco";
$GLOBALS["country"]["142"]  = "Mozambique";
$GLOBALS["country"]["143"]  = "Myanmar";
$GLOBALS["country"]["144"]  = "Namibia";
$GLOBALS["country"]["145"]  = "Nauru";
$GLOBALS["country"]["146"]  = "Nepal";
$GLOBALS["country"]["147"]  = "Netherlands";
$GLOBALS["country"]["148"]  = "Netherlands Antilles";
$GLOBALS["country"]["149"]  = "New Caledonia";
$GLOBALS["country"]["150"]  = "New Zealand";
$GLOBALS["country"]["151"]  = "Nicaragua";
$GLOBALS["country"]["152"]  = "Niger";
$GLOBALS["country"]["153"]  = "Nigeria";
$GLOBALS["country"]["154"]  = "Niue";
$GLOBALS["country"]["155"]  = "Norfolk Island";
$GLOBALS["country"]["156"]  = "Northern Mariana Islands";
$GLOBALS["country"]["157"]  = "Norway";
$GLOBALS["country"]["158"]  = "Oman";
$GLOBALS["country"]["159"]  = "Pakistan";
$GLOBALS["country"]["160"]  = "Palau";
$GLOBALS["country"]["161"]  = "Palestinian Territory";
$GLOBALS["country"]["162"]  = "Panama";
$GLOBALS["country"]["163"]  = "Papua New Guinea";
$GLOBALS["country"]["164"]  = "Paraguay";
$GLOBALS["country"]["165"]  = "Peru";
$GLOBALS["country"]["166"]  = "Philippines";
$GLOBALS["country"]["167"]  = "Pitcairn";
$GLOBALS["country"]["168"]  = "Poland";
$GLOBALS["country"]["169"]  = "Portugal";
$GLOBALS["country"]["170"]  = "Puerto Rico";
$GLOBALS["country"]["171"]  = "Qatar";
//$GLOBALS["country"]["172"]  = "Republic of Korea";
$GLOBALS["country"]["173"]  = "Republic of Moldova";
$GLOBALS["country"]["174"]  = "Reunion";
$GLOBALS["country"]["175"]  = "Romania";
$GLOBALS["country"]["176"]  = "Russian Federation";
$GLOBALS["country"]["177"]  = "Rwanda";
$GLOBALS["country"]["178"]  = "Saint Helena";
$GLOBALS["country"]["179"]  = "Saint Kitts and Nevis";
$GLOBALS["country"]["180"]  = "Saint Lucia";
$GLOBALS["country"]["181"]  = "Saint Pierre and Miquelon";
$GLOBALS["country"]["182"]  = "Saint Vincent and the Grenadines";
$GLOBALS["country"]["183"]  = "Saint-Barthelemy";
$GLOBALS["country"]["184"]  = "Saint-Martin (French part)";
$GLOBALS["country"]["185"]  = "Samoa";
$GLOBALS["country"]["186"]  = "San Marino";
$GLOBALS["country"]["187"]  = "Sao Tome and Principe";
$GLOBALS["country"]["188"]  = "Saudi Arabia";
$GLOBALS["country"]["189"]  = "Senegal";
$GLOBALS["country"]["190"]  = "Serbia";
$GLOBALS["country"]["191"]  = "Seychelles";
$GLOBALS["country"]["192"]  = "Sierra Leone";
$GLOBALS["country"]["193"]  = "Singapore";
$GLOBALS["country"]["194"]  = "Slovakia";
$GLOBALS["country"]["195"]  = "Slovenia";
$GLOBALS["country"]["196"]  = "Solomon Islands";
$GLOBALS["country"]["197"]  = "Somalia";
$GLOBALS["country"]["198"]  = "South Africa";
$GLOBALS["country"]["199"]  = "Spain";
$GLOBALS["country"]["200"]  = "Sri Lanka";
$GLOBALS["country"]["201"]  = "Sudan";
$GLOBALS["country"]["202"]  = "Suriname";
$GLOBALS["country"]["203"]  = "Svalbard and Jan Mayen Islands";
$GLOBALS["country"]["204"]  = "Swaziland";
$GLOBALS["country"]["205"]  = "Sweden";
$GLOBALS["country"]["206"]  = "Switzerland";
$GLOBALS["country"]["207"]  = "Syrian Arab Republic";
$GLOBALS["country"]["208"]  = "Taiwan";
$GLOBALS["country"]["209"]  = "Tajikistan";
$GLOBALS["country"]["210"]  = "Thailand";
$GLOBALS["country"]["211"]  = "The former Yugoslav Republic of Macedonia";
$GLOBALS["country"]["212"]  = "Timor-Leste";
$GLOBALS["country"]["213"]  = "Togo";
$GLOBALS["country"]["214"]  = "Tokelau";
$GLOBALS["country"]["215"]  = "Tonga";
$GLOBALS["country"]["216"]  = "Trinidad and Tobago";
$GLOBALS["country"]["217"]  = "Tunisia";
$GLOBALS["country"]["218"]  = "Turkey";
$GLOBALS["country"]["219"]  = "Turkmenistan";
$GLOBALS["country"]["220"]  = "Turks and Caicos Islands";
$GLOBALS["country"]["221"]  = "Tuvalu";
$GLOBALS["country"]["222"]  = "Uganda";
$GLOBALS["country"]["223"]  = "UK";
$GLOBALS["country"]["224"]  = "Ukraine";
$GLOBALS["country"]["225"]  = "United Arab Emirates";
$GLOBALS["country"]["226"]  = "United Republic of Tanzania";
$GLOBALS["country"]["227"]  = "United States Virgin Islands";
$GLOBALS["country"]["228"]  = "Uruguay";
$GLOBALS["country"]["229"]  = "USA";
$GLOBALS["country"]["230"]  = "Uzbekistan";
$GLOBALS["country"]["231"]  = "Vanuatu";
$GLOBALS["country"]["232"]  = "Venezuela";
$GLOBALS["country"]["233"]  = "Viet Nam";
$GLOBALS["country"]["234"]  = "Wallis and Futuna Islands";
$GLOBALS["country"]["235"]  = "Western Sahara";
$GLOBALS["country"]["236"]  = "Yemen";
$GLOBALS["country"]["237"]  = "Zambia";
$GLOBALS["country"]["238"]  = "Zimbabwe";
//$GLOBALS["country"]["239"]  = "Others";


//連絡先（日本語表記）
$GLOBALS["contact_J"]["1"] = "勤務先";
$GLOBALS["contact_J"]["2"] = "自宅";

//連絡先（英語表記）
$GLOBALS["contact_E"]["1"] = "Office";
$GLOBALS["contact_E"]["2"] = "Home";

//会員・非会員（日本語表記）
$GLOBALS["member_J"]["1"] = "会員";
$GLOBALS["member_J"]["2"] = "非会員";


//会員・非会員（英語表記）
$GLOBALS["member_E"]["1"] = "Yes";
$GLOBALS["member_E"]["2"] = "No";

//共著者の有無（日本語表記）
$GLOBALS["coAuthor_J"]["1"] = "有";
$GLOBALS["coAuthor_J"]["2"] = "無";

//共著者の有無（英語表記）
$GLOBALS["coAuthor_E"]["1"] = "Yes";
$GLOBALS["coAuthor_E"]["2"] = "No";

//自動発行パスワード
define("PASSWD_LEN", 8);
define("PASSWD_TYPE", "alnum");

//エントリーデータステータス
$GLOBALS["entryStatusList"]["0"]  = "";
$GLOBALS["entryStatusList"]["1"]  = "採択";
$GLOBALS["entryStatusList"]["2"]  = "不採択";
$GLOBALS["entryStatusList"]["4"]  = "削除";
$GLOBALS["entryStatusList"]["5"]  = "";
$GLOBALS["entryStatusList"]["99"] = "キャンセル";

//可・不可
$GLOBALS["proprietyList"]["1"] = "可";
$GLOBALS["proprietyList"]["2"] = "不可";


//Prof.・Dr.・Mr.・Ms
$GLOBALS["titleList"]["1"] = "Prof.";
$GLOBALS["titleList"]["2"] = "Dr.";
$GLOBALS["titleList"]["3"] = "Mr.";
$GLOBALS["titleList"]["4"] = "Ms.";




//-----------------------------------------
//決済画面
//-----------------------------------------
//支払方法（これから増えます）
$GLOBALS["method_J"]["1"] = "クレジットカード";
$GLOBALS["method_J"]["2"] = "銀行振込";

$GLOBALS["method_E"]["1"] = "Credit Card";
$GLOBALS["method_E"]["2"] = "Bank Transfer";

//カードの種類
$GLOBALS["card_type_J"]["1"] = "VISA";
$GLOBALS["card_type_J"]["2"] = "MasterCard";
$GLOBALS["card_type_J"]["3"] = "Diners Club";
$GLOBALS["card_type_J"]["4"] = "AMEX";
$GLOBALS["card_type_J"]["5"] = "JCB";

$GLOBALS["card_type_E"]["1"] = "VISA";
$GLOBALS["card_type_E"]["2"] = "MasterCard";
$GLOBALS["card_type_E"]["3"] = "Diners Club";
$GLOBALS["card_type_E"]["4"] = "AMEX";
$GLOBALS["card_type_E"]["5"] = "JCB";

//支払回数
$GLOBALS["jpo1_J"]["10"] = "一括払い(支払回数の設定は不要)";
$GLOBALS["jpo1_J"]["61"] = "分割払い(支払回数を設定してください)";
$GLOBALS["jpo1_J"]["80"] = "リボ払い(支払回数の設定は不要)";

$GLOBALS["jpo1_E"]["10"] = "";
$GLOBALS["jpo1_E"]["61"] = "";
$GLOBALS["jpo1_E"]["80"] = "";

// 決済エラー
$GLOBALS["payerror_J"] = "決済処理中にエラーが発生しました。";
$GLOBALS["payerror_E"] = "Credit Card Transaction Error";

$GLOBALS["3dpayerror_J"] = "決済処理中にエラーが発生しました。";
$GLOBALS["3dpayerror_E"] = "Credit Card Transaction Error";


// お支払い状態
$GLOBALS["paymentstatusList"]["1"] = "与信待ち";
$GLOBALS["paymentstatusList"]["2"] = "与信済み";
$GLOBALS["paymentstatusList"]["3"] = "売上済み";
$GLOBALS["paymentstatusList"]["4"] = "入金待ち";
$GLOBALS["paymentstatusList"]["5"] = "入金済み";
$GLOBALS["paymentstatusList"]["6"] = "キャンセル待ち";
$GLOBALS["paymentstatusList"]["7"] = "キャンセル済み";
$GLOBALS["paymentstatusList"]["99"] = "請求無し";


// メッセージ
$GLOBALS["msg"]["err_require_input"]	= "%sを入力してください。";
$GLOBALS["msg"]["err_require_select"]	= "%sを選択してください";
$GLOBALS["msg"]["err_require_check"]	= "%sをチェックしてください。";
$GLOBALS["msg"]["err_numeric"]			= "%sは数字で入力してください。";
$GLOBALS["msg"]["err_han_numeric"]		= "%sは、半角数字で入力してください。";
$GLOBALS["msg"]["err_zen_numeric"]		= "%sは、全角数字で入力してください。";
$GLOBALS["msg"]["err_han_alphanumeric"]	= "%sは、半角英数字で入力してください。";
$GLOBALS["msg"]["err_zenkana"]			= "%sは全角カタカナで入力してください。";
$GLOBALS["msg"]["err_han_alphanumericmark"]= "%sは、半角英数記号で入力してください。";
$GLOBALS["msg"]["err_zen_alphanumeric"]	= "%sは、全角英数字で入力してください。";
$GLOBALS["msg"]["err_zen"]				= "%sは、全角で入力してください。";
$GLOBALS["msg"]["err_byte"]				= "%sは、%sbyte以内で入力してください。";
$GLOBALS["msg"]["err_mail"]				= "%sは、正しいメールアドレスではありません。";
$GLOBALS["msg"]["err_strlen"]			= "%sは、%s文字で入力してください。";
$GLOBALS["msg"]["err_strlen_max"]		= "%sは、%s文字以内で入力してください。";
$GLOBALS["msg"]["err_strlen_between"]	= "%sは、%s文字から%s文字で入力してください。";
$GLOBALS["msg"]["err_over_max_filesize"]= "%sのファイルサイズが大きすぎます。　%sバイト以下のファイルを選択してください。";
$GLOBALS["msg"]["err_suffix_file"]		= "%sのファイル拡張子はアップロードできません。　アップロード可能な拡張子は%sです。";
$GLOBALS["msg"]["err_format"]			= "%sを確認してください。";
$GLOBALS["msg"]["err_format_han_numeric_haifun"]= "%sを確認してください。(半角数字、ハイフン付きで入力ください)";
$GLOBALS["msg"]["err_format_han_alphanumeric"]	= "%sを確認してください。(半角英数字で入力してください)";
$GLOBALS["msg"]["err_over_lap"]			= "入力した%sは、既に登録済みです。";
$GLOBALS["msg"]["err_not_exist"]		= "%sが見つかりません。";
$GLOBALS["msg"]["err_require_parameter"]= "処理に必要な情報が不足しています。お手数ですが最初からやり直してください。";
$GLOBALS["msg"]["err_db_system"]		= "只今、システムメンテナンス中です。しばらく経ってから再度アクセスしてください。";
$GLOBALS["msg"]["err_word_cnt"]		= "%sは、単語数を%s以内で入力してください。";
$GLOBALS["msg"]["err_tokusyu"]		= "%sに、機種依存文字が含まれています。";


$GLOBALS["msg"]["msg_login"]			= "ID、パスワードを入力して、ログインしてください。";
$GLOBALS["msg"]["msg_logout"]			= "ログアウト完了。ご利用ありがとうございました。";

$GLOBALS["msg"]["err_not_loginid"]		= "ログインID又は、パスワードが間違っています。";
$GLOBALS["msg"]["err_mistake_password"]	= "ログインID又は、パスワードが間違っています。";
$GLOBALS["msg"]["err_login_status"]		= "ログイン権限がありません。";

$GLOBALS["msg"]["validDate_start"]		= "受付開始前のため、ログインできません。受付開始日は%sです。";
$GLOBALS["msg"]["validDate_end"]		= "受付期間は%sに終了いたしました。";
$GLOBALS["msg"]["validDate_middle"]		= "一次受付期間は終了いたしました。二次受付は%sからとなります。";
$GLOBALS["msg"]["dateformat"]			= "Y年m月d日H時";

$GLOBALS["msg"]["login_id"]				= "ログインID";
$GLOBALS["msg"]["login_passwd"]			= "パスワード";

$GLOBALS["msg"]["btn_login"]			= "ログイン";
$GLOBALS["msg"]["btn_logout"]			= "ログアウト";
$GLOBALS["msg"]["btn_return"]			= "戻る";
$GLOBALS["msg"]["btn_next"]				= "次へ";
$GLOBALS["msg"]["btn_regist"]			= "登録";
$GLOBALS["msg"]["btn_update"]			= "更新";
$GLOBALS["msg"]["btn_send"]				= "送信";

$GLOBALS["msg"]["btn_change_fee"]	= "支払方法の変更を行う";
$GLOBALS["msg"]["btn_change_payment"]	= "決済情報の変更を行う";

$GLOBALS["msg"]["reload"] = "既に登録は完了しています。";

// システムメンテナンス用メッセージ
$GLOBALS["msg"]["maintenance"] = 'システムメンテナンス中のためシステムを一時停止しております。<br/>ご迷惑をおかけいたしますが、何卒ご理解賜りますようお願い申し上げます。';

//必須マーク
$GLOBALS["msg"]["need_mark"]				= "＊";



// 金額自動変更
$GLOBALS["auto_change_disp"]["1"] = "earlyのみ";
$GLOBALS["auto_change_disp"]["2"] = "early+late";

// 登録カテゴリの文言
$GLOBALS["regist_j"]["0"] = "事前登録料金";
$GLOBALS["regist_j"]["1"] = "当日登録料金";

$GLOBALS["use"]["0"] = "利用しない";
$GLOBALS["use"]["1"] = "利用する";

$GLOBALS["regist"]["0"] = "Early Registration";
$GLOBALS["regist"]["1"] = "Late / On-site Registration";


// 同意文言
$GLOBALS["msg"]["agree1"] = "同意する";
$GLOBALS["msg"]["agree2"] = "同意しない";
// チェックボックス用の同意分
$GLOBALS["msg"]["agreechk"] = "同意する";

// ブラウザバックの注意書き
$GLOBALS["msg"]["browser_back"]	= "※ブラウザの戻るボタンは使用しないでください。";

// 3Dセキュアに関する文言
$GLOBALS["msg"]["3d_send"]			= "3Dセキュア認証を続行します。<br>認証ボタンをクリックしてください。";
$GLOBALS["msg"]["btn_3d_send"]		= "認証";

$GLOBALS["msg"]["3d_result"]		= 'エントリー完了処理を続行します。<br/>完了ボタンをクリックしてください。';
$GLOBALS["msg"]["btn_3d_result"]	= "完了";

// 確認画面に関する文言
$GLOBALS["msg"]["cofirm_title"]			= "登録内容確認画面";
$GLOBALS["msg"]["cofirm_desc1"]			= "ご登録内容をご確認の上、一番下の「登録」ボタンを押してください。";
$GLOBALS["msg"]["cofirm_desc2"]			= "<b>※この時点ではご登録は完了しておりません。<br />必ず「登録」ボタンを押してください。</b>";
$GLOBALS["msg"]["cofirm_desc_i"]		= '<span style="font-weight: bold;font-size: 17px;">'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';
$GLOBALS["msg"]["cofirm_desc_e"]		= '<span style="font-weight: bold;font-size: 17px;">'.$GLOBALS['msg']['cofirm_title'].'</span><br /><br />'.$GLOBALS['msg']['cofirm_desc1'].'<br /><br /><span id="desc" class="red">'.$GLOBALS['msg']['cofirm_desc2'].'</span>';

// Usr:アカウントロック,二重ログイン
$GLOBALS["msg"]["account_lock"] = "規程回数ログインに失敗したため、アカウントがロックされました。";
$GLOBALS["msg"]["account_same"] = "既に同じIDでログイン中です。";

// 編集メール 更新された項目がない場合
$GLOBALS["msg"]["edit_mail_non"] = "更新された項目はありません。\n\n";
$GLOBALS["msg"]["edit_non"] = "";

// CSVのヘッダ
$GLOBALS["csv"]["entryno"] = "登録No.";
$GLOBALS["csv"]["status"]  = "状態";
$GLOBALS["csv"]["rdate"]   = "エントリー登録日";
$GLOBALS["csv"]["udate"]   = "エントリー更新日";


// Usr:登録経路
define("ENTRY_WAY_USER", 0);
define("ENTRY_WAY_ADMIN", 1);

define("ENTRY_WAY_USER_NAME" , "オンライン");
define("ENTRY_WAY_ADMIN_NAME", "FAX");


// パスワード再発行に関するメッセージ
$GLOBALS["msg"]["passwd_user_id"]      = "登録No";
$GLOBALS["msg"]["passwd_mailaddress"]  = "メールアドレス";
$GLOBALS["msg"]["passwd_btn"]          = "再発行";
$GLOBALS["msg"]["passwd_err_not_auth"] = "登録No又は、メールアドレスが間違っています。";

// クレジットカードの同意に関するエラー
$GLOBALS["msg"]["credit"]["agree"] = "以下をご覧になり同意の上お進みください。";

?>
