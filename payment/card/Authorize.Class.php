<?php
/*
 * クレジットカード決済 与クラス
 *
 */


define('INPUT_PAGE', 'Authorize.php');

define('TXN_FAILURE_CODE', 'failure');
define('TXN_PENDING_CODE', 'pending');
define('TXN_SUCCESS_CODE', 'success');
define('TRUE_FLAG_CODE', 'CAPTURE');
define('FALSE_FLAG_CODE', 'AUTH');

define('ERROR_PAGE_TITLE', 'System Error');
define('NORMAL_PAGE_TITLE', '取引結果');

$form_id = $_REQUEST["form_id"];
require_once(TGMDK_COPY_DIR."/".$form_id."/config.php");


class Authorize{

    private $objOutput;

    /**
     * コンストラクタ
     */
    function __construct(){

        // パッケージ格納ディレクトリ
        ini_set("include_path", ini_get("include_path").":".TGMDK_ORG_DIR);
        // jtbマルチペイメント必要モジュールの読み込み
        require_once( 'com/gmo_pg/client/input/EntryTranInput.php');
        require_once( 'com/gmo_pg/client/input/ExecTranInput.php');
        require_once( 'com/gmo_pg/client/input/EntryExecTranInput.php');
        require_once( 'com/gmo_pg/client/tran/EntryExecTran.php');

        // エラーハンドラ
        include_once(ROOT_DIR.'payment/card/ErrorMessageHandler.php');
    }


    /**
     * 決済処理
     */
    function card_exec($obj){
        $eid       = $obj->order_number;
        $ps_head   = $obj->formdata["head"];
        $form_lang = $obj->formdata["lang"];
        $pn_price  = $obj->total_price;
        $chkstring = $obj->o_entry->makePasswd(PASSWD_LEN, PASSWD_TYPE);
//        $pn_entry_number = $obj->entry_number;

        //------------------------------------
        //セッション情報取得
        //------------------------------------
        $wk_sesssion = $GLOBALS["session"]->getVar("form_parampay");
        $arrForm3    = $GLOBALS["session"]->getVar("form_param3");   // グループ3情報
        $client_field3 = $GLOBALS["session"]->getVar("workDir").",".$arrForm3['hd_file51'].",".$arrForm3['hd_file52'].",".$arrForm3['hd_file53'];


        // 取引IDにeidを利用する *欠番対策
        $wk_order_no = $eid;
        $this->order_id = $ps_head."-".sprintf("%05d", $wk_order_no);

        //支払金額
        $this->payment_amount = (string)$pn_price;

        //与信方法
        $this->is_with_capture = "1";    //売上まで上げるか？
        if ("1" == $this->is_with_capture) {
            $this->is_with_capture = TRUE_FLAG_CODE;
        } else {
            $this->is_with_capture = FALSE_FLAG_CODE;
        }

        // カード番号
        $this->card_number = $wk_sesssion["cardNumber"];

        //カード有効期限 YYMM
        $this->card_expire = substr($wk_sesssion["cardExpire2"],2,2).$wk_sesssion["cardExpire1"];

        //セキュリティーコード
        $this->security_code = $wk_sesssion["securityCode"];


        //-----------------------------------
        // 取引登録時に必要なパラメータ
        //-----------------------------------
        $entryInput = new EntryTranInput();
        $entryInput->setShopId(PGCARD_SHOP_ID);
        $entryInput->setShopPass(PGCARD_SHOP_PASS);
        $entryInput->setJobCd($this->is_with_capture);
        $entryInput->setItemCode("");
        $entryInput->setOrderId($this->order_id);
        $entryInput->setAmount($this->payment_amount);
        // 3Dセキュア
        $entryInput->setTdFlag(TdFlag);
        $entryInput->setTdTenantName(TdTenantName);


        //-----------------------------------
        // 決済実行のパラメータ
        //-----------------------------------
        $execInput = new ExecTranInput();
        $execInput->setOrderId($this->order_id);
        $execInput->setMethod(1);   // 支払方法：一括
        $execInput->setCardNo($this->card_number);
        $execInput->setExpire($this->card_expire);
        $execInput->setSecurityCode($this->security_code);
        $execInput->setHttpUserAgent($_SERVER['HTTP_USER_AGENT']);
        $execInput->setHttpAccept($_SERVER['HTTP_ACCEPT' ]);
        // 任意項目
        $execInput->setClientField1(mb_convert_encoding($obj->ins_eid, 'SJIS' , SCRIPT_ENC));
//        $execInput->setClientField2( mb_convert_encoding( $_POST['ClientField2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
//        $execInput->setClientField3( mb_convert_encoding( $_POST['ClientField3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );


        //------------------------------------------------
        // 実施
        //------------------------------------------------
        $input = new EntryExecTranInput();
        $input->setEntryTranInput($entryInput);
        $input->setExecTranInput($execInput);

        // API通信クラスをインスタンス化します
        $exe = new EntryExecTran();

        // パラメータオブジェクトを引数に、実行メソッドを呼びます。
        // 正常に終了した場合、結果オブジェクトが返るはずです。
        $output = $exe->exec($input);

        // 成功フラグ
        $wk_ret = true;

        // 戻り値配列
        $wk_ret_arr = array();

        // 通信エラー等の場合、例外が発生します。
        if( $exe->isExceptionOccured() ){
            $arrErr = array();
            $arrErr[] = "決済サービスとのデータの送受信に失敗しました。";

            $auth_log  = print_r($exe, true);
//            $auth_log .= print_r($input, true); カード情報が含まれている
            $auth_log .= print_r($output, true);
            error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");

            $wk_ret = false;
            $wk_ret_arr['arrErr']    = $arrErr;
            $wk_ret_arr['order_id']  = $this->order_id;
            $wk_ret_arr['access_id'] = $output->getAccessId();
            $wk_ret_arr['client_field3']  = $client_field3;
            $wk_ret_arr['invalid_flg']    = 3; // 予期せぬエラー

        }else{
            
            //例外が発生していない場合、出力パラメータオブジェクトが戻ります。
            // エラー発生
            if( $output->isErrorOccurred() ){
                $arrErr = array();
                $errorHandle = new ErrorHandler($form_lang);
                $errorList = $output->isEntryErrorOccurred()
                           ? $output->getEntryErrList()
                           : $output->getExecErrList();

                foreach($errorList as $_key => $errorInfo){
                    // $errorInfo->getErrCode()
                    $arrErr[] = $errorInfo->getErrInfo().':'.$errorHandle->getMessage($errorInfo->getErrInfo());
                }

                $wk_ret = false;
                $wk_ret_arr['arrErr']    = $arrErr;
                $wk_ret_arr['order_id']  = $this->order_id;
                $wk_ret_arr['access_id'] = $output->getAccessId();
                $wk_ret_arr['client_field3']  = $client_field3;
                $wk_ret_arr['invalid_flg']    = 2; // 例外が発生した場合

            // 正常終了
            }else{
                $wk_ret = true;
                $wk_ret_arr['order_id']     = $output->getOrderId();
                $wk_ret_arr['access_id']    = $output->getAccessId();
                $wk_ret_arr['access_pass']  = $output->getAccessPass();
                $wk_ret_arr['method']       = $output->getMethod();
                $wk_ret_arr['pay_times']    = $output->getPayTimes();
                $wk_ret_arr['tran_date']    = $output->getTranDate();
                $wk_ret_arr['forward']      = $output->getForward();     // 仕向先カード会社
                $wk_ret_arr['approve']      = $output->getApprovalNo();  // 承認番号
                $wk_ret_arr['tran_id']      = $output->getTranId();      // トランザクションID
                $wk_ret_arr['check_string'] = $output->getCheckString(); // チェック文字列
                // 加盟店自由項目1
                $wk_ret_arr['client_field1'] = htmlspecialchars( mb_convert_encoding( $output->getClientField1() , SCRIPT_ENC , 'SJIS') );
                $wk_ret_arr['client_field2'] = htmlspecialchars( mb_convert_encoding( $output->getClientField2() , SCRIPT_ENC , 'SJIS') );
                $wk_ret_arr['client_field3']  = $client_field3;
                $wk_ret_arr['invalid_flg']    = 1; // 正常終了

                // 3Dセキュアの場合はNULLなので送信したorder_idを明示的にセット
                if(is_null($wk_ret_arr['order_id'])){
                    $wk_ret_arr['order_id'] = $this->order_id;
                }
            }

            //例外発生せず、エラーの戻りもなく、3Dセキュアフラグもオフであるので、実行結果を表示します。
        }

        // 戻りパラメータ
        $this->objOutput = $output;

        //---------------------------
        // ログメッセージ
        //---------------------------
        $inputLog  = $entryInput->toString();
        $outputLog = '';
        foreach($wk_ret_arr as $_key => $_val){
            $outputLog .= $_key.'='.$_val.'&';
        }
        $outputLog = trim($outputLog, '&');
        $GLOBALS["log"]->write_log('PAYMENT', $inputLog .'&'. $outputLog. '&eid = '. $obj->ins_eid);


        //-----------------------------------------
        // 検査用に決済取引番号をセッションに格納
        //-----------------------------------------
        $GLOBALS["session"]->setVar("order_id", $this->order_id);

        //-------------------------------------
        //実行結果を返す
        //-------------------------------------
        return array($wk_ret, $wk_ret_arr);
    }


    function isTdSecure(){
        return $this->objOutput->isTdSecure();
    }

    function getAcsUrl(){
        return $this->objOutput->getAcsUrl();
    }

    function getMd(){
        return $this->objOutput->getAccessId();
    }

    function getPaReq(){
        return $this->objOutput->getPaReq();
    }

    function getTermUrl(){
        return PGCARD_URL;
    }


    /**
     * 決済取引IDの与信結果を変化yくする
     * 
     * @param  string  $order_id  決済取引ID
     * @param  string  $shop_id   店舗ID
     * @param  string  $shop_pass 店舗パスワード
     * @return boolean true:
     */
    function isCapture($order_id, $shop_id=PGCARD_SHOP_ID, $shop_pass=PGCARD_SHOP_PASS){
        // 与信確認パッケージ
        require_once( 'com/gmo_pg/client/input/SearchTradeInput.php');
        require_once( 'com/gmo_pg/client/tran/SearchTrade.php');

        $input = new SearchTradeInput();
        $input->setShopId($shop_id);
        $input->setShopPass($shop_pass);
        $input->setOrderId($order_id);

        $exe = new SearchTrade();
        $output = $exe->exec($input);

        $arrErr = array();

        // 結果確認
        if( $exe->isExceptionOccured()){
            $arrErr[] = "予期しないエラー:".__FUNCTION__;

            $auth_log  = print_r($exe, true);
            $auth_log .= print_r($input, true);
            $auth_log .= print_r($output, true);
            error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");

        }else{
            //例外が発生していない場合、出力パラメータオブジェクトが戻ります。
            if( $output->isErrorOccurred() ){//出力パラメータにエラーコードが含まれていないか、チェックしています。
                $errorHandle = new ErrorHandler($form_lang);
                $errorList = $output->getErrList();

                foreach($errorList as $_key => $errorInfo){
                    // $errorInfo->getErrCode()
                    $arrErr[] = $errorInfo->getErrInfo().':'.$errorHandle->getMessage($errorInfo->getErrInfo());
                    if($errorInfo->getErrInfo() == "E21020002"){
                        $isCancel = true;
                    }
                }

                $auth_log  = print_r($exe, true);
                $auth_log .= print_r($input, true);
                $auth_log .= print_r($output, true);
                $auth_log .= print_r($arrErr, true);
                error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");
            }

            //例外発生せず、エラーの戻りもないので、正常とみなします。
            //このif文を抜けて、結果を表示します。
        }

        // エラーなしかつ状態がCAPTUREであれば与信済み
        return count($arrErr) == 0 && $output->status == "CAPTURE";
    }

}


