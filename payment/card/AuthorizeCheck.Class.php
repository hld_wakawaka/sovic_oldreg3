<?php
/*
 * クレジットカード決済 決済取引番号チェッククラス
 *
 */

$form_id = $_REQUEST["form_id"];
require_once(TGMDK_COPY_DIR."/".$form_id."/config.php");

class AuthorizeCheck {

    private $objOutput;

    /**
     * コンストラクタ
     */
    function __construct(){

        // パッケージ格納ディレクトリ
        ini_set("include_path", ini_get("include_path").":".TGMDK_ORG_DIR);
        // jtbマルチペイメント必要モジュールの読み込み
        require_once( 'com/gmo_pg/client/input/SearchTradeInput.php');
        require_once( 'com/gmo_pg/client/tran/SearchTrade.php');

        // エラーハンドラ
        include_once(ROOT_DIR.'payment/card/ErrorMessageHandler.php');
    }


    /**
     * 決済取引番号の重複チェック
     */
    function checkOrderId($obj, $order_id){

        //-----------------------------------
        // チェックに必要なパラメータ
        //-----------------------------------
        $input = new SearchTradeInput();
        $input->setShopId(PGCARD_SHOP_ID);
        $input->setShopPass(PGCARD_SHOP_PASS);
        $input->setOrderId($order_id);


        // API通信クラスをインスタンス化します
        $exe = new SearchTrade();


        // パラメータオブジェクトを引数に、実行メソッドを呼びます。
        // 正常に終了した場合、結果オブジェクトが返るはずです。
        $output = $exe->exec($input);

        // 通信エラー等の場合、例外が発生します。
        if( $exe->isExceptionOccured() ){
            $arrErr = array();
            $arrErr[] = "決済サービスとのデータの送受信に失敗しました。";

            $auth_log  = print_r($exe, true);
//            $auth_log .= print_r($input, true); カード情報が含まれている
            $auth_log .= print_r($output, true);
            error_log("\n".date("Y-m-d H:i:s")."|".$auth_log, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");
            

        }else{
            $arrErr   = $output->getErrList();

            if($output->isErrorOccurred()){
                foreach($arrErr as $_key => $errorInfo){
                    if($errorInfo->getErrInfo() == "E01110002"){
                        return true;
                    }
                }
                $msg = "エラーのためスキップ>".$order_id;
                error_log("\n".date("Y-m-d H:i:s")."|".$msg, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");
                return false;

            // 正常終了
            }else{
                if(strlen($output->getOrderId()) == 0 && count($arrErr) == 0) return true;

                $msg = "発行済みのためスキップ>".$order_id;
                error_log("\n".date("Y-m-d H:i:s")."|".$msg, 3, LOG_TGMDK_DIR."tgmdk".date("Ymd").".log");
                return false;
            }
        }

        // 重複なし
        return true;
    }

//        // 決済取引IDの重複チェック
//        include_once(ROOT_DIR.'payment/card/AuthorizeCheck.Class.php');
//
//        $order_id = "IPEC-salon-1-00111";
//        $objChk = new AuthorizeCheck();
//        $check = $objChk->checkOrderId($obj, $order_id);
//        if(!$check){
//            // 決済取引IDが重複しているので取り直し
//        }


}


