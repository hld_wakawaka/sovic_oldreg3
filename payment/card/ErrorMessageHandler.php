<?php
/**
 * エラーメッセージハンドラ
 * 
 * エラー詳細(ErrInfo)を、対応する日本語メッセージに置換します。
 *
 */
class ErrorHandler{
    
    var $lang;
    var $messageMap1 = array(); // 日本語エラー配列
    var $messageMap2 = array(); // 英語エラー配列
    
    /**
     * $lang integer
     *  jp : 1
     *  en : 2
     */
    function ErrorHandler($lang=1){
        $this->lang = $lang;

        // 日英のシステムエラー
        $this->systemError1 = 'システムエラーのためカード決済が完了されませんでした。お手数ですが、システム担当者（onregmaster@ics-inc.co.jp） までご連絡ください。';
        $this->systemError2 = '"Due to a system failure, the credit-card transaction was not completed. Please contact onregmaster@ics-inc.co.jp."';

        $this->messageMap1 = array(
            'E41170002'=>'入力されたカードの会社には対応しておりません。別のカード番号を入力して下さい。（VISA, MasterCard, Diners Club, AMEX, JCBがご利用いただけます）',
            'E11010001'=>'この取引は既に決済が終了しています。',
            'E11010002'=>'この取引は決済が終了していませんので、変更する事が出来ません。',
            'E01480008'=>'カード名義人の入力書式が正しくありません。（半角アルファベットのみ）',
            'E21020002'=>'3Dセキュア認証がキャンセルされました。カード情報をご確認の上、再度ご登録ください。',
            'E82010001'=>'実行中にエラーが発生しました。処理は開始されませんでした。',
            'E90010001'=>'現在処理を行っているのでもうしばらくお待ち下さい。',
            '42G020000'=>'カード残高が不足しているために、決済が完了できませんでした。',
            '42G030000'=>'カード限度額を超えているために、決済が完了できませんでした。',
            '42G550000'=>'カード限度額を超えているために、決済が完了できませんでした。',
            '42G040000'=>'カード残高が不足しているために、決済が完了できませんでした。',
            '42G050000'=>'カード限度額を超えているために、決済が完了できませんでした。',
            'E01460008'=>'セキュリティコードの書式が正しくありません。',
            '42G440000'=>'セキュリティーコードが誤っていた為に、決済を完了する事が出来ませんでした。',
            '42G450000'=>'セキュリティーコードが入力されていない為に、決済を完了する事が出来ませんでした。',
            'E01170003'=>'カード番号が最大文字数を超えています。',
            'E01170006'=>'カード番号に数字以外の文字が含まれています。',
            'E01170011'=>'カード番号が10桁～16桁の範囲ではありません。',
            'E41170099'=>'カード番号に誤りがあります。再度確認して入力して下さい。',
            '42G650000'=>'カード番号に誤りがあるために、決済を完了できませんでした。',
            'E01180001'=>'有効期限が指定されていません。',
            'E01180003'=>'有効期限が4桁ではありません。',
            'E01180006'=>'有効期限に数字以外の文字が含まれています。',
            '42G830000'=>'有効期限に誤りがあるために、決済を完了できませんでした。',
            'E21010001'=>'3Dセキュア認証に失敗しました。カード情報をご確認の上、再度ご登録ください。',
            'E21010007'=>'3Dセキュア認証に失敗しました。カード情報をご確認の上、再度ご登録ください。',
            'E21010999'=>'3Dセキュア認証に失敗しました。カード情報をご確認の上、再度ご登録ください。',
            'E21020001'=>'3Dセキュア認証に失敗しました。カード情報をご確認の上、再度ご登録ください。',
            'E21020007'=>'3Dセキュア認証に失敗しました。カード情報をご確認の上、再度ご登録ください。',
            'E21020999'=>'3Dセキュア認証に失敗しました。カード情報をご確認の上、再度ご登録ください。',
            'E21010201'=>'このカードでは取引をする事が出来ません。3Dセキュア認証に対応したカードをお使い下さい。',
            'E21010202'=>'このカードでは取引をする事が出来ません。3Dセキュア認証に対応したカードをお使い下さい。',

            // 追加依頼
            '42G540000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G560000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G600000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G610000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G950000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G960000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G970000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G980000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G990000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G120000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G220000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',
            '42G300000'=>'本カードでの決済がカード会社より承認されませんでした。お手数ですが、カード会社にご確認いただくか、別のカードでの登録手続きをお願い申し上げます。',

            // 2015.01.29 追加
            '42G420000'=>'暗証番号が誤っていた為に、決済を完了する事が出来ませんでした。',

            // システムエラー系
            'E91020001'=>'通信タイムアウトが発生しました。申し訳ございませんが、後ほど改めて再登録いただくか、システム担当者（onregmaster@ics-inc.co.jp）までご連絡ください。',
            'E92000001'=>'通信タイムアウトが発生しました。申し訳ございませんが、後ほど改めて再登録いただくか、システム担当者（onregmaster@ics-inc.co.jp）までご連絡ください。',
        );



        $this->messageMap2 = array(
            'E41170002'=>'This credit card cannot be used for payment. We accept only the following credit cards: VISA, MasterCard, Diners Club, AMEX, JCB.',
            'E11010001'=>'This credit card transaction has been already completed.',
            'E11010002'=>'This credit card transaction is under processing and cannot be changed.',
            'E01480008'=>"Please input Cardholder's name with one-byte alphabetic charater.",
            'E21020002'=>'3D authentication cancelled. Please go through the payment procedure once again.',
            'E82010001'=>'Error has been occurred and credit card transaction has not been started. Please go through the payment procedure once again.',
            'E90010001'=>'Credit card transaction is under processing. Please wait for a moment.',
            '42G020000'=>'This credit card cannot be used for payment. Please check your credit card balance.',
            '42G030000'=>'This credit card cannot be used for payment. Please check your credit card limit.',
            '42G550000'=>'This credit card cannot be used for payment. Please check your credit card limit.',
            '42G040000'=>'This credit card cannot be used for payment. Please check your credit card balance.',
            '42G050000'=>'This credit card cannot be used for payment. Please check your credit card limit.',
            'E01460008'=>'Please input Security Code with one-byte numeric charater.',
            '42G440000'=>'Security code you have entered is invalid.',
            '42G450000'=>'Security code has not been entered.',
            'E01170003'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E01170006'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E01170011'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E41170099'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            '42G650000'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E01180001'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E01180003'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E01180006'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            '42G830000'=>'Credit card information entered invalid. Please check information entered. (number, expire date, etc.).',
            'E21010001'=>'3D authentication failed. Please check the credit card information you have entered and try again.',
            'E21010007'=>'3D authentication failed. Please check the credit card information you have entered and try again.',
            'E21010999'=>'3D authentication failed. Please check the credit card information you have entered and try again.',
            'E21020001'=>'3D authentication failed. Please check the credit card information you have entered and try again.',
            'E21020007'=>'3D authentication failed. Please check the credit card information you have entered and try again.',
            'E21020999'=>'3D authentication failed. Please check the credit card information you have entered and try again.',
            'E21010201'=>'This credit card cannot be used. Please use the credit card that is enrolled for 3D secure.',
            'E21010202'=>'This credit card cannot be used. Please use the credit card that is enrolled for 3D secure.',

            // 追加依頼
            '42G540000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G560000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G600000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G610000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G950000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G960000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G970000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G980000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G990000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G120000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G220000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',
            '42G300000'=>'Your credit card transaction was declined by the credit card company. Please check with your credit card company or use a different credit card.',

            // 2015.01.29 追加
            '42G420000'=>'The password is invalid. Your payment has been failed. ',

            // システムエラー系
            'E91020001'=>'"A system failure has disabled the credit-card transaction function. Please try again later or contact onregmaster@ics-inc.co.jp."',
            'E92000001'=>'"A system failure has disabled the credit-card transaction function. Please try again later or contact onregmaster@ics-inc.co.jp."',
            'E01110010'=>'"Due to a system failure, the credit-card transaction has not been completed. Please contact onregmaster@ics-inc.co.jp."',
        );
    }


    /* エラーコードよりエラーメッセージを返す(言語別) */
    function getMessage( $errorInfo ){
        $messageMap = 'messageMap'.$this->lang;
        if( array_key_exists( $errorInfo , $this->{$messageMap} )){
            return $this->{$messageMap}[ $errorInfo ];
        }

        // 上記以外はシステムエラーとする
        $systemError = 'systemError'.$this->lang;
        return $this->{$systemError};
    }
}
?>