<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_modifier_show_amount($string, $arrValue = '')
{

    if($string == "") return;

    $string = preg_replace("/^事前登録料金(\(|\（)/u", "", $string);
    $string = preg_replace("/(\)|\）)$/u", "", $string);

    return $string;
}

/* vim: set expandtab: */

?>
