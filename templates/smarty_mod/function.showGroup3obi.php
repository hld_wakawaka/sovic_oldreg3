<?php

/**
 * Smarty plugin
 * 各項目のカスタマイズ用の項目ID配列をassign
 * 
 * @package Smarty
 * @subpackage plugins
 */
function smarty_function_showGroup3obi($args, &$smarty) {

    // 現在のグループ詳細番号
    $group3no = $args["group3no"];
    // 0:input, 1:confirm
    $type     = $args["type"];

    // assign変数
    $params  = $smarty->get_template_vars();
    $preGroup3No = isset($params['group3_no']) ? $params['group3_no'] : 0;
    $title       = $params["formData"]["arrgroup3"];
    $word7       = $params["formWord"]["word7"];
    $isUseGroup3 = $params["isUseGroup3"];

    // ファイルアップロード1は必須のみ表示判定をする
    if($params["item_id"] == 51){
        $filecheck= in_array($params["arrForm"]["amount"], $params["filecheck"]);
        if(!$filecheck) return "";
    }

    $isShowObi3 = 0;
    if($type == 0){
        for($i=1; $i<=3; $i++){
            if($isUseGroup3[$i] > 0){
                $isShowObi3 = $i;
                break;
            }
        }
    }


    if($preGroup3No != $group3no){
        $preGroup3No = $group3no;

        print '</table>';
        // 前のブロックを利用する場合に改行
        if($params['isUseGroup3'][$preGroup3No-1] > 0){
            print '<br/>';
        }

        if(strlen($title[$preGroup3No-1]) > 0){
            print '<div class="bname">'. $title[$preGroup3No-1] .'</div>';
        }


        // ブロック3説明
        if($type == 0 && $group3no == $isShowObi3 && $word7 != ""){
            print '<div class="bcomment">'.nl2br($word7).'</div>';
        }

        print '<style>#tbl3 {display:none;}</style>';
        print '<table class="rtbl" id="tbl3_'.$group3no.'">';
    }

    $smarty->assign("group3_no", $preGroup3No);
    return;
}

/* vim: set expandtab: */

?>
