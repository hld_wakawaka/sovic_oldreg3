<?php

/**
 * Smarty plugin
 * 各項目のカスタマイズ用の項目ID配列をassign
 * Mngの詳細画面用
 * 
 * @package Smarty
 * @subpackage plugins
 */
function smarty_function_setIncFormItemMng($args, &$smarty) {

    $smarty->assign("exitems", array());

    $suffix  = "";

    // assign変数
    $params  = $smarty->get_template_vars();
    $form_id    = $params['form_id'];
    $templateDir= isset($args["template_dir"]) ? $args["template_dir"] : $smarty->template_dir;             // package機能を想定してオーバーライド可
    $customDir  = isset($args["custom_dir"])   ? $args["custom_dir"]   : "Mng/entry/include/".$form_id."/"; // package機能を想定してオーバーライド可
    $includeDir = $templateDir.$customDir;

    // ファイル名
    $parts1 = $form_id."formItem*.html";
    $parts2 = $form_id."formItem([0-9]+)\.html";

    // オーバーライドする項目IDを取得
    $ex = glob($includeDir.$parts1);

    $arrItemIds = array();
    if(is_array($ex)){
        foreach($ex as $_key => $script){
            if(preg_match("/".$parts2."$/u", $script, $match) !== 1) continue;

            $item_id = $match[1];

            // item_idをセット
            $arrItemIds[$item_id] = $script;
        }
    }

    // お支払情報をincludeに含める
    $parts = $form_id."formItemPay.html";
    $ex = glob($includeDir.$parts);
    if(is_array($ex) && count($ex) == 1){
        $arrItemIds["pay"] = $ex[0];
    }

    $smarty->assign("exitems", $arrItemIds);
    return;
}

/* vim: set expandtab: */

?>
