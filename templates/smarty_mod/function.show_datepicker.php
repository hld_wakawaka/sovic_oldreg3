<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_show_datepicker($params, &$smarty){

//    $params  = $smarty->get_template_vars();
    $edata     = $params['edata'];
    $data      = $params['data'];

    // datepicker options
    $format    = !isset($params['format']) ? 'yy/mm/dd' : $params['format'];
    $hide      = !isset($params['hideIfNoPrevNext']) ? true : $params['hideIfNoPrevNext'];
    $button    = !isset($params['button']) ? 'button' : $params['button'];
    $imageOnly = !isset($params['buttonImageOnly']) ? true : $params['buttonImageOnly'];
    $imageUrl  = !isset($params['buttonImage']) ? APP_ROOT."htdocs/css/js/images/calendar.gif" : $params['buttonImage'];
    $attr      = !isset($params['attr']) ? 'readonly' : $params['attr'];
    $size      = !isset($params['size']) ? '10' : $params['size'];


    $datepicker = "
        <span class='datepicker_area'>
            <input type='text' name='{$edata}' id='{$edata}' 
                               value='{$data}' size='{$size}' {$attr} />
            <script>
            $(function(){
                $('#{$edata}').datepicker({
                    dateFormat: '{$format}'
                   ,hideIfNoPrevNext:{$hide}
                   ,showOn: '{$button}'
                   ,buttonImageOnly:{$imageOnly}
                   ,buttonImage: '{$imageUrl}'
                });
            });
            </script>
        </span>";


    return $datepicker;
}

/* vim: set expandtab: */

?>
