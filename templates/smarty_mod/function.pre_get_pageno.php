<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_pre_get_pageno($params, &$smarty) {
	$eid = $params["eid"];
	$arrEid = $params["arrEid"];

    $page_info = array_search($eid, $arrEid);
    if(is_null($page_info)) return;

    $page_no = preg_replace("/\-(.*?)$/u", "", $page_info);
    $smarty->assign("page", $page_no);
    return;
}

/* vim: set expandtab: */

?>
