<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_load_datepicker($str, &$smarty){

    $load  = "";
    $load .=  '<link rel="stylesheet" type="text/css" href="'.APP_ROOT.'htdocs/css/js/jquery.ui.theme.css">';
    $load .=  '<link rel="stylesheet" type="text/css" href="'.APP_ROOT.'htdocs/css/js/jquery.ui.datepicker.css">';
    $load .=  '<script type="text/javascript" src="'.APP_ROOT.'htdocs/js/jquery.ui.core.js"></script>';
    $load .=  '<script type="text/javascript" src="'.APP_ROOT.'htdocs/js/jquery.ui.datepicker.js"></script>';

    return $load;
}

/* vim: set expandtab: */

?>
