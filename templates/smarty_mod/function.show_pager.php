<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_show_pager($params, &$smarty)
{
    $buf = array();
	$param = $params["data"];
	
	if(!$param["allcount"] > 0) return "";
	if($param["allcount"] <= $param["limit"]) return "";
	if(!$param["page"] > 0) $param["page"] = 1;
	
	$allpage = ceil($param["allcount"] / $param["limit"]);
	$startpage = $param["page"] - 5;
	if($startpage < 1) $startpage = 1;
	$endpage = $startpage + 10;
	if($endpage > $allpage) $endpage = $allpage;
	
	$format1 = '<li%s><a href="javascript:f_submit(%s);">%s</a></li>';
	$format2 = '<li%s>%s</li>';
	
	if($param["page"] != 1) {
		$buf[] = sprintf($format1, "", $param["page"]-1, "前へ");
	}
	
	for($i = $startpage; $i <= $endpage; $i++) {
		if($i == $param["page"]) {
			$buf[] = sprintf($format2, ' class="active"', $i);
		} else {
			$buf[] = sprintf($format1, "", $i, $i);
		}
		
	}
	
	if($param["page"] != $allpage && count($buf) > 0) {
		$buf[] = sprintf($format1, "", $param["page"]+1, "次へ");
	}
    
    return '<div class="pageswitch">'.implode(" ", $buf).'</div>';
}

/* vim: set expandtab: */

?>
