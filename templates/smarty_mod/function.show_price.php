<?php

/**
 * Smarty plugin
 * フォームの言語の違いを吸収し金額を表示する
 * 
 * @package Smarty
 * @subpackage plugins
 */
function smarty_function_show_price($args, &$smarty) {

    // 結果を表示するかどうかのフラグ # 初期値：表示
    $show_flg = isset($args['show']) ? $args['show'] : true;

    // assign変数
    $params  = $smarty->get_template_vars();
    $formData = $params['formData'];
    $price    = $params['price'];
    $yen_mark = $params['yen_mark'];

    switch($formData['lang']){
        case LANG_JPN :
            $price = number_format($price)."円";
            break;

        case LANG_ENG :
            $price = $yen_mark.number_format($price);
            break;

        default:
            $price = number_format($price);
            break;
    }

    $smarty->assign("show_price", $price);

    if($show_flg) return $price;

    return;
}

?>
