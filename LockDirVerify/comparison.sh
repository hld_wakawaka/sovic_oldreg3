#!/bin/sh

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
LANG=C


# BASEDIR=/Users/doishun/Documents/php/sovic_reg3
# BASEDIR=/var/www/vhosts/testhp.jp/httpdocs/reg3
BASEDIR=/var/www/vhosts/evt-reg.jp/httpdocs/reg3

TARGET=$BASEDIR/LockDir/lock*.txt
TARGETPATH=$BASEDIR/LockDir/
COPYFILEALL=$BASEDIR/LockDirVerify/lock*.txt
COPYPATH=$BASEDIR/LockDirVerify/
SUBJECT='lockfile delete'
MAIL='test@evt-reg.jp'


# ロックファイルが存在するフォルダとロックファイルのコピーが存在するフォルダを比較して
# 紐付けられている(対応している)ロックファイルのコピーがあるか確認する
# 存在しない場合はコピーファイルを作成する
for FILENAME in $TARGET
do
        BASEFILENAME=$(basename $FILENAME)
        COPYFILEPATH=$COPYPATH$BASEFILENAME

        # ロックファイルがあるか確認
        if [ -e $FILENAME ];then

                # コピーファイルがあるか確認
                if [ -e $COPYFILEPATH ];then

                        # ロックファイルとロックファイルのコピーを比較する
                        if [ $FILENAME -nt $COPYFILEPATH ];then

                                # ロックファイルが更新されていれば10分前のロックファイルのコピーを更新
                                cp -p $FILENAME $COPYFILEPATH

                        # 10分前のロックファイルのコピーと比較して、ロックファイルが更新されていない場合
                        else

                                # メールを送って10分前のロックファイルと、今のロックファイルを削除
                                cat $FILENAME | mail -s "$SUBJECT" $MAIL
                                rm -f $FILENAME
                                rm -f $COPYFILEPATH
                        fi

                # 現時点でコピーファイルがない場合
                else
                        # ロックファイルのコピーを作成
                        cp -p $FILENAME $COPYFILEPATH
                fi

        # ロックファイルがない場合
        else

                # コピーファイルがあるか確認
                if [ -e $COPYFILEPATH ];then
                        # コピーファイルの削除
                        rm -f $COPYFILEPATH
                fi

        fi
done

# コピーファイルと紐づくロックファイルの存在チェック
# 紐づくロックファイルが存在しなければ、コピーファイル削除
for DELETECOPYFILENAME in $COPYFILEALL
do
        if [ -e $DELETECOPYFILENAME ];then

                DELETEBASEFILENAME=$(basename $DELETECOPYFILENAME)
                BASEFILEPATH=$TARGETPATH$DELETEBASEFILENAME
                if [ ! -e $BASEFILEPATH ];then
                        rm -f $DELETECOPYFILENAME
                fi
        fi

done
